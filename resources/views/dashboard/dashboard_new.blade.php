@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="row">
    <div class="col-md-3 grid-margin d-flex stretch-card">
        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
            <i class="mdi mdi-calendar"></i>&nbsp;
            <span></span> <i class="mdi mdi-caret-down"></i>
        </div>        
    </div>
    <div class="col-md-1">
        <input type="hidden" name="tgl_awal" id="tgl_awal">
        <input type="hidden" name="tgl_akhir" id="tgl_akhir">
        <button type="button" class="btn btn-info btn-icon-text btn-sm" onclick="getDashboard()">Filter</button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 flex-column d-flex stretch-card">
        <div class="row">            
            <div class="col-lg-4 d-flex grid-margin stretch-card">
                <div class="card sale-visit-statistics-border">
                    <div class="card-body">
                        <h2 class="text-dark mb-2 font-weight-bold" id="total_so"></h2>
                        <h4 class="card-title mb-2">SO (Sales Order)</h4>
                        <small class="text-muted" id="rangedate1"></small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex grid-margin stretch-card">
                <div class="card sale-visit-statistics-border">
                    <div class="card-body">
                        <h2 class="text-dark mb-2 font-weight-bold" id="total_rfp"></h2>
                        <h4 class="card-title mb-2">JO (Job Order)</h4>
                        <small class="text-muted" id="rangedate2"></small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex grid-margin stretch-card">
                <div class="card sale-visit-statistics-border">
                    <div class="card-body">
                        <h2 class="text-dark mb-2 font-weight-bold" id="total_kp"></h2>
                        <h4 class="card-title mb-2">Kartu Produksi</h4>
                        <small class="text-muted" id="rangedate3"></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag1"></h2>
                <h4 class="card-title mb-2">Pemartaian</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-2 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag2"></h2>
                <h4 class="card-title mb-2">Belah Greige</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag3"></h2>
                <h4 class="card-title mb-2">Scouring/Bleaching</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-2 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag4"></h2>
                <h4 class="card-title mb-2">Buka Kain/Revisi</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag5"></h2>
                <h4 class="card-title mb-2">Heatset/LP/LPP</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="total_print"></h2>
                <h4 class="card-title mb-2">Printing</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-2 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag7"></h2>
                <h4 class="card-title mb-2">Curing</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-3 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag8"></h2>
                <h4 class="card-title mb-2">Finishing</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-2 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag9"></h2>
                <h4 class="card-title mb-2">QC</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
    <div class="col-lg-2 d-flex grid-margin stretch-card">
        <div class="card sale-visit-statistics-border">
            <div class="card-body">
                <h2 class="text-dark mb-2 font-weight-bold" id="bag10"></h2>
                <h4 class="card-title mb-2">Delivery</h4>
                <small class="text-muted" class="rangedate"></small>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <h4 class="card-title">Sales Order (Kg)</h4>
                    <h4 class="text-success font-weight-bold">SO/Bulan</h4>
                </div>
                <div id="support-tracker-legend" class="support-tracker-legend"></div>
                <canvas id="supportTracker"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-lg-flex align-items-center justify-content-between mb-4">
                    <h4 class="card-title">Backlog</h4>
                    <p class="text-dark" id="order_deliv"></p>
                </div>
                <div class="product-order-wrap padding-reduced">
                    <div id="productorder-gage" class="gauge productorder-gage"></div>
                </div>
            </div>
        </div>
    </div> 
</div>
<!-- blm dipake karna keuangan blm masuk -->
@if(Auth::user()->role==99)
<div class="row">
    <div class="col-sm-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <h4 class="card-title">SO vs NOTA</h4>
                    <h4 class="text-success font-weight-bold" id="sonota"></h4>
                </div>
                <div id="support-tracker-legend-so-nota" class="support-tracker-legend"></div>
                <canvas id="supportSONOTA"></canvas>
            </div>
        </div>
    </div>   
</div>
{{-- <div class="row">
    <div class="col-sm-12 grid-margin d-flex stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <h4 class="card-title mb-2">Sales Difference</h4>                    
                </div>
                <div>
                    <ul class="nav nav-tabs tab-no-active-fill" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active pl-2 pr-2" id="revenue-for-last-month-tab"
                                data-toggle="tab" href="#revenue-for-last-month" role="tab"
                                aria-controls="revenue-for-last-month" aria-selected="true">Revenue for last
                                month</a>
                        </li>                        
                    </ul>
                    <div class="tab-content tab-no-active-fill-tab-content">
                        <div class="tab-pane fade show active" id="revenue-for-last-month" role="tabpanel"
                            aria-labelledby="revenue-for-last-month-tab">
                            <div class="d-lg-flex justify-content-between">
                                <p class="mb-4">+5.2% vs last 7 days</p>
                                <div id="revenuechart-legend" class="revenuechart-legend">f</div>
                            </div>
                            <canvas id="revenue-for-last-month-chart"></canvas>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endif
<script src="{{ asset('js/dashboard_new.js') }}"></script>
@endsection