@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List RFP Information</h4>
            <p class="card-description">
                <!-- <button type="button" onclick="loadNewPage('{{ route('form.sales_order') }}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Tambah Data
                </button> -->
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>SO Number</th>
                            <th>Customer</th>
                            <th>Tgl Produksi</th>                            
                            <th>Original Kondisi</th>                            
                            <th>Total CW</th>                                                        
                            <th>Total Roll</th>
                            <th>Total Kg</th>
                            <th>Total Mtr</th>                                                                                                                                            
                        </tr>
                    </thead>
                    <tbody>                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('qc.action')
@endsection