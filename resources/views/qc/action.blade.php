<script>
var act_url = '{{ route('dataqc') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_so', name: 'i_no_so' },        
        { data: 'e_nama_pel', name: 'e_nama_pel' },        
        { data: 'tgl_prod', name: 'tgl_prod' },
        { data: 'ori_kond', name: 'ori_kond' },
        { data: 'total_cw', name: 'total_cw' },        
        { data: 'roll', name: 'roll' },
        { data: 'kg', name: 'kg' },
        { data: 'meter', name: 'meter' },
    ]
});

function createPack(i_no_rfp, i_no_kartu, id_rfp, id_kartu)
{
    $.ajax({
        type: "GET",
        url: base_url + '/qc/cek_perisapan/'+ id_rfp,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {
            
        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);
        if(data.code==1){
            loadNewPage(base_url + '/qc/create_pack?norfp='+i_no_rfp+'&&no_kartu='+i_no_kartu+'&&idrfp='+id_rfp+'&&idkartu='+id_kartu);
        } else {
            swal('Peringatan',data.msg,'warning');
        }
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
}

function lihatPack(i_no_rfp, i_no_kartu, id_rfp, id_kartu)
{
    $.ajax({
        type: "GET",
        url: base_url + '/qc/cek_perisapan/'+ id_rfp,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {
            
        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);
        if(data.code==1){
            loadNewPage(base_url + '/qc/lihat_packing?no_rfp=' +i_no_rfp+ '&&no_kartu=' +i_no_kartu+ '&&id_rfp=' +id_rfp+ '&&id_kartu=' +id_kartu);
        } else {
            swal('Peringatan',data.msg,'warning');
        }
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
}

function savePack()
{
    $('#form_packing').validate({
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.css('font-size','14px');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_kartu = $('#form_packing');
            var _form_data = new FormData(form_kartu[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/qc/save_pack',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('qc') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data Packing List Tidak Jadi Disimpan :)", "error");   
                } 
            });
        }
    });
    $('input[id^="asal_sj"]').each(function (k,v) {                
        $(this).rules('add', {
            required: !0,
            // min: 1
        });
    });
    $('input[id^="asal_kp"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,
            // min: 1
        });
    });
    $('input[id^="jadi_kp"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,
            // min: 1
        });
    });
}
</script>