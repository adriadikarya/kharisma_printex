@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['data'])
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_packing">
    @csrf
        <div class="card">
            <div class="card-body text-dark">
                <h2>Packing List From {{$param['nokartu']}} / {{$param['norfp']}}</h2>
                <br>
                <h2>Total Keseluruhan Asal SJ ({{ $param['total_input'][0]->tot_asal_sj }}), Asal KP ({{ $param['total_input'][0]->tot_asal_kp }}), Jadi KP ({{ $param['total_input'][0]->tot_jadi_kp }})</h2>
                <input type="hidden" name="idrfp" id="idrfp" value="{{$idrfp}}"/>
                <input type="hidden" name="idkartu" id="idkartu" value="{{$idkartu}}"/>
                <div class="row">                
                    <input type="hidden" name="totalcw" id="totalcw" value="{{count($param['data'])}}">
                    @foreach($param['data'] as $key => $item)
                    @php 
                        $e_kode = \DB::select("select e_kode from mst_packing_list where i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' group by e_kode order by left(e_kode, 1),COALESCE(substring(e_kode,'\d+')::int,0)");                        
                        $jml = count($e_kode);
                    @endphp
                    <input type="hidden" name="jml{{$key+1}}" id="jml{{$key+1}}" value="{{$jml}}">
                    <input type="hidden" name="id_cw{{$key+1}}" id="id_cw{{$key+1}}" value="{{$item->id_cw}}"/>
                    @foreach($e_kode as $kiy => $kode)
                    @php
                        $list_pack = \DB::select("select * from mst_packing_list where e_kode='".$kode->e_kode."' AND i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' order by i_id");                        
                    @endphp                    
                    <div class="col-md-4">
                        <div class="table-responsive pt-1">
                        <table class="table table-bordered" id="table-harga">
                            <thead style="background-color: lightcyan;">
                                <tr>
                                    <th colspan="4" style="text-align:center;">{{$item->e_uraian_pekerjaan}}</th>
                                <tr>
                                <tr>
                                    <th style="text-align:center;">Roll</th>                                    
                                    <th style="text-align:center;width:150px;">Asal SJ</th>
                                    <th style="text-align:center;width:150px;">Asal KP</th>
                                    <th style="text-align:center;width:150px;">Jadi KP</th>
                                </tr>
                                <tr>                          
                                @php $kd = substr($kode->e_kode,0,1); @endphp      
                                    <th style="text-align:center;">{{$kode->e_kode}}</th>
                                    <input type="hidden" name="kode_dyeing{{$kd}}{{$kiy+1}}" name="kode_dyeing{{$kd}}{{$kiy+1}}" value="{{$kode->e_kode}}"/>
                                    <th style="text-align:center;">Kg</th>
                                    <th style="text-align:center;">Kg</th>
                                    <th style="text-align:center;">Kg</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($list_pack as $koy => $lp)
                            @if(!$lp->f_jadi_sj)
                                <tr>
                                    <td align="center"><input type="hidden" id="id_roll_pack{{$kode->e_kode}}-{{$koy+1}}" name="id_roll_pack{{$kode->e_kode}}-{{$koy+1}}" value="{{$lp->i_id}}">{{$koy+1}}</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_sj{{$kode->e_kode}}-{{$koy+1}}" id="asal_sj{{$kode->e_kode}}-{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_asal_sj}}" readonly></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$kode->e_kode}}-{{$koy+1}}" id="asal_kp{{$kode->e_kode}}-{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_asal_kp}}"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="jadi_kp{{$kode->e_kode}}-{{$koy+1}}" id="jadi_kp{{$kode->e_kode}}-{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_jadi_kp}}"></td>
                                </tr>
                            @endif
                            @endforeach
                            <input type="hidden" name="count_list{{$kode->e_kode}}" value="{{count($list_pack)}}"/>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    @endforeach
                    @endforeach
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <button type="submit" onclick="savePack()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>
                <button type="button" onclick="loadNewPage('{{ route('qc') }}')"
                    class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </div>
        </div>
    </form>
</div>
@endif
@include('qc.action')
@endsection