@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@foreach($data_so as $item)
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_rfp">
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Job Order</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. JO</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="id_so" id="id_so" value="{{ $id_so }}"/>
                                <input type="text" class="form-control form-control-sm border-dark" name="no_rfp" id="no_rfp" maxlength="16" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Target Selesai</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm border-dark" name="d_selesai" id="d_selesai" value="{{ date('d-m-Y',strtotime($item->d_penyerahan_brg)) }}" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label">Keterangan Khusus</label>
                            <div class="col-sm-11">
                                <textarea type="text" class="form-control form-control-sm border-dark" name="ket_khusus" id="ket_khusus" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">A. CUSTOMER DESCRIPTION</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Company Name (<span style="color:red;">Nama Perusahaan</span>):</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="i_pel" id="i_pel" value="{{ $item->i_pel }}"/>
                                <input type="text" class="form-control form-control-sm border-dark" name="nama_pel"
                                    id="nama_pel" value="{{ $item->e_nama_pel }}" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Contact Person (<span style="color:red;">Penanggungjawab</span>):</label>
                            <div class="col-sm-8">                    
                                <input type="text" class="form-control form-control-sm border-dark" name="contact_person"
                                    id="contact_person" value="{{ $item->e_kont_pel }}" maxlength="50"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-sm-2">
                        <h4 class="card-title" style="margin-top: 4%;">B. DESIGN DESCRIPTION</h4>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control form-control-sm border-dark" name="repeat_order"
                                id="repeat_order" readonly>
                            <option value="new" @if($item->f_repeat==false) selected @else disabled @endif >New (<span style="color:red;">Baru</span>)</option>
                            <option value="ro" @if($item->f_repeat==true) selected @else disabled @endif >Repeat (<span style="color:red;">Ulangan</span>)</option>
                        </select>       
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">C. DESIGN IDENTIFICATION</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">C1. DESIGN (<span style="color:red;">Nomor Desain</span>):</label>
                            <div class="col-sm-8">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="desain_kode"
                                    id="desain_kode" value="{{ $item->i_desain }}" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">C2. DESIGN NAME (<span style="color:red;">Nama Desain</span>):</label>
                            <div class="col-sm-8">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="motif_name"
                                    id="motif_name" value="{{ $item->e_motif }}" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">C3. TGL.APPROVAL STRIKE OFF (<span style="color:red;">Persetujuan Strike off</span>):</label>
                            <div class="col-sm-8">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="tgl_approval_strike_off"
                                    id="tgl_approval_strike_off" value="{{ date('d-m-Y',strtotime($item->d_approval_strike_off)) }}" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">D. MATERIAL DESCRIPTION</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">D1. MATERIAL (<span style="color:red;">Jenis Bahan</span>):</label>
                            <div class="col-sm-4">                                
                                <select class="form-control form-control-sm border-dark " name="jns_bahan1" id="jns_bahan1" style="display:block" onchange="jnsBahanLain(this.value)">
                                    <option value="">Pilih Jenis Bahan!</option>
                                    @if($jns_bahan)
                                        @foreach($jns_bahan as $jnsbhn)
                                        <option value="{{$jnsbhn->i_id}}">{{$jnsbhn->e_jns_bahan}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm border-dark" name="jns_bahan2" id="jns_bahan2" style="display:none" maxlength="50"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" style="margin-bottom: -55px;">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">D2. ORIGINAL CONDITION (<span style="color:red;">Kondisi Awal</span>):</label>
                            <div class="col-sm-4">                                
                                <select class="form-control form-control-sm border-dark " name="ori_kondisi1" id="ori_kondisi1">                                    
                                    @if($ori_kon1)
                                        @foreach($ori_kon1 as $orikon1)
                                        <option value="{{$orikon1->i_id}}" @if($item->e_kondisi_kain==$orikon1->i_id) selected @elseif($item->e_kondisi_kain>2) selected @endif>{{$orikon1->e_ori_kondisi}}</option>
                                        @endforeach
                                    @endif                                  
                                </select>
                            </div>
                            <div class="col-sm-4">                                
                                <select class="form-control form-control-sm border-dark " name="ori_kondisi2" id="ori_kondisi2">                                        
                                    @if($ori_kon2)
                                        @foreach($ori_kon2 as $orikon2)
                                        <option value="{{$orikon2->i_id}}">{{$orikon2->e_ori_kondisi}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-4 col-form-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="ket_ori_kondisi" id="ket_ori_kondisi" placeholder="Keterangan"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">D3. JENIS PRINT:</label>
                            <div class="col-sm-4">                                
                                <select class="form-control form-control-sm border-dark " name="jns_print" id="jns_print" disabled>                             
                                    @if($jns_printing)
                                        @foreach($jns_printing as $jnsprint)
                                        <option value="{{$jnsprint->i_id}}" @if($item->e_jenis_printing==$jnsprint->i_id) selected @endif>{{$jnsprint->e_jns_printing}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <input type="hidden" name="jns_print" value="{{$item->e_jenis_printing}}">
                            </div>                            
                        </div>
                    </div>
                    {{--<div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">D4. JENIS KAIN:</label>
                            <div class="col-sm-4">                                
                                <select class="form-control form-control-sm border-dark " name="jns_kain" id="jns_kain" disabled> 
                                @foreach($jenis_kain as $kain)
                                    <option value="{{ $kain->i_id }}" @if($item->i_jns_kain==$kain->i_id) selected @endif>{{ $kain->e_kain }}</option>
                                @endforeach
                                </select>
                                <input type="hidden" name="jns_kain" value="{{$item->i_jns_kain}}">
                            </div>                            
                        </div>
                    </div>--}}
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">D4. COLOR (<span style="color:red;">Warna Dasar</span>):</label>
                            <div class="col-sm-4">                                
                                <select class="form-control form-control-sm border-dark " name="warna" id="warna" style="display:block" onchange="ketWarna(this.value)">                             
                                    @if($warna_dasar)
                                        @foreach($warna_dasar as $wrndasar)
                                        <option value="{{$wrndasar->i_id}}">{{$wrndasar->e_warna_dasar}}</option>
                                        @endforeach
                                    @endif                                    
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control form-control-sm border-dark" name="ket_warna" id="ket_warna" style="display:none" maxlength="50"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mb-0">
                        <div class="form-group row mb-0">
                            <label class="col-sm-4 col-form-label">D5. MATERIAL SUPPLY (<span style="color:red;">Pasokan Material</span>):</label>
                            <div class="col-sm-8">                                
                                <div class="form-group row">
                                    <label class="col-sm-5 col-form-label">RECEIVE FROM (<span style="color:red;">Diterima Dari</span>):</label>
                                    <div class="col-sm-7">
                                        <input type="hidden" name="receive_id" id="receive_id" value="{{ $item->i_penyedia }}">
                                        <input type="text" class="form-control form-control-sm border-text" name="receive_name" id="receive_name" 
                                        @if($item->i_pel==$item->i_penyedia) value="{{ $item->e_nama_pel }}" @else value="PT. Kharisma Printex" @endif readonly>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">                                
                                <div class="form-group row">
                                    <label class="col-sm-6 col-form-label">INCOMING DATE (<span style="color:red;">Tanggal Masuk</span>):</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-sm tgl_input border-text" name="d_tgl_material" id="d_tgl_material" readonly>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group row mb-0">
                            <label class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">QTY (<span style="color:red;">Jumlah *KG</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" style="text-align:right" name="n_qty_material" id="n_qty_material" autocomplete="off" @if($item->qty_tot==0) value="0" @else value="{{ number_format($item->qty_tot) }}" readonly @endif>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="col-md-6 mb-0">
                        <div class="form-group row mb-0">
                            <label class="col-sm-4 col-form-label">D6. Nomor LPK: </label>
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-rounded btn-info btn-sm" onclick="addRowLpk()"><span class="mdi mdi-plus"></span></button>
                                <button type="button" class="btn btn-rounded btn-danger btn-sm"><span class="mdi mdi-minus" onclick="removeRowLpk()"></span></button>
                                <input type="hidden" id="row_lpk" name="row_lpk" value="1">
                            </div>
                            <div class="col-sm-7" id="html_lpk">   
                                <div class="form-group row mb-0">                             
                                    <input type="text" class="form-control form-control-sm border-dark col-md-6" placeholder="Nomor LPK 1" name="lpk1" id="lpk1" maxlength="50" minlength="3"> 
                                    <input type="text" class="form-control form-control-sm border-dark col-md-6" placeholder="Qty LPK 1 (Roll)" name="qty_lpk1" id="qty_lpk1" style="text-align:right;" onkeydown="hanyaangka();" maxlength="8">
                                </div>
                            </div>                            
                        </div>                                                                           
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">E. END PRODUCT IDENTIFICATION</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">      
                            <input type="hidden" name="tot_color_way" value="{{ count($data_so_item) }}">
                            <label class="col-sm-2 col-form-label">E1. COLOR WAYS (<span style="color:red;">Jalur warna</span>):</label>                      
                            @foreach($data_so_item as $key => $item_so)
                            @php $row = $key + 1; @endphp
                            @if($row == 1)                            
                            <div class="col-sm-5">                                
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">CW {{$row}}:</label>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-6" name="cw{{$row}}" id="cw{{$row}}" maxlength="50" readonly>{{$item_so->e_uraian_pekerjaan.' ('.$item_so->n_qty_kg.'kg)'}}</textarea>
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-3"  readonly>{{date('d-m-Y', strtotime($item_so->d_strike_off))}}</textarea>
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-3"   readonly>{{$item_so->e_kain}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @elseif($row % 2 == 0)                                                      
                            <div class="col-sm-5">                                
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">CW {{$row}}:</label>
                                    <div class="col-sm-10">                                        
                                        <div class="row">
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-6" name="cw{{$row}}" id="cw{{$row}}" maxlength="50" readonly>{{$item_so->e_uraian_pekerjaan.' ('.$item_so->n_qty_kg.'kg)'}}</textarea>
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-3"  readonly>{{date('d-m-Y', strtotime($item_so->d_strike_off))}}</textarea>
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-3"  readonly>{{$item_so->e_kain}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            @else                            
                            <div class="col-sm-2">&nbsp;</div>
                            <div class="col-sm-5">                                
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">CW {{$row}}:</label>
                                    <div class="col-sm-10">
                                        <div class="row">
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-6" name="cw{{$row}}" id="cw{{$row}}" maxlength="50" readonly>{{$item_so->e_uraian_pekerjaan.' ('.$item_so->n_qty_kg.'kg)'}}</textarea>
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-3"  readonly>{{date('d-m-Y', strtotime($item_so->d_strike_off))}}</textarea>
                                            <textarea type="text" class="form-control form-control-sm border-dark col-sm-3"  readonly>{{$item_so->e_kain}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">E2. FINISHING: (<span style="color:red;">Penyempurnaan</span>)</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Konstruksi/Gramasi:</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="gramasi_from" id="gramasi_from" placeholder="gr/m2" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="gramasi_to" id="gramasi_to" placeholder="gr/m2" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Penyesuaian Lebar :</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="penyesuaian_from" id="penyesuaian_from" placeholder="inch/cm" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="penyesuaian_to" id="penyesuaian_to" placeholder="inch/cm" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Kondisi Asal :</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="kon_asal_from" id="kon_asal_from" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="kon_asal_to" id="kon_asal_to" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Warna Kain :</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="wrn_kain_from" id="wrn_kain_from" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="wrn_kain_to" id="wrn_kain_to" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Tekstur Akhir :</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="tekstur_from" id="tekstur_from" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="tekstur_to" id="tekstur_to" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>--}}
                    <!-- <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">E1. SHRINKAGE (<span style="color:red;">Pengkerutan</span>):</label>
                            <div class="col-sm-8">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="pengkerutan" style="text-align:right"
                                    id="pengkerutan" placeholder="%"/>
                            </div>
                        </div>
                    </div>                    -->
                    <div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">E3. SHRINKAGE: (<span style="color:red;">Pengkerutan</span>)</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Pakan :</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="pakan_from" id="pakan_from" placeholder="gr/m2" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="pakan_to" id="pakan_to" placeholder="gr/m2" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Lusi :</label>
                                    <label class="col-sm-3 col-form-label">From (<span style="color:red;">Dari</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="lusi_from" id="lusi_from" placeholder="inch/cm" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control form-control-sm border-dark" name="lusi_to" id="lusi_to" placeholder="inch/cm" style="text-align:right" maxlength="50"> 
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row mb-0">
                            <label class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-5">    
                                <div class="form-group row"> 
                                    <label class="col-sm-5 col-form-label">Tekstur Akhir (<span style="color:red;">Final Texture</span>) :</label>
                                    <label class="col-sm-3 col-form-label">To (<span style="color:red;">Menjadi</span>):</label>
                                    <div class="col-sm-4">
                                        <select class="form-control border-dark" name="tekstur_to" id="tekstur_to">
                                            @if($tekstur_akhir)
                                            @foreach($tekstur_akhir as $ta)
                                                <option value="{{$ta->i_id}}">{{$ta->e_tekstur}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div> 
                                </div>                               
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">E. END PRODUCT IDENTIFICATION</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">E1. COLOR WAYS (<span style="color:red;">Jalur warna</span>):</label>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 1:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw1"
                                        id="cw1" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 5:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw5"
                                        id="cw5" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">&nbsp;</label>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 2:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw2"
                                        id="cw2" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 6:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw6"
                                        id="cw6" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">&nbsp;</label>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 3:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw3"
                                        id="cw3" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 7:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw7"
                                        id="cw7" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">&nbsp;</label>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 4:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw4"
                                        id="cw4" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">                                
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">CW 8:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="cw8"
                                        id="cw8" maxlength="50"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="card">
            <div class="card-body">
                <button type="submit" onclick="return prosesSimpan('simpan')" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>
                <button type="button" onclick="loadNewPage('{{ route('rfp.index') }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </div>
        </div>
    </form>
</div>
@endforeach
@include('reqfp.action_form')
@endsection