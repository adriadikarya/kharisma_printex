<script>
var act_url = '{{ route('data_rfp_prod.rfp') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_rfp', name: 'i_no_rfp' }, 
        { data: 'd_selesai', name: 'd_selesai' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },       
        { data: 'f_repeat', name: 'f_repeat' },        
        { data: 'i_no_so', name: 'i_no_so' },
        { data: 'created_at', name: 'created_at' }
    ]
});

function approvedRfpP(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Approved Data JO ini?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/rfp/approved_prod/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('list_approved_prod.rfp') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data JO tidak jadi diapproved','info');
        }
    });
}

function rejectRfpP(id)
{
    swal({
        title: "Reject JO",
        text: "Tulis Alasan Reject",
        type: "input",
        inputPlaceholder: "Alasan",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Lanjut",                     
        cancelButtonText: "Tidak", 
        closeOnConfirm: false,                      
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue.length>200){
            alert('Tidak boleh lebih dari 200 kata');
            return false;
        }
        if (inputValue === "") {
            alert('Alasan harus diisi!');
            return false;
        } else {
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Akan Reject Data JO ini?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "GET",
                        url: base_url + '/rfp/reject_prod/'+ id,
                        data: {input_val: inputValue},
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();            
                        var data = $.parseJSON(res);
                        swal({   
                            title: "Sukses",   
                            text: data.msg,   
                            type: "success",                       
                            showCancelButton: false,   
                            confirmButtonColor: "#e6b034",   
                            confirmButtonText: "Ya",                     
                            closeOnConfirm: true,                                       
                            showLoaderOnConfirm: true 
                        }, function(isConfirm){   
                            if(isConfirm){
                                loadNewPage('{{ route('list_approved_prod.rfp') }}');
                            }
                        });
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });
                } else {
                    swal('Batal','Data JO tidak jadi direject','info');
                }
            });
        }
    });
}

function lihatRfp(id,link)
{
    loadNewPage(base_url + '/rfp/lihat/' +id +'/'+ link);
}
</script>