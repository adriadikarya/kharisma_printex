<script>
var act_url = '{{ route('data.rfp') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_so', name: 'i_no_so' },        
        { data: 'e_nama_pel', name: 'e_nama_pel' },
        { data: 'd_approved_mrk', name: 'd_approved_mrk' },        
        { data: 'd_approved_pro', name: 'd_approved_pro' },
        { data: 'definition', name: 'definition' },
        { data: 'i_no_rfp', name: 'i_no_rfp' },
        { data: 'alasan_reject', name: 'alasan_reject' },
        { data: 'nama_bagian', name: 'nama_bagian' }
    ]
});

function buatRfp(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Buat JO?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: true,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) { 
            $('#loading').show();
            loadNewPage(base_url + '/rfp/create/' +id);
        } else {
            swal('Batal', 'Tidak Jadi Buat JO!', 'error');
        }
    });
}

function kirimRfpMkt(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Kirim Data JO ke Manager Marketing?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/rfp/kirim_approved_mkt/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('rfp.index') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi dikirim ke manager','info');
        }
    });
}

function kirimRfpProd(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Kirim Data JO ke Manager Produksi?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/rfp/kirim_approved_prod/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('rfp.index') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi dikirim ke manager','info');
        }
    });
}

function kirimRfpPPC(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Kirim Data JO ke PPC?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/rfp/kirim_ke_ppc/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('rfp.index') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi dikirim ke ppc','info');
        }
    });
}

function lihat(id,link)
{
    loadNewPage(base_url + '/sales_order/lihat/' +id +'/'+ link);
}

function lihatRfp(id,link)
{
    loadNewPage(base_url + '/rfp/lihat/' +id +'/'+ link);
}

function editRfp(id)
{
    loadNewPage(base_url + '/rfp/edit_rfp/' + id);
}

function printRfp(id, link)
{
    loadNewPage(base_url + '/rfp/print_rfp/' + id + '/' + link);
}
</script>