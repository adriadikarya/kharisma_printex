<script type="text/javascript">

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 

$('#no_rfp').keyup(function(){
    this.value = this.value.toUpperCase();
});

$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});

function jnsBahanLain(nilai)
{
    if(nilai=="3") {
        $('#jns_bahan2').show();
    } else {
        $('#jns_bahan2').hide();
        $('#jns_bahan2').val('');
    }
}

function ketWarna(nilai)
{
    if(nilai=="4") {
        $('#ket_warna').show();
    } else {
        $('#ket_warna').hide();
        $('#ket_warna').val('');
    }
}

function prosesSimpan(text)
{
    $('#form_rfp').validate({
        rules: {
            // sementara tidak dipakai
            // no_rfp: {
            //     required: !0,
            //     maxlength: 16
            // },
            d_selesai: {
                required: !0
            },
            contact_person: {
                required: !0,
                maxlength: 50                
            },
            jns_bahan1: {
                required: !0
            },
            jns_bahan2: {
                required: $('#jns_bahan1')=='Others'?!0:!1,
                maxlength: 50
            },
            ket_warna: {
                required: $('#warna')=='Others'?!0:!1,
                maxlength: 50
            },
            d_tgl_material: {
                required: !0
            },
            n_qty_material: {
                required: !0,
                number: !0
            },
            pengkerutan: {
                required: !0,
                number: !0
            },
            gramasi_from: {
                required: !0
            },
            gramasi_to: {
                required: !0
            },
            penyesuaian_from: {
                required: !0
            },
            penyesuaian_to: {
                required: !0
            },
            pakan_from: {
                required: !0
            },
            pakan_to: {
                required: !0
            },
            lusi_from: {
                required: !0
            },
            lusi_to: {
                required: !0
            },
            tekstur_to: {
                required: !0
            }
        },
        messages: {
            no_rfp: {
                required: "Tolong isi dulu no rfp nya ya :)",
                maxlength: "Kode RFP Tidak boleh lebih dari 16 karakter ya :)"
            },
            d_selesai: "Tolong dipilih dulu tanggal selesainya nya ya :)",
            contact_person: {
                required: "Tolong diisi penanggung jawab nya ya :)",
                maxlength: "Penanggung jawab tidak boleh lebih dari 50 karakter :)"
            },
            jns_bahan1: "Tolong dipilih dulu jenis bahan nya ya :)",
            jns_bahan2: {
                required: "Tolong diisi jenis bahan nya ya :)",
                maxlength: "Jenis bahan tidak boleh lebih dari 50 karakter :)"
            },
            ket_warna: {
                required: "Tolong diisi ya warna nya :)",
                maxlength: "warna tidak boleh lebih dari 50 karakter"
            },
            d_tgl_material: "Tolong dipilih dulu tanggal masuk barang nya ya :)",
            n_qty_material: {
                required: "Tolong diisi ya jumlah barang masuk nya :)",
                number: "Tolong masukan angka saja, jika ingin koma pakai titik ya :)"
            },
            pengkerutan: {
                required: "Tolong diisi dulu pengkerutan nya ya :)",
                number:"Tolong masukan angka saja, jika koma pakai titik ya :)"
            },
            benang_from: "Tolong diisi dulu finishing benang from nya ya :)",
            benang_to: "Tolong diisi dulu finishing benang to nya ya :)",
            penyesuaian_from: "Tolong diisi dulu finishing penyesuaian lebar from nya ya :)",
            penyesuaian_to: "Tolong diisi dulu finishing penyesuaian lebar to nya ya :)",
            kon_asal_from: "Tolong diisi dulu finishing kondisi asal from nya ya :)",
            kon_asal_to: "Tolong diisi dulu finishing kondisi asal to nya ya :)",
            wrn_kain_from: "Tolong diisi dulu finishing warna kain from nya ya :)",
            wrn_kain_to: "Tolong diisi dulu finishing warna kain to nya ya :)",
            tekstur_from: "Tolong diisi dulu finishing tekstur akhir from nya ya :)",
            tekstur_to: "Tolong diisi dulu finishing tekstur akhir to nya ya :)"
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_rfp');
            var _form_data = new FormData(form_rfp[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/rfp/'+text,
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('rfp.index') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data JO Tidak Jadi Di"+text+" :)", "error");   
                } 
            });
        }
    });
    $('input[id^="lpk"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,            
            maxlength: 50,
            minlength: 3, 
            messages: {
                required: "Tolong Diisi dulu Nomor LPK nya ya :)",
                minlength: "Tolong Minimal Nomor LPK adalah 3 karakter :)"
            }
        });
    });
    $('input[id^="qty_lpk"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,            
            maxlength: 8
        });
    });
}

function addRowLpk()
{
    var counter = parseInt($('#row_lpk').val())+1;
    var cols = "";
    cols +='<div class="form-group row mb-0">';
    cols +='<input type="text" class="form-control form-control-sm border-dark col-md-6" placeholder="Nomor LPK '+counter+'" name="lpk'+counter+'" id="lpk'+counter+'" maxlength="50" minlength="3">'; 
    cols += '<input type="text" class="form-control form-control-sm border-dark col-md-6" placeholder="Qty LPK '+counter+' (Roll)" name="qty_lpk'+counter+'" id="qty_lpk'+counter+'" style="text-align:right;" onkeydown="hanyaangka();" maxlength="8">';
    cols += '</div>'
    $("#html_lpk").append(cols);
    $('#row_lpk').val(counter);       
}

function removeRowLpk()
{
    var counter = parseInt($('#row_lpk').val());
    // console.log(counter);
    if(counter>1)
    {
        $('#lpk'+counter).remove(); 
        $('#idlpk'+counter).remove();
        $('#qty_lpk'+counter).remove();       
        counter -= 1;
        $('#row_lpk').val(counter);	
    } else {
        // $('#lpk'+counter).remove();
        $('#row_lpk').val(1);   
    }
}

</script>