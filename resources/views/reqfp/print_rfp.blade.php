@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['datahead'])
@foreach($param['datahead'] as $datahead)
<style type="text/css" media="print">
    @page 
        {            
            /*size: auto;*/
            /*size: F4;*/
            /*size: auto;    auto is the current printer page size */
            margin: 0cm;  /* this affects the margin in the printer settings */
        }
    .isinya {
        font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
        font-size: 12px;
    }
    .noDisplay{
        display:none;
    }
    .pageBreak {
        page-break-before: always;
    }
</style>
<div class="col-12 grid-margin">    
    <div class="card noDisplay">
        <div class="card-body">                
            <button type="button" onclick="loadNewPage('{{ route($rut) }}')" class="btn btn-warning btn-icon-text">
                <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                Kembali
            </button>
            <button type="button" onclick="printDiv('media_print')" class="btn btn-info btn-icon-text">
                <i class="mdi mdi-printer btn-icon-prepend"></i>
                Print
            </button>
        </div>
    </div>
    <div id="media_print">
        <div class="card">
            <div class="card-body text-dark">
                <div class="row">
                    <div class="col-md-10">&nbsp;</div>
                    <div class="col-md-2">
                        <p class="text-right" style="font-size: 13px;"><b><i>FR.MK.01.06</i></b></p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body border border-dark">
                        <div class="row">
                            <div class="col-md-8 p-0">
                                 <h2 class="border border-dark text-center mb-0"><b>Job Order (JO) <br> <i style="color:red;">Perintah Produksi</i></b></h2>                                
                            </div>
                            <div class="col-md-4 p-0">
                                <div class="border border-dark col-md-12 mb-0"><p class="text-left align-text-bottom"><b>No.JO</b> : {{$datahead->i_no_rfp}}</p>
                                </div>
                                <div class="border border-dark col-md-12" style="height: 32px;"><p class="text-left align-text-bottom"><b>Target Selesai</b> : {{date('d M Y', strtotime($datahead->d_selesai))}}</p>
                                </div>                                
                            </div>                            
                        </div>           
                        <br>             
                        <div class="row">
                            <div class="col-md-4">
                                <p class="text-left" style="font-size: 20px;"><b>A. CUSTOMER DESCRIPTION</b></p>
                            </div>
                            <div class="col-md-8 p-0">
                                <div class="col-md-12 mb-0"><p class="text-left">Company name :<br><i style="color:red; font-size:12px;">Nama perusahaan</i></p></div>
                                <div class="border border-dark col-md-12">
                                    <span style="">{{$datahead->e_nama_pel}}</span>
                                </div>
                                <div class="col-md-12 mb-0"><p class="text-left">Contact Person :<br><i style="color:red; font-size:12px;">Penanggung Jawab</i></p></div>
                                <div class="border border-dark col-md-12">
                                    <span style="">{{$datahead->e_contact_person}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="row">                        
                            @php
                            if($datahead->f_repeat==false){
                                $mdi1 = "mdi mdi-check";
                                $mdi2 = "mdi mdi-close";
                            } else {
                                $mdi1 = "mdi mdi-close";
                                $mdi2 = "mdi mdi-check";
                            }
                            @endphp
                            <div class="col-md-4">
                                <p class="text-left" style="font-size: 20px;"><b>B. DESIGN DESCRIPTION</b></p>
                            </div>
                            <div class="col-md-1 p-0">
                                <div class="col-md-12"><p class="text-right">New :<br><i style="color:red; font-size:12px;">Baru</i></p></div>
                            </div>                           
                            <div class="col-md-1 p-0">
                                <div class="h-75 border border-dark col-md-8 text-center">
                                    <span class="{{$mdi1}}" style="font-size:25px;"></span>
                                </div>
                            </div>
                            <div class="col-md-2 p-0">
                                <div class="col-md-12"><p class="text-right">Repeat :<br><i style="color:red; font-size:12px;">Ulangan</i></p></div>
                            </div>
                            <div class="col-md-2 p-0">
                                <div class="h-75 border border-dark col-md-4 text-center">
                                <span class="{{$mdi2}}" style="font-size:25px;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p class="text-left" style="font-size: 20px;"><b>C. DESIGN INDENTIFICATION</b></p>
                            </div>
                            <div class="col-md-8 p-0">
                                <div class="row mb-1">
                                    <div class="col-md-6 mb-0"><p class="text-left">C1. DESIGN #<br><i style="color:red; font-size:12px;">Nomor Desain</i></p></div>:&nbsp;
                                    <div class="border border-dark col-md-5" style="height:40px;">
                                        <span style="">{{$datahead->i_desain}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-0"><p class="text-left">C2. DESIGN NAME<br><i style="color:red; font-size:12px;">Nama Desain</i></p></div>:&nbsp;
                                    <div class="border border-dark col-md-5" style="height:40px;">
                                        <span style="">{{$datahead->e_motif}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 mb-0"><p class="text-left">C3. PERSETUJUAN MENYELURUH<br><i style="color:red; font-size:12px;">Persetujuan Strike Off</i></p></div>:&nbsp;
                                    <div class="border border-dark col-md-5" style="height:40px;">
                                        <span style="">{{date('d M Y', strtotime($datahead->d_approval_strike_off))}}</span>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-left" style="font-size: 20px;"><b>D. MATERIAL DESCRIPTION</b></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">
                                <div class="col-md-12"><p class="text-left">D1. Material<br><i style="color:red; font-size:12px;">Jenis Bahan</i></p></div>
                            </div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-7 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->e_jns_bahan}}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">
                                <div class="col-md-12"><p class="text-left">D2. Original Condition<br><i style="color:red; font-size:12px;">Kondisi asal</i></p></div>
                            </div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-3 p-0">
                                <div class="border border-dark col-md-11" style="height:40px;">{{$datahead->ori_cond1}}</div>
                            </div>
                            <div class="col-md-4 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->ori_cond2}}</div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">&nbsp;</div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-7 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->e_ket_ori_cond}}</div>
                            </div>                            
                        </div>
                        <div class="row mb-2">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">
                                <div class="col-md-12"><p class="text-left">D3. Jenis Print</p></div>
                            </div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-7 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->nama_printing}}</div>
                            </div>
                        </div>
                        {{--<div class="row mb-2">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">
                                <div class="col-md-12"><p class="text-left">D4. Jenis Kain</p></div>
                            </div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-7 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->e_kain}}</div>
                            </div>
                        </div>--}}
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">
                                <div class="col-md-12"><p class="text-left">D4. Color<br><i style="color:red; font-size:12px;">Warna Dasar</i></p></div>
                            </div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-7 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->e_warna_dasar}}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0">
                                <div class="col-md-12"><p class="text-left">D5. Material Supply<br><i style="color:red; font-size:12px;">Pasokan Material</i></p></div>
                            </div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-left">Receive From<br><i style="color:red; font-size:12px;">Diterima Dari</i></p></div></div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-6 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{$datahead->penyedia}}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-left">Incoming Date<br><i style="color:red; font-size:12px;">Tanggal Masuk</i></p></div></div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-2 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{date('d M Y', strtotime($datahead->d_tgl_material_in))}}</div>
                            </div>
                            <div class="col-md-1 p-0"><div class="col-md-12"><p class="text-right">Qty<br><i style="color:red; font-size:12px;">Jumlah</i></p></div></div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-2 p-0">
                                <div class="border border-dark col-md-12" style="height:40px;">{{number_format($datahead->n_qty_material,2,'.',',')}}</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-3 p-0">
                                <div class="col-md-12"><p class="text-left">D6. Nomor LPK   </p></div>
                            </div>
                            <div class="col-md-1 p-1"><p class="text-right">:</p></div>
                            <div class="col-md-7">
                                <div class="row">
                                    @foreach($param['datalpk'] as $lpk)
                                    <div class="border border-dark col-md-3 mr-2" style="height:40px;">{{$lpk->e_nomor_lpk.', '.$lpk->qty_roll.' roll'}}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-left" style="font-size: 20px;"><b>E. END PRODUCT DESCRIPTION</b></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0">
                                <div class="col-md-12"><p class="text-left">E1. Color Ways<br><i style="color:red; font-size:12px;">Jalur Warna</i></p></div>
                            </div>                            
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">      
                                            @foreach($param['dataitem'] as $key => $item_so)
                                            @php $row = $key + 1; @endphp
                                            @if($row == 1)                            
                                            <div class="col-sm-6">                                
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">CW {{$row}}:</label>
                                                    <div class="col-sm-9">
                                                        <div class="row">
                                                            <div class="border border-dark col-md-5"><p>{{$item_so->e_uraian_pekerjaan.' ('.$item_so->n_qty_kg.'kg)'}}</p></div> 
                                                            <div class="border border-dark col-md-4"><p>{{date('d-m-Y', strtotime($item_so->d_strike_off))}}</p></div>
                                                            <div class="border border-dark col-md-3"><p>{{$item_so->e_kain}}</p></div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($row % 2 == 0)                                                      
                                            <div class="col-sm-6">                                
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">CW {{$row}}:</label>
                                                    <div class="col-sm-9">                                        
                                                        <div class="row">
                                                            <div class="border border-dark col-md-5"><p>{{$item_so->e_uraian_pekerjaan.' ('.$item_so->n_qty_kg.'kg)'}}</p></div> 
                                                            <div class="border border-dark col-md-4"><p>{{date('d-m-Y', strtotime($item_so->d_strike_off))}}</p></div>
                                                            <div class="border border-dark col-md-3"><p>{{$item_so->e_kain}}</p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                            
                                            @else                                                                        
                                            <div class="col-sm-6">                                
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">CW {{$row}}:</label>
                                                    <div class="col-sm-9">
                                                        <div class="row">
                                                                <div class="border border-dark col-md-5"><p>{{$item_so->e_uraian_pekerjaan.' ('.$item_so->n_qty_kg.'kg)'}}</p></div> 
                                                            <div class="border border-dark col-md-4"><p>{{date('d-m-Y', strtotime($item_so->d_strike_off))}}</p></div>
                                                            <div class="border border-dark col-md-3"><p>{{$item_so->e_kain}}</p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-left">E2. Finishing<br><i style="color:red; font-size:12px;">Penyempurnaan</i></p></div></div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-right">Kontruksi/Gramasi</p></div></div>
                            <div class="col-md-2 p-0 mr-n5"><div class="col-md-12"><p class="text-left">From<br><i style="color:red; font-size:12px;">Dari</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5 mr-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_gramasi_from}} gr/m2</div>
                                </div>
                            </div>
                            <div class="col-md-2 ml-5 mr-n4"><div class="col-md-12"><p class="text-left">To<br><i style="color:red; font-size:12px;">Menjadi</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_gramasi_to}} gr/m2</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-right">Penyesuaian Lebar<br><i style="color:red; font-size:12px;">Width setting</i></p></div></div>
                            <div class="col-md-2 p-0 mr-n5"><div class="col-md-12"><p class="text-left">From<br><i style="color:red; font-size:12px;">Dari</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5 mr-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_penyesuaian_lebar_from}} inch/cm</div>
                                </div>
                            </div>
                            <div class="col-md-2 ml-5 mr-n4"><div class="col-md-12"><p class="text-left">To<br><i style="color:red; font-size:12px;">Menjadi</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_penyesuaian_lebar_to}} inch/cm</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-left">E3. SHRINKAGE<br><i style="color:red; font-size:12px;">Pengkerutan</i></p></div></div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-right">Pakan</p></div></div>
                            <div class="col-md-2 p-0 mr-n5"><div class="col-md-12"><p class="text-left">From<br><i style="color:red; font-size:12px;">Dari</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5 mr-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_pakan_from}} %</div>
                                </div>
                            </div>
                            <div class="col-md-2 ml-5 mr-n4"><div class="col-md-12"><p class="text-left">To<br><i style="color:red; font-size:12px;">Menjadi</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_pakan_to}} %</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-right">Lusi</p></div></div>
                            <div class="col-md-2 p-0 mr-n5"><div class="col-md-12"><p class="text-left">From<br><i style="color:red; font-size:12px;">Dari</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5 mr-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_lusi_from}} %</div>
                                </div>
                            </div>
                            <div class="col-md-2 ml-5 mr-n4"><div class="col-md-12"><p class="text-left">To<br><i style="color:red; font-size:12px;">Menjadi</i></p></div></div>
                            <div class="col-md-2 p-0 ml-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-right" style="height:40px;">{{$datahead->e_lusi_to}} %</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-1 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0">&nbsp;</div>
                            <div class="col-md-2 p-0"><div class="col-md-12"><p class="text-right">Tekstur Akhir<br><i style="color:red; font-size:12px;">Final Texture</i></p></div></div>
                            <div class="col-md-2 p-0 mr-n5"><div class="col-md-12"><p class="text-left">To<br><i style="color:red; font-size:12px;">Menjadi</i></p></div></div>
                            <div class="col-md-5 p-0 ml-n5">
                                <div class="row">
                                    :&nbsp;<div class="border border-dark col-md-11 text-left" style="height:40px;">{{$datahead->e_tekstur}}</div>
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="border border-dark col-md-6"><p class="text-left align-text-bottom"><b>Target Approve Produksi</b> : @if($datahead->d_approved_pro) {{date('d M Y h:i:s', strtotime($datahead->d_approved_pro))}} @endif</p>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <div class="border border-dark col-md-12 mb-0"><p class="text-left align-text-bottom"><b>KETERANGAN KHUSUS</b> : {{$datahead->e_ket_rfp}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endif
<script type="text/javascript">
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
@endsection