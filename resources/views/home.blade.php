@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">                
                <div class="card-body">                    
                    <center><b>SELAMAT DATANG {{strtoupper(Auth::user()->name)}} ! DI KHARISMA PRINTEX WEB :)</b></center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
