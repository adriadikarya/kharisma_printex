@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Color Ways Card Number {{$param['no_kartu'][0]->i_no_kartu}} / JO Number {{$param['no_rfp'][0]->i_no_rfp}}</h4>
            <p class="card-description">
                <button type="button" onclick="loadNewPage('{{ route($rut) }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr style="background-color:powderblue;">
                            <th style="font-weight: bolder;">Action</th>
                            <th style="font-weight: bolder;">No.</th>                        
                            <th style="font-weight: bolder;">Color Ways</th>                            
                            <th style="font-weight: bolder;">Qty Roll</th>                                                        
                            <th style="font-weight: bolder;">Qty Meter</th>                            
                            <th style="font-weight: bolder;">Qty Kg</th>
                            <th style="font-weight: bolder;">Qty Kg Residual</th>                            
                            <th style="font-weight: bolder;">Created Date</th> 
                            <th style="font-weight: bolder;">Position Status</th>                                      
                        </tr>
                    </thead>
                    <tbody>
                        @if($param['data'])
                        @php 
                            $sisa = 0; 
                            $list_cw = array();
                        @endphp
                            @foreach($param['data'] as $key => $item)
                            @php      
                                array_push($list_cw, array(
                                    'i_cw' => $item->i_id,
                                    'e_cw' => $item->e_uraian_pekerjaan,                                    
                                ));                            
                                $sisa += $item->n_qty_kg_sisa; 
                            @endphp
                                <tr>
                                    <td>
                                    {{-- @if($item->n_qty_kg_sisa>0) --}}
                                        <button type="button" class="btn btn-info btn-sm" onclick="showUpdate('{{ $item->i_id }}','{{ $item->e_uraian_pekerjaan }}','{{ $item->n_qty_kg }}','{{ $item->i_id_bagian }}')">Update WS</button>
                                    {{-- @endif --}}
                                    </td>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->e_uraian_pekerjaan}}</td>
                                    <td>{{$item->n_qty_roll==''?0:$item->n_qty_roll}}</td>
                                    <td>{{$item->n_qty_pjg==''?0:$item->n_qty_pjg}}</td>
                                    <td>{{$item->n_qty_kg}}</td>
                                    <td>{{$item->n_qty_kg_sisa}}</td>                                    
                                    <td>{{date('d-m-Y H:i:s',strtotime($item->created_at))}}</td>
                                    <td>{{$item->nama_bagian}}</td>
                                </tr>
                            @endforeach
                            @if($sisa==0)
                            <tr>
                                <td colspan="9" align="left"><button type="button" class="btn btn-success btn-sm" onclick="selesai('{{$param['no_kartu'][0]->i_id}}',{{json_encode($list_cw)}})">Selesai</button></td>
                            </tr>
                            @else
                            <tr>
                                <td colspan="9" align="center">Akan muncul tombol selesai bila semua proses sudah terpenuhi untuk update status kartu produksi, rfp dan SO</td>
                            </tr>
                            @endif
                        @else
                        <tr>
                            <td colspan="9" align="center">Maaf Data Color Ways Tidak Ada / Terjadi Kesalahan</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="_update_cw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Color Ways</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_cw">
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Color Ways</label>
                        <div class="col-sm-10">  
                            <input type="hidden" id="id_rfp" name="id_rfp" value="{{ $param['id_rfp'] }}">
                            <input type="hidden" id="id_kartu" name="id_kartu" value="{{ $param['id_kartu'] }}">                              
                            <input type="hidden" name="id_cw" id="id_cw" value=""/>
                            <input type="hidden" name="qty_kg" id="qty_kg" value=""/>
                            <input type="text" class="form-control form-control-sm border-dark" name="cw" id="cw" value="" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Bagian</label>
                        <div class="col-sm-10">
                            <select id="bagian" name="bagian" class="form-control form-control-sm border-dark" onchange="ambilNilai(this.value,$('#id_cw').val(),$('#qty_kg').val())"></select>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Qty Order / Color Way</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="qty_tot" id="qty_tot" value="" placeholder="Qty Total" value="" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Qty Proses / Bagian</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="qty_proses_bagian" id="qty_proses_bagian" value="" placeholder="Qty yg sudah di Proses Per Bagian" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Qty Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="qty_proses" id="qty_proses" autocomplete="off" value="" placeholder="Qty Proses" onkeydown="hanyaangka()"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Roll yang di Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="roll_proses" id="roll_proses" autocomplete="off" value="" placeholder="Jumlah roll yang di Proses" onkeydown="hanyaangka()"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Meter yang di Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="meter_proses" id="meter_proses" autocomplete="off" value="" placeholder="Jumlah meter yang di Proses" onkeydown="hanyaangka()" onkeyup="formatRp(this)"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Pelaksana</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="pelaksana" id="pelaksana" value="" placeholder="Pelaksana"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Shift</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="shift" id="shift" value="" placeholder="Pelaksana"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tanggal</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="d_proses" id="d_proses" value="" placeholder="Tanggal Proses" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Waktu Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="t_proses" id="t_proses" placeholder="Waktu Proses"/>
                        </div>                        
                    </div>  
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="ket" id="ket" placeholder="Keterangan" maxlength="250"/>
                        </div>                        
                    </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-primary" id="submit" onclick="update_cw()">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>
@include('workstation.action')
@endsection