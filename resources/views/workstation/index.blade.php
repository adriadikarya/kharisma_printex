@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Production Card</h4>
            <p class="card-description">
                <!-- <button type="button" onclick="loadNewPage('{{ route('form.sales_order') }}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Tambah Data
                </button> -->
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>No</th>
                            <th>SO Number</th>
                            <th>Customer</th>                                                        
                            <th>Design Number</th>
                            <th>Repeat</th>
                            <th>CW</th>
                            <th>Roll</th>
                            <th>Kg</th>
                            <th>WorkStation</th>
                            <th>Alasan Pindah</th>
                            <th>Keterangan</th>                              
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('workstation.action')
@endsection
