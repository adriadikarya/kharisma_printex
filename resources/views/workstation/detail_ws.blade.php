@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<style type="text/css" media="print">    
    .noDisplay {
        display: none;
    }

    .biru th { 
        background-color: powderblue !important;
        -webkit-print-color-adjust: exact; 
    }        
    
    @page{
        size: 21cm 29.7cm;
        margin: 0.05in 0.79in 0.10in 0.60in;
        /* margin: 0cm;         */
    }
</style>
<div class="col-lg-12 grid-margin stretch-card" id="div_print">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Detail Workstation (CW) No.SO {{$param['no_so'][0]->i_no_so}}</h4>
            <p class="card-description">
                <button type="button" onclick="loadNewPage('{{ route($rut) }}')" class="btn btn-warning btn-icon-text noDisplay">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <button type="button" onclick="printDiv('div_print')" class="btn btn-info btn-icon-text noDisplay">
                    <i class="mdi mdi-printer btn-icon-prepend"></i>
                    Print
                </button>
                <button type="button" onclick="printExcel({{ $id_rfp }},{{ $id_kartu }})" class="btn btn-success btn-icon-text noDisplay">
                    <i class="mdi mdi mdi-file-excel btn-icon-prepend"></i>
                    Print Excel
                </button>
                @if($param['datahead'])
                @php $cw = ""; $no=0; @endphp
                @foreach($param['datahead'] as $head)
                    @php $cw.= $head->e_uraian_pekerjaan.' ('.$head->n_qty_kg.') '; @endphp
                @endforeach
                @endif                
            </p>
            <b>{{$cw}}</b>
            @if($param['dataitem'])
                @php $id_cw = ""; @endphp
                @foreach($param['dataitem'] as $key => $item)                    
                    @if($id_cw!=$item->i_cw)
                    <div class="table-responsive pt-3">            
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:powderblue;" class="biru">
                                    <th style="font-weight: bolder;" class="noDisplay">Action</th>
                                    <th style="font-weight: bolder;">No.</th>
                                    <th style="font-weight: bolder;">CW</th>
                                    <th style="font-weight: bolder;">Bagian</th>
                                    <th style="font-weight: bolder;">Pelaksana</th>                            
                                    <th style="font-weight: bolder;">Shift</th>                                                        
                                    <th style="font-weight: bolder;">Roll Proses</th>
                                    <th style="font-weight: bolder;">Meter Proses</th>
                                    <th style="font-weight: bolder;">Qty Proses</th>
                                    <th style="font-weight: bolder;">Qty Sisa/Bagian</th>                            
                                    <th style="font-weight: bolder;">Tgl</th>
                                    <th style="font-weight: bolder;">Waktu</th>                                       
                                    <th style="font-weight: bolder;">Info</th>                                       
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="noDisplay">    
                                    @if(Auth::user()->role==99 || ($item->e_penanggung_jawab==Auth::user()->id || $item->e_penanggung_jawab2==Auth::user()->id))                                    
                                        <button type="button" class="btn btn-info btn-sm" onclick="detUpdate('{{ $item->i_id }}')">Edit</button>                
                                    @endif                        
                                    </td>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->e_cw}}</td>
                                    <td>{{$item->nama_bagian}}</td>
                                    <td>{{$item->e_pelaksana}}</td>
                                    <td>{{$item->e_shift}}</td>
                                    <td>{{$item->n_roll}}</td>
                                    <td>{{number_format($item->n_meter)}}</td>
                                    <td>{{$item->n_proses}}</td>
                                    <td>{{$item->n_sisa}}</td>
                                    <td>{{date('d M Y',strtotime($item->d_tgl))}}</td>
                                    <td>{{$item->d_time}}</td>
                                    <td>{{$item->e_ket}}</td>
                                </tr>                           
                    @else
                                <tr>
                                    <td class="noDisplay">  
                                    @if(Auth::user()->role==99 || ($item->e_penanggung_jawab==Auth::user()->id || $item->e_penanggung_jawab2==Auth::user()->id))                                      
                                        <button type="button" class="btn btn-info btn-sm" onclick="detUpdate('{{ $item->i_id }}')">Edit</button>                                   
                                    @endif     
                                    </td>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->e_cw}}</td>
                                    <td>{{$item->nama_bagian}}</td>
                                    <td>{{$item->e_pelaksana}}</td>
                                    <td>{{$item->e_shift}}</td>
                                    <td>{{$item->n_roll}}</td>
                                    <td>{{number_format($item->n_meter)}}</td>
                                    <td>{{$item->n_proses}}</td>
                                    <td>{{$item->n_sisa}}</td>
                                    <td>{{date('d M Y',strtotime($item->d_tgl))}}</td>
                                    <td>{{$item->d_time}}</td>
                                    <td>{{$item->e_ket}}</td>
                                </tr> 
                    @endif
                    @php $id_cw = $item->i_cw; @endphp
                @endforeach
                        </tbody>
                    </table>
                </div>
            @else
            <div class="table-responsive pt-3">            
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr style="background-color:powderblue;">
                            <th style="font-weight: bolder;" class="noDisplay">Action</th>
                            <th style="font-weight: bolder;">No.</th>
                            <th style="font-weight: bolder;">CW</th>
                            <th style="font-weight: bolder;">Bagian</th>
                            <th style="font-weight: bolder;">Pelaksana</th>                            
                            <th style="font-weight: bolder;">Shift</th>                                                        
                            <th style="font-weight: bolder;">Roll Proses</th>
                            <th style="font-weight: bolder;">Meter Proses</th>
                            <th style="font-weight: bolder;">Qty Proses</th>
                            <th style="font-weight: bolder;">Qty Sisa/Bagian</th>                            
                            <th style="font-weight: bolder;">Tgl</th>
                            <th style="font-weight: bolder;">Waktu</th>                                       
                            <th style="font-weight: bolder;">Info</th>                                     
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="13" style="text-align:center;">Maaf Admin Belum Update Data Color Ways / Terjadi Kesalahan</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>
<div class="modal fade" id="_update_cw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Color Ways</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_cw">
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Color Ways</label>
                        <div class="col-sm-10">                              
                            <input type="hidden" name="id_det_ws" id="id_det_ws" value=""/>
                            <input type="hidden" name="id_cw" id="id_cw" value=""/>
                            <input type="hidden" name="qty_kg" id="qty_kg" value=""/>
                            <input type="text" class="form-control form-control-sm border-dark" name="cw" id="cw" value="" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Bagian</label>
                        <div class="col-sm-10">
                            <select id="bagian" name="bagian" class="form-control form-control-sm border-dark" onchange="ambilNilai(this.value,$('#id_cw').val(),$('#qty_kg').val())"></select>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Qty Order / Color Way</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="qty_tot" id="qty_tot" value="" placeholder="Qty Total" value="" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Qty Proses / Bagian</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="qty_proses_bagian" id="qty_proses_bagian" value="" placeholder="Qty yg sudah di Proses Per Bagian" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Qty Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="qty_proses" id="qty_proses" autocomplete="off" value="" placeholder="Qty Proses" onkeydown="hanyaangka()"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Roll yang di Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="roll_proses" id="roll_proses" autocomplete="off" value="" placeholder="Jumlah roll yang di Proses" onkeydown="hanyaangka()"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Meter yang di Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="meter_proses" id="meter_proses" autocomplete="off" value="" placeholder="Jumlah meter yang di Proses" onkeydown="hanyaangka()" onkeyup="formatRp(this)"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Pelaksana</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="pelaksana" id="pelaksana" value="" placeholder="Pelaksana"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Shift</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="shift" id="shift" value="" placeholder="Pelaksana"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tanggal</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="d_proses" id="d_proses" value="" placeholder="Tanggal Proses" readonly/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Waktu Proses</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="t_proses" id="t_proses" placeholder="Waktu Proses"/>
                        </div>                        
                    </div>  
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Keterangan</label>
                        <div class="col-sm-10">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="ket" id="ket" placeholder="Keterangan" maxlength="250"/>
                        </div>                        
                    </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-primary" id="submit" onclick="update_det()">Update</button>
            </div>
            </form>
        </div>
    </div>
</div>
<form class="form" id="form-export" method="POST" class="d-none">
    @csrf
    <input type="hidden" name="id_rfp_excel" id="id_rfp_excel">
    <input type="hidden" name="id_kartu_excel" id="id_kartu_excel">
    <button type="submit" value="excel" id="excel" name="excel" class="btn btn-success btn-icon-text d-none" ><i class="mdi mdi-download btn-icon-prepend"></i>Export</button>
</form>
@include('workstation.action')
@endsection