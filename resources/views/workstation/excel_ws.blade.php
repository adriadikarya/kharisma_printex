<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:powderblue !important;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>
<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
  .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:powderblue !important;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
@if($module['datahead'])
@php $cw = ""; $no=0; @endphp
@foreach($module['datahead'] as $head)
    @php $cw.= $head->e_uraian_pekerjaan.' ('.$head->n_qty_kg.') '; @endphp
@endforeach
@endif
<table class="tg" border="1">
    <tr>    
        <td colspan="5"> List Detail Workstation (CW) No.SO {{$module['no_so'][0]->i_no_so}} </td>
    </tr>
    <tr>    
        <td colspan="5"> {{ $cw }} </td>
    </tr>
</table>
<table class="tg" border="1">
@if($module['dataitem'])
    @php $id_cw = ""; @endphp
    @foreach($module['dataitem'] as $key => $item)                    
        @if($id_cw!=$item->i_cw)                            
            <tr>                        
                <th style="font-weight: bolder;">No.</th>
                <th style="font-weight: bolder;">Color Ways</th>
                <th style="font-weight: bolder;">Section</th>
                <th style="font-weight: bolder;">Managing</th>                            
                <th style="font-weight: bolder;">Shift</th>
                <th style="font-weight: bolder;">Qty Process</th>
                <th style="font-weight: bolder;">Qty Residual / Section</th>                            
                <th style="font-weight: bolder;">Date</th>
                <th style="font-weight: bolder;">Process Time</th>                                       
                <th style="font-weight: bolder;">Information</th>
            </tr>                                
            <tr>                        
                <td>{{$key+1}}</td>
                <td>{{$item->e_cw}}</td>
                <td>{{$item->nama_bagian}}</td>
                <td>{{$item->e_pelaksana}}</td>
                <td>{{$item->e_shift}}</td>
                <td>{{$item->n_proses}}</td>
                <td>{{$item->n_sisa}}</td>
                <td>{{date('d M Y',strtotime($item->d_tgl))}}</td>
                <td>{{$item->d_time}}</td>
                <td>{{$item->e_ket}}</td>
            </tr>
        @else
            <tr>                        
                <td>{{$key+1}}</td>
                <td>{{$item->e_cw}}</td>
                <td>{{$item->nama_bagian}}</td>
                <td>{{$item->e_pelaksana}}</td>
                <td>{{$item->e_shift}}</td>
                <td>{{$item->n_proses}}</td>
                <td>{{$item->n_sisa}}</td>
                <td>{{date('d M Y',strtotime($item->d_tgl))}}</td>
                <td>{{$item->d_time}}</td>
                <td>{{$item->e_ket}}</td>
            </tr>  
        @endif
        @php $id_cw = $item->i_cw; @endphp
    @endforeach                        
</table>                
@endif