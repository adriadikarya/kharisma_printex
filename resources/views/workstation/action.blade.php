<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 
$('#d_proses').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "top left",
    autoclose: true,
    todayHighlight: true
});

$(function () {
    $('#t_proses').datetimepicker({
        format: 'HH:mm'
    });
});

$.validator.addMethod("time24", function(value, element) {
    if (!/^\d{2}:\d{2}$/.test(value)) return false;
    var parts = value.split(':');
    if (parts[0] > 23 || parts[1] > 59) return false;
    return true;
}, "Invalid time format.");

var act_url = '{{ route('data.workstation') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    lengthMenu: [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ], 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', orderable: false},
        { data: 'urutan', name: 'urutan', orderable: false},
        { data: 'i_no_so', name: 'i_no_so', orderable: false},
        { data: 'e_nama_pel', name: 'e_nama_pel', orderable: false},
        { data: 'i_desain', name: 'i_desain', orderable: false},
        { data: 'f_repeat', name: 'f_repeat', orderable: false},        
        { data: 'hitungcw', name: 'hitungcw', orderable: false},
        { data: 'roll', name: 'roll', orderable: false},        
        { data: 'kg', name: 'kg', orderable: false},        
        { data: 'nama_bagian', name: 'nama_bagian', orderable: false},
        { data: 'alasan_pindah_slot', name: 'alasan_pindah_slot', orderable: false},
        { data: 'keterangan_kp', name: 'keterangan_kp', orderable: false},        
    ],
    dom: 'lBfrtip',
    buttons: [ {
        extend: 'print',
        exportOptions: {
            columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
        },
    } ],
});

var _update_modal = $('#_update_cw');

function lihatRfp(id,link)
{
    loadNewPage(base_url + '/rfp/lihat/' +id +'/'+ link);
}

function updateWorkStation(id_kartu, id_rfp, id_so, link)
{
    loadNewPage(base_url + '/workstation/list_cw/' +id_kartu +'/'+ id_rfp +'/'+ id_so +'/'+ link);
}

function detailWorkStation(id_rfp,id_so,link)
{
    loadNewPage(base_url + '/workstation/detail/' +id_rfp+'/'+id_so+'/'+link);
}

function showUpdate(id_cw, cw_name, qty_kg, id_bagian)
{
    $('#id_cw').val(id_cw);
    $('#cw').val(cw_name);
    $('#qty_kg').val(qty_kg);
    $('#qty_tot').val(qty_kg);
    console.log(id_bagian)
    $.ajax({
        type: "GET",
        url: base_url + '/workstation/bagian/' + id_cw + '/' +id_bagian,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);        
        var html_bag = '<option value="">Pilih Bagian!</option>';
        $.each(data, function(k,v){
            console.log(v)
            // if(v.i_id==id_bagian){
            //     html_bag+= '<option value="'+v.i_id+'">'+v.nama_bagian+'</option>';
            // } else if(v.i_id==parseInt(id_bagian)+1) {
            //     html_bag+= '<option value="'+v.i_id+'">'+v.nama_bagian+'</option>';
            // } else {
            //     html_bag+= '<option value="'+v.i_id+'" disabled>'+v.nama_bagian+'</option>';
            // }
            html_bag+= '<option value="'+v.i_id+'">'+v.nama_bagian+'</option>';
        });            
        $('#bagian').html(html_bag);
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });

    _update_modal.modal({backdrop: 'static', keyboard: false});      
    // _update_modal.modal('show');
}

function ambilNilai(bagian,id_cw,tot_qty)
{
    if(bagian!=''){
        $.ajax({
            type: "GET",
            url: base_url + '/workstation/ambilSisa/' + id_cw + '/' + bagian,
            beforeSend: function() {
                $('#loading').show();
                $('#submit').attr('disabled','disabled');
            },
            success: function(res) {

            },
        }).done(function(res){
            $('#loading').hide();            
            $('#submit').attr('disabled',false);
            var data = $.parseJSON(res);   
            var qty = data.tot_qty_proses==null?0:data.tot_qty_proses;
            $('#qty_proses_bagian').val(qty);             
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
    }
}

jQuery.validator.addMethod("lebihDariSisa", function(value, element) {        
    var total_qty = $('#qty_kg').val();
    var total_proses = $('#qty_proses_bagian').val();
    var tot = parseFloat(total_qty) - parseFloat(total_proses);
    console.log(parseFloat(tot.toFixed(2)))
    return this.optional(element) || (parseFloat(value) <= tot.toFixed(2));
}, "Qty Proses Tidak Boleh Lebih dari Qty Total Order / Sudah Harus pindah bagian");

function update_cw()
{
    $('#form_cw').validate({
        rules: {            
            bagian: {
                required: !0                
            },
            qty_proses: {
                required: !0,                
                lebihDariSisa : !0
            },
            pelaksana: {
                required: !0,
                maxlength: 75                
            },
            shift: {
                required: !0,
                maxlength: 50              
            },
            d_proses: {
                required: !0
            },
            t_proses: {
                required: !0,
                time24: !0
            },
            roll_proses: {
                required: !0
            },
            meter_proses: {
                required: !0
            }
        },
        messages: {
            bagian: {
                required: "Tolong pilih dulu bagian nya ya :)"
            },
            qty_proses: {
                required: "Tolong isi dulu qty proses nya ya :)",
                digits: "Hanya angka ya :)"
            },
            pelaksana: {
                required: "Tolong diisi pelaksana nya ya :)",
                maxlength: "Handfeel tidak boleh lebih dari 75 karakter :)"
            },
            shift: {
                required: "Tolong diisi shift nya ya :)",
                maxlength: "Shift maximal 50 karakter :)"
            },
            d_proses: {
                required: "Tolong diisi tanggal nya ya :)",
            },
            t_proses: {
                required: "Tolong diisi waktu proses nya ya :)",
                time24: "Format Time Nya Salah"
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_kartu = $('#form_cw');
            var _form_data = new FormData(form_kartu[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/workstation/update_cw',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    // window.location.href = base_url + '/workstation'
                                    location.reload();
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Update Color Ways Tidak Jadi :)", "error");   
                } 
            });
        }
    });
}

function selesai(id_kartu, list_cw)
{        
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Update Selesai Orderan Ini?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/workstation/selesai/'+ id_kartu,
                data: { arr_cw: list_cw },
                dataType: 'json', 
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();                                                        
                var data = $.parseJSON(JSON.stringify(res));  
                if(data.code==1){
                    swal({   
                        title: "Sukses",   
                        text: data.msg,   
                        type: "success",                       
                        showCancelButton: false,   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Ya",                     
                        closeOnConfirm: true,                                       
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){   
                        if(isConfirm){
                            loadNewPage('{{ route('workstation.index') }}');
                        }
                    });
                } else {
                    swal({
                        title: 'Informasi',
                        text: data.msg,
                        type: 'info',
                    });    
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi diupdate selesai','info');
        }
    }); 
}

function detUpdate(id)
{
    $('#id_det_ws').val(id);
    $.ajax({
        type: "GET",
        url: base_url + '/workstation/get_det_ws/'+ id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {
            
        },
    }).done(function(res){
        $('#loading').hide();
        console.log(res)
        $('#id_cw').val(res.data_det[0].i_cw);
        $('#qty_kg').val(res.data_det[0].n_qty_kg);
        $('#cw').val(res.data_det[0].e_cw);
        var html_bag = '<option value="">Pilih Bagian!</option>';
        $.each(res.bag, function(k,v){
            var selected = v.i_id==res.data_det[0].i_id_bagian?'selected':'';
            html_bag+= '<option value="'+v.i_id+'" '+selected+'>'+v.nama_bagian+'</option>';
        });            
        $('#bagian').html(html_bag);        
        let qty_pb = parseFloat(res.data_det[0].n_qty_kg) - parseFloat(res.data_det[0].n_sisa) - parseFloat(res.data_det[0].n_proses);
        if(parseFloat(qty_pb)<0) qty_pb = parseFloat(qty_pb) * -1;        
        $('#qty_tot').val(res.data_det[0].n_qty_kg);        
        $('#qty_proses_bagian').val(qty_pb.toFixed(2));
        $('#qty_proses').val(res.data_det[0].n_proses);
        $('#pelaksana').val(res.data_det[0].e_pelaksana);
        $('#shift').val(res.data_det[0].e_shift);
        $('#d_proses').val(res.data_det[0].d_tgl);
        $('#t_proses').val(res.data_det[0].d_time);
        $('#ket').val(res.data_det[0].e_ket);
        $('#meter_proses').val(addCommas(res.data_det[0].n_meter));
        $('#roll_proses').val(res.data_det[0].n_roll);
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _update_modal.modal({backdrop: 'static', keyboard: false});
}

function update_det()
{
    $('#form_cw').validate({
        rules: {            
            bagian: {
                required: !0                
            },
            qty_proses: {
                required: !0,                
                lebihDariSisa : !0
            },
            pelaksana: {
                required: !0,
                maxlength: 75                
            },
            shift: {
                required: !0,
                maxlength: 50              
            },
            d_proses: {
                required: !0
            },
            t_proses: {
                required: !0,
                time24: !0
            },
            roll_proses: {
                required: !0
            },
            meter_proses: {
                required: !0
            }
        },
        messages: {
            bagian: {
                required: "Tolong pilih dulu bagian nya ya :)"
            },
            qty_proses: {
                required: "Tolong isi dulu qty proses nya ya :)",
                digits: "Hanya angka ya :)"
            },
            pelaksana: {
                required: "Tolong diisi pelaksana nya ya :)",
                maxlength: "Handfeel tidak boleh lebih dari 75 karakter :)"
            },
            shift: {
                required: "Tolong diisi shift nya ya :)",
                maxlength: "Shift maximal 50 karakter :)"
            },
            d_proses: {
                required: "Tolong diisi tanggal nya ya :)",
            },
            t_proses: {
                required: "Tolong diisi waktu proses nya ya :)",
                time24: "Format Time Nya Salah"
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_kartu = $('#form_cw');
            var _form_data = new FormData(form_kartu[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/workstation/update_det_ws',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    location.reload();
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Update Detail Workstation Batal :)", "error");   
                } 
            });
        }
    });
}

function printDiv(divName){    
    // var printContents = document.getElementById(divName).innerHTML;
    // var strHtml = "<html>\n<head>\n <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" type=\"text/css\"><body>\n"
	// 	+ printContents + "\n</body>\n</html>";
    // var originalContents = document.body.innerHTML;

    // document.body.innerHTML = printContents;
    // document.body.innerHTML = strHtml;

    // window.print();

    // document.body.innerHTML = originalContents;

    var contents = document.getElementById(divName).innerHTML;
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head><title>DIV Contents</title>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.    
    frameDoc.document.write('<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<style type="text/css" media="print"> .noDisplay {display: none;} .biru th {        background-color: powderblue !important;-webkit-print-color-adjust: exact;} @page{ size: 22cm 31cm; /* margin: 0cm; */} </style>');
    //Append the DIV contents.
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);
}

function printExcel(id_rfp,id_kartu) {
    // window.open(base_url + '/workstation/excel_det_ws?id_rfp=' + id_rfp + '&id_kartu=' + id_kartu, "_blank");
    $('#id_rfp_excel').val(id_rfp);
    $('#id_kartu_excel').val(id_kartu);
    $('#form-export').attr('action', base_url + '/workstation/excel_det_ws');
    $('#form-export').submit();
}
</script>