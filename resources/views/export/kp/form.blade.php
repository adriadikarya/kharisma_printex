@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Export Kartu Produksi</h4>
            <form class="form" id="form-export" method="GET">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Awal</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-sm tgl_input border-dark" name="d_awal" id="d_awal" readonly required/>
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-md-6 grid-margin">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tanggal Akhir</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control form-control-sm tgl_input border-dark" name="d_akhir" id="d_akhir" readonly required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" value="excel" id="excel" name="excel" class="btn btn-success btn-icon-text" onclick="validasi()"><i class="mdi mdi-download btn-icon-prepend"></i>Export</button>
                                <button type="reset" class="btn btn-warning btn-icon-text"><i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});

function validasi()
{
    $('#form-export').validate({
        rules: {
            d_awal: {
                required: !0
            },
            d_akhir: {
                required: !0
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            e.submit();
        }
    });
}
</script>
@endsection