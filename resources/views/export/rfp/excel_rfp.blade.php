<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>
<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
  .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
<table class="tg" border="1">
  <thead>
    <tr>
      <th>Nomor JO</th>
      <th>Nomor SO</th>
      <th>Tgl Buat JO</th>                 
      <th>Target Selesai</th>
      <th>Approved Mng Marketing</th>      
      <th>Approved Mng Produksi</th>      
      <th>Pelanggan</th>      
      <th>Repeat Order</th>
      <th>Desain</th>
      <th>Nama Desain</th>
      <th>Approval Strike Off</th>
      <th>Jenis Bahan</th>
      <th>Kondisi Awal</th>      
      <th>Warna Dasar</th>      
      <th>Kain Diterima Dari</th>
      <th>Tgl Masuk Barang</th>      
      <th>Qty Total</th>
      <th>CW1</th>
      <th>CW2</th>
      <th>CW3</th>
      <th>CW4</th>
      <th>CW5</th>
      <th>CW6</th>
      <th>CW7</th>
      <th>CW8</th>
      <th>Gramasi From</th>
      <th>Gramasi To</th>
      <th>Penyesuaian Lebar From</th>
      <th>Penyesuaian Lebar To</th>
      <th>Pakan From</th>
      <th>Pakan To</th>
      <th>Lusi From</th>
      <th>Lusi To</th>
      <th>Tekstur Akhir</th>
      <th>Keterangan JO</th>
    </tr>
  </thead>
  <tbody>
    @if($module)
    @foreach($module as $item)
    <tr>
      <td>{{$item->i_no_rfp}}</td>
      <td>{{$item->i_no_so}}</td>
      <td>{{date('d M Y', strtotime($item->created_at))}}</td>
      <td>{{$item->d_selesai}}</td>
      <td>{{$item->d_approved_mrk}}</td>
      <td>{{$item->d_approved_pro}}</td>
      <td>{{$item->e_nama_pel}}</td>
      <td>{{$item->f_repeat}}</td>
      <td>{{$item->i_desain}}</td>
      <td>{{$item->e_motif}}</td>
      <td>{{$item->d_approval_strike_off}}</td>
      <td>{{$item->e_jns_bahan}}</td>
      <td>{{$item->ori_kond1.' '.$item->ori_kond2.' '.$item->e_ket_ori_cond}}</td>
      <td>{{$item->e_warna_dasar}}</td>
      <td>{{$item->penyedia}}</td>      
      <td>{{$item->d_tgl_material_in}}</td>      
      <td>{{$item->n_qty_material}}</td>
      <td>{{$item->e_cw_1}}</td>
      <td>{{$item->e_cw_2}}</td>
      <td>{{$item->e_cw_3}}</td>
      <td>{{$item->e_cw_4}}</td>
      <td>{{$item->e_cw_5}}</td>
      <td>{{$item->e_cw_6}}</td>
      <td>{{$item->e_cw_7}}</td>
      <td>{{$item->e_cw_8}}</td>
      <td>{{$item->e_gramasi_from}}</td>
      <td>{{$item->e_gramasi_to}}</td>
      <td>{{$item->e_penyesuaian_lebar_from}}</td>
      <td>{{$item->e_penyesuaian_lebar_to}}</td>
      <td>{{$item->e_pakan_from}}</td>
      <td>{{$item->e_pakan_to}}</td>
      <td>{{$item->e_lusi_from}}</td>
      <td>{{$item->e_lusi_to}}</td>
      <td>{{$item->e_tekstur}}</td>
      <td>{{$item->e_ket_rfp}}</td>
    </tr>
    @endforeach
    @endif
  </tbody>
</table>