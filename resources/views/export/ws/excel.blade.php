<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>
<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
  .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
<table class="tg" border="1">
  <thead>
    <tr>      
      <th>Nomor SO</th>
      <th>Nomor JO</th>            
      <th>Nama Pelanggan</th>            
      <th>Kode Design</th>            
      <th>Nama Design</th>            
      <th>Color Ways</th>                  
      <th>Workstation</th>
      <th>Pelaksana</th>
      <th>Jadwal Shift</th>
      <th>Tanggal</th>
      <th>Jam</th>
      <th>Qty Roll Proses</th>
      <th>Qty Meter Proses</th>
      <th>Qty Proses</th>
      <th>Qty Sisa</th>
      <th>Keterangan</th>
      <th>Dibuat User Tanggal</th>
      <th>Diupdate User Tanggal</th>      
    </tr>
  </thead>
  <tbody>
    @if($module)
    @foreach($module as $item)
    <tr>
      <td>{{$item->i_no_so}}</td>
      <td>{{$item->i_no_rfp}}</td>
      <td>{{$item->e_nama_pel}}</td>
      <td>{{$item->i_desain}}</td>
      <td>{{$item->e_motif}}</td>
      <td>{{$item->e_cw}}</td>
      <td>{{$item->nama_bagian}}</td>
      <td>{{$item->e_pelaksana}}</td>
      <td>{{$item->e_shift}}</td>
      <td>{{date('d F Y', strtotime($item->d_tgl))}}</td>      
      <td>{{$item->d_time}}</td>
      <td>{{$item->n_roll}}</td>      
      <td>{{$item->n_meter}}</td>                  
      <td>{{$item->n_proses}}</td>                  
      <td>{{$item->n_sisa}}</td>                  
      <td>{{$item->e_ket}}</td>                  
      <td>{{$item->created_at}}</td>                  
      <td>{{$item->updated_at}}</td>                  
    </tr>
    @endforeach
    @endif
  </tbody>
</table>