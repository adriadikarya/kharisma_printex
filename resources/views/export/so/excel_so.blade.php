<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>
<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
  .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
<table class="tg" border="1">
  <thead>
    <tr>
      <th>Marketing</th>
      <th>Nomor SO</th>      
      <th>Tgl SO</th>      
      <th>Tgl Buat</th>      
      <th>Tgl Approved Manager</th>      
      <th>Pelanggan</th>
      <th>Nomor PO Pelanggan</th>
      <th>Repeat Order</th>
      <th>Desain</th>
      <th>Nama Desain</th>
      <th>Jenis Printing</th>
      <th>Color Way</th>
      <th>Qty (Roll)</th>
      <th>Qty (Meter)</th>
      <th>Qty (Kg)</th>
      <th>Toleransi Cacat Kain</th>      
      <th>Kondisi Kain</th>      
      <th>Penyedia Kain</th>
      <th>Strike Off</th>
      <th>Approval Strike Off</th>
      <th>Tgl Penyerahan Brg</th>
      <th>Total Harga</th>
      <th>PPN</th>
      <th>Harga Pekerjaan</th>
      <th>Down Payment</th>
      <th>Yang Harus Di Bayar</th>
      <th>Keterangan Kirim</th>
    </tr>
  </thead>
  <tbody>
    @if($module)
    @foreach($module as $item)
    <tr>
      <td>{{$item->flag_name}}</td>
      <td>{{$item->i_no_so}}</td>
      <td>{{date('d M Y', strtotime($item->d_so))}}</td>
      <td>{{date('d M Y H:i:s', strtotime($item->created_at))}}</td>
      <td>{{date('d M Y H:i:s', strtotime($item->d_approved))}}</td>
      <td>{{$item->e_nama_pel}}</td>
      <td>{{$item->i_no_po}}</td>
      <td>{{$item->f_repeat}}</td>
      <td>{{$item->i_desain}}</td>
      <td>{{$item->e_motif}}</td>
      <td>{{$item->e_jns_printing}}</td>
      <td>{{$item->n_color_way.' '.$item->e_color_way}}</td>
      <td>{{number_format($item->qty_roll)}}</td>      
      <td>{{number_format($item->qty_pjg)}}</td>      
      <td>{{number_format($item->qty_kg)}}</td>                  
      <td>{{$item->e_toleransi_cacat}}</td>      
      <td>{{$item->e_kondisi_kain}}</td>
      <td>{{$item->penyedia}}</td>
      <td>{{$item->d_strike_off}}</td>
      <td>{{$item->d_approval_strike_off}}</td>
      <td>{{$item->d_penyerahan_brg}}</td>
      <td>{{$item->v_pekerjaan}}</td>
      @if($item->f_pkp)
      @php $ppn = $item->v_pekerjaan*0.1; @endphp
      <td>{{$ppn}}</td>
      @else
      <td>0</td>
      @endif
      <td>{{$item->v_pekerjaan_plus_ppn}}</td>
      <td>{{$item->v_discount}}</td>
      <td>{{$item->v_sisa}}</td>
      <td>{{$item->e_keterangan_kirim}}</td>
    </tr>
    @endforeach
    @endif
  </tbody>
</table>