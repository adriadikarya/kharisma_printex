<script>
var act_url = '{{ route('data.sales_order') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_so', name: 'i_no_so' },
        { data: 'd_so', name: 'd_so' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },
        // { data: 'f_repeat', name: 'f_repeat' },
        // { data: 'd_strike_off', name: 'd_strike_off' },
        // { data: 'd_approval_strike_off', name: 'd_approval_strike_off' },
        // { data: 'd_penyerahan_brg', name: 'd_penyerahan_brg' },
        { data: 'd_approved', name: 'd_approved' },
        { data: 'created_at', name: 'created_at' },
        { data: 'definition', name: 'definition' },
        { data: 'e_reject', name: 'e_reject' },
    ]
});

function downloadSO(id)
{
    window.open(base_url + '/sales_order/download/' +id);
}

function kirimApprove(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Kirim SO ke Manager?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/sales_order/kirim_approve/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('sales_order.index') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi dikirim ke manager','info');
        }
    });
}


function kirimRfp(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Kirim Data SO ke RFP?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/sales_order/rfp/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('sales_order.index') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi dikirim ke manager','info');
        }
    });
}

function edit(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Edit Data?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: true,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) { 
            $('#loading').show();
            loadNewPage(base_url + '/sales_order/edit/' +id);
        } else {
            swal('Batal', 'Tidak Jadi Edit Data!', 'error');
        }
    });
}

function lihat(id,link)
{
    loadNewPage(base_url + '/sales_order/lihat/' +id +'/'+ link);
}

function printSO(id,link)
{
    loadNewPage(base_url + '/sales_order/print/' +id +'/'+ link);   
}


</script>