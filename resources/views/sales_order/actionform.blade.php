<script type="text/javascript">

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 

$('#pel').select2().css('width','100%');

$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});

function getAlamat(i_pel)
{
    var act_url = '{{ route('alamat.sales_order', ':id') }}';
    act_url = act_url.replace(':id', i_pel);
    if(i_pel!=''){
        $.ajax({
            type: "GET",
            url: act_url,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(res) {
            
            },
        }).done(function(res){
            $('#loading').hide();
            var data = $.parseJSON(res);            
            $('#alamat').val(data.alamat);
            var penyedia_kain = '<option value="0">PT. Kharisma Printex</option>'; 
            penyedia_kain += '<option value="'+i_pel+'">'+data.nama_pel+'</option>';
            $('#penyedia_kain').append(penyedia_kain);
            $('#pkp').val(data.pkp);
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
    } else {
        $('#alamat').val('');
    }
}

function tipeMotif(tipe)
{
    if(tipe=='ro'){
        // $('#motif_desain').html('');
        var i_pel = $('#pel').val();
        var html_motif = '';
        if(i_pel==''){
            swal('Peringatan','Pilih Pelanggan Dulu :)','warning');  
            $('#repeat_order').val('new');
        } else {
            var act_url = '{{ route('repeat.sales_order', ':id') }}';
            act_url = act_url.replace(':id', i_pel);            
            $('#desain_kode1').hide();
            $('#desain_kode1').val('');
            $.ajax({
                type: "GET",
                url: act_url,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                html_motif+='<option value="">Pilih Kode Desain!</option>';
                $.each(data, function(k,v){
                    html_motif+='<option value="'+v.i_desain+'">'+v.i_desain+' '+v.e_motif+'</option>';
                });
                $('#desain_kode2').append(html_motif);
                $('#desain_kode2').next(".select2-container").show();
                $('#desain_kode2').select2();
                $('#desain_kode2').show();
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                alert(errorThrown);
            });
        }
    } else {
        $('#desain_kode1').show();
        $('#desain_kode2').html('');
        $('#desain_kode2').hide();
        $('#desain_kode2').next(".select2-container").hide();

        $('#jml_warna').val(1);
        $('#motif_nama').val('');
        $('#jns_printing').val('Pigment');
        $('#color_way').val(1);
        $('#color_way_desc').val('');
        $('#toleransi_cacat_kain').val('');

        $('#jenis_kain').val('');
        $('#kondisi_kain').val('Greige');
        $('#lebar').val('');
        $('#gramasi').val('');
        $('#penyedia_kain').val(0);
    }
}

function getKain(desain)
{
    if(desain!=''){
        var kode_desain = desain.replaceAll('/','%252F');
        console.log(kode_desain)
        var i_pel = $('#pel').val();
        var act_url = '{{ route('get_kain.sales_order', ['id'=>':id', 'desain'=>':desain']) }}';
        act_url = act_url.replace(':id', i_pel);            
        act_url = act_url.replace(':desain', kode_desain);            
        $.ajax({
            type: "GET",
            url: act_url,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(res) {
                
            },
        }).done(function(res){
            $('#loading').hide();            
            var data = $.parseJSON(res);
            $.each(data.tx_pek, function(k,v){
                $('#jml_warna').val(v.i_qty_warna);
                $('#motif_nama').val(v.e_motif);
                $('#jns_printing').val(v.e_jenis_printing);
                $('#color_way').val(v.n_colow_way);
                $('#color_way_desc').val(v.e_color_way);
                $('#toleransi_cacat_kain').val(v.e_toleransi_cacat);
            });

            $.each(data.tx_kain, function(k,v){
                // $('#jenis_kain').val(v.i_jns_kain);
                $('#kondisi_kain').val(v.e_kondisi_kain);
                $('#lebar').val(v.e_lebar);
                $('#gramasi').val(v.e_gramasi);
                $('#penyedia_kain').val(v.i_penyedia);
            });
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
    } else {
        $('#jml_warna').val(1);
        $('#motif_nama').val('');
        $('#jns_printing').val('Pigment');
        $('#color_way').val(1);
        $('#color_way_desc').val('');
        $('#toleransi_cacat_kain').val('');

        $('#jenis_kain').val('');
        $('#kondisi_kain').val('Greige');
        $('#lebar').val('');
        $('#gramasi').val('');
        $('#penyedia_kain').val(0);
    }
}

function getDate(strikeoff){
    $(strikeoff).datepicker({
        format: "dd-mm-yyyy",
        todayBtn: "linked",
        language: "id",
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true
    });
}

function addRow()
{
    if($('#pel').val()==''){
        swal('Peringatan','Pilih Pelanggan Dulu :)','warning');      
    } else {                
        var hitung_by = $('#hitung_by').attr('disabled','disabled');
        var hitung_by = $('#hitung_by').val();
        var counter = parseInt($('#totrow').val())+1;
        var cols = "";
        cols += '<tr id="tr'+counter+'"><form id="tes'+counter+'">';
        cols += '<td align="center"><input type="hidden" class="form-control form-control-sm" name="baris' + counter + '" id="baris' + counter + '" value="'+counter+'"/>'+counter+'</td>';    
        cols += '<td><div class="row row-error"><textarea type="text" class="form-control form-control-sm border-dark col-sm-6 row-error" rows="1" name="uraian_pek_'+counter+'" id="uraian_pek_'+counter+'" maxlength="100"/></textarea><input type="text" name="strike_off_'+counter+'" id="strike_off_'+counter+'" class="form-control form-control-sm border-dark col-sm-3 row-error" onclick="getDate(this);" readonly/><select class="form-control form-control-sm border-dark col-sm-3 row-error" name="i_jns_kain_'+counter+'" id="i_jns_kain_'+counter+'"> @if($jenis_kain) <option value="">Pilih Kain!</option> @foreach($jenis_kain as $kain) <option value="{{ $kain->i_id }}">{{ $kain->e_kain }}</option> @endforeach @else <option value="0">Tidak Ada Kain</option> @endif</select><div id="error_uraian_pek_'+counter+'" class="error"></div></div></td>';

        if(hitung_by=='1'){
            // roll
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="roll_'+counter+'" id="roll_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'roll_'+counter+'\'));" onkeydown="hanyaangka();" /></td>';
            // yard/m
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="yard_m_'+counter+'" id="yard_m_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'yard_m_'+counter+'\'));" onkeydown="hanyaangka();" /></div></td>';
            // kg
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="kg_'+counter+'" id="kg_'+counter+'" style="text-align: right;" onkeyup="hitungSatu(\'#kg_'+counter+'\',\'#hrg_'+counter+'\','+counter+');formatRp(document.getElementById(\'kg_'+counter+'\'));" onkeydown="hanyaangka();" /><div id="error_kg_'+counter+'" class="error"></div></td>'; 
            // jml_harga 
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="hrg_'+counter+'" id="hrg_'+counter+'" style="text-align: right;" onkeyup="hitungSatu(\'#kg_'+counter+'\',this,'+counter+');formatRp(document.getElementById(\'hrg_'+counter+'\'));" onkeydown="hanyaangka();" /><div id="error_hrg_'+counter+'" class="error"></div></td>'; 
        } else if(hitung_by=='2') {
            // roll
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="roll_'+counter+'" id="roll_'+counter+'" style="text-align: right;" onkeyup="hitungSatu(\'#roll_'+counter+'\',\'#hrg_'+counter+'\','+counter+');formatRp(document.getElementById(\'roll_'+counter+'\'));" onkeydown="hanyaangka();" /></td>';
            // yard/m
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="yard_m_'+counter+'" id="yard_m_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'yard_m_'+counter+'\'));" onkeydown="hanyaangka();" /></div></td>';
            // kg
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="kg_'+counter+'" id="kg_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'kg_'+counter+'\'));" onkeydown="hanyaangka();" /><div id="error_kg_'+counter+'" class="error"></div></td>'; 
            // jml_harga  
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="hrg_'+counter+'" id="hrg_'+counter+'" style="text-align: right;" onkeyup="hitungSatu(\'#roll_'+counter+'\',this,'+counter+');formatRp(document.getElementById(\'hrg_'+counter+'\'));" onkeydown="hanyaangka();" /><div id="error_hrg_'+counter+'" class="error"></div></td>';
        } else {
            // roll
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="roll_'+counter+'" id="roll_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'roll_'+counter+'\'));" onkeydown="hanyaangka();" /></td>';
            // yard/m
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="yard_m_'+counter+'" id="yard_m_'+counter+'" style="text-align: right;" onkeyup="hitungSatu(\'#yard_m_'+counter+'\',\'#hrg_'+counter+'\','+counter+');formatRp(document.getElementById(\'yard_m_'+counter+'\'));" onkeydown="hanyaangka();" /></div></td>';
            // kg
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="kg_'+counter+'" id="kg_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'kg_'+counter+'\'));" onkeydown="hanyaangka();" /><div id="error_kg_'+counter+'" class="error"></div></td>'; 
            // jml_harga 
            cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="hrg_'+counter+'" id="hrg_'+counter+'" style="text-align: right;" onkeyup="hitungSatu(\'#yard_m_'+counter+'\',this,'+counter+');formatRp(document.getElementById(\'hrg_'+counter+'\'));" onkeydown="hanyaangka();" /><div id="error_hrg_'+counter+'" class="error"></div></td>'; 
        }
        cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="jml_hrg_'+counter+'" id="jml_hrg_'+counter+'" style="text-align: right;" readonly /></td>';
        cols += '</form></tr>';
        $("#detailbody").append(cols);
        $('#totrow').val(counter);
    }
}

function deleteRow()
{
    var nilai = $('#hitung_by').val();
    var counter = parseInt($('#totrow').val());
    if(counter>1)
    {
        $('#tr'+counter).remove();       
        counter -= 1;
        $('#totrow').val(counter);	
        hitung_lagi_by(nilai)   
    } else {
        $('#down_payment_percent').val(0)
        $('#ppn_percent').val(0)
        var hitung_by = $('#hitung_by').removeAttr('disabled');
        $('#tr'+counter).remove();
        $('#totrow').val(0);
        hitung_lagi_by(nilai)   
    }
}

function hitung_by_change(nilai)
{
    $('#hitung_by_real').val(nilai);
}

function hitung_lagi_by(nilai)
{
    var totrow = $('#totrow').val();
    var downPayPersen = $('#down_payment_percent').val()==""?$('#down_payment_percent').val(0):downPayPersen;
        downPayPersen = $('#down_payment_percent').val() || 0;
    var ppnPercent = $('#ppn_percent').val();
    var totAll = 0; var ppn = 0; var hrgPek = 0; var downPayNilai = 0; var sisa = 0;
    if(nilai=='1'){               
        id = '#kg_';
    } else if(nilai=='2') {
        id = '#roll_';
    } else {
        id = '#yard_m_';
    }

    for(var i=1; i<=totrow; i++){
        qty = formatulang($(id+i).val());
        hrg = formatulang($('#hrg_'+i).val());
        var tot = parseFloat(qty) * parseFloat(hrg);
        $('#jml_hrg_'+i).val(formatuang(tot));
        totAll += parseFloat(formatulang($('#jml_hrg_'+i).val())); 
    }
    if(ppnPercent>0){
        ppn = Math.round(parseFloat(totAll)*(ppnPercent/100));
        $('#ppn').val(formatuang(ppn));
    } else {
        $('#ppn').val(ppn);
    }
    hrgPek = parseFloat(totAll) + parseFloat(ppn);
    $('#hrg_pekerjaan').val(formatuang(Math.round(hrgPek)));
    if(parseFloat(downPayPersen)==0 || downPayPersen==''){
        $('#down_payment_nilai').val(0);
        $('#sisa_pembayaran').val(formatuang(Math.round(hrgPek)));    
    } else {
        var hrgPekerjaan = $('#hrg_pekerjaan').val();
        hrgPek = formatulang(hrgPekerjaan);
        downPayPersen = parseFloat(downPayPersen)/100;
        downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
        $('#down_payment_nilai').val(formatuang(downPayNilai));
        sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);
        $('#sisa_pembayaran').val(formatuang(Math.round(sisa)));
    }
    $('#total_harga').val(formatuang(Math.round(totAll)));
    
}

function hitungDownPayment()
{
    var hrgPek = $('#hrg_pekerjaan').val();
        hrgPek = formatulang(hrgPek);
    var downPayPersen = $('#down_payment_percent').val()==""?$('#down_payment_percent').val(0):downPayPersen;
        downPayPersen = $('#down_payment_percent').val() || 0;
    downPayPersen = parseFloat(downPayPersen)/100;
    downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
    $('#down_payment_nilai').val(formatuang(downPayNilai));
    sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);    
    $('#sisa_pembayaran').val(formatuang(Math.round(sisa)));
}

function hitungPPN()
{
    var tothrg = $('#total_harga').val();
        tothrg = formatulang(tothrg);
    var ppn = $('#ppn_percent').val()==""?$('#ppn_percent').val(0):ppn;
        ppn = $('#ppn_percent').val() || 0;
    ppn = parseFloat(ppn)/100;
    ppnNilai = Math.round(parseFloat(tothrg)*ppn);
    $('#ppn').val(formatuang(ppnNilai));
    hrgPek = parseFloat(tothrg) + parseFloat(ppnNilai);    
    $('#hrg_pekerjaan').val(formatuang(Math.round(hrgPek)));
    hitungDownPayment();    
}

function hitungSatu(qty,hrg,baris)
{   
    var totrow = $('#totrow').val();
    var kg = $(qty).val()==""?$(qty).val():kg;
        kg = $(qty).val() || '0';
        kg = formatulang(kg);
    var hrg = $(hrg).val()==""?$(hrg).val():hrg;
        hrg = $(hrg).val() || '0';
    var pkp = $('#pkp').val();
    var ppnPercent = $('#ppn_percent').val()==""?$('#ppn_percent').val(0):ppnPercent;
        ppnPercent = $('#ppn_percent').val() || 0;
    var downPayPersen = $('#down_payment_percent').val()==""?$('#down_payment_percent').val(0):downPayPersen;
        downPayPersen = $('#down_payment_percent').val() || 0;
    var totAll = 0; var ppn = 0; var hrgPek = 0; var downPayNilai = 0; var sisa = 0;
    if(hrg==""){
        $('#jml_hrg_'+baris).val(0);
        for(var i=1; i<=totrow; i++){
            totAll += Math.round(parseFloat(formatulang($('#jml_hrg_'+i).val()))); 
        }
        if(parseInt(ppnPercent)>0){
            ppn = Math.round(parseFloat(totAll)*(ppnPercent/100));
            $('#ppn').val(formatuang(ppn));
        } else {
            $('#ppn').val(0);
        }
        hrgPek = parseFloat(totAll) + parseFloat(ppn);
        $('#hrg_pekerjaan').val(formatuang(Math.round(hrgPek)));
        if(parseFloat(downPayPersen)==0 || downPayPersen==''){
            $('#down_payment_nilai').val(0);
            $('#sisa_pembayaran').val(formatuang(Math.round(hrgPek)));    
        } else {
            var hrgPekerjaan = $('#hrg_pekerjaan').val();
            hrgPek = formatulang(hrgPekerjaan);
            downPayPersen = parseFloat(downPayPersen)/100;
            downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
            $('#down_payment_nilai').val(formatuang(downPayNilai));
            sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);
            $('#sisa_pembayaran').val(formatuang(Math.round(sisa)));
        }
        $('#total_harga').val(formatuang(Math.round(totAll)));
    } else {        
        hrg = formatulang(hrg);
        var tot = parseFloat(kg) * parseFloat(hrg);
        $('#jml_hrg_'+baris).val(formatuang(tot));
        for(var i=1; i<=totrow; i++){                        
            totAll += Math.round(parseFloat(formatulang($('#jml_hrg_'+i).val()))); 
        }
        if(parseInt(ppnPercent)>0){
            ppn = Math.round(parseFloat(totAll)*(ppnPercent/100));
            $('#ppn').val(formatuang(ppn));
        } else {
            $('#ppn').val(ppn);
        }
        // if(pkp){
        //     ppn = Math.round(parseFloat(totAll)*0.1);
        //     $('#ppn').val(formatuang(ppn));
        // } else {
        //     $('#ppn').val(ppn);
        // }
        hrgPek = parseFloat(totAll) + parseFloat(ppn);
        $('#hrg_pekerjaan').val(formatuang(Math.round(hrgPek)));
        if(parseFloat(downPayPersen)==0 || downPayPersen==''){
            $('#down_payment_nilai').val(0);
            $('#sisa_pembayaran').val(formatuang(Math.round(hrgPek)));    
        } else {
            var hrgPekerjaan = $('#hrg_pekerjaan').val();
            hrgPek = formatulang(hrgPekerjaan);
            downPayPersen = parseFloat(downPayPersen)/100;
            downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
            $('#down_payment_nilai').val(formatuang(downPayNilai));
            sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);
            $('#sisa_pembayaran').val(formatuang(Math.round(sisa)));
        }
        $('#total_harga').val(formatuang(Math.round(totAll)));
    }
}

function prosesSimpan(linkLast)
{
    $('#form_so').validate({
        rules: {
            flag_so: {
                required: !0
            },
            pel: {
                required: !0
            },
            d_so: {
                required: !0
            },
            desain_kode1: {
                required: $('#repeat_order').val()=='new'?!0:!1,
                maxlength: 16
            },
            desain_kode2: {
                required: $('#repeat_order').val()=='ro'?!0:!1
            },
            toleransi_cacat_kain: {
                required: !0
            },
            jenis_kain: {
                required: !0
            },
            lebar: {
                required: !0
            },
            gramasi: {
                required: !0
            },
            tgl_strike_off: {
                required: !0
            },
            approval_strike_off: {
                required: !0
            },
            tgl_penyerahan: {
                required: !0
            }
        },
        messages: {
            pel: "Tolong dipilih dulu pelanggannya ya :)",
            d_so: "Tolong dipilih dulu tanggal SO nya ya :)",
            desain_kode1: {
                required: "Tolong diisi kode desain nya ya :)",
                minlength: "Kode desain tidak boleh lebih dari 16 karakter :)"
            },
            desain_kode2: "Tolong dipilih dulu kode desain repeat order nya ya :)",
            toleransi_cacat_kain: "Tolong diisi toleransi cacat kain nya ya :)",
            jenis_kain: "Tolong dipilih dulu jenis kain nya ya :)",
            lebar: "Tolong diisi dulu lebar nya ya :)",
            gramasi: "Tolong diisi dulu gramasi nya ya :)",
            tgl_strike_off: "Tolong dipilih dulu tanggal strike off nya ya :)",
            approval_strike_off: "Tolong dipilih dulu tanggal approval strike off nya ya :)",
            tgl_penyerahan: "Tolong dipilih dulu tanggal penyerahan nya ya :)"
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else if(element.hasClass('row-error') && element.prev('.row-error').length) {
                label.insertAfter(element.next('.row-error'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var totRow = $('#totrow').val();
            // var flag = new Array();
            if(parseInt(totRow)==0){
                swal('Peringatan','Tolong ditambahkan minimal 1 row pada bagian Harga Pekerjaan','warning');
                return false;
            } else if(parseInt(totRow)!=0) {
                // for(var i=1; i<=totRow; i++)
                // {
                //     var uraian_pek = $('input[name="uraian_pek_'+i+'"]').val();
                //     var kg = $('input[name="kg_'+i+'"]').val();
                //     var hrg = $('input[name="hrg_'+i+'"]').val();
                //     if(uraian_pek==""){
                //         $('#error_uraian_pek_'+i).addClass('mt-2 text-danger');
                //         $('#error_uraian_pek_'+i).text("Tolong Isi Uraian Pekerjaan nya ya :)");
                //         return false;
                //     } else if(uraian_pek!="") {
                //         $('#error_uraian_pek_'+i).removeClass('mt-2 text-danger');
                //         $('#error_uraian_pek_'+i).text("");
                //     } 
                //     if(kg=="" || parseInt(kg)==0) {
                //         $('#error_kg_'+i).addClass('mt-2 text-danger');
                //         $('#error_kg_'+i).text("Tolong Isi Kg nya ya :)");
                //         return false;
                //     } else if(kg!="" || parseInt(kg)!=0) {
                //         $('#error_kg_'+i).removeClass('mt-2 text-danger');
                //         $('#error_kg_'+i).text("");
                //     } 
                //     if(hrg=="" || parseInt(hrg)==0) {
                //         $('#error_hrg_'+i).addClass('mt-2 text-danger');
                //         $('#error_hrg_'+i).text("Tolong Isi Harga nya ya :)");
                //         return false;
                //     } else if(hrg!="" || parseInt(hrg)!=0) {
                //         $('#error_hrg_'+i).removeClass('mt-2 text-danger');
                //         $('#error_hrg_'+i).text("");
                //     } 
                //     if(uraian_pek=="" && (kg=="" || parseInt(kg)==0) && (hrg!="" || parseInt(hrg)!=0)) {
                //         $('#error_uraian_pek_'+i).removeClass('mt-2 text-danger');
                //         $('#error_uraian_pek_'+i).text("");
                //         $('#error_kg_'+i).removeClass('mt-2 text-danger');
                //         $('#error_kg_'+i).text("");
                //         $('#error_hrg_'+i).removeClass('mt-2 text-danger');
                //         $('#error_hrg_'+i).text("");
                //     }
                // }

                var form_so = $('#form_so');
                var _form_data = new FormData(form_so[0]);

                swal({   
                    title: "Anda Yakin?",   
                    text: "Yakin Data Sudah Benar Semua?",   
                    type: "info",                       
                    showCancelButton: true,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    cancelButtonText: "Tidak",   
                    closeOnConfirm: false,   
                    closeOnCancel: false,
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        $.ajax({
                            type: "POST",
                            url: base_url + '/sales_order/'+linkLast,
                            data: _form_data,
                            processData: false,
                            contentType: false,
                            beforeSend: function() {
                                $('#loading').show();
                            },
                            success: function(res) {
                                
                            },
                        }).done(function(res){
                            var data = $.parseJSON(res);
                            if(data.code==1){
                                swal({   
                                    title: "Sukses",   
                                    text: data.msg,   
                                    type: "success",                       
                                    showCancelButton: false,   
                                    confirmButtonColor: "#e6b034",   
                                    confirmButtonText: "Ya",                     
                                    closeOnConfirm: true,                                       
                                    showLoaderOnConfirm: true 
                                }, function(isConfirm){   
                                    if(isConfirm){
                                        loadNewPage('{{ route('sales_order.index') }}');
                                    }
                                });
                            } else {
                                swal('Gagal',data.msg,'error');
                            }
                        }).fail(function(xhr,textStatus,errorThrown){
                            $('#loading').hide();
                            swal(textStatus,errorThrown,'error');
                        });                        
                    } else {     
                        swal("Batal", "Data Sales Order Tidak Jadi Di"+linkLast+" :)", "error");   
                    } 
                });
            }
        }
    });
    $('input[id^="roll_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
    $('input[id^="yard_m_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
    $('input[id^="kg_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
    $('input[id^="hrg_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
    $('input[id^="uraian_pek_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
    $('input[id^="strike_off_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
    $('input[id^="i_jns_kain_"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,           
        });
    });
}


</script>