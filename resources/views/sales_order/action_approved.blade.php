<script>
var act_url = '{{ route('data_approved.sales_order') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_so', name: 'i_no_so' },
        { data: 'd_so', name: 'd_so' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },
        { data: 'f_repeat', name: 'f_repeat' },
        // { data: 'd_strike_off', name: 'd_strike_off' },
        // { data: 'd_approval_strike_off', name: 'd_approval_strike_off' },
        { data: 'd_penyerahan_brg', name: 'd_penyerahan_brg' },        
        { data: 'created_at', name: 'created_at' },
        { data: 'definition', name: 'definition' },
    ]
});

function approved(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Approved SO ini?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/sales_order/approved/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('list_approved.sales_order') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi diapproved','info');
        }
    });
}

function reject(id)
{
    swal({
        title: "Reject SO",
        text: "Tulis Alasan Reject",
        type: "input",
        inputPlaceholder: "Alasan",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Lanjut",                     
        cancelButtonText: "Tidak", 
        closeOnConfirm: false,                      
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue.length>200){
            alert('Tidak boleh lebih dari 200 kata');
            return false;
        }
        if (inputValue === "") {
            alert('Alasan harus diisi!');
            return false;
        } else {
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Reject SO ini?",            
                type: "warning",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true,                       
            }, function(isConfirm){   
                if (isConfirm) {                               
                    $.ajax({
                        type: "GET",
                        url: base_url + '/sales_order/reject/'+ id,
                        data: {input_val: inputValue},
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();            
                        var data = $.parseJSON(res);
                        swal({   
                            title: "Sukses",   
                            text: data.msg,   
                            type: "success",                       
                            showCancelButton: false,   
                            confirmButtonColor: "#e6b034",   
                            confirmButtonText: "Ya",                     
                            closeOnConfirm: true,                                       
                            showLoaderOnConfirm: true 
                        }, function(isConfirm){   
                            if(isConfirm){
                                loadNewPage('{{ route('list_approved.sales_order') }}');
                            }
                        });
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });
                } else {
                    swal('Batal','Data tidak jadi direject','info');
                }
            });
        }        
    });
}

function lihat(id,link)
{
    loadNewPage(base_url + '/sales_order/lihat/' +id +'/'+ link);
}
</script>