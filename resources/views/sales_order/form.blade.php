@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_so">
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Sales Order</h4>
                <div class="row">
                    {{-- <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor SO</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_so" id="no_so" value="{{ $nomor_so }}" readonly />
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Flag SO</label>
                            <div class="col-sm-10">
                                <select name="flag_so" id="flag_so" class="form-control form-control-sm border-dark">
                                    <option value="">Pilih Flag!</option>                             
                                @if($flag)
                                    @foreach($flag as $row_flag)
                                        <option value="{{$row_flag->i_id}}">{{$row_flag->flag_name}}</option>
                                    @endforeach
                                @else
                                    <option value="">Flag Tidak Ada</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Pelanggan</label>
                            <div class="col-sm-10">
                                <select class="form-control form-control-sm" name="pel"
                                    id="pel" onchange="getAlamat(this.value)">
                                    @if($pelanggan)
                                        <option value="">Pilih Pelanggan!!</option>
                                    @foreach($pelanggan as $pel)
                                        <option value="{{ $pel->i_pel }}">{{ $pel->e_nama_pel }}</option>
                                    @endforeach
                                    @endif
                                </select>                                
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tanggal SO</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark" name="d_so" id="d_so" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="alamat" id="alamat" readonly />
                                <input type="hidden" id="pkp" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">PO Pelanggan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_po" id="no_po" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Hitung By</label>
                            <div class="col-sm-3">
                                <select id="hitung_by" name="hitung_by" class="form-control border-dark" onchange="hitung_by_change(this.value);">
                                    <option value="1">Kg</option>
                                    <option value="2">Roll</option>
                                    <option value="3">Yard/Meter</option>
                                </select>
                                <input type="hidden" name="hitung_by_real" id="hitung_by_real" value="1"/>
                            </div>
                            <label class="col-sm-2 col-form-label">Cara Bayar</label>
                            <div class="col-sm-3">
                                <select id="cara_bayar" name="cara_bayar" class="form-control border-dark">
                                    <option value="2">TRANSFER</option>                                    
                                    <option value="1">COD/CASH</option>
                                </select>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">A. SPESIFIKASI PEKERJAAN</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A1. MOTIF/DESAIN:</label>
                            <div class="col-sm-3">
                                <select class="form-control form-control-sm border-dark" name="repeat_order" id="repeat_order" onchange="tipeMotif(this.value)">
                                    <option value="new">Baru</option>
                                    <option value="ro">Repeat Order</option>
                                </select>
                            </div>
                            <div class="col-sm-6" id="motif_desain">
                                <input type="text" class="form-control form-control-sm border-dark" style="display:block;" name="desain_kode1"
                                    id="desain_kode1" maxlength="16"/>
                                <select class="form-control form-control-sm" style="display:none" name="desain_kode2" id="desain_kode2" onchange="getKain(this.value)">
                                </select>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A2. JUMLAH WARNA:</label>
                            <div class="col-sm-3">
                                <select class="form-control form-control-sm border-dark" name="jml_warna" id="jml_warna">
                                    @for($i=1; $i<9; $i++) 
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                        <option value="0">Others</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark" name="motif_nama"
                                    id="motif_nama" maxlength="50"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A3. JENIS PRINTING:</label>
                            <div class="col-sm-4">
                                <select class="form-control form-control-sm border-dark" name="jns_printing" id="jns_printing">
                                    @if($jns_printing)
                                        @foreach($jns_printing as $item)
                                            <option value="{{$item->i_id}}">{{$item->e_jns_printing}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A4. COLOR WAY:</label>
                            <div class="col-sm-3">
                                <select class="form-control form-control-sm border-dark" name="color_way" id="color_way">
                                    @for($i=1; $i<9; $i++) <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                        <option value="0">Others</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark" name="color_way_desc"
                                    id="color_way_desc" maxlength="100" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A5. TOLERANSI CACAT KAIN:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm border-dark" name="toleransi_cacat_kain"
                                    id="toleransi_cacat_kain" maxlength="250" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">B. SPESIFIKASI KAIN</h4>
                {{--<div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B1. JENIS KAIN</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm border-dark" name="jenis_kain" id="jenis_kain">
                                @if($jenis_kain)
                                    <option value="">Pilih Kain!</option>
                                    @foreach($jenis_kain as $kain)
                                        <option value="{{ $kain->i_id }}">{{ $kain->e_kain }}</option>
                                    @endforeach
                                @else
                                    <option value="0">Tidak Ada Kain</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>--}}
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B1. KONDISI KAIN</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm border-dark" name="kondisi_kain" id="kondisi_kain">
                                    @if($kondisi_kain)
                                        @foreach($kondisi_kain as $item)
                                            <option value="{{$item->i_id}}">{{$item->e_kondisi_kain}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B2. LEBAR</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm border-dark" name="lebar" id="lebar" maxlength="50" />
                            </div>
                            <div class="col-sm-2">
                                <p>cm/inch</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B3. GRAMASI</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm border-dark" name="gramasi" id="gramasi" maxlength="25" />
                            </div>
                            <div class="col-sm-2">
                                <p>gr/m^2</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B4. PENYEDIA KAIN</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm border-dark" name="penyedia_kain" id="penyedia_kain">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">C. HARGA PEKERJAAN</h4>
                <p class="card-description">
                    <input type="hidden" id="totrow" name="totrow" value="0">
                    <button type="button" onclick="addRow()" class="btn btn-info btn-icon-text">
                        <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                        Tambah Row
                    </button>
                    <button type="button" onclick="deleteRow()" class="btn btn-danger btn-icon-text">
                        <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                        Kurang Row
                    </button>
                </p>
                <div class="table-responsive pt-1">
                    <table class="table table-bordered" id="table-harga">
                        <thead style="background-color: lightcyan;">
                            <tr>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;">NO.</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;width:35%;">URAIAN PEKERJAAN</th>
                                <th colspan="3" style="text-align: center;vertical-align: middle;">QUANTITY</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;width:16%;">HARGA SATUAN (Rp.)</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;">JUMLAH HARGA</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;vertical-align: middle;width:10%;">Roll</th>
                                <th style="text-align: center;vertical-align: middle;width:10%;">Panjang (yds/m)</th>
                                <th style="text-align: center;vertical-align: middle;width:10%;">Berat (kg)</th>
                            </tr>
                        </thead>
                        <tbody id="detailbody">                        
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Total Harga =</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="total_harga" id="total_harga" readonly/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">PPN = <input type="text" style="text-align:right;width:20%;" class="" name="ppn_percent" id="ppn_percent" onkeyup="hitungPPN();" onkeydown="hanyaangka();" value="0"/>%</td></td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="ppn" id="ppn" readonly/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Harga Pekerjaan =</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="hrg_pekerjaan" id="hrg_pekerjaan" readonly/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Down Payment = <input type="text" style="text-align:right;width:20%;" class="" name="down_payment_percent" id="down_payment_percent" onkeyup="hitungDownPayment();" onkeydown="hanyaangka();" value="0"/>%</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="down_payment_nilai" id="down_payment_nilai" readonly/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Sisa Pembayaran =</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="sisa_pembayaran" id="sisa_pembayaran" readonly/></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">D. PENYELESAIAN PEKERJAAN</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">D1. PERSETUJUAN STRIKE OFF:</label>                            
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="tgl_strike_off" id="tgl_strike_off" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">D2. PERSETUJUAN MENYELURUH:</label>                            
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="approval_strike_off"
                                    id="approval_strike_off" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">D3. PENYERAHAN BARANG:</label>                            
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="tgl_penyerahan"
                                    id="tgl_penyerahan" readonly/>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">E. KETERANGAN KHUSUS</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">KETERANGAN</label>                            
                            <div class="col-sm-6">
                                <textarea type="text" class="form-control border-dark" name="ket_penyerahan"
                                    id="ket_penyerahan" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="card">
            <div class="card-body">
                <button type="submit" onclick="return prosesSimpan('simpan')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Submit
                </button>
                <button type="button" onclick="loadNewPage('{{ route('sales_order.index') }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </div>
        </div>
    </form>
</div>
@include('sales_order.actionform')
@endsection
