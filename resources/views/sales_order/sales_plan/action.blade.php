<script>
var act_url = '{{ route('data.sales_plan') }}';
var table = $('#datatable1').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [        
        { data: 'n_qty', name: 'n_qty' },
        { data: 'bulan', name: 'bulan' },
        { data: 'tahun', name: 'tahun' },        
    ],
    columnDefs: [
    {
        targets: 0, // your case first column
        className: "text-center",        
    },
    {
        targets: 1,
        className: "text-center",
    },
    {
        targets: 2,
        className: "text-center",   
    }]
});

var d = new Date();
var currentYear = d.getFullYear();

function get_sales_plan(tahun){
    if(tahun==''){
        $('input[id^="qty"]').each(function (k,v) {        
            $(this).val('');
        });
    } else {
        $.ajax({
            type: "GET",
            url: base_url + '/sales_plan/ambil_data/'+tahun,        
            beforeSend: function() {
                $('#loading').show();
                $('input[id^="qty"]').each(function (k,v) {        
                    $(this).val('');
                });
            },
            success: function(res) {
                
            },
        }).done(function(res){
            $('#loading').hide();
            var data = $.parseJSON(res);
            if(data) {
                $.each(data, function(k,v){
                    $('#qty'+(k+1)).val(formatuang(v.n_qty));
                });
            }
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        }); 
    }
}

function save(){
    $('#form_sales_plan').validate({
        rules : {
            tahun: {
                required:!0
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_sales_plan');
            var _form_data = new FormData(form_rfp[0]);
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/sales_plan/save',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('plan.sales_order') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
    $('input[id^="qty"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0
        });
    });
}
</script>