@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Add / Update Sales Plan</h4>
            <div class="card">
                <div class="card-body">
                <form class="form-sample" id="form_sales_plan">
                @csrf
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group row">
                                <label class="col-sm-2">Tahun</label>
                                <div class="col-sm-4">
                                    <select class="form-control form-control-sm border-dark" id="tahun" name="tahun" onchange="get_sales_plan(this.value)" required>
                                    <option value="">Pilih Tahun</option>
                                    @php
                                        $now=date('Y', strtotime('-1 years'));                            
                                        $end = date('Y', strtotime('+1 years'));
                                        for ($a=$now;$a<=$end;$a++)
                                        {
                                            echo '<option value="'.$a.'">'.$a.'</option>';
                                        }                            
                                    @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">                        
                        @php
                            $namaBulan = array( 1 => "Januari", 7 => "Juli", 2 => "Februari", 8 => "Agustus", 3 => "Maret", 9 => "September", 4 => "April", 10 => "Oktober", 5 => "Mei", 11 => "November", 6 => "Juni", 12 => "Desember");
                            $noBulan = 1;                                
                        @endphp                                
                        @foreach($namaBulan as $key => $bulan)
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-4">Qty {{$bulan}} (Kg) :</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="bulan{{$key}}" id="bulan{{$key}}" value="{{$key}}"/>
                                    <input type="text" name="qty{{$key}}" id="qty{{$key}}" class="form-control form-control-sm border-dark" style="text-align:right;" onkeyup="formatRp(this);" onkeydown="hanyaangka();">
                                </div>                                        
                            </div>
                        </div>
                        @php $noBulan++; @endphp
                        @endforeach                            
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="tot_bulan" value="12">
                            <button type="submit" onclick="save()" class="btn btn-success btn-icon-text">
                                <i class="mdi mdi-content-save btn-icon-prepend"></i>
                                Save
                            </button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('sales_order.sales_plan.action')
@endsection