@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Sales Plan</h4>
            <p class="card-description">
                <button type="button" onclick="loadNewPage('{{ route('form_plan.sales_order') }}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add / Update Sales Plan
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable1">
                    <thead>
                        <tr>                            
                            <th style="text-align:center;">Quantity</th>
                            <th style="text-align:center;">Month</th> 
                            <th style="text-align:center;">Year</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('sales_order.sales_plan.action')
@endsection