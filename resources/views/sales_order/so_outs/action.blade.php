<script>
var act_url = '{{ route('so_outsourcing.data') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_so', name: 'i_no_so' },
        { data: 'd_so', name: 'd_so' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },
        // { data: 'f_repeat', name: 'f_repeat' },
        // { data: 'd_strike_off', name: 'd_strike_off' },
        // { data: 'd_approval_strike_off', name: 'd_approval_strike_off' },
        // { data: 'd_penyerahan_brg', name: 'd_penyerahan_brg' },
        { data: 'd_approved', name: 'd_approved' },
        { data: 'created_at', name: 'created_at' },
        { data: 'definition', name: 'definition' },
        { data: 'e_reject', name: 'e_reject' },
    ]
});
</script>