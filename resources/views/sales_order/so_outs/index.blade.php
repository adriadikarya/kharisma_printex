@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Daftar Sales Order (SO) OutSourcing</h4>
            <p class="card-description">
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>SO Number</th>
                            <th>SO Date</th>
                            <th>Customer</th>
                            {{--<th>Repeat Order</th>
                            <th>Tgl Strike Off</th>
                            <th>Tgl Approve Strike Off</th>
                            <th>Tgl Penyerahan Barang</th>--}}
                            <th>Date Approved / Reject Manager</th>
                            <th>Created Date</th>
                            <th>Status</th>
                            <th>Reject Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('sales_order.so_outs.action')
@endsection
