@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@php
    date_default_timezone_set('Asia/Jakarta');
    function toNum($data) {
        $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
        echo $alpha[$data];
    }
@endphp
@foreach($param['datahead'] as $datahead)
<style type="text/css" media="print">
    @page 
        {            
            /*size: auto;*/
            /*size: F4;*/
            /*size: auto;    auto is the current printer page size */
            margin: 0cm;  /* this affects the margin in the printer settings */
        }
    .isinya {
        font-family: Helvetica, Geneva, Arial,
          SunSans-Regular, sans-serif; 
        font-size: 12px;
    }
    .noDisplay{
        display:none;
    }
    .pageBreak {
        page-break-before: always;
    }
</style>
<div class="col-12 grid-margin">    
    <div class="card noDisplay">
        <div class="card-body">                
            <button type="button" onclick="loadNewPage('{{ route($rut) }}')" class="btn btn-warning btn-icon-text">
                <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                Kembali
            </button>
            <button type="button" onclick="printDiv('media_print')" class="btn btn-info btn-icon-text">
                <i class="mdi mdi-printer btn-icon-prepend"></i>
                Print
            </button>
        </div>
    </div>
    <div id="media_print">
        <div class="card">
            <div class="card-body text-dark">
                <div class="col-md-12 text-right"><u><b>FR.MK.01.04</b></u></div>
                <div class="col-md-12">
                @if($param['logo'])                
                    <img src="{{ asset('images/logo_so/'.$param['logo'][0]->logo_name) }}" alt="" style="width: 100%;height:75px;">
                @else
                    <img src="{{ asset('images/Untitled.png') }}" alt="" style="width: 100%;height:75px;">
                @endif
                </div>
                <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12">                    
                    <table class="table" style="border: 3px solid black;">                                                
                        <tbody>
                            <tr>
                                <td rowspan="3" colspan="3" style="text-align: center;font-size: 60px;border: 3px solid black;width: 50%">
                                    <b>SALES ORDER</b>
                                </td>                                
                                <td style="border: 1px solid black;font-size:12px;">No. SO</td>
                                <td style="border: 1px solid black;font-size:12px;">{{ $datahead->i_no_so }}</td>  
                                <!--<td rowspan="3" style="text-align: center;font-size: 60px;border: 3px solid black;width: 10%">
                                {!! QRCode::text($param['qrCode'])
                                        ->setErrorCorrectionLevel('M')
                                        ->setSize(1)
                                        ->setMargin(1)
                                        ->svg(); !!}
                                </td> -->
                            </tr>
                            <tr>                                
                                <td style="border: 1px solid black;font-size:12px;">Tanggal</td>
                                <td style="border: 1px solid black;font-size:12px;">{{ date('d M Y', strtotime($datahead->d_so)) }}</td>
                            </tr>
                            <tr>                                
                                <td style="border: 1px solid black;border-bottom: 3px solid black;font-size:12px;">No. PO</td>
                                <td style="border: 1px solid black;border-bottom: 3px solid black;font-size:12px;">{{ $datahead->i_no_po }}</td>
                            </tr>                            
                        </tbody>
                    </table>                    
                </div>
                <div class="col-md-12" style="margin-top: -15px;">&nbsp;</div>
                <div class="col-md-12">
                    <table class="table" style="border: 3px solid black;"> 
                        <tbody>
                            <tr>
                                <td style="border: 1px solid black;font-size:12px;width:20%;">PELANGGAN</td>
                                <td style="border: 1px solid black;font-size:12px;">{{ $datahead->e_nama_pel }}</td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid black;font-size:12px;">ALAMAT</td>
                                <td style="border: 1px solid black;font-size:12px;">{{ $datahead->e_alamat_pel }}</td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid black;font-size:12px;">KETERANGAN KHUSUS</td>
                                <td style="border: 1px solid black;font-size:12px;"><b>{{ $datahead->e_keterangan_kirim }}</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>    
                <div class="col-md-12">&nbsp;</div>            
                <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top: -10px;">
                    <label class="col-md-6" style="font-size:15px;"><b>A. SPESIFIKASI PEKERJAAN</b></label>
                    <label class="col-md-6" style="font-size:15px;"><b>B. SPESIFIKASI KAIN</b></label>
                </div>        
                <div class="row" style="margin-left: 0px;margin-right: 0px;">                        
                    <div class="col-md-6" style="margin-top:-10px;">
                        <table align="left" class="table" style="border: 0px;" cellpadding="2" cellspacing="0">                                                
                            <tbody>
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">A1. MOTIF/DESAIN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->i_desain }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">A2. JUMLAH WARNA</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->n_qty_warna.' '.$datahead->e_motif }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">A3. JENIS PRINTING</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->e_jns_printing }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">A4. COLOR WAY</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->n_color_way.' '.$datahead->e_color_way }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 20%;font-size:12px;">A5. TOLERANSI CACAT KAIN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->e_toleransi_cacat }}</td>
                                </tr>
                            </tbody>
                        </table>                    
                    </div>
                    <!-- <div class="col-md-12">
                        <label><b>B. SPESIFIKASI KAIN</b></label>
                    </div> -->
                    <div class="col-md-6" style="margin-top:-10px;">
                        <table class="table" style="border: 0px;">                                                
                            <tbody>
                                {{--<tr>                                    
                                    <td style="width: 40%;font-size:12px;">B1. JENIS KAIN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->e_kain }}</td>
                                </tr>--}}
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">B1. KONDISI KAIN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->e_kondisi_kain }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">B2. LEBAR</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->e_lebar.' cm/inch' }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 40%;font-size:12px;">B3. GRAMASI</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->e_gramasi.' gr/m2' }}</td>
                                </tr>
                                <tr>
                                    
                                    <td style="width: 20%;font-size:12px;">B4. PENYEDIA KAIN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ $datahead->penyedia }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <label style="font-size:15px;"><b>C. HARGA PEKERJAAN</b></label>
                </div>
                <div class="col-md-12">
                    <table class="table" style="border: 0px solid black;">                        
                        <tbody>
                            <tr style="background-color: lightblue;">
                                <td rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;">NO.</td>
                                <td rowspan="2" style="text-align: center;vertical-align: middle;width:50   %;border: 1px solid black;">URAIAN PEKERJAAN</td>
                                <td colspan="3" style="text-align: center;vertical-align: middle;border: 1px solid black;">QUANTITY</td>
                                <td rowspan="2" style="text-align: center;vertical-align: middle;width:14%;border: 1px solid black;">HARGA SATUAN (Rp.)</td>
                                <td rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;width:12%;">JUMLAH HARGA</td>
                            </tr>
                            <tr style="background-color: lightblue;">
                                <td style="text-align: center;vertical-align: middle;border: 1px solid black;width: 6%;">Roll</td>
                                <td style="text-align: center;vertical-align: middle;border: 1px solid black;width: 1%;">Panjang (yds/m)</td>
                                <td style="text-align: center;vertical-align: middle;border: 1px solid black;width: 6%;">Berat (kg)</td>
                            </tr>
                            @if($param['dataitem'])
                            @php $no=1; @endphp
                            @foreach($param['dataitem'] as $item)
                                @php
                                if($datahead->hitung_by==1){
                                    $jmlHrg = $item->n_qty_kg * $item->v_harga_sat;
                                } else if($datahead->hiitung_by==2) {
                                    $jmlHrg = $item->n_qty_roll * $item->v_harga_sat;
                                } else {
                                    $jmlHrg = $item->n_qty_pjg * $item->v_harga_sat;
                                }
                                @endphp                                                            
                                <tr>
                                    <td align="center" style="border: 1px solid black;font-size:12px;">{{ $no++ }}</td>
                                    <td style="border: 1px solid black;font-size:12px;">{{ $item->e_uraian_pekerjaan }} | strikeoff - {{date('d M Y', strtotime($item->d_strike_off))}} | jenis kain - {{$item->e_kain}}</td>
                                    <td align="right" style="border: 1px solid black;font-size:12px;">{{ $item->n_qty_roll }}</td>
                                    <td align="right" style="border: 1px solid black;font-size:12px;">{{ $item->n_qty_pjg }}</td>
                                    <td align="right" style="border: 1px solid black;font-size:12px;">{{ $item->n_qty_kg }}</td>
                                    <td align="right" style="border: 1px solid black;font-size:12px;">{{ number_format($item->v_harga_sat,2,'.',',') }}</td>
                                    <td align="right" style="border: 1px solid black;font-size:12px;">{{ number_format($jmlHrg) }}</td>
                                </tr>                                                             
                            @endforeach
                            @else
                            <tr>
                                <td colspan="7">Maaf Data Item Tidak Ada (Terjadi Kesalahan)!</td>
                            </tr>
                            @endif
                            <tr>                                
                                <td colspan="5" style="border: none;font-size:12px;"><b><u>Catatan:</u></b></td>
                                <td style="text-align:right;font-size:12px;">Total Harga =</td>
                                <td style="text-align:right;border: 1px solid black;font-size:14px;"><b>{{ number_format($datahead->v_pekerjaan) }}</b></td>
                            </tr>
                            <tr>
                                <td colspan="5" rowspan="4" style="border:0;vertical-align: baseline;font-size:12px;">Harga pekerjaan dihitung berdasarkan berat kain greige atau berat kain yang diterima apabila penyedia kain adalah pemberi order,<br> Sisa Pembayaran  (Selambat-lambatnya {{$datahead->n_jth_tempo}} hari dari penyerahan barang)</td>
                                <td style="text-align:right;font-size:12px;">PPN {{ $datahead->n_ppn }}% =</td>
                                <td style="text-align:right;border: 1px solid black;font-size:14px;">
                                {{-- @if($datahead->f_pkp)
                                @php $ppn = $datahead->v_pekerjaan*0.1; @endphp
                                    <b>{{number_format($ppn,0,'.',',')}}</b>
                                @else
                                    <b>0</b>
                                @endif --}}
                                    <b>{{ number_format($datahead->v_ppn,0,'.',',') }}</b>
                                </td>
                            </tr>
                            <tr>                                
                                <td style="text-align:right;font-size:12px;">Harga Pekerjaan =</td>
                                <td style="text-align:right;border: 1px solid black;font-size:14px;"><b>{{ number_format($datahead->v_pekerjaan_plus_ppn) }}</b></td>
                            </tr>
                            <tr>                                
                                <td style="text-align:right;font-size:12px;">Down Payment = {{ $datahead->n_discount }}%</td>
                                <td style="text-align:right;border: 1px solid black;font-size:14px;"><b>{{ number_format($datahead->v_discount) }}</b></td>
                            </tr>
                            <tr>                                
                                <td style="text-align:right;font-size:12px;">Sisa Pembayaran =</td>
                                <td style="text-align:right;border: 1px solid black;font-size:14px;"><b>{{ number_format($datahead->v_sisa) }}</b></td>
                            </tr>
                            </tr>
                        </tbody>                        
                    </table>
                </div>
                <div class="col-md-12">&nbsp;</div>                
                <!-- <div class="col-md-12">&nbsp;</div>
                <div class="col-md-12">&nbsp;</div> -->
                <div class="row" style="margin-left: 0px;margin-right: 0px;">
                    <div class="col-md-6">
                        <label style="font-size:15px;"><b>D. PENYELESAIAN PEKERJAAN</b></label>
                    </div>
                    <div class="col-md-6">
                        <label style="font-size:15px;"><b>E. KETENTUAN PEMBAYARAN</b></label>
                    </div>                
                    <div class="col-md-6">
                        <table class="table" style="border: 0px;">                                                
                            <tbody>
                                <tr>                                    
                                    <td style="width: 40%;font-size:12px;">D1. PERSETUJUAN STRIKE OFF</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ date('d M Y', strtotime($datahead->d_strike_off)) }}</td>
                                </tr>
                                <tr>                                    
                                    <td style="width: 40%;font-size:12px;">D2. PERSETUJUAN MENYELURUH</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ date('d M Y', strtotime($datahead->d_approval_strike_off)) }}</td>
                                </tr>
                                <tr>                                    
                                    <td style="width: 20%;font-size:12px;">D3. PENYERAHAN BARANG</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ date('d M Y', strtotime($datahead->d_penyerahan_brg)) }}</td>
                                </tr>                            
                            </tbody>
                        </table>
                    </div>
                    <!-- <div class="col-md-12">
                        <label><b>E. KETENTUAN PEMBAYARAN</b></label>
                    </div> -->
                    <div class="col-md-6">
                        <table class="table" style="border: 0px;">                                                
                            <tbody>
                                <tr>
                                    <td style="width: 35%;font-size:12px;">E1. DOWN PAYMENT</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">{{ number_format($datahead->v_discount) }}</td>
                                </tr>
                                <tr>
                                    <td style="width: 35%;font-size:12px;">E2. SISA PEMBAYARAN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;"><b>{{ number_format($datahead->v_sisa) }}</b></td>
                                </tr>
                                <tr>
                                    <td style="width: 20%;font-size:12px;">E3. CARA PEMBAYARAN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">
                                    @if($datahead->cara_bayar==2) Transfer melalui rek. BCA PT Kharisma Printex A/C No. 157 300 1161 @else CASH/COD @endif</td>
                                </tr>   
                                <tr>
                                    <td style="width: 20%;font-size:12px;">E4. LAMA PEMBAYARAN</td>
                                    <td style="width: 1%">:</td>
                                    <td style="font-size:12px;">@if($datahead->cara_bayar==2) {{ $datahead->n_jth_tempo }} @else 0 @endif Hari</td>
                                </tr>                                                                             
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table" style="border: 0px;">                        
                        <tbody>
                            <tr>
                                <td style="width: 1%">&nbsp;</td>
                                <td style="width: 20%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>DITERIMA OLEH</b></td>
                                <td style="width: 20%"></td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>PEMESAN</b></td>
                                <td align="right" rowspan="3">{!! QRCode::text($param['qrCode'])
                                        ->setErrorCorrectionLevel('M')
                                        ->setSize(2)
                                        ->setMargin(1)
                                        ->svg(); !!}</td>
                            </tr>   
                            <tr>                                
                                <td colspan="4">&nbsp;</td>
                            </tr>                                                                   
                            <tr>                                
                                <td style="width: 1%">&nbsp;</td>
                                <td style="width: 20%"><b><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></b><br><b>Manajer Pemasaran</b></td>
                                <td style="width: 20%"></td>
                                <td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><br>&nbsp;</td>
                            </tr>                              
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <label style="font-size:11px;"><b><u>KETENTUAN UMUM</u></b></label>
                </div>
                @if($param['ket_umum'])
                    @foreach($param['ket_umum'] as $key => $ket_umum)                    
                    <div class="col-md-12" style="margin-top:-12px">
                        <i style="font-size:9px;">{{($key+1).". ".$ket_umum->e_ket_umum}}</i>
                    </div>
                    @php
                        $detail = \DB::select("SELECT * FROM ref_ket_umum_so_child WHERE i_id_ket_umum='".$ket_umum->i_id."'");
                    @endphp
                    @if($detail)
                        @foreach($detail as $key => $det)
                            <div class="col-md-12" style="margin-top:-12px">                    
                                <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{toNum($key).". ".$det->e_child_ket_umum}}</i>
                            </div>
                        @endforeach
                    @endif
                    @endforeach
                @else
                <div class="col-md-12" style="margin-top:-15px">
                    <i style="font-size:9px;">1. Sales Order (SO) ini berlaku sebagai Purchase Order (PO) apabila Pemesan tidak menerbitkan Purchase Order (PO)</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">2. PT Kharisma Printex (selanjutnya disebut PT KP) dibebaskan dari segala tuntutan hukum atas desain kain yang diminta oleh Pemesan.</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">3. Pekerjaan dapat dilaksanakan setelah Sales Order ditandatangani oleh Pemesan, dan tidak dapat diubah baik sebagian atau seluruhnya (desain, warna, dan quantity) apabila kain telah dipersiapkan/dipartaikan untuk printing.</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">4. Sales Order (SO) ini berlaku 2 bulan sejak diserahkan kepada Pemesan.</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">5. Komplain/klaim :</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">                    
                    <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. PT KP tidak melayani komplain/klaim setelah 30 hari sejak tanggal penerimaan barang oleh Pemesan, lewat dari waktu tersebut 
   dianggap barang telah disetujui untuk diterima.</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. PT KP hanya menerima komplain/klaim dalam bentuk kain utuh, bukan dalam bentuk potongan kain atau potongan/sampel baju.</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">                    
                    <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c. PT KP tidak menanggung klaim keterlambatan pengiriman yang diakibatkan oleh pengiriman pihak ketiga yang telah disetujui
   Pemesan.</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. PT KP tidak menanggung klaim keterlambatan dan/atau kerusakan barang yang diakibatkan terjadinya force majeur.</i>
                </div>                
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">6. Atas terjadinya pembatalan yang diminta oleh Pemesan, dikenakan biaya :</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Strike off : Rp. 200.000 (dua ratus ribu rupiah)/warna</i>
                </div>
                <div class="col-md-12" style="margin-top:-12px">                    
                    <i style="font-size:9px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Screen : Rp. 1.000.000 (satu juta rupiah)/warna</i>
                </div>                
                <div class="col-md-12" style="margin-top:-12px">
                    <i style="font-size:9px;">7. Minimum order 600 kg (baru maupun pengulangan)</i>
                </div>
                @endif
            </div>
        </div>            
    </div>
</div>
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;
    window.print();

    document.body.innerHTML = originalContents;
}
</script>
@endforeach
@endsection