@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@foreach($param['datahead'] as $datahead)
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_so">
        <div class="card">
            <div class="card-body">                
                <button type="button" onclick="loadNewPage('{{ route($rut) }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Sales Order</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nomor SO</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="id_so" id="id_so" value="{{ $datahead->i_id }}">  
                                <input type="text" class="form-control form-control-sm border-dark" name="no_so" id="no_so" value="{{ $datahead->i_no_so }}" disabled />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Pelanggan</label>
                            <div class="col-sm-10">
                                <select class="form-control form-control-sm" name="pel"
                                    id="pel" onchange="getAlamat(this.value)" disabled>
                                    @if($param['pelanggan'])
                                        <option value="">Pilih Pelanggan!!</option>
                                    @foreach($param['pelanggan'] as $pel)
                                        @php
                                            $selected = $pel->i_pel==$datahead->i_pel?"selected":"";
                                        @endphp
                                        <option value="{{ $pel->i_pel }}" {{ $selected }}>{{ $pel->e_nama_pel }}</option>
                                    @endforeach
                                    @endif
                                </select>                                
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tanggal SO</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark" name="d_so" id="d_so" value="{{ date('d-m-Y', strtotime($datahead->d_so)) }}" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="alamat" id="alamat" value="{{ $datahead->e_alamat_pel }}" disabled />
                                <input type="hidden" id="pkp" value="{{ $datahead->f_pkp }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">PO Pelanggan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark" name="no_po" id="no_po" value="{{ $datahead->i_no_po }}" disabled/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Hitung By</label>
                            <div class="col-sm-3">
                                <select id="hitung_by" name="hitung_by" class="form-control border-dark" onchange="hitung_by_change(this.value);" disabled>
                                    <option value="1" @if($datahead->hitung_by=='1') selected @endif>Kg</option>
                                    <option value="2" @if($datahead->hitung_by=='2') selected @endif>Roll</option>
                                    <option value="3" @if($datahead->hitung_by=='3') selected @endif>Yard/Meter</option>
                                </select>
                                <input type="hidden" name="hitung_by_real" id="hitung_by_real" value="1"/>
                            </div>
                            <label class="col-sm-2 col-form-label">Cara Bayar</label>
                            <div class="col-sm-3">
                                <select id="cara_bayar" name="cara_bayar" class="form-control border-dark" disabled>
                                    <option value="2" @if($datahead->cara_bayar=='2') selected @endif>TRANSFER</option>
                                    <option value="1" @if($datahead->cara_bayar=='1') selected @endif>COD/CASH</option>
                                </select>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">A. SPESIFIKASI PEKERJAAN</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A1. MOTIF/DESAIN:</label>
                            <div class="col-sm-3">
                                <select class="form-control form-control-sm border-dark" name="repeat_order" id="repeat_order" onchange="tipeMotif(this.value)" disabled>
                                    <option value="new" @if($datahead->f_repeat==false) selected @endif>Baru</option>
                                    <option value="ro" @if($datahead->f_repeat==true) selected @endif>Repeat Order</option>
                                </select>
                            </div>
                            <div class="col-sm-6" id="motif_desain">
                                <input type="text" class="form-control form-control-sm border-dark" style="display:block;" name="desain_kode1"
                                    id="desain_kode1" maxlength="16" value="{{ $datahead->i_desain }}" disabled/>
                                <select class="form-control form-control-sm" style="display:none" name="desain_kode2" id="desain_kode2" onchange="getKain(this.value)" disabled>
                                </select>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A2. JUMLAH WARNA:</label>
                            <div class="col-sm-3">
                                <select class="form-control form-control-sm border-dark" name="jml_warna" id="jml_warna" disabled>
                                    @for($i=1; $i<9; $i++) 
                                        <option value="{{$i}}" @if($i==$datahead->n_qty_warna) selected @endif>{{$i}}</option>
                                    @endfor
                                    <option value="0" @if($datahead->n_qty_warna==0) selected @endif>Others</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark" name="motif_nama"
                                    id="motif_nama" maxlength="50" value="{{ $datahead->e_motif }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A3. JENIS PRINTING:</label>
                            <div class="col-sm-4">
                                <select class="form-control form-control-sm border-dark" name="jns_printing" id="jns_printing" disabled>
                                    @if($param['jns_printing'])
                                        @foreach($param['jns_printing'] as $item)
                                        <option value="{{$item->i_id}}" @if($datahead->e_jenis_printing==$item->i_id) selected @endif>{{$item->e_jns_printing}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A4. COLOR WAY:</label>
                            <div class="col-sm-3">
                                <select class="form-control form-control-sm border-dark" name="color_way" id="color_way" disabled>
                                    @for($i=1; $i<9; $i++) 
                                        <option value="{{$i}}" @if($i==$datahead->n_color_way) selected @endif>{{$i}}</option>
                                    @endfor
                                    <option value="0" @if($datahead->n_color_way==0) selected @endif>>Others</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark" name="color_way_desc"
                                    id="color_way_desc" maxlength="100" value="{{ $datahead->e_color_way }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">A5. TOLERANSI CACAT KAIN:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm border-dark" name="toleransi_cacat_kain"
                                    id="toleransi_cacat_kain" maxlength="250" value="{{ $datahead->e_toleransi_cacat }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">B. SPESIFIKASI KAIN</h4>
                {{--<div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B1. JENIS KAIN</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm border-dark" name="jenis_kain" id="jenis_kain" disabled>
                                @if($param['jenis_kain'])
                                    <option value="">Pilih Kain!</option>
                                    @foreach($param['jenis_kain'] as $kain)
                                        <option value="{{ $kain->i_id }}" @if($datahead->i_jns_kain==$kain->i_id) selected @endif>{{ $kain->e_kain }}</option>
                                    @endforeach
                                @else
                                    <option value="0">Tidak Ada Kain</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>--}}
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B1. KONDISI KAIN</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm border-dark" name="kondisi_kain" id="kondisi_kain" disabled>
                                    @if($param['kondisi_kain'])
                                        @foreach($param['kondisi_kain'] as $item)
                                            <option value="{{$item->i_id}}" @if($datahead->e_kondisi_kain==$item->i_id) selected @endif>{{$item->e_kondisi_kain}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B2. LEBAR</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm border-dark" name="lebar" id="lebar" maxlength="50" value="{{ $datahead->e_lebar }}" disabled/>
                            </div>
                            <div class="col-sm-2">
                                <p>cm/inch</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B3. GRAMASI</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm border-dark" name="gramasi" id="gramasi" maxlength="25" value="{{ $datahead->e_gramasi }}" disabled/>
                            </div>
                            <div class="col-sm-2">
                                <p>gr/m^2</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">B4. PENYEDIA KAIN</label>
                            <div class="col-sm-6">
                                <select class="form-control form-control-sm border-dark" name="penyedia_kain" id="penyedia_kain" disabled>
                                    <option value="0" @if($datahead->i_penyedia==0) selected @endif>PT. Kharisma Printex</option>
                                    <option value="{{ $datahead->i_pel }}" @if($datahead->i_penyedia==$datahead->i_pel) selected @endif>{{ $datahead->e_nama_pel }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">C. HARGA PEKERJAAN</h4>
                <p class="card-description">
                    <input type="hidden" id="totrow" name="totrow" value="{{ count($param['dataitem']) }}">
                    <button type="button" onclick="addRow()" class="btn btn-info btn-icon-text" disabled>
                        <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                        Tambah Row
                    </button>
                    <button type="button" onclick="deleteRow()" class="btn btn-danger btn-icon-text" disabled>
                        <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                        Kurang Row
                    </button>
                </p>
                <div class="table-responsive pt-1">
                    <table class="table table-bordered" id="table-harga">
                        <thead style="background-color: lightcyan;">
                            <tr>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;">NO.</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;width:35%;">URAIAN PEKERJAAN</th>
                                <th colspan="3" style="text-align: center;vertical-align: middle;">QUANTITY</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;width:20%;">HARGA SATUAN (Rp.)</th>
                                <th rowspan="2" style="text-align: center;vertical-align: middle;">JUMLAH HARGA</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;vertical-align: middle;">Roll</th>
                                <th style="text-align: center;vertical-align: middle;">Panjang (yds/m)</th>
                                <th style="text-align: center;vertical-align: middle;">Berat (kg)</th>
                            </tr>
                        </thead>
                        <tbody id="detailbody">     
                            @php $no=0; @endphp
                            @foreach($param['dataitem'] as $item)
                                @php 
                                    $no++;                                     
                                @endphp
                                @if($datahead->hitung_by=='1')
                                @php $jml_hrg = $item->n_qty_kg * $item->v_harga_sat; @endphp
                                <tr id="tr{{ $no }}">
                                    <td align="center"><input type="hidden" class="form-control form-control-sm" name="baris{{ $no }}" id="baris{{ $no }}" value="{{ $no }}"/>{{ $no }}</td>
                                    <td><div class="row row-error"><textarea type="text" class="form-control form-control-sm border-dark row-error col-sm-6" rows="1" name="uraian_pek_{{ $no }}" id="uraian_pek_{{ $no }}" maxlength="100" readonly>{{ $item->e_uraian_pekerjaan }}</textarea><input type="text" name="strike_off_{{ $no }}" id="strike_off_{{ $no }}" class="form-control form-control-sm border-dark col-sm-3 row-error" onclick="getDate(this);" value="{{date('d-m-Y', strtotime($item->d_strike_off)) }}" readonly/><select class="form-control form-control-sm border-dark col-sm-3 row-error" name="i_jns_kain_{{ $no }}" id="i_jns_kain_{{ $no }}" disabled> @if($jenis_kain) <option value="">Pilih Kain!</option> @foreach($jenis_kain as $kain) <option value="{{ $kain->i_id }}" @if($item->e_jenis_kain==$kain->i_id) selected @endif>{{ $kain->e_kain }}</option> @endforeach @else <option value="0" @if($item->e_jenis_kain=='0') selected @endif>Tidak Ada Kain</option> @endif</select><div id="error_uraian_pek_{{ $no }}" class="error"></div></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="roll_{{ $no }}" id="roll_{{ $no }}" style="text-align: right;" onkeyup="formatRp(document.getElementById('roll_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_roll,2,'.',',') }}" readonly/></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="yard_m_{{ $no }}" id="yard_m_{{ $no }}" style="text-align: right;" onkeyup="formatRp(document.getElementById('yard_m_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_pjg,2,'.',',') }}" readonly/></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="kg_{{ $no }}" id="kg_{{ $no }}" style="text-align: right;" onkeyup="hitungSatu('#kg_{{$no}}','#hrg_{{$no}}',{{$no}});formatRp(document.getElementById('kg_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_kg,2,'.',',') }}" readonly/><div id="error_kg_{{ $no }}" class="error"></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="hrg_{{ $no }}" id="hrg_{{ $no }}" style="text-align: right;" onkeyup="hitungSatu('#kg_{{$no}}',this,{{$no}});formatRp(document.getElementById('hrg_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->v_harga_sat,2,'.',',') }}" readonly/><div id="error_hrg_{{ $no }}" class="error"></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="jml_hrg_{{ $no }}" id="jml_hrg_{{ $no }}" style="text-align: right;" value="{{ number_format($jml_hrg) }}" readonly /></td>
                                </tr>
                                @elseif($datahead->hitung_by=='2')
                                @php $jml_hrg = $item->n_qty_roll * $item->v_harga_sat; @endphp
                                <tr id="tr{{ $no }}">
                                    <td align="center"><input type="hidden" class="form-control form-control-sm" name="baris{{ $no }}" id="baris{{ $no }}" value="{{ $no }}"/>{{ $no }}</td>
                                    <td><div class="row row-error"><textarea type="text" class="form-control form-control-sm border-dark row-error col-sm-6" rows="1" name="uraian_pek_{{ $no }}" id="uraian_pek_{{ $no }}" maxlength="100" readonly>{{ $item->e_uraian_pekerjaan }}</textarea><input type="text" name="strike_off_{{ $no }}" id="strike_off_{{ $no }}" class="form-control form-control-sm border-dark col-sm-3 row-error" onclick="getDate(this);" value="{{date('d-m-Y', strtotime($item->d_strike_off)) }}" readonly/><select class="form-control form-control-sm border-dark col-sm-3 row-error" name="i_jns_kain_{{ $no }}" id="i_jns_kain_{{ $no }}" disabled> @if($jenis_kain) <option value="">Pilih Kain!</option> @foreach($jenis_kain as $kain) <option value="{{ $kain->i_id }}" @if($item->e_jenis_kain==$kain->i_id) selected @endif>{{ $kain->e_kain }}</option> @endforeach @else <option value="0" @if($item->e_jenis_kain=='0') selected @endif>Tidak Ada Kain</option> @endif</select><div id="error_uraian_pek_{{ $no }}" class="error"></div></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="roll_{{ $no }}" id="roll_{{ $no }}" style="text-align: right;" onkeyup="hitungSatu('#roll_{{$no}}','#hrg_{{$no}}',{{$no}});formatRp(document.getElementById('roll_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_roll,2,'.',',') }}" readonly/></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="yard_m_{{ $no }}" id="yard_m_{{ $no }}" style="text-align: right;" onkeyup="formatRp(document.getElementById('yard_m_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_pjg,2,'.',',') }}" readonly/></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="kg_{{ $no }}" id="kg_{{ $no }}" style="text-align: right;" onkeyup="formatRp(document.getElementById('kg_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_kg,2,'.',',') }}" readonly/><div id="error_kg_{{ $no }}" class="error"></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="hrg_{{ $no }}" id="hrg_{{ $no }}" style="text-align: right;" onkeyup="hitungSatu('#roll_{{$no}}',this,{{$no}});formatRp(document.getElementById('hrg_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->v_harga_sat,2,'.',',') }}" readonly/><div id="error_hrg_{{ $no }}" class="error"></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="jml_hrg_{{ $no }}" id="jml_hrg_{{ $no }}" style="text-align: right;" value="{{ number_format($jml_hrg) }}" readonly /></td>
                                </tr>
                                @else
                                @php $jml_hrg = $item->n_qty_pjg * $item->v_harga_sat; @endphp
                                <tr id="tr{{ $no }}">
                                    <td align="center"><input type="hidden" class="form-control form-control-sm" name="baris{{ $no }}" id="baris{{ $no }}" value="{{ $no }}"/>{{ $no }}</td>
                                    <td><div class="row row-error"><textarea type="text" class="form-control form-control-sm border-dark row-error col-sm-6" rows="1" name="uraian_pek_{{ $no }}" id="uraian_pek_{{ $no }}" maxlength="100">{{ $item->e_uraian_pekerjaan }}</textarea><input type="text" name="strike_off_{{ $no }}" id="strike_off_{{ $no }}" class="form-control form-control-sm border-dark col-sm-3 row-error" onclick="getDate(this);" value="{{date('d-m-Y', strtotime($item->d_strike_off)) }}" readonly/><select class="form-control form-control-sm border-dark col-sm-3 row-error" name="i_jns_kain_{{ $no }}" id="i_jns_kain_{{ $no }}"> @if($jenis_kain) <option value="">Pilih Kain!</option> @foreach($jenis_kain as $kain) <option value="{{ $kain->i_id }}" @if($item->e_jenis_kain==$kain->i_id) selected @endif>{{ $kain->e_kain }}</option> @endforeach @else <option value="0" @if($item->e_jenis_kain=='0') selected @endif>Tidak Ada Kain</option> @endif</select><div id="error_uraian_pek_{{ $no }}" class="error"></div></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="roll_{{ $no }}" id="roll_{{ $no }}" style="text-align: right;" onkeyup="formatRp(document.getElementById('roll_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_roll,2,'.',',') }}" /></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="yard_m_{{ $no }}" id="yard_m_{{ $no }}" style="text-align: right;" onkeyup="hitungSatu('#yard_m_{{$no}}','#hrg_{{$no}}',{{$no}});formatRp(document.getElementById('yard_m_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_pjg,2,'.',',') }}" /></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="kg_{{ $no }}" id="kg_{{ $no }}" style="text-align: right;" onkeyup="formatRp(document.getElementById('kg_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->n_qty_kg,2,'.',',') }}" /><div id="error_kg_{{ $no }}" class="error"></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="hrg_{{ $no }}" id="hrg_{{ $no }}" style="text-align: right;" onkeyup="hitungSatu('#yard_m_{{$no}}',this,{{$no}});formatRp(document.getElementById('hrg_{{ $no }}'));" onkeydown="hanyaangka();" value="{{ number_format($item->v_harga_sat,2,'.',',') }}"/><div id="error_hrg_{{ $no }}" class="error"></div></td>
                                    <td><input type="text" class="form-control form-control-sm border-dark" name="jml_hrg_{{ $no }}" id="jml_hrg_{{ $no }}" style="text-align: right;" value="{{ number_format($jml_hrg) }}" readonly /></td>
                                </tr>
                                @endif
                            @endforeach                   
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Total Harga =</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="total_harga" id="total_harga" value="{{ number_format($datahead->v_pekerjaan) }}" disabled/></td>
                            </tr>
                            <tr>                              
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">PPN = <input type="text" style="text-align:right;width:20%;" class="" name="down_payment_percent" id="down_payment_percent" onkeyup="hitungPPN();" onkeydown="hanyaangka();" value="{{ $datahead->n_ppn }}" disabled/>%</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="ppn" id="ppn" value="{{ number_format($datahead->v_ppn) }}" disabled/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Harga Pekerjaan =</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="hrg_pekerjaan" id="hrg_pekerjaan" value="{{ number_format($datahead->v_pekerjaan_plus_ppn) }}" disabled/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Down Payment = <input type="text" style="text-align:right;width:20%;" class="" name="down_payment_percent" id="down_payment_percent" onkeyup="hitungDownPayment();" onkeydown="hanyaangka();" value="{{ $datahead->n_discount }}" disabled/>%</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="down_payment_nilai" id="down_payment_nilai" value="{{ number_format($datahead->v_discount) }}" disabled/></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border:0;"></td>
                                <td style="text-align:right;">Sisa Pembayaran =</td>
                                <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="sisa_pembayaran" id="sisa_pembayaran" value="{{ number_format($datahead->v_sisa) }}" disabled/></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">D. PENYELESAIAN PEKERJAAN</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">D1. PERSETUJUAN STRIKE OFF:</label>                            
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="tgl_strike_off"
                                    id="tgl_strike_off" value="{{ date('d-m-Y', strtotime($datahead->d_strike_off)) }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">D2. PERSETUJUAN MENYELURUH:</label>                            
                            <div class="col-sm-6"> 
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="approval_strike_off"
                                    id="approval_strike_off" value="{{ date('d-m-Y', strtotime($datahead->d_approval_strike_off)) }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">D3. PENYERAHAN BARANG:</label>                            
                            <div class="col-sm-6">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="tgl_penyerahan"
                                    id="tgl_penyerahan" value="{{ date('d-m-Y', strtotime($datahead->d_penyerahan_brg)) }}" disabled/>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">E. KETERANGAN KHUSUS</h4>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-dark">KETERANGAN</label>                            
                            <div class="col-sm-6">                                
                                <textarea type="text" class="form-control border-dark" name="ket_penyerahan"
                                    id="ket_penyerahan" rows="5" disabled>{{ $datahead->e_keterangan_kirim }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach
@endsection