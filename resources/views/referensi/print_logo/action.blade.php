<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(function() {
    $('.file-upload-browse').on('click', function() {
        var file = $(this).parent().parent().parent().find('.file-upload-default'); 
        file.trigger('click');       
    });
    $('.file-upload-default').on('change', function() {
      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
});       
var table = $('#datatable1').DataTable();

function save() {
    $('#form_print_logo').validate({
        rules: {
            img: {
                required: !0,
                extension: "png",
                maxsize: 5242880
            }
        },
        messages: {
            img: {
                extension: "Please enter a value with a valid extension. ex: png, jpg, jpeg",
                maxsize: "File size must not exceed 5MB"
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.insertAfter(element);            
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){                                
            var form_rfp = $('#form_print_logo');
            var _form_data = new FormData(form_rfp[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/print_logo/save',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('referensi.print_logo') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}

function aktif(id) {
    swal({   
        title: "Anda Yakin?",   
        text: "Aktifkan Logo Ini?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "POST",
                url: base_url + '/referensi/print_logo/aktif',
                data: { id: id },                
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal({   
                        title: "Sukses",   
                        text: data.msg,   
                        type: "success",                       
                        showCancelButton: false,   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Ya",                     
                        closeOnConfirm: true,                                       
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){   
                        if(isConfirm){
                            loadNewPage('{{ route('referensi.print_logo') }}');
                        }
                    });
                } else {
                    swal('Gagal',data.msg,'error');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });                        
        } else {     
            swal("Batal","", "error");   
        } 
    });
}

function nonAktif(id) {
    swal({   
        title: "Anda Yakin?",   
        text: "Non Aktifkan Logo Ini?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "POST",
                url: base_url + '/referensi/print_logo/non_aktif',
                data: { id: id },                
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal({   
                        title: "Sukses",   
                        text: data.msg,   
                        type: "success",                       
                        showCancelButton: false,   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Ya",                     
                        closeOnConfirm: true,                                       
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){   
                        if(isConfirm){
                            loadNewPage('{{ route('referensi.print_logo') }}');
                        }
                    });
                } else {
                    swal('Gagal',data.msg,'error');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });                        
        } else {     
            swal("Batal","", "error");   
        } 
    });
}
</script>