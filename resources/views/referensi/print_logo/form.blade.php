@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_print_logo" enctype="multipart/form-data">
    @csrf
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Add Print Logo</h4>                
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>File upload</label>                            
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                            <input type="file" name="img" class="file-upload-default">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" onclick="save()" class="btn btn-success btn-icon-text">
                            <i class="mdi mdi-content-save btn-icon-prepend"></i>
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('referensi.print_logo.action')
@endsection