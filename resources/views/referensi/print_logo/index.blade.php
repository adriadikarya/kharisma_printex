@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Print Logo SO </h4>
            <p class="card-description">
                <button type="button" onclick="loadNewPage('{{ route('referensi.form_print_logo') }}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add Data
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable1">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Action</th>
                            <th style="text-align:center;">Number</th>
                            <th style="text-align:center;">Print Logo SO</th> 
                            <th style="text-align:center;">Status Active</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($data)
                        @php $no=1; $head=""; $row=0; @endphp
                            @foreach($data as $item)                          
                            <tr>
                                <td align="center">                                    
                                    @if($item->f_active==false)
                                        <a class="btn btn-sm btn-success" style="cursor: pointer;" onclick="aktif('{{$item->i_id}}')"><i class="mdi mdi-clipboard-check"></i> Active</a>
                                    @else 
                                        <a class="btn btn-sm btn-danger" style="cursor: pointer;" onclick="nonAktif('{{$item->i_id}}')"><i class="mdi mdi-clipboard-check"></i> Non-Active</a>
                                    @endif                                        
                                </td>                                
                                <td align="center">{{$no++}}</td>
                                <td align="center"><img class="img-fluid" src="{{ url('images/logo_so/'.$item->logo_name) }}"/></td>
                                <td style="word-wrap:break-word;">
                                    @if($item->f_active=='t')
                                        Active
                                    @else
                                        Non Active
                                    @endif
                                </td>
                            </tr>                                                
                            @endforeach                            
                        @else
                            <tr>
                                <td colspan="4" align="center">Sorry Your Data Doesn't Exist</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('referensi.print_logo.action')
@endsection