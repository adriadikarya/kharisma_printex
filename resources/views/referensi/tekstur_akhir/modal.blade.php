<div class="modal fade" id="_modal_tekstur_akhir" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Final Texture</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_tekstur_akhir">
            @csrf
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name Final Texture</label>
                        <div class="col-sm-8">                                                         
                            <input type="hidden" name="id_tekstur_akhir" id="id_tekstur_akhir" value=""/>                        
                            <input type="text" class="form-control form-control-sm border-dark" name="nama_tekstur_akhir" id="nama_tekstur_akhir" value="" autocomplete="off"/>
                        </div>                        
                    </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-success" onclick="insert($('#id_tekstur_akhir').val())">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>