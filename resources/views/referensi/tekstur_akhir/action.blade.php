<script>
var act_url = '{{ route('data_tekstur_akhir') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', className: 'text-center' },
        { data: 'e_tekstur', name: 'e_tekstur', className: 'text-center' },                      
    ]
});
var _modal_user = $('#_modal_tekstur_akhir');
function tambah()
{
    var form_rfp = $('#form_tekstur_akhir');
    $('#id_tekstur_akhir').val('');
    form_rfp[0].reset();
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function insert(id)
{    
    // var tujuan = id==''?'simpan':'update';
    $('#form_tekstur_akhir').validate({
        rules: {
            nama_tekstur_akhir: {
                required: !0,                
                maxlength: 100 
            },             
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_tekstur_akhir');
            var _form_data = new FormData(form_rfp[0]);                        

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/tekstur_akhir/simpan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal('Berhasil',data.msg,'success');
                            form_rfp[0].reset();
                            _modal_user.modal('hide');
                            reload();
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}

function reload()
{
    table.ajax.reload();
}

function edit(id)
{
    var form_rfp = $('#form_tekstur_akhir');
    $.ajax({
        type: "GET",
        url: base_url + '/referensi/tekstur_akhir/edit/' + id,
        beforeSend: function() {
            $('#loading').show();
            form_rfp[0].reset();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);                
        $.each(data, function(k,v){
            $('#id_tekstur_akhir').val(v.i_id);
            $('#nama_tekstur_akhir').val(v.e_tekstur);            
        });                    
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function hapus(id)
{
    var form_rfp = $('#form_tekstur_akhir');
    swal({   
        title: "Are You Sure?",   
        text: "Delete This Data?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: base_url + '/referensi/tekstur_akhir/hapus/' + id,
                beforeSend: function() {
                    $('#loading').show();
                    form_rfp[0].reset();
                },
                success: function(res) {

                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);                
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal('Berhasil',data.msg,'success');                                
                    reload();
                } else {
                    swal('Gagal',data.msg,'error');
                }                    
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal("Batal","", "error");
        }
    });
}
</script>