<script>
var act_url = '{{ route('data_so_flag') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', className: 'text-center' },
        { data: 'flag_name', name: 'flag_name', className: 'text-center' },                      
    ]
});
var _modal_user = $('#_modal_so_flag');
function tambah()
{
    var form_rfp = $('#form_so_flag');
    $('#id_so_flag').val('');
    form_rfp[0].reset();
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function insert(id)
{    
    // var tujuan = id==''?'simpan':'update';
    $('#form_so_flag').validate({
        rules: {
            nama_so_flag: {
                required: !0,                
                maxlength: 100 
            },             
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_so_flag');
            var _form_data = new FormData(form_rfp[0]);                        

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/so_flag/simpan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal('Berhasil',data.msg,'success');
                            form_rfp[0].reset();
                            _modal_user.modal('hide');
                            reload();
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}

function reload()
{
    table.ajax.reload();
}

function edit(id)
{
    var form_rfp = $('#form_so_flag');
    $.ajax({
        type: "GET",
        url: base_url + '/referensi/so_flag/edit/' + id,
        beforeSend: function() {
            $('#loading').show();
            form_rfp[0].reset();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);                
        $.each(data, function(k,v){
            $('#id_so_flag').val(v.i_id);
            $('#nama_so_flag').val(v.flag_name);            
        });                    
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function hapus(id)
{
    var form_rfp = $('#form_so_flag');
    swal({   
        title: "Are You Sure?",   
        text: "Delete This Data?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: base_url + '/referensi/so_flag/hapus/' + id,
                beforeSend: function() {
                    $('#loading').show();
                    form_rfp[0].reset();
                },
                success: function(res) {

                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);                
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal('Berhasil',data.msg,'success');                                
                    reload();
                } else {
                    swal('Gagal',data.msg,'error');
                }                    
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal("Batal","", "error");
        }
    });
}
</script>