@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List SO FLAG </h4>
            <p class="card-description">
                <button type="button" onclick="tambah()" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add SO FLAG
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Action</th>
                            <th style="text-align:center;">SO FLAG</th>                                                                                           
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('referensi.so_flag.modal')
@include('referensi.so_flag.action')
@endsection