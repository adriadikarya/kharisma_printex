@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@php
function toNum($data) {
    $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
    echo $alpha[$data];
}
@endphp
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_edit_ket_umum">
    @csrf
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Edit General Requirements</h4>
                <button type="submit" onclick="update()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-content-save btn-icon-prepend"></i>
                    Update
                </button>
                <button type="button" onclick="back()" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Back
                </button>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Parent General Requirements :</label>
                            <div class="col-sm-6 grid-margin">
                                <input type="hidden" name="idhead" id="idhead" value="{{$param['headid']}}"/>
                                <textarea class="form-control border-dark" maxlength="250" name="ket_umum" id="ket_umum" style="height:100px;">{{$param['head']}}</textarea>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Child General Requirements</h4>
                <input type="hidden" id="totrow" name="totrow" @if($param['det']) value="{{count($param['det'])}}" @else value="0" @endif>
                <button type="button" onclick="addChild()" class="btn btn-info btn-icon-text">
                    <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                    Add Child
                </button>
                <button type="button" onclick="deleteChild()" class="btn btn-danger btn-icon-text">
                    <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                    Remove Child
                </button>
                <div class="table-responsive pt-1">
                    <table class="table table-bordered" id="table-harga">
                        <thead style="background-color: lightcyan;">
                            <tr>
                                <th style="text-align:center;">Order</th>
                                <th style="text-align:center;">Child General Requirements</th>
                            </tr>                            
                        </thead>
                        <tbody id="detailbody">
                        @if($param['det'])
                            @foreach($param['det'] as $key => $det)                    
                                <tr id="tr{{$key+1}}">
                                    <td align="center"><input type="hidden" class="form-control form-control-sm" name="baris{{$key+1}}" id="baris{{$key+1}}" value="{{$key+1}}"/>{{toNum($key)}}</td>
                                    <td><textarea type="text" class="form-control form-control-sm border-dark" name="child{{$key+1}}" id="child{{$key+1}}" maxlength="250" style="word-warp:break-word">{{$det->e_child_ket_umum}}</textarea></div></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
@include('referensi.ket_umum.action')
@endsection