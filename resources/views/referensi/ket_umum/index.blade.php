@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List General Requirements</h4>
            <p class="card-description">
                <button type="button" onclick="loadNewPage('{{ route('referensi.form_ket_umum') }}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add Data
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable1">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Action</th>
                            <th style="text-align:center;">Number</th>
                            <th style="text-align:center;">General Requirements</th> 
                            <th style="text-align:center;">Total Child</th>                                                        
                        </tr>
                    </thead>
                    <tbody>
                        @if($data)
                        @php $no=1; $head=""; $row=0; @endphp
                            @foreach($data as $item)                          
                            <tr>
                                <td align="center">
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                                        <i class="mdi mdi-apps"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                                        @if($item->total_child>0)
                                            <a class="dropdown-item" style="cursor: pointer;" onclick="detail('{{$item->id_head}}','{{$item->e_ket_umum}}')"><i class="mdi mdi-clipboard-check"></i> Detail Child</a>
                                            <div class="dropdown-divider"></div>
                                        @endif
                                            <a class="dropdown-item" style="cursor: pointer;" onclick="edit('{{$item->id_head}}')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                                        </div>
                                    </div>
                                </td>                                
                                <td align="center">{{$no++}}</td>
                                <td style="word-wrap:break-word;">{{$item->e_ket_umum}}</td>
                                <td style="word-wrap:break-word;">{{$item->total_child}}</td>
                            </tr>                                                
                            @endforeach                            
                        @else
                            <tr>
                                <td colspan="4" align="center">Sorry Your Data Doesn't Exist</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('referensi.ket_umum.modal')
@include('referensi.ket_umum.action')
@endsection