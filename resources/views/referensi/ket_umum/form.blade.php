@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_ket_umum">
    @csrf
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Add General Requirements</h4>
                <button type="submit" onclick="save()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-content-save btn-icon-prepend"></i>
                    Save
                </button>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Parent General Requirements :</label>
                            <div class="col-sm-6 grid-margin">
                                <textarea class="form-control border-dark" maxlength="250" name="ket_umum" id="ket_umum" style="height:100px;"></textarea>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Child General Requirements</h4>
                <input type="hidden" id="totrow" name="totrow" value="0">
                <button type="button" onclick="addChild()" class="btn btn-info btn-icon-text">
                    <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                    Add Child
                </button>
                <button type="button" onclick="deleteChild()" class="btn btn-danger btn-icon-text">
                    <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                    Remove Child
                </button>
                <div class="table-responsive pt-1">
                    <table class="table table-bordered" id="table-harga">
                        <thead style="background-color: lightcyan;">
                            <tr>
                                <th style="text-align:center;">Order</th>
                                <th style="text-align:center;">Child General Requirements</th>
                            </tr>                            
                        </thead>
                        <tbody id="detailbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
@include('referensi.ket_umum.action')
@endsection