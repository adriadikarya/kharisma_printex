<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 
var table = $('#datatable1').DataTable();

function toLetters(num) {
    "use strict";
    var mod = num % 26;
    var pow = num / 26 | 0;
    var out = mod ? String.fromCharCode(64 + mod) : (pow--, 'Z');
    return pow ? toLetters(pow) + out : out;
}

function addChild()
{
    var counter = parseInt($('#totrow').val())+1;
    var huruf = toLetters(counter);
    var cols = "";
    cols += '<tr id="tr'+counter+'">';
    cols += '<td align="center"><input type="hidden" class="form-control form-control-sm" name="baris' + counter + '" id="baris' + counter + '" value="'+counter+'"/>'+huruf+'</td>';    
    cols += '<td><textarea type="text" class="form-control form-control-sm border-dark" name="child'+counter+'" id="child'+counter+'" maxlength="250" style="word-warp:break-word"/></textarea></div></td>';    
    cols += '</tr>';
    $("#detailbody").append(cols);    
    $('#totrow').val(counter);
}

function deleteChild()
{
    var counter = parseInt($('#totrow').val());
    if(counter>0)
    {
        $('#tr'+counter).remove();       
        counter -= 1;
        $('#totrow').val(counter);	
    } else {
        $('#tr'+counter).remove();
        $('#totrow').val(0);   
    }
}

function save()
{
    $('#form_ket_umum').validate({
        rules: {
            ket_umum: {
                required: !0,
                maxlength: 250  
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_ket_umum');
            var _form_data = new FormData(form_rfp[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/ket_umum/save',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('referensi.ket_umum') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
    $('textarea[id^="child"]').each(function (k,v) {   
        console.log(k)     
        $(this).rules('add', {
            required: !0,            
            maxlength: 250,            
        });
    });
}

function update()
{
    $('#form_edit_ket_umum').validate({
        rules: {
            ket_umum: {
                required: !0,
                maxlength: 250  
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_edit_ket_umum');
            var _form_data = new FormData(form_rfp[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/ket_umum/update',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('referensi.ket_umum') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
    $('textarea[id^="child"]').each(function (k,v) {   
        console.log(k)     
        $(this).rules('add', {
            required: !0,            
            maxlength: 250,            
        });
    });
}


function detail(id, e_ket_umum)
{
    var _modal = $('#modal');
    $('#ket_umum').val(e_ket_umum);
    var textarea = "";
    $.ajax({
        type: "GET",
        url: base_url + '/referensi/child/' + id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);        
        console.log(data)
        $.each(data, function(k,v){
            textarea += (k+1) + '. '+ v.e_child_ket_umum +'\n';
        });            
        $('#child').html(textarea);
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _modal.modal({backdrop: 'static', keyboard: false});
}

function edit(id)
{
    loadNewPage(base_url + '/referensi/ket_umum/edit/' +id);
}

function back()
{
    loadNewPage(base_url + '/referensi/ket_umum');
}
</script>