<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Child General Requirements</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_cw">
            <div class="modal-body">                
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label">Head</label>
                        <div class="col-sm-12">   
                            <textarea class="form-control border-dark" maxlength="250" name="ket_umum" id="ket_umum" style="height:100px;" readonly></textarea>                           
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-form-label">Child</label>
                        <div class="col-sm-12">
                            <textarea class="form-control border-dark" maxlength="250" name="child" id="child" style="height:200px;font-size:15px;" readonly><b></b>
                            </textarea>                           
                        </div>                        
                    </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>                
            </div>
            </form>
        </div>
    </div>
</div>