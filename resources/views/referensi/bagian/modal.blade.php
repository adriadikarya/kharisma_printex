<div class="modal fade" id="_modal_bagian" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Workstation Reference</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_bagian">
            @csrf
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Workstation Name</label>
                        <div class="col-sm-8">                                                         
                            <input type="hidden" name="id_bagian" id="id_bagian" value=""/>                        
                            <input type="text" class="form-control form-control-sm border-dark" name="nama_bagian" id="nama_bagian" value="" autocomplete="off"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Section Person 1</label>
                        <div class="col-sm-8">
                            <select name="penanggung_jawab" id="penanggung_jawab" class="form-control">
                                <option value="">Pilih User Bagian</option>
                                @if($user)
                                    @foreach($user as $us)
                                        <option value="{{ $us->id }}">{{ $us->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>                        
                    </div>               
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Section Person 2</label>
                        <div class="col-sm-8">
                            <select name="penanggung_jawab2" id="penanggung_jawab2" class="form-control">
                                <option value="">Pilih User Bagian</option>
                                @if($user)
                                    @foreach($user as $us)
                                        <option value="{{ $us->id }}">{{ $us->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>                        
                    </div>               
                    {{-- <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Wajib Isi</label>
                        <div class="col-sm-8">
                            <select name="wajib_isi" id="wajib_isi" class="form-control">
                                <option value="">Pilih</option>                               
                                <option value="t">Ya</option>                               
                                <option value="f">Tidak</option>                               
                            </select>
                        </div>                        
                    </div> --}}               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-success" onclick="insert($('#id_bagian').val())">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>