<script>
var act_url = '{{ route('data_bagian') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', className: 'text-center' },
        { data: 'nama_bagian', name: 'nama_bagian', className: 'text-center' },              
        { data: 'name', name: 'name', className: 'text-center' },
        { data: 'name2', name: 'name2', className: 'text-center' },
        { data: 'last_workstation', name: 'last_workstation', className: 'text-center' },
        // { data: 'wajib_isi', name: 'wajib_isi', className: 'text-center' },
    ]
});

var _modal_user = $('#_modal_bagian');
function tambah()
{
    var form_rfp = $('#form_bagian');
    $('#id_bagian').val('');
    form_rfp[0].reset();
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

$.validator.addMethod("user_not_same", function(value, element) {
   return $('#penanggung_jawab').val() != $('#penanggung_jawab2').val()
}, "Section person should not match");

function insert(id)
{    
    // var tujuan = id==''?'simpan':'update';
    $('#form_bagian').validate({
        rules: {
            nama_bagian: {
                required: !0,                
                maxlength: 50 
            },
            penanggung_jawab: {
                required: !0                
            },                
            penanggung_jawab2: {
                required: !0,
                user_not_same: !0,
            },
            /* wajib_isi: {
                required: !0,
            }  */ 
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_bagian');
            var _form_data = new FormData(form_rfp[0]);                        

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/bagian/simpan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal('Berhasil',data.msg,'success');
                            form_rfp[0].reset();
                            _modal_user.modal('hide');
                            reload();
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}

function reload()
{
    table.ajax.reload();
}

function edit(id)
{
    var form_rfp = $('#form_bagian');
    $.ajax({
        type: "GET",
        url: base_url + '/referensi/bagian/edit/' + id,
        beforeSend: function() {
            $('#loading').show();
            form_rfp[0].reset();
        },
        success: function(res) {

        },
    }).done(function(res){        
        $('#loading').hide();            
        var data = $.parseJSON(res);                
        console.log(data)
        $.each(data, function(k,v){
            $('#id_bagian').val(v.i_id);
            $('#nama_bagian').val(v.nama_bagian);
            $('#penanggung_jawab').val(v.e_penanggung_jawab);                        
            $('#penanggung_jawab2').val(v.e_penanggung_jawab2);                        
            // $('#wajib_isi').val(v.wajib_isi?'t':'f');                        
        });                    
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function hapus(id)
{
    var form_rfp = $('#form_bagian');
    swal({   
        title: "Are You Sure?",   
        text: "Delete This Data?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: base_url + '/referensi/bagian/hapus/' + id,
                beforeSend: function() {
                    $('#loading').show();
                    form_rfp[0].reset();
                },
                success: function(res) {

                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);                
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal('Berhasil',data.msg,'success');                                
                    reload();
                } else {
                    swal('Gagal',data.msg,'error');
                }                    
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal("Batal","", "error");
        }
    });
}

function makeLast(id)
{
    var form_rfp = $('#form_bagian');
    swal({   
        title: "Yakin?",   
        text: "Data ini akan jd Workstation Terakhir?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {
            $.ajax({
                type: "GET",
                url: base_url + '/referensi/bagian/make_last/' + id,
                beforeSend: function() {
                    $('#loading').show();
                    form_rfp[0].reset();
                },
                success: function(res) {

                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);                
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal('Berhasil',data.msg,'success');                                
                    reload();
                } else {
                    swal('Gagal',data.msg,'error');
                }                    
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal("Batal","", "error");
        }
    });
}
</script>