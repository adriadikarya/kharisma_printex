@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Workstation </h4>
            <p class="card-description">
                <button type="button" onclick="tambah()" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add Workstation
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Action</th>
                            <th style="text-align:center;">Workstation Name</th>                            
                            <th style="text-align:center;">Section Person 1</th>                                                        
                            <th style="text-align:center;">Section Person 2</th>                                                        
                            <th style="text-align:center;">Last Workstation</th>                                                        
                            {{-- <th style="text-align:center;">Required</th>                                                         --}}
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('referensi.bagian.modal')
@include('referensi.bagian.action')
@endsection