<div class="modal fade" id="_modal_jns_bahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Jenis Bahan</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_jns_bahan">
            @csrf
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Nama Jenis Bahan</label>
                        <div class="col-sm-8">                                                         
                            <input type="hidden" name="id_jns_bahan" id="id_jns_bahan" value=""/>                        
                            <input type="text" class="form-control form-control-sm border-dark" name="nama_jns_bahan" id="nama_jns_bahan" value="" autocomplete="off"/>
                        </div>                        
                    </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-success" onclick="insert($('#id_jns_bahan').val())">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>