<div class="modal fade" id="_modal_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input User</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_user">
            @csrf
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">User Name</label>
                        <div class="col-sm-8">                                                         
                            <input type="hidden" name="id_user" id="id_user" value=""/>                        
                            <input type="text" class="form-control form-control-sm border-dark" name="username" id="username" value="" autocomplete="off"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm border-dark" name="name" id="name" value=""/>                            
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Role</label>
                        <div class="col-sm-8">                                                        
                            <select id="role" name="role" class="form-control form-control-sm border-dark" >
                                <option value="">Pilih</option>
                                @if($role)
                                    @foreach($role as $rol)
                                        <option value="{{$rol->i_id}}">{{$rol->e_role_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Aktif</label>
                        <div class="col-sm-8">                                                        
                            <select id="active" name="active" class="form-control form-control-sm border-dark" >
                                <option value="t">Aktif</option>
                                <option value="f">Tidak Aktif</option>
                            </select>
                        </div>                        
                    </div>                    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-success" onclick="insert($('#id_user').val())">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>