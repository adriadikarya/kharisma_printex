<script>
var act_url = '{{ route('data_user') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'username', name: 'username' },              
        { data: 'name', name: 'name' },
        { data: 'e_role_name', name: 'e_role_name' },        
        { data: 'login_pertama', name: 'login_pertama' },
        { data: 'is_active', name: 'is_active' },
        { data: 'count_reset_password', name: 'count_reset_password' },        
        { data: 'status', name: 'status' },
    ]
});
var _modal_user = $('#_modal_user');
function tambah()
{
    var form_rfp = $('#form_user');
    form_rfp[0].reset();
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function edit(id)
{
    $.ajax({
        type: "GET",
        url: base_url + '/referensi/user/edit/' + id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);                
        $.each(data, function(k,v){
            $('#id_user').val(v.id);
            $('#username').val(v.username);
            $('#role').val(v.role);
            $('#name').val(v.name);
            if(v.is_active){
                $('#active').val('t');
            } else {
                $('#active').val('f');
            }               
        });                    
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _modal_user.modal({backdrop: 'static', keyboard: false});
}

function reset(id)
{
    var form_rfp = $('#form_user');
    var _form_data = new FormData(form_rfp[0]);                        
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Reset Password?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/referensi/user/reset_password/' + id,                                
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal('Berhasil',data.msg,'success');
                    form_rfp[0].reset();
                    _modal_user.modal('hide');
                    reload();
                } else {
                    swal('Gagal',data.msg,'error');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });                        
        } else {     
            swal("Batal","", "error");   
        } 
    });
}

function insert(id)
{    
    // var tujuan = id==''?'simpan':'update';
    $('#form_user').validate({
        rules: {
            username: {
                required: !0,
                nowhitespace: !0,
				userName: !0,
                maxlength: 250 
            },
            name: {
                required: !0,
                maxlength: 250
            },
            role : {
                required: !0
            },            
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_user');
            var _form_data = new FormData(form_rfp[0]);                        

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/user/simpan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal('Berhasil',data.msg,'success');
                            form_rfp[0].reset();
                            _modal_user.modal('hide');
                            reload();
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}

function reload()
{
    table.ajax.reload();
}
</script>