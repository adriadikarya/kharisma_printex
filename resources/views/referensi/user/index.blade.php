@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List User </h4>
            <p class="card-description">
                <button type="button" onclick="tambah()" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add User
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Action</th>
                            <th style="text-align:center;">User Name</th>                            
                            <th style="text-align:center;">Name</th>                            
                            <th style="text-align:center;">Role</th>
                            <th style="text-align:center;">Login Pertama</th> 
                            <th style="text-align:center;">Aktif</th>
                            <th style="text-align:center;">Hitung Reset</th>
                            <th style="text-align:center;">Status</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('referensi.user.modal')
@include('referensi.user.action')
@endsection