<script>
var act_url = '{{ route('data_hak_akses') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', className: 'text-center' },
        { data: 'e_role_name', name: 'e_role_name', className: 'text-center' },                      
    ]
});

function tambah()
{
    loadNewPage(base_url + '/tambah_hak_akses');
}

function prosesSimpan()
{            
    // console.log(_form_data)
    var result = new Array();    
    $('input[id^="item"]').each(function() {
        var input = $(this);        
        if (input.prop("checked") === true) {            
            result.push(input.val());
        }
    })
    if(result.length>0){    
        $('#form_hak_akses').validate({
            rules: {
                role: {
                    required: !0,                                    
                },                 
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');
                if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                    label.insertAfter(element.next('.select2-container'));
                } else {
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger').removeClass('has-success')
                $(element).addClass('form-control-danger').removeClass('form-control-success')
            },
            unhighlight: function (element, errorClass) {
                $(element).parent().addClass('has-success').removeClass('has-danger')
                $(element).addClass('form-control-success').removeClass('form-control-danger')
            },
            submitHandler: function(e){
                var form_rfp = document.getElementById('form_hak_akses');
                var _form_data = new FormData(form_rfp);                                           
                swal({   
                    title: "Anda Yakin?",   
                    text: "Yakin Hak Akses Sudah Benar Semua?",   
                    type: "info",                       
                    showCancelButton: true,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    cancelButtonText: "Tidak",   
                    closeOnConfirm: false,   
                    closeOnCancel: false,
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        $.ajax({
                            type: "POST",
                            url: base_url + '/simpan_hak_akses',
                            data: _form_data,                            
                            processData: false,                            
                            contentType: false,                    
                            beforeSend: function() {
                                $('#loading').show();
                            },
                            success: function(res) {
                                
                            },
                        }).done(function(res){
                            $('#loading').hide();
                            var data = $.parseJSON(res);
                            if(data.code==1){
                                // swal('Berhasil',data.msg,'success');
                                swal({   
                                    title: "Berhasil",   
                                    text: data.msg,   
                                    type: "success",                       
                                    showCancelButton: false,   
                                    confirmButtonColor: "#e6b034",   
                                    confirmButtonText: "Ya",                     
                                    cancelButtonText: "Tidak",   
                                    closeOnConfirm: true,   
                                    closeOnCancel: false,
                                    showLoaderOnConfirm: true 
                                }, function(isConfirm){   
                                    if (isConfirm) {                                                                                                
                                        loadNewPage('{{route('referensi.hak_akses')}}');
                                    }
                                });
                            } else {
                                swal('Gagal',data.msg,'error');
                            }
                        }).fail(function(xhr,textStatus,errorThrown){
                            $('#loading').hide();
                            swal(textStatus,errorThrown,'error');
                        });                        
                    } else {     
                        swal("Batal","Hak Akses Tidak Jadi Disimpan", "error");   
                    } 
                });
            }
        });
    } else {
        swal('Informasi','Minimal 1 Menu Harus Dipilih','info');
        return false;
    }
}

function prosesUpdate()
{
    var result = new Array();    
    $('input[id^="item"]').each(function() {
        var input = $(this);        
        if (input.prop("checked") === true) {            
            result.push(input.val());
        }
    })
    if(result.length>0){    
        $('#form_hak_akses').validate({
            rules: {
                role: {
                    required: !0,                                    
                },                 
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');
                if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                    label.insertAfter(element.next('.select2-container'));
                } else {
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger').removeClass('has-success')
                $(element).addClass('form-control-danger').removeClass('form-control-success')
            },
            unhighlight: function (element, errorClass) {
                $(element).parent().addClass('has-success').removeClass('has-danger')
                $(element).addClass('form-control-success').removeClass('form-control-danger')
            },
            submitHandler: function(e){
                var form_rfp = document.getElementById('form_hak_akses');
                var _form_data = new FormData(form_rfp);                                           
                swal({   
                    title: "Anda Yakin?",   
                    text: "Yakin Hak Akses Sudah Benar Semua?",   
                    type: "info",                       
                    showCancelButton: true,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    cancelButtonText: "Tidak",   
                    closeOnConfirm: false,   
                    closeOnCancel: false,
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        $.ajax({
                            type: "POST",
                            url: base_url + '/update_hak_akses',
                            data: _form_data,                            
                            processData: false,                            
                            contentType: false,                    
                            beforeSend: function() {
                                $('#loading').show();
                            },
                            success: function(res) {
                                
                            },
                        }).done(function(res){
                            $('#loading').hide();
                            var data = $.parseJSON(res);
                            if(data.code==1){
                                // swal('Berhasil',data.msg,'success');
                                swal({   
                                    title: "Berhasil",   
                                    text: data.msg,   
                                    type: "success",                       
                                    showCancelButton: false,   
                                    confirmButtonColor: "#e6b034",   
                                    confirmButtonText: "Ya",                     
                                    cancelButtonText: "Tidak",   
                                    closeOnConfirm: true,   
                                    closeOnCancel: false,
                                    showLoaderOnConfirm: true 
                                }, function(isConfirm){   
                                    if (isConfirm) {                                                                                                
                                        loadNewPage('{{route('referensi.hak_akses')}}');
                                    }
                                });
                            } else {
                                swal('Gagal',data.msg,'error');
                            }
                        }).fail(function(xhr,textStatus,errorThrown){
                            $('#loading').hide();
                            swal(textStatus,errorThrown,'error');
                        });                        
                    } else {     
                        swal("Batal","Hak Akses Tidak Jadi Disimpan", "error");   
                    } 
                });
            }
        });
    } else {
        swal('Informasi','Minimal 1 Menu Harus Dipilih','info');
        return false;
    }
}

function hapus(roleid)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Hak Akses Akan Di Hapus?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "POST",
                url: base_url + '/hapus_hak_akses',
                data: {roleid: roleid},                                            
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();
                var data = $.parseJSON(res);
                if(data.code==1){
                    // swal('Berhasil',data.msg,'success');
                    swal({   
                        title: "Berhasil",   
                        text: data.msg,   
                        type: "success",                       
                        showCancelButton: false,   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Ya",                     
                        cancelButtonText: "Tidak",   
                        closeOnConfirm: true,   
                        closeOnCancel: false,
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){   
                        if (isConfirm) {                                                                                                
                            loadNewPage('{{route('referensi.hak_akses')}}');
                        }
                    });
                } else {
                    swal('Gagal',data.msg,'error');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });                        
        } else {     
            swal("Batal","Hak Akses Tidak Jadi Disimpan", "error");   
        } 
    });
}

function edit(roleid)
{
    loadNewPage(base_url + '/edit_hak_akses/' + roleid);
}
</script>