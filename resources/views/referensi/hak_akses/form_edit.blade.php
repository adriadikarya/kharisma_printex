@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Hak Akses Role</h4>
            <form class="form" id="form_hak_akses">
                @csrf            
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Role</label>
                            <div class="col-sm-8">
                                <select name="role" id="role" class="form-control form-control-sm" readonly>
                                @if($param['role'])                                    
                                    @foreach($param['role'] as $role)
                                        <option value="{{$role->i_id}}">{{$role->e_role_name}}</option>
                                    @endforeach
                                @else
                                    <option value="">Pilih Role!</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><u>List Menu</u></h4>
                                <div class="row">
                                    @if($param['data'])
                                    <input type="hidden" name="totalhead" value="{{count($param['data'])}}">
                                    @foreach($param['data'] as $key => $head)
                                    @php 
                                        if($head['head']->stand_alone){
                                            $total = 1;
                                        } else {
                                            $total = count($head['item']); 
                                        }                                        
                                    @endphp
                                        <div class="col-md-3 grid-margin stretch-card">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title"><input type="hidden" name="head{{$key+1}}" id="head{{$key+1}}" value="{{$head['head']->i_id}}"><input type="hidden" name="tothead{{$key+1}}" id="tothead{{$key+1}}" value="{{$total}}">{{$head['head']->nama_menu}}</h4>
                                                    <div class="form_group">
                                                        @if($head['head']->stand_alone)
                                                            <div class="form-check form-check-primary">
                                                                <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" name="item{{$key+1}}{{$key+1}}" id="item{{$key+1}}{{$key+1}}" value="{{$head['head']->i_id}}" {{$head['checked']}}>{{$head['head']->nama_menu}}
                                                                <i class="input-helper"></i></label>
                                                            </div>
                                                        @else                                                        
                                                            @foreach($head['item'] as $key2 => $item)
                                                            <div class="form-check form-check-primary">
                                                                <label class="form-check-label">
                                                                <input type="checkbox" class="form-check-input" name="item{{$key+1}}{{$key2+1}}" id="item{{$key+1}}{{$key2+1}}" value="{{$item->i_id}}" {{$item->checked}}>{{$item->nama_sub_menu}}
                                                                <i class="input-helper"></i></label>
                                                            </div>                         
                                                            @endforeach                       
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" onclick="return prosesUpdate()" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Update
                </button>
                <button type="button" onclick="loadNewPage('{{route('referensi.hak_akses')}}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </form>
        </div>
    </div>
</div>
@include('referensi.hak_akses.action')
@endsection