<div class="modal fade" id="_modal_pelanggan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Customer</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_pelanggan">
            @csrf
            <div class="modal-body">                
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Customer Name</label>
                        <div class="col-sm-8">                                                         
                            <input type="hidden" name="id_pel" id="id_pel" value=""/>                        
                            <input type="text" class="form-control form-control-sm border-dark" name="nama_pel" id="nama_pel" value="" autocomplete="off"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Address</label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control form-control-sm border-dark" name="alamat" id="alamat" value=""></textarea>                            
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">PKP / NON PKP</label>
                        <div class="col-sm-8">                                                        
                            <select id="pkp" name="pkp" class="form-control form-control-sm border-dark" >
                                <option value="">Pilih</option>
                                <option value="t">PKP</option>
                                <option value="f">NON/PKP</option>
                            </select>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">NPWP</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="npwp" id="npwp" value="000000000000000" placeholder="NPWP"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Telephone</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="telp" id="telp" autocomplete="off" value="" onkeydown="hanyaangka()"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="contact" id="contact" value="" placeholder="Contact"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">FAX</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="fax" id="fax" value="" placeholder="Fax"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">City</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="city" id="city" value="" placeholder="City"/>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Marketing Code</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="kode_marketing" id="kode_marketing" placeholder="Marketing Code"/>
                        </div>                        
                    </div>  
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Due Date</label>
                        <div class="col-sm-8">                            
                            <input type="text" class="form-control form-control-sm border-dark" name="jth_tempo" id="jth_tempo" placeholder="example 120" onkeydown="hanyaangka()"/>
                        </div>                        
                    </div>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                <!-- <button type="button" class="btn btn-secondary" onclick="back()">Batal</button> -->
                <button type="submit" class="btn btn-success" onclick="insert($('#id_pel').val())">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>