<script>

var act_url = '{{ route('data_pelanggan') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_pel', name: 'i_pel' },        
        { data: 'e_nama_pel', name: 'e_nama_pel' },
        { data: 'e_alamat_pel', name: 'e_alamat_pel' },        
        { data: 'f_pkp', name: 'f_pkp' },
        { data: 'e_npwp_pel', name: 'e_npwp_pel' },
        { data: 'e_telp_pel', name: 'e_telp_pel' },
        { data: 'e_kont_pel', name: 'e_kont_pel' },
        { data: 'e_fax_pel', name: 'e_fax_pel' },
        { data: 'e_kota_pel', name: 'e_kota_pel' },
        { data: 'e_kode_marketing', name: 'e_kode_marketing' },
        { data: 'n_jth_tempo', name: 'n_jth_tempo' }
    ]
});
var _modal_pelanggan = $('#_modal_pelanggan');
function tambah()
{
    var form_rfp = $('#form_pelanggan');
    form_rfp[0].reset();
    _modal_pelanggan.modal({backdrop: 'static', keyboard: false});
}

function edit(id)
{
    $.ajax({
        type: "GET",
        url: base_url + '/referensi/pelanggan/edit/' + id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);        
        console.log(data)
        $.each(data, function(k,v){
            $('#id_pel').val(v.i_pel);
            $('#nama_pel').val(v.e_nama_pel);
            $('#alamat').val(v.e_alamat_pel);
            var pkp = v.f_pkp==false?'f':'t';
            $('#pkp').val(pkp);
            $('#npwp').val(v.e_npwp_pel);
            $('#telp').val(v.e_telp_pel);
            $('#contact').val(v.e_kont_pel);
            $('#fax').val(v.e_fax_pel);
            $('#city').val(v.e_kota_pel);
            $('#kode_marketing').val(v.e_kode_marketing);
            $('#jth_tempo').val(v.n_jth_tempo);
        });                    
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    _modal_pelanggan.modal({backdrop: 'static', keyboard: false});
}

function insert(id)
{    
    // var tujuan = id==''?'simpan':'update';
    $('#form_pelanggan').validate({
        rules: {
            nama_pel: {
                required: !0,
                maxlength: 250 
            },
            alamat: {
                required: !0,
                maxlength: 250
            },
            pkp : {
                required: !0
            },
            npwp: {
                required: !0,
                maxlength: 15,
                digits: !0
            },
            telp: {
                required: !0,
                maxlength: 30,
                digits: !0
            },
            contact: {
                required: !0,
                maxlength: 30
            },
            fax: {                
                maxlength: 30
            },
            city: {
                required: !0,
                maxlength: 60
            },
            kode_marketing: {
                maxlength: 16
            },
            jth_tempo: {
                digits: !0
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_pelanggan');
            var _form_data = new FormData(form_rfp[0]);                        

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/referensi/pelanggan/simpan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal('Berhasil',data.msg,'success');
                            form_rfp[0].reset();
                            _modal_pelanggan.modal('hide');
                            reload();
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}

function reload()
{
    table.ajax.reload();
}
</script>