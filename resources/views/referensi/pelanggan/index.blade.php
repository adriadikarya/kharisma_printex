@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Pelanggan </h4>
            <p class="card-description">
                <button type="button" onclick="tambah()" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add Customer
                </button>
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Action</th>
                            <th style="text-align:center;">Customer ID</th>
                            <th style="text-align:center;">Customer Name</th>
                            <th style="text-align:center;">Address</th>
                            <th style="text-align:center;">PKP</th> 
                            <th style="text-align:center;">NPWP</th>
                            <th style="text-align:center;">Telephone</th>
                            <th style="text-align:center;">Contact</th>
                            <th style="text-align:center;">Fax</th>
                            <th style="text-align:center;">City</th>
                            <th style="text-align:center;">Marketing Code</th>
                            <th style="text-align:center;">Due Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('referensi.pelanggan.modal')
@include('referensi.pelanggan.action')
@endsection