@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Create Delivery Order</h4>
            <form class="form" id="form-do" method="GET">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Customer</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <select class="form-control form-control-sm" name="pel" id="pel">
                                                @if($param['pel'])
                                                <option value="">Choose Customer!!</option>
                                                @foreach($param['pel'] as $pel)
                                                <option value="{{ $pel->i_pel }}">{{ $pel->e_nama_pel }}</option>
                                                @endforeach
                                                @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>                                                        
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Card Production</label>
                                    <div class="col-sm-8">
                                        <div class="form-group row">
                                            <input type="text" name="no_kartu" id="no_kartu" class="form-control form-control-sm border-dark col-sm-8" readonly>
                                            <button type="button" class="btn btn-sm btn-info btn-round" onclick="showKartu()">Pick</button> 
                                        </div>
                                        <input type="hidden" name="idkartu" id="idkartu" value=""/>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">    
                                    <label class="col-sm-3 col-form-label">JO</label>
                                    <div class="col-sm-8"> 
                                        <div class="form-group row">                                   
                                            <input type="text" name="no_rfp" id="no_rfp" class="form-control form-control-sm border-dark col-sm-8" readonly/>
                                            <input type="hidden" name="idrfp" id="idrfp" value=""/>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">    
                                    <label class="col-sm-3 col-form-label">SO</label>
                                    <div class="col-sm-8"> 
                                        <div class="form-group row">                                   
                                            <input type="text" name="no_so" id="no_so" class="form-control form-control-sm border-dark col-sm-8" readonly/>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success btn-icon-text" onclick="validasi()"><i class="mdi mdi-download btn-icon-prepend"></i>Proses</button>
                                <button type="reset" class="btn btn-warning btn-icon-text" onclick="reset()"><i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Delivery Orders</h4>
                    <div class="table-responsive pt-3">            
                        <table class="table table-bordered table-striped" id="datatable" width="100%">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>No</th>
                                    <th>DO Number</th>                                                                                        
                                    <th>Send Date</th>                                                                                            
                                    <th>Customer</th>
                                    <th>Total Roll</th>
                                    <th>Total SJ From</th>
                                    <th>Total KP From</th>
                                    <th>Total KP To</th>
                                    <th>Status</th>
                                    <th>Alasan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>            
                    </div>
                </div>
            </div>            
        </div>
    </div>
</div>
@include('do.modal')
@include('do.action')
<script>
    $('#pel').select2();
</script>
@endsection