<script>

$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});

var act_url = '{{ route('do_data') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true, 
    lengthMenu: [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],   
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action'},
        { data: 'urutan', name: 'urutan'},
        { data: 'i_no_sj', name: 'i_no_sj'},
        // { data: 'tgl_produksi', name: 'tgl_produksi'},
        { data: 'd_sj', name: 'd_sj'},
        // { data: 'd_due_date', name: 'd_due_date'},
        { data: 'e_nama_pel', name: 'e_nama_pel'},        
        { data: 'n_tot_roll', name: 'n_tot_roll'},
        { data: 'n_tot_asal_sj', name: 'n_tot_asal_sj'},
        { data: 'n_tot_asal_kp', name: 'n_tot_asal_kp'},
        { data: 'n_tot_jadi_kp', name: 'n_tot_jadi_kp'}, 
        { data: 'definition', name: 'definition'},       
        { data: 'alasan_reject', name: 'alasan_reject'},        
    ],
    // dom: 'Blfrtip',
    dom: 'lBfrtip',
    buttons: [ {
        extend: 'print',
        exportOptions: {
            columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
        },
    }],
});

function prosesSimpan(text){    
    var tot = $('#tot_roll').val();
    var tgl_kirim = $('#tgl_sj').val();
    var expedisi = $('#expedisi').val();
    if(tgl_kirim==''){
        $('#tgl_sj').focus();
        swal('Peringatan','Tolong Pilih Tgl Kirim SJ','warning');
        return false;
    }

    if(expedisi=='') {
        $('#expedisi').focus();
        swal('Peringatan','Expedisi Wajib Diisi','warning');
        return false;
    }

    if(tot==''){
        $('#tot_roll').focus();
        swal('Peringatan','Tolong Pilih Minimal Satu Roll di Packing List','warning');
        return false;
    }    

    var form_kartu = $('#form_do');
    var _form_data = new FormData(form_kartu[0]);

    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Data Sudah Benar Semua?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "POST",
                url: base_url + '/do/simpan',
                data: _form_data,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal({   
                        title: "Sukses",   
                        text: data.msg,   
                        type: "success",                       
                        showCancelButton: false,   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Ya",                     
                        closeOnConfirm: true,                                       
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){   
                        if(isConfirm){
                            loadNewPage('{{ route('do') }}');
                        }
                    });
                } else {
                    swal('Gagal',data.msg,'error');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });                        
        } else {     
            swal("Batal", "Data Surat Jalan Tidak Jadi Di"+text+" :)", "error");   
        } 
    });
}

function validasi(){    
    $('#form-do').validate({
        rules: {
            pel: {
                required: !0,
            },
            no_kartu: {
                required: !0,
            },
            no_rfp: {
                required: !0
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');            
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element.next());
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e) {
            var idrfp = $('#idrfp').val();
            var idkartu = $('#idkartu').val();                        
            $.ajax({
                type: "GET",
                url: base_url + '/do/cek_qc/'+ idrfp,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                if(data.code==1){
                    loadNewPage(base_url + '/do/create_do?idrfp='+idrfp+'&&idkartu='+idkartu);
                } else {
                    swal('Peringatan',data.msg,'warning');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        }
    });
}

function showKartu(){
    var pel = $('#pel').val();
    if(pel==''){
        swal('Peringatan','Tolong pilih dulu Pelanggannya!','warning');
    } else {
        $.ajax({
            type: "GET",
            url: base_url + '/do/list/'+pel,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(res) {

            },
        }).done(function(res){
            $('#loading').hide();            
            var data = $.parseJSON(res);        
            var tbl_list = '';
            $.each(data, function(k,v){
                console.log(v)
                tbl_list+= '<tr><td><a href="#" onclick="pilih(\''+v.i_no_kartu+'\',\''+v.id_kartu+'\',\''+v.i_no_rfp+'\',\''+v.id_rfp+'\',\''+pel+'\',\''+v.i_no_so+'\')">'+v.i_no_kartu+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih(\''+v.i_no_kartu+'\',\''+v.id_kartu+'\',\''+v.i_no_rfp+'\',\''+v.id_rfp+'\',\''+pel+'\',\''+v.i_no_so+'\')">'+v.i_no_rfp+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih(\''+v.i_no_kartu+'\',\''+v.id_kartu+'\',\''+v.i_no_rfp+'\',\''+v.id_rfp+'\',\''+pel+'\',\''+v.i_no_so+'\')">'+v.i_no_so+'</a></td></tr>';
            });            
            $('#list').html(tbl_list);
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
        $('#_list_kartu').modal({backdrop: 'static', keyboard: false}); 
    }    
}

function pilih(nokartu, idkartu, norfp, idrfp, pel, noso){
    $('#no_kartu').val(nokartu);
    $('#idkartu').val(idkartu);
    $('#no_rfp').val(norfp);
    $('#idrfp').val(idrfp);    
    $('#no_so').val(noso);
    $('#_list_kartu').modal('hide');
}

function reset() {
    $('#idkartu').val('');
    $('#idrfp').val('');
    $('#real_pel').val('');
}

function getJatuhTempo(source,destination,range) { // d/m/Y
    var nilai;
    // 27-11-2014
    console.log(source)
    var jangka=$("#lama_jth_tempo").val();
    var tsplit	= source.split('-');
    var baru = tsplit['2']+'/'+tsplit['1']+'/'+tsplit['0'];
    var firstDay = new Date(baru);    
    var test = firstDay.getTime() + parseInt(parseInt(jangka) * 24 * 60 * 60 * 1000);    
    var tanggal = new Date(test);
    var tgl	= tanggal.getDate();
    var tgl = pad(tgl,2);
    var bln = tanggal.getMonth()+1; // +1 karena getMonth itu startnya dari 0 (Januari), 1 (Februari), dst
    var bln = pad(bln, 2);
    var thn = tanggal.getFullYear();

    nilai = tgl+'-'+bln+'-'+thn;
    destination.value	= nilai;    
}

function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

function hitungSatu(pilih,asalsj,asalkp,jadikp,kode){   
    var urut = cekIndexAlphabet(kode);
    var urut = parseInt(urut)+1;

    var a = $('#'+asalsj).val();
    var b = $('#'+asalkp).val();
    var c = $('#'+jadikp).val();
    var tot = $('#tot_roll').val() || 0;
    var tota = $('#tot_asal_sj').val() || 0;
    var totb = $('#tot_asal_kp').val() || 0;
    var totc = $('#tot_jadi_kp').val() || 0;
    var totcw = $('#tot_cw'+urut).val() || 0;
    if($(pilih).prop("checked") == true){
        if(c==0 || c==''){
            $(pilih).prop("checked", false);
        } else {
            tot = parseFloat(tot) + 1;
            tota = parseFloat(tota) + parseFloat(a);
            totb = parseFloat(totb) + parseFloat(b);
            totc = parseFloat(totc) + parseFloat(c);
            totcw = parseFloat(totcw) + parseFloat(c);
            $('#tot_roll').val(tot.toFixed(2));
            $('#tot_asal_sj').val(tota.toFixed(2));
            $('#tot_asal_kp').val(totb.toFixed(2));
            $('#tot_jadi_kp').val(totc.toFixed(2));
            $('#tot_cw'+urut).val(totcw.toFixed(2));
        }
    } else {
        tot = parseFloat(tot) - 1;
        tota = parseFloat(tota) - parseFloat(a);
        totb = parseFloat(totb) - parseFloat(b);
        totc = parseFloat(totc) - parseFloat(c);
        totcw = parseFloat(totcw) - parseFloat(c);
        $('#tot_roll').val(tot.toFixed(2));
        $('#tot_asal_sj').val(tota.toFixed(2));
        $('#tot_asal_kp').val(totb.toFixed(2));
        $('#tot_jadi_kp').val(totc.toFixed(2));
        $('#tot_cw'+urut).val(totcw.toFixed(2));
    }
}

function cekIndexAlphabet(huruf){
    var alphabet = ['A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z'];
    return alphabet.indexOf(huruf);
}

function cekQtyPrint() {
    if($('#asal_sj').prop('checked') == false && $('#asal_kp').prop('checked') == false && $('#jadi_kp').prop('checked') == false) {
        swal('Informasi','Pilih minimal salah satu Qty','info');
    }
}

function printSJ(id,idkartu,idrfp){
    $('#id_sj_m').val(id);
    $('#id_rfp_m').val(idrfp);
    $('#id_kartu_m').val(idkartu);
    $('#_pilih_jenis_qty').modal({backdrop: 'static', keyboard: false});
    // loadNewPage(base_url + '/do/print_do?id='+id+'&idkartu='+idkartu+'&idrfp='+idrfp);
}

function printSJForm(id, idrfp, idkartu, el_asalsj, el_asalkp, el_jadikp){
    $('#_pilih_jenis_qty').modal('hide');
    let new_get = '';
    if($(el_asalsj).prop("checked") == true) new_get+='&asalsj=ya';
    if($(el_asalkp).prop("checked") == true) new_get+='&asalkp=ya';
    if($(el_jadikp).prop("checked") == true) new_get+='&jadikp=ya';    
    loadNewPage(base_url + '/do/print_do?id='+id+'&idkartu='+idkartu+'&idrfp='+idrfp+new_get);
}

function lihatSJ(id, idkartu, idrfp){
    loadNewPage(base_url + '/do/lihat_do?id='+id+'&idkartu='+idkartu+'&idrfp='+idrfp);
}

function requestPrintSJ(id, nosj){
    swal({   
        title: "Kirim Request",   
        text: "Kirim Request "+nosj+"?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/do/request_print_sj/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('do') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Request Print SJ','info');
        }
    });
}

function editSJ(id, idkartu, idrfp){
    $.ajax({
        type: "GET",
        url: base_url + '/do/cek_print_sj/'+id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);        
        if(data.code==1){
            loadNewPage(base_url + '/do/edit_do?id='+id+'&&idkartu='+idkartu+'&&idrfp='+idrfp);
        } else {
            swal('Peringatan',data.msg,'warning');
        }
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
}

</script>