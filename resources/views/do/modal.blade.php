<div class="modal fade" id="_list_kartu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Card Production</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_cw">
            <div class="modal-body">                
                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-striped">        
                        <thead>
                            <th>Card Number</th>
                            <th>JO Number</th>
                            <th>SO Number</th>
                        </thead>
                        <tbody id="list">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="_pilih_jenis_qty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pilih Print Qty</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_cw">
            <div class="modal-body">  
                <input type="hidden" name="id_rfp_m" id="id_rfp_m">              
                <input type="hidden" name="id_kartu_m" id="id_kartu_m">              
                <input type="hidden" name="id_sj_m" id="id_sj_m">              
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-check form-check-primary">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="asal_sj" value="1">
                                  Asal SJ
                                <i class="input-helper"></i></label>
                              </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-check form-check-success">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="asal_kp" value="2">
                                  Asal KP
                                <i class="input-helper"></i></label>
                              </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-check form-check-danger">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" id="jadi_kp" value="3">
                                  Jadi KP
                                <i class="input-helper"></i></label>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-success" onclick="printSJForm($('#id_sj_m').val(),$('#id_rfp_m').val(),$('#id_kartu_m').val(),$('#asal_sj'),$('#asal_kp'),$('#jadi_kp')); cekQtyPrint();">Pilih</button>
            </div>
            </form>
        </div>
    </div>
</div>