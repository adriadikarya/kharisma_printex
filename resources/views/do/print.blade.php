@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@php
date_default_timezone_set('Asia/Jakarta');
@endphp
<style type="text/css" media="print">    
    .noDisplay {
        display: none;
    }    
    
    @page{
        /* size: A4 landscape; */
        /* margin: 0.05in 0.79in 0.10in 0.60in; */
        margin: 0cm;
        /* size: letter landscape; */
        /* orientation: landscape; */
    }    

    .isinya {
        font-family: Helvetica, Geneva, Arial,
            SunSans-Regular, sans-serif;
        font-size: 12px;
    }

    .pageBreak {
        page-break-before: always;
    }
</style>
<div class="col-12-grid-margin">
    @if($param['data'])    
    @foreach($param['data'] as $data)
    <div class="card">
        <div class="card-body text-dark">
            <div id="printAble">
                <input type="hidden" name="id_sj" id="id_sj" value="{{$data->i_id}}"/>
                <div class="row">
                    <div class="col-md-4">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="font-family:Arial, Helvetica, sans-serif;font-size:22px;"><b>PT KHARISMA PRINTEX</b></td>
                                </tr>
                                <tr>
                                    <td style="font-family:courier;">Jl. Holis No. 461 Bandung</td>
                                </tr>
                                <tr>
                                    <td style="font-size:16px;"><b>email: kharismabdg@gmail.com</b></td>
                                </tr>
                                <tr>
                                    <td style="font-size:16px;"><b>email: kppemasaran@gmail.com</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="table">
                            <tbody> 
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>                       
                                <tr>
                                    <td align="center" style="font-size:22px;">SURAT JALAN No. {{$data->i_no_sj}}</td>
                                </tr>                                
                                <tr>
                                    <td align="center" style="font-size:22px;">SO No. {{$data->i_no_so}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="2" align="right" style="font-size:22px;">FR.QC.01.06</td>
                                </tr>                                
                                <tr>
                                    <td colspan="2" style="font-size:22px;">Bandung, {{date('d M Y', strtotime($data->d_sj))}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="font-size:22px;">Kepada, {{$data->e_nama_pel}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;">Alamat :</td>
                                    <td style="font-size:22px;">{{$data->e_alamat_pel}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-3">
                        <table class="table table-bordered">
                            <tbody id="list">
                            <tr>
                                <td style="font-size:18px;">No. Design :</td>
                                <td style="font-size:18px;">{{$data->i_desain}}</td>
                            </tr>
                            {{--<tr> 
                                <td style="font-size:18px;">Jenis Kain :</td>
                                <td style="font-size:18px;">{{$data->e_kain}}</td>
                            </tr>--}}
                            <tr>
                                <td style="font-size:18px;">Ket Design :</td>
                                <td style="font-size:18px;">{{$data->e_motif}}</td>
                            </tr>
                            <tr>
                                <td style="font-size:18px;">Jenis Proses :</td>
                                <td style="font-size:18px;">
                                @foreach($param['jp'] as $jp)
                                    {{$jp->description.' '}}
                                @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size:18px;">Jenis Printing :</td>
                                <td style="font-size:18px;">{{$data->nama_printing}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="table table-bordered">
                            <tr>
                                <td rowspan="3" style="font-size:18px;">Pemartaian Jumlah</td>
                                <td style="font-size:14px;">Roll:</td>
                                <td style="font-size:18px;">{{$data->roll_pem}}</td>
                            </tr>
                            <tr>                                 
                                <td style="font-size:14px;">Kg:</td>
                                <td style="font-size:18px;">{{$data->kg_pem}}</td>
                            </tr>
                            <tr>                                
                                <td style="font-size:14px;">Mtr:</td>
                                <td style="font-size:18px;">{{$data->mtr_pem}}</td>
                            </tr>
                            <tr>
                                <td style="font-size:18px;">Tgl. Produksi :</td>
                                <td colspan="2" style="font-size:18px;">{{date('d M Y',strtotime($data->tgl_produksi))}}</td>
                            </tr>  
                            <tr>
                                <td style="font-size:18px;">No. SJ :</td>
                                <td colspan="2" style="font-size:18px;">{{$data->i_no_sj}}</td>
                            </tr>                                                                              
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="table table-bordered">                                    
                            <tbody id="list">
                                <tr>                                    
                                    <td style="font-size:18px;">Total Roll :</td>
                                    <td style="font-size:18px;">{{$data->n_tot_roll}}</td>
                                </tr>
                                <tr>                                 
                                    <td style="font-size:18px;">Asal SJ :</td>
                                    <td style="font-size:18px;">{{$data->n_tot_asal_sj}}</td>
                                </tr>
                                <tr>                                
                                    <td style="font-size:18px;">Asal KP :</td>
                                    <td style="font-size:18px;">{{$data->n_tot_asal_kp}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:18px;">Jadi KP :</td>
                                    <td style="font-size:18px;">{{$data->n_tot_jadi_kp}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:18px;">No. SO :</td>
                                    <td style="font-size:18px;">{{$data->i_no_so}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:18px;">No. PO :</td>
                                    <td style="font-size:18px;">{{$data->i_no_po}}</td>
                                </tr>                                                                          
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="table table-bordered">                                    
                            <tbody id="list">
                                <tr>
                                    <td style="font-size:18px;">Ex. SJ :</td>
                                    <td style="font-size:18px;"> 
                                    @php
                                        $i = 0;
                                        $len = count($param['exsj']);
                                    @endphp
                                    @foreach($param['exsj'] as $exsj)
                                        @if ($i == $len - 1)
                                            {{$exsj->i_no_sj}}
                                        @else
                                            {{$exsj->i_no_sj.' +'}}
                                        @endif                                        
                                        @php                       
                                        $i++;
                                        @endphp
                                    @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size:18px;">LPK :</td>
                                    <td style="font-size:18px;">
                                    @php
                                        $i = 0;
                                        $len = count($param['lpk']);
                                    @endphp
                                    @foreach($param['lpk'] as $lpk)
                                        @if ($i == $len - 1)
                                            {{$lpk->e_nomor_lpk.' ('.$lpk->qty_roll.')'}}
                                        @else
                                            {{$lpk->e_nomor_lpk.' ('.$lpk->qty_roll.')'.' +'}}
                                        @endif                                        
                                        @php                       
                                        $i++;
                                        @endphp
                                    @endforeach
                                    </td>
                                </tr>                                                                       
                                <tr>
                                    <td style="font-size:18px;">Expedisi :</td>
                                    <td style="font-size:18px;">{{$data->expedisi}}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="87" style="font-size:18px;vertical-align: text-top;">Keterangan : {{$data->e_ket}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="card-title">Harap diterima barang sebagai berikut: </h3>
                    </div>
                </div>
                <div class="row">         
                    <input type="hidden" name="totalcw" id="totalcw" value="{{count($param['packing'])}}">
                        @foreach($param['packing'] as $key => $item)                    
                        @php 
                            $e_kode = \DB::select("select e_kode from mst_packing_list where i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' AND i_id_sj='".$param['idsj']."' group by e_kode order by left(e_kode, 1),COALESCE(substring(e_kode,'\d+')::int,0)");  
                            $qHarga = \DB::select("SELECT v_harga_sat FROM mst_so_item WHERE i_id='".$item->id_cw."'");                      
                            $jml = count($e_kode);                            
                        @endphp                                             
                    <input type="hidden" name="jml{{$key+1}}" id="jml{{$key+1}}" value="{{$jml}}">
                    <input type="hidden" name="id_cw{{$key+1}}" id="id_cw{{$key+1}}" value="{{$item->id_cw}}"/>
                    <input type="hidden" name="hrg_cw{{$key+1}}" id="hrg_cw{{$key+1}}" value="{{$qHarga[0]->v_harga_sat}}"/>
                    <input type="hidden" name="tot_cw{{$key+1}}" id="tot_cw{{$key+1}}" value=""/>
                        @foreach($e_kode as $kiy => $kode)
                        @php
                            $list_pack = \DB::select("select * from mst_packing_list where e_kode='".$kode->e_kode."' AND i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' order by i_id");
                            $tot_n_asal_sj=0;
                            $tot_n_asal_kp=0;
                            $tot_n_jadi_kp=0;
                        @endphp                    
                    <div class="col-md-4 mb-2">
                        <table class="table table-bordered" id="table-harga">
                                <tr>
                                    <th colspan="5" style="text-align:center;font-size:18px;">{{$item->e_uraian_pekerjaan}}</th>
                                <tr>
                                {{-- @if($data->i_penyedia==0)
                                    <tr>
                                        <th style="text-align:center;font-size:18px;">Roll</th>                                    
                                        <th style="text-align:center;width:150px;font-size:18px;">Asal SJ</th>
                                    </tr>    
                                    <tr>                          
                                    @php $kd = substr($kode->e_kode,0,1); @endphp      
                                        <th style="text-align:center;font-size:18px;">{{$kode->e_kode}}</th>
                                        <input type="hidden" name="kode_dyeing{{$kd}}{{$kiy+1}}" name="kode_dyeing{{$kd}}{{$kiy+1}}" value="{{$kode->e_kode}}"/>
                                        <th style="text-align:center;font-size:18px;">Kg</th>                                        
                                    </tr>
                                @else --}}
                                    <tr>
                                        <th style="text-align:center;font-size:18px;">Roll</th>            
                                        @if($param['asal_sj'])                        
                                        <th style="text-align:center;width:150px;font-size:18px;">Asal SJ</th>
                                        @endif
                                        @if($param['asal_kp'])
                                        <th style="text-align:center;width:150px;font-size:18px;">Asal KP</th>
                                        @endif
                                        @if($param['jadi_kp'])
                                        <th style="text-align:center;width:150px;font-size:18px;">Jadi KP</th>
                                        @endif
                                    </tr>
                                    <tr>                          
                                    @php $kd = substr($kode->e_kode,0,1); @endphp      
                                        <th style="text-align:center;font-size:18px;">{{$kode->e_kode}}</th>
                                        <input type="hidden" name="kode_dyeing{{$kd}}{{$kiy+1}}" name="kode_dyeing{{$kd}}{{$kiy+1}}" value="{{$kode->e_kode}}"/>
                                        @if($param['asal_sj'])
                                        <th style="text-align:center;font-size:18px;">Kg</th>
                                        @endif
                                        @if($param['asal_kp'])
                                        <th style="text-align:center;font-size:18px;">Kg</th>
                                        @endif
                                        @if($param['jadi_kp'])
                                        <th style="text-align:center;font-size:18px;">Kg</th>                                    
                                        @endif
                                    </tr>
                                {{-- @endif                                                             --}}
                            @foreach($list_pack as $koy => $lp)                            
                                @if($lp->f_jadi_sj && $lp->i_id_sj==$param['idsj'])
                                    {{-- @if($data->i_penyedia==0)                                
                                        <tr>
                                            <td align="center" style="font-size:18px;">{{$koy+1}}</td>
                                            <td align="center" style="font-size:18px;">{{$lp->n_asal_sj}}</td>                                    
                                        </tr>
                                    @else                             --}}
                                        <tr>
                                            <td align="center" style="font-size:18px;">{{$koy+1}}</td>
                                            @if($param['asal_sj'])
                                            <td align="center" style="font-size:18px;">{{$lp->n_asal_sj}}</td>
                                            @endif
                                            @if($param['asal_kp'])
                                            <td align="center" style="font-size:18px;">{{$lp->n_asal_kp}}</td>
                                            @endif
                                            @if($param['jadi_kp'])
                                            <td align="center" style="font-size:18px;">{{$lp->n_jadi_kp}}</td>
                                            @endif
                                        </tr> 
                                    {{-- @endif              --}}
                                    @php
                                        $tot_n_asal_sj+= $lp->n_asal_sj;
                                        $tot_n_asal_kp+= $lp->n_asal_kp;
                                        $tot_n_jadi_kp+= $lp->n_jadi_kp;
                                    @endphp
                                @else
                                    @php 
                                        $koy++;                                        
                                    @endphp
                                @endif                                                  
                            @endforeach
                            <tr>
                                <td align="center" style="font-size:18px;">Total</td>
                                @if($param['asal_sj'])
                                <td align="center" style="font-size:18px;">{{$tot_n_asal_sj}}</td>
                                @endif
                                @if($param['asal_kp'])
                                <td align="center" style="font-size:18px;">{{$tot_n_asal_kp}}</td>
                                @endif
                                @if($param['jadi_kp'])
                                <td align="center" style="font-size:18px;">{{$tot_n_jadi_kp}}</td>
                                @endif
                            </tr>
                            <input type="hidden" name="count_list{{$kode->e_kode}}" value="{{count($list_pack)}}"/>                            
                        </table>                        
                    </div>
                        @endforeach                                                
                        @endforeach                   
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-3">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="font-size:14px;">1. Putih: Penagihan</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px;">2. Merah: Arsip</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px;">3. Hijau: Pelanggan</td>
                                </tr>
                                <tr>
                                    <td style="font-size:14px;">4. Kuning: Satpam</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td align="center" style="font-size:22px;"><b>Penerima<br>{{$data->e_nama_pel}}</b></td>
                                </tr>
                                <tr>
                                    <td style="font-size:11px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size:11px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" style="font-size:11px;">................................................</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td align="center" style="font-size:22px;"><b>Bagian Pengiriman<br>
                                        @php
                                            $tmp = explode('/',$data->expedisi);                                            
                                            $nama_pengirim = !empty($tmp[1])?$tmp[1]:"";
                                        @endphp
                                        {{ $nama_pengirim }}&nbsp;
                                    </b></td>
                                </tr>
                                <tr>
                                    <td style="font-size:11px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size:11px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" style="font-size:11px;">................................................</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td align="center" style="font-size:20px;"><b>Hormat Kami<br>PT KHARISMA PRINTEX</b></td>
                                </tr>
                                <tr>
                                    <td style="font-size:11px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="font-size:11px;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" style="font-size:11px;">................................................</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endif
    <div class="card noDisplay">
        <div class="card-body">                
            <button type="button" onclick="loadNewPage('{{ route('do') }}')" class="btn btn-warning btn-icon-text">
                <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                Kembali
            </button>
            <button type="button" onclick="printDiv('printAble',$('#id_sj').val())" class="btn btn-info btn-icon-text">
                <i class="mdi mdi-printer btn-icon-prepend"></i>
                Print
            </button>
        </div>
    </div>
</div>
<script type="text/javascript">
function printDiv(divName, id) {
    $.ajax({
        type: "GET",
        url: base_url + '/do/update_print/'+id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);        
        if(data.code==1){            
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            // var css = '@page { margin: 0cm; size: landscape; }',
            //     head = document.head || document.getElementsByTagName('head')[0],
            //     style = document.createElement('style');

            // style.type = 'text/css';
            // style.media = 'print';

            // if (style.styleSheet){
            //     style.styleSheet.cssText = css;
            // } else {
            //     style.appendChild(document.createTextNode(css));
            // }
            // head.appendChild(style);
            window.print();

            document.body.innerHTML = originalContents;
        } else {
            swal('Error',data.msg,'error');
        }
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
}
</script>
@endsection