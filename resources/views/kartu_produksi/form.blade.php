@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['data'])
@foreach($param['data'] as $item)
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_kartu">
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Kartu Produksi</h4>
                <div class="row">
                    <input type="hidden" name="id_rfp" id="id_rfp" value="{{ $item->id_rfp }}" />
                    <input type="hidden" name="id_so" id="id_so" value="{{ $item->id_so }}" />
                    <input type="hidden" name="id_kartu" id="id_kartu" value="{{ $item->i_id }}" />
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">NOMOR KARTU</label>
                            <div class="col-sm-9">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="no_kartu"
                                    id="no_kartu" maxlength="25" @if($item->i_no_kartu) value="{{$item->i_no_kartu}}" @else value="" @endif readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">TANGGAL PENGIRIMAN</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="d_selesai" id="d_selesai"
                                    value="{{ date('d-m-Y', strtotime($item->d_selesai)) }}" readonly />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">NOMOR SO/PO:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="no_so_po" id="no_so_po" value="{{ $item->i_no_so.' / '.$item->i_no_po }}"
                                    readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">NOMOR JO:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="no_rfp" id="no_rfp" value="{{ $item->i_no_rfp }}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">NAMA PELANGGAN</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark" name="pel"
                                    id="pel" value="{{ $item->e_nama_pel }}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">NOMOR DESAIN</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="desain" id="desain" value="{{ $item->i_desain }}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">JENIS PRINT</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="desain" id="desain" value="{{ $item->nama_printing }}" readonly />
                            </div>
                        </div>
                    </div>
                    {{--<div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">JENIS KAIN</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="desain" id="desain" value="{{ $item->e_kain }}" readonly />
                            </div>
                        </div>
                    </div>--}}
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">KONDISI KAIN</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="desain" id="desain"
                                    @if($item->e_ket_ori_cond) value="{{ $item->ori_cond1.'/'.$item->ori_cond2.'/'.$item->e_ket_ori_cond }}" @else value="{{ $item->ori_cond1.'/'.$item->ori_cond2 }}" @endif
                                    readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">WARNA DASAR</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    name="desain" id="desain" @if($item->e_color_others) value="{{ $item->e_color_others }}" @else value="{{$item->e_warna_dasar}}" @endif readonly />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            @foreach($param['lpk'] as $key => $lpk)
                            @if($key % 3 == 0)
                            <label class="col-sm-4 col-form-label mb-0">NOMOR LPK</label>
                            <div class="col-sm-8 mb-0">
                                <div class="form-group row mb-0">
                                    <input type="text" class="form-control form-control-sm border-dark col-md-6"
                                    name="lpk{{$key}}" id="lpk{{$key}}" value="{{ $lpk->e_nomor_lpk }}" readonly />
                                    <input type="text" class="form-control form-control-sm border-dark col-md-6" name="qty_lpk{{$key}}" id="qty{{$key}}" value="{{$lpk->qty_roll}} Roll" readonly />
                                </div>
                            </div>
                            @else
                            <label class="col-sm-4 col-form-label mb-0">&nbsp;</label>
                            <div class="col-sm-8 mb-0">
                                <div class="form-group row mb-0">
                                    <input type="text" class="form-control form-control-sm tgl_input border-dark col-md-6"
                                    name="lpk{{$key}}" id="lpk{{$key}}" value="{{ $lpk->e_nomor_lpk }}" readonly />
                                    <input type="text" class="form-control form-control-sm border-dark col-md-6" name="qty_lpk{{$key}}" id="qty{{$key}}" value="{{$lpk->qty_roll}} Roll" readonly />
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">NOMOR SJ &nbsp; <button type="button"
                                    class="btn btn-rounded btn-info btn-sm" onclick="addRowSJ()"><span
                                        class="mdi mdi-plus"></span></button><button type="button"
                                    class="btn btn-rounded btn-danger btn-sm"><span class="mdi mdi-minus"
                                        onclick="removeRowSJ()"></span></button><input type="hidden" id="row_sj"
                                    name="row_sj" @if($param['sj']) value="{{ count($param['sj']) }}" @else value="1" @endif>
                            </label>
                            <div class="col-sm-8" id="html_sj">
                                @if($param['sj'])
                                    @foreach($param['sj'] as $key => $sj)
                                    @php $row = $key + 1; @endphp
                                    <input type="text" class="form-control form-control-sm border-dark"
                                    placeholder="Nomor SJ {{$row}}" name="sj{{$row}}" id="sj{{$row}}" value="{{$sj->i_no_sj}}">        
                                    @endforeach
                                @else
                                <input type="text" class="form-control form-control-sm border-dark"
                                    placeholder="Nomor SJ 1" name="sj1" id="sj1">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">JENIS PROSES &nbsp; <button type="button"
                                    class="btn btn-rounded btn-info btn-sm" onclick="addRowJP()"><span
                                        class="mdi mdi-plus"></span></button><button type="button"
                                    class="btn btn-rounded btn-danger btn-sm"><span class="mdi mdi-minus"
                                        onclick="removeRowJP()"></span></button><input type="hidden" id="row_jp"
                                    name="row_jp" @if($param['jp']) value="{{ count($param['jp']) }}" @else value="1" @endif>
                            </label>
                            <div class="col-sm-8" id="html_jp">
                                @if($param['jp'])
                                    @foreach($param['jp'] as $key => $jp)
                                    @php $row = $key + 1; @endphp
                                    <input type="text" class="form-control form-control-sm border-dark"
                                    placeholder="Jenis Proses {{$row}}" name="jp{{$row}}" id="jp{{$row}}" value="{{$jp->description}}">        
                                    @endforeach
                                @else
                                <input type="text" class="form-control form-control-sm border-dark"
                                    placeholder="Jenis Proses 1" name="jp1" id="jp1">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @php
                    $roll = 0;
                    $kg = 0;
                    $pjg = 0;
                    @endphp
                    <input type="hidden" name="count_cw" id="count_cw" value="{{count($param['cw'])}}"/>
                    @foreach($param['cw'] as $key => $cw)
                    @php
                    $roll += $cw->n_qty_roll;
                    $kg += $cw->n_qty_kg;
                    $pjg += $cw->n_qty_pjg;
                    @endphp
                    @if($key==0)
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">CW {{$key+1}}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <input type="hidden" id="id_cw{{$key+1}}" name="id_cw{{$key+1}}" value="{{$cw->i_id}}"/>
                                    <textarea type="text" class="form-control form-control-sm tgl_input border-dark col-sm-4"
                                        name="cw{{$key+1}}" id="cw{{$key+1}}" rows="1" readonly>@if($param['text']=='update') {{$cw->e_cw}} @else {{$cw->e_uraian_pekerjaan}} @endif</textarea>
                                    <textarea type="text" class="form-control form-control-sm tgl_input border-dark col-sm-4"
                                        name="strike_off{{$key+1}}" id="strike_off{{$key+1}}" rows="1" readonly> {{$cw->d_strike_off}} </textarea>
                                    <textarea type="text" class="form-control form-control-sm tgl_input border-dark col-sm-4"
                                        name="nama_jns_kain{{$key+1}}" id="nama_jns_kain{{$key+1}}" rows="1" readonly> {{$cw->e_kain}} </textarea>
                                    <input type="hidden" id="jns_kain{{$key+1}}" name="jns_kain{{$key+1}}" value="{{$cw->e_jenis_kain}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1-grid-margin">:</div>
                    <div class="col-md-2">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="roll{{$key+1}}" id="roll{{$key+1}}" onkeyup="formatRp(this);hitungRoll(this);" onkeydown="hanyaangka();" value="{{number_format($cw->n_qty_roll,2,'.',',')}}"/>
                            </div>
                            <label class="col-sm-4 col-form-label">Roll</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="kg{{$key+1}}" id="kg{{$key+1}}" onkeyup="formatRp(this);hitungKg(this);" onkeydown="hanyaangka();" value="{{number_format($cw->n_qty_kg,2,'.',',')}}"/>
                            </div>
                            <label class="col-sm-4 col-form-label">KG</label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="pjg{{$key+1}}" id="pjg{{$key+1}}" onkeyup="formatRp(this);hitungPjg(this);" onkeydown="hanyaangka();" value="{{number_format($cw->n_qty_pjg,2,'.',',')}}"/>
                            </div>
                            <label class="col-sm-4 col-form-label">Meter</label>
                        </div>
                    </div>
                    @else
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">CW {{$key+1}}</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <input type="hidden" id="id_cw{{$key+1}}" name="id_cw{{$key+1}}" value="{{$cw->i_id}}"/>
                                    <textarea type="text" class="form-control form-control-sm tgl_input border-dark col-sm-4"
                                        name="cw{{$key+1}}" id="cw{{$key+1}}" rows="1" readonly>@if($param['text']=='update') {{$cw->e_cw}} @else {{$cw->e_uraian_pekerjaan}} @endif</textarea>
                                    <textarea type="text" class="form-control form-control-sm tgl_input border-dark col-sm-4"
                                        name="strike_off{{$key+1}}" id="strike_off{{$key+1}}" rows="1" readonly> {{$cw->d_strike_off}} </textarea>
                                    <textarea type="text" class="form-control form-control-sm tgl_input border-dark col-sm-4"
                                        name="nama_jns_kain{{$key+1}}" id="nama_jns_kain{{$key+1}}" rows="1" readonly> {{$cw->e_kain}} </textarea>
                                    <input type="hidden" id="jns_kain{{$key+1}}" name="jns_kain{{$key+1}}" value="{{$cw->e_jenis_kain}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1-grid-margin" style="margin-top: -3%;">:</div>
                    <div class="col-md-2" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="roll{{$key+1}}" id="roll{{$key+1}}" onkeyup="formatRp(this);hitungRoll(this);" onkeydown="hanyaangka();" value="{{number_format($cw->n_qty_roll,2,'.',',')}}"/>
                            </div>
                            <label class="col-sm-4 col-form-label">Roll</label>
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="kg{{$key+1}}" id="kg{{$key+1}}" onkeyup="formatRp(this);hitungKg(this);" onkeydown="hanyaangka();" value="{{number_format($cw->n_qty_kg,2,'.',',')}}"/>
                            </div>
                            <label class="col-sm-4 col-form-label">KG</label>
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="pjg{{$key+1}}" id="pjg{{$key+1}}" onkeyup="formatRp(this);hitungPjg(this);" onkeydown="hanyaangka();" value="{{number_format($cw->n_qty_pjg,2,'.',',')}}"/>
                            </div>
                            <label class="col-sm-4 col-form-label">Meter</label>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">&nbsp;</label>
                            <div class="col-sm-8" style="text-align:right">
                                TOTAL ORDER
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1-grid-margin" style="margin-top: -3%;">:</div>
                    <div class="col-md-2" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="tot_roll" id="tot_roll" value="{{number_format($roll,2,'.',',')}}" readonly/>
                            </div>
                            <label class="col-sm-4 col-form-label">Roll</label>
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="tot_kg" id="tot_kg" value="{{number_format($kg,2,'.',',')}}" readonly/>
                            </div>
                            <label class="col-sm-4 col-form-label">KG</label>
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="tot_pjg" id="tot_pjg" value="{{number_format($pjg,2,'.',',')}}" readonly/>
                            </div>
                            <label class="col-sm-4 col-form-label">Meter</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">LEBAR DI BLANKET</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control form-control-sm border-dark" name="lebar_blanket"
                                    id="lebar_blanket" maxlength="75" value="{{ $item->e_lebar_blanket }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">HANDFEEL</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="handfeel"
                                    id="handfeel" maxlength="75" value="{{ $item->e_tekstur }}" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">LEBAR KAIN</label>
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label">:</label>
                                    <div class="col-sm-8-grid-margin">
                                        <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                            style="text-align:right" name="lebar_kain_from" id="lebar_kain_from"
                                            value="{{ $item->e_penyesuaian_lebar_from }}" readonly />
                                    </div>
                                    <div class="col-sm-3-grid-margin">
                                        <label class="col-sm-12 col-form-label">Asal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5-grid-margin">
                        <div class="form-group row">
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="lebar_kain_to" id="lebar_kain_to" value="{{ $item->e_penyesuaian_lebar_to }}" readonly  />
                            </div>
                            <label class="col-sm-5 col-form-label">Permintaan</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="lebar_kain_jadi" id="lebar_kain_jadi" readonly/>
                            </div>
                            <label class="col-sm-5 col-form-label">Jadi</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">GRAMASI KAIN</label>
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label">:</label>
                                    <div class="col-sm-8-grid-margin">
                                        <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                            style="text-align:right" name="gramasi_from" id="gramasi_from"
                                            value="{{ $item->e_gramasi_from }}" readonly />
                                    </div>
                                    <div class="col-sm-3-grid-margin">
                                        <label class="col-sm-12 col-form-label">Asal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5-grid-margin" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="gramasi_to" id="gramasi_to" value="{{ $item->e_gramasi_to }}" readonly />
                            </div>
                            <label class="col-sm-5 col-form-label">Permintaan</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="gramasi_jadi" id="gramasi_jadi" readonly/>
                            </div>
                            <label class="col-sm-5 col-form-label">Jadi</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">SHRINKAGE PAKAN</label>
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label">:</label>
                                    <div class="col-sm-8-grid-margin">
                                        <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                            style="text-align:right" name="pakan_from" id="pakan_from"
                                            value="{{ $item->e_pakan_from }}" readonly />
                                    </div>
                                    <div class="col-sm-3-grid-margin">
                                        <label class="col-sm-12 col-form-label">Asal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5-grid-margin" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="pakan_to" id="pakan_to"  value="{{ $item->e_pakan_to }}" readonly  />
                            </div>
                            <label class="col-sm-5 col-form-label">Permintaan</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="pakan_jadi" id="pakan_jadi" readonly/>
                            </div>
                            <label class="col-sm-5 col-form-label">Jadi</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">SHRINKAGE LUSI</label>
                            <div class="col-sm-8">
                                <div class="form-group row">
                                    <label class="col-sm-1 col-form-label">:</label>
                                    <div class="col-sm-8-grid-margin">
                                        <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                            style="text-align:right" name="lusi_from" id="lusi_from"
                                            value="{{ $item->e_lusi_from }}" readonly />
                                    </div>
                                    <div class="col-sm-3-grid-margin">
                                        <label class="col-sm-12 col-form-label">Asal</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5-grid-margin" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-7">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="lusi_to" id="lusi_to" value="{{ $item->e_lusi_to }}" readonly />
                            </div>
                            <label class="col-sm-5 col-form-label">Permintaan</label>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-top: -3%;">
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm tgl_input border-dark"
                                    style="text-align:right" name="lusi_jadi" id="lusi_jadi" readonly/>
                            </div>
                            <label class="col-sm-5 col-form-label">Jadi</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">KETERANGAN TAMBAHAN</label>
                            <div class="col-sm-10">
                                <textarea id="keterangan" name="keterangan" class="form-control border-dark">{{ $item->e_ket }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <button type="submit" onclick="return prosesSimpan('{{$param['text']}}')" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    {{$param['text']}}
                </button>
                <button type="button" onclick="loadNewPage('{{ route('kartu_produksi.index') }}')"
                    class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </div>
        </div>
    </form>
</div>
@endforeach
@include('kartu_produksi.action')
@else
<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body text-dark">
            <h4 class="card-title">Kartu Produksi</h4>
            <div class="row">
                <div class="col-md-12">
                    <center>Maaf Data SO & RFP Untuk Membuat Kartu Produksi Tidak Ada / Terjadi Kesalahan</center>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <center><button type="button" onclick="loadNewPage('{{ route('kartu_produksi.index') }}')"
                    class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button></center>
        </div>
    </div>
</div>
@endif
@endsection
