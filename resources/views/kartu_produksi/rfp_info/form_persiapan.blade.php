@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($data)
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_persiapan">
        <div class="card">
            <div class="card-body text-dark">
                <h1 >Persiapan {{$totalroll[0]->i_no_rfp.', total roll = '.$totalroll[0]->roll.', total cw = '.$totalroll[0]->total_cw}}</h1>
                <div class="row">
                @php 
                    $cw = ""; 
                    function toNum($data) {
                        $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
                        return $alpha[$data];
                    }
                @endphp
                <input type="hidden" name="totalcw" id="totalcw" value="{{count($data)}}">
                <input type="hidden" name="id_rfp" name="id_rfp" value="{{$id}}"/>
                <input type="hidden" name="count_by_ori_cond" name="count_by_ori_cond" value="{{$ori_cond}}"/>
                <input type="hidden" name="angka_pembagi" name="angka_pembagi" value="{{$pembagi}}"/>
                @foreach($data as $key => $item)
                    @if($cw!=$item->e_uraian_pekerjaan)
                    @php                        
                        $jml = 0;
                        $jml = ceil($totalpercw/$pembagi); 
                    @endphp                    
                    <input type="hidden" name="jml{{$key+1}}" id="jml{{$key+1}}" value="{{$jml}}">
                    <input type="hidden" name="i_cw{{$key+1}}" id="i_cw{{$key+1}}" value="{{$item->id_cw}}">
                    @for($i=1; $i<=$jml; $i++)
                    <div class="col-md-3">
                        <div class="table-responsive pt-1">
                        <table class="table table-bordered" id="table-harga">
                            <thead style="background-color: lightcyan;">
                                <tr>
                                    <th colspan="3" style="text-align:center;">{{$item->e_uraian_pekerjaan}}</th>
                                <tr>
                                <tr>
                                    <th style="text-align:center;">Roll</th>                                    
                                    <th style="text-align:center;width:150px;">Asal SJ</th>
                                    <th style="text-align:center;width:150px;">Asal KP</th>                                    
                                </tr>
                                <tr>
                                @if(($i-1)==0)
                                    <th style="text-align:center;">{{toNum($key)}}</th>
                                    <input type="hidden" name="kode_dyeing{{toNum($key)}}{{$i}}" name="kode_dyeing{{toNum($key)}}{{$i}}" value="{{toNum($key)}}"/>
                                @else 
                                    <th style="text-align:center;">{{toNum($key)}}{{$i-1}}</th>
                                    <input type="hidden" name="kode_dyeing{{toNum($key)}}{{$i}}" name="kode_dyeing{{toNum($key)}}{{$i}}" value="{{toNum($key)}}{{$i-1}}"/>
                                @endif
                                    <th style="text-align:center;">Kg</th>
                                    <th style="text-align:center;">Kg</th>                                    
                                </tr>                                
                            </thead>
                            <tbody>
                                @if(($i-1)==0)
                                    @php $number = toNum($key); @endphp
                                @else
                                    @php $number = toNum($key).($i-1); @endphp
                                @endif

                                @for($batas=1; $batas<=$ori_cond; $batas++)
                                {{-- @php var_dump($number.'-'.$batas); @endphp --}}
                                <tr>
                                    <td align="center">{{ $batas }}</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number.'-'.$batas}}" id="roll_kg{{$number.'-'.$batas}}" onkeyup="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number.'-'.$batas}}" id="asal_kp{{$number.'-'.$batas}}" onkeyup="hanyaangka()"></td>
                                </tr>
                                @endfor                                
                                {{-- Lama --}}
                                {{-- <tr>
                                    <td align="center">1</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number}}1" id="roll_kg{{$number}}1" onkeydown="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number}}1" id="asal_kp{{$number}}1" onkeydown="hanyaangka()"></td>
                                </tr>
                                <tr>
                                    <td align="center">2</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number}}2" id="roll_kg{{$number}}2" onkeydown="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number}}2" id="asal_kp{{$number}}2" onkeydown="hanyaangka()"></td>
                                </tr>
                                <tr>
                                    <td align="center">3</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number}}3" id="roll_kg{{$number}}3" onkeydown="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number}}3" id="asal_kp{{$number}}3" onkeydown="hanyaangka()"></td>
                                </tr>
                                <tr>
                                    <td align="center">4</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number}}4" id="roll_kg{{$number}}4" onkeydown="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number}}4" id="asal_kp{{$number}}4" onkeydown="hanyaangka()"></td>
                                </tr>
                                <tr>
                                    <td align="center">5</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number}}5" id="roll_kg{{$number}}5" onkeydown="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number}}5" id="asal_kp{{$number}}5" onkeydown="hanyaangka()"></td>
                                </tr>
                                <tr>
                                    <td align="center">6</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="roll_kg{{$number}}6" id="roll_kg{{$number}}6" onkeydown="hanyaangka()"></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$number}}6" id="asal_kp{{$number}}6" onkeydown="hanyaangka()"></td>
                                </tr> --}}
                            </tbody>
                        </table>
                        </div>
                    </div>
                    @endfor
                    @endif
                    @php $cw = $item->e_uraian_pekerjaan; @endphp
                @endforeach
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <button type="button" onclick="return simpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>
                <button type="button" onclick="loadNewPage('{{ route('rfp_matrix.kartu_produksi') }}')"
                    class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            </div>
        </div>
    </form>
</div>
@endif
@include('kartu_produksi.rfp_info.action')
@endsection