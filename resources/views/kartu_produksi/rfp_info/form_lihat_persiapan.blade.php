@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="card">
    <div class="card-body text-dark">
        <h6 class="card-title">Persiapan {{$totalroll[0]->i_no_rfp.', total roll = '.$totalroll[0]->roll.', total cw = '.$totalroll[0]->total_cw}}</h6>
        <div class="row">         
            <input type="hidden" name="totalcw" id="totalcw" value="{{count($data)}}">
            <input type="hidden" name="count_by_ori_cond" name="count_by_ori_cond" value="{{$ori_cond}}"/>
        @foreach($data as $key => $item)                    
                @php 
                    $e_kode = \DB::select("select e_kode from mst_packing_list where i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' group by e_kode order by left(e_kode, 1),COALESCE(substring(e_kode,'\d+')::int,0)");  
                    $qHarga = \DB::select("SELECT v_harga_sat FROM mst_so_item WHERE i_id='".$item->id_cw."'");
                    $jml = count($e_kode);
                @endphp                                             
            <input type="hidden" name="jml{{$key+1}}" id="jml{{$key+1}}" value="{{$jml}}">
            <input type="hidden" name="id_cw{{$key+1}}" id="id_cw{{$key+1}}" value="{{$item->id_cw}}"/>
            <input type="hidden" name="hrg_cw{{$key+1}}" id="hrg_cw{{$key+1}}" value="{{$qHarga[0]->v_harga_sat}}"/>
            <input type="hidden" name="tot_cw{{$key+1}}" id="tot_cw{{$key+1}}" value=""/>
            @foreach($e_kode as $kiy => $kode)
                @php
                    $list_pack = \DB::select("select * from mst_packing_list where e_kode='".$kode->e_kode."' AND i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' order by i_id");
                @endphp                    
            <div class="col-md-4">
                <div class="table-responsive pt-1">
                <table class="table table-bordered" id="table-harga">
                    <thead style="background-color: lightcyan;">
                        <tr>
                            <th colspan="5" style="text-align:center;">{{$item->e_uraian_pekerjaan}}</th>
                        <tr>
                        <tr>
                            <th style="text-align:center;">Roll</th>                                    
                            <th style="text-align:center;width:150px;">Asal SJ</th>
                            <th style="text-align:center;width:150px;">Asal KP</th>
                            <th style="text-align:center;width:150px;">SJ</th>                            
                        </tr>
                        <tr>                          
                        @php $kd = substr($kode->e_kode,0,1); @endphp      
                            <th style="text-align:center;">{{$kode->e_kode}}</th>
                            <input type="hidden" name="kode_dyeing{{$kd}}{{$kiy+1}}" name="kode_dyeing{{$kd}}{{$kiy+1}}" value="{{$kode->e_kode}}"/>
                            <th style="text-align:center;">Kg</th>
                            <th style="text-align:center;">Kg</th>
                            <th style="text-align:center;">Stat</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $asal_sj=0;
                            $asal_kp=0;
                        @endphp
                    @foreach($list_pack as $koy => $lp)
                        <tr>
                            <td align="center"><input type="hidden" id="id_roll_pack{{$kode->e_kode}}{{$koy+1}}" name="id_roll_pack{{$kode->e_kode}}{{$koy+1}}" value="{{$lp->i_id}}">{{$koy+1}}</td>
                            <td><input type="text" style="text-align:right;" class="form-control" name="asal_sj{{$kode->e_kode}}{{$koy+1}}" id="asal_sj{{$kode->e_kode}}{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_asal_sj}}" readonly></td>
                            <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$kode->e_kode}}{{$koy+1}}" id="asal_kp{{$kode->e_kode}}{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_asal_kp}}" readonly></td>
                            <td align="center">{{$lp->f_jadi_sj?"Sudah":"Belum"}}</td>
                        </tr>          
                        @php
                            $asal_sj+=$lp->n_asal_sj;
                            $asal_kp+=$lp->n_asal_kp;
                        @endphp                                                      
                    @endforeach                    
                    @for($koy=count($list_pack)+1;$koy<=$ori_cond;$koy++)
                        <tr>
                            <td align="center"><input type="hidden" id="id_roll_pack{{$kode->e_kode}}{{$koy}}" name="id_roll_pack{{$kode->e_kode}}{{$koy}}" value="">{{$koy}}</td>
                            <td><input type="text" style="text-align:right;" class="form-control" name="asal_sj{{$kode->e_kode}}{{$koy}}" id="asal_sj{{$kode->e_kode}}{{$koy}}" onkeydown="hanyaangka()" value="" readonly></td>
                            <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$kode->e_kode}}{{$koy}}" id="asal_kp{{$kode->e_kode}}{{$koy}}" onkeydown="hanyaangka()" value="" readonly></td>
                            <td align="center">-</td>
                        </tr>
                    @endfor
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total</td>
                            <td align="center">{{ number_format($asal_sj,2) }}</td>
                            <td align="center">{{ number_format($asal_kp,2) }}</td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>                        
            @endforeach                                                
        @endforeach                   
        </div>
    </div>
    <div class="card">
        <div class="card-body text-dark">
            {{-- <button type="button" onclick="return update()" class="btn btn-success btn-icon-text">
                <i class="mdi mdi-file-check btn-icon-prepend"></i>
                Simpan
            </button> --}}
            <button type="button" onclick="loadNewPage('{{ route('rfp_matrix.kartu_produksi') }}')"
                class="btn btn-warning btn-icon-text">
                <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                Kembali
            </button>
        </div>
    </div>
</div>
@endsection