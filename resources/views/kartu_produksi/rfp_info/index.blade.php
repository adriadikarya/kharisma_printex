@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List JO Information</h4>
            <p class="card-description">
                <!-- <button type="button" onclick="loadNewPage('{{ route('form.sales_order') }}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Tambah Data
                </button> -->
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable_rfp">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>JO Number</th>
                            <th>SO Number</th>                            
                            <th>Customer</th>
                            <th>Date Approved Mng Produksi</th>
                            <th>Ori Cond</th>
                            <th>Total CW</th>
                            <th>Total Roll</th>
                            <th>Status</th>                                                        
                            <th>WorkStation</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($data)
                        @foreach($data as $row)
                        <tr>
                            <td>
                            <div class="dropdown">
                                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                                <i class="mdi mdi-apps"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                                    <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp({{$row->id_rfp}},'rfp_matrix.kartu_produksi')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>                                                                        
                                    @php
                                        $id_user = Auth::user()->id;
                                        if($id_user==9999 || $id_user==1){
                                            $data = \DB::select("SELECT * FROM tx_rfp_accepted WHERE i_id_rfp='".$row->id_rfp."' AND user_accept in ('1','9999')");
                                        } else {
                                            $data = \DB::select("SELECT * FROM tx_rfp_accepted WHERE i_id_rfp='".$row->id_rfp."' AND user_accept = '".$id_user."'");
                                        }                                        
                                    @endphp
                                    @if(empty($data))
                                        <div class="dropdown-divider"></div>                        
                                        <a class="dropdown-item" style="cursor: pointer;" onclick="acceptRfp({{$row->id_rfp}})"><i class="mdi mdi-plus-circle-outline"></i> Accept JO</a>
                                    @elseif(!empty($data) && (Auth::user()->role==99 || Auth::user()->role==6 || Auth::user()->role==7))
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="persiapan({{$row->id_rfp}},{{ $row->e_original_cond1 }})"><i class="mdi mdi-plus-circle-outline"></i> Create Code</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="lihat_persiapan({{$row->id_rfp}},{{ $row->e_original_cond1 }})"><i class="mdi mdi-clipboard-check"></i> Look Code</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" style="cursor: pointer" onclick="edit_persiapan({{$row->id_rfp}},{{ $row->e_original_cond1 }})"><i class="mdi mdi-lead-pencil"></i> Edit Code</a>
                                    @endif
                                </div>
                            </div>
                            </td>
                            <td>{{$row->i_no_rfp}}</td>
                            <td>{{$row->i_no_so}}</td>
                            <td>{{$row->e_nama_pel}}</td>                            
                            <td>{{date('d F Y H:i:s', strtotime($row->d_approved_pro))}}</td>                            
                            <td>{{$row->ori_kond}}</td>
                            <td>{{$row->totalcw}}</td>
                            <td>{{$row->total_roll}}</td>
                            <td>{{$row->definition}}</td>
                            <td>{{$row->nama_bagian}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" style="text-align:center;">Sorry Your Data Doesn't Exist</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('kartu_produksi.rfp_info.action')
@endsection
