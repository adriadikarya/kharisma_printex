<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 

/* var table = $('#datatable_rfp').DataTable({
    columnDefs: [ {
      targets: [0,1,2,3,4,5,6,7,8],      
      orderable: false,
    }],    
    // order: ['4', 'desc']
}); */

function lihatRfp(id,link)
{
    loadNewPage(base_url + '/rfp_matrix/kartu_produksi/lihat/' +id +'/'+ link);
}

function acceptRfp(id)
{
    swal({   
        title: "JO Infomation Accepted",   
        text: "Yakin Sudah Melihat?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/home/accept/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        // loadNewPage('{{ route('rfp_matrix.kartu_produksi') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi diaccept','info');
        }
    });
}

function persiapan(id,ori_cond)
{
    $.ajax({
        type: "GET",
        url: base_url + '/rfp_matrix/cek_perisapan/'+ id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {
            
        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);
        if(data.code==1){
            swal({
                title: "Persiapan",
                text: "Masukan Angka Pembagi Untuk Kode Dyeing?",
                type: "input",
                inputPlaceholder: "Angka",
                showCancelButton: true,
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Lanjut",                     
                cancelButtonText: "Tidak", 
                closeOnConfirm: true,                      
            }, function (inputValue) {        
                if (inputValue === false) return false;
                if (inputValue.length>2){
                    alert('Tidak boleh lebih dari 2 digit');
                    return false;
                }        
                if (inputValue === "") {
                    alert('Angka harus diisi!');
                    return false;
                } else {     
                    loadNewPage(base_url + '/rfp_matrix/persiapan/' +id +'/'+ inputValue +'/'+ ori_cond);       
                }        
            });
        } else {
            swal('Peringatan',data.msg,'warning');
        }
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
}

function lihat_persiapan(id,ori_cond)
{
    $.ajax({
        type: "GET",
        url: base_url + '/rfp_matrix/cek_lihat_persiapan/'+ id,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {
            
        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);
        if(data.code==1){
            loadNewPage(base_url + '/rfp_matrix/lihat_persiapan/' +id+'?ori_cond='+ori_cond); 
        } else {
            swal('Peringatan',data.msg,'warning');
        }
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
}

function edit_persiapan(id, ori_cond)
{    
    loadNewPage(base_url + '/rfp_matrix/edit_persiapan/' +id+'?ori_cond='+ori_cond); 
}

function update()
{
    /* $('#form_persiapan').validate({
        rules: {   
            roll_kgA1:{
                required: !0
            },
            roll_kgB1:{
                required: !0
            },
            roll_kgC1:{
                required: !0
            },
            roll_kgD1:{
                required: !0
            },
            roll_kgE1:{
                required: !0
            },
            roll_kgF1:{
                required: !0
            },
            roll_kgG1:{
                required: !0
            },
            roll_kgH1:{
                required: !0
            },
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.css('margin','19%');
            label.css('font-size','16px');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){ */
            var form_kartu = $('#form_persiapan');
            var _form_data = new FormData(form_kartu[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/rfp_matrix/update_persiapan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('rfp_matrix.kartu_produksi') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data Persiapan Tidak Jadi Diupdate :)", "error");   
                } 
            });
       /*  }
    }); */
}

function simpan()
{
    /* var roll_kgA1 = $('#roll_kgA-1').val();
    var roll_kgB1 = $('#roll_kgB-1').val();
    var roll_kgC1 = $('#roll_kgC-1').val();
    var roll_kgD1 = $('#roll_kgD-1').val();
    var roll_kgE1 = $('#roll_kgE-1').val();
    var roll_kgF1 = $('#roll_kgF-1').val();
    var roll_kgG1 = $('#roll_kgG-1').val();
    var roll_kgH1 = $('#roll_kgH-1').val();

    var asal_kpA1 = $('#asal_kpA-1').val();
    var asal_kpB1 = $('#asal_kpB-1').val();
    var asal_kpC1 = $('#asal_kpC-1').val();
    var asal_kpD1 = $('#asal_kpD-1').val();
    var asal_kpE1 = $('#asal_kpE-1').val();
    var asal_kpF1 = $('#asal_kpF-1').val();
    var asal_kpG1 = $('#asal_kpG-1').val();
    var asal_kpH1 = $('#asal_kpH-1').val();

    if(((roll_kgA1=='' && roll_kgA1==0) && (roll_kgB1=='' && roll_kgB1==0) && (roll_kgC1=='' && roll_kgC1==0)&& (roll_kgD1=='' && roll_kgD1==0) && (roll_kgE1=='' && roll_kgE1==0) && (roll_kgF1=='' && roll_kgF1==0) && (roll_kgG1=='' && roll_kgG1==0) && (roll_kgH1=='' && roll_kgH1==0)) || ((asal_kpA1=='' && asal_kpA1==0) && (asal_kpB1=='' && asal_kpB1==0) && (asal_kpC1=='' && asal_kpC1==0)&& (asal_kpD1=='' && asal_kpD1==0) && (asal_kpE1=='' && asal_kpE1==0) && (asal_kpF1=='' && asal_kpF1==0) && (asal_kpG1=='' && asal_kpG1==0) && (asal_kpH1=='' && asal_kpH1==0))) {
        swal('Peringatan','Tolong Isi Salah Satu atau Keduanya minimal 1 Antara Asal SJ atau Asal KP!!!', 'warning');
    } else { */
        var form_kartu = $('#form_persiapan');
        var _form_data = new FormData(form_kartu[0]);

        swal({   
            title: "Anda Yakin?",   
            text: "Yakin Data Sudah Benar Semua?",   
            type: "info",                       
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",                     
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false,
            showLoaderOnConfirm: true 
        }, function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    type: "POST",
                    url: base_url + '/rfp_info/save_persiapan',
                    data: _form_data,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('#loading').show();
                    },
                    success: function(res) {
                        
                    },
                }).done(function(res){
                    var data = $.parseJSON(res);
                    if(data.code==1){
                        swal({   
                            title: "Sukses",   
                            text: data.msg,   
                            type: "success",                       
                            showCancelButton: false,   
                            confirmButtonColor: "#e6b034",   
                            confirmButtonText: "Ya",                     
                            closeOnConfirm: true,                                       
                            showLoaderOnConfirm: true 
                        }, function(isConfirm){   
                            if(isConfirm){
                                loadNewPage('{{ route('rfp_matrix.kartu_produksi') }}');
                            }
                        });
                    } else {
                        swal('Gagal',data.msg,'error');
                    }
                }).fail(function(xhr,textStatus,errorThrown){
                    $('#loading').hide();
                    swal(textStatus,errorThrown,'error');
                });                        
            } else {     
                swal("Batal", "Data Persiapan Tidak Jadi Disimpan :)", "error");   
            } 
        });
    // }
}
</script>