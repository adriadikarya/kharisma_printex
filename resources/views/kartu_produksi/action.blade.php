<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
}); 
var act_url = '{{ route('data.kartu_produksi') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true, 
    lengthMenu: [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],   
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', orderable: false},
        { data: 'urutan', name: 'urutan', orderable: false},
        { data: 'i_no_so', name: 'i_no_so', orderable: false},
        { data: 'e_nama_pel', name: 'e_nama_pel', orderable: false},
        { data: 'i_desain', name: 'i_desain', orderable: false},
        { data: 'f_repeat', name: 'f_repeat', orderable: false},        
        { data: 'hitungcw', name: 'hitungcw', orderable: false},
        { data: 'roll', name: 'roll', orderable: false},        
        { data: 'kg', name: 'kg', orderable: false},        
        { data: 'nama_bagian', name: 'nama_bagian', orderable: false},
        { data: 'alasan_pindah_slot', name: 'alasan_pindah_slot', orderable: false},
        { data: 'keterangan_kp', name: 'keterangan_kp', orderable: false},        
    ],
    // dom: 'Blfrtip',
    dom: 'lBfrtip',
    buttons: [ {
        extend: 'print',
        exportOptions: {
            columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ]
        },
    } ],
});

var act_url = '{{ route('data_history.kartu_produksi') }}';
var table = $('#datatable_history').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true, 
    lengthMenu: [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],   
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action', orderable: false},
        { data: 'urutan', name: 'urutan', orderable: false},
        { data: 'i_no_kartu', name: 'i_no_kartu', orderable: false},
        { data: 'i_no_rfp', name: 'i_no_rfp', orderable: false},
        { data: 'i_no_so', name: 'i_no_so', orderable: false},
        { data: 'e_nama_pel', name: 'e_nama_pel', orderable: false},
        { data: 'd_selesai', name: 'd_selesai', orderable: false},
        { data: 'd_beres', name: 'd_beres', orderable: false },        
    ],
    // dom: 'Blfrtip',
    dom: 'lBfrtip',
    buttons: [ {
        extend: 'print',
        exportOptions: {
            columns: [ 1, 2, 3, 4, 5, 6, 7 ]
        },
    } ],
});

function lihatRfp(id,link)
{
    loadNewPage(base_url + '/rfp/lihat/' +id +'/'+ link);
}

function lihatKartu(id, link)
{
    loadNewPage(base_url + '/kartu_produksi/lihat_kartu/' +id +'/'+ link);
}

function lihatSO(id,link)
{
    loadNewPage(base_url + '/sales_order/lihat/' +id +'/'+ link);
}

function printSO(id,link)
{
    loadNewPage(base_url + '/sales_order/print/' +id +'/'+ link);   
}

function buatKartu(id)
{
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Buat Kartu Produksi?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: true,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {             
            $.ajax({
                type: "GET",
                url: base_url + '/kartu_produksi/cek_accept_rfp/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                if(data.code==1){
                    loadNewPage(base_url + '/kartu_produksi/create/' +id);
                } else {
                    swal('Peringatan',data.msg,'warning');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });            
        } else {
            swal('Batal', 'Tidak Jadi Buat Kartu Produksi!', 'error');
        }
    });
}

function acceptRfp(id)
{
    swal({   
        title: "JO Infomation Accepted",   
        text: "Yakin Sudah Melihat?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/home/accept/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        // loadNewPage('{{ route('kartu_produksi.index') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data tidak jadi diaccept','info');
        }
    });
}

function addRowSJ()
{
    var counter = parseInt($('#row_sj').val())+1;
    var cols = "";
    cols +='<input type="text" class="form-control form-control-sm border-dark" placeholder="Nomor SJ '+counter+'" name="sj'+counter+'" id="sj'+counter+'" maxlength="25" minlength="3">'; 
    $("#html_sj").append(cols);
    $('#row_sj').val(counter);       
}

function removeRowSJ()
{
    var counter = parseInt($('#row_sj').val());
    // console.log(counter);
    if(counter>1)
    {
        $('#sj'+counter).remove(); 
        $('#idsj'+counter).remove();       
        counter -= 1;
        $('#row_sj').val(counter);	
    } else {
        // $('#sj'+counter).remove();
        $('#row_sj').val(1);   
    }
}

function addRowJP()
{
    var counter = parseInt($('#row_jp').val())+1;
    var cols = "";
    cols +='<input type="text" class="form-control form-control-sm border-dark" placeholder="JENIS PROSES '+counter+'" name="jp'+counter+'" id="jp'+counter+'" maxlength="25" minlength="3">'; 
    $("#html_jp").append(cols);
    $('#row_jp').val(counter);       
}

function removeRowJP()
{
    var counter = parseInt($('#row_jp').val());
    // console.log(counter);
    if(counter>1)
    {
        $('#jp'+counter).remove(); 
        $('#idjp'+counter).remove();       
        counter -= 1;
        $('#row_jp').val(counter);	
    } else {
        // $('#jp'+counter).remove();
        $('#row_jp').val(1);   
    }
}

function prosesSimpan(text)
{
    $('#form_kartu').validate({
        rules: {            
            no_kartu: {
                maxlength: 25
            },
            lebar_blanket: {                
                maxlength: 75
            },
            handfeel: {                
                maxlength: 75                
            },
            keterangan: {                
                minlength: 10,
            },
        },
        messages: {
            no_kartu: {
                required: "Tolong isi dulu no kartu nya ya :)",
                maxlength: "Nomor Kartu Tidak boleh lebih dari 25 karakter ya :)"
            },
            lebar_blanket: {
                required: "Tolong isi dulu lebar di blanket nya ya :)",
                maxlength: "Lebar di blanket Tidak boleh lebih dari 75 karakter ya :)"
            },
            handfeel: {
                required: "Tolong diisi handfeel nya ya :)",
                maxlength: "Handfeel  tidak boleh lebih dari 75 karakter :)"
            },
            keterangan: {
                minlength: "Keterangan minimal 10 karakter"                
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            label.css('margin','19%');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_kartu = $('#form_kartu');
            var _form_data = new FormData(form_kartu[0]);

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/kartu_produksi/'+text,
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('kartu_produksi.index') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data Kartu Produksi Tidak Jadi Di"+text+" :)", "error");   
                } 
            });
        }
    });
    $('input[id^="sj"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,            
            maxlength: 50,
            minlength: 3, 
            messages: {
                required: "Tolong Diisi dulu Nomor SJ nya ya :)",
                minlength: "Tolong Minimal Nomor SJ adalah 3 karakter :)",
                maxlength: "Tolong Minimal Nomor SJ adalah 50 karakter :)"
            }
        });
    });
    $('input[id^="jp"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,            
            maxlength: 50,
            minlength: 3, 
            messages: {
                required: "Tolong Diisi dulu Nomor JP nya ya :)",
                minlength: "Tolong Minimal Nomor JP adalah 3 karakter :)",
                maxlength: "Tolong Minimal Nomor JP adalah 50 karakter :)"
            }
        });
    });
    $('input[id^="roll"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0, 
            messages: {
                required: "harus diisi!"
            }                       
        });
    });
    $('input[id^="kg"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,
            messages: {
                required: "harus diisi!"
            }                        
        });
    });
    $('input[id^="pjg"]').each(function (k,v) {        
        $(this).rules('add', {
            required: !0,
            messages: {
                required: "harus diisi!"
            }                        
        });
    });
}

function editKartu(id,link)
{
    loadNewPage(base_url + '/kartu_produksi/edit_kartu/' + id);
}

function printKartu(id_kartu,link)
{
    loadNewPage(base_url + '/kartu_produksi/print/' +id_kartu +'/'+ link); 
    // window.open(base_url + '/kartu_produksi/print/' +id_kartu +'/'+ link);
}

function downloadKartu(id_kartu)
{
    window.open(base_url + '/kartu_produksi/download/' +id_kartu);
}

function detailWorkStation(id_rfp,id_so,link)
{
    loadNewPage(base_url + '/workstation/detail/' +id_rfp+'/'+id_so+'/'+link);
}

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function pindahSlot(id_rfp){
    swal({
        title: "Pindah Slot",
        text: "Masukan alasan kenapa harus pindah slot!",
        type: "input",
        inputPlaceholder: "Alasan Pindah Slot",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Lanjut",                     
        cancelButtonText: "Tidak", 
        closeOnConfirm: false,                      
    }, function (inputValue) {        
        if (inputValue === false) return false;  
        if (inputValue.length>100) {
            alert('Alasan tidak boleh lebh dari 100 karakter');
            return false;
        }      
        if (inputValue === "") {
            alert('Alasan harus diisi!');
            return false;
        } else {     
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Slot Akan Di Pindah?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "GET",
                        url: base_url + '/kartu_produksi/pindah_slot/'+id_rfp+'?alasan_pindah='+inputValue,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);                
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('kartu_produksi.index') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data Kartu Produksi Tidak Jadi Di Pindah Slot :)", "error");   
                } 
            });                
        }        
    });
}

function hitungRoll(qty){    
    var baris = $('#count_cw').val();
    var totAll = 0; 
    if($(qty).val()==""){        
        for(var i=1; i<=baris; i++){
            var roll = $('#roll'+i).val()==""?$('#roll'+i).val():roll;
            roll = $('#roll'+i).val() || '0';
            totAll += parseFloat(formatulang(roll));
        }
    } else {
        for(var i=1; i<=baris; i++){
            var roll = $('#roll'+i).val()==""?$('#roll'+i).val():roll;
            roll = $('#roll'+i).val() || '0';
            totAll += parseFloat(formatulang(roll));
        }
    }
    $('#tot_roll').val(formatuang(totAll.toFixed(2)));
}

function hitungKg(qty){    
    var baris = $('#count_cw').val();
    var totAll = 0; 
    if($(qty).val()==""){
        $(qty).val(0);
        for(var i=1; i<=baris; i++){
            var kg = $('#kg'+i).val()==""?$('#kg'+i).val():kg;
            kg = $('#kg'+i).val() || '0';
            totAll += parseFloat(formatulang(kg));
        }
    } else {
        for(var i=1; i<=baris; i++){
            var kg = $('#kg'+i).val()==""?$('#kg'+i).val():kg;
            kg = $('#kg'+i).val() || '0';
            totAll += parseFloat(formatulang(kg));
        }
    }
    console.log(totAll)
    $('#tot_kg').val(formatuang(totAll.toFixed(2)));
}

function hitungPjg(qty){    
    var baris = $('#count_cw').val();
    var totAll = 0; 
    if($(qty).val()==""){
        $(qty).val(0);
        for(var i=1; i<=baris; i++){
            var pjg = $('#pjg'+i).val()==""?$('#pjg'+i).val():pjg;
            pjg = $('#pjg'+i).val() || '0';
            totAll += parseFloat(formatulang(pjg));
        }
    } else {
        for(var i=1; i<=baris; i++){
            var pjg = $('#pjg'+i).val()==""?$('#pjg'+i).val():pjg;
            pjg = $('#pjg'+i).val() || '0';
            totAll += parseFloat(formatulang(pjg));
        }
    }
    console.log(totAll)
    $('#tot_pjg').val(formatuang(totAll.toFixed(2)));
}
</script>