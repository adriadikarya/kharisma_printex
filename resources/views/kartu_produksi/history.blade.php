@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Daftar History Kartu</h4>
            <p class="card-description">
                <!-- <button type="button" onclick="printDiv('printAble2')" class="btn btn-info btn-icon-text">
                    <i class="mdi mdi-printer btn-icon-prepend"></i>
                    Print
                </button> -->
            </p>            
            <div class="table-responsive pt-3">
            <!-- <div id="printAble2"> -->
                <table class="table table-bordered table-striped" id="datatable_history" width="100%">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>No</th>
                            <th>No Production Card</th>
                            <th>No JO</th>                            
                            <th>No SO</th>
                            <th>Customer</th>                                                        
                            <th>Target Done</th>                                                                                                                
                            <th>Date Done</th>                                                                                                              
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            <!-- </div> -->
            </div>
        </div>
    </div>
</div>
@include('kartu_produksi.action')
@endsection
