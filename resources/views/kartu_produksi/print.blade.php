@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@php
date_default_timezone_set('Asia/Jakarta');
@endphp
<style type="text/css" media="print">    
    .noDisplay {
    display: none;
    }    
    
    @page{
        /* size: 21cm 29.7cm;
        margin: 0.05in 0.79in 0.10in 0.60in; */
        margin: 0cm;
    }

    .isinya {
        font-family: Helvetica, Geneva, Arial,
            SunSans-Regular, sans-serif;
        font-size: 12px;
    }

    .pageBreak {
        page-break-before: always;
    }
</style>
<style type="text/css">
    .warna-hitam {
        color:black;
    }
</style>
<div class="col-12-grid-margin">
    @if($param['data'])    
    @foreach($param['data'] as $item)
    <div class="card">
        <div class="card-body text-dark">
            <div id="printAble">
                <div class="row">
                    <div class="col-md-12">        
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="font-size:15px;" class="warna-hitam"><b>PT KHARISMA PRINTEX<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BANDUNG</b></td>                                
                                    <td colspan="2" style="text-align:right;font-size:15px;"><b>FR.PC.01.12.REV.1</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align:center;font-size:25px;width: 89%" class="warna-hitam">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><b>K A R T U P R O D U K S I</b></u></td>                                
                                    <td rowspan="4">{!! QRCode::text($param['QRCodeNew'])
                                        ->setErrorCorrectionLevel('L')
                                        ->setSize(3)
                                        ->setMargin(1)
                                        ->svg(); !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:34%;">&nbsp;</td>
                                    {{-- <td class="form-inline" style="text-align:center;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>NO KARTU:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" style="width:145px;" value="{{$item->i_no_kartu}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:20px;"><b>NO KARTU &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$item->i_no_kartu}} </b></td>
                                </tr>
                                <tr>
                                    <td style="width:10%;">&nbsp;</td>
                                    {{-- <td class="form-inline" style="text-align:center;font-size:15px;"><b>TANGGAL PENGIRIMAN:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" style="width:145px;" value="{{date('d M Y',strtotime($item->d_selesai))}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:20px;"><b>TANGGAL PENGIRIMAN :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{date('d F Y',strtotime($item->d_selesai))}}</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>                
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="font-size:22px;width:35%;" class="warna-hitam"><b>NOMOR SO/PO</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" value="{{$item->i_no_so.' - '.$item->i_no_po}}" readonly/></td> --}}
                                    <td style="color:black;font-size:22px">@if($item->i_no_po) {{$item->i_no_so.' - '.$item->i_no_po}} @else {{ $item->i_no_so }} @endif</td>
                                </tr>  
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>NOMOR JO</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" value="{{$item->i_no_rfp}}" readonly/></td> --}}
                                    <td style="color:black;font-size:22px;">{{$item->i_no_rfp}}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>NAMA PELANGGAN</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" value="{{$item->e_nama_pel}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;">{{ $item->e_nama_pel }}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>NOMOR DESIGN</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" value="{{$item->i_desain}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;">{{ $item->i_desain }}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>JENIS PRINT</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" value="{{$item->nama_printing}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;">{{ $item->nama_printing }}</td>
                                </tr>                                
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>KONDISI KAIN</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" @if($item->e_ket_ori_cond) value="{{$item->ori_cond1.' '.$item->ori_cond2.' '.$item->e_ket_ori_cond}}" @else value="{{$item->ori_cond1.' '.$item->ori_cond2}}" @endif readonly/></td> --}}
                                    <td style="color:black;font-size:22px;">@if($item->e_ket_ori_cond) {{$item->ori_cond1.' '.$item->ori_cond2.' '.$item->e_ket_ori_cond}} @else {{$item->ori_cond1.' '.$item->ori_cond2}} @endif</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>WARNA DASAR</b></td>
                                    <td style="text-align:right;">:</td>
                                    {{-- <td><input type="text" style="width:100%;" value="{{$item->e_warna_dasar}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;">{{ $item->e_warna_dasar }}</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>NOMOR LPK</b></td>
                                    <td style="text-align:right;">:</td>
                                    <td>
                                @if($param['lpk'])
                                    <div class="row">
                                    @foreach($param['lpk'] as $key => $lpk)                             
                                        @if($key == 0)
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-right:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$lpk->e_nomor_lpk}}" readonly/> --}}
                                            {{ $lpk->e_nomor_lpk }}
                                        </div>
                                        @elseif($key % 2 == 0)
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-right:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$lpk->e_nomor_lpk}}" readonly/> --}}
                                            {{ $lpk->e_nomor_lpk }}
                                        </div>
                                        @else
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-left:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$lpk->e_nomor_lpk}}" readonly/> --}}
                                            {{ $lpk->e_nomor_lpk }}
                                        </div>
                                        @endif
                                    @endforeach
                                    </div>
                                @else                                                                    
                                    <input type="text" style="width:100%;" value="" readonly/>
                                @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>NOMOR SURAT JALAN</b></td>
                                    <td style="text-align:right;">:</td>
                                    <td>
                                @if($param['sj'])
                                    <div class="row">
                                    @foreach($param['sj'] as $key => $sj)                             
                                        @if($key == 0)
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-right:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$sj->i_no_sj}}" readonly/> --}}
                                            {{ $sj->i_no_sj }}
                                        </div>
                                        @elseif($key % 2 == 0)
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-right:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$sj->i_no_sj}}" readonly/> --}}
                                            {{ $sj->i_no_sj }}
                                        </div>
                                        @else
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-left:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$sj->i_no_sj}}" readonly/> --}}
                                            {{ $sj->i_no_sj }}
                                        </div>
                                        @endif
                                    @endforeach
                                    </div>
                                @else
                                    <input type="text" style="width:100%;" readonly/>
                                @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;" class="warna-hitam"><b>JENIS PROSES</b></td>
                                    <td style="text-align:right;">:</td>
                                    <td>
                                @if($param['jp'])
                                    <div class="row">
                                    @foreach($param['jp'] as $key => $jp)                             
                                        @if($key == 0)
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-right:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$jp->description}}" readonly/> --}}
                                            {{ $jp->description }}
                                        </div>
                                        @elseif($key % 2 == 0)
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-right:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$jp->description}}" readonly/> --}}
                                            {{ $jp->description }}
                                        </div>
                                        @else
                                        <div class="col-md-6 warna-hitam mb-2" style="padding-left:0;font-size:22px;">
                                            {{-- <input type="text" style="width:100%;" value="{{$jp->description}}" readonly/> --}}
                                            {{ $jp->description }}
                                        </div>
                                        @endif
                                    @endforeach
                                    </div>
                                @else
                                    <input type="text" style="width:100%;" readonly/>
                                    <input type="text" style="width:100%;" readonly/>
                                    <input type="text" style="width:100%;" readonly/>
                                @endif
                                    </td>
                                </tr>                                    
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tbody>   
                                @if($param['cw'])                              
                                @php $no=1; $roll=0; $kg=0; $meter=0; @endphp
                                @foreach($param['cw'] as $cw)
                                @php $roll+=$cw->n_qty_roll; $kg+=$cw->n_qty_kg; $meter+=$cw->n_qty_pjg; @endphp
                                    <tr>
                                        <td style="font-size:22px;width:50%;" class="warna-hitam"><b>CW {{$no++.' ('.$cw->e_uraian_pekerjaan.')/'.$cw->d_strike_off.'/'.$cw->e_kain}}</b></td>
                                        <td>:</td>
                                        {{-- <td><input type="text" style="width:20%;text-align:right;" value="{{number_format($cw->n_qty_roll,0,',','.')}}" readonly/>&nbsp;Roll&nbsp;&nbsp;&nbsp;<input type="text" style="width:20%;text-align:right;" value="{{number_format($cw->n_qty_kg,0,',','.')}}" readonly/>&nbsp;Kg&nbsp;&nbsp;&nbsp;<input type="text" style="width:20%;text-align:right;" value="{{number_format($cw->n_qty_pjg,0,',','.')}}"readonly/>&nbsp;Meter</td> --}}
                                        <td class="warna-hitam" style="font-size:22px;">{{number_format($cw->n_qty_roll,0,',','.')}}&nbsp;Roll&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($cw->n_qty_kg,2,',','.')}}&nbsp;Kg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($cw->n_qty_pjg,0,',','.')}}&nbsp;Meter</td>
                                    </tr>
                                    <tr>
                                    </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td style="font-size:22px;width:30%;"><b>CW :</b></td>
                                    <td>:</td>
                                    {{-- <td><input type="text" style="width:20%" readonly/>&nbsp;Roll&nbsp;&nbsp;&nbsp;<input type="text" style="width:20%" readonly/>&nbsp;Kg&nbsp;&nbsp;&nbsp;<input type="text" style="width:20%" readonly/>&nbsp;Meter</td> --}}
                                    <td class="warna-hitam" style="font-size:22px;">&nbsp;Roll&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Meter</td>
                                </tr>
                                @endif
                                <tr>
                                    <td style="font-size:22px;width:40%;" class="warna-hitam"><b>TOTAL ORDER</b></td>
                                    <td>:</td> 
                                    {{-- <td><input type="text" style="width:20%;text-align:right;" value="{{number_format($roll,0,',','.')}}" readonly/>&nbsp;Roll&nbsp;&nbsp;&nbsp;<input type="text" style="width:20%;text-align:right;" value="{{number_format($kg,0,',','.')}}" readonly/>&nbsp;Kg&nbsp;&nbsp;&nbsp;<input type="text" style="width:20%;text-align:right;" value="{{number_format($meter,0,',','.')}}" readonly/>&nbsp;Meter</td> --}}
                                    <td class="warna-hitam" style="font-size:22px;">{{number_format($roll,0,',','.')}}&nbsp;Roll&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($kg,2,',','.')}}&nbsp;Kg&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($meter,0,',','.')}}&nbsp;Meter</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td style="font-size:22px;width:30%;" class="warna-hitam"><b>LEBAR DI BLANKET</b></td>
                                    <td>:</td>
                                    {{-- <td><input type="text" style="width:100%" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;"> {{ $item->e_lebar_blanket }} </td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;width:30%;" class="warna-hitam"><b>HANDFEEL</b></td>
                                    <td>:</td>
                                    {{-- <td><input type="text" style="width:100%" value="{{$item->e_tekstur}}" readonly/></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;"> {{ $item->e_tekstur }} </td>
                                </tr>
                                <tr>
                                    <td class="warna-hitam"></td>
                                    <td class="warna-hitam"></td>
                                    <td class="warna-hitam">&emsp;&emsp;Asal&emsp;&emsp;&emsp;Permintaan&emsp;&emsp;&emsp;&nbsp;Jadi</td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;width:30%;" class="warna-hitam"><b>LEBAR KAIN</b></td>
                                    <td>:</td>
                                    <td><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_penyesuaian_lebar_from}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_penyesuaian_lebar_to}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" readonly/></td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;width:30%;" class="warna-hitam"><b>GRAMASI KAIN</b></td>
                                    <td>:</td>
                                    <td><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_gramasi_from}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_gramasi_to}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" readonly/></td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;width:30%;" class="warna-hitam"><b>SHRINKAGE PAKAN</b></td>
                                    <td>:</td>
                                    <td><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_pakan_from}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_pakan_to}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" readonly/></td>
                                </tr>
                                <tr>
                                    <td style="font-size:22px;width:30%;" class="warna-hitam"><b>SHRINKAGE LUSI</b></td>
                                    <td>:</td>
                                    <td><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_lusi_from}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" value="{{$item->e_lusi_to}}" readonly/><input type="text" style="width:29%;text-align:right;margin:5px;font-size:18px;" readonly/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <table class="table" width="100%" style="table-layout:fixed;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;font-size:24px;border:1px solid black;" class="warna-hitam"><b>KETERANGAN TAMBAHAN</b></td>                                    
                                </tr>
                                <tr>
                                    <td style="vertical-align: baseline; border:1px solid black; font-size:22px; word-break: break-all;" height="350" class="warna-hitam">{{ $item->e_ket }}</td>
                                </tr>                                
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-6">&nbsp;</div>
                    <div class="col-sm-6" style="margin-left:-5px;">
                        <table class="table" width="100%" style="table-layout:fixed;">
                            <tbody>
                                <tr>                                    
                                    <td style="text-align:right;font-size:22px;" class="warna-hitam">Bandung,</td>
                                    {{-- <td><input type="text" style="border:1px solid black;text-align:right;" value="{{date('d M Y')}}"></td> --}}
                                    <td class="warna-hitam" style="font-size:22px;"> {{ date('d F Y') }} </td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;text-align:center;font-size:15px;" class="warna-hitam"><b>DISETUJUI OLEH,</b></td>
                                    <td style="border:1px solid black;text-align:center;font-size:15px;" class="warna-hitam" width="50"><b>DIBUAT OLEH,</b></td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;" height="160">&nbsp;</td>
                                    <td style="border:1px solid black;" height="160">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;text-align:center;font-size:18px;" class="warna-hitam"><b>MANAGER PRODUKSI</b></td>
                                    <td style="border:1px solid black;text-align:center;font-size:18px;" class="warna-hitam"><b>KA. PPC</b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">&nbsp;</div>
                {{-- <div class="row">
                    <div class="col-md-12">
                        <table class="table" width="100%" style="table-layout:fixed;">
                            <tbody>
                                <tr>
                                    <td colspan="11" style="text-align:center;font-size:25px;border:1px solid black;"><b>WORK STATION PRODUKSI</b></td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">CW</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">PERMARTAIAN/INSPECTING</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">BELAH GREIGE</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">SCOURING/BLEACHING</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">BUKA KAIN/REVISI</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">HEAT SET/LP/LPP</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">PRINTING</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">CURRING</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">FINISHING</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">QC</td>
                                    <td style="border:1px solid black;word-wrap: break-word;font-size:12px;">DELIVERY</td>
                                </tr>
                                @if($param['cw'])
                                @foreach($param['cw'] as $cw2)
                                <tr>
                                    <td style="border:1px solid black;">{{$cw2->e_uraian_pekerjaan}}</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;">QTY ({{$cw2->n_qty_kg}})</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td style="border:1px solid black;">Tidak Ada Color Ways</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;text-align:right;">QUANTITY</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                    <td style="border:1px solid black;text-align:right;">&nbsp;</td>
                                </tr>                                
                                @endif
                                <tr>
                                    <td style="border:1px solid black;">TANGGAL</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;">WAKTU PROSES</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;">PELAKSANA</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;">SHIFT</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="border:1px solid black;">PARAF</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                    <td style="border:1px solid black;">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="card">
        <div class="card-body text-dark">
            <h4 class="card-title">Daftar PR yang akan dibuat Kartu Produksi</h4>
            <p class="card-description">
                <center>MAAF TIDAK ADA DATA KARTU YANG AKAN DI PRINT</center>
            </p>
        </div>
    </div>
    @endif
    <div class="card noDisplay">
        <div class="card-body">                
            <button type="button" onclick="loadNewPage('{{ route($link) }}')" class="btn btn-warning btn-icon-text">
                <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                Kembali
            </button>
            <button type="button" onclick="printDiv('printAble')" class="btn btn-info btn-icon-text">
                <i class="mdi mdi-printer btn-icon-prepend"></i>
                Print
            </button>
        </div>
    </div>
</div>
<script type="text/javascript">
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
@endsection
