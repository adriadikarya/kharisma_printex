<script>
var act_url = '{{ route('rfp_known.data') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_rfp', name: 'i_no_rfp' },
        { data: 'i_no_so', name: 'i_no_so' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },        
        { data: 'd_approved_mrk', name: 'd_approved_mrk' },
        { data: 'd_approved_pro', name: 'd_approved_pro' },        
        { data: 'definition', name: 'definition' },
        { data: 'nama_bagian', name: 'nama_bagian' },
    ]
});

function detAcc(id_rfp,link)
{
    loadNewPage(base_url + '/rfp_known/detail/' +id_rfp+'/'+link);
}
</script>