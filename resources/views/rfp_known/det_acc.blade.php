@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Detail Accepted</h4>
            <p class="card-description">
                <button type="button" onclick="loadNewPage('{{ route($rut) }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
            <div class="table-responsive pt-3">            
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr style="background-color:powderblue;">                            
                            <th style="font-weight: bolder;">No.</th>          
                            <th style="font-weight: bolder;">No. JO</th>                  
                            <th style="font-weight: bolder;">Date Accepted</th>                            
                            <th style="font-weight: bolder;">User Accepted</th>
                        </tr>
                    </thead>
                    <tbody>      
                        @if($data)
                        @php $no=1; @endphp
                            @foreach($data as $item)
                                <tr>
                                    <td style="text-align:center;">{{$no++}}</td>
                                    <td style="text-align:center;">{{$item->i_no_rfp}}</td>
                                    <td style="text-align:center;">{{date('d-m-Y H:i:s', strtotime($item->d_accepted))}}</td>
                                    <td style="text-align:center;">{{$item->name}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" style="text-align:center;">Sorry No One Head Division or Admin Division Accepted JO </td>
                            </tr>
                        @endif                  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection