@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['notahead'])
@foreach($param['notahead'] as $notahead)
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Look A Nota</h4>
            <form class="form" id="form_input_nota">
            @csrf
                {{--<button type="submit" onclick="prosesSimpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>--}}
                <button type="button" onclick="loadNewPage('{{route('keuangan.nota')}}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>                                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">No. Nota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_nota" id="no_nota" value="{{$notahead->i_nota_code}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Tgl Nota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="d_nota" id="d_nota" value="{{date('d M Y', strtotime($notahead->d_nota))}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Pelanggan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="nama_pel" id="nama_pel" value="{{$notahead->e_nama_pel}}" readonly />
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Tgl Jth Tempo</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="d_jth_tempo" id="d_jth_tempo" value="{{date('d M Y', strtotime($notahead->d_due_date))}}" readonly />
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-12">                                                
                        <div class="table-responsive pt-1">
                            <table class="table table-bordered" id="table-harga">
                                <thead style="background-color: lightcyan;">
                                    <tr>
                                        <th colspan="6" align="text-dark" style="text-align: center;vertical-align: middle;">DETAIL NOTA</th>
                                    </tr>
                                    <tr>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">NO.</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">NO SJ</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">TGL SJ</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">QUANTITY</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">HARGA SATUAN (Rp.)</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">JUMLAH HARGA</th>
                                    </tr>                                    
                                </thead>
                                <tbody>                                    
                                    @if($param['notaitem'])  
                                        @foreach($param['notaitem'] as $key => $notaitem)                                            
                                            @php $jml_hrg = $notaitem->n_qty * $notaitem->v_hrg_satuan; @endphp
                                            <tr>
                                                <td align="center">{{$key+1}}</td>
                                                <td align="center">{{$notaitem->i_no_sj}}</td>
                                                <td align="center">{{$notaitem->d_sj}}</td>
                                                <td align="right">{{$notaitem->n_qty}}</td>
                                                <td align="right">{{number_format($notaitem->v_hrg_satuan)}}</td>
                                                <td align="right">{{number_format($jml_hrg)}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" align="center">Maaf Data SJ Tidak Ada!</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" style="border:0;"></td>
                                        <td align="right">Total Qty</td>
                                        <td align="right">{{$notahead->n_tot_qty}}</td>
                                        <td style="text-align:right;">Total</td>
                                        <td align="right">{{number_format($notahead->v_total_nota)}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td align="right" style="text-align:right;">PPN {{$notahead->n_ppn}}%</td></td>
                                        <td align="right">{{number_format($notahead->v_ppn)}}</td>
                                    </tr>
                                    <tr>
                                        @php $total_nota = $notahead->v_total_nota + $notahead->v_ppn; @endphp
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Total Nota</td>
                                        <td align="right">{{number_format($total_nota)}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Diskon {{$notahead->n_diskon}}%</td>
                                        <td align="right">{{number_format($notahead->v_diskon)}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Grand Total</td>
                                        <td align="right">{{number_format($notahead->v_total_nppn_sisa)}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>  
</div>
@endforeach
@endif
@include('keuangan.action')
@endsection