@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['data'])
@foreach($param['data'] as $data)
<div class="col-12 grid-margin">
    <form class="form-sample" id="form_do">
    @csrf
        <div class="card">
            <div class="card-body text-dark">
                <h4 class="card-title">Look Delivery Order</h4>
                {{-- <button type="button" onclick="return prosesSimpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button> --}}
                <button type="button" onclick="loadNewPage('{{ route('list_approved_print_sj') }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                {{--<input type="hidden" name="idkartu" id="idkartu" value="{{$param['idkartu']}}"/>
                <input type="hidden" name="idrfp" id="idrfp" value="{{$param['idrfp']}}"/>
                <input type="hidden" name="lama_jth_tempo" id="lama_jth_tempo" value="{{$data->n_jth_tempo}}"/>
                <input type="hidden" name="tgl_produksi" id="tgl_produksi" value="{{$data->tgl_produksi}}"/>--}}
                <div class="row">&nbsp;</div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">No. SJ</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_sj" id="no_sj" value="{{ $data->i_no_sj }}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kepada</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="pel" id="pel" value="{{ $data->e_nama_pel }}" readonly />
                                <input type="hidden" name="idpel" id="idpel" value="{{$data->i_pel}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tgl Kirim</label>
                            <div class="col-sm-10">
                                {{--<input type="text" class="form-control form-control-sm border-dark" name="tgl_sj" id="tgl_sj" value="" readonly />--}}
                                <input type="text" class="form-control form-control-sm border-dark" name="tgl_sj" id="tgl_sj" value="{{date('d M Y', strtotime($data->d_sj))}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="alamat" id="alamat" value="{{$data->e_alamat_pel}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Tgl Jth Tempo</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="tgl_jth_tempo" id="tgl_jth_tempo" value="{{date('d M Y', strtotime($data->d_due_date))}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Expedisi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm border-dark" name="expedisi" id="expedisi" value="{{$data->expedisi}}" maxlength="16" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label class="col-sm-1 col-form-label">Keterangan</label>
                            <div class="col-sm-11">
                                <textarea type="text" name="keterangan" id="keterangan" maxlength="250" class="form-control form-control-sm border-dark" readonly>{{$data->e_ket}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h6 class="card-title">Detail Delivery Order</h6>
                <div class="row">
                    <div class="col-md-6">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered">                                    
                                <tbody id="list">
                                <tr>
                                    <td>No. Design :</td>
                                    <td>{{$data->i_desain}}</td>
                                </tr>
                                {{--<tr> 
                                    <td>Jenis Kain :</td>
                                    <td>{{$data->e_kain}}</td>
                                </tr>--}}
                                <tr>
                                    <td>Ket Design :</td>
                                    <td>{{$data->e_motif}}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Proses :</td>
                                    <td>
                                    @foreach($param['jp'] as $jp)
                                        {{$jp->description.' '}}
                                    @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jenis Printing :</td>
                                    <td>{{$data->e_jenis_printing}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered">                                    
                                <tbody id="list">
                                <tr>
                                    <td rowspan="3">Pemartaian Jumlah</td>
                                    <td>Roll:</td>
                                    <td>{{$data->roll_pem}}</td>
                                </tr>
                                <tr>                                 
                                    <td>Kg:</td>
                                    <td>{{$data->kg_pem}}</td>
                                </tr>
                                <tr>                                
                                    <td>Mtr:</td>
                                    <td>{{$data->mtr_pem}}</td>
                                </tr>
                                <tr>
                                    <td>Tgl. Produksi :</td>
                                    <td colspan="2">{{date('d M Y',strtotime($data->tgl_produksi))}}</td>
                                </tr>                                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered">                                    
                                <tbody id="list">
                                    <tr>                                    
                                        <td>Total Roll :</td>
                                        <td><input type="text" name="tot_roll" id="tot_roll" class="form-control form-control-sm border-text" style="text-align:right;" value="{{$data->n_tot_roll}}" readonly></td>
                                    </tr>
                                    <tr>                                 
                                        <td>Asal SJ :</td>
                                        <td><input type="text" name="tot_asal_sj" id="tot_asal_sj" class="form-control form-control-sm border-text" style="text-align:right;" value="{{$data->n_tot_asal_sj}}" readonly></td>
                                    </tr>
                                    <tr>                                
                                        <td>Asal KP :</td>
                                        <td><input type="text" name="tot_asal_kp" id="tot_asal_kp" class="form-control form-control-sm border-text" style="text-align:right;" value="{{$data->n_tot_asal_kp}}" readonly></td>
                                    </tr>
                                    <tr>
                                        <td>Jadi KP :</td>
                                        <td><input type="text" name="tot_jadi_kp" id="tot_jadi_kp" class="form-control form-control-sm border-text" style="text-align:right;" value="{{$data->n_tot_jadi_kp}}" readonly></td>
                                    </tr>                                                                          
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive pt-3">
                            <table class="table table-bordered">                                    
                                <tbody id="list">  
                                <tr>
                                    <td>No. SO :</td>
                                    <td>{{$data->i_no_so}}</td>
                                </tr>
                                <tr>
                                    <td>No. PO :</td>
                                    <td>{{$data->i_no_po}}</td>
                                </tr>                              
                                <tr>
                                    <td>Ex. SJ :</td>
                                    <td> 
                                    @php
                                        $i = 0;
                                        $len = count($param['exsj']);
                                    @endphp
                                    @foreach($param['exsj'] as $exsj)                                        
                                        @if ($i == $len - 1)
                                            {{$exsj->i_no_sj}}
                                        @else
                                            {{$exsj->i_no_sj.' +'}}
                                        @endif                                        
                                        @php                       
                                        $i++;
                                        @endphp
                                    @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>LPK :</td>
                                    <td>
                                    @php
                                        $i = 0;
                                        $len = count($param['lpk']);
                                    @endphp
                                    @foreach($param['lpk'] as $lpk)                                        
                                        @if ($i == $len - 1)
                                            {{$lpk->e_nomor_lpk.' ('.$lpk->qty_roll.')'}}
                                        @else
                                            {{$lpk->e_nomor_lpk.' ('.$lpk->qty_roll.')'.' +'}}
                                        @endif                                        
                                        @php                       
                                        $i++;
                                        @endphp                                        
                                    @endforeach
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body text-dark">
                <h6 class="card-title">Packing List Pick</h6>
                <div class="row">         
                    <input type="hidden" name="totalcw" id="totalcw" value="{{count($param['packing'])}}">
                        @foreach($param['packing'] as $key => $item)                    
                        @php 
                            $e_kode = \DB::select("select e_kode from mst_packing_list where i_cw='".$item->id_cw."' AND i_id_sj='".$param['idsj']."' group by e_kode order by e_kode");  
                            $qHarga = \DB::select("SELECT v_harga_sat FROM mst_so_item WHERE i_id='".$item->id_cw."'");                      
                            $jml = count($e_kode);
                        @endphp                                             
                    <input type="hidden" name="jml{{$key+1}}" id="jml{{$key+1}}" value="{{$jml}}">
                    <input type="hidden" name="id_cw{{$key+1}}" id="id_cw{{$key+1}}" value="{{$item->id_cw}}"/>
                    <input type="hidden" name="hrg_cw{{$key+1}}" id="hrg_cw{{$key+1}}" value="{{$qHarga[0]->v_harga_sat}}"/>
                    <input type="hidden" name="tot_cw{{$key+1}}" id="tot_cw{{$key+1}}" value=""/>
                        @foreach($e_kode as $kiy => $kode)
                        @php
                            $list_pack = \DB::select("select * from mst_packing_list where e_kode='".$kode->e_kode."' AND i_cw='".$item->id_cw."' AND i_id_rfp='".$item->i_id."' AND i_id_sj='".$param['idsj']."' order by i_id");                        
                        @endphp                    
                    <div class="col-md-6">
                        <div class="table-responsive pt-1">
                        <table class="table table-bordered" id="table-harga">
                            <thead style="background-color: lightcyan;">
                                <tr>
                                    <th colspan="5" style="text-align:center;">{{$item->e_uraian_pekerjaan}}</th>
                                <tr>
                                <tr>
                                    <th style="text-align:center;">Roll</th>                                    
                                    <th style="text-align:center;width:150px;">Asal SJ</th>
                                    <th style="text-align:center;width:150px;">Asal KP</th>
                                    <th style="text-align:center;width:150px;">Jadi KP</th>                                    
                                </tr>
                                <tr>                          
                                @php $kd = substr($kode->e_kode,0,1); @endphp      
                                    <th style="text-align:center;">{{$kode->e_kode}}</th>
                                    <input type="hidden" name="kode_dyeing{{$kd}}{{$kiy+1}}" name="kode_dyeing{{$kd}}{{$kiy+1}}" value="{{$kode->e_kode}}"/>
                                    <th style="text-align:center;">Kg</th>
                                    <th style="text-align:center;">Kg</th>
                                    <th style="text-align:center;">Kg</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($list_pack as $koy => $lp)
                                <tr>
                                    <td align="center"><input type="hidden" id="id_roll_pack{{$kode->e_kode}}{{$koy+1}}" name="id_roll_pack{{$kode->e_kode}}{{$koy+1}}" value="{{$lp->i_id}}">{{$koy+1}}</td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_sj{{$kode->e_kode}}{{$koy+1}}" id="asal_sj{{$kode->e_kode}}{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_asal_sj}}" readonly></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="asal_kp{{$kode->e_kode}}{{$koy+1}}" id="asal_kp{{$kode->e_kode}}{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_asal_kp}}" readonly></td>
                                    <td><input type="text" style="text-align:right;" class="form-control" name="jadi_kp{{$kode->e_kode}}{{$koy+1}}" id="jadi_kp{{$kode->e_kode}}{{$koy+1}}" onkeydown="hanyaangka()" value="{{$lp->n_jadi_kp}}" readonly></td>                                    
                                </tr>                                                                
                            @endforeach
                            <input type="hidden" name="count_list{{$kode->e_kode}}" value="{{count($list_pack)}}"/>
                            </tbody>
                        </table>
                        </div>
                    </div>                        
                        @endforeach                                                
                        @endforeach                   
                </div>
            </div>
        </div>
    </form>
</div>
@endforeach
@endif
@include('do.action')
@endsection