@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">List Request Print DO</h4>
            <div class="table-responsive pt-3">            
                <table class="table table-bordered table-striped" id="datatable1" width="100%">
                    <thead>
                        <tr>
                            <th>Action</th>                            
                            <th>DO Number</th>                                                                                        
                            <th>Send Date</th>                                                                                            
                            <th>Customer</th>
                            <th>Total Roll</th>
                            <th>Total SJ From</th>
                            <th>Total KP From</th>
                            <th>Total KP To</th>
                            <th>Status</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>            
            </div>
        </div>
    </div>            
</div>
@include('keuangan.action')
@endsection