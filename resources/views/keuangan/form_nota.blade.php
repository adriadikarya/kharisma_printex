@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Make A Nota</h4>
            <form class="form" id="form_input_nota">
            @csrf
                <button type="submit" onclick="prosesSimpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>
                <button type="button" onclick="location.reload()" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>
                <input type="hidden" name="pel" id="pel" value="{{$param['pel']}}"/>
                <input type="hidden" name="qty_pick" id="qty_pick" value="{{$param['qty_pick']}}"/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">No. Nota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_nota" id="no_nota" value="{{$param['no_nota']}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Tgl Nota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_nota" id="d_nota" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Pelanggan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="nama_pel" id="nama_pel" value="{{$param['nama_pel']}}" readonly />
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Tgl Jth Tempo</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_jth_tempo" id="d_jth_tempo" readonly />
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-12">
                        @if($param['qty_pick']==1)
                            @php $name_qty = "asal sj"; @endphp
                        @elseif($param['qty_pick']==2)
                            @php $name_qty = "asal kp"; @endphp
                        @else
                            @php $name_qty = "jadi kp"; @endphp
                        @endif
                        <input type="hidden" name="totrow" id="totrow" value="{{count($param['arr_sj'])}}"/>
                        <div class="table-responsive pt-1">
                            <table class="table table-bordered" id="table-harga">
                                <thead style="background-color: lightcyan;">
                                    <tr>
                                        <th colspan="6" align="text-dark" style="text-align: center;vertical-align: middle;">DETAIL NOTA</th>
                                    </tr>
                                    <tr>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">NO.</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">NO SJ</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">TGL SJ</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">QUANTITY {{strtoupper($name_qty)}}</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">HARGA SATUAN (Rp.)</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">JUMLAH HARGA</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    @php $totqty = 0; @endphp 
                                    @if($param['arr_sj'])  
                                        @foreach($param['arr_sj'] as $key => $arr_sj)
                                            @if($param['qty_pick']==1)
                                                @php $qty = $arr_sj[0]->n_tot_asal_sj; @endphp
                                            @elseif($param['qty_pick']==2)
                                                @php $qty = $arr_sj[0]->n_tot_asal_kp; @endphp
                                            @else
                                                @php $qty = $arr_sj[0]->n_tot_jadi_kp; @endphp
                                            @endif
                                            @php $totqty+= $qty; @endphp
                                            <tr>
                                                <td align="center">{{$key+1}}</td>
                                                <td align="center"><input type="hidden" name="idsj{{$key+1}}" id="idsj{{$key+1}}" value="{{$arr_sj[0]->i_id}}"/>{{$arr_sj[0]->i_no_sj}}</td>
                                                <td align="center">{{$arr_sj[0]->d_sj}}</td>
                                                <td><input type="text" class="form-control form-control-sm border-dark" name="qty_{{$key+1}}" id="qty_{{$key+1}}" style="text-align: right;" onkeyup="formatRp(document.getElementById('qty_{{$key+1}}'));" value="{{$qty}}" readonly/></td>
                                                <td><input type="text" class="form-control form-control-sm border-dark text-right" name="hrg_sat{{$key+1}}" id="hrg_sat{{$key+1}}" value="0" onkeyup="formatRp(this);hitung({{$key+1}});"></td>
                                                <td><input type="text" class="form-control form-control-sm border-dark text-right" name="jml_hrg{{$key+1}}" id="jml_hrg{{$key+1}}" value="0" readonly></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" align="center">Maaf Data SJ Tidak Ada!</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="border:0;"></td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="totqty" id="totqty" value="{{$totqty}}" readonly/></td>
                                        <td style="text-align:right;">Total =</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="total" id="total" value="0" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">PPN = <input type="text" style="text-align:right;width:20%;" class="" name="ppn_percent" id="ppn_percent" onkeyup="hitungPPN();" onkeydown="hanyaangka();" value="0"/>%</td></td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="ppn" id="ppn" value="0" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Total Nota =</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="total_nota" id="total_nota" value="0" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Diskon = <input type="text" style="text-align:right;width:20%;" class="" name="diskon_percent" id="diskon_percent" onkeyup="hitungDownPayment();" onkeydown="hanyaangka();" value="0"/>%</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="diskon_nilai" id="diskon_nilai" value="0" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Grand Total =</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="grand_tot" id="grand_tot" value="0" readonly/></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>  
</div>
@include('keuangan.action')
@endsection