<script>
$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});
var act_url = '{{ route('data_request_print_sj') }}';
var table = $('#datatable1').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'i_no_sj', name: 'i_no_sj' },        
        { data: 'd_sj', name: 'd_sj' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },                
        { data: 'n_tot_roll', name: 'n_tot_roll' },
        { data: 'n_tot_asal_sj', name: 'n_tot_asal_sj' },
        { data: 'n_tot_asal_kp', name: 'n_tot_asal_kp' },
        { data: 'n_tot_jadi_kp', name: 'n_tot_jadi_kp' },
        { data: 'definition', name: 'definition' },
        { data: 'created_at', name: 'created_at' }        
    ]
});

$('#datatable2').DataTable();

var act_url1 = '{{ route('data_nota') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
 
    ajax: {
        "url" : act_url1,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                alert(errorThrown);
            }
        },
    columns: [        
        { data: 'action', name: 'action' },
        { data: 'urutan', name: 'urutan' },
        { data: 'i_nota_code', name: 'i_nota_code' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },        
        { data: 'd_nota', name: 'd_nota' },
        { data: 'd_due_date', name: 'd_due_date' },                                
        { data: 'v_total_nppn', name: 'v_total_nppn' },
        { data: 'f_lunas', name: 'f_lunas' },
        { data: 'f_invoice', name: 'f_invoice' },                
    ]
});

function showSJ(){
    var pel = $('#pel').val();
    var qty_pick = $('#qty_pick').val();
    if(pel==''){
        swal('Peringatan','Tolong pilih dulu Pelanggannya!','warning');
    } else if(qty_pick=='') {
        swal('Peringatan','Tolong pilih dulu Qty yg dijadikan Acuan Hitungan','warning');
    } else {
        $.ajax({
            type: "GET",
            url: base_url + '/keuangan/list_sj/'+pel,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(res) {

            },
        }).done(function(res){
            $('#loading').hide();            
            var data = $.parseJSON(res);   
            var no=1;     
            var tbl_list = '<input type="hidden" name="qty_pick" id="qty_pick" value="'+qty_pick+'">';            
            $.each(data, function(k,v){
                if(qty_pick==1){
                    pilihqty = v.n_tot_asal_sj;
                } else if(qty_pick==2) {
                    pilihqty = v.n_tot_asal_kp;
                } else {
                    pilihqty = v.n_tot_jadi_kp;
                }
                tbl_list+= '<tr><td><a href="#" onclick="pilih(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+pilihqty+'\')">'+v.i_no_sj+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+pilihqty+'\')">'+v.d_sj+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+pilihqty+'\')">'+v.e_nama_pel+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+pilihqty+'\')">'+addCommas(pilihqty)+'</a></td>';
                tbl_list+= '<td>'+
                '<input type="hidden" name="id_sj'+no+'" id="id_sj'+no+'" value="'+v.i_id+'">'+
                '<input type="checkbox" id="f_ck_tblItem'+no+'" name="f_ck_tblItem'+no+'" value="'+v.i_no_sj+'"></td></tr>';
                no++;
            }); 
            tbl_list+= '<input type="hidden" name="iter" id="iter" value="'+(no-1)+'"/>';           
            $('#list').html(tbl_list);
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
        $('#_list_sj').modal({backdrop: 'static', keyboard: false}); 
    }    
}

function getSJ(counter, pel, qty_pick){
    var tbl_list='';
    $.ajax({
        type: "GET",
        url: base_url + '/keuangan/list_sj/'+pel,
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(res) {

        },
    }).done(function(res){
        $('#loading').hide();            
        var data = $.parseJSON(res);   
        console.log(data)                     
        $.each(data, function(k,v){
            if(qty_pick==1){
                pilihqty = v.n_tot_asal_sj;
            } else if(qty_pick==2) {
                pilihqty = v.n_tot_asal_kp;
            } else {
                pilihqty = v.n_tot_jadi_kp;
            }
            tbl_list+= '<tr><td><a href="#" onclick="pilih2(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+v.d_sj+'\',\''+pilihqty+'\',\''+counter+'\')">'+v.i_no_sj+'</a></td>';
            tbl_list+= '<td><a href="#" onclick="pilih2(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+v.d_sj+'\',\''+pilihqty+'\',\''+counter+'\')">'+v.d_sj+'</a></td>';
            tbl_list+= '<td><a href="#" onclick="pilih2(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+v.d_sj+'\',\''+pilihqty+'\',\''+counter+'\')">'+v.e_nama_pel+'</a></td>';
            tbl_list+= '<td><a href="#" onclick="pilih2(\''+v.i_id+'\',\''+v.i_no_sj+'\',\''+v.d_sj+'\',\''+pilihqty+'\',\''+counter+'\')">'+addCommas(pilihqty)+'</a></td>';
            tbl_list+= '</tr>';            
        });         
        $('#list2').html(tbl_list);
    }).fail(function(xhr,textStatus,errorThrown){
        $('#loading').hide();
        swal(textStatus,errorThrown,'error');
    });
    $('#_list_sj_new').modal({backdrop: 'static', keyboard: false}); 
}

function pilih(id, nosj, qty){
    $('#nosj').val(nosj);
    $('#idsj').val(id);
    $('#_list_sj').modal('hide');
}

function pilih_all(){
	var x = $('#iter').val();	
    
	if(document.getElementById('ckall').checked==true){        
		for (i = 1; i <= parseInt(x); i++){
			document.getElementById('f_ck_tblItem'+i).checked = true;
		}
	}else{
		for (i = 1; i <= parseInt(x); i++){
			document.getElementById('f_ck_tblItem'+i).checked = false;
		}
	}
}

function ambil(){
    var x = $('#iter').val();    
	var iter;
	var iter2;	

	var iter_external ;
	var iter_external_trim;
	var i_sj_external;
	var i_sj_trim;

	$('#nosj').val('');
	$('#idsj').val('');

	for(var i = 1; i <= parseInt(x); i++) {
		
		if(document.getElementById('f_ck_tblItem'+i).checked==true) {
			
			iter_external	= $('#nosj').val();
			iter_external_trim 	= $.trim(iter_external);
			i_sj_external	= $('#idsj').val();
			i_sj_trim 		= $.trim(i_sj_external);			

            iter = document.getElementById('f_ck_tblItem'+i).value;            
            iter2 = document.getElementById('id_sj'+i).value;		            	
            
			if(iter_external_trim=='') {
				$('#nosj').val($.trim(iter));
				$('#idsj').val($.trim(iter2));				
			}else{
				$('#nosj').val(iter_external_trim+'#'+$.trim(iter));
				$('#idsj').val(i_sj_trim+'#'+$.trim(iter2));				
			}

		}
	}
	console.log(i)
	if((i-1)==parseInt(x))  
		$('#_list_sj').modal('hide');
}

function hitungUlang(nilai, baris){
    var total = formatulang($('#total').val());
    var ppnPercent = $('#ppn_percent').val();
    var diskonPersen = $('#diskon_percent').val();
    var totalQty = $('#totqty').val();
    var totalQtyNew = 0;
    var totalNew = 0;
    var ppn = 0; 
    var totalNota = 0; 
    var total_nota = 0;    
    var diskonNilai = 0;
    var grandTot = 0;
    if($('#pilih_hapus'+baris).is(':checked')==true){
        totalNew = parseFloat(total) - parseFloat(nilai);
        $("#total").val(formatuang(Math.round(totalNew)));
        var total = formatulang($('#total').val());
        if(parseInt(ppnPercent)>0){
            ppn = Math.round(parseFloat(totalNew)*(ppnPercent/100));
            $('#ppn').val(formatuang(ppn));
        } else {
            $('#ppn').val(0);
        }
        totalNota = parseFloat(totalNew) + parseFloat(ppn);
        $('#total_nota').val(formatuang(Math.round(totalNota)));
        if(parseFloat(diskonPersen)==0 || diskonPersen==''){
            $('#diskon_nilai').val(0);
            $('#grand_tot').val(formatuang(Math.round(totalNew)));    
        } else {
            var totalNota = $('#total_nota').val();
            total_nota = formatulang(totalNota);
            diskonPersen = parseFloat(diskonPersen)/100;
            diskonNilai = Math.round(parseFloat(total_nota)*diskonPersen);
            $('#diskon_nilai').val(formatuang(diskonNilai));
            sisa = parseFloat(total_nota) - parseFloat(diskonNilai);
            $('#grand_tot').val(formatuang(Math.round(sisa)));
        }
        totalQtyNew = parseFloat(totalQty) - parseFloat($('#qty_'+baris).val());
        $('#totqty').val(totalQtyNew.toFixed(2));
    } else {     
        totalNew = parseFloat(total) + parseFloat(nilai);
        $("#total").val(formatuang(Math.round(totalNew)));
        var total = formatulang($('#total').val());
        if(parseInt(ppnPercent)>0){
            ppn = Math.round(parseFloat(totalNew)*(ppnPercent/100));
            $('#ppn').val(formatuang(ppn));
        } else {
            $('#ppn').val(0);
        }
        totalNota = parseFloat(totalNew) + parseFloat(ppn);
        $('#total_nota').val(formatuang(Math.round(totalNota)));
        if(parseFloat(diskonPersen)==0 || diskonPersen==''){
            $('#diskon_nilai').val(0);
            $('#grand_tot').val(formatuang(Math.round(totalNew)));    
        } else {
            var totalNota = $('#total_nota').val();
            total_nota = formatulang(totalNota);
            diskonPersen = parseFloat(diskonPersen)/100;
            diskonNilai = Math.round(parseFloat(total_nota)*diskonPersen);
            $('#diskon_nilai').val(formatuang(diskonNilai));
            sisa = parseFloat(total_nota) - parseFloat(diskonNilai);
            $('#grand_tot').val(formatuang(Math.round(sisa)));
        }         
        totalQtyNew = parseFloat(totalQty) + parseFloat($('#qty_'+baris).val());
        $('#totqty').val(totalQtyNew.toFixed(2));
    }
}

function munculTombol(){
    const form = document.querySelector('#form_edit_nota');
    const checkboxes = form.querySelectorAll('input[type=checkbox]');
    const checkboxLength = checkboxes.length;    
    for (let i = 0; i < checkboxLength; i++) {
        if (checkboxes[i].checked) {
            $('#addsj').removeClass('d-none');
            $('#removesj').removeClass('d-none');     
            break;
        } else {
            $('#addsj').addClass('d-none');
            $('#removesj').addClass('d-none');     
        }
    }
}

function pilih2(id, nosj, d_sj, qty, counter){
    var qty_bef = $('#totqty').val();
    var qty_new = 0;
    $('#idsj'+counter).val(id);
    $('#no_sj'+counter).val(nosj);
    $('#d_sj'+counter).val(d_sj);
    $('#qty_'+counter).val(qty);  
    $('#hrg_sat'+counter).removeAttr('readonly');    
    qty_new = parseFloat(qty_bef)+parseFloat(qty);
    $('#totqty').val(qty_new.toFixed(2));
    $('#_list_sj_new').modal('hide'); 
}

function addSJ(){
    var counter = parseInt($('#totrow').val())+1;
    var cols = "";
    cols += '<tr id="tr'+counter+'">';
    cols += '<td align="center">'+counter+'</td>';
    cols += '<td align="center"><input type="hidden" class="form-control form-control-sm border-dark" name="idsj'+counter+'" id="idsj'+counter+'" value=""/><input type="text" class="form-control form-control-sm border-dark" name="no_sj'+counter+'" id="no_sj'+counter+'" onclick="getSJ('+counter+','+$('#pel').val()+','+$('#qty_pick').val()+')" readonly/></td>';
    cols += '<td align="center"><input type="text" class="form-control form-control-sm border-dark" name="d_sj'+counter+'" id="d_sj'+counter+'" readonly/></td>';
    cols += '<td><input type="text" class="form-control form-control-sm border-dark" name="qty_'+counter+'" id="qty_'+counter+'" style="text-align: right;" onkeyup="formatRp(document.getElementById(\'qty_'+counter+'\'));" value="" readonly/></td>';
    cols += '<td><input type="text" class="form-control form-control-sm border-dark text-right" name="hrg_sat'+counter+'" id="hrg_sat'+counter+'" value="" onkeyup="formatRp(this);hitung('+counter+');" readonly></td>';
    cols += '<td><input type="text" class="form-control form-control-sm border-dark text-right" name="jml_hrg'+counter+'" id="jml_hrg'+counter+'" value="" readonly><input type="checkbox" name="pilih_hapus'+counter+'" id="pilih_hapus'+counter+'" class="d-none"/></td>';
    $("#detailbody").append(cols);
    $('#totrow').val(counter);
}

function deleteSJ()
{
    var nilai = $('#hitung_by').val();
    var counter = parseInt($('#totrow').val());    
    var batas = '{{count($param['notaitem'])}}';        
    if(counter>batas)
    {
        $('#tr'+counter).remove();       
        counter -= 1;
        $('#totrow').val(counter);	
        // hitung_lagi_by(nilai)   
    }
}

function hitungDownPayment()
{
    var hrgPek = $('#total_nota').val();
        hrgPek = formatulang(hrgPek);
    var downPayPersen = $('#diskon_percent').val()==""?$('#diskon_percent').val(0):downPayPersen;
        downPayPersen = $('#diskon_percent').val() || 0;
    downPayPersen = parseFloat(downPayPersen)/100;
    downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
    $('#diskon_nilai').val(formatuang(downPayNilai));
    sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);    
    $('#grand_tot').val(formatuang(Math.round(sisa)));
}

function hitungPPN()
{
    var tothrg = $('#total').val();
        tothrg = formatulang(tothrg);
    var ppn = $('#ppn_percent').val();
    ppn = parseFloat(ppn)/100;
    ppnNilai = Math.round(parseFloat(tothrg)*ppn);
    $('#ppn').val(formatuang(ppnNilai));
    hrgPek = parseFloat(tothrg) + parseFloat(ppnNilai);    
    $('#total_nota').val(formatuang(Math.round(hrgPek)));
    hitungDownPayment();    
}

function hitung(key){
    var totrow = $('#totrow').val();
    var qty = $('#qty_'+key).val();        
        qty = formatulang(qty);        
    var hrg = $('#hrg_sat'+key).val();
        hrg = formatulang(hrg);    
    var ppnPercent = $('#ppn_percent').val();        
    var downPayPersen = $('#diskon_percent').val();        
    var totAll = 0; var ppn = 0; var hrgPek = 0; var downPayNilai = 0; var sisa = 0;
    if(hrg==""){
        $('#jml_hrg'+key).val(0);
        for(var i=1; i<=totrow; i++){
            totAll += Math.round(parseFloat(formatulang($('#jml_hrg'+i).val()))); 
        }
        if(parseInt(ppnPercent)>0){
            ppn = Math.round(parseFloat(totAll)*(ppnPercent/100));
            $('#ppn').val(formatuang(ppn));
        } else {
            $('#ppn').val(0);
        }
        hrgPek = parseFloat(totAll) + parseFloat(ppn);
        $('#total_nota').val(formatuang(Math.round(hrgPek)));
        if(parseFloat(downPayPersen)==0 || downPayPersen==''){
            $('#diskon_nilai').val(0);
            $('#grand_tot').val(formatuang(Math.round(hrgPek)));    
        } else {
            var hrgPekerjaan = $('#total_nota').val();
            hrgPek = formatulang(hrgPekerjaan);
            downPayPersen = parseFloat(downPayPersen)/100;
            downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
            $('#diskon_nilai').val(formatuang(downPayNilai));
            sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);
            $('#grand_tot').val(formatuang(Math.round(sisa)));
        }
        $('#total').val(formatuang(Math.round(totAll)));
    } else {                
        var jmlhrg = parseFloat(qty) * parseFloat(hrg);
        $('#jml_hrg' + key).val(formatuang(jmlhrg));
        for(var i=1; i<=totrow; i++){                        
            totAll += Math.round(parseFloat(formatulang($('#jml_hrg'+i).val()))); 
        }
        if(parseInt(ppnPercent)>0){
            ppn = Math.round(parseFloat(totAll)*(ppnPercent/100));
            $('#ppn').val(formatuang(ppn));
        } else {
            $('#ppn').val(ppn);
        }        
        hrgPek = parseFloat(totAll) + parseFloat(ppn);
        $('#total_nota').val(formatuang(Math.round(hrgPek)));
        if(parseFloat(downPayPersen)==0 || downPayPersen==''){
            $('#diskon_nilai').val(0);
            $('#grand_tot').val(formatuang(Math.round(hrgPek)));    
        } else {
            var hrgPekerjaan = $('#total_nota').val();
            hrgPek = formatulang(hrgPekerjaan);
            downPayPersen = parseFloat(downPayPersen)/100;
            downPayNilai = Math.round(parseFloat(hrgPek)*downPayPersen);
            $('#diskon_nilai').val(formatuang(downPayNilai));
            sisa = parseFloat(hrgPek) - parseFloat(downPayNilai);
            $('#grand_tot').val(formatuang(Math.round(sisa)));
        }
        $('#total').val(formatuang(Math.round(totAll)));
    }
}

function lihatSJ(id, idkartu, idrfp){
    loadNewPage(base_url + '/keuangan/lihat_do?id='+id+'&idkartu='+idkartu+'&idrfp='+idrfp);
}

function lihatNota(id){
    loadNewPage(base_url + '/keuangan/lihat_nota?id='+id);
}

function printNota(id){
    loadNewPage(base_url + '/keuangan/print_nota?id='+id);
}

function edit(id){
    loadNewPage(base_url + '/keuangan/edit_nota?id='+id);
}

function validasi(){
    $('#form_nota').validate({
        rules: {            
            pel: {
                required: !0
            },
            nosj: {                
                required: !0
            },
            qty_pick: {                
                required: !0                
            },            
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');            
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_nota = $('#form_nota');
            var _form_data = new FormData(form_nota[0]);
            $.ajax({
                type: "POST",
                url: base_url + '/keuangan/input_nota',
                data: _form_data,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();
                countPosisi();
                $('#pageLoad').html(res).fadeIn('slow');                
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });     
        }
    });
}

function prosesSimpan(){    
    $('#form_input_nota').validate({
        rules: {            
            d_nota: {
                required: !0
            },
            d_jth_tempo: {
                required: !0
            }            
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');            
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_nota = $('#form_input_nota');
            var _form_data = new FormData(form_nota[0]);
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/keuangan/nota/simpan_nota',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('keuangan.nota') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data Nota Tidak Jadi Disimpan :)", "error");   
                } 
            });
        }
    });
    $('input[id^="hrg_sat"]').each(function (k,v) {                       
        var nilai = formatulang(v.value)        
        $(this).rules('add', {
            required: !0,     
            greaterThanZero: !0      
        });
    });
}

function prosesUpdate(){    
    $('#form_edit_nota').validate({
        rules: {            
            d_nota: {
                required: !0
            },
            d_jth_tempo: {
                required: !0
            }            
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');            
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_nota = $('#form_edit_nota');
            var _form_data = new FormData(form_nota[0]);
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/keuangan/nota/update_nota',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('keuangan.nota') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal", "Data Nota Tidak Jadi Disimpan :)", "error");   
                } 
            });
        }
    });
    $('input[id^="hrg_sat"]').each(function (k,v) {                       
        var nilai = formatulang(v.value)        
        $(this).rules('add', {
            required: !0,     
            greaterThanZero: !0      
        });
    });
}

function approved(id){
    swal({   
        title: "Anda Yakin?",   
        text: "Yakin Akan Approved Request Print SJ ini?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",                     
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) {     
            $.ajax({
                type: "GET",
                url: base_url + '/keuangan/approved_sj/'+ id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                $('#loading').hide();            
                var data = $.parseJSON(res);
                swal({   
                    title: "Sukses",   
                    text: data.msg,   
                    type: "success",                       
                    showCancelButton: false,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    closeOnConfirm: true,                                       
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if(isConfirm){
                        loadNewPage('{{ route('list_approved_print_sj') }}');
                    }
                });
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
        } else {
            swal('Batal','Data Request Print SJ tidak jadi diapproved','info');
        }
    });
}

function reject(id){
    swal({
        title: "Reject SJ",
        text: "Tulis Alasan Reject",
        type: "input",
        inputPlaceholder: "Alasan",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Lanjut",                     
        cancelButtonText: "Tidak", 
        closeOnConfirm: false,                      
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue.length>200){
            alert('Tidak boleh lebih dari 200 kata');
            return false;
        }
        if (inputValue === "") {
            alert('Alasan harus diisi!');
            return false;
        } else {
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Akan Reject Data SJ ini?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "GET",
                        url: base_url + '/keuangan/reject_sj/'+ id,
                        data: {input_val: inputValue},
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();            
                        var data = $.parseJSON(res);
                        swal({   
                            title: "Sukses",   
                            text: data.msg,   
                            type: "success",                       
                            showCancelButton: false,   
                            confirmButtonColor: "#e6b034",   
                            confirmButtonText: "Ya",                     
                            closeOnConfirm: true,                                       
                            showLoaderOnConfirm: true 
                        }, function(isConfirm){   
                            if(isConfirm){
                                loadNewPage('{{ route('list_approved_print_sj') }}');
                            }
                        });
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });
                } else {
                    swal('Batal','Data Request Print SJ tidak jadi direject','info');
                }
            });
        }
    });
}
</script>