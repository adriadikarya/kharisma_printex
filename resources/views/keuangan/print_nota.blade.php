@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['notahead'])
@foreach($param['notahead'] as $head)
<style type="text/css" media="print">    
    .noDisplay {
        display: none;
    }

    .kuning th { 
        background-color: #fcd53b !important;
        -webkit-print-color-adjust: exact; 
    }        
    
    @page{
        /* size: 21cm 29.7cm;
        margin: 0.05in 0.79in 0.10in 0.60in; */
        margin: 0cm;        
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card px-2">
            <div id="printAble">
                <div class="card-body">
                    {{--<div class="container-fluid">
                        <h3 class="text-right my-5">Invoice&nbsp;&nbsp;#INV-17</h3>
                        <hr>
                    </div>--}}
                    <div class="container-fluid d-flex justify-content-between">
                    <div class="col-lg-3 pl-0">
                        <p class="mt-5 mb-2"><b>PT KHARISMA PRINTEX</b></p>
                        <p>Jl. Holis No. 461 Bandung</p>
                    </div>
                    <div class="col-lg-3 pr-0">
                        <p class="mt-5 mb-2 text-right"><b>Kepada</b></p>
                        <p class="text-right">{{$head->e_nama_pel}},<br> {{$head->e_alamat_pel}}</p>
                    </div>
                    </div>
                    <div class="container-fluid d-flex justify-content-between">
                    <div class="col-lg-3 pl-0">
                        <p class="mb-0 mt-5"> Nomor Nota : {{$head->i_nota_code}}</p>
                        <p class="mb-0">Tanggal Nota : {{date('d M Y',strtotime($head->d_nota))}}</p>
                        <p>Jatuh Tempo : {{date('d M Y', strtotime($head->d_due_date))}}</p>
                    </div>
                    </div>
                    <div class="container-fluid mt-5 d-flex justify-content-center w-100">
                    <div class="table-responsive w-100">
                        <table class="table">
                            <thead>
                            <tr class="bg-warning kuning">
                                <th class="text-dark">#</th>
                                <th class="text-dark">Nomor SJ</th>
                                <th class="text-dark">Tanggal SJ</th>
                                <th class="text-right text-dark">Quantity</th>
                                <th class="text-right text-dark">Harga Satuan</th>
                                <th class="text-right text-dark">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($param['notaitem'])  
                                @foreach($param['notaitem'] as $key => $notaitem)                                            
                                    @php $jml_hrg = $notaitem->n_qty * $notaitem->v_hrg_satuan; @endphp
                                    <tr class="text-right">
                                        <td class="text-left">{{$key+1}}</td>
                                        <td class="text-left">{{$notaitem->i_no_sj}}</td>
                                        <td class="text-left">{{$notaitem->d_sj}}</td>
                                        <td>{{$notaitem->n_qty}}</td>
                                        <td>{{number_format($notaitem->v_hrg_satuan)}}</td>
                                        <td>{{number_format($jml_hrg)}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" align="center">Maaf Data SJ Tidak Ada!</td>
                                </tr>
                            @endif                         
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2" style="border:0;"></td>
                                    <td align="right"><b>Total Qty</b></td>
                                    <td align="right">{{$head->n_tot_qty}}</td>
                                    <td style="text-align:right;"><b>Total</b></td>
                                    <td align="right">{{number_format($head->v_total_nota)}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4" rowspan="4" style="border:0;"><p><u>Cara Bayar</u><br>Bank : BCA<br>No Rekening : 123456789<br>Kirim via Bank BCA</p></td>
                                    <td align="right" style="text-align:right;"><b>PPN {{$head->n_ppn}}%</b></td></td>
                                    <td align="right">{{number_format($head->v_ppn)}}</td>
                                </tr>
                                <tr>
                                    @php $total_nota = $head->v_total_nota + $head->v_ppn; @endphp                                
                                    <td style="text-align:right;"><b>Total Nota</b></td>
                                    <td align="right">{{number_format($total_nota)}}</td>
                                </tr>
                                <tr>                                
                                    <td style="text-align:right;"><b>Diskon {{$head->n_diskon}}%</b></td>
                                    <td align="right">{{number_format($head->v_diskon)}}</td>
                                </tr>
                                <tr>                                
                                    <td style="text-align:right;"><b>Grand Total</b></td>
                                    <td align="right">{{number_format($head->v_total_nppn_sisa)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
                    {{--<div class="container-fluid mt-5 w-100">
                    <p class="text-right mb-2">Total amount: $12,348</p>
                    <p class="text-right">vat (10%) : $138</p>
                    <h4 class="text-right mb-5">Total : $13,986</h4>
                    <hr>
                    </div>--}}
                    {{--<div class="container-fluid w-100 noDisplay">
                        <a onclick="printDiv('printAble')" class="btn btn-primary float-right mt-4 ml-2"><i class="mdi mdi-printer mr-1"></i>Print</a>
                        <a href="#" class="btn btn-success float-right mt-4"><i class="mdi mdi-telegram mr-1"></i>Send Invoice</a>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card noDisplay">
    <div class="card-body">                
        <button type="button" onclick="loadNewPage('{{ route('keuangan.nota') }}')" class="btn btn-warning btn-icon-text">
            <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
            Kembali
        </button>
        <button type="button" onclick="printDiv('printAble')" class="btn btn-info btn-icon-text">
            <i class="mdi mdi-printer btn-icon-prepend"></i>
            Print
        </button>
    </div>
</div>
@endforeach
@endif
<script type="text/javascript">
function printDiv(divName){    
    // var printContents = document.getElementById(divName).innerHTML;
    // var strHtml = "<html>\n<head>\n <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" type=\"text/css\"><body>\n"
	// 	+ printContents + "\n</body>\n</html>";
    // var originalContents = document.body.innerHTML;

    // document.body.innerHTML = printContents;
    // document.body.innerHTML = strHtml;

    // window.print();

    // document.body.innerHTML = originalContents;

    var contents = document.getElementById(divName).innerHTML;
    var frame1 = $('<iframe />');
    frame1[0].name = "frame1";
    frame1.css({ "position": "absolute", "top": "-1000000px" });
    $("body").append(frame1);
    var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
    frameDoc.document.open();
    //Create a new HTML document.
    frameDoc.document.write('<html><head><title>DIV Contents</title>');
    frameDoc.document.write('</head><body>');
    //Append the external CSS file.    
    frameDoc.document.write('<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />');
    frameDoc.document.write('<style type="text/css" media="print"> .noDisplay {display: none;} .kuning th { background-color: #fcd53b !important; -webkit-print-color-adjust: exact; } @page{ /* size: 21cm 29.7cm; margin: 0.05in 0.79in 0.10in 0.60in; */ margin: 0cm;} </style>');
    //Append the DIV contents.
    frameDoc.document.write(contents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        frame1.remove();
    }, 500);
}
</script>
@endsection