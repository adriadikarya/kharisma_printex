@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['notahead'])
@foreach($param['notahead'] as $notahead)
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Edit A Nota</h4>
            <form class="form" id="form_edit_nota">
            @csrf
                <button type="submit" onclick="prosesUpdate()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Update
                </button>
                <button type="button" onclick="loadNewPage('{{ route('keuangan.nota') }}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>
                <input type="hidden" name="id_nota" id="id_nota" value="{{$notahead->i_nota}}"/>
                <input type="hidden" name="pel" id="pel" value="{{$notahead->i_pel}}"/>
                <input type="hidden" name="qty_pick" id="qty_pick" value="{{$notahead->i_qty_from}}"/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">No. Nota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_nota" id="no_nota" value="{{$notahead->i_nota_code}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Tgl Nota</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_nota" id="d_nota" value="{{date('d-m-Y', strtotime($notahead->d_nota))}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Pelanggan</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="nama_pel" id="nama_pel" value="{{$notahead->e_nama_pel}}" readonly />
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Tgl Jth Tempo</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_jth_tempo" id="d_jth_tempo" value="{{date('d-m-Y', strtotime($notahead->d_due_date))}}" readonly />
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-12">                        
                        <button type="button" onclick="addSJ()" class="btn btn-info btn-icon-text" id="addsj">
                            <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                            Tambah SJ
                        </button>
                        <button type="button" onclick="deleteSJ()" class="btn btn-danger btn-icon-text" id="removesj">
                            <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                            Kurang SJ
                        </button>
                        @if($notahead->i_qty_from==1)
                            @php $name_qty = "asal sj"; @endphp
                        @elseif($notahead->i_qty_from==2)
                            @php $name_qty = "asal kp"; @endphp
                        @else
                            @php $name_qty = "jadi kp"; @endphp
                        @endif
                        <input type="hidden" name="totrow" id="totrow" value="{{count($param['notaitem'])}}"/>
                        <div class="table-responsive pt-1">
                            <table class="table table-bordered" id="table-harga">
                                <thead style="background-color: lightcyan;">
                                    <tr>
                                        <th colspan="7" align="text-dark" style="text-align: center;vertical-align: middle;">DETAIL NOTA</th>
                                    </tr>
                                    <tr>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">NO.</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">NO SJ</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">TGL SJ</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">QUANTITY {{strtoupper($name_qty)}}</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">HARGA SATUAN (Rp.)</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">JUMLAH HARGA</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">PILIH</th>
                                    </tr>                                    
                                </thead>
                                <tbody id="detailbody">
                                    @php $totqty = 0; @endphp 
                                    @if($param['notaitem'])  
                                        @foreach($param['notaitem'] as $key => $arr_sj)
                                            @php 
                                                $totqty+= $arr_sj->n_qty; 
                                                $jmlhrg = $arr_sj->n_qty*$arr_sj->v_hrg_satuan;
                                            @endphp
                                            <tr>
                                                <td align="center">{{$key+1}}</td>
                                                <td align="center"><input type="hidden" name="idsj{{$key+1}}" id="idsj{{$key+1}}" value="{{$arr_sj->i_sj}}"/>{{$arr_sj->i_no_sj}}</td>
                                                <td align="center">{{$arr_sj->d_sj}}</td>
                                                <td><input type="text" class="form-control form-control-sm border-dark" name="qty_{{$key+1}}" id="qty_{{$key+1}}" style="text-align: right;" onkeyup="formatRp(document.getElementById('qty_{{$key+1}}'));" value="{{$arr_sj->n_qty}}" readonly/></td>
                                                <td><input type="text" class="form-control form-control-sm border-dark text-right" name="hrg_sat{{$key+1}}" id="hrg_sat{{$key+1}}" value="{{number_format($arr_sj->v_hrg_satuan,2,'.',',')}}" onkeyup="formatRp(this);hitung({{$key+1}});"></td>
                                                <td><input type="text" class="form-control form-control-sm border-dark text-right" name="jml_hrg{{$key+1}}" id="jml_hrg{{$key+1}}" value="{{number_format($jmlhrg)}}" readonly></td>
                                                <td align="center"><input type="checkbox" value="y" name="pilih_hapus{{$key+1}}" id="pilih_hapus{{$key+1}}" onclick="hitungUlang({{$jmlhrg}},{{$key+1}});"/></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" align="center">Maaf Data SJ Tidak Ada!</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="border:0;"></td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="totqty" id="totqty" value="{{$totqty}}" readonly/></td>
                                        <td style="text-align:right;">Total</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="total" id="total" value="{{number_format($notahead->v_total_nota)}}" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">PPN = <input type="text" style="text-align:right;width:20%;" class="" name="ppn_percent" id="ppn_percent" onkeyup="hitungPPN();" onkeydown="hanyaangka();" value="{{$notahead->n_ppn}}"/>%</td></td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="ppn" id="ppn" value="{{number_format($notahead->v_ppn)}}" readonly/></td>
                                    </tr>
                                    <tr>
                                        @php $total_nota = $notahead->v_total_nota + $notahead->v_ppn; @endphp
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Total Nota =</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="total_nota" id="total_nota" value="{{number_format($total_nota)}}" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Diskon = <input type="text" style="text-align:right;width:20%;" class="" name="diskon_percent" id="diskon_percent" onkeyup="hitungDownPayment();" onkeydown="hanyaangka();" value="{{$notahead->n_diskon}}"/>%</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="diskon_nilai" id="diskon_nilai" value="{{number_format($notahead->v_diskon)}}" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border:0;"></td>
                                        <td style="text-align:right;">Grand Total =</td>
                                        <td><input type="text" style="text-align:right;" class="form-control form-control-sm border-dark" name="grand_tot" id="grand_tot" value="{{number_format($notahead->v_total_nppn_sisa)}}" readonly/></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>  
</div>
@endforeach
@endif
@include('keuangan.modal2')
@include('keuangan.action')
@endsection