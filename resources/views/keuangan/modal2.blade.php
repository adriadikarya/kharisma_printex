<div class="modal fade" id="_list_sj_new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Delivery Orders</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>
            <form id="form_sj">
            <div class="modal-body">                
                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-striped" id="datatable2">        
                        <thead>
                            <th>DO Number</th>
                            <th>Send Date</th>
                            <th>Customer</th>
                            <th>Qty</th>                            
                        </thead>
                        <tbody id="list2">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>