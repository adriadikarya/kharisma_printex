<div class="modal fade" id="_change_pass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>                
            </div>
            <form id="form_change_pass">
            @csrf
            <div class="modal-body">                
                    <div class="form-group">
                        <label class="col-sm-4 col-form-label mb-0">* Old Password</label>
                        <div class="col-sm-12">   
                            <input type="password" class="form-control form-control-sm border-dark" maxlength="50" name="old_pass" id="old_pass"/>                           
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 col-form-label mb-0">* New Password</label>
                        <div class="col-sm-12">
                            <input type="password" class="form-control form-control-sm border-dark" maxlength="50" name="new_pass" id="new_pass"/>                           
                        </div>                        
                    </div>  
                    <div class="form-group">
                        <label class="col-sm-4 col-form-label mb-0">* Repeat Password</label>
                        <div class="col-sm-12">
                            <input type="password" class="form-control form-control-sm border-dark" maxlength="50" name="rep_pass" id="rep_pass"/>                           
                        </div>                        
                    </div>             
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success" onclick="save_change_pass()">Save</button>         
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
function save_change_pass(){
    $('#form_change_pass').validate({
        rules: {
            old_pass: {
                required: !0,
                minlength: 3,
                maxlength: 50 
            },
            new_pass: {
                required: !0,
                minlength: 3,
                maxlength: 50,
                regex: "^[a-zA-Z][a-zA-Z0-9]"
            },
            rep_pass : {
                required: !0,
                minlength: 3,
                maxlength: 50,
                equalTo: "#new_pass",
                regex: "^[a-zA-Z][a-zA-Z0-9]"
            },
        },
        messages: {
            rep_pass: {
                equalTo: "password doesn't match"
            }
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
        	    label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){
            var form_rfp = $('#form_change_pass');
            var _form_data = new FormData(form_rfp[0]);                        

            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Akan Mengganti Password?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/home/change_pass',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        $('#loading').hide();
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal('Berhasil',data.msg,'success');
                            form_rfp[0].reset();
                            $('#_change_pass').modal('hide');
                            $.ajax({
                                type: 'POST',
                                url: base_url + '/logout2',
                                success: function(res) {
                                    window.location.href = base_url + '/login';
                                }
                            }).done(function(res) {}).fail(function(res) {});
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Batal","", "error");   
                } 
            });
        }
    });
}
</script>