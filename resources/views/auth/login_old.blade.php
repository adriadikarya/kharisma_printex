<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PT KHARISMA PRINTEX</title>
    <!-- base:css -->
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/base/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/sweetalert/sweetalert.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/loader27.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
</head>

<body>
    <div class="container-scroller">
    <div id="loading" style="display:none;">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_four"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_one"></div>
            </div>
        </div>
    </div>
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                <div class="row flex-grow">
                    <div class="col-lg-6 d-flex align-items-center justify-content-center">
                        <div class="auth-form-transparent text-left p-3">
                            <div class="brand-logo">
                                <img src="{{ asset('images/logo-depan.jpg') }}" alt="logo">
                                <!-- <p alt="logo"><b>PT KHARISMA PRINTEX</b></p> -->
                            </div>
                            <!-- <h4>Welcome back!</h4> -->
                            <!-- <h6 class="font-weight-light">Happy to see you again!</h6> -->
                            <form id="form_login" class="pt-3">
                            @csrf
                            {{--@if(session()->has('login_error'))
                                <div class="alert alert-success">
                                    {{ session()->get('login_error') }}
                                </div>
                            @endif--}}
                                <div class="alert alert-info d-none" id="sukses"></div>
                                <div class="alert alert-danger d-none" id="error"></div>                            
                                <div class="form-group {{ $errors->has('identity') ? ' has-error' : '' }}">
                                    <label for="identity">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                            <span class="input-group-text bg-transparent border-right-0">
                                                <i class="mdi mdi-account-outline text-primary"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-lg border-left-0"
                                            id="identity" name="identity" placeholder="username">
                                    </div>
                                </div>
                                {{--@if ($errors->has('identity'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('identity') }}</strong>
                                </span>
                                @endif--}}
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="exampleInputPassword">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                            <span class="input-group-text bg-transparent border-right-0">
                                                <i class="mdi mdi-lock-outline text-primary"></i>
                                            </span>
                                        </div>
                                        <input type="password" class="form-control form-control-lg border-left-0"
                                            id="exampleInputPassword" name="password" placeholder="Password">
                                    </div>
                                </div>
                                {{--@if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif--}}
                                <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        <label class="form-check-label text-muted">
                                            <input type="checkbox" class="form-check-input">
                                            Keep me signed in
                                        </label>
                                    </div>
                                    <a href="#" class="auth-link text-black">Forgot password?</a>
                                </div> -->
                                <div class="my-3">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" onclick="login()"
                                        >LOGIN</button>
                                </div>
                                <!-- <div class="mb-2 d-flex">
                                    <button type="button" class="btn btn-facebook auth-form-btn flex-grow mr-1">
                                        <i class="mdi mdi-facebook mr-2"></i>Facebook
                                    </button>
                                    <button type="button" class="btn btn-google auth-form-btn flex-grow ml-1">
                                        <i class="mdi mdi-google mr-2"></i>Google
                                    </button>
                                </div> -->
                                <!-- <div class="text-center mt-4 font-weight-light">
                                    Don't have an account? <a href="register-2.html" class="text-primary">Create</a>
                                </div> -->
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 login-half-bg d-flex flex-row">
                        <!-- <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy;
                            2018 All rights reserved.</p> -->
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <div class="modal fade in" id="modal_change" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title_modal"> Change Password </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                </div>
                <form id="form-data" method="POST">
                @csrf
                    <div class="modal-body">                    
                        <input type="hidden" name="id" value="" id="id">
                        <div class="form-group">
                            <label for="branch_name">New Password</label>
                            <input type="password" class="form-control" id="new_pass" name="new_pass" placeholder="Insert New Password">
                        </div>
                        
                    </div>
                    <div class="modal-footer">      
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" onclick="change_pass();">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script src="{{ asset('vendors/base/vendor.bundle.base.js')}}"></script>
    <script src="{{ asset('js/template.js')}}"></script>
    <script src="{{ asset('vendors/sweetalert/sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.validator.addMethod("regex",function(value, element, regexp) {
		var re = new RegExp(regexp);
		console.log(re.test(value))		
        return this.optional(element) || re.test(value);
    }, $.validator.format( "First character for password must be letter only!" ) );

    function login(){
        $('#form_login').validate({
            rules: {
                identity: {
                    required: !0
                },
                password: {
                    required: !0
                }
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');
                if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                    label.insertAfter(element.next('.select2-container'));
                } else {
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger').removeClass('has-success')
                $(element).addClass('form-control-danger').removeClass('form-control-success')
            },
            unhighlight: function (element, errorClass) {
                $(element).parent().addClass('has-success').removeClass('has-danger')
                $(element).addClass('form-control-success').removeClass('form-control-danger')
            },
            submitHandler: function(e){
                var form_rfp = $('#form_login');
                var _form_data = new FormData(form_rfp[0]);                       
                $.ajax({
                    type: "POST",
                    url: '{{route('login')}}',
                    data: _form_data,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('#sukses').addClass('d-none');
                        $('#error').addClass('d-none');
                        $('#loading').show();
                    },
                    success: function(res) {
                        
                    },
                }).done(function(res){
                    $('#loading').hide(); 
                    console.log(res.rm)                   
                    switch (res.rc) {
                        // password / username invalid
                        case 0:                            
                            $("#exampleInputPassword").val('');
                            $("#error").removeClass('d-none');
                            $("#error").text(res.rm);
                        break;
                        // akun tidak aktif
                        case 1:
                            $("#identity").val('');
                            $("#exampleInputPassword").val('');
                            $("#sukses").removeClass('d-none');
                            $("#sukses").text(res.rm);
                        break;
                        // reset Password
                        case 2:
                            $("#identity").val('');
                            $("#exampleInputPassword").val('');
                            form_rfp[0].reset();                            
                            $('#modal_change').modal({backdrop: 'static', keyboard: false});
                            $("#id").val(res.id_user);
                        break;
                        // login success
                        case 3:
                            window.location.href = '{{ route('home') }}';
                        break;
                        case 4:
                            $("#identity").val('');
                            $("#exampleInputPassword").val('');
                            $("#error").removeClass('d-none');
                            $("#error").text(res.rm);
                        break;
                    }
                }).fail(function(xhr,textStatus,errorThrown){
                    $('#loading').hide();
                    swal(textStatus,errorThrown,'error');
                });                                                             
            }
        });        
    }

    function change_pass(){
        $('#form-data').validate({
            rules: {
                new_pass: {
                    required: !0,
                    minlength: 3,
                    maxlength: 50,
                    regex: "^[a-zA-Z][a-zA-Z0-9]"
                }
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');
                if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                    label.insertAfter(element.next('.select2-container'));
                } else {
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger').removeClass('has-success')
                $(element).addClass('form-control-danger').removeClass('form-control-success')
            },
            unhighlight: function (element, errorClass) {
                $(element).parent().addClass('has-success').removeClass('has-danger')
                $(element).addClass('form-control-success').removeClass('form-control-danger')
            },
            submitHandler: function(e){
                var form_rfp = $('#form-data');
                var _form_data = new FormData(form_rfp[0]);                        
                $.ajax({
                    type: "POST",
                    url: '{{route('change_pass')}}',
                    data: _form_data,                    
                    processData: false,
                    cache: false,
                    contentType: false,
                    beforeSend: function() {                        
                        $("#error").addClass('d-none');
                        $("#sukses").addClass('d-none');
                        $('#loading').show();
                    },
                    success: function(res) {
                        
                    },
                }).done(function(res){
                    $('#loading').hide();
                    $("#modal").modal('hide');
                    swal({   
                        title: "Berhasil",   
                        text: res.rm,   
                        type: "success",                                               
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){  
                        if (isConfirm) {    
                            window.location.href = '{{ route('login') }}';
                        }
                    });
                    
                }).fail(function(xhr,textStatus,errorThrown){
                    $('#loading').hide();
                    swal(textStatus,errorThrown,'error');
                });                        
            }
        });
    }
    </script>
</body>
</html>