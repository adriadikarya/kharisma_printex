@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['pelunasanhead'])
@foreach($param['pelunasanhead'] as $datahead)
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Repayment</h4>
            <form class="form" id="form_repayment">
            @csrf                
                <button type="button" onclick="loadNewPage('{{route('pelunasan')}}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No. Repayment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" class="form-control form-control-sm border-dark" name="no_repayment" id="no_repayment" maxlength="20" value="{{$datahead->i_pelunasan_code}}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Date Repayment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_repayment" id="d_repayment" value="{{$datahead->d_pelunasan}}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Customer</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" class="form-control form-control-sm border-dark tgl_input" name="pel" id="pel" value="{{$datahead->e_nama_pel}}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pick Invoice</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" name="invoice_no" id="invoice_no" class="form-control form-control-sm border-dark" placeholder="Click This Input" value="{{$datahead->i_invoice_code}}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Payment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" name="payment" id="payment" class="form-control form-control-sm border-dark" style="text-align:right" value="{{number_format($datahead->v_pelunasan)}}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Info Payment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" name="keterangan" id="keterangan" class="form-control form-control-sm border-dark" value="{{$datahead->keterangan}}" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body text-dark">
                        <h4 class="card-title">Detail Payment</h4>
                        <p class="card-description">
                            <input type="hidden" id="totrow" name="totrow" value="0">
                            <button type="button" id="addNotaTombol" class="btn btn-info btn-icon-text" disabled>
                                <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                                Add Nota
                            </button>
                            {{-- <button type="button" onclick="deleteNota()" class="btn btn-danger btn-icon-text">
                                <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                                Remove Nota
                            </button> --}}
                        </p>
                        <div class="table-responsive pt-1">
                            <table class="table table-bordered" id="table-harga">
                                <thead style="background-color: lightcyan;">
                                    <tr>
                                        <th>Nota Number</th>
                                        <th>Date Nota</th>
                                        <th>Nota Bill</th>
                                        <th>Nota Paid</th>                                        
                                    </tr>                                    
                                </thead>
                                <tbody id="detailbody"> 
                                @if($param['pelunasanitem'])
                                @foreach($param['pelunasanitem'] as $key => $item)
                                    <tr id="tr{{$key+1}}">
                                        <td align="center"><input type="text" name="i_nota_no{{$key+1}}" id="i_nota_no{{$key+1}}" class="form-control form-control-sm" value="{{$item->i_nota_code}}" readonly/></td>
                                        <td align="center"><input type="text" name="d_nota{{$key+1}}" id="d_nota{{$key+1}}" class="form-control form-control-sm" value="{{$item->d_nota}}" readonly/></td>
                                        <td align="center"><input type="text" name="v_total_nota{{$key+1}}" id="v_total_nota{{$key+1}}" class="form-control form-control-sm" value="{{number_format($item->v_total_nppn)}}" readonly/></td>
                                        <td align="center"><input type="text" name="v_total_nota_paid{{$key+1}}" id="v_total_nota_paid{{$key+1}}" class="form-control form-control-sm" value="{{number_format($item->v_bayar_nota)}}" readonly/></td>
                                    </tr>  
                                @endforeach        
                                @else
                                    <tr><td colspan="4" align="center">Sorry Your Nota Doesn't Exist</td></tr>
                                @endif             
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
@else
<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body text-dark">
            <h4 class="card-title">Repayment</h4>
            <div class="row">
                <div class="col-md-12">
                    <center>Sorry Your Data Doesn't Exist Or Error</center>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <center><button type="button" onclick="loadNewPage('{{ route('pelunasan') }}')" class="btn btn-warning btn-icon-text">
                <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                Kembali
            </button></center>
        </div>
    </div>
</div>
@endif
@endsection