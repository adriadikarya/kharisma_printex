@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Make A Repayment</h4>
            <form class="form" id="form_repayment">
            @csrf
                <button type="submit" onclick="return prosesSimpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>
                <button type="button" onclick="loadNewPage('{{route('pelunasan')}}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">No. Repayment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" class="form-control form-control-sm border-dark" name="no_repayment" id="no_repayment" maxlength="20" value="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Date Repayment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_repayment" id="d_repayment" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Customer</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <select class="form-control form-control-sm" name="pel" id="pel">
                                                @if($param['pel'])
                                                <option value="">Choose Customer!!</option>
                                                @foreach($param['pel'] as $pel)
                                                <option value="{{ $pel->i_pel }}">{{ $pel->e_nama_pel }}</option>
                                                @endforeach
                                                @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Pick Invoice</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" name="invoice_no" id="invoice_no" class="form-control form-control-sm border-dark" placeholder="Click This Input" onclick="pickInvoice()" readonly/>
                                            <input type="hidden" name="invoice_id" id="invoice_id" class="form-control form-control-sm border-dark"/>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Total Payment</label>
                                    <div class="col-sm-4">
                                        <select class="form-control form-control-sm" name="tipe_bayar" id="tipe_bayar" onchange="jumlahBayar(this.value)"> 
                                            <option value="">Pick Type Payment</option>
                                            <option value="1">New</option>
                                            <option value="2">Take From Residual</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group row">
                                            <input type="text" name="payment" id="payment" class="form-control form-control-sm border-dark d-none" onkeyup="formatRp(this);addToPaymentSisa(this.value);"  onkeydown="hanyaangka();" style="text-align:right"/>
                                            <input type="hidden" name="payment_sisa" id="payment_sisa"/>
                                            <input type="hidden" name="id_uang_masuk" id="id_uang_masuk"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Info Payment</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <input type="text" name="keterangan" id="keterangan" class="form-control form-control-sm border-dark"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body text-dark">
                        <h4 class="card-title">Detail Payment</h4>
                        <p class="card-description">
                            <input type="hidden" id="totrow" name="totrow" value="0">
                            <button type="button" onclick="addNota()" id="addNotaTombol" class="btn btn-info btn-icon-text">
                                <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                                Add Nota
                            </button>
                            {{-- <button type="button" onclick="deleteNota()" class="btn btn-danger btn-icon-text">
                                <i class="mdi mdi-minus-box btn-icon-prepend"></i>
                                Remove Nota
                            </button> --}}
                        </p>
                        <div class="table-responsive pt-1">
                            <table class="table table-bordered" id="table-harga">
                                <thead style="background-color: lightcyan;">
                                    <tr>
                                        <th>Nota Number</th>
                                        <th>Date Nota</th>
                                        <th>Nota Bill</th>
                                        <th>Nota Paid</th>
                                        <th>Payment Money Residual</th>
                                    </tr>                                    
                                </thead>
                                <tbody id="detailbody">                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('pelunasan.modal_nota')
@include('pelunasan.modal_invoice')
@include('pelunasan.modal_sisa_uang_masuk')
@include('pelunasan.action')
<script>
    $('#pel').select2();
</script>
@endsection