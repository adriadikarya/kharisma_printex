@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Repayment</h4>
            <p class="card-description">                
                <button type="button" onclick="loadNewPage('{{route('form_pelunasan')}}')" class="btn btn-primary btn-icon-text">
                    <i class="mdi mdi-library-plus btn-icon-prepend"></i>
                    Add Repayment
                </button>                
            </p>
            <div class="table-responsive pt-3">
                <table class="table table-bordered table-striped" id="datatable">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Repayment Number</th>                        
                            <th>Customer</th>                            
                            <th>Date Repayment</th>
                            <th>Total Repayment</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('pelunasan.action')
@endsection
