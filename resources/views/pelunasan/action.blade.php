<script>
var act_url = '{{ route('data_pelunasan') }}';
var table = $('#datatable').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    ajax: {
        "url" : act_url,                                
        "error": function(jqXHR, textStatus, errorThrown)
        {
            // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            alert(errorThrown);
        }
    },        
    columns: [        
        { data: 'action', name: 'action' },
        // { data: 'urutan', name: 'urutan' },
        { data: 'i_pelunasan_code', name: 'i_pelunasan_code' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },        
        { data: 'd_pelunasan', name: 'd_pelunasan' },        
        { data: 'v_pelunasan', name: 'v_pelunasan' },        
    ]            
});

$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});

function jumlahBayar(id){
    if(id==''){
        $('#payment').addClass('d-none');
        $('#payment').removeAttr('readonly');
        $('#payment').attr('onclick','');
        $('#payment').val('');
    } else if(id=='1'){
        $('#payment').removeClass('d-none');
        $('#payment').removeAttr('readonly');
        $('#payment').attr('onclick','');
        $('#payment').val('');
    } else {
        if($('#pel').val()==''){
            $('#payment').val('');
            $('#payment').addClass('d-none');
            $('#payment').removeAttr('readonly');
            $('#payment').attr('onclick','');            
            swal('Information','Choose Customer First!','info');            
        } else {
            $('#payment').removeClass('d-none');
            $('#payment').attr('readonly','readonly');
            $('#payment').attr('onclick','pilihSisaUangMasuk()');
            $('#payment').val('');
        }        
    }
}

function addNota(){
    var invoice_no = $('#invoice_no').val();
    var tipe_bayar = $('#tipe_bayar').val();
    var payment = $('#payment').val();
    var invoice_id = $('#invoice_id').val();    
    if(invoice_no==''){
        swal('Information', 'Please Pick Invoice First!', 'info');
    } else if(tipe_bayar==''){
        swal('Information', 'Please Pick Total Payment!', 'info');
    } else if(tipe_bayar==1){
        if(payment==''){
            swal('Information', 'Please Input Total Payment!', 'info');
        } else {
            var counter = parseInt($('#totrow').val())+1;
            var cols = "";
            cols += '<tr id="tr'+counter+'">';
            cols += '<td align="center"><input type="hidden" name="i_invoice' + counter + '" id="i_invoice' + counter + '" value="'+counter+'"/><input type="hidden" name="i_nota' + counter + '" id="i_nota' + counter + '" value="'+counter+'"/><input type="text" name="i_nota_no' + counter + '" id="i_nota_no' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="d_nota' + counter + '" id="d_nota' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="v_total_nota' + counter + '" id="v_total_nota' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="v_total_nota_paid' + counter + '" id="v_total_nota_paid' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="v_total_nota_sisa' + counter + '" id="v_total_nota_sisa' + counter + '" class="form-control form-control-sm" readonly/></td></tr>';
            $("#detailbody").append(cols);
            $('#totrow').val(counter);
            $.ajax({
                type: "GET",
                url: base_url + '/pelunasan/list_nota/'+invoice_id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {

                },
            }).done(function(res){
                $('#loading').hide();   
                countPosisi();         
                var data = $.parseJSON(res);        
                var tbl_list = '';
                $.each(data, function(k,v){                
                    tbl_list+= '<tr><td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+v.i_nota_code+'</a></td>';
                    tbl_list+= '<td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+v.d_nota+'</a></td>';
                    tbl_list+= '<td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+addCommas(v.v_nilai_nota)+'</a></td>';
                    tbl_list+= '<td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+addCommas(v.v_total_nppn_sisa)+'</a></td></tr>';                
                });            
                $('#list_nota').html(tbl_list);
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
            $('#_list_nota').modal({backdrop: 'static', keyboard: false});
        }
    } else if(tipe_bayar==2) {
        if(payment==''){
            swal('Information', 'Please Input Total Payment!', 'info');
        } else {
            var counter = parseInt($('#totrow').val())+1;
            var cols = "";
            cols += '<tr id="tr'+counter+'">';
            cols += '<td align="center"><input type="hidden" name="i_invoice' + counter + '" id="i_invoice' + counter + '" value="'+counter+'"/><input type="hidden" name="i_nota' + counter + '" id="i_nota' + counter + '" value="'+counter+'"/><input type="text" name="i_nota_no' + counter + '" id="i_nota_no' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="d_nota' + counter + '" id="d_nota' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="v_total_nota' + counter + '" id="v_total_nota' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="v_total_nota_paid' + counter + '" id="v_total_nota_paid' + counter + '" class="form-control form-control-sm" readonly/></td>';
            cols += '<td align="center"><input type="text" name="v_total_nota_sisa' + counter + '" id="v_total_nota_sisa' + counter + '" class="form-control form-control-sm" readonly/></td></tr>';
            $("#detailbody").append(cols);
            $('#totrow').val(counter);
            $.ajax({
                type: "GET",
                url: base_url + '/pelunasan/list_nota/'+invoice_id,
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {

                },
            }).done(function(res){
                $('#loading').hide();   
                countPosisi();         
                var data = $.parseJSON(res);        
                var tbl_list = '';
                $.each(data, function(k,v){                
                    tbl_list+= '<tr><td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+v.i_nota_code+'</a></td>';
                    tbl_list+= '<td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+v.d_nota+'</a></td>';
                    tbl_list+= '<td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+addCommas(v.v_nilai_nota)+'</a></td>';
                    tbl_list+= '<td><a href="#" onclick="pilih_nota(\''+v.i_invoice+'\',\''+v.i_nota+'\',\''+v.d_nota+'\',\''+v.v_nilai_nota+'\',\''+v.v_total_nppn_sisa+'\',\''+counter+'\',\''+v.i_nota_code+'\')">'+addCommas(v.v_total_nppn_sisa)+'</a></td></tr>';                
                });            
                $('#list_nota').html(tbl_list);
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });
            $('#_list_nota').modal({backdrop: 'static', keyboard: false});
        }
    }
}

function pilih_nota(i_invoice, inota, dnota, v_nota, v_nota_sisa, baris, inotacode){
    $('#tipe_bayar').attr('readonly','readonly');
    $('#payment').attr('readonly','readonly');
    var payment_sisa = $('#payment_sisa').val();
    $('#i_inovice' + baris).val(i_invoice);
    $('#i_nota' + baris).val(inota);
    $('#i_nota_no' + baris).val(inotacode);
    $('#d_nota' + baris).val(dnota);
    $('#v_total_nota' + baris).val(addCommas(v_nota_sisa));
    if(parseFloat(payment_sisa)>parseFloat(v_nota_sisa)){
        var sisa = parseFloat(payment_sisa)-parseFloat(v_nota_sisa);
        $('#payment_sisa').val(addCommas(sisa));
        $('#v_total_nota_paid' + baris).val(addCommas(v_nota_sisa));
        $('#v_total_nota_sisa' + baris).val(addCommas(sisa));
        $('#addNotaTombol').removeAttr('readonly');
    } else if(parseFloat(payment_sisa)==parseFloat(v_nota_sisa)){
        var sisa = parseFloat(payment_sisa)-parseFloat(v_nota_sisa);
        $('#payment_sisa').val(addCommas(sisa));
        $('#v_total_nota_paid' + baris).val(addCommas(v_nota_sisa));
        $('#v_total_nota_sisa' + baris).val(0);
        $('#addNotaTombol').attr('readonly','readonly');
    } else {
        var sisa = parseFloat(v_nota_sisa)-parseFloat(payment_sisa);
        $('#payment_sisa').val(0);
        $('#v_total_nota_paid' + baris).val(addCommas(payment_sisa));
        $('#v_total_nota_sisa' + baris).val(0);        
        $('#addNotaTombol').attr('readonly','readonly');
    }
    $('#_list_nota').modal('hide');
}

function deleteNota(){
    var counter = parseInt($('#totrow').val());
    $('#tr'+counter).remove();       
    if(counter>1)
    {
        counter -= 1;
        $('#totrow').val(counter);
        $('#_list_nota').modal('hide');
    } else {
        $('#payment').removeAttr('readonly');
        $('#tipe_bayar').removeAttr('readonly');
        counter -= 1;
        $('#totrow').val(counter);
        $('#_list_nota').modal('hide');
    }
}

function addToPaymentSisa(nilai){
    var payment = formatulang(nilai);
    $('#payment_sisa').val(payment);
}

function pickInvoice(){
    var pel = $('#pel').val();
    if(pel==''){
        swal('Information', 'Please Choose Customer First!', 'info');
    } else {
        $.ajax({
            type: "GET",
            url: base_url + '/pelunasan/list_kontrabon/'+pel,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(res) {

            },
        }).done(function(res){
            $('#loading').hide();   
            countPosisi();         
            var data = $.parseJSON(res);        
            var tbl_list = '';
            $.each(data, function(k,v){                
                tbl_list+= '<tr><td><a href="#" onclick="pilih_kb(\''+v.i_invoice+'\',\''+v.i_invoice_code+'\')">'+v.i_invoice_code+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih_kb(\''+v.i_invoice+'\',\''+v.i_invoice_code+'\')">'+v.d_invoice+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih_kb(\''+v.i_invoice+'\',\''+v.i_invoice_code+'\')">'+addCommas(v.v_total_invoice)+'</a></td>';
                tbl_list+= '<td><a href="#" onclick="pilih_kb(\''+v.i_invoice+'\',\''+v.i_invoice_code+'\')">'+addCommas(v.v_total_invoice_sisa)+'</a></td></tr>';                
            });            
            $('#list_kontra').html(tbl_list);
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
        $('#_list_kontra_bon').modal({backdrop: 'static', keyboard: false});
    }
}

function pilihSisaUangMasuk(){
    var pel = $('#pel').val();
    if(pel==''){
        swal('Information', 'Please Choose Customer First!', 'info');
    } else {
        $.ajax({
            type: "GET",
            url: base_url + '/pelunasan/list_sisa_uang_masuk/'+pel,
            beforeSend: function() {
                $('#loading').show();
            },
            success: function(res) {

            },
        }).done(function(res){
            $('#loading').hide();   
            countPosisi();         
            var data = $.parseJSON(res);        
            var tbl_list = '';
            $.each(data, function(k,v){                
                tbl_list+= '<tr><td><a href="#" onclick="pilih_su(\''+v.i_uang_masuk+'\',\''+v.v_sisa_uang_masuk+'\')">'+v.i_uang_masuk+'</a></td>';                
                tbl_list+= '<td><a href="#" onclick="pilih_su(\''+v.i_uang_masuk+'\',\''+v.v_sisa_uang_masuk+'\')">'+addCommas(v.v_sisa_uang_masuk)+'</a></td></tr>';                
            });            
            $('#list_uang').html(tbl_list);
        }).fail(function(xhr,textStatus,errorThrown){
            $('#loading').hide();
            swal(textStatus,errorThrown,'error');
        });
        $('#_list_uang_masuk').modal({backdrop: 'static', keyboard: false});
    }
}

function pilih_su(id_uang_masuk, v_sisa_uang_masuk){
    $('#payment').val(addCommas(v_sisa_uang_masuk));
    $('#payment_sisa').val(v_sisa_uang_masuk);
    $('#id_uang_masuk').val(id_uang_masuk);
    $('#_list_uang_masuk').modal('hide');
}

function pilih_kb(i_invoice,i_invoice_no){
    $('#invoice_no').val(i_invoice_no);
    $('#invoice_id').val(i_invoice);
    $('#_list_kontra_bon').modal('hide');
}

function prosesSimpan(){
    if($('#totrow').val()==0){
        swal('Information','Doesn`t has Nota for Payment!','info');
        return false;
    } else {
        $('#form_repayment').validate({
            rules: {
                no_repayment: {
                    required: !0,
                    maxlength: 20
                },
                d_repayment: {
                    required: !0
                },
                pel: {
                    required: !0
                },
                invoice_no: {
                    required: !0
                },
                tipe_bayar: {
                    required: !0
                }
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');
                if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                    label.insertAfter(element.next('.select2-container'));
                } else if(element.hasClass('row-error') && element.prev('.row-error').length) {
                    label.insertAfter(element.next('.row-error'));
                } else {
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger').removeClass('has-success')
                $(element).addClass('form-control-danger').removeClass('form-control-success')
            },
            unhighlight: function (element, errorClass) {
                $(element).parent().addClass('has-success').removeClass('has-danger')
                $(element).addClass('form-control-success').removeClass('form-control-danger')
            },
            submitHandler: function(e){            
                var form_so = $('#form_repayment');
                var _form_data = new FormData(form_so[0]);

                swal({   
                    title: "Anda Yakin?",   
                    text: "Yakin Data Sudah Benar Semua?",   
                    type: "info",                       
                    showCancelButton: true,   
                    confirmButtonColor: "#e6b034",   
                    confirmButtonText: "Ya",                     
                    cancelButtonText: "Tidak",   
                    closeOnConfirm: false,   
                    closeOnCancel: false,
                    showLoaderOnConfirm: true 
                }, function(isConfirm){   
                    if (isConfirm) {     
                        $.ajax({
                            type: "POST",
                            url: base_url + '/pelunasan/simpan',
                            data: _form_data,
                            processData: false,
                            contentType: false,
                            beforeSend: function() {
                                $('#loading').show();
                            },
                            success: function(res) {
                                
                            },
                        }).done(function(res){
                            var data = $.parseJSON(res);
                            if(data.code==1){
                                swal({   
                                    title: "Sukses",   
                                    text: data.msg,   
                                    type: "success",                       
                                    showCancelButton: false,   
                                    confirmButtonColor: "#e6b034",   
                                    confirmButtonText: "Ya",                     
                                    closeOnConfirm: true,                                       
                                    showLoaderOnConfirm: true 
                                }, function(isConfirm){   
                                    if(isConfirm){
                                        loadNewPage('{{ route('pelunasan') }}');
                                    }
                                });
                            } else {
                                swal('Gagal',data.msg,'error');
                            }
                        }).fail(function(xhr,textStatus,errorThrown){
                            $('#loading').hide();
                            swal(textStatus,errorThrown,'error');
                        });                        
                    } else {     
                        swal("Batal", "Data RePayment Cancel Save :)", "error");   
                    } 
                });                
            }
        });
    }
    
}

function lihatPelunasan(id){
    loadNewPage(base_url + '/pelunasan/lihat/' +id);
}
</script>