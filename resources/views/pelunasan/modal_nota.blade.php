<div class="modal fade" id="_list_nota" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Nota</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>            
            <div class="modal-body">                
                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-striped">        
                        <thead>
                            <th>Nota Number</th>
                            <th>Date Nota</th>
                            <th>Total Nota</th>
                            <th>Total Nota Residual</th>
                        </thead>
                        <tbody id="list_nota">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="deleteNota()">Batal</button>
            </div>            
        </div>
    </div>
</div>