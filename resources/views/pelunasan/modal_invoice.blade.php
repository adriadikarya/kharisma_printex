<div class="modal fade" id="_list_kontra_bon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">List Kontra Bon</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button> -->
            </div>            
            <div class="modal-body">                
                <div class="table-responsive pt-3">
                    <table class="table table-bordered table-striped">        
                        <thead>
                            <th>KontraBon Number</th>
                            <th>Date KontraBon</th>
                            <th>Total Invoice</th>
                            <th>Total Invoice Residual</th>
                        </thead>
                        <tbody id="list_kontra">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>            
        </div>
    </div>
</div>