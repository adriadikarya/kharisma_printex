<nav class="bottom-navbar noDisplay" style="background: #f3f5f8 !important;">
    <div class="container">
        <ul class="nav page-navigation">
            @if(Auth::user()->role==0 || Auth::user()->role==99 || Auth::user()->role==1 || Auth::user()->role==3)
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="loadNewPage('{{ route('home') }}')" id="{{ route('home') }}">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role==99 || Auth::user()->role==1 || Auth::user()->role==2)
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-briefcase menu-icon"></i>
                    <span class="menu-title">Marketing</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        @if(Auth::user()->role==99 || Auth::user()->role==1)
                        <li class="nav-item">
                            <a href="#" onclick="loadNewPage('{{ route('plan.sales_order') }}')" class="nav-link" id="{{ route('plan.sales_order') }}">
                                <i class="mdi mdi-briefcase menu-icon"></i>
                                <span class="menu-title">Sales Plan</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif                        
                        <li class="nav-item">
                            <a href="#" onclick="loadNewPage('{{ route('sales_order.index') }}')" class="nav-link" id="{{ route('sales_order.index') }}">
                                <i class="mdi mdi-briefcase menu-icon"></i>
                                <span class="menu-title">Sales Order</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @if(Auth::user()->role==99 || Auth::user()->role==1)                        
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('list_approved.sales_order') }}')" id="{{ route('list_approved.sales_order') }}">
                                <i class="mdi mdi-briefcase menu-icon"></i>
                                <span class="menu-title">Approved SO by Manager</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('rfp.index') }}')" id="{{ route('rfp.index') }}">
                                <i class="mdi mdi-briefcase menu-icon"></i>
                                <span class="menu-title">PR (Production Request)</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('so_outsourcing') }}')" id="{{ route('so_outsourcing') }}">
                                <i class="mdi mdi-briefcase menu-icon"></i>
                                <span class="menu-title">SO Outs After Approved</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> -->
                        @if(Auth::user()->role==99 || Auth::user()->role==1)
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('list_approved_mkt.rfp') }}')" id="{{ route('list_approved_mkt.rfp') }}">
                                <i class="mdi mdi-briefcase menu-icon"></i>
                                <span class="menu-title">Approved PR by Manager</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.pelanggan') }}')" id="{{ route('referensi.pelanggan') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Master Pelanggan</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.so_flag') }}')" id="{{ route('referensi.so_flag') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                    <span class="menu-title">Ref SO Flag Marketing</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>                        
                    </ul>
                </div>
            </li>
            @endif
            @if(Auth::user()->role!=1 && Auth::user()->role!=2 && Auth::user()->role!=0)
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-wrench menu-icon"></i>
                    <span class="menu-title">Produksi</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        @if(Auth::user()->role==3 || Auth::user()->role==4 || Auth::user()->role==5 || Auth::user()->role==99)
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('kartu_produksi.index') }}')" id="{{ route('kartu_produksi.index') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">Kartu Produksi</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->role==3 || Auth::user()->role==99)
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('list_approved_prod.rfp') }}')" id="{{ route('list_approved_prod.rfp') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">Approved PR by Manager</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->role==3 || Auth::user()->role==4 || Auth::user()->role==5 || Auth::user()->role==99)
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('workstation.index') }}')" id="{{ route('workstation.index') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">WorkStation</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->role==3 || Auth::user()->role==99)
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('history_kartu.kartu_produksi') }}')" id="{{ route('history_kartu.kartu_produksi') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">History Kartu</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('rfp_known') }}')" id="{{ route('rfp_known') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">PR Known/Accepted by</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->role!=3 && Auth::user()->role!=4 && Auth::user()->role!=5)
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('rfp_matrix.kartu_produksi') }}')" id="{{ route('rfp_matrix.kartu_produksi') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">PR Information Matrix</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>                        
                        @endif
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('qc') }}')" id="{{ route('qc') }}">
                                <i class="mdi mdi-wrench menu-icon"></i>
                                <span class="menu-title">QC</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif
            @if(Auth::user()->role==99)
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-settings menu-icon"></i>
                    <span class="menu-title">Referensi</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.ket_umum') }}')" id="{{ route('referensi.ket_umum') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Ketentuan Umum</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.print_logo') }}')" id="{{ route('referensi.print_logo') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Logo Sales Order</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.bagian') }}')" id="{{ route('referensi.bagian') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Workstation Reference</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.jns_bahan') }}')" id="{{ route('referensi.jns_bahan') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Jenis Bahan</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.jns_printing') }}')" id="{{ route('referensi.jns_printing') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Jenis Printing</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.kain') }}')" id="{{ route('referensi.kain') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Ref Kain</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.kondisi_kain') }}')" id="{{ route('referensi.kondisi_kain') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Ref Kondisi Kain</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.ori_kondisi') }}')" id="{{ route('referensi.ori_kondisi') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Ref Original Kondisi</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.tekstur_akhir') }}')" id="{{ route('referensi.tekstur_akhir') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Ref Final Texture</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.warna_dasar') }}')" id="{{ route('referensi.warna_dasar') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                <span class="menu-title">Ref Warna Dasar</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>                      
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.user') }}')" id="{{ route('referensi.user') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                    <span class="menu-title">Master User</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li> 
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('referensi.role') }}')" id="{{ route('referensi.role') }}">
                                <i class="mdi mdi-settings menu-icon"></i>
                                    <span class="menu-title">Ref Role</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>                                               
                    </ul>
                </div>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-truck menu-icon"></i>
                    <span class="menu-title">Delivery Orders</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('do') }}')">
                                <i class="mdi mdi-truck menu-icon"></i>
                                <span class="menu-title">Delivery Order</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-square-inc-cash menu-icon"></i>
                    <span class="menu-title">Finance</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('list_approved_print_sj') }}')">
                                <i class="mdi mdi-square-inc-cash menu-icon"></i>
                                <span class="menu-title">Approved Req Print SJ</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('keuangan.nota') }}')">
                                <i class="mdi mdi-square-inc-cash menu-icon"></i>
                                <span class="menu-title">Invoice</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('invoice') }}')">
                                <i class="mdi mdi-square-inc-cash menu-icon"></i>
                                <span class="menu-title">Kontra Bon</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('pelunasan') }}')">
                                <i class="mdi mdi-square-inc-cash menu-icon"></i>
                                <span class="menu-title">Repayment</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>            
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-download menu-icon"></i>
                    <span class="menu-title">Export</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('so_export') }}')" id="{{ route('so_export') }}">
                                <i class="mdi mdi-download menu-icon"></i>
                                <span class="menu-title">Export SO</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('rfp_export') }}')" id="{{ route('rfp_export') }}">
                                <i class="mdi mdi-download menu-icon"></i>
                                <span class="menu-title">Export PR</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link" onclick="loadNewPage('{{ route('kp_export') }}')" id="{{ route('kp_export') }}">
                                <i class="mdi mdi-download menu-icon"></i>
                                <span class="menu-title">Export Kartu Produksi</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>                                 
                    </ul>
                </div>
            </li>
            <!-- <li class="nav-item">
                <a href="pages/forms/basic_elements.html" class="nav-link">
                    <i class="mdi mdi-wrench menu-icon"></i>
                    <span class="menu-title">Form-Element</span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="nav-item">
                <a href="pages/charts/chartjs.html" class="nav-link">
                    <i class="mdi mdi-finance menu-icon"></i>
                    <span class="menu-title">Charts</span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="nav-item">
                <a href="pages/tables/basic-table.html" class="nav-link">
                    <i class="mdi mdi-grid menu-icon"></i>
                    <span class="menu-title">Tables</span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="nav-item">
                <a href="pages/icons/mdi.html" class="nav-link">
                    <i class="mdi mdi-emoticon menu-icon"></i>
                    <span class="menu-title">Icons</span>
                    <i class="menu-arrow"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link">
                    <i class="mdi mdi-codepen menu-icon"></i>
                    <span class="menu-title">Sample Pages</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="submenu">
                    <ul class="submenu-item">
                        <li class="nav-item"><a class="nav-link" href="pages/samples/login.html">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="pages/samples/login-2.html">Login 2</a></li>
                        <li class="nav-item"><a class="nav-link" href="pages/samples/register.html">Register</a></li>
                        <li class="nav-item"><a class="nav-link" href="pages/samples/register-2.html">Register 2</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="pages/samples/lock-screen.html">Lockscreen</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a href="docs/documentation.html" class="nav-link">
                    <i class="mdi mdi-file-document-box-outline menu-icon"></i>
                    <span class="menu-title">Documentation</span></a>
            </li> -->
        </ul>
    </div>
</nav>
