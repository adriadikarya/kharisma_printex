<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PT KHARISMA PRINTEX</title>
    @include('master.head')	
    @include('master.script')
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon2.png') }}">
    
  </head>
  <body>
    <div id="loading" style="display:none;">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_four"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_one"></div>
            </div>
        </div>
    </div>
    <div class="container-scroller">
        <div class="horizontal-menu noDisplay">
            @include('master.header')
            @include('master.menu')
        </div>
        <div class="container-fluid page-body-wrapper">
            <div class="main-panel">
                <div class="content-wrapper" style="background-image: url('{{asset('public/images/bg-login2.jpeg')}}'); background-size:contain;">
                    <div class="col-lg-12 col-12 p-0">
                        <div class="container-fluid">
                            <div class="d-flex align-items-center justify-content-center">
                                <ul class="none">                            
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="so_baru"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;">SO NEW</span>
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="so_outs"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;">SO OUT</span>                                                                        
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="kirim_rfp"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >APPROVED SO</span>                 
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="reject_so"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >REJECTED SO</span>     
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="rfp"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >JO</span>                         
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="approved_rfp_mkt"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >APPROVED JO MKT</span>                 
                                    <div class="badge badge-info mr-1 text-white" style="font-size:16px;" id="rejected_rfp_mkt"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >REJECTED JO MKT</span>     
                                    <div class="badge badge-danger mr-1 text-white" style="font-size:16px;" id="approved_rfp_pro"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >APPROVED JO PROD</span>                 
                                    <div class="badge badge-danger mr-1 text-white" style="font-size:16px;" id="reject_rfp_pro"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >REJECTED JO PROD</span>                 
                                    <div class="badge badge-danger mr-1 text-white" style="font-size:16px;" id="kartu_produksi"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >KARTU PRODUKSI</span> 
                                    <div class="badge badge-danger mr-1 text-white" style="font-size:16px;" id="selesai"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >SELESAI</span>
                                    <div class="badge badge-success mr-1 text-white" style="font-size:16px;" id="req_print_sj"><br></div>                                                
                                    <span class="text-dark" style="font-size:12px;" >REQ PRINT SJ</span> 
                                </ul>
                            </div>
                        </div>
                    </div>                    
                    <div id="pageLoad">
                        @yield('content')
                    </div>
                </div>
                <footer class="footer">
                    <div class="footer-wrap" style="background: #fffffb2b;">
                        <div class="w-100 clearfix">
                            <span class="d-block text-center text-sm-left d-sm-inline-block">© PT. Kharisma Printex, 2019. <br/> Created by Rai - Supervised by Adji, Sutan, Nana.</span>
                            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted &amp; made with <i class="mdi mdi-heart-outline"></i></span>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    @include('auth.change_pass')
    <div id="data_script">@yield('script')</div>            
    <script type="text/javascript">         
        function change_pass()
        {
            var form_change = $('#form_change_pass');
            form_change[0].reset();
            $('#_change_pass').modal({backdrop: 'static', keyboard: false});
        }      
        
        $.sessionTimeout({
            title: 'Session Timeout Notification',
            message: 'Your session is about to expire.',
            logoutButton: 'Logout',
            // warnAfter: (60000 * 10), //warn after 5 seconds
            // redirAfter: (61000 * 10), //redirect after 10 secons,
            warnAfter: (180000 * 10),
            redirAfter: (181000 * 10),
            countdownMessage: 'Redirecting in {timer} seconds.',
            countdownBar: !0,
            onRedir: function() {
                endSession();
            }
        });

        function endSession() {
            $.ajax({
                type: 'POST',
                url: base_url + '/logout2',
                success: function(res) {
                    window.location.href = base_url + '/login';
                }
            }).done(function(res) {}).fail(function(res) {});
        }

        $(function(){
            $.ajax({
                type: 'GET',
                url: base_url + '/select/count_posisi',
                success: function (res) {
                    //var data = $.parseJSON(res);                
                }
            }).done(function(res){                
                var data = $.parseJSON(res);
                console.log(data)
                $.each(data.countPos, function(k,v){
                    console.log(v)
                    $('#so_baru').text(v.so_baru);
                    $('#so_outs').text(v.so_outs);
                    $('#kirim_rfp').text(v.kirim_rfp);
                    $('#reject_so').text(v.rejected_so);
                    $('#rfp').text(v.rfp);
                    $('#approved_rfp_mkt').text(v.approved_rfp_mkt);
                    $('#rejected_rfp_mkt').text(v.rejected_rfp_mkt);
                    $('#approved_rfp_pro').text(v.approved_rfp_prod);
                    $('#reject_rfp_pro').text(v.rejected_rfp_prod);
                    $('#kartu_produksi').text(v.kartu_produksi);
                    $('#selesai').text(v.selesai);
                });
                $('#req_print_sj').text(data.countReqPSJ[0].req_print_sj);
            });

            $.ajax({
                type: 'GET',
                url: base_url + '/home/menu',
                success: function(res) {

                }
            }).done(function(res){   
                var html_menu = '';             
                if(res.rc==0){
                    html_menu = '<ul class="nav page-navigation">';                    
                    $.each(res.data, function(k,v){
                        console.log(v)
                        if(v.head){
                            if(v.head.stand_alone){
                                html_menu += '<li class="nav-item" onclick="menuMobile(this)">';
                                    html_menu += '<a href="#" class="nav-link" onclick="loadNewPage(\''+v.head.rut_name+'\');hideMenuMobile();" id="\''+v.head.rut_name+'\'">';
                                        html_menu += '<i class="'+v.head.icon_menu+' menu-icon"></i>';
                                        html_menu += '<span class="menu-title">'+v.head.nama_menu+'</span>';
                                    html_menu += '</a>';
                                html_menu+='</li>';
                            } else {
                                html_menu += '<li class="nav-item" onclick="menuMobile(this)">';
                                    html_menu+='<a class="nav-link"><i class="'+v.head.icon_menu+' menu-icon"></i><span class="menu-title">'+v.head.nama_menu+'</span><i class="menu-arrow"></i></a>';
                                if(v.item){
                                    html_menu +='<div class="submenu"><ul class="submenu-item">';
                                    $.each(v.item, function(k1,v1){                                        
                                        html_menu += '<li class="nav-item">';
                                        html_menu += '<a href="#" class="nav-link" onclick="loadNewPage(\''+v1.rut_name+'\');hideMenuMobile();" id="\''+v1.rut_name+'\'">';
                                        html_menu += '<i class="'+v1.icon_menu+' menu-icon"></i>';
                                        html_menu += '<span class="menu-title">'+v1.nama_menu+'</span>';
                                        html_menu += '</a></li>';                                        
                                    });
                                    html_menu +='</ul></div>';
                                }
                                html_menu += '</li>';
                            }
                        }                                                
                    });
                    html_menu+='</ul>';
                    $('#list_menu').html(html_menu);
                } else {
                    swal('ERROR',res.rm,'error');
                }
            });
            
        });
    </script>    
  </body> 
</html>