<nav class="navbar top-navbar col-lg-12 col-12 p-0">
    <div class="container-fluid">
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-between">
            <ul class="navbar-nav navbar-nav-left">                
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center"
                        id="notificationDropdown" href="#" data-toggle="dropdown">                        
                        <div class="badge badge-primary mr-1" style="font-size:16px;">2<br></div>                                                
                        <span class="text-dark" style="font-size:12px;">SO BARU</span>                                                
                    </a>              
                </li>
                <li class="nav-item dropdown">                    
                    <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center"
                        id="messageDropdown" href="#" data-toggle="dropdown">
                        <div class="badge badge-success mr-1 text-white" style="font-size:16px;">2<br></div>                                                
                        <span class="text-dark" style="font-size:12px;">APPROVED SO</span> 
                    </a>                    
                </li> 
                <li class="nav-item dropdown">                    
                    <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center"
                        id="messageDropdown" href="#" data-toggle="dropdown">
                        <div class="badge badge-info mr-1 text-white" style="font-size:16px;">2<br></div>                                                
                        <span class="text-dark" style="font-size:12px;">RFP</span> 
                    </a>                    
                </li>                
                <li class="nav-item dropdown">                    
                    <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center"
                        id="messageDropdown" href="#" data-toggle="dropdown">
                        <div class="badge badge-success mr-1 text-white" style="font-size:16px;">2<br></div>                                                
                        <span class="text-dark" style="font-size:12px;">APPROVED RFP MKT</span> 
                    </a>                    
                </li> 
                <li class="nav-item dropdown">                    
                    <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center"
                        id="messageDropdown" href="#" data-toggle="dropdown">
                        <div class="badge badge-success mr-1 text-white" style="font-size:16px;">2<br></div>                                                
                        <span class="text-dark" style="font-size:12px;">APPROVED RFP PROD</span> 
                    </a>                    
                </li>  -->
            </ul>
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <!-- <a class="navbar-brand brand-logo" href="index.html"><img src="{{ asset('images/logo-kp2-MODIF.jpg') }}" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('images/logo-kp-modif.jpg') }}" alt="logo" /></a> -->
                <!-- <p class="navbar-brand brand-logo" alt="logo"><b>PT KHARISMA PRINTEX</b></p> -->
                <a class="navbar-brand brand-logo" href="#"><img src="{{ asset('images/target5k.jpg') }}" style="height: 130px;width: 314px;margin-top: 31px;" alt="logo"/></a>
                <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('images/target5k.jpg') }}" style="height: 70px;margin-left: -33px;margin-top: 10px;" alt="logo" /></a>
            </div>
            <ul class="navbar-nav navbar-nav-right">
                <!-- <li class="nav-item dropdown  d-lg-flex d-none">
                    <button type="button" class="btn btn-inverse-primary btn-sm">Product </button>
                </li>
                <li class="nav-item dropdown d-lg-flex d-none">
                    <a class="dropdown-toggle show-dropdown-arrow btn btn-inverse-primary btn-sm" id="nreportDropdown"
                        href="#" data-toggle="dropdown">
                        Reports
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list"
                        aria-labelledby="nreportDropdown">
                        <p class="mb-0 font-weight-medium float-left dropdown-header">Reports</p>
                        <a class="dropdown-item">
                            <i class="mdi mdi-file-pdf text-primary"></i>
                            Pdf
                        </a>
                        <a class="dropdown-item">
                            <i class="mdi mdi-file-excel text-primary"></i>
                            Exel
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown d-lg-flex d-none">
                    <button type="button" class="btn btn-inverse-primary btn-sm">Settings</button>
                </li> -->
                <li class="nav-item nav-profile dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                        <span class="nav-profile-name">{{Auth::user()->name}}</span>
                        <span class="online-status"></span>
                        <span class="mdi mdi-account-box" alt="profile"></span>
                        
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                        <a style="cursor: pointer;" class="dropdown-item" onclick="change_pass();">
                            <i class="mdi mdi-settings text-primary"></i>
                            Change Password
                        </a>
                        <form class="dropdown-item" id="formLogout" action="{{ route('logout') }}" method="post">
                            {{ csrf_field() }}                         
                            <a style="cursor: pointer;" onclick="document.getElementById('formLogout').submit();">
                                <i class="mdi mdi-logout text-primary"></i>
                                logout
                            </a>
                        </form>    
                    </div>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                data-toggle="horizontal-menu-toggle">
                <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </div>
</nav>
