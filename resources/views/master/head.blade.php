<link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('vendors/base/vendor.bundle.base.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('vendors/select2/select2.min.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}" type="text/css">
<!-- datepicker -->
<link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
<!-- datepicker -->
<link rel="stylesheet" href="{{ asset('vendors/sweetalert/sweetalert.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/loader27.css') }}">
<link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}" type="text/css">

<!-- <link rel="shortcut icon" href="{{ asset('images/logo-kp-modif.jpg') }}" /> -->

