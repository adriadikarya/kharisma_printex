<script src="{{ asset('vendors/base/vendor.bundle.base.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-session-timeout.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/buttons.print.min.js') }}"></script>
<script src="{{ asset('vendors/chart.js/Chart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/template.js') }}"></script>
<script src="{{ asset('vendors/progressbar.js/progressbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/justgage/raphael-2.1.4.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/justgage/justgage.js') }}" type="text/javascript"></script>
<!-- datepicker -->
<script src="{{ asset('vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/datetimepicker.js') }}"></script>
<script src="{{ asset('js/daterangepicker.min.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('vendors/sweetalert/sweetalert.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/_global.js') }}"></script>
<script src="{{ asset('vendors/typeahead.js/typeahead.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/select2/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/file-upload.js') }}"></script>
<script src="{{ asset('js/typeahead.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>
<!-- validasi -->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script>

$.validator.addMethod( "extension", function( value, element, param ) {
	param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
	return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
}, $.validator.format( "Please enter a value with a valid extension." ) );

$.validator.addMethod("greaterThanZero", function(value, element) {
	var nilai = formatulang(value);
    return this.optional(element) || (parseFloat(nilai) > 0);
}, $.validator.format( "Amount must be greater than zero" ));

$.validator.addMethod( "maxsize", function( value, element, param ) {
	if ( this.optional( element ) ) {
		return true;
	}

	if ( $( element ).attr( "type" ) === "file" ) {
		if ( element.files && element.files.length ) {
			for ( var i = 0; i < element.files.length; i++ ) {
				if ( element.files[ i ].size > param ) {
					return false;
				}
			}
		}
	}

	return true;
}, $.validator.format( "File size must not exceed {0} bytes each." ) );

$.validator.addMethod("regex",function(value, element, regexp) {
		var re = new RegExp(regexp);
		console.log(re.test(value))		
        return this.optional(element) || re.test(value);
}, $.validator.format( "First character for password must be letter only!" ) );

$.validator.addMethod("nowhitespace", function(t, e) {
    return this.optional(e) || /^\S+$/i.test(t)
}, $.validator.format("Tidak Boleh Menggunakan Spasi") );
		
$.validator.addMethod("userName", function(t, e) {
	return this.optional(e) || /^[a-zA-Z][a-zA-Z0-9_.\s]+$/i.test(t)
}, $.validator.format("Username Tidak Boleh Diawali oleh Angka!") );

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<!-- end validasi -->