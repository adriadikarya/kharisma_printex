@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Make A Invoice</h4>
            <form class="form" id="form_invoice">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-0">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Customer</label>
                                    <div class="col-sm-9">
                                        <div class="form-group row">
                                            <select class="form-control form-control-sm" name="pel" id="pel" onchange="getDataNota(this.value)">
                                                @if($param['pel'])
                                                <option value="">Choose Customer!!</option>
                                                @foreach($param['pel'] as $pel)
                                                <option value="{{ $pel->i_pel }}">{{ $pel->e_nama_pel }}</option>
                                                @endforeach
                                                @endif
                                            </select>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>                                                        
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success btn-icon-text" onclick="return validasi()"><i class="mdi mdi-file-check btn-icon-prepend"></i>Proses</button>
                                <button type="reset" class="btn btn-warning btn-icon-text" onclick="location.reload()"><i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Nota</h4>
                    <input type="hidden" name="total_data" id="total_data"/>
                    <div class="table-responsive pt-3">            
                        <table class="table table-bordered table-striped" id="datatable" width="100%">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>No</th>
                                    <th>Nota Number</th>
                                    <th>Customer</th>
                                    <th>Date Nota</th>
                                    <th>Due Date</th>
                                    <th>Total Nota</th>                                    
                                    <th>Lunas</th>
                                    <th>Invoice</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>            
                    </div>
                </div>
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Invoice</h4>                    
                    <div class="table-responsive pt-3">            
                        <table class="table table-bordered table-striped" id="datatable_invoice" width="100%">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>No</th>
                                    <th>Invoice Number</th>
                                    <th>Customer</th>
                                    <th>Date Invoice</th>                                    
                                    <th>Total Invoice</th>
                                    <th>Total Invoice Residual</th>                                    
                                    <th>Lunas</th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>            
                    </div>
                </div>
            </div> 
            </form>
        </div>
    </div>
</div>
@include('invoice.action')
<script>
    $('#pel').select2();
</script>
@endsection