@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Make A Invoice</h4>
            <form class="form" id="form_input_invoice">
            @csrf
                <button type="submit" onclick="prosesSimpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>
                <button type="button" onclick="location.reload()" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">No. Invoice</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="no_invoice" id="no_invoice" maxlength="20" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Date Invoice</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark tgl_input" name="d_invoice" id="d_invoice" readonly />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right">Customer</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control form-control-sm border-dark" name="nama_pel" id="nama_pel" value="{{$param['namapel']}}" readonly />
                                        <input type="hidden" name="pel" id="pel" value="{{$param['ipel']}}" />
                                    </div>
                                </div>
                            </div> 
                        </div>                        
                    </div>
                </div>
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">List Nota</h4>
                    <input type="hidden" name="total_data" id="total_data" value="{{count($param['data'])}}"/>
                    <div class="table-responsive pt-3">            
                        <table class="table table-bordered table-striped" id="datatable" width="100%">
                            <thead>
                                <tr style="text-align:center;background:lightblue;color:black;">                                    
                                    <th>No</th>
                                    <th>Nota Number</th>                                    
                                    <th>Date Nota</th>
                                    <th>Due Date</th>
                                    <th>Total Nota</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($param['data'] as $key => $item)
                                    <tr>
                                        <td align="center">{{$key+1}}<input type="hidden" name="i_nota{{$key+1}}" id="i_nota{{$key+1}}" value="{{$item->i_nota}}"></td>
                                        <td align="center">{{$item->i_nota_code}}</td>
                                        <td align="center">{{date('d M Y', strtotime($item->d_nota))}}</td>
                                        <td align="center">{{$item->d_due_date}}</td>
                                        <td align="right">{{number_format($item->v_total_nppn)}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="right" colspan="4">Total Invoice :</td>
                                    <td align="right">{{number_format($param['totnilainota'])}}
                                        <input type="hidden" name="totnilai" id="totnilai" value="{{$param['totnilainota']}}"/>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>            
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('invoice.action')
@endsection