<script type="text/javascript">

var act_url = '{{ route('data_invoice',':id') }}';
var act_url = act_url.replace(':id',pel);
var table_invoice = $('#datatable_invoice').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    ajax: {
        "url" : act_url,                                
        "error": function(jqXHR, textStatus, errorThrown)
        {
            // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            alert(errorThrown);
        }
    },        
    columns: [        
        { data: 'action', name: 'action', className: 'text-center' },
        { data: 'urutan', name: 'urutan' },
        { data: 'i_invoice_code', name: 'i_invoice_code' },
        { data: 'e_nama_pel', name: 'e_nama_pel' },        
        { data: 'd_invoice', name: 'd_invoice' },        
        { data: 'v_total_invoice', name: 'v_total_invoice' },
        { data: 'v_total_invoice_sisa', name: 'v_total_invoice_sisa' },
        { data: 'f_lunas', name: 'f_lunas' },        
    ]            
});

$('.tgl_input').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    language: "id",
    orientation: "bottom left",
    autoclose: true,
    todayHighlight: true
});

function cek(klik){
    var input = $(klik)
    console.log(input.prop("checked"));
}

function getDataNota(pel){    
    var table = $('#datatable');
    var total = 0;
    if(pel==''){
        table.DataTable().clear().destroy();
        $('#total_data').val(total);
    } else {            
        table.DataTable().clear().destroy();        
        var act_url1 = '{{ route('data_nota_invoice',':id') }}';
        var act_url1 = act_url1.replace(':id',pel);
        table = table.DataTable({
            aaSorting: [],
            processing: true,
            serverSide: true,
            ajax: {
                "url" : act_url1,                                
                "error": function(jqXHR, textStatus, errorThrown)
                {
                    // toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                    alert(errorThrown);
                }
            },        
            columns: [        
                { data: 'action', name: 'action', className: 'text-center' },
                { data: 'urutan', name: 'urutan' },
                { data: 'i_nota_code', name: 'i_nota_code' },
                { data: 'e_nama_pel', name: 'e_nama_pel' },        
                { data: 'd_nota', name: 'd_nota' },
                { data: 'd_due_date', name: 'd_due_date' },                                
                { data: 'v_total_nppn', name: 'v_total_nppn' },
                { data: 'f_lunas', name: 'f_lunas' },
                { data: 'f_invoice', name: 'f_invoice' },                
            ]            
        });   
        table.on( 'xhr', function () {
            var json = table.ajax.json();
            $('#total_data').val(json.recordsTotal);
        } );       
    }    
}

function validasi(){    
    var result = new Array();    
    $('input[id^="pilih"]').each(function() {
        var input = $(this);        
        if (input.prop("checked") === true) {            
            result.push(input.val());
        }
    })                
    if(result.length>0){    
        $('#form_invoice').validate({
            rules: {            
                pel: {
                    required: !0
                }          
            },
            errorPlacement: function(label, element) {
                label.addClass('mt-2 text-danger');            
                if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                    label.insertAfter(element.next('.select2-container'));
                } else {
                    label.insertAfter(element);
                }
            },
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-danger').removeClass('has-success')
                $(element).addClass('form-control-danger').removeClass('form-control-success')
            },
            unhighlight: function (element, errorClass) {
                $(element).parent().addClass('has-success').removeClass('has-danger')
                $(element).addClass('form-control-success').removeClass('form-control-danger')
            },
            submitHandler: function(e){            
                // var form_nota = $('#form_invoice');
                // var _form_data = new FormData(form_nota[0]);
                var result = new Array();    
                $('input[id^="pilih"]').each(function() {
                    var input = $(this);
                    if (input.prop("checked") == true) {
                        result.push(input.val());
                    }
                })                
                var ipel = $('#pel').val();                
                var total_data = $('#total_data').val();
                $.ajax({
                    type: "POST",
                    url: base_url + '/invoice/form_invoice',
                    data: {
                        pel: ipel,
                        result: result,
                        total_data: total_data                        
                    },                    
                    beforeSend: function() {
                        $('#loading').show();
                    },
                    success: function(res) {
                        
                    },
                }).done(function(res){
                    $('#loading').hide();
                    countPosisi();
                    $('#pageLoad').html(res).fadeIn('slow');                
                }).fail(function(xhr,textStatus,errorThrown){
                    $('#loading').hide();
                    swal(textStatus,errorThrown,'error');
                });                
            }
        });    
    } else {
        swal('Informasi','Minimal 1 Nota Harus Dipilih','info');
        return false;
    }
}

function prosesSimpan(){
    $('#form_input_invoice').validate({
        rules: {            
            no_invoice: {
                required: !0
            },
            d_invoice: {
                required: !0
            }          
        },
        errorPlacement: function(label, element) {
            label.addClass('mt-2 text-danger');            
            if(element.hasClass('select2-hidden-accessible') && element.next('.select2-container').length) {
                label.insertAfter(element.next('.select2-container'));
            } else {
                label.insertAfter(element);
            }
        },
        highlight: function(element, errorClass) {
            $(element).parent().addClass('has-danger').removeClass('has-success')
            $(element).addClass('form-control-danger').removeClass('form-control-success')
        },
        unhighlight: function (element, errorClass) {
            $(element).parent().addClass('has-success').removeClass('has-danger')
            $(element).addClass('form-control-success').removeClass('form-control-danger')
        },
        submitHandler: function(e){            
            var form_nota = $('#form_input_invoice');
            var _form_data = new FormData(form_nota[0]);   
            swal({   
                title: "Anda Yakin?",   
                text: "Yakin Data Sudah Benar Semua?",   
                type: "info",                       
                showCancelButton: true,   
                confirmButtonColor: "#e6b034",   
                confirmButtonText: "Ya",                     
                cancelButtonText: "Tidak",   
                closeOnConfirm: false,   
                closeOnCancel: false,
                showLoaderOnConfirm: true 
            }, function(isConfirm){   
                if (isConfirm) {     
                    $.ajax({
                        type: "POST",
                        url: base_url + '/invoice/simpan',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#loading').show();
                        },
                        success: function(res) {
                            
                        },
                    }).done(function(res){
                        var data = $.parseJSON(res);
                        if(data.code==1){
                            swal({   
                                title: "Sukses",   
                                text: data.msg,   
                                type: "success",                       
                                showCancelButton: false,   
                                confirmButtonColor: "#e6b034",   
                                confirmButtonText: "Ya",                     
                                closeOnConfirm: true,                                       
                                showLoaderOnConfirm: true 
                            }, function(isConfirm){   
                                if(isConfirm){
                                    loadNewPage('{{ route('invoice') }}');
                                }
                            });
                        } else {
                            swal('Gagal',data.msg,'error');
                        }
                    }).fail(function(xhr,textStatus,errorThrown){
                        $('#loading').hide();
                        swal(textStatus,errorThrown,'error');
                    });                        
                } else {     
                    swal("Cancel", "Data Invoice Tidak Jadi Disimpan :)", "error");   
                } 
            });
        }
    });
}

function lihatKontraBon(id){
    loadNewPage(base_url + '/invoice/lihat/' +id);
}

function cancel(id){
    swal({   
        title: "Are you sure?",   
        text: "Sure Cancel This KontraBon?",   
        type: "info",                       
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Yes",                     
        cancelButtonText: "No",   
        closeOnConfirm: false,   
        closeOnCancel: false,
        showLoaderOnConfirm: true 
    }, function(isConfirm){   
        if (isConfirm) { 
            $.ajax({
                type: "POST",
                url: base_url + '/invoice/cancel_kontrabon',
                data: { id: id },                
                beforeSend: function() {
                    $('#loading').show();
                },
                success: function(res) {
                    
                },
            }).done(function(res){
                var data = $.parseJSON(res);
                if(data.code==1){
                    swal({   
                        title: "Success",   
                        text: data.msg,   
                        type: "success",                       
                        showCancelButton: false,   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Ya",                     
                        closeOnConfirm: true,                                       
                        showLoaderOnConfirm: true 
                    }, function(isConfirm){   
                        if(isConfirm){
                            loadNewPage('{{ route('invoice') }}');
                        }
                    });
                } else {
                    swal('Gagal',data.msg,'error');
                }
            }).fail(function(xhr,textStatus,errorThrown){
                $('#loading').hide();
                swal(textStatus,errorThrown,'error');
            });          
        } else {
            swal('Regret', 'Regreting Cancel KontraBon', 'error');
        }
    });
}
</script>