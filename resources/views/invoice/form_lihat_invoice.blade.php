@extends($reqAjax == "ajax" ? 'master.only_content' : 'master.master')
@section('content')
@if($param['datahead'])
@foreach($param['datahead'] as $item)
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Look A Nota</h4>
            <form class="form" id="form_input_nota">
            @csrf
                {{--<button type="submit" onclick="prosesSimpan()" class="btn btn-success btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Simpan
                </button>--}}
                <button type="button" onclick="loadNewPage('{{route('invoice')}}')" class="btn btn-warning btn-icon-text">
                    <i class="mdi mdi-keyboard-backspace btn-icon-prepend"></i>
                    Kembali
                </button>
                <div class="row">&nbsp;</div>                                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">No. Invoice</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control form-control-sm border-dark" name="no_invoice" id="no_invoice" maxlength="20" value="{{$item->i_invoice_code}}" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Date Invoice</label>
                            <div class="col-sm-8">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="d_invoice" id="d_invoice" value="{{date('d M Y', strtotime($item->d_invoice))}}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-right">Customer</label>
                            <div class="col-sm-8">                                
                                <input type="text" class="form-control form-control-sm border-dark" name="nama_pel" id="nama_pel" value="{{$item->e_nama_pel}}" readonly />                                
                            </div>
                        </div>
                    </div>                                                        
                    <div class="col-md-12">                                                
                        <div class="table-responsive pt-1">
                            <table class="table table-bordered" id="table-harga">
                                <thead style="background-color: lightcyan;">
                                    <tr>
                                        <th colspan="6" align="text-dark" style="text-align: center;vertical-align: middle;">DETAIL NOTA</th>
                                    </tr>
                                    <tr>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">No.</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">Nota Number</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle">Date Nota</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">Due Date</th>
                                        <th class="text-dark" style="text-align: center;vertical-align: middle;">Total Nota (Rp.)</th>                                        
                                    </tr>                                    
                                </thead>
                                <tbody>                                    
                                    @if($param['dataitem'])  
                                        @foreach($param['dataitem'] as $key => $notaitem)
                                            <tr>
                                                <td align="center">{{$key+1}}</td>
                                                <td align="center">{{$notaitem->i_nota_code}}</td>
                                                <td align="center">{{date('d M Y', strtotime($notaitem->d_nota))}}</td>
                                                <td align="center">{{$notaitem->d_due_date}}</td>
                                                <td align="right">{{number_format($notaitem->v_nilai_nota)}}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="6" align="center">Maaf Data Nota Tidak Ada!</td>
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td align="right" colspan="4">Total Kontra Bon :</td>
                                        <td align="right">{{number_format($item->v_total_invoice)}}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>  
</div>
@endforeach
@endif
@endsection