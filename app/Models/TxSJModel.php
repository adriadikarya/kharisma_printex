<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxSJModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'tx_sj_kartu';
    public $timestamps = true;
}
