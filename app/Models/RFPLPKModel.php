<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RFPLPKModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'rfp_lpk';
    public $timestamps = true;
}
