<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrderDetailModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'mst_so_item';
    public $timestamps = true;
}
