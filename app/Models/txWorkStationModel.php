<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class txWorkStationModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'tx_workstation';
    public $timestamps = true;
}
