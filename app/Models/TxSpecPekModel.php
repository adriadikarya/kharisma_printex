<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxSpecPekModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'tx_spec_pekerjaan';
    public $timestamps = true;
}
