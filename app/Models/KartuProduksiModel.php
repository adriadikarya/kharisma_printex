<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KartuProduksiModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'mst_kartu';
    public $timestamps = true;
}
