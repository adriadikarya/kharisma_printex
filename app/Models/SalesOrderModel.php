<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrderModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'mst_so';
    public $timestamps = true;
    // protected $dateFormat = 'Y-m-d H:i:sO';
}
