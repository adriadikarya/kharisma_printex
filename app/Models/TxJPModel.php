<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxJPModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'tx_jns_proses_kartu';
    public $timestamps = true;
}
