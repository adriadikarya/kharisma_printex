<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RFPModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'rfp';
    public $timestamps = true;
}
