<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TxSpecKainModel extends Model
{
    protected $primaryKey = 'i_id';
    protected $table = 'tx_spec_kain';
    public $timestamps = true;
}
