<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class KeuanganController extends Controller
{
    //
    public function index()
    {
        $param['pel'] = \DB::select("SELECT * FROM ref_pelanggan WHERE i_pel<>0");
        $param['notaitem'] = array();
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.index',compact('reqAjax','param'));
    }

    public function data_nota(Request $request){
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY mn.i_nota)) as urutan, mn.*, rp.e_nama_pel FROM mst_nota mn 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mn.i_pel                 
        ORDER BY mn.i_nota");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            if($data->f_invoice===false){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" href="#" onclick="lihatNota('.$data->i_nota.')"><i class="mdi mdi-clipboard-check"></i> Lihat Nota</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="edit('.$data->i_nota.')"><i class="mdi mdi-check-circle-outline"></i> Edit Nota</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="printNota('.$data->i_nota.')"><i class="mdi mdi-printer"></i> Print Nota</a>
                    </div>
                </div>';          
            } else {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" href="#" onclick="lihatNota('.$data->i_nota.')"><i class="mdi mdi-clipboard-check"></i> Lihat Nota</a>                        
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#" onclick="printNota('.$data->i_nota.')"><i class="mdi mdi-printer"></i> Print Nota</a>
                    </div>
                </div>';
            }
        })
        ->editColumn('d_nota', function($data) {            
            return date('d M Y', strtotime($data->d_nota));            
        })
        ->editColumn('d_due_date', function($data) {            
            return date('d M Y', strtotime($data->d_due_date));            
        })
        ->editColumn('v_total_nppn', function($data) {            
            return 'Rp. '.number_format($data->v_total_nppn);            
        })
        ->editColumn('f_lunas', function($data) {
            if($data->f_lunas == 1) {
                return 'Lunas';
            } else {
                return 'Belum Lunas';
            }
        }) 
        ->editColumn('f_invoice', function($data) {
            if($data->f_invoice == 1) {
                return 'Sudah';
            } else {
                return 'Belum';
            }
        })
        ->make(true);
    }

    public function input_nota(Request $request)
    {
        $q_no_nota = \DB::select("SELECT substr(i_nota_code,6,5) as nomor FROM mst_nota ORDER BY i_nota_code DESC LIMIT 1");
        if(!empty($q_no_nota)){
            $nomor_nota = $q_no_nota[0]->nomor+1;
            $nomor_nota = str_pad($nomor_nota,5,"0",STR_PAD_LEFT); 
            $param['no_nota'] = 'NOTA-'.$nomor_nota;
        } else {
            $param['no_nota'] = 'NOTA-00001';
        }
        $param['pel'] = $request->input('pel');
        $qpel = collect(\DB::select("SELECT e_nama_pel FROM ref_pelanggan WHERE i_pel='".$request->input('pel')."'"))->first();
        $param['nama_pel'] = $qpel->e_nama_pel;
        $param['qty_pick'] = $request->input('qty_pick');        
        $idsj = $request->input('idsj');
        $exp_idsj = explode('#', $idsj);
        foreach($exp_idsj as $idsj){
            $arr_per_sj[] = \DB::select("SELECT * FROM mst_do_sj WHERE i_id='".$idsj."'");
        }
        $param['arr_sj'] = $arr_per_sj;
        $param['notaitem'] = array();
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.form_nota',compact('reqAjax','param'));
    }

    public function lihat_nota(Request $request){
        $id_nota = $request->get('id');
        $param['notahead'] = \DB::select("SELECT mn.*, rp.e_nama_pel FROM mst_nota mn
        LEFT JOIN ref_pelanggan rp ON mn.i_pel=rp.i_pel WHERE i_nota='".$id_nota."'");
        $param['notaitem'] = \DB::select("SELECT mni.*, mdo.i_no_sj, mdo.d_sj FROM mst_nota_item mni
        JOIN mst_do_sj mdo ON mdo.i_id=mni.i_sj WHERE mni.i_nota='".$id_nota."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.lihat_nota',compact('reqAjax','param'));
    }

    public function edit_nota(Request $request){
        $id_nota = $request->get('id');
        $param['notahead'] = \DB::select("SELECT mn.*, rp.e_nama_pel FROM mst_nota mn
        LEFT JOIN ref_pelanggan rp ON mn.i_pel=rp.i_pel WHERE i_nota='".$id_nota."'");
        $param['notaitem'] = \DB::select("SELECT mni.*, mdo.i_no_sj, mdo.d_sj FROM mst_nota_item mni
        JOIN mst_do_sj mdo ON mdo.i_id=mni.i_sj WHERE mni.i_nota='".$id_nota."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.form_edit_nota',compact('reqAjax','param'));
    }

    public function print_nota(Request $request){
        $id_nota = $request->get('id');
        $param['notahead'] = \DB::select("SELECT mn.*, rp.e_nama_pel, rp.e_alamat_pel FROM mst_nota mn
        JOIN ref_pelanggan rp ON mn.i_pel=rp.i_pel WHERE i_nota='".$id_nota."'");
        $param['notaitem'] = \DB::select("SELECT mni.*, mdo.i_no_sj, mdo.d_sj FROM mst_nota_item mni
        JOIN mst_do_sj mdo ON mdo.i_id=mni.i_sj WHERE mni.i_nota='".$id_nota."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.print_nota',compact('reqAjax','param'));
    }

    public function simpan_nota(Request $request){
        \DB::beginTransaction();
        $qId = \DB::select("SELECT i_nota FROM mst_nota ORDER BY i_nota DESC LIMIT 1");
        if(empty($qId)){
            $id_nota = 1;
        } else {
            $id_nota = $qId[0]->i_nota+1;
        } 
        try{
            \DB::table('mst_nota')->insert([
                'i_nota' => $id_nota ,
                'i_nota_code' => $request->input('no_nota'),
                'i_pel' => $request->input('pel'),
                'd_nota' => date('Y-m-d', strtotime($request->input('d_nota'))),
                'd_due_date' => date('Y-m-d', strtotime($request->input('d_jth_tempo'))),
                'n_diskon' => $request->input('diskon_percent'),
                'v_diskon' => str_replace(',','',$request->input('diskon_nilai')),
                'n_tot_qty' => $request->input('totqty'),
                'v_total_nota' => str_replace(',','',$request->input('total')),
                'v_total_nppn' => str_replace(',','',$request->input('grand_tot')),
                'v_total_nppn_sisa' => str_replace(',','',$request->input('grand_tot')),                
                'created_at' => date('Y-m-d h:i:s'),                
                'n_ppn' => $request->input('ppn_percent'),
                'v_ppn' => str_replace(',','',$request->input('ppn')),
                'i_qty_from' => $request->input('qty_pick')
            ]);

            for($i=1; $i<=$request->input('totrow'); $i++){
                \DB::table('mst_nota_item')->insert([    
                    'i_nota' => $id_nota,
                    'i_sj' => $request->input('idsj'.$i),
                    'n_qty' => $request->input('qty_'.$i),
                    'v_hrg_satuan' => str_replace(',','',$request->input('hrg_sat'.$i)),
                    'i_qty_from' => $request->input('qty_pick'),
                    'created_at' => date('Y-m-d h:i:s')
                ]);
                \DB::table('mst_do_sj')->where('i_id',$request->input('idsj'.$i))
                ->update(['f_nota' => true]);
            }
            \DB::commit();
            return $this->sendResponse('1','Input Nota No '.$request->input('no_nota').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Input Nota No '.$request->input('no_nota').' gagal');
        }
    }

    public function update_nota(Request $request)
    {
        // dd($request->all());exit();
        \DB::beginTransaction();
        try{
            \DB::table('mst_nota')->where('i_nota', $request->input('id_nota'))
            ->update([                                                
                'd_nota' => date('Y-m-d', strtotime($request->input('d_nota'))),
                'd_due_date' => date('Y-m-d', strtotime($request->input('d_jth_tempo'))),
                'n_diskon' => $request->input('diskon_percent'),
                'v_diskon' => str_replace(',','',$request->input('diskon_nilai')),
                'n_tot_qty' => $request->input('totqty'),
                'v_total_nota' => str_replace(',','',$request->input('total')),
                'v_total_nppn' => str_replace(',','',$request->input('grand_tot')),
                'v_total_nppn_sisa' => str_replace(',','',$request->input('grand_tot')),                
                'updated_at' => date('Y-m-d h:i:s'),                
                'n_ppn' => $request->input('ppn_percent'),
                'v_ppn' => str_replace(',','',$request->input('ppn'))
            ]);
            \DB::table('mst_nota_item')->where('i_nota', $request->input('id_nota'))->delete();
            for($i=1; $i<=$request->input('totrow'); $i++){                
                if($request->input('pilih_hapus'.$i)=='y'){
                    \DB::table('mst_do_sj')->where('i_id',$request->input('idsj'.$i))
                    ->update(['f_nota' => false]);
                } else {
                    \DB::table('mst_nota_item')->insert([    
                        'i_nota' => $request->input('id_nota'),
                        'i_sj' => $request->input('idsj'.$i),
                        'n_qty' => $request->input('qty_'.$i),
                        'v_hrg_satuan' => str_replace(',','',$request->input('hrg_sat'.$i)),
                        'i_qty_from' => $request->input('qty_pick'),
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                    \DB::table('mst_do_sj')->where('i_id',$request->input('idsj'.$i))
                    ->update(['f_nota' => true]);
                }                
            }

            \DB::commit();
            return $this->sendResponse('1','Edit Nota No '.$request->input('no_nota').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Edit Nota No '.$request->input('no_nota').' gagal');
        }
    }

    public function list_sj($pel)
    {
        $data = \DB::select("SELECT mdo.*, rp.e_nama_pel FROM mst_do_sj mdo JOIN ref_pelanggan rp ON mdo.i_pel=rp.i_pel WHERE mdo.i_pel='".$pel."' AND f_print='t' AND f_nota='f' ORDER BY i_id");
        return json_encode($data);
    }

    public function list_approved_print_sj()
    {
        $param['notaitem'] = array();
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.list_approved',compact('reqAjax','param'));
    }

    public function data_print_sj()
    {
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY mdo.i_id)) as urutan, mdo.*, rp.e_nama_pel, rw.definition FROM mst_do_sj mdo 
        JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel 
        LEFT JOIN ref_workflow rw ON rw.i_id=mdo.i_status
        WHERE mdo.i_status=16
        ORDER BY mdo.i_id");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" href="#" onclick="lihatSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-clipboard-check"></i> Lihat SJ</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="approved('.$data->i_id.')"><i class="mdi mdi-check-circle-outline"></i> Approved SJ</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="reject('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Reject SJ</a>
                </div>
            </div>';          
        })
        ->make(true);
    }

    public function lihat_do(Request $request)
    {
        $idkartu = $request->get('idkartu');
        $idrfp = $request->get('idrfp');
        /* old query */
        /* $param['data'] = \DB::select("SELECT mdo.*, rp.e_nama_pel, rp.e_alamat_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing,
        rk.e_kain, sum(mkc.n_qty_roll) as roll_pem, sum(mkc.n_qty_pjg) as mtr_pem, sum(mkc.n_qty_kg) as kg_pem,
        tw.d_tgl as tgl_produksi, ms.i_no_so, ms.i_no_po
        FROM mst_do_sj mdo 
        JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel
        JOIN rfp ON rfp.i_id=mdo.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        JOIN ref_kain rk ON rk.i_id=ms.i_jns_kain
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mdo.i_id_kartu
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=6
        group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=rfp.i_id
        WHERE mdo.i_id='".$request->get('id')."'
        GROUP BY mdo.i_id, rp.e_nama_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, rk.e_kain, tw.d_tgl,
            ms.i_no_so, ms.i_no_po, rp.e_alamat_pel
        ORDER BY mdo.i_id"); */

        $param['data'] = \DB::select("SELECT mdo.*, rp.e_nama_pel, rp.e_alamat_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, sum(mkc.n_qty_roll) as roll_pem, sum(mkc.n_qty_pjg) as mtr_pem, sum(mkc.n_qty_kg) as kg_pem,
        tw.d_tgl as tgl_produksi, ms.i_no_so, ms.i_no_po, rjp.e_jns_printing as nama_printing
        FROM mst_do_sj mdo 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel        
        JOIN rfp ON rfp.i_id=mdo.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so        
        LEFT JOIN ref_jns_printing rjp ON rjp.i_id=cast(ms.e_jenis_printing as integer)
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mdo.i_id_kartu
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=6
        group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=rfp.i_id
        WHERE mdo.i_id='".$request->get('id')."'
        GROUP BY mdo.i_id, rp.e_nama_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, tw.d_tgl,
            ms.i_no_so, ms.i_no_po, rp.e_alamat_pel, rjp.e_jns_printing
        ORDER BY mdo.i_id");
        
        $param['idsj'] = $request->get('id');
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$idrfp."'");
        $param['exsj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['packing'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$idrfp."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('keuangan.form_lihat_sj', compact('reqAjax','param'));
    }

    public function approved_print_sj($id)
    {        
        \DB::table('mst_do_sj')->where('i_id', $id)
            ->update([
                'alasan_reject' => null,
                'i_status' => 17,
                'd_approved' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);

        return $this->sendResponse('1', 'Approved Request Print SJ Berhasil!');
    }

    public function reject_print_sj(Request $request, $id)
    {                
        \DB::table('mst_do_sj')->where('i_id', $id)
            ->update([
                'alasan_reject' => $request->input('input_val'),
                'i_status' => 18,
                'd_approved' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);

        return $this->sendResponse('1', 'Reject Request SJ Berhasil!');
    }
}
