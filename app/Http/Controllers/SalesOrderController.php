<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
// use Yajra\DataTables\DataTables;
use DataTables;
use App\Models\SalesOrderDetailModel;
use App\Models\SalesOrderModel;
use App\Models\TxSpecKainModel;
use App\Models\TxSpecPekModel;
use Illuminate\Support\Facades\Auth;

class SalesOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.index',compact('reqAjax'));
    }

    public function data()
    {
        $data = \DB::select("SELECT mst_so.i_id, i_no_so, d_so, mst_so.i_pel, rp.e_nama_pel, f_repeat, d_strike_off, d_approval_strike_off,
            d_penyerahan_brg, d_approved, i_status, rw.definition, mst_so.created_at, e_reject 
        FROM mst_so 
        LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
        LEFT JOIN ref_workflow rw ON mst_so.i_status = rw.i_id
        order by mst_so.created_at desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            if($data->i_status==0 || $data->i_status==5 || $data->i_status==3 || $data->i_status==401){
                if(Auth::user()->role==99 || Auth::user()->role==2) {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-lead-pencil"></i> Edit SO</a>
                            <div class="dropdown-divider"></div>                        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="kirimApprove('.$data->i_id.')"><i class="mdi mdi-file-send"></i> Kirim Approve</a>
                        </div>
                    </div>';
                } else {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                        </div>
                    </div>';
                }
            } else if($data->i_status==1){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                    </div>
                </div>';
            } else if($data->i_status==2){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="printSO('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-printer"></i> print SO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="kirimRfp('.$data->i_id.')"><i class="mdi mdi-file-send"></i> Kirim ke JO</a>
                    </div>
                </div>';
            } else {
                if(Auth::user()->role==99 || Auth::user()->role==2) {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printSO('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-printer"></i> print SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="downloadSO('.$data->i_id.')"><i class="mdi mdi-download"></i> download QRCode SO</a>
                        </div>
                    </div>';
                } else if(Auth::user()->role==99 || Auth::user()->role==1){
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="downloadSO('.$data->i_id.')"><i class="mdi mdi-download"></i> download QRCode SO</a>
                        </div>
                    </div>';
                } else {
                    return '<a class="btn btn-sm btn-info" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>';
                }
            }
            })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat == 1) {
                return 'Repeat';
            } else {
                return 'Tidak';
            }
        }) 
        ->editColumn('created_at',function($data) {
           return date('d-m-Y H:i:s',strtotime($data->created_at));
        })
        ->editColumn('d_so',function($data) {
            return date('d-m-Y',strtotime($data->d_so));
        })
        ->editColumn('d_approved',function($data) {
            if($data->d_approved){
                return date('d-m-Y H:i:s',strtotime($data->d_approved));
            }               
        })
        ->make(true);
    }

    public function form()
    {
        // $qNoSo = collect(\DB::select("SELECT no_urut, bln, thn FROM ref_no_urut WHERE code='SO' AND thn='".date('Y')."'"))->first();
        // if(!empty($qNoSo)){
        //     $bulan = $this->getRomawi(date('m'));
        //     $tahun = substr($qNoSo->thn,2,2);
        //     $nomor_so = $qNoSo->no_urut.'/OS/B/'.$bulan.'/'.$tahun;
        // } else {
        //     $tahun = substr(date('Y'),2,2);
        //     $nomor_so = '0001/OS/B/'.$this->getRomawi(date('m')).'/'.$tahun;
        // }
        $flag = \DB::table('ref_so_flag')->get();
        $jenis_kain = \DB::table('ref_kain')->get();
        $jns_printing = \DB::table('ref_jns_printing')->get();
        $kondisi_kain = \DB::table('ref_kondisi_kain')->get();
        $pelanggan = \DB::table('ref_pelanggan')->where('i_pel','>',0)->get();
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.form',compact('reqAjax','pelanggan','jenis_kain','flag','jns_printing','kondisi_kain'));
    }

    public function getAlamat($id)
    {
        $qAlamat = collect(\DB::select("SELECT * FROM ref_pelanggan WHERE i_pel='".$id."'"))->first();
        return json_encode(array('alamat' => $qAlamat->e_alamat_pel, 'nama_pel' => $qAlamat->e_nama_pel, 'pkp' => $qAlamat->f_pkp));
    }

    public function repeatOrder($i_pel)
    {
        $qRepeat = \DB::table('tx_spec_pekerjaan')
                    ->where('i_pel',$i_pel)
                    ->get();
        return json_encode($qRepeat);
    }

    function getKain($i_pel, $desain)
    {
        $desain_new = urldecode($desain);
        $qTxSpecKain = \DB::table('tx_spec_kain')
                        ->where('i_pel',$i_pel)
                        ->where('i_desain',$desain_new)
                        ->get();

        $qTxSpecPek = \DB::table('tx_spec_pekerjaan')
                    ->where('i_pel',$i_pel)
                    ->where('i_desain',$desain_new)
                    ->get();

        return json_encode(array('tx_kain' => $qTxSpecKain, 'tx_pek' => $qTxSpecPek));
    }

    function getRomawi($bln){
        switch ($bln){
            case 1: 
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
    }   

    function reformatRomawi($bln){
        switch ($bln){
            case I: 
                return "01";
                break;
            case II:
                return "02";
                break;
            case III:
                return "03";
                break;
            case IV:
                return "04";
                break;
            case V:
                return "05";
                break;
            case VI:
                return "06";
                break;
            case VII:
                return "07";
                break;
            case VIII:
                return "08";
                break;
            case IX:
                return "09";
                break;
            case X:
                return "10";
                break;
            case XI:
                return "11";
                break;
            case XII:
                return "12";
                break;
        }
    }

    public function simpan(Request $request)
    {        
        \DB::beginTransaction();
        $qId = \DB::select("SELECT i_id FROM mst_so ORDER BY i_id DESC LIMIT 1");
        if(empty($qId)){
            $id_so = 1;
        } else {
            $id_so = $qId[0]->i_id+1;
        }   
        $qNoSo = collect(\DB::select("SELECT no_urut, bln, thn FROM ref_no_urut WHERE code='SO' AND thn='".date('Y')."'"))->first();
        if($qNoSo){
            $no_so = $qNoSo->no_urut+1;            
            $no_so = str_pad($no_so,5,"0",STR_PAD_LEFT);                        
        } else {
            $no_so = '00001';
        }   
        try{
            $sales_order = new SalesOrderModel();
            $sales_order->i_id = $id_so;
            $sales_order->i_no_so = $request->input('flag_so').'-'.date('m').date('Y').'-'.$no_so;
            $sales_order->i_no_po = $request->input('no_po');
            // masih error kalau tgl 30 & 31
            $sales_order->d_so = date('Y-m-d', strtotime($request->input('d_so')));
            $sales_order->i_pel = $request->input('pel');
            $sales_order->f_repeat = $request->input('repeat_order')=='new'?false:true;
            $sales_order->i_desain = $request->input('desain_kode1')==''?$request->input('desain_kode2'):$request->input('desain_kode1');
            $sales_order->n_qty_warna = $request->input('jml_warna');
            $sales_order->e_motif = $request->input('motif_nama');
            $sales_order->e_jenis_printing = $request->input('jns_printing');
            $sales_order->n_color_way = $request->input('color_way');
            $sales_order->e_color_way = $request->input('color_way_desc');
            $sales_order->e_toleransi_cacat = $request->input('toleransi_cacat_kain');
            // $sales_order->i_jns_kain = $request->input('jenis_kain');
            $sales_order->e_kondisi_kain = $request->input('kondisi_kain');
            $sales_order->e_lebar = $request->input('lebar');
            $sales_order->e_gramasi = $request->input('gramasi');
            $sales_order->i_penyedia = $request->input('penyedia_kain');
            // masih error kalau tgl 30 & 31
            $sales_order->d_strike_off = date('Y-m-d', strtotime($request->input('tgl_strike_off')));
            // masih error kalau tgl 30 & 31 strtotime('+7 hour',strtotime($data->created_at))
            $sales_order->d_approval_strike_off = date('Y-m-d', strtotime($request->input('approval_strike_off')));
            // masih error kalau tgl 30 & 31
            $sales_order->d_penyerahan_brg = date('Y-m-d', strtotime($request->input('tgl_penyerahan')));
            $sales_order->e_keterangan_kirim = $request->input('ket_penyerahan');
            $sales_order->i_status = 0;
            $sales_order->v_pekerjaan = str_replace(',', '', $request->input('total_harga'));
            $sales_order->v_pekerjaan_plus_ppn = str_replace(',','',$request->input('hrg_pekerjaan'));
            $sales_order->v_discount = str_replace(',','',$request->input('down_payment_nilai'));
            $sales_order->n_discount = str_replace(',','',$request->input('down_payment_percent'));
            $sales_order->v_sisa = str_replace(',','',$request->input('sisa_pembayaran'));
            $sales_order->flag_so = $request->input('flag_so');
            $sales_order->hitung_by = $request->input('hitung_by_real');
            $sales_order->n_ppn = $request->input('ppn_percent');
            $sales_order->v_ppn = str_replace(',','',$request->input('ppn'));
            $sales_order->exclude_include = $request->input('ppn_percent')>0?'E':'I';
            $sales_order->cara_bayar = $request->input('cara_bayar');
            $sales_order->save();

            $totRow = (int)$request->input('totrow');
            for($i=1; $i<=$totRow; $i++){
                $so_detail = new SalesOrderDetailModel();
                $so_detail->i_id_so = $id_so;
                $so_detail->e_uraian_pekerjaan = $request->input('uraian_pek_'.$i);
                $so_detail->n_qty_roll = str_replace(',','',$request->input('roll_'.$i));
                $so_detail->n_qty_pjg = str_replace(',','',$request->input('yard_m_'.$i));
                $so_detail->n_qty_kg = str_replace(',','',$request->input('kg_'.$i));
                $so_detail->v_harga_sat = str_replace(',','',$request->input('hrg_'.$i));
                $so_detail->n_qty_kg_sisa = str_replace(',','',$request->input('kg_'.$i));
                $so_detail->d_strike_off = date('Y-m-d', strtotime($request->input('strike_off_'.$i)));
                $so_detail->e_jenis_kain = $request->input('i_jns_kain_'.$i);
                $so_detail->save();
            }
            $ro = $request->input('repeat_order')=='new'?false:true;

            if($ro==false){
                $tx_spec_pek = new TxSpecPekModel();
                $tx_spec_pek->i_desain = $request->input('desain_kode1')==''?$request->input('desain_kode2'):$request->input('desain_kode1');
                $tx_spec_pek->i_qty_warna = $request->input('jml_warna');
                $tx_spec_pek->e_motif = $request->input('motif_nama');
                $tx_spec_pek->e_jenis_printing = $request->input('jns_printing');
                $tx_spec_pek->n_colow_way = $request->input('color_way');
                $tx_spec_pek->e_color_way = $request->input('color_way_desc');
                $tx_spec_pek->e_toleransi_cacat = $request->input('toleransi_cacat_kain');
                $tx_spec_pek->i_pel = $request->input('pel');
                $tx_spec_pek->save();

                $tx_spec_kain = new TxSpecKainModel();
                // $tx_spec_kain->i_jns_kain = $request->input('jenis_kain');
                $tx_spec_kain->e_kondisi_kain = $request->input('kondisi_kain');
                $tx_spec_kain->e_lebar = $request->input('lebar');
                $tx_spec_kain->e_gramasi = $request->input('gramasi');
                $tx_spec_kain->i_penyedia = $request->input('penyedia_kain');
                $tx_spec_kain->i_pel = $request->input('pel');
                $tx_spec_kain->i_desain = $request->input('desain_kode1')==''?$request->input('desain_kode2'):$request->input('desain_kode1');
                $tx_spec_kain->save();
            }
            
            $qNoSo = collect(\DB::select("SELECT no_urut FROM ref_no_urut WHERE code='SO' AND thn='".date('Y')."'"))->first();
            if(!empty($qNoSo)){
                // $nomorSO = explode('/',$sales_order->i_no_so);            
                // $noUrutSo = $qNoSo->no_urut+1;
                // $noUrutSoNew = str_pad($noUrutSo,4,"0",STR_PAD_LEFT);
                \DB::table('ref_no_urut')
                ->where('code', 'SO')                
                ->where('thn', date('Y'))                
                ->update(array('no_urut' => $no_so));
            } else {
                \DB::table('ref_no_urut')
                ->insert([
                    'code' => 'SO', 
                    'no_urut' => $no_so,                    
                    'thn' => date('Y'),                    
                ]);
            }

            \DB::commit();
            return $this->sendResponse('1','Input Sales Order No '.$sales_order->i_no_so.' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan Input Sales Order Gagal');
        }
    }

    public function kirim_approve($id)
    {
        $sales_order = SalesOrderModel::find($id);
        $sales_order->i_status = 1;
        $sales_order->save();

        return $this->sendResponse('1', 'Kirim Approve Ke Manager Berhasil!');
    }

    public function rfp($id)
    {
        $sales_order = SalesOrderModel::find($id);
        $sales_order->i_status = 4;
        $sales_order->save();

        return $this->sendResponse('1', 'Kirim SO untuk dijadikan bahan JO Berhasil!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $param['datahead'] = \DB::select("SELECT mst_so.*, rp.* FROM mst_so 
                    LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
                    WHERE i_id=".$id." AND i_status in (0,3,5,401)");
        $param['dataitem'] = \DB::select("SELECT * FROM mst_so_item WHERE i_id_so=".$id);

        $jenis_kain = \DB::table('ref_kain')->get();

        $param['tx_pek'] = \DB::table('tx_spec_pekerjaan')
                            ->where('i_pel',$param['datahead'][0]->i_pel)                            
                            ->get();

        $param['jns_printing'] = \DB::table('ref_jns_printing')->get();
        $param['kondisi_kain'] = \DB::table('ref_kondisi_kain')->get();

        $param['pelanggan'] = \DB::table('ref_pelanggan')->where('i_pel','>',0)->get();

        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.form_edit',compact('reqAjax','param','jenis_kain'));

    }

    public function lihat($id,$link)
    {
        $param['datahead'] = \DB::select("SELECT mst_so.*, rp.* FROM mst_so 
                    LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
                    WHERE i_id=".$id."");
        $param['dataitem'] = \DB::select("SELECT * FROM mst_so_item WHERE i_id_so='".$id."' ORDER BY i_id ");

        $jenis_kain = \DB::table('ref_kain')->get();

        $param['jns_printing'] = \DB::table('ref_jns_printing')->get();
        $param['kondisi_kain'] = \DB::table('ref_kondisi_kain')->get();

        $param['pelanggan'] = \DB::table('ref_pelanggan')->where('i_pel','>',0)->get();
        $rut = $link;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.form_lihat',compact('reqAjax','param','rut','jenis_kain'));
    }

    public function print_so($id,$link)
    {
        $param['datahead'] = \DB::select("SELECT mst_so.*, rp.*, rp2.e_nama_pel as penyedia, rjp.e_jns_printing, rkk.e_kondisi_kain FROM mst_so 
                    LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
                    LEFT JOIN ref_pelanggan rp2 ON mst_so.i_penyedia = rp2.i_pel  
                    LEFT JOIN ref_jns_printing rjp ON cast(mst_so.e_jenis_printing as integer) = rjp.i_id
                    LEFT JOIN ref_kondisi_kain rkk ON cast(mst_so.e_kondisi_kain as integer) = rkk.i_id                   
                    WHERE mst_so.i_id=".$id."");
        $param['dataitem'] = \DB::select("SELECT a.*, b.e_kain FROM mst_so_item a
                    LEFT JOIN ref_kain b ON cast(a.e_jenis_kain as integer)=b.i_id
                    WHERE a.i_id_so=".$id." ORDER BY i_id");
        $param['logo'] = \DB::select("SELECT * FROM ref_logo WHERE f_active='t'");
        
        $param['ket_umum'] = \DB::table('ref_ket_umum_so')->orderBy('i_id')->get();
        $param['qrCode'] = $this->qrCodeSo($param);
        $rut = $link;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.print_so',compact('reqAjax','param','rut'));   
    }

    public function qrCodeSo($data)
    {
        $string_qr = "";
        if($data['datahead']){
            foreach($data['datahead'] as $item){
                $string_qr.="NO.SO = ".$item->i_no_so."\n";
                $string_qr.="TGL.SO = ".date('d M Y', strtotime($item->d_so))."\n";
                $string_qr.="PEL = ".$item->e_nama_pel."\n";
                // $string_qr.="PO PEL = ".$item->i_no_po."\n";
                // $string_qr.="ALAMAT PEL = ".$item->e_alamat_pel."\n";
                // $string_qr.="A.\n";
                $string_qr.="DESAIN = ".$item->i_desain."\n";
                // $string_qr.="JML WRN = ".$item->n_qty_warna."\n";
                // $string_qr.="JNS PRINT = ".$item->e_jns_printing."\n";
                // $string_qr.="COLOR WAY = ".$item->n_color_way." ".$item->e_color_way."\n";
                // $string_qr.="TOLERANSI CACAT = ".$item->e_toleransi_cacat."\n";
                // $string_qr.="B.\n";
                // $string_qr.="JNS KAIN = ".$item->e_kain."\n";
                // $string_qr.="KON KAIN = ".$item->e_kondisi_kain."\n";
                // $string_qr.="LEBAR = ".$item->e_lebar." cm/inch\n";
                // $string_qr.="GRAMASI = ".$item->e_gramasi." gr/m2\n";
                // $string_qr.="PENYEDIA KAIN = ".$item->penyedia."\n";
                // $string_qr.="C.\n";
                if($data['dataitem']){
                    $no=1;
                    $totalBerat = 0;
                    foreach($data['dataitem'] as $row){
                        // if($item->hitung_by==1){
                        //     $jmlHrg = $row->n_qty_kg * $row->v_harga_sat;
                        // } else if($item->hitung_by==2) {
                        //     $jmlHrg = $row->n_qty_roll * $row->v_harga_sat;
                        // } else {
                        //     $jmlHrg = $row->n_qty_pjg * $row->v_harga_sat;
                        // }                          
                        // $string_qr.="No.".$no++."\n";
                        // $string_qr.="URAIAN PEK = ".$row->e_uraian_pekerjaan."\n";
                        // $string_qr.="ROLL = ".$row->n_qty_roll."\n";
                        // $string_qr.="PJG = ".$row->n_qty_pjg."\n";
                        // $string_qr.="KG = ".$row->n_qty_kg."\n";
                        // $string_qr.="HRG = ".number_format($row->v_harga_sat,2,'.',',')."\n";
                        // $string_qr.="JML = ".number_format($jmlHrg,2,'.',',')."\n";
                        // $string_qr.="TGL STRIKE OFF = ".date('d M Y', strtotime($row->d_strike_off))."\n";
                        // $string_qr.="JNS KAIN = ".$row->e_kain."\n";
                        $totalBerat+= $row->n_qty_kg;
                    }
                    $string_qr.="TOTAL BERAT (Kg) = ".$totalBerat."\n";
                } else {
                    $string_qr.="Maaf Data Uraian Pekerjaan Tidak Ada (Terjadi Kesalahan)!\n";
                }                
                // $string_qr.="TOTAL HRG = ".number_format($item->v_pekerjaan,2,',','.')."\n";
                // if($item->f_pkp){
                //     $ppn = $item->v_pekerjaan*0.1;
                //     $string_qr.="PPN = ".number_format($ppn,2,',','.')."\n";
                // } else {
                //     $string_qr.="PPN = 0\n";
                // } 
                // $string_qr.="HRG PEK = ".number_format($item->v_pekerjaan_plus_ppn)."\n";           
                // $string_qr.="DOWN PAYMENT = ".$item->n_discount."\n";           
                // $string_qr.="HRG PEK = ".number_format($item->v_discount)."\n";
                // $string_qr.="SISA PEMBAYARAN =".number_format($item->v_sisa)."\n";           
                // $string_qr.="D.\n";
                // $string_qr.="STRIKE OFF = ".date('d M Y', strtotime($item->d_strike_off))."\n";
                // $string_qr.="APPROVAL STRIKE OFF = ".date('d M Y', strtotime($item->d_approval_strike_off))."\n";
                $string_qr.="PENYERAHAN BRG = ".date('d M Y', strtotime($item->d_penyerahan_brg))."\n";
                // $string_qr.="E.\n";                
                $string_qr.="KET KHUSUS = ".$item->e_keterangan_kirim."\n";
            }
        } else {
            $string_qr.="DATA SO TIDAK ADA";
        }
        return $string_qr;
    }

    public function download($id)
    {        
        $param['datahead'] = \DB::select("SELECT mst_so.*, rp.*, rp2.e_nama_pel as penyedia, rjp.e_jns_printing,    rkk.e_kondisi_kain FROM mst_so 
                    LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
                    LEFT JOIN ref_pelanggan rp2 ON mst_so.i_penyedia = rp2.i_pel  
                    LEFT JOIN ref_jns_printing rjp ON cast(mst_so.e_jenis_printing as integer) = rjp.i_id
                    LEFT JOIN ref_kondisi_kain rkk ON cast(mst_so.e_kondisi_kain as integer) = rkk.i_id                   
                    WHERE mst_so.i_id=".$id."");        
        $param['dataitem'] = \DB::select("SELECT a.*, b.e_kain FROM mst_so_item a
                    LEFT JOIN ref_kain b ON cast(a.e_jenis_kain as integer)=b.i_id
                    WHERE a.i_id_so=".$id." ORDER BY i_id");
        
        $qrcode = $this->qrCodeSo($param);
        $nomor_so = str_replace('/','-',$param['datahead'][0]->i_no_so);
        $file = "public/images/qrcode/QRCodeSO-".$nomor_so.".png";
        
        \QRCode::text($qrcode)
            ->setErrorCorrectionLevel('H')
            ->setSize(3)
            ->setMargin(1)
            ->setOutfile($file)
            ->png();

        header("Content-disposition: attachment; filename=QRCodeSO-".$nomor_so.".png");
        header("Content-Type: image/png");
        readfile($file);

        ignore_user_abort(true);
        unlink($file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        \DB::beginTransaction();
        try{
            $sales_order = SalesOrderModel::find($request->input('id_so'));
            $sales_order->i_no_po = $request->input('no_po');
            $sales_order->d_so = date('Y-m-d', strtotime($request->input('d_so')));
            $sales_order->i_pel = $request->input('pel');
            $sales_order->f_repeat = $request->input('repeat_order')=='new'?false:true;
            $sales_order->i_desain = $request->input('desain_kode1')==''?$request->input('desain_kode2'):$request->input('desain_kode1');
            $sales_order->n_qty_warna = $request->input('jml_warna');
            $sales_order->e_motif = $request->input('motif_nama');
            $sales_order->e_jenis_printing = $request->input('jns_printing');
            $sales_order->n_color_way = $request->input('color_way');
            $sales_order->e_color_way = $request->input('color_way_desc');
            $sales_order->e_toleransi_cacat = $request->input('toleransi_cacat_kain');
            // $sales_order->i_jns_kain = $request->input('jenis_kain');
            $sales_order->e_kondisi_kain = $request->input('kondisi_kain');
            $sales_order->e_lebar = $request->input('lebar');
            $sales_order->e_gramasi = $request->input('gramasi');
            $sales_order->i_penyedia = $request->input('penyedia_kain');
            // masih error kalau tgl 30 & 31
            $sales_order->d_strike_off = date('Y-m-d', strtotime($request->input('tgl_strike_off')));
            // masih error kalau tgl 30 & 31 strtotime('+7 hour',strtotime($data->created_at))
            $sales_order->d_approval_strike_off = date('Y-m-d', strtotime($request->input('approval_strike_off')));
            // masih error kalau tgl 30 & 31
            $sales_order->d_penyerahan_brg = date('Y-m-d', strtotime($request->input('tgl_penyerahan')));
            $sales_order->e_keterangan_kirim = $request->input('ket_penyerahan');
            $sales_order->i_status = 3;
            $sales_order->v_pekerjaan = str_replace(',', '', $request->input('total_harga'));
            $sales_order->v_pekerjaan_plus_ppn = str_replace(',','',$request->input('hrg_pekerjaan'));
            $sales_order->v_discount = str_replace(',','',$request->input('down_payment_nilai'));
            $sales_order->n_discount = str_replace(',','',$request->input('down_payment_percent'));
            $sales_order->v_sisa = str_replace(',','',$request->input('sisa_pembayaran'));            
            $sales_order->hitung_by = $request->input('hitung_by_real');
            $sales_order->n_ppn = $request->input('ppn_percent');
            $sales_order->v_ppn = str_replace(',','',$request->input('ppn'));
            $sales_order->exclude_include = $request->input('ppn_percent')>0?'E':'I';
            $sales_order->cara_bayar = $request->input('cara_bayar');
            $sales_order->save();

            $res = SalesOrderDetailModel::where('i_id_so',$request->input('id_so'))->delete();

            $totRow = (int)$request->input('totrow');
            for($i=1; $i<=$totRow; $i++){
                $so_detail = new SalesOrderDetailModel();
                $so_detail->i_id_so = $request->input('id_so');
                $so_detail->e_uraian_pekerjaan = $request->input('uraian_pek_'.$i);
                $so_detail->n_qty_roll = str_replace(',','',$request->input('roll_'.$i));
                $so_detail->n_qty_pjg = str_replace(',','',$request->input('yard_m_'.$i));
                $so_detail->n_qty_kg = str_replace(',','',$request->input('kg_'.$i));
                $so_detail->v_harga_sat = str_replace(',','',$request->input('hrg_'.$i));
                $so_detail->n_qty_kg_sisa = str_replace(',','',$request->input('kg_'.$i));
                $so_detail->d_strike_off = date('Y-m-d', strtotime($request->input('strike_off_'.$i)));
                $so_detail->e_jenis_kain = $request->input('i_jns_kain_'.$i);
                $so_detail->save();
            }   

            \DB::commit();
            return $this->sendResponse('1','Update Sales Order No '.$request->input('no_so').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan Input Sales Order Gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list_approved()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.list_approved',compact('reqAjax'));
    }   

    public function data_approved()
    {
        $data = \DB::select("SELECT mst_so.i_id, i_no_so, d_so, mst_so.i_pel, rp.e_nama_pel, f_repeat, d_strike_off, d_approval_strike_off,
            d_penyerahan_brg, d_approved, i_status, rw.definition, mst_so.created_at 
        FROM mst_so 
        LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
        LEFT JOIN ref_workflow rw ON mst_so.i_status = rw.i_id
        WHERE mst_so.i_status=1
        order by i_no_so desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" href="#" onclick="lihat('.$data->i_id.',\'list_approved.sales_order\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="approved('.$data->i_id.')"><i class="mdi mdi-check-circle-outline"></i> Approved SO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="reject('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Reject SO</a>
                </div>
            </div>';
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat == 1) {
                return 'Repeat';
            } else {
                return 'Tidak';
            }
        }) 
        ->editColumn('created_at',function($data) {
           return date('d-m-Y H:i:s',strtotime($data->created_at));
        })
        ->editColumn('d_so',function($data) {
            return date('d-m-Y',strtotime($data->d_so));
        })
        ->editColumn('d_penyerahan_brg',function($data) {
            return date('d-m-Y',strtotime($data->d_penyerahan_brg));
        })
        ->make(true);
    }

    public function approved($id)
    {
        $sales_order = SalesOrderModel::find($id);
        $sales_order->e_reject = null;
        $sales_order->i_status = 4;
        $sales_order->d_approved = date('Y-m-d H:i:s');
        $sales_order->save();

        return $this->sendResponse('1', 'Approve SO Berhasil!');
    }

    public function reject(Request $request, $id)
    {        
        $sales_order = SalesOrderModel::find($id);
        $sales_order->e_reject = $request->input('input_val');
        $sales_order->i_status = 5;
        $sales_order->d_approved = date('Y-m-d H:i:s');
        $sales_order->save();

        return $this->sendResponse('1', 'Reject SO Berhasil!');
    }

    public function sales_plan(Request $request)
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.sales_plan.index',compact('reqAjax'));
    }

    public function data_sales_plan()
    {
        $data = \DB::select("SELECT * FROM mst_sales_plan WHERE tahun>='".date('Y')."' ORDER BY tahun, i_id asc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<a class="btn btn-sm btn-info" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>';
        })
        ->editColumn('n_qty', function($data){
            return number_format($data->n_qty);
        })
        ->editColumn('bulan', function($data) {
            return $this->getNamaBulan($data->bulan);
        })
        ->make(true);
    }

    public function form_sales_plan(Request $request)
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.sales_plan.form',compact('reqAjax'));
    }

    public function get_data_sales_plan($tahun)
    {
        $data = \DB::select("SELECT * FROM mst_sales_plan WHERE tahun='".$tahun."' ORDER BY i_id asc");
        return json_encode($data);
    }

    public function save_sales_plan(Request $request)
    {
        \DB::beginTransaction();
        try{
            \DB::table('mst_sales_plan')->where('tahun',$request->input('tahun'))->delete();
            for($i=1; $i<=$request->input('tot_bulan');$i++){
                if($request->input('qty'.$i)){
                    \DB::table('mst_sales_plan')
                        ->insert([
                            'n_qty' => str_replace(',','',$request->input('qty'.$i)),
                            'bulan' => $request->input('bulan'.$i),
                            'tahun' => $request->input('tahun')
                        ]);
                }
            }

            \DB::commit();
            return $this->sendResponse('1','Input Sales Plan Berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan Input Sales Plan Gagal');
        }
    }

    function getNamaBulan($bln)
    {
        switch ($bln){
            case 1: 
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }   

    public function so_outsourcing()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('sales_order.so_outs.index',compact('reqAjax'));
    }

    public function data_so_outs()
    {
        $data = \DB::select("SELECT mst_so.i_id, i_no_so, d_so, mst_so.i_pel, rp.e_nama_pel, f_repeat, d_strike_off, d_approval_strike_off,
            d_penyerahan_brg, d_approved, i_status, rw.definition, mst_so.created_at, e_reject 
        FROM mst_so 
        LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
        LEFT JOIN ref_workflow rw ON mst_so.i_status = rw.i_id
        WHERE flag_so > 3
        order by i_no_so desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            // if($data->i_status==0 || $data->i_status==5 || $data->i_status==3){
            //     if(Auth::user()->role==99 || Auth::user()->role==2) {
            //         return '<div class="dropdown">
            //             <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
            //             <i class="mdi mdi-apps"></i>
            //             </button>
            //             <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
            //                 <div class="dropdown-divider"></div>
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-lead-pencil"></i> Edit SO</a>
            //                 <div class="dropdown-divider"></div>                        
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="kirimApprove('.$data->i_id.')"><i class="mdi mdi-file-send"></i> Kirim Approve</a>
            //             </div>
            //         </div>';
            //     } else {
            //         return '<div class="dropdown">
            //             <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
            //             <i class="mdi mdi-apps"></i>
            //             </button>
            //             <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
            //             </div>
            //         </div>';
            //     }
            // } else if($data->i_status==1){
            //     return '<div class="dropdown">
            //         <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
            //         <i class="mdi mdi-apps"></i>
            //         </button>
            //         <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
            //             <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
            //         </div>
            //     </div>';
            // } else if($data->i_status==2){
            //     return '<div class="dropdown">
            //         <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
            //         <i class="mdi mdi-apps"></i>
            //         </button>
            //         <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
            //             <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
            //             <div class="dropdown-divider"></div>
            //             <a class="dropdown-item" style="cursor: pointer;" onclick="printSO('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-printer"></i> print SO</a>
            //             <div class="dropdown-divider"></div>
            //             <a class="dropdown-item" style="cursor: pointer;" onclick="kirimRfp('.$data->i_id.')"><i class="mdi mdi-file-send"></i> Kirim ke RFP</a>
            //         </div>
            //     </div>';
            // } else {
            //     if(Auth::user()->role==99 || Auth::user()->role==2) {
            //         return '<div class="dropdown">
            //             <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
            //             <i class="mdi mdi-apps"></i>
            //             </button>
            //             <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
            //                 <div class="dropdown-divider"></div>
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="printSO('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-printer"></i> print SO</a>
            //                 <div class="dropdown-divider"></div>
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="downloadSO('.$data->i_id.')"><i class="mdi mdi-download"></i> download QRCode SO</a>
            //             </div>
            //         </div>';
            //     } else if(Auth::user()->role==99 || Auth::user()->role==1){
            //         return '<div class="dropdown">
            //             <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
            //             <i class="mdi mdi-apps"></i>
            //             </button>
            //             <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
            //                 <div class="dropdown-divider"></div>
            //                 <a class="dropdown-item" style="cursor: pointer;" onclick="downloadSO('.$data->i_id.')"><i class="mdi mdi-download"></i> download QRCode SO</a>
            //             </div>
            //         </div>';
            //     } else {
            //         return '<a class="btn btn-sm btn-info" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'sales_order.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>';
            //     }
            // }
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat == 1) {
                return 'Repeat';
            } else {
                return 'Tidak';
            }
        }) 
        ->editColumn('created_at',function($data) {
            return date('d-m-Y H:i:s',strtotime($data->created_at));
        })
        ->editColumn('d_so',function($data) {
            return date('d-m-Y',strtotime($data->d_so));
        })
        ->editColumn('d_approved',function($data) {
            return date('d-m-Y H:i:s',strtotime($data->d_approved));
        })
        ->make(true);
    }
}
