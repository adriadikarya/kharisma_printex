<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Request as Req;
use DataTables;
use Hash;
use Crypt;

class ReferensiController extends Controller
{
    public function ket_umum(Request $request)
    {
        $data = \DB::select("SELECT a.i_id as id_head, a.e_ket_umum, b.i_id_ket_umum, count(b.i_id_ket_umum) as total_child FROM ref_ket_umum_so a LEFT JOIN ref_ket_umum_so_child b ON a.i_id = b.i_id_ket_umum GROUP BY a.i_id, b.i_id_ket_umum ORDER BY a.i_id");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.ket_umum.index',compact('reqAjax','data'));
    }

    public function form_ket_umum(Request $request)
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.ket_umum.form',compact('reqAjax'));
    }

    public function child($id)
    {
        $data = \DB::select("SELECT * FROM ref_ket_umum_so_child WHERE i_id_ket_umum='".$id."'");
        return json_encode($data);
    }

    public function edit_ket_umum($id)
    {
        $data = \DB::select("SELECT i_id, e_ket_umum FROM ref_ket_umum_so WHERE i_id='".$id."'");
        if($data) {
            $param['head'] = trim($data[0]->e_ket_umum);
            $param['headid'] = $data[0]->i_id;
        } else {
            $param['head'] = '';
            $param['headid'] = '';
        }
        $param['det'] = \DB::select("SELECT * FROM ref_ket_umum_so_child WHERE i_id_ket_umum='".$id."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('referensi.ket_umum.form_edit',compact('reqAjax','param'));
    }

    public function save_ket_umum(Request $request)
    {
        \DB::beginTransaction();
        try{
            $qId = \DB::select("SELECT i_id FROM ref_ket_umum_so ORDER BY i_id DESC LIMIT 1");
            if(empty($qId)){
                $id_ket = 1;
            } else {
                $id_ket = $qId[0]->i_id+1;
            } 

            \DB::table('ref_ket_umum_so')->insert(['i_id' => $id_ket,'e_ket_umum' => $request->input('ket_umum')]);
            if($request->input('totrow')>0){
                for($i=1; $i<=$request->input('totrow'); $i++){
                    \DB::table('ref_ket_umum_so_child')
                        ->insert(['i_id_ket_umum' => $id_ket, 'e_child_ket_umum' => $request->input('child'.$i)]);
                }
            }

            \DB::commit();
            return $this->sendResponse('1','Save General Requirements Success','Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Generel Requirements  Fail');
        }
    }

    public function update_ket_umum(Request $request)
    {
        \DB::beginTransaction();
        try{            
            \DB::table('ref_ket_umum_so')
                ->where('i_id', $request->input('idhead'))
                ->update(['e_ket_umum' => $request->input('ket_umum')]);
            
            \DB::table('ref_ket_umum_so_child')->where('i_id_ket_umum', $request->input('idhead'))->delete();
            if($request->input('totrow')>0){                
                for($i=1; $i<=$request->input('totrow'); $i++){
                    \DB::table('ref_ket_umum_so_child')
                        ->insert(['i_id_ket_umum' => $request->input('idhead'), 'e_child_ket_umum' => $request->input('child'.$i)]);
                }
            }

            \DB::commit();
            return $this->sendResponse('1','Update General Requirements Success','Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Update Generel Requirements  Fail');
        }
    }

    public function print_logo(Request $request)
    {
        $data = \DB::select("SELECT * FROM ref_logo ORDER BY f_active");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.print_logo.index',compact('reqAjax','data'));
    }

    public function form_print_logo()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.print_logo.form',compact('reqAjax'));
    }

    public function save_print_logo(Request $request)
    {
        \DB::beginTransaction();
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            \DB::table('ref_logo')
                ->update(['f_active' => false]);

            // nama file
            // $file->getClientOriginalName();	
            $nama_file = date('Ymdhisa')."_".$file->getClientOriginalName();            

            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'public/images/logo_so';
    
            // upload file
            $file->move($tujuan_upload,$nama_file);

            \DB::table('ref_logo')->insert([
                'logo_name' => $nama_file,
                'f_active' => true,
                'created_at' => date('Y-m-d h:i:s')
            ]);
            \DB::commit();
            return $this->sendResponse('1','Save Print Logo','Success');
        } else {
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Print Logo Fail');
        }
    }

    public function aktif_print_logo(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::table('ref_logo')
                ->update(['f_active' => false]);

            \DB::table('ref_logo')
                ->where('i_id', $request->input('id'))
                ->update(['f_active' => true]);

            \DB::commit();
            return $this->sendResponse('1','Active Print Logo Success','Success');
        } catch(Exception $e) {
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Active Print Logo Fail');            
        }
    }

    public function non_aktif_print_logo(Request $request)
    {
        \DB::beginTransaction();
        try {            

            \DB::table('ref_logo')
                ->where('i_id', $request->input('id'))
                ->update(['f_active' => false]);
                
            \DB::commit();
            return $this->sendResponse('1','Non Active Print Logo Success','Success');
        } catch(Exception $e) {
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Non Active Print Logo Fail');
        }
    }

    public function user()
    {
        $role = \DB::select("SELECT * FROM ref_role");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";                        
        return view('referensi.user.index',compact('reqAjax','role'));
    }

    public function data_user()
    {
        $data = \DB::select("SELECT a.*, b.e_role_name FROM users a LEFT JOIN ref_role b ON a.role=b.i_id ORDER BY id DESC");        
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            if($data->login_pertama){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                        
                        <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->id.')"><i class="mdi mdi-pencil"></i> Edit</a>
                    </div>
                </div>';
            } else if($data->count_reset_password<3) {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="reset('.$data->id.')"><i class="mdi mdi-clipboard-check"></i> Reset Password</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->id.')"><i class="mdi mdi-pencil"></i> Edit</a>
                    </div>
                </div>';
            } else {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                        
                        <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->id.')"><i class="mdi mdi-pencil"></i> Edit</a>
                    </div>
                </div>'; 
            }            
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })
        ->editColumn('login_pertama', function($data) {
            if($data->login_pertama) {
                return 'Ya';
            } else {
                return 'Tidak';
            }
        })         
        ->editColumn('is_active', function($data) {
            if($data->is_active) {
                return 'Ya';
            } else {
                return 'Tidak';
            }
        }) 
        ->make(true);
    }

    public function simpan_user(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_user')){
                \DB::table('users')
                    ->where('id', $request->input('id_user'))
                    ->update([
                        'name' => $request->input('name'),
                        'username' => $request->input('username'),
                        'role' => $request->input('role'),
                        'is_active' => $request->input('active')                
                    ]);
                $text = 'Update';
            } else {
                $checkUser = \DB::select("SELECT * FROM users WHERE upper(username)='".strtoupper($request->input('username'))."'");
                if($checkUser){
                    return $this->sendResponse('2','Username Exists');
                } else {
                    \DB::table('users')
                        ->insert([
                            'name' => $request->input('name'),
                            'username' => $request->input('username'),
                            'role' => $request->input('role'),
                            'password' => Hash::make('welcome1'),
                            'login_pertama' => true,
                            'is_active' => $request->input('active'),
                            'count_reset_password' => 0 
                        ]);
                    $text = 'Save';
                }                
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' User Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save User Fail');
        }
    }

    public function edit_user($id)
    {
        $data = \DB::select("SELECT * FROM users WHERE id='".$id."'");
        return json_encode($data);
    }

    public function reset_password($id)
    {
        \DB::beginTransaction();
        $count_reset = \DB::select("SELECT count_reset_password FROM users WHERE id='".$id."'");
        if(empty($count_reset)){
            $count = 1;
        } else {
            $count = $count_reset[0]->count_reset_password+1;
        }

        if($count==1){
            $status = 'Peringatan Ke 1';
            $login_pertama = true;
            $active = true;
        } else if($count==2){
            $status = 'Peringatan Ke 2';
            $login_pertama = true;
            $active = true;
        } else {
            $status = 'Mendapatkan SP1 dan Username Di Non-Aktifkan';
            $login_pertama = false;
            $active = false;
        }
        try {            
            \DB::table('users')
                ->where('id', $id)
                ->update([
                    'password' => Hash::make('welcome1'),
                    'login_pertama' => $login_pertama,
                    'is_active' => $active,                
                    'count_reset_password' => $count,
                    'status' => $status
                ]);
            $text = 'Reset Password';            
            \DB::commit();
            return $this->sendResponse('1',$text.' User Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save User Fail');
        }
    }

    public function pelanggan()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.pelanggan.index',compact('reqAjax'));
    }

    public function data_pelanggan()
    {
        $data = \DB::select("SELECT * FROM ref_pelanggan WHERE i_pel not in ('0','1') ORDER BY i_pel DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })
        ->editColumn('f_pkp', function($data) {
            if($data->f_pkp == 1 || $data->f_pkp == true || $data->f_pkp == 't') {
                return 'PKP';
            } else {
                return 'NON PKP';
            }
        }) 
        ->make(true);
    }

    public function simpan_pelanggan(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_pel')){
                \DB::table('ref_pelanggan')
                    ->where('i_pel', $request->input('id_pel'))
                    ->update([
                        'e_nama_pel' => $request->input('nama_pel'),
                        'e_alamat_pel' => $request->input('alamat'),
                        'f_pkp' => $request->input('pkp'),
                        'e_npwp_pel' => $request->input('npwp'),
                        'e_telp_pel' => $request->input('telp'),
                        'e_kont_pel' => $request->input('contact'),
                        'created_at' => date('Y-m-d h:i:s'),
                        'e_fax_pel' => $request->input('fax'),
                        'e_kota_pel' => $request->input('city'),
                        'e_kode_marketing' => $request->input('kode_marketing'),
                        'n_jth_tempo' => $request->input('jth_tempo')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_pelanggan')
                    ->insert([
                        'e_nama_pel' => $request->input('nama_pel'),
                        'e_alamat_pel' => $request->input('alamat'),
                        'f_pkp' => $request->input('pkp'),
                        'e_npwp_pel' => $request->input('npwp'),
                        'e_telp_pel' => $request->input('telp'),
                        'e_kont_pel' => $request->input('contact'),
                        'created_at' => date('Y-m-d h:i:s'),
                        'e_fax_pel' => $request->input('fax'),
                        'e_kota_pel' => $request->input('city'),
                        'e_kode_marketing' => $request->input('kode_marketing'),
                        'n_jth_tempo' => $request->input('jth_tempo')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Customer Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Customer Fail');
        }
    }

    public function edit_pelanggan($id)
    {
        $data = \DB::select("SELECT * FROM ref_pelanggan WHERE i_pel='".$id."'");
        return json_encode($data);
    }

    public function bagian()
    {                
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";  
        $user = \DB::table('users')->whereNotIn('role',[99,0])->get();      
        return view('referensi.bagian.index',compact('reqAjax','user'));
    }

    public function data_bagian()
    {
        $data = \DB::select("SELECT rb.*, us.name, us2.name as name2 FROM ref_bagian rb 
        LEFT JOIN users us ON us.id=rb.e_penanggung_jawab 
        LEFT JOIN users us2 ON us2.id=rb.e_penanggung_jawab2
        ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="makeLast('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Make Last</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })      
        ->editColumn('last_workstation', function($data) {
            if($data->last_workstation) {
                return 'Last';
            } else {
                return '';
            }
        })
       /*  ->editColumn('wajib_isi', function($data) {
            if($data->wajib_isi) {
                return 'Ya';
            } else {
                return 'Tidak';
            }
        })   */ 
        ->make(true);
    }

    public function simpan_bagian(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_bagian')){
                \DB::table('ref_bagian')
                    ->where('i_id', $request->input('id_bagian'))
                    ->update([
                        'nama_bagian' => $request->input('nama_bagian'),
                        'e_penanggung_jawab' => $request->input('penanggung_jawab'),                            
                        'e_penanggung_jawab2' => $request->input('penanggung_jawab2'),                            
                        // 'wajib_isi' => $request->input('wajib_isi')=='t'?true:false,                            
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_bagian')
                    ->insert([
                        'nama_bagian' => $request->input('nama_bagian'),
                        'e_penanggung_jawab' => $request->input('penanggung_jawab'),
                        'e_penanggung_jawab2' => $request->input('penanggung_jawab2'),
                        // 'wajib_isi' => $request->input('wajib_isi')=='t'?true:false,   
                        'last_workstation' => false, 
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' WorkStation Reference Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save WorkStation Reference Fail');
        }
    }

    public function edit_bagian($id)
    {
        $data = \DB::select("SELECT * FROM ref_bagian WHERE i_id='".$id."'");        
        return json_encode($data);
    }

    public function hapus_bagian($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_bagian')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete WorkStation Reference Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'Data sedang dipakai, tidak dapat menghapus data tersebut';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function make_last_bagian($id)
    {
        \DB::beginTransaction();
        try {
            \DB::table('ref_bagian')->update(['last_workstation' => false]);
            \DB::table('ref_bagian')->where('i_id',$id)->update(['last_workstation' => true]);
            \DB::commit();
            return $this->sendResponse('1','Make Last Workstation Success');
        } catch (\Exception $e) {
            \DB::rollBack();
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }
    }

    public function jns_bahan()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.jns_bahan.index',compact('reqAjax'));
    }

    public function data_jns_bahan()
    {
        $data = \DB::select("SELECT * FROM ref_jns_bahan ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_jns_bahan(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_jns_bahan')){
                \DB::table('ref_jns_bahan')
                    ->where('i_id', $request->input('id_jns_bahan'))
                    ->update([
                        'e_jns_bahan' => $request->input('nama_jns_bahan'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_jns_bahan')
                    ->insert([
                        'e_jns_bahan' => $request->input('nama_jns_bahan'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Jenis Bahan Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Jenis Bahan Fail');
        }
    }

    public function edit_jns_bahan($id)
    {
        $data = \DB::select("SELECT * FROM ref_jns_bahan WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_jns_bahan($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_jns_bahan')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Jenis Bahan Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function jns_printing()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.jns_printing.index',compact('reqAjax'));
    }

    public function data_jns_printing()
    {
        $data = \DB::select("SELECT * FROM ref_jns_printing ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_jns_printing(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_jns_printing')){
                \DB::table('ref_jns_printing')
                    ->where('i_id', $request->input('id_jns_printing'))
                    ->update([
                        'e_jns_printing' => $request->input('nama_jns_printing'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_jns_printing')
                    ->insert([
                        'e_jns_printing' => $request->input('nama_jns_printing'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Jenis Printing Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Jenis Printing Fail');
        }
    }

    public function edit_jns_printing($id)
    {
        $data = \DB::select("SELECT * FROM ref_jns_printing WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_jns_printing($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_jns_printing')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Jenis Printing Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function kain()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.kain.index',compact('reqAjax'));
    }

    public function data_kain()
    {
        $data = \DB::select("SELECT * FROM ref_kain ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_kain(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_kain')){
                \DB::table('ref_kain')
                    ->where('i_id', $request->input('id_kain'))
                    ->update([
                        'e_kain' => $request->input('nama_kain'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_kain')
                    ->insert([
                        'e_kain' => $request->input('nama_kain'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Kain Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Kain Fail');
        }
    }

    public function edit_kain($id)
    {
        $data = \DB::select("SELECT * FROM ref_kain WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_kain($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_kain')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Kain Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function kondisi_kain()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.kondisi_kain.index',compact('reqAjax'));
    }

    public function data_kondisi_kain()
    {
        $data = \DB::select("SELECT * FROM ref_kondisi_kain ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_kondisi_kain(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_kondisi_kain')){
                \DB::table('ref_kondisi_kain')
                    ->where('i_id', $request->input('id_kondisi_kain'))
                    ->update([
                        'e_kondisi_kain' => $request->input('nama_kondisi_kain'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_kondisi_kain')
                    ->insert([
                        'e_kondisi_kain' => $request->input('nama_kondisi_kain'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Kondisi Kain Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Kondisi Kain Fail');
        }
    }

    public function edit_kondisi_kain($id)
    {
        $data = \DB::select("SELECT * FROM ref_kondisi_kain WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_kondisi_kain($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_kondisi_kain')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Kondisi Kain Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function ori_kondisi()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.ori_kondisi.index',compact('reqAjax'));
    }

    public function data_ori_kondisi()
    {
        $data = \DB::select("SELECT * FROM ref_ori_kondisi ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_ori_kondisi(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_ori_kondisi')){
                \DB::table('ref_ori_kondisi')
                    ->where('i_id', $request->input('id_ori_kondisi'))
                    ->update([
                        'e_ori_kondisi' => $request->input('nama_ori_kondisi'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_ori_kondisi')
                    ->insert([
                        'e_ori_kondisi' => $request->input('nama_ori_kondisi'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Original Condition Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Original Condition Fail');
        }
    }

    public function edit_ori_kondisi($id)
    {
        $data = \DB::select("SELECT * FROM ref_ori_kondisi WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_ori_kondisi($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_ori_kondisi')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Original Condition Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function role()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.role.index',compact('reqAjax'));
    }

    public function data_role()
    {
        $data = \DB::select("SELECT * FROM ref_role ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_role(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_role')){
                \DB::table('ref_role')
                    ->where('i_id', $request->input('id_role'))
                    ->update([
                        'e_role_name' => $request->input('nama_role'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_role')
                    ->insert([
                        'e_role_name' => $request->input('nama_role'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Role Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Role Fail');
        }
    }

    public function edit_role($id)
    {
        $data = \DB::select("SELECT * FROM ref_role WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_role($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_role')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Role Success');
            \DB::commit();
        }catch (QueryException $e){    
            \DB::rollback();        
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            \DB::rollback(); 
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function so_flag()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.so_flag.index',compact('reqAjax'));
    }

    public function data_so_flag()
    {
        $data = \DB::select("SELECT * FROM ref_so_flag ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_so_flag(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_so_flag')){
                \DB::table('ref_so_flag')
                    ->where('i_id', $request->input('id_so_flag'))
                    ->update([
                        'flag_name' => $request->input('nama_so_flag'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_so_flag')
                    ->insert([
                        'flag_name' => $request->input('nama_so_flag'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' SO FLAG Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save SO FLAG Fail');
        }
    }

    public function edit_so_flag($id)
    {
        $data = \DB::select("SELECT * FROM ref_so_flag WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_so_flag($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_so_flag')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete SO FLAG Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function tekstur_akhir()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.tekstur_akhir.index',compact('reqAjax'));
    }

    public function data_tekstur_akhir()
    {
        $data = \DB::select("SELECT * FROM ref_tekstur_akhir ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_tekstur_akhir(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_tekstur_akhir')){
                \DB::table('ref_tekstur_akhir')
                    ->where('i_id', $request->input('id_tekstur_akhir'))
                    ->update([
                        'e_tekstur' => $request->input('nama_tekstur_akhir'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_tekstur_akhir')
                    ->insert([
                        'e_tekstur' => $request->input('nama_tekstur_akhir'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Tekstur Akhir Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Tekstur Akhir Fail');
        }
    }

    public function edit_tekstur_akhir($id)
    {
        $data = \DB::select("SELECT * FROM ref_tekstur_akhir WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_tekstur_akhir($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_tekstur_akhir')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Tekstur Akhir Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function warna_dasar()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.warna_dasar.index',compact('reqAjax'));
    }

    public function data_warna_dasar()
    {
        $data = \DB::select("SELECT * FROM ref_warna_dasar ORDER BY i_id DESC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';
            // belum nemu cara foreign key
            // <div class="dropdown-divider"></div>
            // <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->i_pel.')"><i class="mdi mdi-clipboard-check"></i> Delete</a>
        })         
        ->make(true);
    }

    public function simpan_warna_dasar(Request $request)
    {
        \DB::beginTransaction();
        try {
            if($request->input('id_warna_dasar')){
                \DB::table('ref_warna_dasar')
                    ->where('i_id', $request->input('id_warna_dasar'))
                    ->update([
                        'e_warna_dasar' => $request->input('nama_warna_dasar'),                        
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Update';
            } else {
                \DB::table('ref_warna_dasar')
                    ->insert([
                        'e_warna_dasar' => $request->input('nama_warna_dasar'),                        
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                $text = 'Save';
            }
            \DB::commit();
            return $this->sendResponse('1',$text.' Warna Dasar Success');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','there is an error, Save Warna Dasar Fail');
        }
    }

    public function edit_warna_dasar($id)
    {
        $data = \DB::select("SELECT * FROM ref_warna_dasar WHERE i_id='".$id."'");
        return json_encode($data);
    }

    public function hapus_warna_dasar($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('ref_warna_dasar')->where('i_id',$id)->delete();
            return $this->sendResponse('1','Delete Warna Dasar Success');
        }catch (QueryException $e){            
            $errorCode = $e->errorInfo[0];
            if ($errorCode == "23503") {
                $rm = 'This Data has been used, Can`t run delete data';
                return $this->sendResponse('2',$rm);
            }else {
                return $this->sendResponse('2',$e->errorInfo[0]);                
            }
        }catch (CustomException $e){
            return $this->sendResponse('2',$e->getMessage());            
        }
    }

    public function hak_akses()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.hak_akses.index',compact('reqAjax'));
    }

    public function data_hak_akses()
    {
        $data = \DB::select("SELECT a.role_id, b.e_role_name FROM mst_menu_role a JOIN ref_role b ON a.role_id=b.i_id WHERE a.role_id NOT IN (99) GROUP BY a.role_id, b.e_role_name ORDER BY a.role_id ASC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" style="cursor: pointer;" onclick="edit('.$data->role_id.')"><i class="mdi mdi-clipboard-check"></i> Edit</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="hapus('.$data->role_id.')"><i class="mdi mdi-close-circle-outline"></i> Hapus</a>
                </div>
            </div>';            
        })         
        ->make(true);
    }

    public function tambah_hak_akses()
    {
        $menu_head = [];
        $q_menu_head = \DB::select("SELECT * FROM ref_menu_head ORDER BY i_id");
        if($q_menu_head){
            foreach($q_menu_head as $key => $mn_head){
                if($mn_head->stand_alone==true){                    
                    $headitem = array(
                        'head' => $mn_head,
                        'item' => []
                    );
                    array_push($menu_head, $headitem);                        
                } else {
                    $q_menu_item = \DB::select("SELECT * FROM ref_menu_item WHERE id_menu_head = '".$mn_head->i_id."' ORDER BY i_id");
                    $menu_item = [];
                    if($q_menu_item){
                        foreach($q_menu_item as $mn_item){                          
                            array_push($menu_item, $mn_item);                                              
                        }
                        $headitem = array(
                            'head' => $mn_head,
                            'item' => $menu_item,
                        );                        
                        array_push($menu_head, $headitem);
                    } else {
                        $param['data'] = false;            
                    }                        
                }                
            }
            $param['data'] = $menu_head;
        } else {
            $param['data'] = false;
        }                
        $param['role'] = \DB::select("SELECT * FROM ref_role WHERE i_id not in (select role_id from mst_menu_role GROUP BY role_id) ORDER BY i_id");        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.hak_akses.form_tambah',compact('reqAjax','param'));
    }

    public function simpan_hak_akses(Request $request)
    {        
        \DB::beginTransaction();
        try {
            for($i=1; $i<=$request->input('totalhead');$i++){
                for($j=1; $j<=$request->input('tothead'.$i);$j++){
                    if($request->input('item'.$i.$j)){
                        \DB::table('mst_menu_role')->insert([
                            'role_id' => $request->input('role'),
                            'id_menu_head' => $request->input('head'.$i),
                            'id_menu_item' => $request->input('item'.$i.$j),
                            'created_at' => date('Y-m-d h:i:s')
                        ]);
                    }
                }
            }
            \DB::commit();
            return $this->sendResponse('1','Save Hak Akses Sukses');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','Terjadi Kesalahan, Save Hak Akses Gagal');
        }
    }

    public function hapus_hak_akses(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::table('mst_menu_role')->where('role_id',$request->input('roleid'))->delete();
            \DB::commit();
            return $this->sendResponse('1','Hapus Hak Akses Sukses');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','Terjadi Kesalahan, Hapus Hak Akses Gagal');
        }
    }

    public function edit_hak_akses($roleid)
    {
        $menu_head = [];
        $q_menu_head = \DB::select("SELECT * FROM ref_menu_head ORDER BY i_id");
        if($q_menu_head){
            foreach($q_menu_head as $key => $mn_head){
                if($mn_head->stand_alone==true){                    
                    $q_r_m_h = \DB::select("SELECT i_id FROM mst_menu_role WHERE role_id='".$roleid."' AND id_menu_head='".$mn_head->i_id."'");
                    if($q_r_m_h){
                        $headitem = array(
                            'head' => $mn_head,
                            'item' => [],
                            'checked' => 'checked'
                        );
                    } else {
                        $headitem = array(
                            'head' => $mn_head,
                            'item' => [],
                            'checked' => ''
                        );
                    }                    
                    array_push($menu_head, $headitem);                        
                } else {
                    $q_menu_item = \DB::select("SELECT * FROM ref_menu_item WHERE id_menu_head = '".$mn_head->i_id."' ORDER BY i_id");
                    $menu_item = [];                    
                    if($q_menu_item){
                        foreach($q_menu_item as $mn_item){   
                            $q_r_m_i = \DB::select("SELECT i_id FROM mst_menu_role WHERE role_id='".$roleid."' AND id_menu_head='".$mn_head->i_id."' AND id_menu_item='".$mn_item->i_id."'");    
                            if(!empty($q_r_m_i)){
                                // array_push($mn_item, array('checked' => 'checked'));
                                $mn_item = (object) array_merge( (array)$mn_item, array( 'checked' => 'checked' ) );
                            } else {
                                // array_push($mn_item, array('checked' => ''));
                                $mn_item = (object) array_merge( (array)$mn_item, array( 'checked' => '' ) );
                            }                                               
                            array_push($menu_item, $mn_item);                            
                        }                                                
                        $headitem = array(
                            'head' => $mn_head,
                            'item' => $menu_item,
                        );                        
                        array_push($menu_head, $headitem);
                    } else {
                        $param['data'] = false;            
                    }                        
                }                
            }
            $param['data'] = $menu_head;
        } else {
            $param['data'] = false;
        }                        
        $param['roleid'] = $roleid;
        $param['role'] = \DB::select("SELECT * FROM ref_role WHERE i_id='".$roleid."' ORDER BY i_id");        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";        
        return view('referensi.hak_akses.form_edit',compact('reqAjax','param'));
    }

    public function update_hak_akses(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::table('mst_menu_role')->where('role_id', $request->input('role'))->delete();

            for($i=1; $i<=$request->input('totalhead');$i++){
                for($j=1; $j<=$request->input('tothead'.$i);$j++){
                    if($request->input('item'.$i.$j)){
                        \DB::table('mst_menu_role')->insert([
                            'role_id' => $request->input('role'),
                            'id_menu_head' => $request->input('head'.$i),
                            'id_menu_item' => $request->input('item'.$i.$j),
                            'created_at' => date('Y-m-d h:i:s')
                        ]);
                    }
                }
            }
            \DB::commit();
            return $this->sendResponse('1','Update Hak Akses Sukses');
        }catch(Exception $e){
            \DB::rollBack();
            return $this->sendResponse('2','Terjadi Kesalahan, Update Hak Akses Gagal');
        }
    }
}
