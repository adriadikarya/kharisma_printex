<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use DB;
use Hash;
use Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        $identity  = request()->get('identity');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }

    public function authenticate(Request $request)
    {        
        $credentials = $request->only('username', 'password');
        $user = DB::table('users')->where('username', $request->input('identity'))->first();
        if (!is_null($user)) {
            
            if (!Hash::check($request->input('password'), $user->password)){
            // if(md5($request->input('password'))==$user->password){
                return response()->json([
                    'rc' => 0,
                    'rm' => 'Password salah'
                ]);
            }

            if (!$user->is_active){
                return response()->json([
                    'rc' => 1,
                    'rm' => 'Akun anda tidak aktif. Silahkan hubungi admin. Karena Anda Sudah lebih dari 2x reset password Maka Anda Mendapatkan SP1'
                ]);
            }

            if ($user->login_pertama){
                return response()->json([
                    'rc' => 2,
                    'rm' => 'success',
                    'id_user' => $user->id
                ]);
            }

            // Session::put('id', $user->id);
            // Session::put('roleId', $user->user_role_id);
            Auth::loginUsingId($user->id);
            return response()->json([
                'rc' => 3,
                'rm' => 'success'
            ]);
            

        } else {
            // login failed
            return response()->json([
                'rc' => 4,
                'rm' => 'Username salah'
            ]);
        }
    }

    public function change_pass(Request $request)
    {                
        $passHashed = Hash::make($request->input('new_pass'));
        $idUser = $request->input('id');
        DB::table('users')->where('id','=', $idUser)->update([
            'password' => $passHashed,
            'login_pertama' => false
        ]);

        return response()->json([
            'rc' => 0,
            'rm' => 'Password berhasil diubah, Silahkan Login!'
        ]);
    }

    /**
     * Validate the user login.
     * @param Request $request
     */
    protected function validateLogin(Request $request)
    {
        $this->validate(
            $request,
            [
                'identity' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'identity.required' => 'Username is required',
                'password.required' => 'Password is required',
            ]
        );
    }
    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
        );
    }
}
