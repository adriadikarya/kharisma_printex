<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use App\MyFunc;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExportExcelController extends Controller
{
    public function so(Request $request)
    {
        if($request->has('excel')){
            $d_awal=date('Y-m-d', strtotime($request->get('d_awal')));
            $d_akhir=date('Y-m-d', strtotime($request->get('d_akhir')));
            $fields=[];
            $query1= \DB::select("SELECT mst_so.*, 
                rp.e_nama_pel, 
                rp.f_pkp, 
                rp2.e_nama_pel as penyedia, 
                rk.e_kain, 
                rf.flag_name, 
                rjp.e_jns_printing,
                sum(msi.n_qty_pjg) as qty_pjg,
                sum(msi.n_qty_kg) as qty_kg,
                sum(msi.n_qty_roll) as qty_roll
                FROM mst_so 
                JOIN mst_so_item msi ON msi.i_id_so=mst_so.i_id
                LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
                LEFT JOIN ref_pelanggan rp2 ON mst_so.i_penyedia = rp2.i_pel
                LEFT JOIN ref_so_flag rf ON mst_so.flag_so = rf.i_id
                LEFT JOIN ref_kain rk ON mst_so.i_jns_kain = rk.i_id
                LEFT JOIN ref_jns_printing rjp ON mst_so.e_jenis_printing::INT=rjp.i_id
                WHERE to_char(mst_so.created_at,'yyyy-mm-dd')>='".$d_awal."' 
                    AND to_char(mst_so.created_at,'yyyy-mm-dd')<='".$d_akhir."' 
                    AND mst_so.i_status not in ('5','9','12','401')
                GROUP BY mst_so.i_id, 
                rp.e_nama_pel, 
                rp.f_pkp, 
                rp2.e_nama_pel, 
                rk.e_kain, 
                rf.flag_name, 
                rjp.e_jns_printing
                ORDER BY mst_so.i_id"); 

            return MyFunc::printto($query1,'Laporan_SO_'.$d_awal.'_'.$d_akhir, $fields, 0,['excel'=>'export.so.excel_so'],'landscape','a4');
        }
    
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('export.so.form_so',compact('reqAjax'));
    }

    public function rfp(Request $request)
    {
        if($request->has('excel')){
            $d_awal=date('Y-m-d', strtotime($request->get('d_awal')));
            $d_akhir=date('Y-m-d', strtotime($request->get('d_akhir')));
            $fields=[];
            $query1= \DB::select("SELECT rfp.*, rp.e_nama_pel, rp2.e_nama_pel as penyedia, ms.i_no_so, ms.d_approval_strike_off, rta.e_tekstur, rjb.e_jns_bahan, rok1.e_ori_kondisi as ori_kond1, rok2.e_ori_kondisi as ori_kond2, rwd.e_warna_dasar FROM rfp
            LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
            LEFT JOIN ref_pelanggan rp2 ON rfp.i_penyedia=rp2.i_pel
            LEFT JOIN ref_tekstur_akhir rta ON rta.i_id=rfp.e_tekstur_akhir_to::INT
            LEFT JOIN ref_jns_bahan rjb ON rjb.i_id=rfp.e_material::INT
            LEFT JOIN ref_ori_kondisi rok1 ON rok1.i_id=rfp.e_original_cond1::INT
            LEFT JOIN ref_ori_kondisi rok2 ON rok2.i_id=rfp.e_original_cond2::INT
            LEFT JOIN ref_warna_dasar rwd ON rwd.i_id=rfp.e_color::INT
            JOIN mst_so ms ON ms.i_id=rfp.i_id_so
            WHERE to_char(rfp.created_at,'yyyy-mm-dd')>='".$d_awal."' AND to_char(rfp.created_at,'yyyy-mm-dd')<='".$d_akhir."' AND rfp.i_status not in ('5','9','12','401')
            ORDER BY rfp.i_id"); 

            return MyFunc::printto($query1,'Laporan_JO_'.$d_awal.'_'.$d_akhir, $fields, 0,['excel'=>'export.rfp.excel_rfp'],'landscape','a4');
        }
    
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('export.rfp.form_rfp',compact('reqAjax'));
    }

    public function kp(Request $request)
    {
        if($request->has('excel')){
            $d_awal=date('Y-m-d', strtotime($request->get('d_awal')));
            $d_akhir=date('Y-m-d', strtotime($request->get('d_akhir')));
            $fields=[];
            $query1=\DB::select("SELECT mk.*, tw.n_roll as roll, tw.i_id, tw.cw, tw.n_proses as kg, tw.n_meter as meter, ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rp.e_nama_pel, rp2.e_nama_pel as penyedia, rta.e_tekstur, 
                rf.flag_name, ms.d_so, rjp.e_jns_printing, ms.e_toleransi_cacat, ms.d_strike_off, ms.d_approval_strike_off, 
                ms.e_keterangan_kirim as ket_so, rfp.d_selesai, rfp.d_approved_mrk, rfp.d_approved_pro, rfp.f_repeat, rfp.i_desain, rfp.e_motif, 
                rjb.e_jns_bahan, rok1.e_ori_kondisi as ori_kond1, rok2.e_ori_kondisi as ori_kond2, rfp.e_ket_ori_cond, 
                rfp.e_color, rwd.e_warna_dasar, rfp.d_tgl_material_in, rfp.n_qty_material, rfp.e_cw_1, rfp.e_cw_2, 
                rfp.e_cw_3, rfp.e_cw_4, rfp.e_cw_5, rfp.e_cw_6, rfp.e_cw_7, rfp.e_cw_8, rfp.e_gramasi_from, 
                rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to, rfp.e_pakan_from, 
                rfp.e_pakan_to, rfp.e_lusi_to, rfp.e_lusi_from, rfp.e_ket_rfp, tw.d_tgl as tgl_prod, tw2.d_tgl as tgl_finishing, tw3.d_tgl as tgl_delivery
                FROM mst_kartu mk
                LEFT JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mk.i_id
                LEFT JOIN (select x.i_id, x.i_id_rfp, x.i_id_bagian, x.i_cw as cw, x.d_tgl as d_tgl, x.n_proses, x.n_roll, x.n_meter
                from (select i_id, i_id_rfp, i_id_bagian, i_cw as i_cw, d_tgl as d_tgl, n_proses, n_roll, n_meter 
                from tx_workstation order by i_cw) as x
                where x.i_id_bagian=6
                group by x.i_id, x.i_id_rfp, x.i_id_bagian, x.i_cw, x.d_tgl, x.n_proses, x.n_roll, x.n_meter) as tw ON tw.i_id_rfp=mk.i_id_rfp
                LEFT JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
                from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
                from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
                where x.i_id_bagian=8
                group by x.i_id_rfp, x.i_id_bagian) as tw2 ON tw2.i_id_rfp=mk.i_id_rfp
                LEFT JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
                from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
                from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
                where x.i_id_bagian=10
                group by x.i_id_rfp, x.i_id_bagian) as tw3 ON tw3.i_id_rfp=mk.i_id_rfp
                LEFT JOIN mst_so ms ON ms.i_id=mk.i_id_so
                LEFT JOIN ref_so_flag rf ON ms.flag_so = rf.i_id
                LEFT JOIN ref_jns_printing rjp ON ms.e_jenis_printing::INT=rjp.i_id
                LEFT JOIN rfp ON rfp.i_id=mk.i_id_rfp AND rfp.i_status!=401
                LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
                LEFT JOIN ref_pelanggan rp2 ON rfp.i_penyedia=rp2.i_pel
                LEFT JOIN ref_tekstur_akhir rta ON rta.i_id=rfp.e_tekstur_akhir_to::INT
                LEFT JOIN ref_jns_bahan rjb ON rjb.i_id=rfp.e_material::INT
                LEFT JOIN ref_ori_kondisi rok1 ON rok1.i_id=rfp.e_original_cond1::INT
                LEFT JOIN ref_ori_kondisi rok2 ON rok2.i_id=rfp.e_original_cond2::INT
                LEFT JOIN ref_warna_dasar rwd ON rwd.i_id=rfp.e_color::INT
                WHERE to_char(tw.d_tgl,'yyyy-mm-dd')>='".$d_awal."' AND to_char(tw.d_tgl,'yyyy-mm-dd')<='".$d_akhir."'
                GROUP BY mk.i_id, ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rjp.e_jns_printing, rp.e_nama_pel, rp2.e_nama_pel,
                rta.e_tekstur, rf.flag_name, ms.d_so, ms.e_toleransi_cacat, ms.d_strike_off, ms.d_approval_strike_off,
                ms.e_keterangan_kirim, rfp.d_selesai, rfp.d_approved_mrk, rfp.d_approved_pro, rfp.f_repeat, rfp.i_desain, rfp.e_motif,
                rjb.e_jns_bahan, rok1.e_ori_kondisi, rok2.e_ori_kondisi, rfp.e_ket_ori_cond, rfp.e_color, rwd.e_warna_dasar,
                rfp.d_tgl_material_in, rfp.n_qty_material, rfp.e_cw_1, rfp.e_cw_2, rfp.e_cw_3, rfp.e_cw_4, rfp.e_cw_5, rfp.e_cw_6,
                rfp.e_cw_7, rfp.e_cw_8, rfp.e_gramasi_from, rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to,
                rfp.e_pakan_from, rfp.e_pakan_to, rfp.e_lusi_to, rfp.e_lusi_from, rfp.e_ket_rfp, tw.d_tgl, tw2.d_tgl, tw3.d_tgl, tw.n_proses, tw.cw, tw.i_id, tw.n_roll, tw.n_meter
                ORDER BY tw.d_tgl");

            return MyFunc::printto($query1,'Laporan_KartuProduksi_'.$d_awal.'_'.$d_akhir, $fields, 0,['excel'=>'export.kp.excel'],'landscape','a4');
        }
    
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('export.kp.form',compact('reqAjax'));
    }

    public function ws(Request $request)
    {        
        if($request->has('excel')){
            $idBagian=$request->get('id_ws');
            if($idBagian=='all') {
                $whereBagian='';
            } else {
                $whereBagian='and tw.i_id_bagian='.$idBagian;
            }
            $d_awal=date('Y-m-d', strtotime($request->get('d_awal')));
            $d_akhir=date('Y-m-d', strtotime($request->get('d_akhir')));
            $fields=[];
            $query1 = DB::select("select rp.e_nama_pel, ms.i_desain, ms.e_motif, tw.e_cw, rb.nama_bagian, ms.i_no_so, rfp.i_no_rfp, n_proses, tw.n_sisa, 
                    tw.e_ket, tw.created_at, tw.updated_at,	tw.e_pelaksana, tw.e_shift, 
                    tw.d_tgl, tw.d_time, tw.n_roll, tw.n_meter
                from tx_workstation tw 
                left join ref_bagian rb on rb.i_id = tw.i_id_bagian
                left join rfp on rfp.i_id = tw.i_id_rfp
                left join mst_so ms on ms.i_id = rfp.i_id_so
                left join ref_pelanggan rp on rp.i_pel = ms.i_pel
                where d_tgl>=:d_awal and d_tgl<=:d_akhir ".$whereBagian."
                order by tw.i_cw ASC, tw.i_id_bagian ASC, tw.n_sisa DESC, tw.d_tgl ASC",['d_awal'=>$d_awal,'d_akhir'=>$d_akhir]);
            return MyFunc::printto($query1,'Laporan_Workstation_'.$idBagian.'_'.$d_awal.'_'.$d_akhir, $fields, 0,['excel'=>'export.ws.excel'],'landscape','a4');
        }
        $bagian = DB::select("select * from ref_bagian where e_penanggung_jawab in (:id1) or e_penanggung_jawab2 in (:id2) order by i_id",['id1' => Auth::user()->id, 'id2' => Auth::user()->id]);
        if(empty($bagian)) {
            $bagian = DB::select("select * from ref_bagian order by i_id");
        }        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('export.ws.form',compact('reqAjax','bagian'));
    }
}
