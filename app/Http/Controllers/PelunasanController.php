<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class PelunasanController extends Controller
{
    public function index()
    {                
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('pelunasan.index',compact('reqAjax'));
    }

    public function data_pelunasan()
    {
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY mn.i_pelunasan)) as urutan, mn.*, rp.e_nama_pel FROM mst_pelunasan mn 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mn.i_pel                         
        ORDER BY mn.i_pelunasan");                
        return DataTables::of($data)
        ->addColumn('action', function($data) {            
            return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" href="#" onclick="lihatPelunasan('.$data->i_pelunasan.')"><i class="mdi mdi-clipboard-check"></i> Look Repayment</a>
                    </div>
                </div>';  
        })
        ->editColumn('d_pelunasan', function($data) {            
            return date('d M Y', strtotime($data->d_pelunasan));            
        })        
        ->editColumn('v_pelunasan', function($data) {            
            return 'Rp. '.number_format($data->v_pelunasan);            
        })        
        ->make(true);
    }

    public function form_pelunasan()
    {
        $param['pel'] = \DB::select("SELECT * FROM ref_pelanggan WHERE i_pel<>0");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('pelunasan.form',compact('reqAjax','param'));  
    }

    public function list_kontrabon($id)
    {
        $data = \DB::select("SELECT * FROM mst_invoice WHERE i_pel='".$id."' AND f_cancel='f' AND f_lunas='f' ORDER BY i_invoice");
        return json_encode($data);
    }

    public function list_nota($id)
    {
        $data = \DB::select("SELECT a.*, b.v_total_nppn_sisa, b.d_nota, b.i_nota_code FROM mst_invoice_item a 
                            LEFT JOIN mst_nota b on b.i_nota=a.i_nota
                            WHERE a.i_invoice='".$id."' and a.f_lunas='f'");
        return json_encode($data);
    }

    public function list_sisa_uang_masuk($id)
    {
        $data = \DB::select("SELECT * FROM tx_sisa_uang_masuk WHERE i_pel='".$id."' AND v_sisa_uang_masuk>0 ORDER BY i_uang_masuk");
        return json_encode($data);
    }

    public function simpan(Request $request)
    {
        // dd($request->all());exit();
        $qId = \DB::select("SELECT i_pelunasan FROM mst_pelunasan ORDER BY i_pelunasan DESC LIMIT 1");
        if(empty($qId)){
            $id_pelunasan = 1;
        } else {
            $id_pelunasan = $qId[0]->i_pelunasan+1;
        } 
        \DB::beginTransaction();
        try{
            $pelunasan = (int)str_replace(',','',$request->input('payment'))-(int)str_replace(',','',$request->input('payment_sisa'));
            \DB::table('mst_pelunasan')->insert([
                'i_pelunasan' => $id_pelunasan,
                'i_pelunasan_code' => $request->input('no_repayment'),
                'i_pel' => $request->input('pel'),
                'd_pelunasan' => date('Y-m-d', strtotime($request->input('d_repayment'))),
                'v_pelunasan' => $pelunasan,
                'f_cancel' => false,
                'created_at' => date('Y-m-d h:i:s'),
                'keterangan' => $request->input('keterangan'),
                'i_invoice' => $request->input('invoice_id')
            ]);
            
            $sisa_kb = \DB::select("SELECT v_total_invoice_sisa FROM mst_invoice WHERE i_invoice='".$request->input('invoice_id')."'");
            $nilai_sisa = $sisa_kb[0]->v_total_invoice_sisa - $pelunasan;
            if($nilai_sisa==0){
                \DB::table('mst_invoice')->where('i_invoice',$request->input('invoice_id'))
                ->update([
                    'v_total_invoice_sisa' => $nilai_sisa,
                    'f_lunas' => true
                ]);
            } else {
                \DB::table('mst_invoice')->where('i_invoice',$request->input('invoice_id'))
                ->update([
                    'v_total_invoice_sisa' => $nilai_sisa                   
                ]);
            }            

            for($i=1; $i<=$request->input('totrow'); $i++){
                \DB::table('mst_pelunasan_item')->insert([
                    'i_pelunasan' => $id_pelunasan,
                    'i_invoice' => $request->input('invoice_id'),
                    'i_nota' => $request->input('i_nota'.$i),
                    'v_bayar_nota' => str_replace(',','',$request->input('v_total_nota_paid'.$i)),
                    'f_cancel' => false,
                    'created_at' => date('Y-m-d h:i:s')
                ]);

                $paid = (int)str_replace(',','',$request->input('v_total_nota'.$i)) - (int)str_replace(',','',$request->input('v_total_nota_paid'.$i));
                if($paid==0){
                    \DB::table('mst_nota')->where('i_nota',$request->input('i_nota'.$i))
                    ->update([
                        'v_total_nppn_sisa' => $paid,
                        'f_lunas' => true,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);

                    \DB::table('mst_invoice_item')
                        ->where('i_invoice',$request->input('invoice_id'))
                        ->where('i_nota',$request->input('i_nota'.$i))
                        ->where('f_cancel',false)
                        ->update([
                            'f_lunas' => true,
                            'updated_at' => date('Y-m-d h:i:s')
                        ]);
                } else {
                    \DB::table('mst_nota')->where('i_nota',$request->input('i_nota'.$i))
                    ->update([
                        'v_total_nppn_sisa' => $paid,
                        'f_lunas' => false,
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                }

                $tipe_bayar = $request->input('tipe_bayar');
                $payment_sisa = $request->input('payment_sisa');
                if($tipe_bayar=='1' && $payment_sisa>0){
                    \DB::table('tx_sisa_uang_masuk')->insert([
                        'i_pel' => $request->input('pel'),
                        'v_sisa_uang_masuk' => str_replace(',','',$payment_sisa),
                        'created_at' => date('Y-m-d h:i:s')
                    ]);
                } else if($tipe_bayar=='2') {
                    \DB::table('tx_sisa_uang_masuk')->where('i_uang_masuk',$request->input('id_uang_masuk'))
                    ->update([
                        'v_sisa_uang_masuk' => str_replace(',','',$payment_sisa),
                        'updated_at' => date('Y-m-d h:i:s')
                    ]);
                }
            }
            \DB::commit();
            return $this->sendResponse('1','Input Repayment '.$request->input('no_repayment').' success');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Input Repayment '.$request->input('no_repayment').' failed');
        }
    }

    public function lihat_pelunasan($id)
    {
        $id_pelunasan = $id;
        $param['pelunasanhead'] = \DB::select("SELECT mn.*, rp.e_nama_pel, mi.i_invoice_code FROM mst_pelunasan mn
        LEFT JOIN ref_pelanggan rp ON mn.i_pel=rp.i_pel 
        LEFT JOIN mst_invoice mi ON mi.i_invoice=mn.i_invoice WHERE i_pelunasan='".$id_pelunasan."'");
        $param['pelunasanitem'] = \DB::select("SELECT mni.*, mdo.i_nota_code, mdo.d_nota, mdo.v_total_nppn FROM mst_pelunasan_item mni
        JOIN mst_nota mdo ON mdo.i_nota=mni.i_nota WHERE mni.i_pelunasan='".$id_pelunasan."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('pelunasan.lihat_pelunasan',compact('reqAjax','param'));
    }
}
