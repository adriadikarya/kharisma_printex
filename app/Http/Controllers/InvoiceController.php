<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class InvoiceController extends Controller
{
    public function index()
    {
        $param['pel'] = \DB::select("SELECT * FROM ref_pelanggan WHERE i_pel<>0");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('invoice.index',compact('reqAjax','param'));
    }

    public function data_nota(Request $request, $id)
    {
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY mn.i_nota)) as urutan, mn.*, rp.e_nama_pel FROM mst_nota mn 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mn.i_pel                 
        WHERE mn.f_invoice='f' AND mn.i_pel='".$id."'
        ORDER BY mn.i_nota");                
        return DataTables::of($data)
        ->addColumn('action', function ($data) {            
            return '<input type="checkbox" name="pilih'.$data->urutan.'" id="pilih'.$data->urutan.'" value="'.$data->i_nota.'" onclick="cek(this)"/>';
        })
        ->editColumn('d_nota', function($data) {            
            return date('d M Y', strtotime($data->d_nota));            
        })
        ->editColumn('d_due_date', function($data) {            
            return date('d M Y', strtotime($data->d_due_date));            
        })
        ->editColumn('v_total_nppn', function($data) {            
            return 'Rp. '.number_format($data->v_total_nppn);            
        })
        ->editColumn('f_lunas', function($data) {
            if($data->f_lunas == 1) {
                return 'Lunas';
            } else {
                return 'Belum Lunas';
            }
        }) 
        ->editColumn('f_invoice', function($data) {
            if($data->f_invoice == 1) {
                return 'Sudah';
            } else {
                return 'Belum';
            }
        })
        ->make(true);
    }

    public function data_invoice(Request $request)
    {        
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY mn.i_invoice)) as urutan, mn.*, rp.e_nama_pel FROM mst_invoice mn 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mn.i_pel  
        WHERE mn.f_cancel='f'                       
        ORDER BY mn.i_invoice");                
        return DataTables::of($data)
        ->addColumn('action', function ($data) {            
            if($data->v_total_invoice==$data->v_total_invoice_sisa){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKontraBon('.$data->i_invoice.')"><i class="mdi mdi-clipboard-check"></i> Lihat Kontra Bon</a>
                        <div class="dropdown-divider"></div>                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="cancel('.$data->i_invoice.')"><i class="mdi mdi-close-circle-outline"></i> Cancel KontraBon</a>       
                    </div>
                </div>';
            } else {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKontraBon('.$data->i_invoice.')"><i class="mdi mdi-clipboard-check"></i> Lihat Kontra Bon</a>                        
                    </div>
                </div>';
            }            
        })
        ->editColumn('d_invoice', function($data) {            
            return date('d M Y', strtotime($data->d_invoice));            
        })        
        ->editColumn('v_total_invoice', function($data) {            
            return 'Rp. '.number_format($data->v_total_invoice);            
        })
        ->editColumn('v_total_invoice_sisa', function($data) {            
            return 'Rp. '.number_format($data->v_total_invoice_sisa);            
        })
        ->editColumn('f_lunas', function($data) {
            if($data->f_lunas == 1) {
                return 'Lunas';
            } else {
                return 'Belum Lunas';
            }
        })
        ->make(true);
    }

    public function form_invoice(Request $request)
    {
        $data = array();
        $totnilainota = 0;        
        foreach($request->input('result') as $pilihan){
            $notapilih = collect(\DB::select("SELECT i_nota, i_nota_code, d_nota, d_due_date, v_total_nppn FROM mst_nota WHERE i_nota=".$pilihan.""))->first();
            $totnilainota+= $notapilih->v_total_nppn;
            $data[] = $notapilih;            
        }  
        $qpel = collect(\DB::select("SELECT i_pel, e_nama_pel FROM ref_pelanggan WHERE i_pel<>0"))->first();      
        $param['data'] = $data;
        $param['ipel'] = $qpel->i_pel;
        $param['namapel'] = $qpel->e_nama_pel;
        $param['totnilainota'] = $totnilainota;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('invoice.form_invoice',compact('reqAjax','param'));       
    }

    public function lihat($id)
    {
        $param['datahead'] = \DB::select("SELECT a.*, b.e_nama_pel FROM mst_invoice a LEFT JOIN ref_pelanggan b ON b.i_pel=a.i_pel WHERE a.i_invoice='".$id."'");
        $param['dataitem'] = \DB::select("SELECT a.*, b.i_nota_code, d_nota, d_due_date FROM mst_invoice_item a JOIN mst_nota b ON a.i_nota=b.i_nota WHERE a.i_invoice='".$id."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('invoice.form_lihat_invoice',compact('reqAjax','param'));
    }

    public function simpan(Request $request)
    {
        \DB::beginTransaction();
        $qId = \DB::select("SELECT i_invoice FROM mst_invoice ORDER BY i_invoice DESC LIMIT 1");
        if(empty($qId)){
            $id_invoice = 1;
        } else {
            $id_invoice = $qId[0]->i_invoice+1;
        } 
        try{
            \DB::table('mst_invoice')->insert([
                'i_invoice' => $id_invoice,
                'i_invoice_code' => $request->input('no_invoice'),
                'i_pel' => $request->input('pel'),
                'd_invoice' => date('Y-m-d', strtotime($request->input('d_invoice'))),
                'v_total_invoice' => $request->input('totnilai'),
                'v_total_invoice_sisa' => $request->input('totnilai'),
                'created_at' => date('Y-m-d h:i:s')
            ]);
            for($i=1; $i<=$request->input('total_data'); $i++){
                $idnota = $request->input('i_nota'.$i);
                $detNota = collect(\DB::select("SELECT * FROM mst_nota WHERE i_nota='".$idnota."'"))->first();
                \DB::table('mst_invoice_item')->insert([
                    'i_invoice' => $id_invoice,
                    'i_nota' => $idnota,
                    'v_qty_nota' => $detNota->n_tot_qty,
                    'v_nilai_nota' => $detNota->v_total_nppn,
                    'created_at' => date('Y-m-d h:i:s')
                ]); 
                \DB::table('mst_nota')->where('i_nota',$idnota)->update(['f_invoice' => true]);
            }
            \DB::commit();
            return $this->sendResponse('1','Input Invoice No '.$request->input('no_invoice').' success');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Input Invoice No '.$request->input('no_invoice').' gagal');
        }
    }

    public function cancel(Request $request){        
        $id = $request->input('id');
        \DB::beginTransaction();        
        try{            
            $qNota = \DB::select("SELECT i_nota FROM mst_invoice_item WHERE i_invoice='".$id."'");
            if($qNota){
                \DB::table('mst_invoice')->where('i_invoice',$id)->update(['f_cancel'=>true]);
                \DB::table('mst_invoice_item')->where('i_invoice',$id)->update(['f_cancel'=>true]);
                foreach($qNota as $item){
                    \DB::table('mst_nota')->where('i_nota',$item->i_nota)->update(['f_invoice'=>false]);
                }
                \DB::commit();
                return $this->sendResponse('1','Cancel KontraBon Success');
            } else {
                return $this->sendResponse('2','This KontraBon Doesn`t Have Any Data');
            }                   
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Cancel KontraBon Failed');
        }
    }
}
