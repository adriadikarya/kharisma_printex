<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Session;
use DB;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function logout2()
    {        
        auth()->logout();        
        Session::flush();
    }

    public function change_pass(Request $request)
    {
        $data = $request->all();
        $user = User::find(auth()->user()->id);
        if (!\Hash::check($data['old_pass'], $user->password)) {
            return $this->sendResponse('2','Password lama Anda Salah');
        } else {
            $user->update([
                'password' => \Hash::make($request->input('new_pass')),
            ]);

            return $this->sendResponse('1','Password berhasil diganti');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        
        /* if(Auth::user()->role==99 || Auth::user()->role==0 || Auth::user()->role==1 || Auth::user()->role==3 || Auth::user()->role==14){ */
            $reqAjax = (Req::ajax()) ? "ajax" : "normal";
            // return view('dashboard.dashboard',compact('reqAjax'));
            return view('dashboard.dashboard_new',compact('reqAjax'));
        /* } else {
            $reqAjax = (Req::ajax()) ? "ajax" : "normal";
            // return view('dashboard.dashboard',compact('reqAjax'));
            return view('home',compact('reqAjax'));
        } */
    }

    public function menu()
    {
        $menu_head = [];        
        if(Auth::user()->role==99){
            $q_menu_head = \DB::select("SELECT * FROM ref_menu_head ORDER BY i_id");
            if($q_menu_head){
                foreach($q_menu_head as $key => $mn_head){
                    if($mn_head->stand_alone==true){                    
                        $headitem = array(
                            'head' => array(
                                'stand_alone' => $mn_head->stand_alone,
                                'icon_menu' => $mn_head->icon_menu,
                                'nama_menu' => $mn_head->nama_menu,
                                'rut_name' => route($mn_head->rut_name)
                            ),
                            'item' => []
                        );
                        array_push($menu_head, $headitem);                        
                    } else {
                        $q_menu_item = \DB::select("SELECT * FROM ref_menu_item WHERE id_menu_head = '".$mn_head->i_id."' ORDER BY i_id");
                        $menu_item = [];
                        if($q_menu_item){
                            foreach($q_menu_item as $mn_item){                          
                                array_push($menu_item, array(
                                    'icon_menu' => $mn_item->icon_sub_menu,
                                    'nama_menu' => $mn_item->nama_sub_menu,
                                    'rut_name' => route($mn_item->rut_name)
                                ));                                              
                            }
                            $headitem = array(
                                'head' => array(
                                    'stand_alone' => $mn_head->stand_alone,
                                    'icon_menu' => $mn_head->icon_menu,
                                    'nama_menu' => $mn_head->nama_menu                                    
                                ),
                                'item' => $menu_item,
                            );                        
                            array_push($menu_head, $headitem);
                        } else {
                            return response()->json([
                                'rc' => 99,
                                'rm' => "Menu Tidak Ditemukan ",
                                'data' => null
                            ]);
                        }                        
                    }                
                }
            } else {
                return response()->json([
                    'rc' => 99,
                    'rm' => "Menu Tidak Ditemukan ",
                    'data' => null
                ]);
            }            
        } else {
            $role = Auth::user()->role;
            $q_menu_head = \DB::select("SELECT a.id_menu_head, b.* FROM mst_menu_role a JOIN ref_menu_head b ON a.id_menu_head=b.i_id WHERE role_id='".$role."' GROUP BY a.id_menu_head, b.i_id ORDER BY a.id_menu_head");
            if($q_menu_head){
                foreach($q_menu_head as $key => $mn_head){
                    if($mn_head->stand_alone==true){                    
                        $headitem = array(
                            'head' => array(
                                'stand_alone' => $mn_head->stand_alone,
                                'icon_menu' => $mn_head->icon_menu,
                                'nama_menu' => $mn_head->nama_menu,
                                'rut_name' => route($mn_head->rut_name)
                            ),
                            'item' => []
                        );
                        array_push($menu_head, $headitem);                        
                    } else {
                        $q_menu_item = \DB::select("SELECT a.id_menu_item, b.* from mst_menu_role a JOIN ref_menu_item b ON a.id_menu_item=b.i_id WHERE role_id='".$role."' AND b.id_menu_head='".$mn_head->id_menu_head."' GROUP BY a.id_menu_item, b.i_id ORDER BY a.id_menu_item");
                        $menu_item = [];
                        if($q_menu_item){
                            foreach($q_menu_item as $mn_item){                          
                                array_push($menu_item, array(
                                    'icon_menu' => $mn_item->icon_sub_menu,
                                    'nama_menu' => $mn_item->nama_sub_menu,
                                    'rut_name' => route($mn_item->rut_name)
                                ));                                              
                            }
                            $headitem = array(
                                'head' => array(
                                    'stand_alone' => $mn_head->stand_alone,
                                    'icon_menu' => $mn_head->icon_menu,
                                    'nama_menu' => $mn_head->nama_menu                                    
                                ),
                                'item' => $menu_item,
                            );                        
                            array_push($menu_head, $headitem);
                        } else {
                            return response()->json([
                                'rc' => 99,
                                'rm' => "Menu Tidak Ditemukan ",
                                'data' => null
                            ]);
                        }                        
                    }                
                }
            } else {
                return response()->json([
                    'rc' => 99,
                    'rm' => "Menu Tidak Ditemukan ",
                    'data' => null
                ]);
            }            

        }        

        return response()->json([
            'rc' => 00,
            'rm' => "Berhasil",
            'data' => $menu_head
        ]); 
    }

    public function countPosisi()
    {
        $countPos = \DB::select("SELECT 
        coalesce(sum(case when i_status in (0,1,3) AND flag_so<4 then 1 else 0 end),0) as so_baru,
        coalesce(sum(case when i_status in (0,1,3) AND flag_so>3 then 1 else 0 end),0) as so_outs,
        coalesce(sum(case when i_status=4 then 1 else 0 end),0) as kirim_rfp,
        coalesce(sum(case when i_status=5 then 1 else 0 end),0) as rejected_so,
        coalesce(sum(case when i_status=6 then 1 else 0 end),0) as rfp,
        coalesce(sum(case when i_status=10 then 1 else 0 end),0) as approved_rfp_mkt,
        coalesce(sum(case when i_status=9 then 1 else 0 end),0) as rejected_rfp_mkt,
        coalesce(sum(case when i_status=13 then 1 else 0 end),0) as approved_rfp_prod,
        coalesce(sum(case when i_status=12 then 1 else 0 end),0) as rejected_rfp_prod,
        coalesce(sum(case when i_status=14 then 1 else 0 end),0) as kartu_produksi,
        coalesce(sum(case when i_status=15 then 1 else 0 end),0) as selesai        
        FROM mst_so
        WHERE to_char(created_at,'yyyy')='".date('Y')."'");

        $countReqPSJ = \DB::select("SELECT
        coalesce(sum(case when i_status=16 then 1 else 0 end),0) as req_print_sj
        FROM mst_do_sj
        WHERE to_char(d_sj, 'yyyy')='".date('Y')."'");

        return json_encode(array('countPos' => $countPos, 'countReqPSJ' => $countReqPSJ));
    }

    public function getDataDashboard($tgl_awal, $tgl_akhir)
    {
        $countSO = \DB::select("SELECT count(i_id) AS totalso FROM mst_so WHERE to_char(created_at,'yyyy-mm-dd')>='".$tgl_awal."' AND to_char(created_at,'yyyy-mm-dd')<='".$tgl_akhir."'");
        $countRFP = \DB::select("SELECT count(i_id) AS totalrfp FROM rfp WHERE to_char(created_at,'yyyy-mm-dd')>='".$tgl_awal."' AND to_char(created_at,'yyyy-mm-dd')<='".$tgl_akhir."'");
        $countKP = \DB::select("SELECT count(i_id) AS totalkartu FROM mst_kartu WHERE to_char(created_at,'yyyy-mm-dd')>='".$tgl_awal."' AND to_char(created_at,'yyyy-mm-dd')<='".$tgl_akhir."'");
        $qtyOrder = \DB::select("SELECT sum(n_qty_kg) as qty_order FROM mst_so_item mst JOIN mst_so ms ON ms.i_id=mst.i_id_so WHERE to_char(ms.created_at,'yyyy-mm-dd')>='".$tgl_awal."' AND to_char(ms.created_at,'yyyy-mm-dd')<='".$tgl_akhir."'");

        $qtybag1 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag1 FROM tx_workstation WHERE i_id_bagian=1 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtybag2 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag2 FROM tx_workstation WHERE i_id_bagian=2 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");        
        $qtybag3 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag3 FROM tx_workstation WHERE i_id_bagian=3 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtybag4 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag4 FROM tx_workstation WHERE i_id_bagian=4 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtybag5 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag5 FROM tx_workstation WHERE i_id_bagian=5 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtybag7 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag7 FROM tx_workstation WHERE i_id_bagian=7 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtybag8 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag8 FROM tx_workstation WHERE i_id_bagian=8 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtybag9 = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totbag9 FROM tx_workstation WHERE i_id_bagian=9 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");        
        $countPrinting = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) AS totalprinting FROM tx_workstation WHERE i_id_bagian=6 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");
        $qtyDelivery = \DB::select("SELECT cast(sum(n_proses) as decimal(10,2)) as totaldeliver FROM tx_workstation WHERE i_id_bagian=10 AND d_tgl>='".$tgl_awal."' AND d_tgl<='".$tgl_akhir."'");

        $qBarChart = \DB::select("SELECT sum(totalorderatas) as totalorderatas, sum(totalorderbawah) as totalorderbawah, sum(sales_plan) as sales_plan, bulan, tahun_atas, tahun_bawah, tahun_plan 
        FROM (SELECT coalesce(sum(n_qty_kg),0) as totalorderatas, 0 as totalorderbawah, 0 as sales_plan, date_part('month', ms.created_at) as bulan, '".date('Y')."' as tahun_atas , '".date('Y', strtotime('-1 years'))."' as tahun_bawah , '".date('Y', strtotime('+1 years'))."' as tahun_plan  FROM mst_so ms JOIN mst_so_item mst ON ms.i_id=mst.i_id_so WHERE to_char(ms.created_at,'yyyy') = '".date('Y')."' GROUP BY date_part('month', ms.created_at)
        UNION 
        SELECT 0 as totalorderatas, coalesce(sum(n_qty_kg),0) as totalorderbawah, 0 as sales_plan, date_part('month', ms.created_at) as bulan, '".date('Y')."' as tahun_atas , '".date('Y', strtotime('-1 years'))."' as tahun_bawah , '".date('Y', strtotime('+1 years'))."' as tahun_plan 
        FROM mst_so ms JOIN mst_so_item mst ON ms.i_id=mst.i_id_so WHERE to_char(ms.created_at,'yyyy') = '".date('Y', strtotime('-1 years'))."' GROUP BY date_part('month', ms.created_at)
        UNION
        SELECT 0 as totalorderatas, 0 as totalorderbawah, coalesce(n_qty,0) as sales_plan, cast(bulan as double precision) as bulan, '".date('Y')."' as tahun_atas , '".date('Y', strtotime('-1 years'))."' as tahun_bawah , '".date('Y', strtotime('+1 years'))."' as tahun_plan
        FROM mst_sales_plan
	    WHERE tahun='".date('Y')."') as x 
        GROUP BY x.bulan, x.tahun_atas, x.tahun_bawah, x.tahun_plan
        ORDER BY x.bulan");

        $qSONota = \DB::select("SELECT sum(a.v_sisa) as totnilaiso, sum(b.totaltagihan) as totaltagihan
        FROM mst_so a LEFT JOIN (
        SELECT sum(b.v_total_nppn) as totaltagihan, a.i_id_so
        FROM mst_do_sj a
        JOIN mst_nota_item c ON c.i_sj=a.i_id
        JOIN mst_nota b ON b.i_nota=c.i_nota
        WHERE b.f_cancel='f' GROUP BY a.i_id_so ) as b ON b.i_id_so=a.i_id
        WHERE to_char(a.created_at,'yyyy-mm-dd')>='".$tgl_awal."' AND to_char(a.created_at,'yyyy-mm-dd')<='".$tgl_akhir."'");
        return response()->json([
            'totalSO' => $countSO[0]->totalso,
            'totalRFP' => $countRFP[0]->totalrfp,
            'totalKP' => $countKP[0]->totalkartu,
            'totalPrinting' => $countPrinting[0]->totalprinting,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'qtyOrder' => $qtyOrder[0]->qty_order,
            'totalDeliv' => $qtyDelivery[0]->totaldeliver,
            'qBarChart' => $qBarChart,
            'totbag1' => $qtybag1[0]->totbag1,
            'totbag2' => $qtybag2[0]->totbag2,
            'totbag3' => $qtybag3[0]->totbag3,
            'totbag4' => $qtybag4[0]->totbag4,
            'totbag5' => $qtybag5[0]->totbag5,
            'totbag7' => $qtybag7[0]->totbag7,
            'totbag8' => $qtybag8[0]->totbag8,
            'totbag9' => $qtybag9[0]->totbag9, 
            'nilaiSO' => $qSONota[0]->totnilaiso,
            'nilaiNota' => $qSONota[0]->totaltagihan            
        ]);
    }

    public function accept_rfp($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('tx_rfp_accepted')
                ->insert([
                    'i_id_rfp' => $id,
                    'd_accepted' => date('Y-m-d H:i:s'),
                    'user_accept' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
            \DB::commit();
            return $this->sendResponse('1','Accept JO Berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan Accept JO Gagal');
        }
    }

    public function rfp_known(Request $request)
    {        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('rfp_known.index',compact('reqAjax'));
    }

    public function data_rfp_known()
    {
        $data = \DB::select("SELECT mst_so.i_id, i_no_so, d_so, mst_so.i_pel, rp.e_nama_pel, mst_so.f_repeat, d_strike_off, d_approval_strike_off,
            d_penyerahan_brg, d_approved, mst_so.i_status, rw.definition, mst_so.created_at, rfp.i_no_rfp, rfp.d_approved_mrk, rfp.d_approved_pro, rfp.i_id as id_rfp,
            (CASE WHEN e_reject_mkt is null THEN e_reject_prod ELSE e_reject_mkt END) as alasan_reject, rb.nama_bagian
        FROM mst_so 
        LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
        LEFT JOIN rfp ON mst_so.i_id = rfp.i_id_so
        LEFT JOIN (select max(x.i_id_bagian) as i_id_bagian, x.i_id_rfp, min(x.i_cw)
			from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw from tx_workstation
			group by i_id_rfp,i_id_bagian, i_cw
			order by i_cw ) as x
			group by x.i_id_rfp
			order by i_id_rfp) as posbag on posbag.i_id_rfp = rfp.i_id
        LEFT JOIN ref_bagian rb on rb.i_id = posbag.i_id_bagian
        LEFT JOIN ref_workflow rw ON mst_so.i_status = rw.i_id
        WHERE rfp.i_status >= 13 AND rfp.i_status !=401
        order by rfp.d_approved_pro desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<a class="btn btn-sm btn-info" style="cursor: pointer;color: black;" onclick="detAcc('.$data->id_rfp.',\'rfp_known\')"><i class="mdi mdi-clipboard-check"></i>Detail Accepted</a>';
        })
        ->editColumn('d_approved_mrk', function($data) {
            return date('d-m-Y H:i:s',strtotime($data->d_approved_mrk));
        })
        ->editColumn('d_approved_pro', function($data) {
            return date('d-m-Y H:i:s',strtotime($data->d_approved_pro));
        })
        ->make(true);
    }

    public function det_rfp_known($id_rfp,$link)
    {
        $data = \DB::select("SELECT a.d_accepted, b.i_no_rfp, u.name FROM tx_rfp_accepted a
        JOIN rfp b ON a.i_id_rfp=b.i_id JOIN users u ON a.user_accept=u.id WHERE a.i_id_rfp='".$id_rfp."' ORDER BY a.i_id_rfp, a.user_accept");
        $rut = $link;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('rfp_known.det_acc',compact('reqAjax','data','rut'));
    }
}
