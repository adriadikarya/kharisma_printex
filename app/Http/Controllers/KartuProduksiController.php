<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
// use Yajra\DataTables\DataTables;
use DataTables;
use App\Models\KartuProduksiModel;
use App\Models\TxJPModel;
use App\Models\TxSJModel;
use App\Models\RFPModel;
use App\Models\SalesOrderModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class KartuProduksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.index',compact('reqAjax'));
    }

    public function data()
    {
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY rfp.urutan_fifo, rfp.d_approved_pro asc)) as urutan, rfp.i_id, rfp.i_no_rfp, rp.e_nama_pel, rfp.d_selesai, rfp.i_desain, rw.definition, rfp.i_status, rfp.d_approved_pro,
        (CASE WHEN rfp.f_proses='t' THEN 'proses' ELSE '' END) as proses, 
        (
            select count(*)
            from (values (rfp.e_cw_1), (rfp.e_cw_2), (rfp.e_cw_3), (rfp.e_cw_4), (rfp.e_cw_5), (rfp.e_cw_6), (rfp.e_cw_7), (rfp.e_cw_8)) as v(col)
            where v.col is not null
        ) as hitungcw, mk.i_no_kartu, mk.i_id as id_kartu, rfp.i_id_so, rb.nama_bagian, rfp.old_urutan_fifo,
        rfp.i_desain, sum(mkc.n_qty_roll) as roll, sum(mkc.n_qty_kg) as kg, ms.i_no_so, rfp.f_repeat, rfp.alasan_pindah_slot, mk.e_ket as keterangan_kp
        FROM rfp 
        LEFT JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN mst_kartu mk ON mk.i_id_rfp=rfp.i_id
        LEFT JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mk.i_Id
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel = rp.i_pel
        LEFT JOIN ref_workflow rw ON rfp.i_status = rw.i_id
        LEFT JOIN (select MAX(x.i_id_bagian_urut) as i_id_bagian_urut, x.i_id_rfp, min(x.i_cw)
            from (
            select i_id_rfp, 
                CASE 
                    WHEN(i_id_bagian=12) THEN 1
                    WHEN(i_id_bagian=1) THEN 2
                    WHEN(i_id_bagian=2) THEN 3
                    WHEN(i_id_bagian=3) THEN 4
                    WHEN(i_id_bagian=4) THEN 5
                    WHEN(i_id_bagian=5) THEN 6
                    WHEN(i_id_bagian=6) THEN 7
                    WHEN(i_id_bagian=7) THEN 8
                    WHEN(i_id_bagian=8) THEN 9
                    WHEN(i_id_bagian=9) THEN 10
                    WHEN(i_id_bagian=10) THEN 11 END as i_id_bagian_urut,
                i_id_bagian,
                min(i_cw) as i_cw from tx_workstation
            group by i_id_rfp,i_id, i_cw
            order by i_cw ) as x
            group by x.i_id_rfp
            order by i_id_rfp) as posbag on posbag.i_id_rfp = rfp.i_id
        LEFT JOIN (select 
            CASE 
            WHEN(i_id=12) THEN 1
            WHEN(i_id=1) THEN 2
            WHEN(i_id=2) THEN 3
            WHEN(i_id=3) THEN 4
            WHEN(i_id=4) THEN 5
            WHEN(i_id=5) THEN 6
            WHEN(i_id=6) THEN 7
            WHEN(i_id=7) THEN 8
            WHEN(i_id=8) THEN 9
            WHEN(i_id=9) THEN 10
            WHEN(i_id=10) THEN 11 
            END as i_id_urut, rb.*
        from ref_bagian rb
        order by i_id_urut) rb on rb.i_id_urut = posbag.i_id_bagian_urut
        WHERE rfp.i_status IN ('13','14')
        GROUP BY rfp.i_id, rp.e_nama_pel, rw.definition, mk.i_no_kartu, mk.i_id, rb.nama_bagian, ms.i_no_so
        ORDER BY urutan ASC");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            if($data->i_status==13){
                $id_user = Auth::user()->id;
                $acc = \DB::select("SELECT * FROM tx_rfp_accepted WHERE i_id_rfp='".$data->i_id."' AND user_accept = '".$id_user."'");
                if(Auth::user()->role==99 || Auth::user()->role==5){
                    if(empty($acc)){
                        return '<div class="dropdown">
                            <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                            <i class="mdi mdi-apps"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                                <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                                <div class="dropdown-divider"></div>                        
                                <a class="dropdown-item" style="cursor: pointer;" onclick="buatKartu('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Buat Kartu Produksi</a>
                                <div class="dropdown-divider"></div>                        
                                <a class="dropdown-item" style="cursor: pointer;" onclick="acceptRfp('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Accept JO</a>
                            </div>
                        </div>';
                    } else {
                        return '<div class="dropdown">
                            <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                            <i class="mdi mdi-apps"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                                <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                                <div class="dropdown-divider"></div>                        
                                <a class="dropdown-item" style="cursor: pointer;" onclick="buatKartu('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Buat Kartu Produksi</a>
                            </div>
                        </div>';
                    }
                } else if((Auth::user()->role==99 || Auth::user()->role==4)){
                    if(empty($acc)){
                        return '<div class="dropdown">
                            <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                            <i class="mdi mdi-apps"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                                <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                                <div class="dropdown-divider"></div>                        
                                <a class="dropdown-item" style="cursor: pointer;" onclick="buatKartu('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Buat Kartu Produksi</a>
                                <div class="dropdown-divider"></div>                        
                                <a class="dropdown-item" style="cursor: pointer;" onclick="acceptRfp('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Accept JO</a>
                            </div>
                        </div>';
                    } else {
                        return '<div class="dropdown">
                            <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                            <i class="mdi mdi-apps"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                                <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                                <div class="dropdown-divider"></div>                        
                                <a class="dropdown-item" style="cursor: pointer;" onclick="buatKartu('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Buat Kartu Produksi</a>
                            </div>
                        </div>';
                    }
                } else {
                    // return '<a class="btn btn-sm btn-info" style="cursor: pointer;color: black;font-size: 12px;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>';
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>
                        </div>
                    </div>';
                }
            } elseif($data->i_status==14 && $data->proses=='' && $data->old_urutan_fifo=='') {
                if(Auth::user()->role==99){
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="editKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-lead-pencil"></i> Edit Kartu Produksi</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-printer"></i> Print Kartu Produksi</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="downloadKartu('.$data->id_kartu.')"><i class="mdi mdi-download"></i> Download QRCode</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="pindahSlot('.$data->i_id.')"><i class="mdi mdi-navigation"></i> Pindah Slot</a>
                        </div>
                    </div>';
                } else if(Auth::user()->role==5 || Auth::user()->role==4) {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="editKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-lead-pencil"></i> Edit Kartu Produksi</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-printer"></i> Print Kartu Produksi</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="downloadKartu('.$data->id_kartu.')"><i class="mdi mdi-download"></i> Download QRCode</a>
                        </div>
                    </div>';
                } else if(Auth::user()->role==3){
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="pindahSlot('.$data->i_id.')"><i class="mdi mdi-navigation"></i> Pindah Slot</a>
                        </div>
                    </div>';                
                } else {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>
                        </div>
                    </div>';
                }
            } else {
                // if(Auth::user()->role==99 || Auth::user()->role==5){
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>    
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="editKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-lead-pencil"></i> Edit Kartu Produksi</a>                        
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-printer"></i> Print Kartu Produksi</a>
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="downloadKartu('.$data->id_kartu.')"><i class="mdi mdi-download"></i> Download QRCode</a>
                        </div>
                    </div>';
                /* } else {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                            <div class="dropdown-divider"></div>        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'kartu_produksi.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>
                        </div>
                    </div>';
                } */
            }
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat) {
                return 'Ya';
            } else {
                return 'Tidak';
            }
        })
        ->make(true);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $param['data'] = \DB::select("SELECT ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rp.e_nama_pel, rfp.i_desain, b.e_jns_printing as nama_printing, ori1.e_ori_kondisi as ori_cond1, ori2.e_ori_kondisi as ori_cond2, rfp.e_ket_ori_cond, rfp.e_color_others, d.e_tekstur,
		c.e_warna_dasar, rfp.i_id_so as id_so, rfp.e_gramasi_from, rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to, rfp.e_pakan_from, rfp.e_pakan_to, rfp.e_lusi_from, rfp.e_lusi_to, rfp.i_id as id_rfp, rfp.e_tekstur_akhir_to, rfp.d_selesai, mk.*
        FROM rfp
        LEFT JOIN mst_kartu mk ON rfp.i_id=mk.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=rfp.i_pel
        LEFT JOIN ref_ori_kondisi ori1 ON ori1.i_id=cast(rfp.e_original_cond1 as integer)
        LEFT JOIN ref_ori_kondisi ori2 ON ori2.i_id=cast(rfp.e_original_cond2 as integer)        
        LEFT JOIN ref_jns_printing b ON b.i_id=cast(ms.e_jenis_printing as integer)
        LEFT JOIN ref_warna_dasar c ON c.i_id=cast(rfp.e_color as integer)
        LEFT JOIN ref_tekstur_akhir d ON d.i_id=cast(rfp.e_tekstur_akhir_to as integer)
        WHERE rfp.i_id='".$id."'");  
        $id_so = $param['data'][0]->id_so;
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id."'");
        $param['cw'] = \DB::select("SELECT a.*, b.e_kain FROM mst_so_item a 
                                    LEFT JOIN ref_kain b ON b.i_id=cast(a.e_jenis_kain as integer)
                                    WHERE i_id_so='".$id_so."' ORDER BY a.i_id");
        $param['sj'] = false;
        $param['jp'] = false;
        $param['text'] = 'simpan';
        // $qNoKP = collect(\DB::select("SELECT no_urut, bln, thn FROM ref_no_urut WHERE code='KP' AND thn='".date('Y')."'"))->first();
        // if(!empty($qNoKP)){
        //     $bulan = date('m');
        //     $tahun = $qNoKP->thn;
        //     $nomor_kp = 'KP-'.$tahun.$bulan.'-'.$qNoKP->no_urut;
        // } else {
        //     $tahun = date('Y');
        //     $nomor_kp = 'KP-'.date('Y').date('m').'-0001';
        // }
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.form',compact('reqAjax','param'));
    }

    public function simpan(Request $request)
    {                        
        \DB::beginTransaction();
        $qId = \DB::select("SELECT i_id FROM mst_kartu ORDER BY i_id DESC LIMIT 1");
        if(empty($qId)){
            $id_kartu = 1;
        } else {
            $id_kartu = $qId[0]->i_id+1;
        } 
        $qNoKP = collect(\DB::select("SELECT no_urut, bln, thn FROM ref_no_urut WHERE code='KP' AND thn='".date('Y')."'"))->first();
        $id_rfp = $request->input('id_rfp');
        $no_rfp = \DB::select("SELECT RIGHT(i_no_rfp,5) as no_rfp FROM rfp WHERE i_id = '".$id_rfp."' ");
        if(!empty($qNoKP)){            
            $no_kp = $qNoKP->no_urut+1;
            $no_kp = str_pad($no_kp,5,"0",STR_PAD_LEFT);  
            $nomor_kp = 'KP-'.$no_rfp[0]->no_rfp.'-'.$no_kp;
        } else {            
            $no_kp = '00001';
            $nomor_kp = 'KP-'.$no_rfp[0]->no_rfp.'-'.$no_kp;
        }

        try{
            $kartu = new KartuProduksiModel();
            $kartu->i_id = $id_kartu;
            $kartu->i_no_kartu = $nomor_kp;
            $kartu->d_pengiriman = date('Y-m-d', strtotime($request->input('d_selesai')));
            $kartu->i_id_so = $request->input('id_so');
            $kartu->i_id_rfp = $request->input('id_rfp');
            $kartu->e_lebar_blanket = $request->input('lebar_blanket');
            $kartu->e_handfeel = $request->input('handfeel');
            $kartu->e_ket = $request->input('keterangan');
            $kartu->i_status = 14;
            $kartu->save();

            for($i=1; $i<=intval($request->input('row_sj')); $i++)
            {
                if($request->input('sj'.$i)){   
                    $txsj = new TxSJModel();
                    $txsj->i_no_sj = $request->input('sj'.$i);
                    $txsj->i_id_kartu = $id_kartu;
                    $txsj->save();
                }
            }

            for($i=1; $i<=intval($request->input('row_jp')); $i++)
            {   
                if($request->input('jp'.$i)){   
                    $txsj = new TxJPModel();
                    $txsj->description = $request->input('jp'.$i);
                    $txsj->i_id_kartu = $id_kartu;
                    $txsj->save();
                }
            }

            for($i=1; $i<=intval($request->input('count_cw')); $i++)
            {           
                $tmp_roll = explode('.',$request->input('roll'.$i));
                if(count($tmp_roll)>1){
                    if($tmp_roll[1]>0){
                        $roll = str_replace(',','',$tmp_roll[0].'.'.$tmp_roll[1]);
                    } else {
                        $roll = str_replace(',','',$tmp_roll[0]);
                    }
                } else {
                    $roll = str_replace(',','',$tmp_roll[0]);
                }

                $tmp_kg = explode('.',$request->input('kg'.$i));
                if(count($tmp_kg)>1){
                    if($tmp_kg[1]>0){
                        $kg = str_replace(',','',$tmp_kg[0].'.'.$tmp_kg[1]);
                    } else {
                        $kg = str_replace(',','',$tmp_kg[0]);
                    }
                } else {
                    $kg = str_replace(',','',$tmp_kg[0]);
                }

                $tmp_pjg = explode('.',$request->input('pjg'.$i));
                if(count($tmp_pjg)>1){
                    if($tmp_pjg[1]>0){
                        $pjg = str_replace(',','',$tmp_pjg[0].'.'.$tmp_pjg[1]);
                    } else {
                        $pjg = str_replace(',','',$tmp_pjg[0]);
                    }
                } else {
                    $pjg = str_replace(',','',$tmp_pjg[0]);
                }
                
                \DB::table('mst_kartu_cw')
                    ->insert([
                        'i_id' => $request->input('id_cw'.$i),
                        'i_id_kartu' => $id_kartu,
                        'e_cw' => $request->input('cw'.$i),
                        'n_qty_roll' => $roll,
                        'n_qty_pjg' => $pjg,
                        'n_qty_kg' => $kg,
                        'created_at' => date('Y-m-d h:i:s'),
                        'n_qty_kg_sisa' => $kg,
                        'd_strike_off' => $request->input('strike_off'.$i),
                        'e_jenis_kain' => $request->input('jns_kain'.$i)
                    ]);
            }

            $sales_order = SalesOrderModel::find($request->input('id_so'));
            $sales_order->i_status = 14;
            $sales_order->save();

            $rfp = RFPModel::find($request->input('id_rfp'));
            $rfp->i_status = 14;
            $rfp->save();
            
            $qNoKP = collect(\DB::select("SELECT no_urut FROM ref_no_urut WHERE code='KP' AND thn='".date('Y')."'"))->first();
            $id_rfp = $request->input('id_rfp');
            $no_rfp = \DB::select("SELECT RIGHT(i_no_rfp,5) as no_rfp FROM rfp WHERE i_id = '".$id_rfp."' ");
            if(!empty($qNoKP)){
                // $nomorKP = explode('/',$sales_order->i_no_KP);                            
                \DB::table('ref_no_urut')
                ->where('code', 'KP')                
                ->where('thn', date('Y'))
                ->update(array('no_urut' => $no_kp));
            } else {
                \DB::table('ref_no_urut')
                ->insert([
                    'code' => 'KP', 
                    'no_urut' => $no_kp,                    
                    'thn' => date('Y')
                ]);
            }

            \DB::commit();
            return $this->sendResponse('1','Input Kartu Produksi No '.$nomor_kp.' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Input Kartu Gagal');
        }
    }

    public function update(Request $request)
    {        
        \DB::beginTransaction();        
        try{
            $kartu = KartuProduksiModel::find($request->input('id_kartu'));            
            // $kartu->i_no_kartu = $request->input('no_kartu');                        
            $kartu->e_lebar_blanket = $request->input('lebar_blanket');
            $kartu->e_handfeel = $request->input('handfeel');
            $kartu->e_ket = $request->input('keterangan');            
            $kartu->save();

            TxSJModel::where('i_id_kartu',$request->input('id_kartu'))->delete();
            for($i=1; $i<=intval($request->input('row_sj')); $i++)
            {   
                $txsj = new TxSJModel();
                $txsj->i_no_sj = $request->input('sj'.$i);
                $txsj->i_id_kartu = $request->input('id_kartu');
                $txsj->save();
            }

            TxJPModel::where('i_id_kartu',$request->input('id_kartu'))->delete();
            for($i=1; $i<=intval($request->input('row_jp')); $i++)
            {   
                $txsj = new TxJPModel();
                $txsj->description = $request->input('jp'.$i);
                $txsj->i_id_kartu = $request->input('id_kartu');
                $txsj->save();
            }    
            
            \DB::table('mst_kartu_cw')->where('i_id_kartu',$request->input('id_kartu'))->delete();
            for($i=1; $i<=intval($request->input('count_cw')); $i++)
            {           
                $tmp_roll = explode('.',$request->input('roll'.$i));
                if(count($tmp_roll)>1){
                    if($tmp_roll[1]>0){
                        $roll = str_replace(',','',$tmp_roll[0].'.'.$tmp_roll[1]);
                    } else {
                        $roll = str_replace(',','',$tmp_roll[0]);
                    }
                } else {
                    $roll = str_replace(',','',$tmp_roll[0]);
                }

                $tmp_kg = explode('.',$request->input('kg'.$i));
                if(count($tmp_kg)>1){
                    if($tmp_kg[1]>0){
                        $kg = str_replace(',','',$tmp_kg[0].'.'.$tmp_kg[1]);
                    } else {
                        $kg = str_replace(',','',$tmp_kg[0]);
                    }
                } else {
                    $kg = str_replace(',','',$tmp_kg[0]);
                }

                $tmp_pjg = explode('.',$request->input('pjg'.$i));
                if(count($tmp_pjg)>1){
                    if($tmp_pjg[1]>0){
                        $pjg = str_replace(',','',$tmp_pjg[0].'.'.$tmp_pjg[1]);
                    } else {
                        $pjg = str_replace(',','',$tmp_pjg[0]);
                    }
                } else {
                    $pjg = str_replace(',','',$tmp_pjg[0]);
                }
                
                \DB::table('mst_kartu_cw')
                    ->insert([
                        'i_id' => $request->input('id_cw'.$i),
                        'i_id_kartu' => $request->input('id_kartu'),
                        'e_cw' => $request->input('cw'.$i),
                        'n_qty_roll' => $roll,
                        'n_qty_pjg' => $pjg,
                        'n_qty_kg' => $kg,
                        'created_at' => date('Y-m-d h:i:s'),
                        'n_qty_kg_sisa' => $kg,
                        'd_strike_off' => $request->input('strike_off'.$i),
                        'e_jenis_kain' => $request->input('jns_kain'.$i)
                    ]);
            }   

            \DB::commit();
            return $this->sendResponse('1','Edit Kartu Produksi No '.$request->input('no_kartu').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Edit Kartu Gagal');
        }
    }

    public function edit_kartu($id)
    {         
        $param['data'] = \DB::select("SELECT ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rp.e_nama_pel, rfp.i_desain, b.e_jns_printing as nama_printing, ori1.e_ori_kondisi as ori_cond1, ori2.e_ori_kondisi as ori_cond2, rfp.e_ket_ori_cond, rfp.e_color_others, d.e_tekstur,
		c.e_warna_dasar, rfp.i_id_so as id_so, rfp.e_gramasi_from, rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to, rfp.e_pakan_from, rfp.e_pakan_to, rfp.e_lusi_from, rfp.e_lusi_to, rfp.i_id as id_rfp, rfp.e_tekstur_akhir_to, rfp.d_selesai, mk.*
        FROM mst_kartu mk
        JOIN rfp ON rfp.i_id = mk.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=rfp.i_pel
        LEFT JOIN ref_ori_kondisi ori1 ON ori1.i_id=cast(rfp.e_original_cond1 as integer)
        LEFT JOIN ref_ori_kondisi ori2 ON ori2.i_id=cast(rfp.e_original_cond2 as integer)        
        LEFT JOIN ref_jns_printing b ON b.i_id=cast(ms.e_jenis_printing as integer)
        LEFT JOIN ref_warna_dasar c ON c.i_id=cast(rfp.e_color as integer)
        LEFT JOIN ref_tekstur_akhir d ON d.i_id=cast(rfp.e_tekstur_akhir_to as integer)        
        WHERE mk.i_id='".$id."'");  
        $id_rfp = $param['data'][0]->id_rfp;
        $id_so = $param['data'][0]->id_so;
        /* $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id_rfp."'");
        $param['cw'] = \DB::select("SELECT * FROM mst_kartu_cw WHERE i_id_kartu='".$id."'"); */
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id_rfp."'");
        $param['cw'] = \DB::select("SELECT a.*, b.e_kain FROM mst_kartu_cw a 
                                    LEFT JOIN ref_kain b ON b.i_id=cast(a.e_jenis_kain as integer)
                                    WHERE i_id_kartu='".$id."' ORDER BY a.i_id");        
        $param['sj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$id."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$id."'");
        $param['text'] = 'update';
        // $nomor_kp = false;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.form',compact('reqAjax','param'));
    }

    public function lihat_kartu($id,$link)
    {   
        $param['data'] = \DB::select("SELECT ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rp.e_nama_pel, rfp.i_desain, b.e_jns_printing as nama_printing, ori1.e_ori_kondisi as ori_cond1, ori2.e_ori_kondisi as ori_cond2, rfp.e_ket_ori_cond, rfp.e_color_others, d.e_tekstur,
		c.e_warna_dasar, rfp.i_id_so as id_so, rfp.e_gramasi_from, rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to, rfp.e_pakan_from, rfp.e_pakan_to, rfp.e_lusi_from, rfp.e_lusi_to, rfp.i_id as id_rfp, rfp.e_tekstur_akhir_to, rfp.d_selesai, mk.*
        FROM mst_kartu mk
        JOIN rfp ON rfp.i_id = mk.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=rfp.i_pel
        LEFT JOIN ref_ori_kondisi ori1 ON ori1.i_id=cast(rfp.e_original_cond1 as integer)
        LEFT JOIN ref_ori_kondisi ori2 ON ori2.i_id=cast(rfp.e_original_cond2 as integer)        
        LEFT JOIN ref_jns_printing b ON b.i_id=cast(ms.e_jenis_printing as integer)
        LEFT JOIN ref_warna_dasar c ON c.i_id=cast(rfp.e_color as integer)
        LEFT JOIN ref_tekstur_akhir d ON d.i_id=cast(rfp.e_tekstur_akhir_to as integer)        
        WHERE mk.i_id='".$id."'");  
        $id_rfp = $param['data'][0]->id_rfp;
        $id_so = $param['data'][0]->id_so;
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id_rfp."'");        
        $param['cw'] = \DB::select("SELECT a.*, a.e_cw as e_uraian_pekerjaan, b.e_kain FROM mst_kartu_cw a 
                                    LEFT JOIN ref_kain b ON b.i_id=cast(a.e_jenis_kain as integer)
                                    WHERE i_id_kartu='".$id."' ORDER BY a.i_id");
        $param['sj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$id."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$id."'");
        $param['text'] = 'update';
        $param['rut'] = $link;
        $nomor_kp = false;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.form_lihat',compact('reqAjax','param','nomor_kp'));
    }

    public function print($id_kartu, $link)
    {
        $param['data'] = \DB::select("SELECT ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rp.e_nama_pel, rfp.i_desain, b.e_jns_printing as nama_printing, ori1.e_ori_kondisi as ori_cond1, ori2.e_ori_kondisi as ori_cond2, rfp.e_ket_ori_cond, rfp.e_color_others, d.e_tekstur,
		c.e_warna_dasar, rfp.i_id_so as id_so, rfp.e_gramasi_from, rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to, rfp.e_pakan_from, rfp.e_pakan_to, rfp.e_lusi_from, rfp.e_lusi_to, rfp.i_id as id_rfp, rfp.e_tekstur_akhir_to, rfp.d_selesai, mk.*
        FROM mst_kartu mk
        JOIN rfp ON rfp.i_id = mk.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=rfp.i_pel
        LEFT JOIN ref_ori_kondisi ori1 ON ori1.i_id=cast(rfp.e_original_cond1 as integer)
        LEFT JOIN ref_ori_kondisi ori2 ON ori2.i_id=cast(rfp.e_original_cond2 as integer)        
        LEFT JOIN ref_jns_printing b ON b.i_id=cast(ms.e_jenis_printing as integer)
        LEFT JOIN ref_warna_dasar c ON c.i_id=cast(rfp.e_color as integer)
        LEFT JOIN ref_tekstur_akhir d ON d.i_id=cast(rfp.e_tekstur_akhir_to as integer)        
        WHERE mk.i_id='".$id_kartu."'");  
        $id_rfp = $param['data'][0]->id_rfp;
        $id_so = $param['data'][0]->id_so;        
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id_rfp."'");
        $param['cw'] = \DB::select("SELECT a.*, a.e_cw as e_uraian_pekerjaan, b.e_kain FROM mst_kartu_cw a 
                                    LEFT JOIN ref_kain b ON b.i_id=cast(a.e_jenis_kain as integer)
                                    WHERE i_id_kartu='".$id_kartu."' ORDER BY a.i_id");
        $param['sj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$id_kartu."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$id_kartu."'");
        $param['QRCodeNew'] = $this->createQRCode($param);
        $nomor_kp = false;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.print',compact('reqAjax','param','nomor_kp','link'));
    }

    public function createQRCode($data)
    {
        $string_qr = "";
        $roll=0;
        $kg=0;
        $meter=0;
        if($data['data']){
            foreach($data['data'] as $item){
                // $string_qr.="KARTU = ".$item->i_no_kartu."\n";
                $string_qr.="SO/PO = ".$item->i_no_so." - ".$item->i_no_po."\n";
                // $string_qr.="JO = ".$item->i_no_rfp."\n";
                $string_qr.="PELANGGAN = ".$item->e_nama_pel."\n";
                $string_qr.="DESAIN = ".$item->i_desain."\n";
                // $string_qr.="JNS PRINT = ".$item->nama_printing."\n";                
                if($item->e_ket_ori_cond){
                    $string_qr.="KOND KAIN = ".$item->ori_cond1."/".$item->ori_cond2."/".$item->e_ket_ori_cond."\n";
                } else {
                    $string_qr.="KOND KAIN = ".$item->ori_cond1."/".$item->ori_cond2."\n";
                } 
                $string_qr.="WRN DSR = ".$item->e_warna_dasar."\n";
                // $string_qr.="NO LPK = ";
                // if($data['lpk']){
                //     foreach($data['lpk'] as $lpk){
                //         $string_qr.=$lpk->e_nomor_lpk." (".$lpk->qty_roll." Roll),";
                //     }
                //     $string_qr.="\n";
                // } else {
                //     $string_qr.="TIDAK ADA NOMOR LPK\n";
                // }
                // $string_qr.="\n";
                // $string_qr.="NO SJ = ";
                // if($data['sj']){
                //     foreach($data['sj'] as $sj){
                //         $string_qr.=$sj->i_no_sj.",";
                //     }
                //     $string_qr.="\n";
                // } else {
                //     $string_qr.="TIDAK ADA SURAT JALAN\n";
                // }
                // $string_qr.="\n";
                // $string_qr.="JNS PROSES = ";
                // if($data['jp']){
                //     foreach($data['jp'] as $jp){
                //         $string_qr.=$jp->description.",";
                //     }
                //     $string_qr.="\n";
                // } else {
                //     $string_qr.="TIDAK ADA SURAT JALAN\n";
                // }
                // $string_qr.="\n";
                if($data['cw']){
                    $nocw = 1;
                    foreach($data['cw'] as $cw){   
                        $roll+=$cw->n_qty_roll;
                        $kg+=$cw->n_qty_kg;
                        $meter+=$cw->n_qty_pjg;                     
                        $string_qr.="CW".$nocw++."(".$cw->e_uraian_pekerjaan.")\n";
                        $string_qr.="QTY: ".$cw->n_qty_roll." roll, ".$cw->n_qty_kg." kg, ".$cw->n_qty_pjg." meter\n";
                        $string_qr.="STRIKE_OFF: ".date('d M Y', strtotime($cw->d_strike_off))."\n";
                        $string_qr.="JNS KAIN: ".$cw->e_kain."\n";
                    }
                    $string_qr.="\n";
                } else {
                    $string_qr.="TIDAK ADA COLOR WAY\n";
                }
                $string_qr.="TOTAL ORDER: ".$roll." roll, ".$kg." kg, ".$meter." meter\n";
                $string_qr.="HANDFEEL: ".$item->e_tekstur."\n";
                $string_qr.="LEBAR KAIN = ".$item->e_penyesuaian_lebar_from." asal, ".$item->e_penyesuaian_lebar_to." permintaan\n";
                // $string_qr.="GRAMASI KAIN = ".$item->e_gramasi_from." asal, ".$item->e_gramasi_to." permintaan\n";
                // $string_qr.="PAKAN = ".$item->e_pakan_from." asal, ".$item->e_pakan_to." permintaan\n";
                // $string_qr.="LUSI = ".$item->e_lusi_from." asal, ".$item->e_lusi_to." permintaan\n";
            }
        } else {
            $string_qr.="DATA KARTU TIDAK ADA";
        }            
        return $string_qr;
    }

    public function download($id_kartu)
    {
        $data['data'] = \DB::select("SELECT ms.i_no_so, ms.i_no_po, rfp.i_no_rfp, rp.e_nama_pel, rfp.i_desain, b.e_jns_printing as nama_printing, ori1.e_ori_kondisi as ori_cond1, ori2.e_ori_kondisi as ori_cond2, rfp.e_ket_ori_cond, rfp.e_color_others, d.e_tekstur,
		c.e_warna_dasar, rfp.i_id_so as id_so, rfp.e_gramasi_from, rfp.e_gramasi_to, rfp.e_penyesuaian_lebar_from, rfp.e_penyesuaian_lebar_to, rfp.e_pakan_from, rfp.e_pakan_to, rfp.e_lusi_from, rfp.e_lusi_to, rfp.i_id as id_rfp, rfp.e_tekstur_akhir_to, rfp.d_selesai, mk.*
        FROM mst_kartu mk
        JOIN rfp ON rfp.i_id = mk.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=rfp.i_pel
        LEFT JOIN ref_ori_kondisi ori1 ON ori1.i_id=cast(rfp.e_original_cond1 as integer)
        LEFT JOIN ref_ori_kondisi ori2 ON ori2.i_id=cast(rfp.e_original_cond2 as integer)        
        LEFT JOIN ref_jns_printing b ON b.i_id=cast(ms.e_jenis_printing as integer)
        LEFT JOIN ref_warna_dasar c ON c.i_id=cast(rfp.e_color as integer)
        LEFT JOIN ref_tekstur_akhir d ON d.i_id=cast(rfp.e_tekstur_akhir_to as integer)        
        WHERE mk.i_id='".$id_kartu."'"); 
        $no_kartu = $data['data'][0]->i_no_kartu; 
        $id_rfp = $data['data'][0]->id_rfp;
        $id_so = $data['data'][0]->id_so;        
        $data['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id_rfp."'");
        $data['cw'] = \DB::select("SELECT a.*, a.e_cw as e_uraian_pekerjaan, b.e_kain FROM mst_kartu_cw a 
        LEFT JOIN ref_kain b ON b.i_id=cast(a.e_jenis_kain as integer)
        WHERE i_id_kartu='".$id_kartu."'");
        $data['sj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$id_kartu."'");
        $data['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$id_kartu."'");       
        $string_qr = "";
        $roll=0;
        $kg=0;
        $meter=0;
        if($data['data']){
            foreach($data['data'] as $item){
                // $string_qr.="KARTU = ".$item->i_no_kartu."\n";
                $string_qr.="SO/PO = ".$item->i_no_so." - ".$item->i_no_po."\n";
                // $string_qr.="JO = ".$item->i_no_rfp."\n";
                $string_qr.="PELANGGAN = ".$item->e_nama_pel."\n";
                $string_qr.="DESAIN = ".$item->i_desain."\n";
                // $string_qr.="JNS PRINT = ".$item->nama_printing."\n";                
                if($item->e_ket_ori_cond){
                    $string_qr.="KOND KAIN = ".$item->ori_cond1."/".$item->ori_cond2."/".$item->e_ket_ori_cond."\n";
                } else {
                    $string_qr.="KOND KAIN = ".$item->ori_cond1."/".$item->ori_cond2."\n";
                }                
                $string_qr.="WRN DSR = ".$item->e_warna_dasar."\n";
                // $string_qr.="NO LPK = ";
                // if($data['lpk']){
                //     foreach($data['lpk'] as $lpk){
                //         $string_qr.=$lpk->e_nomor_lpk." (".$lpk->qty_roll." Roll),";
                //     }
                //     $string_qr.="\n";
                // } else {
                //     $string_qr.="TIDAK ADA NOMOR LPK\n";
                // }
                // $string_qr.="\n";
                // $string_qr.="NO SJ = ";
                // if($data['sj']){
                //     foreach($data['sj'] as $sj){
                //         $string_qr.=$sj->i_no_sj.",";
                //     }
                //     $string_qr.="\n";
                // } else {
                //     $string_qr.="TIDAK ADA SURAT JALAN\n";
                // }
                // $string_qr.="\n";
                // $string_qr.="JNS PROSES = ";
                // if($data['jp']){
                //     foreach($data['jp'] as $jp){
                //         $string_qr.=$jp->description.",";
                //     }
                //     $string_qr.="\n";
                // } else {
                //     $string_qr.="TIDAK ADA SURAT JALAN\n";
                // }
                // $string_qr.="\n";
                if($data['cw']){
                    $nocw = 1;
                    foreach($data['cw'] as $cw){   
                        $roll+=$cw->n_qty_roll;
                        $kg+=$cw->n_qty_kg;
                        $meter+=$cw->n_qty_pjg;                     
                        $string_qr.="CW".$nocw++."(".$cw->e_uraian_pekerjaan.")\n";
                        $string_qr.="QTY: ".$cw->n_qty_roll." roll, ".$cw->n_qty_kg." kg, ".$cw->n_qty_pjg." meter\n";
                        $string_qr.="STRIKE_OFF: ".date('d M Y', strtotime($cw->d_strike_off))."\n";
                        $string_qr.="JNS KAIN: ".$cw->e_kain."\n";
                    }
                    $string_qr.="\n";
                } else {
                    $string_qr.="TIDAK ADA COLOR WAY\n";
                }
                $string_qr.="TOTAL ORDER: ".$roll." roll, ".$kg." kg, ".$meter." meter\n";
                $string_qr.="HANDFEEL: ".$item->e_tekstur."\n";
                $string_qr.="LEBAR KAIN = [".$item->e_penyesuaian_lebar_from."] asal, [".$item->e_penyesuaian_lebar_to."] permintaan\n";
                // $string_qr.="GRAMASI KAIN = ".$item->e_gramasi_from." asal, ".$item->e_gramasi_to." permintaan\n";
                // $string_qr.="PAKAN = ".$item->e_pakan_from." asal, ".$item->e_pakan_to." permintaan\n";
                // $string_qr.="LUSI = ".$item->e_lusi_from." asal, ".$item->e_lusi_to." permintaan\n";
            }
        } else {
            $string_qr.="DATA KARTU TIDAK ADA";
        }
        $file = "public/images/qrcode/qrcode-".$no_kartu.".png";
        
        \QRCode::text($string_qr)
            ->setErrorCorrectionLevel('H')
            ->setSize(3)
            ->setMargin(1)
            ->setOutfile($file)
            ->png();

        header("Content-disposition: attachment; filename=qrcode-".$no_kartu.".png");
        header("Content-Type: image/png");
        readfile($file);

        ignore_user_abort(true);
        unlink($file);
    }

    public function pindah_slot(Request $request,$id_rfp)
    {
        \DB::beginTransaction();
        try{
            $cekUrutan = \DB::select("SELECT max(urutan_fifo) as urut_fifo FROM rfp WHERE urutan_fifo<51 AND i_status=14");
            $ambilUrutan = \DB::select("SELECT urutan_fifo FROM rfp WHERE i_id=:idrfp",['idrfp' => $id_rfp]);
            $oldFifo = $ambilUrutan[0]->urutan_fifo;
            if($cekUrutan){
                $noUrut = $cekUrutan[0]->urut_fifo+1;
            } else {
                $noUrut = 1;
            }
            \DB::table('rfp')->where('i_id',$id_rfp)
                ->update(['urutan_fifo'=>$noUrut, 'old_urutan_fifo'=>$oldFifo, 'alasan_pindah_slot' => $request->get('alasan_pindah')]);
            $urutan = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY rfp.urutan_fifo, rfp.d_approved_pro asc)) as urutan, i_id FROM rfp WHERE rfp.i_status IN ('13','14')");
            $no_urut=0;
            if($urutan){
                foreach($urutan as $nu) {
                    $no_urut++;
                    if($nu->i_id==$id_rfp) {
                        break;
                    }
                }
            }
            \DB::commit();
            return $this->sendResponse('1','Berhasil Pindah Slot Kartu Produksi/RFP ke Posisi '.$no_urut);
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Pindah Slot Gagal');
        }
    }

    public function history_kartu()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.history',compact('reqAjax'));
    }

    public function data_history()
    {
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY tw2.d_tgl ASC)) as urutan, rfp.i_id, rfp.i_no_rfp, rp.e_nama_pel, rfp.d_selesai, mk.i_no_kartu, mk.i_id as id_kartu, rfp.i_id_so, ms.i_no_so, tw2.d_tgl as d_beres
        FROM rfp 
        LEFT JOIN mst_kartu mk ON mk.i_id_rfp=rfp.i_id
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel = rp.i_pel
        LEFT JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        LEFT JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
            from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
                from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
            where x.i_id_bagian=8
            group by x.i_id_rfp, x.i_id_bagian) as tw2 ON tw2.i_id_rfp=mk.i_id_rfp
        WHERE rfp.i_status = 15
        order by urutan desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                    <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'history_kartu.kartu_produksi\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                    <div class="dropdown-divider"></div>        
                    <a class="dropdown-item" style="cursor: pointer;" onclick="lihatKartu('.$data->id_kartu.',\'history_kartu.kartu_produksi\')"><i class="mdi mdi-clipboard-check"></i> Lihat Kartu</a>   
                    <div class="dropdown-divider"></div>                                
                    <a class="dropdown-item" style="cursor: pointer;" onclick="printKartu('.$data->id_kartu.',\'history_kartu.kartu_produksi\')"><i class="mdi mdi-printer"></i> Print Kartu Produksi</a>
                    <div class="dropdown-divider"></div>   
                    <a class="dropdown-item" style="cursor: pointer;" onclick="lihatSO('.$data->i_id_so.',\'history_kartu.kartu_produksi\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" style="cursor: pointer;" onclick="printSO('.$data->i_id_so.',\'history_kartu.kartu_produksi\')"><i class="mdi mdi-printer"></i> print SO</a> 
                    <div class="dropdown-divider"></div>        
                    <a class="dropdown-item" style="cursor: pointer;" onclick="detailWorkStation('.$data->i_id.','.$data->id_kartu.',\'history_kartu.kartu_produksi\')"><i class="mdi mdi-clipboard-check"></i> Detail WorkStation</a>    
                </div>
            </div>';
        })
        ->editColumn('d_selesai', function($data) {
            if($data->d_selesai) {
                return date('d F Y',strtotime($data->d_selesai));
            }
        })
        ->editColumn('d_beres', function($data) {
            if($data->d_beres) {
                return date('d F Y',strtotime($data->d_beres));
            }
        })
        ->make(true);        
    }

    public function rfp_info(Request $request)
    {
        $data = \DB::select("SELECT mst_so.i_id, i_no_so, d_so, mst_so.i_pel, rp.e_nama_pel, mst_so.f_repeat, mst_so.d_strike_off, d_approval_strike_off, d_penyerahan_brg, d_approved, mst_so.i_status, rw.definition, mst_so.created_at, rfp.i_no_rfp, rfp.d_approved_mrk, rfp.d_approved_pro, rfp.i_id as id_rfp,(CASE WHEN e_reject_mkt is null THEN e_reject_prod ELSE e_reject_mkt END) as alasan_reject, rb.nama_bagian, count(msi.i_id) as totalcw, sum(msi.n_qty_roll) as total_roll, rfp.e_original_cond1, rok.e_ori_kondisi as ori_kond
        FROM mst_so
        JOIN mst_so_item msi ON msi.i_id_so=mst_so.i_id 
        LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
        LEFT JOIN rfp ON mst_so.i_id = rfp.i_id_so
        LEFT JOIN ref_ori_kondisi rok ON rok.i_id=rfp.e_original_cond1::INT
        LEFT JOIN (select MAX(x.i_id_bagian_urut) as i_id_bagian_urut, x.i_id_rfp, min(x.i_cw)
            from (
            select i_id_rfp, 
                CASE 
                    WHEN(i_id_bagian=12) THEN 1
                    WHEN(i_id_bagian=1) THEN 2
                    WHEN(i_id_bagian=2) THEN 3
                    WHEN(i_id_bagian=3) THEN 4
                    WHEN(i_id_bagian=4) THEN 5
                    WHEN(i_id_bagian=5) THEN 6
                    WHEN(i_id_bagian=6) THEN 7
                    WHEN(i_id_bagian=7) THEN 8
                    WHEN(i_id_bagian=8) THEN 9
                    WHEN(i_id_bagian=9) THEN 10
                    WHEN(i_id_bagian=10) THEN 11 END as i_id_bagian_urut,
                i_id_bagian,
                min(i_cw) as i_cw from tx_workstation
            group by i_id_rfp,i_id, i_cw
            order by i_cw ) as x
            group by x.i_id_rfp
            order by i_id_rfp) as posbag on posbag.i_id_rfp = rfp.i_id
        LEFT JOIN (select 
            CASE 
            WHEN(i_id=12) THEN 1
            WHEN(i_id=1) THEN 2
            WHEN(i_id=2) THEN 3
            WHEN(i_id=3) THEN 4
            WHEN(i_id=4) THEN 5
            WHEN(i_id=5) THEN 6
            WHEN(i_id=6) THEN 7
            WHEN(i_id=7) THEN 8
            WHEN(i_id=8) THEN 9
            WHEN(i_id=9) THEN 10
            WHEN(i_id=10) THEN 11 
            END as i_id_urut, rb.*
        from ref_bagian rb
        order by i_id_urut) rb on rb.i_id_urut = posbag.i_id_bagian_urut
        LEFT JOIN ref_workflow rw ON mst_so.i_status = rw.i_id        
        WHERE rfp.i_status in (13,14)
        GROUP BY mst_so.i_id, rp.e_nama_pel, rw.definition, rfp.i_no_rfp, rfp.d_approved_mrk, rfp.d_approved_pro, rfp.i_id, rb.nama_bagian, rok.e_ori_kondisi
        ORDER BY rfp.d_approved_pro desc");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.rfp_info.index',compact('reqAjax','data'));
    }

    public function lihat_rfp($id,$link)
    {
        $param['datahead'] = \DB::select("SELECT rfp.*, rp.e_nama_pel, rp.e_kont_pel, rp2.e_nama_pel as penyedia, ms.d_approval_strike_off, ms.e_jenis_printing 
                    FROM rfp
                    JOIN mst_so ms ON rfp.i_id_so=ms.i_id
                    LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
                    LEFT JOIN ref_pelanggan rp2 ON rfp.i_penyedia=rp2.i_pel
                    WHERE rfp.i_id='".$id."'");
        $param['dataitem'] = \DB::select("SELECT b.i_id as id_cw, b.e_uraian_pekerjaan, b.n_qty_kg,
        b.d_strike_off, b.e_jenis_kain, a.i_id as id_rfp, c.e_kain 
                    FROM rfp a
                    JOIN mst_so_item b ON a.i_id_so=b.i_id_so
                    LEFT JOIN ref_kain c ON cast(b.e_jenis_kain as integer)=c.i_id
                    WHERE a.i_id='".$id."'
                    ORDER BY b.i_id");
        $param['datalpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id."'");
        // $jenis_kain = \DB::table('ref_kain')->get();        
        $jns_bahan = \DB::table('ref_jns_bahan')->get();
        $ori_kon1 = \DB::table('ref_ori_kondisi')->where('tipe','1')->get();
        $ori_kon2 = \DB::table('ref_ori_kondisi')->where('tipe','2')->get();
        $jns_printing = \DB::table('ref_jns_printing')->get();
        $warna_dasar = \DB::table('ref_warna_dasar')->get();
        $tekstur_akhir = \DB::table('ref_tekstur_akhir')->get();
        $rut = $link;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.rfp_info.rfp_lihat',compact('reqAjax','param','rut','jns_bahan','ori_kon1','ori_kon2','jns_printing','warna_dasar','tekstur_akhir'));        
    }

    public function cek_accept_rfp($id_rfp)
    {
        $userid = Auth::user()->id;
        $data = \DB::select("SELECT * FROM tx_rfp_accepted WHERE i_id_rfp='".$id_rfp."' AND user_accept='".$userid."'");
        if(!empty($data)){
            return $this->sendResponse('1','Lanjutkan');
        } else {
            return $this->sendResponse('2','Accept Terlebih Dahulu JO / data ini');
        }
    }

    public function persiapan($id, $pembagi, $ori_cond)
    {
        $data = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi LEFT JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$id."' AND rfp.i_status in (13,14)");
        $totalroll = \DB::select("SELECT ceil(sum(msi.n_qty_roll)/count(msi.i_id)) as total_roll, rfp.i_no_rfp, sum(msi.n_qty_roll) as roll, count(msi.i_id) as total_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$id."' AND rfp.i_status in (13,14) GROUP BY rfp.i_no_rfp");
        $totalpercw = $totalroll[0]->total_roll;
        $ori_cond = $ori_cond==1?6:25;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.rfp_info.form_persiapan',compact('reqAjax','pembagi','data','totalpercw','id','totalroll','ori_cond'));
    }

    public function save_persiapan(Request $request)
    {                           
        \DB::beginTransaction();
        try{            
            for($i=1; $i<=$request->input('totalcw'); $i++){                
                $huruf = $this->toNum($i-1);
                $i_cw = $request->input('i_cw'.$i);
                for($j=1; $j<=$request->input('jml'.$i); $j++){
                    $kode = $request->input('kode_dyeing'.$huruf.$j);
                    for($z=1; $z<=$request->input('count_by_ori_cond'); $z++){
                        if($z==1){                            
                            if($request->input('roll_kg'.$kode.'-'.$z) || $request->input('asal_kp'.$kode.'-'.$z)){
                                \DB::table('mst_packing_list')
                                ->insert([
                                    'i_id_rfp' => $request->input('id_rfp'),
                                    'i_cw' => $i_cw,
                                    'e_kode' => $kode,
                                    'n_asal_sj' => $request->input('roll_kg'.$kode.'-'.$z)==''?0:$request->input('roll_kg'.$kode.'-'.$z),
                                    'n_asal_kp' => $request->input('asal_kp'.$kode.'-'.$z)==''?0:$request->input('asal_kp'.$kode.'-'.$z),
                                    'created_at' => date('Y-m-d H:i:s')
                                    ]);
                            } else {
                                \DB::table('mst_packing_list')
                                    ->insert([
                                        'i_id_rfp' => $request->input('id_rfp'),
                                        'i_cw' => $i_cw,
                                        'e_kode' => $kode,
                                        'n_asal_sj' => 0,
                                        'n_asal_kp' => 0,
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
                            }
                        } else {
                            if($request->input('roll_kg'.$kode.'-'.$z) || $request->input('asal_kp'.$kode.'-'.$z)){
                                \DB::table('mst_packing_list')
                                    ->insert([
                                        'i_id_rfp' => $request->input('id_rfp'),
                                        'i_cw' => $i_cw,
                                        'e_kode' => $kode,
                                        'n_asal_sj' => $request->input('roll_kg'.$kode.'-'.$z)==''?0:$request->input('roll_kg'.$kode.'-'.$z),
                                        'n_asal_kp' => $request->input('asal_kp'.$kode.'-'.$z)==''?0:$request->input('asal_kp'.$kode.'-'.$z),
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
                            }
                        }
                    }
                }
            }
            \DB::commit();
            return $this->sendResponse('1','Berhasil Input Data Persiapan');
        }catch(\Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Gagal Input Data Persiapan, '.$e->getMessage());
        }
    }

    function toNum($data) {
        $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
        return $alpha[$data];
    }

    public function cek_persiapan($id)
    {
        $data = \DB::select("SELECT * FROM mst_packing_list WHERE i_id_rfp='".$id."'");
        if(!empty($data)){
            return $this->sendResponse('2','Anda Sudah Melakukan Create Persiapan!');
        } else {
            return $this->sendResponse('1','Lanjutkan');
        }
    }

    public function cek_lihat_persiapan($id)
    {
        $data = \DB::select("SELECT * FROM mst_packing_list WHERE i_id_rfp='".$id."'");
        if(empty($data)){
            return $this->sendResponse('2','Data Persiapan Belum dibuat, Tolong dibuat terlebih dahulu!');
        } else {
            return $this->sendResponse('1','Lanjutkan');
        }
    }

    public function lihat_persiapan(Request $request,$id)
    {
        $data = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$id."'");
        $totalroll = \DB::select("SELECT ceil(sum(msi.n_qty_roll)/count(msi.i_id)) as total_roll, rfp.i_no_rfp, sum(msi.n_qty_roll) as roll, count(msi.i_id) as total_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$id."' AND rfp.i_status in (13,14) GROUP BY rfp.i_no_rfp");
        $ori_cond = $request->get('ori_cond')==1?6:25;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.rfp_info.form_lihat_persiapan',compact('reqAjax','data','totalroll','ori_cond'));
    }

    public function edit_persiapan(Request $request,$id)
    {
        $data = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi LEFT JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$id."'");
        $totalroll = \DB::select("SELECT ceil(sum(msi.n_qty_roll)/count(msi.i_id)) as total_roll, rfp.i_no_rfp, sum(msi.n_qty_roll) as roll, count(msi.i_id) as total_cw FROM mst_so_item msi LEFT JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$id."' AND rfp.i_status in (13,14) GROUP BY rfp.i_no_rfp");
        $ori_cond = $request->get('ori_cond')==1?6:25;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('kartu_produksi.rfp_info.form_edit_persiapan',compact('reqAjax','data','totalroll','ori_cond','id'));
    }

    public function update_persiapan(Request $request)
    {        
        // dd($request->all());exit();
        \DB::beginTransaction();
        try{
            for($i=1; $i<=$request->input('totalcw'); $i++){                
                $huruf = $this->toNum($i-1);
                $i_cw = $request->input('id_cw'.$i);
                for($j=1; $j<=$request->input('jml'.$i); $j++){                    
                    $kode = $request->input('kode_dyeing'.$huruf.$j);
                    for($z=1; $z<=$request->input('count_list'.$kode); $z++){                                     
                        if($request->input('id_roll_pack'.$kode.'-'.$z)){           
                            \DB::table('mst_packing_list')
                                ->where('i_id', $request->input('id_roll_pack'.$kode.'-'.$z))
                                ->update([
                                    'n_asal_sj' => $request->input('asal_sj'.$kode.'-'.$z),
                                    'n_asal_kp' => $request->input('asal_kp'.$kode.'-'.$z),
                                    'updated_at' => date('Y-m-d h:i:s')
                                ]);                                                  
                        } else {
                            if($request->input('asal_sj'.$kode.'-'.$z) || $request->input('asal_kp'.$kode.'-'.$z)){
                                \DB::table('mst_packing_list')
                                    ->insert([
                                        'i_id_rfp' => $request->input('id_rfp'),
                                        'i_cw' => $i_cw,
                                        'e_kode' => $kode,
                                        'n_asal_sj' => $request->input('asal_sj'.$kode.'-'.$z)==''?0:$request->input('asal_sj'.$kode.'-'.$z),
                                        'n_asal_kp' => $request->input('asal_kp'.$kode.'-'.$z)==''?0:$request->input('asal_kp'.$kode.'-'.$z),
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
                            }
                        }
                    }
                }
            }                    
            \DB::commit();
            return $this->sendResponse('1','Berhasil Update Data Persiapan');
        }catch(\Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Gagal Update Data Persiapan');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
