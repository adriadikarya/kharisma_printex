<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use DataTables;

class QCController extends Controller
{
    //
    public function index()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('qc.index',compact('reqAjax'));
    }

    public function dataqc()
    {
        $data = \DB::select("SELECT mk.i_id, mk.i_id_rfp, rfp.i_no_rfp, mk.i_no_kartu, rp.e_nama_pel, tw.d_tgl as tgl_prod, 
        count(mkc.i_id) as total_cw, sum(mkc.n_qty_roll) as roll,
        sum(mkc.n_qty_pjg) as meter, sum(mkc.n_qty_kg) as kg,
        ms.i_no_so, rok.e_ori_kondisi as ori_kond
        FROM mst_kartu mk
        JOIN rfp ON rfp.i_id=mk.i_id_rfp
        LEFT JOIN ref_ori_kondisi rok ON rok.i_id=rfp.e_original_cond1::INT
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
        LEFT JOIN mst_so ms ON rfp.i_id_so=ms.i_id
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mk.i_id
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=6
        group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=mk.i_id_rfp
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=8
        group by x.i_id_rfp, x.i_id_bagian) as tw2 ON tw2.i_id_rfp=mk.i_id_rfp
        WHERE mk.i_status=14
        GROUP BY mk.i_id, mk.i_no_kartu, rp.e_nama_pel, tw.d_tgl, tw2.d_tgl, mk.i_id_rfp, rfp.i_no_rfp, ms.i_no_so, ms.created_at, rok.e_ori_kondisi
        ORDER BY ms.created_at desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {            
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                    <a class="dropdown-item" style="cursor: pointer;" onclick="lihatPack(\''.$data->i_no_rfp.'\',\''.$data->i_no_kartu.'\','.$data->i_id_rfp.','.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Look Packing List</a>   
                    <div class="dropdown-divider"></div>        
                    <a class="dropdown-item" style="cursor: pointer;" onclick="createPack(\''.$data->i_no_rfp.'\',\''.$data->i_no_kartu.'\','.$data->i_id_rfp.','.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Create / Update Packing List</a>
                </div>
            </div>'; 
            // return '<a class="btn btn-sm btn-info" style="cursor: pointer;color:black;" onclick="createPack(\''.$data->i_no_rfp.'\',\''.$data->i_no_kartu.'\','.$data->i_id_rfp.','.$data->i_id.')"><i class="mdi mdi-clipboard-check"></i> Create Packing List</a>';                        
        })
        ->editColumn('tgl_prod', function($data) {
            return date('d-m-Y',strtotime($data->tgl_prod));
        })
        ->make(true);
    }

    public function create_pack(Request $request)
    {        
        $param['norfp'] = $request->get('norfp');
        $param['nokartu'] = $request->get('no_kartu');
        $idrfp = $request->get('idrfp');
        $idkartu = $request->get('idkartu');
        $param['data'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi 
        JOIN rfp ON msi.i_id_so=rfp.i_id_so
        -- JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        --     from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
        --         from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        --     where x.i_id_bagian in (6,9)
        --     group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=rfp.i_id AND msi.i_id=tw.cw 
        WHERE rfp.i_id=:id
        GROUP BY rfp.i_id, msi.e_uraian_pekerjaan, msi.i_id
        ORDER BY msi.i_id",['id' => $idrfp]);
        // $cek_no = \DB::select("SELECT * FROM ref_no_urut WHERE code='PL' AND thn='".date('Y')."'");
        // if($cek_no){
        //     $no = $cek_no[0]->no_urut+1;
        //     $no = str_pad($no,5,"0",STR_PAD_LEFT); 
        //     $param['no'] = 'PC-'.$no;
        // } else {
        //     $param['no'] = 'PC-00001';
        // }

        $param['total_input'] = \DB::select("SELECT sum(mpl.n_asal_sj) as tot_asal_sj, sum(mpl.n_asal_kp) as tot_asal_kp, sum(mpl.n_jadi_kp) as tot_jadi_kp
            FROM mst_packing_list mpl WHERE i_id_rfp=:id",['id' => $idrfp]);

        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('qc.packing_list',compact('reqAjax','param','idrfp','idkartu'));
    }

    public function lihat_pack(Request $request)
    {
        $param['norfp'] = $request->get('no_rfp');
        $param['nokartu'] = $request->get('no_kartu');
        $idrfp = $request->get('id_rfp');
        $idkartu = $request->get('id_kartu');
        $param['data'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$idrfp."' ORDER BY msi.i_id");
        $param['total_input'] = \DB::select("SELECT sum(mpl.n_asal_sj) as tot_asal_sj, sum(mpl.n_asal_kp) as tot_asal_kp, sum(mpl.n_jadi_kp) as tot_jadi_kp
            FROM mst_packing_list mpl WHERE i_id_rfp=:id",['id' => $idrfp]);

        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('qc.lihat_packing_list',compact('reqAjax','param','idrfp','idkartu'));
    }

    public function cek_persiapan($id)
    {
        $data = \DB::select("SELECT * FROM mst_packing_list WHERE i_id_rfp='".$id."'");
        if(!empty($data)){
            return $this->sendResponse('1','Lanjutkan');
        } else {
            return $this->sendResponse('2','Lakukan Proses Input Persiapan terlebih dahulu!, Tanyakan ke bagian persiapan');
        }
    }

    public function save_pack(Request $request)
    {        
        // dd($request->all());exit();
        \DB::beginTransaction();
        try{
            for($i=1; $i<=$request->input('totalcw'); $i++){                
                $huruf = $this->toNum($i-1);
                $i_cw = $request->input('id_cw'.$i);
                for($j=1; $j<=$request->input('jml'.$i); $j++){                    
                    $kode = $request->input('kode_dyeing'.$huruf.$j);
                    for($z=1; $z<=$request->input('count_list'.$kode); $z++){                                                
                        \DB::table('mst_packing_list')
                            ->where('i_id', $request->input('id_roll_pack'.$kode.'-'.$z))
                            ->update([
                                'n_asal_kp' => $request->input('asal_kp'.$kode.'-'.$z),
                                'n_jadi_kp' => $request->input('jadi_kp'.$kode.'-'.$z),
                                'i_id_kartu' => $request->input('idkartu'),
                                // 'i_no_packing' => $request->input('packing_number'),
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);                                                  
                    }
                }
            }                    
            \DB::commit();
            return $this->sendResponse('1','Berhasil Update Data Packing List');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Gagal Update Data Packing List');
        }
    }

    function toNum($data) {
        $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
        return $alpha[$data];
    }
}
