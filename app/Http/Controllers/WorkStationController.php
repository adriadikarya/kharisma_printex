<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
// use Yajra\DataTables\DataTables;
use DataTables;
// use App\Models\KartuProduksiModel;
// use App\Models\TxJPModel;
// use App\Models\TxSJModel;
// use App\Models\RFPModel;
// use App\Models\SalesOrderModel;
use App\Models\txWorkstationModel;
use Illuminate\Support\Facades\Auth;
use App\MyFunc;

class WorkStationController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('workstation.index',compact('reqAjax'));
    }

    public function data()
    {
        /* query lama */
        /* $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY rfp.urutan_fifo, rfp.d_approved_pro asc)) as urutan, rfp.i_id, rfp.i_no_rfp, rp.e_nama_pel, rfp.d_selesai, rfp.i_desain, rw.definition, rfp.i_status, rfp.d_approved_pro,
            (CASE WHEN rfp.f_proses='t' THEN 'proses' ELSE '' END) as proses, 
            (
                select count(*)
                from (values (rfp.e_cw_1), (rfp.e_cw_2), (rfp.e_cw_3), (rfp.e_cw_4), (rfp.e_cw_5), (rfp.e_cw_6), (rfp.e_cw_7), (rfp.e_cw_8)) as v(col)
                where v.col is not null
            ) as hitungcw, mk.i_no_kartu, mk.i_id as id_kartu, rfp.i_id_so, rb.nama_bagian,
            rfp.i_desain, sum(mkc.n_qty_roll) as roll, sum(mkc.n_qty_kg) as kg, ms.i_no_so, rfp.f_repeat, rfp.alasan_pindah_slot, mk.e_ket as keterangan_kp
            FROM rfp 
            LEFT JOIN mst_so ms ON ms.i_id=rfp.i_id_so
            LEFT JOIN mst_kartu mk ON mk.i_id_rfp=rfp.i_id
            LEFT JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mk.i_Id
            LEFT JOIN ref_pelanggan rp ON rfp.i_pel = rp.i_pel
            LEFT JOIN ref_workflow rw ON rfp.i_status = rw.i_id
            LEFT JOIN (select max(x.i_id_bagian) as i_id_bagian, x.i_id_rfp, min(x.i_cw)
                from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw from tx_workstation
                group by i_id_rfp,i_id_bagian, i_cw
                order by i_cw ) as x
                group by x.i_id_rfp
                order by i_id_rfp) as posbag on posbag.i_id_rfp = rfp.i_id
            LEFT JOIN ref_bagian rb on rb.i_id = posbag.i_id_bagian
            WHERE rfp.i_status=14
            GROUP BY rfp.i_id, rp.e_nama_pel, rw.definition, mk.i_no_kartu, mk.i_id, rb.nama_bagian, ms.i_no_so
            order by urutan asc")
        ; */

        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY rfp.urutan_fifo, rfp.d_approved_pro asc)) as urutan, rfp.i_id, rfp.i_no_rfp, 
            rp.      e_nama_pel, rfp.d_selesai, rfp.i_desain, rw.definition, rfp.i_status, rfp.d_approved_pro,(CASE WHEN rfp.f_proses='t' THEN 'proses' ELSE '' END) as proses, (
                select count(*)
                from (values (rfp.e_cw_1), (rfp.e_cw_2), (rfp.e_cw_3), (rfp.e_cw_4), (rfp.e_cw_5), (rfp.e_cw_6), (rfp.e_cw_7), (rfp.e_cw_8)) as v(col)
                where v.col is not null
            ) as hitungcw, mk.i_no_kartu, mk.i_id as id_kartu, rfp.i_id_so, rb.nama_bagian,
            rfp.i_desain, sum(mkc.n_qty_roll) as roll, sum(mkc.n_qty_kg) as kg, ms.i_no_so, rfp.f_repeat, rfp.alasan_pindah_slot, mk.e_ket as keterangan_kp
            FROM rfp 
            LEFT JOIN mst_so ms ON ms.i_id=rfp.i_id_so
            LEFT JOIN mst_kartu mk ON mk.i_id_rfp=rfp.i_id
            LEFT JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mk.i_Id
            LEFT JOIN ref_pelanggan rp ON rfp.i_pel = rp.i_pel
            LEFT JOIN ref_workflow rw ON rfp.i_status = rw.i_id
            LEFT JOIN (select MAX(x.i_id_bagian_urut) as i_id_bagian_urut, x.i_id_rfp, min(x.i_cw)
                from (
                select i_id_rfp, 
                    CASE 
                        WHEN(i_id_bagian=12) THEN 1
                        WHEN(i_id_bagian=1) THEN 2
                        WHEN(i_id_bagian=2) THEN 3
                        WHEN(i_id_bagian=3) THEN 4
                        WHEN(i_id_bagian=4) THEN 5
                        WHEN(i_id_bagian=5) THEN 6
                        WHEN(i_id_bagian=6) THEN 7
                        WHEN(i_id_bagian=7) THEN 8
                        WHEN(i_id_bagian=8) THEN 9
                        WHEN(i_id_bagian=9) THEN 10
                        WHEN(i_id_bagian=10) THEN 11 END as i_id_bagian_urut,
                    i_id_bagian,
                    min(i_cw) as i_cw from tx_workstation
                group by i_id_rfp,i_id, i_cw
                order by i_cw ) as x
                group by x.i_id_rfp
                order by i_id_rfp) as posbag on posbag.i_id_rfp = rfp.i_id
            LEFT JOIN (select 
                CASE 
                WHEN(i_id=12) THEN 1
                WHEN(i_id=1) THEN 2
                WHEN(i_id=2) THEN 3
                WHEN(i_id=3) THEN 4
                WHEN(i_id=4) THEN 5
                WHEN(i_id=5) THEN 6
                WHEN(i_id=6) THEN 7
                WHEN(i_id=7) THEN 8
                WHEN(i_id=8) THEN 9
                WHEN(i_id=9) THEN 10
                WHEN(i_id=10) THEN 11 
                END as i_id_urut, rb.*
            from ref_bagian rb
            order by i_id_urut) rb on rb.i_id_urut = posbag.i_id_bagian_urut
            WHERE rfp.i_status=14
            GROUP BY rfp.i_id, rp.e_nama_pel, rw.definition, mk.i_no_kartu, mk.i_id, rb.nama_bagian, ms.i_no_so
            order by urutan asc"
        );
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            // if(Auth::user()->role==99 || Auth::user()->role==5){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'workstation.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>   
                        <div class="dropdown-divider"></div>        
                        <a class="dropdown-item" style="cursor: pointer;" onclick="updateWorkStation('.$data->id_kartu.','.$data->i_id.','.$data->i_id_so.',\'workstation.index\')"><i class="mdi mdi-clipboard-check"></i> Update WorkStation</a>
                        <div class="dropdown-divider"></div>        
                        <a class="dropdown-item" style="cursor: pointer;" onclick="detailWorkStation('.$data->i_id.','.$data->id_kartu.',\'workstation.index\')"><i class="mdi mdi-clipboard-check"></i> Detail WorkStation</a>
                    </div>
                </div>';
            /* } else {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->i_id.',\'workstation.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>                           
                        <div class="dropdown-divider"></div>        
                        <a class="dropdown-item" style="cursor: pointer;" onclick="detailWorkStation('.$data->i_id.','.$data->id_kartu.',\'workstation.index\')"><i class="mdi mdi-clipboard-check"></i> Detail WorkStation</a>
                    </div>
                </div>';
            } */
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat) {
                return 'Ya';
            } else {
                return 'Tidak';
            }
        })
        ->make(true);
    }

    public function list_cw($id_kartu,$id_rfp,$id_so,$link)
    {
        $param['data'] = \DB::select("SELECT msi.*, msi.e_cw as e_uraian_pekerjaan, rb.nama_bagian FROM mst_kartu_cw msi LEFT JOIN ref_bagian rb ON rb.i_id=msi.i_id_bagian WHERE msi.i_id_kartu = '".$id_kartu."' ORDER BY msi.i_id ASC");
        $param['no_kartu'] = \DB::select("SELECT i_id, i_no_kartu FROM mst_kartu WHERE i_id='".$id_kartu."'");
        $param['no_rfp'] = \DB::select("SELECT i_no_rfp FROM rfp WHERE i_id='".$id_rfp."'");        
        $param['id_rfp'] = $id_rfp;
        $rut = $link;
        $param['id_kartu'] = $id_kartu;
        $last_ws = \DB::table('ref_bagian')->where('last_workstation',true)->first();
        $param['last_ws'] = $last_ws?$last_ws->i_id:99;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('workstation.list_cw',compact('reqAjax','param','rut'));
    }

    public function bagian($id, $id_bagian)
    {                
        $last_ws = \DB::table('ref_bagian')->where('last_workstation',true)->first();
        if(Auth::user()->role==99) {
            $data = \DB::select("SELECT * FROM ref_bagian ORDER BY i_id");
        } else {
            $data = \DB::select("SELECT * FROM ref_bagian WHERE (e_penanggung_jawab=:user OR e_penanggung_jawab2=:user) ORDER BY i_id",['user'=>Auth::user()->id]);
        }
        return json_encode($data);
    }

    public function update_cw(Request $request)
    {
        // dd($request->all());exit();
        $id_rfp = $request->input('id_rfp');
        $cekDateProses = \DB::select("SELECT d_proses FROM rfp WHERE i_id='".$id_rfp."'");
        \DB::beginTransaction();
        try{
            $sisa = (float)$request->input('qty_tot')-(float)($request->input('qty_proses')+(float)$request->input('qty_proses_bagian'));
            $tx_ws = new txWorkStationModel();
            $tx_ws->i_id_rfp = $request->input('id_rfp');
            $tx_ws->e_cw = $request->input('cw');
            $tx_ws->i_id_bagian = $request->input('bagian');
            $tx_ws->n_tot_qty = (float)$request->input('qty_tot');            
            $tx_ws->n_proses = (float)$request->input('qty_proses');
            $tx_ws->n_sisa = $sisa;
            $tx_ws->e_pelaksana = $request->input('pelaksana');
            $tx_ws->e_shift = $request->input('shift');
            $tx_ws->d_tgl = date('Y-m-d',strtotime($request->input('d_proses')));
            $tx_ws->d_time = $request->input('t_proses');
            $tx_ws->e_ket = $request->input('ket');
            $tx_ws->i_id_kartu = $request->input('id_kartu');
            $tx_ws->i_cw = $request->input('id_cw');
            $tx_ws->n_roll = $request->input('roll_proses');
            $tx_ws->n_meter = str_replace(',','',$request->input('meter_proses'));
            $tx_ws->save();
            if($cekDateProses[0]->d_proses==null || $cekDateProses[0]->d_proses==''){
                \DB::table('rfp')
                ->where('i_id', $request->input('id_rfp'))
                ->update(['f_proses' => 't', 'd_proses' => date('Y-m-d h:i:s')]);
            }
            $last_ws = \DB::table('ref_bagian')->where('last_workstation',true)->first();
            if($request->input('bagian')==$last_ws->i_id){
                $get_sisa = \DB::select("SELECT n_qty_kg_sisa FROM mst_kartu_cw WHERE i_id='".$request->input('id_cw')."'");
                $hitungan = $get_sisa[0]->n_qty_kg_sisa - (float)$request->input('qty_proses');
                if($hitungan<0){
                    \DB::rollback();
                    return $this->sendResponse('2','Tidak boleh melebihi Qty yang di order');
                } else {
                    \DB::table('mst_kartu_cw')->where('i_id',$request->input('id_cw'))
                        ->update(['n_qty_kg_sisa'=>$hitungan, 'i_id_bagian'=>$request->input('bagian')]);
                }
            } else {
                \DB::table('mst_kartu_cw')->where('i_id',$request->input('id_cw'))
                        ->update(['i_id_bagian'=>$request->input('bagian')]);
            }
            \DB::commit();
            return $this->sendResponse('1','Update Color Ways '.$request->input('cw').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Update Color Ways Gagal');
        }
    }

    public function detail_ws($id_rfp,$id_kartu,$link)
    {

        $param['datahead'] = \DB::select("SELECT *, e_cw as e_uraian_pekerjaan FROM mst_kartu_cw WHERE i_id_kartu = :idkartu",['idkartu' => $id_kartu]);
        $param['dataitem'] = \DB::select("SELECT tw.*,
        CASE 
            WHEN(i_id_bagian=12) THEN 1
            WHEN(i_id_bagian=1) THEN 2
            WHEN(i_id_bagian=2) THEN 3
            WHEN(i_id_bagian=3) THEN 4
            WHEN(i_id_bagian=4) THEN 5
            WHEN(i_id_bagian=5) THEN 6
            WHEN(i_id_bagian=6) THEN 7
            WHEN(i_id_bagian=7) THEN 8
            WHEN(i_id_bagian=8) THEN 9
            WHEN(i_id_bagian=9) THEN 10
            WHEN(i_id_bagian=10) THEN 11 END as 
        i_id_bagian_urut
        , rb.nama_bagian, rb.e_penanggung_jawab, rb.e_penanggung_jawab2 FROM tx_workstation tw
        JOIN ref_bagian rb ON tw.i_id_bagian = rb.i_id WHERE tw.i_id_rfp=:idrfp
        ORDER BY tw.i_cw ASC, i_id_bagian_urut ASC, tw.n_sisa DESC, tw.d_tgl ASC",['idrfp' => $id_rfp]);
        $param['no_rfp'] = \DB::select("SELECT i_no_rfp FROM rfp WHERE i_id=:idrfp",['idrfp' => $id_rfp]);  
        $param['no_so'] = \DB::select("SELECT i_no_so FROM mst_so ms LEFT JOIN rfp ON rfp.i_id_so=ms.i_id WHERE rfp.i_id=:idrfp",['idrfp' => $id_rfp]);        
        $rut = $link;        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('workstation.detail_ws',compact('reqAjax','param','rut','id_rfp','id_kartu'));
    }

    public function excel_det_ws(Request $request)
    {                
        $fields=[];
        $id_rfp = $request->get('id_rfp_excel');
        $id_kartu = $request->get('id_kartu_excel');        
        $param['datahead'] = \DB::select("SELECT *, e_cw as e_uraian_pekerjaan FROM mst_kartu_cw WHERE i_id_kartu = :idkartu",['idkartu' => $id_kartu]);
        $param['dataitem'] = \DB::select("SELECT tw.*,
        CASE 
            WHEN(i_id_bagian=12) THEN 1
            WHEN(i_id_bagian=1) THEN 2
            WHEN(i_id_bagian=2) THEN 3
            WHEN(i_id_bagian=3) THEN 4
            WHEN(i_id_bagian=4) THEN 5
            WHEN(i_id_bagian=5) THEN 6
            WHEN(i_id_bagian=6) THEN 7
            WHEN(i_id_bagian=7) THEN 8
            WHEN(i_id_bagian=8) THEN 9
            WHEN(i_id_bagian=9) THEN 10
            WHEN(i_id_bagian=10) THEN 11 END as 
        i_id_bagian_urut
        , rb.nama_bagian FROM tx_workstation tw
        JOIN ref_bagian rb ON tw.i_id_bagian = rb.i_id WHERE tw.i_id_rfp=:idrfp
        ORDER BY tw.i_cw ASC, i_id_bagian_urut ASC, tw.n_sisa DESC, tw.d_tgl ASC",['idrfp' => $id_rfp]);
        $param['no_rfp'] = \DB::select("SELECT i_no_rfp FROM rfp WHERE i_id=:idrfp",['idrfp' => $id_rfp]);  
        $param['no_so'] = \DB::select("SELECT i_no_so FROM mst_so ms LEFT JOIN rfp ON rfp.i_id_so=ms.i_id WHERE rfp.i_id=:idrfp",['idrfp' => $id_rfp]);
        
        $excel = MyFunc::printto($param,'Detail_WS_SO_'.$param['no_so'][0]->i_no_so.'_'.date('d-m-Y'), $fields, 0,['excel'=>'workstation.excel_ws'],'landscape','a4');        
        return $excel;
    }

    public function ambilSisa($id_cw, $bagian)
    {
        $qtySumProses = \DB::select("SELECT sum(n_proses) as sum_proses FROM tx_workstation WHERE i_cw='".$id_cw."' AND i_id_bagian='".$bagian."'");        
        $totalQtyProses = $qtySumProses[0]->sum_proses;        
        return json_encode(array('tot_qty_proses'=>$totalQtyProses));
    }

    public function selesai(Request $request, $id_kartu)
    {        
        $ambilID = \DB::table('mst_kartu')->select('i_id_so', 'i_id_rfp')->where('i_id',$id_kartu)->get();
        $message = "";
        foreach($request->get('arr_cw') as $row) {        
            $cekStation = \DB::select("SELECT rb.i_id, rb.nama_bagian, tw.e_cw as e_cw, tw.i_cw
            FROM ref_bagian rb
            LEFT JOIN tx_workstation tw on tw.i_id_bagian=rb.i_id AND tw.i_id_kartu=:idkartu AND tw.i_cw=:icw
            WHERE rb.wajib_isi='t' AND tw.i_cw ISNULL",['idkartu' => $id_kartu, 'icw' => $row['i_cw']]);
            if($cekStation) {
                $message.="Harap ingatkan Bagian wajib yang belum input pada color ways ".$row['e_cw']."\n";
                $message.="Bagian wajib yang belum input:\n";
                foreach($cekStation as $cs) {
                    $message.="- ".$cs->nama_bagian."\n";
                }
            }
        }
    
        \DB::beginTransaction();
        try{            
            if($message!="") {                                
                \DB::rollback();
                return $this->sendResponse('2',$message);
            } else {
                \DB::table('mst_kartu')->where('i_id',$id_kartu)->update(['i_status'=>15,'d_beres'=>date('Y-m-d H:i:s')]);
                \DB::table('mst_so')->where('i_id',$ambilID[0]->i_id_so)->update(['i_status'=>15]);
                $ambilUrutan = \DB::select("SELECT old_urutan_fifo FROM rfp WHERE i_id=:idrfp",['idrfp' => $ambilID[0]->i_id_rfp]);                        
                if($ambilUrutan) {                                
                    $oldFifo = $ambilUrutan[0]->old_urutan_fifo;
                    if($oldFifo==null){
                        \DB::table('rfp')->where('i_id',$ambilID[0]->i_id_rfp)->update(['i_status'=>15]);
                    } else {
                        \DB::table('rfp')->where('i_id',$ambilID[0]->i_id_rfp)->update(['i_status'=>15, 'urutan_fifo'=>$oldFifo,'old_urutan_fifo'=>null]);
                    }
                } else {
                    \DB::table('rfp')->where('i_id',$ambilID[0]->i_id_rfp)->update(['i_status'=>15]);
                }
                \DB::commit();
                return $this->sendResponse('1', 'Berhasil Update Status Selesai Pada Orderan Ini!');   
            }
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Update Status Selesai Gagal');
        }
    }

    public function edit_detail($id)
    {
        $data_det = \DB::select("SELECT x.*, (SELECT sum(n_proses) as sum_proses FROM tx_workstation tw2
                WHERE tw2.i_cw=x.i_cw AND tw2.i_id_bagian=x.i_id_bagian) as total_proses
            FROM (SELECT tw.*, mkc.n_qty_kg FROM tx_workstation tw 
            LEFT JOIN mst_kartu_cw mkc ON mkc.i_id = tw.i_cw	
            WHERE tw.i_id=:id ) as x",['id'=>$id]);
        // $bagian = \DB::select("SELECT * FROM ref_bagian ORDER BY i_id");
        $last_ws = \DB::table('ref_bagian')->where('last_workstation',true)->first();
        if(Auth::user()->role==99) {
            $bagian = \DB::select("SELECT * FROM ref_bagian ORDER BY i_id");
        } else {
            $bagian = \DB::select("SELECT * FROM ref_bagian WHERE (e_penanggung_jawab=:user OR e_penanggung_jawab2=:user) ORDER BY i_id",['user'=>Auth::user()->id]);
        }

        return response()->json([
            'data_det' => $data_det,
            'bag' => $bagian
        ]);
    }

    public function update_detail(Request $request)
    {
        // $id_rfp = $request->input('id_rfp');
        // $cekDateProses = \DB::select("SELECT d_proses FROM rfp WHERE i_id='".$id_rfp."'");
        \DB::beginTransaction();
        try{                        
            $sisa = (float)$request->input('qty_tot')-((float)$request->input('qty_proses')+(float)$request->input('qty_proses_bagian'));               
            $tx_ws = txWorkStationModel::find($request->input('id_det_ws'));            
            // $tx_ws->i_id_rfp = $request->input('id_rfp');
            // $tx_ws->e_cw = $request->input('cw');
            $tx_ws->i_id_bagian = $request->input('bagian');
            $tx_ws->n_tot_qty = $request->input('qty_tot');            
            $tx_ws->n_proses = $request->input('qty_proses');
            $tx_ws->n_sisa = round($sisa,2);
            $tx_ws->e_pelaksana = $request->input('pelaksana');
            $tx_ws->e_shift = $request->input('shift');
            $tx_ws->d_tgl = date('Y-m-d',strtotime($request->input('d_proses')));
            $tx_ws->d_time = $request->input('t_proses');
            $tx_ws->e_ket = $request->input('ket');
            $tx_ws->n_roll = $request->input('roll_proses');
            $tx_ws->n_meter = str_replace(',','',$request->input('meter_proses'));
            // $tx_ws->i_id_kartu = $request->input('id_kartu');
            // $tx_ws->i_cw = $request->input('id_cw');
            $tx_ws->save();
            // if($cekDateProses[0]->d_proses==null || $cekDateProses[0]->d_proses==''){
            //     \DB::table('rfp')
            //     ->where('i_id', $request->input('id_rfp'))
            //     ->update(['f_proses' => 't', 'd_proses' => date('Y-m-d h:i:s')]);
            // }
            $last_ws = \DB::table('ref_bagian')->where('last_workstation',true)->first();
            if($request->input('bagian')==$last_ws->i_id){                                
                if($sisa<0){
                    \DB::rollback();
                    return $this->sendResponse('2','Tidak boleh melebihi Qty yang di order');
                } else {
                    \DB::table('mst_kartu_cw')->where('i_id',$request->input('id_cw'))
                        ->update(['n_qty_kg_sisa'=>round($sisa,2), 'i_id_bagian'=>$request->input('bagian')]);
                }
            } else {
                \DB::table('mst_kartu_cw')->where('i_id',$request->input('id_cw'))
                        ->update(['i_id_bagian'=>$request->input('bagian')]);
            }
            \DB::commit();
            return $this->sendResponse('1','Update Detail Workstation Color Ways '.$request->input('cw').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Update Color Ways Gagal');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
