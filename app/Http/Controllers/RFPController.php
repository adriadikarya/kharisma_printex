<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
// use Yajra\DataTables\DataTables;
use DataTables;
use App\Models\SalesOrderModel;
use App\Models\RFPModel;
use App\Models\RFPLPKModel;
use Illuminate\Support\Facades\Auth;

class RFPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.index',compact('reqAjax'));
    }

    public function data()
    {
        $data = \DB::select("SELECT mst_so.i_id, i_no_so, d_so, mst_so.i_pel, rp.e_nama_pel, mst_so.f_repeat, d_strike_off, d_approval_strike_off,
            d_penyerahan_brg, d_approved, mst_so.i_status, rw.definition, mst_so.created_at, rfp.i_no_rfp, rfp.d_approved_mrk, rfp.d_approved_pro, rfp.i_id as id_rfp,
            (CASE WHEN e_reject_mkt is null THEN e_reject_prod ELSE e_reject_mkt END) as alasan_reject, rb.nama_bagian
        FROM mst_so 
        LEFT JOIN ref_pelanggan rp ON mst_so.i_pel = rp.i_pel
        LEFT JOIN rfp ON mst_so.i_id = rfp.i_id_so
        LEFT JOIN (select max(x.i_id_bagian) as i_id_bagian, x.i_id_rfp, min(x.i_cw)
			from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw from tx_workstation
			group by i_id_rfp,i_id_bagian, i_cw
			order by i_cw ) as x
			group by x.i_id_rfp
			order by i_id_rfp) as posbag on posbag.i_id_rfp = rfp.i_id
        LEFT JOIN ref_bagian rb on rb.i_id = posbag.i_id_bagian
        LEFT JOIN ref_workflow rw ON mst_so.i_status = rw.i_id
        WHERE mst_so.flag_so < 4 AND mst_so.i_status!=401
        order by mst_so.created_at desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            if($data->i_status<4){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                    </div>
                </div>';
            } else if($data->i_status==4){
                if(Auth::user()->role==99 || Auth::user()->role==2) {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>                        
                            <a class="dropdown-item" style="cursor: pointer;" onclick="buatRfp('.$data->i_id.')"><i class="mdi mdi-plus-circle-outline"></i> Buat JO</a>
                        </div>
                    </div>';
                } else {
                    return '<a class="btn btn-sm btn-info" style="cursor: pointer;color:black;font-size:13.5px;" onclick="lihat('.$data->i_id.',\'rfp.index\')">Lihat SO</a>';
                }
            } else if($data->i_status==6 || $data->i_status==9) {
                if(Auth::user()->role==99 || Auth::user()->role==2) {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="editRfp('.$data->id_rfp.')"><i class="mdi mdi-lead-pencil"></i> Edit JO</a>
                            <div class="dropdown-divider"></div>                
                            <a class="dropdown-item" style="cursor: pointer;" onclick="kirimRfpMkt('.$data->id_rfp.')"><i class="mdi mdi-plus-circle-outline"></i> Kirim JO ke Manager Marketing</a>
                        </div>
                    </div>';
                } else {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                        </div>
                    </div>';
                }
            } else if($data->i_status==7 || $data->i_status==10 || $data->i_status==13) {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="printRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-printer"></i> Print JO</a>
                    </div>
                </div>';
            } else if($data->i_status==8) {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="kirimRfpProd('.$data->id_rfp.')"><i class="mdi mdi-plus-circle-outline"></i> Kirim JO ke Manager Produksi</a>
                    </div>
                </div>';
            } else if($data->i_status==11) {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="kirimRfpPPC('.$data->id_rfp.')"><i class="mdi mdi-plus-circle-outline"></i> Kirim JO ke PPC</a>
                    </div>
                </div>';
            } else if($data->i_status==12){
                if(Auth::user()->role==99 || Auth::user()->role==2) {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="editRfp('.$data->id_rfp.')"><i class="mdi mdi-lead-pencil"></i> Edit JO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-printer"></i> Print JO</a>
                            <div class="dropdown-divider"></div>                
                            <a class="dropdown-item" style="cursor: pointer;" onclick="kirimRfpProd('.$data->id_rfp.')"><i class="mdi mdi-plus-circle-outline"></i> Kirim JO ke Manager Produksi</a>
                        </div>
                    </div>';
                } else {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                        </div>
                    </div>';
                }
            } else {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihat('.$data->i_id.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat SO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="printRfp('.$data->id_rfp.',\'rfp.index\')"><i class="mdi mdi-printer"></i> Print JO</a>
                    </div>
                </div>';
            }
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat == 1) {
                return 'Repeat';
            } else {
                return 'Tidak';
            }
        }) 
        ->editColumn('d_approved_pro',function($data) {
            if($data->d_approved_pro){
                return date('d-m-Y H:i:s',strtotime($data->d_approved_pro));
            }
        })
        ->editColumn('d_approved_mrk',function($data) {
            if($data->d_approved_mrk){
                return date('d-m-Y H:i:s',strtotime($data->d_approved_mrk));
            }
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data_so = \DB::select("SELECT sum(n_qty_kg) as qty_tot, ms.*, rp.e_nama_pel, rp.e_kont_pel
        FROM mst_so ms
        LEFT JOIN ref_pelanggan rp ON ms.i_pel = rp.i_pel
        JOIN mst_so_item msi ON msi.i_id_so = ms.i_id        
        WHERE ms.i_id='".$id."'
        GROUP BY ms.i_id, rp.e_nama_pel, rp.e_nama_pel, rp.e_kont_pel");        
        $data_so_item = \DB::select("SELECT a.*, b.e_kain FROM mst_so_item a
        LEFT JOIN ref_kain b ON cast(a.e_jenis_kain as integer)=b.i_id
        WHERE i_id_so='".$id."' ORDER BY a.i_id");
        $jenis_kain = \DB::table('ref_kain')->get();
        $jns_bahan = \DB::table('ref_jns_bahan')->get();
        $ori_kon1 = \DB::table('ref_ori_kondisi')->where('tipe','1')->get();
        $ori_kon2 = \DB::table('ref_ori_kondisi')->where('tipe','2')->get();
        $jns_printing = \DB::table('ref_jns_printing')->get();
        $warna_dasar = \DB::table('ref_warna_dasar')->get();
        $tekstur_akhir = \DB::table('ref_tekstur_akhir')->get();
        $id_so = $id;

        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.form',compact('reqAjax','data_so','id_so','jenis_kain','data_so_item','jns_bahan','ori_kon1','ori_kon2','jns_printing','warna_dasar','tekstur_akhir'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan(Request $request)
    {        
        // dd($request->all());exit();
        \DB::beginTransaction();
        $qId = \DB::select("SELECT i_id FROM rfp ORDER BY i_id DESC LIMIT 1");
        if(empty($qId)){
            $id_rfp = 1;
        } else {
            $id_rfp = $qId[0]->i_id+1;
        } 
        try{
            $rfp = new RFPModel();
            $rfp->i_id = $id_rfp;
            // sementara tidak dipakai karna penomoran akan didapat setelah approved mng prod
            $rfp->i_no_rfp = "JOSM-".$id_rfp;
            // masih error kalau tgl 30 & 31
            $rfp->d_selesai = date('Y-m-d', strtotime($request->input('d_selesai')));
            $rfp->i_pel = $request->input('i_pel');
            $rfp->e_contact_person = $request->input('contact_person');
            $rfp->f_repeat = $request->input('repeat_order')=='new'?false:true;
            $rfp->i_desain = $request->input('desain_kode');
            $rfp->e_motif = $request->input('motif_name');
            $rfp->e_material = $request->input('jns_bahan1');
            $rfp->e_original_cond1 = $request->input('ori_kondisi1');
            $rfp->e_original_cond2 = $request->input('ori_kondisi2');
            $rfp->e_ket_ori_cond = $request->input('ket_ori_kondisi');
            $rfp->e_color = $request->input('warna');
            $rfp->i_penyedia = $request->input('receive_id');
            $rfp->d_tgl_material_in = date('Y-m-d', strtotime($request->input('d_tgl_material')));
            $rfp->n_qty_material = str_replace(',','',$request->input('n_qty_material'));
            $rfp->n_pengkerutan = $request->input('pengkerutan');
            $rfp->e_gramasi_from = $request->input('gramasi_from');
            $rfp->e_gramasi_to = $request->input('gramasi_to');
            $rfp->e_penyesuaian_lebar_from = $request->input('penyesuaian_from');
            $rfp->e_penyesuaian_lebar_to = $request->input('penyesuaian_to');
            $rfp->e_pakan_from = $request->input('pakan_from');
            $rfp->e_pakan_to = $request->input('pakan_to');
            $rfp->e_lusi_from = $request->input('lusi_from');
            $rfp->e_lusi_to = $request->input('lusi_to');
            // $rfp->e_tekstur_akhir_from = $request->input('tekstur_from');
            $rfp->e_tekstur_akhir_to = $request->input('tekstur_to');
            // diubah sistem nyimpennya
            $rfp->e_cw_1 = $request->input('cw1')?$request->input('cw1'):null;
            $rfp->e_cw_2 = $request->input('cw2')?$request->input('cw2'):null;
            $rfp->e_cw_3 = $request->input('cw3')?$request->input('cw3'):null;
            $rfp->e_cw_4 = $request->input('cw4')?$request->input('cw4'):null;
            $rfp->e_cw_5 = $request->input('cw5')?$request->input('cw5'):null;
            $rfp->e_cw_6 = $request->input('cw6')?$request->input('cw6'):null;
            $rfp->e_cw_7 = $request->input('cw7')?$request->input('cw7'):null;
            $rfp->e_cw_8 = $request->input('cw8')?$request->input('cw8'):null;
            $rfp->e_ket_rfp = $request->input('ket_khusus');
            $rfp->e_material_others = $request->input('jns_bahan1')=='3'?$request->input('jns_bahan2'):null;
            $rfp->e_color_others = $request->input('warna')=='4'?$request->input('ket_warna'):null;
            // for($i=1; $i<=intval($request->input('tot_color_way')); $i++){
            //     $rfp->e_cw_.$i = $request->input('cw'.$i);                
            // }
            $rfp->i_status = 6;
            $rfp->i_id_so = $request->input('id_so');
            $rfp->save();

            for($i=1; $i<=$request->input('row_lpk'); $i++){
                $lpk = new RFPLPKModel();
                $lpk->i_id_rfp = $rfp->i_id;
                $lpk->e_nomor_lpk = $request->input('lpk'.$i);
                $lpk->qty_roll = $request->input('qty_lpk'.$i);
                $lpk->save();
            }

            $sales_order = SalesOrderModel::find($request->input('id_so'));
            $sales_order->i_status = 6;
            $sales_order->save();

            \DB::commit();
            return $this->sendResponse('1','Input JO No '.$request->input('no_rfp').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Input JO Gagal');
        }
    }

    public function kirim_approved_mkt($id)
    {
        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->i_status = 7;
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 7;
        $sales_order->save();

        return $this->sendResponse('1', 'Kirim Ke Manager Marketing Berhasil!');
    }

    public function list_approved_mkt()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.list_approved_mkt',compact('reqAjax'));
    }   

    public function data_approved_mkt()
    {
        $data = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, rfp.d_selesai, rp.e_nama_pel, rfp.f_repeat, ms.i_no_so, rfp.created_at, rfp.i_id_so
        FROM rfp 
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel = rp.i_pel
        JOIN mst_so ms ON rfp.i_id_so = ms.i_id
        WHERE rfp.i_status=7
        order by i_no_rfp desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" href="#" onclick="lihatRfp('.$data->i_id.',\'list_approved_mkt.rfp\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="approvedRfp('.$data->i_id.')"><i class="mdi mdi-check-circle-outline"></i> Approved JO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="rejectRfp('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Reject JO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="backToSO('.$data->i_id.','.$data->i_id_so.')"><i class="mdi mdi mdi-backup-restore"></i> Kembalikan ke SO</a>
                </div>
            </div>';
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat == 1) {
                return 'Repeat';
            } else {
                return 'Tidak';
            }
        }) 
        ->editColumn('created_at',function($data) {
           return date('d-m-Y H:i:s',strtotime($data->created_at));
        })
        ->editColumn('d_selesai',function($data) {
            if($data->d_selesai){
                return date('d-m-Y',strtotime($data->d_selesai));
            }
        })
        ->make(true);
    }

    public function approved_mkt($id)
    {
        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->e_reject_mkt = null;
        $rfp->i_status = 10;
        $rfp->d_approved_mrk = date('Y-m-d H:i:s');
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 10;
        $sales_order->save();

        return $this->sendResponse('1', 'Approve JO Berhasil!');
    }

    public function reject_mkt(Request $request,$id)
    {
        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->e_reject_mkt = $request->input('input_val');
        $rfp->i_status = 9;
        $rfp->d_approved_mrk = date('Y-m-d H:i:s');
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 9;
        $sales_order->save();

        return $this->sendResponse('1', 'Reject JO Berhasil!');
    }

    public function backToSO(Request $request)
    {
        \DB::beginTransaction();
        try{

            $rfp = RFPModel::find($request->input('id_rfp'));
            $rfp->i_status = 401;
            $rfp->save();
            
            $sales_order = SalesOrderModel::find($request->input('id_so'));
            $sales_order->i_status = 401;
            $sales_order->e_reject = $request->input('input_val');
            $sales_order->save();
            \DB::commit();
            return $this->sendResponse('1', 'Kembalikan JO ke SO Berhasil!');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2', 'Kembalikan JO ke SO Gagal!');
        }
    }

    public function kirim_approved_prod($id)
    {
        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->i_status = 10;
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 10;
        $sales_order->save();

        return $this->sendResponse('1', 'Kirim Ke Manager Produksi Berhasil!');
    }

    public function list_approved_prod()
    {
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.list_approved_prod',compact('reqAjax'));
    }   

    public function data_approved_prod()
    {
        $data = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, rfp.d_selesai, rp.e_nama_pel, rfp.f_repeat, ms.i_no_so, rfp.created_at 
        FROM rfp 
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel = rp.i_pel
        JOIN mst_so ms ON rfp.i_id_so = ms.i_id
        WHERE rfp.i_status=10
        order by i_no_rfp desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            return '<div class="dropdown">
                <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                <i class="mdi mdi-apps"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">
                    <a class="dropdown-item" href="#" onclick="lihatRfp('.$data->i_id.',\'list_approved_prod.rfp\')"><i class="mdi mdi-clipboard-check"></i> Lihat JO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="approvedRfpP('.$data->i_id.')"><i class="mdi mdi-check-circle-outline"></i> Approved JO</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="rejectRfpP('.$data->i_id.')"><i class="mdi mdi-close-circle-outline"></i> Reject JO</a>
                </div>
            </div>';
        })
        ->editColumn('f_repeat', function($data) {
            if($data->f_repeat == 1) {
                return 'Repeat';
            } else {
                return 'Tidak';
            }
        })         
        ->editColumn('created_at',function($data) {
            return date('d-m-Y H:i:s',strtotime($data->created_at));
         })
         ->editColumn('d_selesai',function($data) {
             if($data->d_selesai){
                 return date('d-m-Y',strtotime($data->d_selesai));
             }
         })
        ->make(true);
    }

    public function approved_prod($id)
    {
        $qNoRFP = collect(\DB::select("SELECT no_urut, bln, thn FROM ref_no_urut WHERE code='JO' AND thn='".date('Y')."'"))->first();
        $no_so = \DB::select("SELECT RIGHT(i_no_so,5) as no_so, flag_so FROM rfp a JOIN mst_so b ON b.i_id=a.i_id_so WHERE a.i_id = '".$id."' ");
        if(!empty($qNoRFP)){
            // $nomorSO = explode('/',$sales_order->i_no_so);            
            $noUrutRFP = $qNoRFP->no_urut+1;
            $noUrutRFPNew = str_pad($noUrutRFP,5,"0",STR_PAD_LEFT);
            \DB::table('ref_no_urut')
            ->where('code', 'JO')            
            ->where('thn', date('Y'))
            ->update(array('no_urut' => $noUrutRFPNew));
            $nomorRFP = "JO-".$no_so[0]->flag_so.'-'.$no_so[0]->no_so."-".$noUrutRFPNew;
        } else {
            \DB::table('ref_no_urut')
            ->insert([
                'code' => 'JO', 
                'no_urut' => '00001',                
                'thn' => date('Y')
            ]);
            $nomorRFP = "JO-".$no_so[0]->flag_so.'-'.$no_so[0]->no_so."-00001";
        }


        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->i_no_rfp = $nomorRFP;
        $rfp->e_reject_prod = null;
        $rfp->i_status = 13;
        $rfp->d_approved_pro = date('Y-m-d H:i:s');
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 13;
        $sales_order->save();

        return $this->sendResponse('1', 'Approve JO Berhasil!');
    }

    public function reject_prod(Request $request,$id)
    {
        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->e_reject_prod = $request->input('input_val');
        $rfp->i_status = 12;
        $rfp->d_approved_pro = date('Y-m-d H:i:s');
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 12;
        $sales_order->save();

        return $this->sendResponse('1', 'Reject JO Berhasil!');
    }

    public function lihat($id,$link)
    {
        /* query untuk hitung total cw dalam satu row
        (
            select count(*)
            from (values (rfp.e_cw_1), (rfp.e_cw_2), (rfp.e_cw_3), (rfp.e_cw_4), (rfp.e_cw_5), (rfp.e_cw_6), (rfp.e_cw_7), (rfp.e_cw_8)) as v(col)
            where v.col is not null
        ) as hitungcw */
        $param['datahead'] = \DB::select("SELECT rfp.*, rp.e_nama_pel, rp.e_kont_pel, rp2.e_nama_pel as penyedia, ms.d_approval_strike_off, ms.e_jenis_printing 
                    FROM rfp
                    JOIN mst_so ms ON rfp.i_id_so=ms.i_id
                    LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
                    LEFT JOIN ref_pelanggan rp2 ON rfp.i_penyedia=rp2.i_pel
                    WHERE rfp.i_id='".$id."'");
        $param['dataitem'] = \DB::select("SELECT b.i_id as id_cw, b.e_uraian_pekerjaan, b.n_qty_kg,
        b.d_strike_off, b.e_jenis_kain, a.i_id as id_rfp, c.e_kain 
                    FROM rfp a
                    JOIN mst_so_item b ON a.i_id_so=b.i_id_so
                    LEFT JOIN ref_kain c ON cast(b.e_jenis_kain as integer)=c.i_id
                    WHERE a.i_id='".$id."'
                    ORDER BY b.i_id");
        $param['datalpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id."'");
        // $jenis_kain = \DB::table('ref_kain')->get();        
        $jns_bahan = \DB::table('ref_jns_bahan')->get();
        $ori_kon1 = \DB::table('ref_ori_kondisi')->where('tipe','1')->get();
        $ori_kon2 = \DB::table('ref_ori_kondisi')->where('tipe','2')->get();
        $jns_printing = \DB::table('ref_jns_printing')->get();
        $warna_dasar = \DB::table('ref_warna_dasar')->get();
        $tekstur_akhir = \DB::table('ref_tekstur_akhir')->get();
        $rut = $link;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.form_lihat',compact('reqAjax','param','rut','jns_bahan','ori_kon1','ori_kon2','jns_printing','warna_dasar','tekstur_akhir'));
    }

    public function print_rfp($id,$link)
    {
        $param['datahead'] = \DB::select("SELECT rfp.*, rp.e_nama_pel, rp.e_kont_pel, rp2.e_nama_pel as penyedia, ms.d_approval_strike_off, ms.e_jenis_printing,
        a.e_jns_bahan, b.e_ori_kondisi as ori_cond1, c.e_ori_kondisi as ori_cond2, d.e_jns_printing as nama_printing,
        e.e_warna_dasar, f.e_tekstur
        FROM rfp
        JOIN mst_so ms ON rfp.i_id_so=ms.i_id
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
        LEFT JOIN ref_pelanggan rp2 ON rfp.i_penyedia=rp2.i_pel
        LEFT JOIN ref_jns_bahan a ON cast(rfp.e_material as integer)=a.i_id
        LEFT JOIN ref_ori_kondisi b ON cast(rfp.e_original_cond1 as integer)=b.i_id
        LEFT JOIN ref_ori_kondisi c ON cast(rfp.e_original_cond2 as integer)=c.i_id
        LEFT JOIN ref_jns_printing d ON cast(ms.e_jenis_printing as integer)=d.i_id
        LEFT JOIN ref_warna_dasar e ON cast(rfp.e_color as integer)=e.i_id
        LEFT JOIN ref_tekstur_akhir f ON cast(rfp.e_tekstur_akhir_to as integer)=f.i_id
        WHERE rfp.i_id='".$id."'");
        $param['dataitem'] = \DB::select("SELECT b.i_id as id_cw, b.e_uraian_pekerjaan, b.n_qty_kg,
        b.d_strike_off, b.e_jenis_kain, a.i_id as id_rfp, c.e_kain 
                    FROM rfp a
                    JOIN mst_so_item b ON a.i_id_so=b.i_id_so
                    LEFT JOIN ref_kain c ON cast(b.e_jenis_kain as integer)=c.i_id
                    WHERE a.i_id='".$id."'
                    ORDER BY b.i_id");
        $param['datalpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id."'");
        $rut = $link;
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.print_rfp',compact('reqAjax','param','rut'));
    }

    public function kirim_ke_ppc($id)
    {
        $rfp = RFPModel::find($id);
        $id_so = $rfp->i_id_so;
        $rfp->i_status = 13;
        $rfp->save();

        $sales_order = SalesOrderModel::find($id_so);
        $sales_order->i_status = 13;
        $sales_order->save();

        return $this->sendResponse('1', 'Kirim Ke PPC Berhasil!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_rfp($id)
    {
        $data_rfp = \DB::select("SELECT rfp.*, rp.e_nama_pel, rp.e_kont_pel, rp2.e_nama_pel as penyedia, ms.d_approval_strike_off, ms.e_jenis_printing 
        FROM rfp
        JOIN mst_so ms ON rfp.i_id_so=ms.i_id
        LEFT JOIN ref_pelanggan rp ON rfp.i_pel=rp.i_pel
        LEFT JOIN ref_pelanggan rp2 ON rfp.i_penyedia=rp2.i_pel
        WHERE rfp.i_id='".$id."'");
        $param['dataitem'] = \DB::select("SELECT b.i_id as id_cw, b.e_uraian_pekerjaan, b.n_qty_kg,
        b.d_strike_off, b.e_jenis_kain, a.i_id as id_rfp, c.e_kain 
                FROM rfp a
                JOIN mst_so_item b ON a.i_id_so=b.i_id_so
                LEFT JOIN ref_kain c ON cast(b.e_jenis_kain as integer)=c.i_id
                WHERE a.i_id='".$id."'
                ORDER BY b.i_id");        
        $param['datalpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$id."'");        
        $jns_bahan = \DB::table('ref_jns_bahan')->get();
        $ori_kon1 = \DB::table('ref_ori_kondisi')->where('tipe','1')->get();
        $ori_kon2 = \DB::table('ref_ori_kondisi')->where('tipe','2')->get();
        $jns_printing = \DB::table('ref_jns_printing')->get();
        $warna_dasar = \DB::table('ref_warna_dasar')->get();
        $tekstur_akhir = \DB::table('ref_tekstur_akhir')->get();
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('reqfp.form_edit',compact('reqAjax','data_rfp','param','jns_bahan','ori_kon1','ori_kon2','jns_printing','warna_dasar','tekstur_akhir'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        \DB::beginTransaction();        
        try{
            $rfp = RFPModel::find($request->input('id_rfp'));
            // masih error kalau tgl 30 & 31
            $rfp->d_selesai = date('Y-m-d', strtotime($request->input('d_selesai')));
            $rfp->e_contact_person = $request->input('contact_person');
            $rfp->e_material = $request->input('jns_bahan1');
            $rfp->e_material_others = $request->input('jns_bahan1')=='3'?$request->input('jns_bahan2'):null;
            $rfp->e_original_cond1 = $request->input('ori_kondisi1');
            $rfp->e_original_cond2 = $request->input('ori_kondisi2');
            $rfp->e_ket_ori_cond = $request->input('ket_ori_kondisi');
            $rfp->e_color = $request->input('warna');
            $rfp->e_color_others = $request->input('warna')=='4'?$request->input('ket_warna'):null;
            $rfp->d_tgl_material_in = date('Y-m-d', strtotime($request->input('d_tgl_material')));
            $rfp->n_qty_material = $request->input('n_qty_material');
            // $rfp->n_pengkerutan = $request->input('pengkerutan');
            // $rfp->e_benang_from = $request->input('benang_from');
            // $rfp->e_benang_to = $request->input('benang_to');
            // $rfp->e_penyesuaian_lebar_from = $request->input('penyesuaian_from');
            // $rfp->e_penyesuaian_lebar_to = $request->input('penyesuaian_to');
            // $rfp->e_kondisi_asal_from = $request->input('kon_asal_from');
            // $rfp->e_kondisi_asal_to = $request->input('kon_asal_to');
            // $rfp->e_warna_kain_from = $request->input('wrn_kain_from');
            // $rfp->e_warna_kain_to = $request->input('wrn_kain_to');
            // $rfp->e_tekstur_akhir_from = $request->input('tekstur_from');
            $rfp->e_gramasi_from = $request->input('gramasi_from');
            $rfp->e_gramasi_to = $request->input('gramasi_to');
            $rfp->e_penyesuaian_lebar_from = $request->input('penyesuaian_from');
            $rfp->e_penyesuaian_lebar_to = $request->input('penyesuaian_to');
            $rfp->e_pakan_from = $request->input('pakan_from');
            $rfp->e_pakan_to = $request->input('pakan_to');
            $rfp->e_lusi_from = $request->input('lusi_from');
            $rfp->e_lusi_to = $request->input('lusi_to');
            $rfp->e_tekstur_akhir_to = $request->input('tekstur_to');
            $rfp->e_ket_rfp = $request->input('ket_khusus');
            $rfp->save();            
            
            RFPLPKModel::where('i_id_rfp',$request->input('id_rfp'))->delete();
            for($i=1; $i<=$request->input('row_lpk'); $i++){
                $lpkNew = new RFPLPKModel();
                $lpkNew->i_id_rfp = $request->input('id_rfp');
                $lpkNew->e_nomor_lpk = $request->input('lpk'.$i);
                $lpkNew->qty_roll = $request->input('qty_lpk'.$i);
                $lpkNew->save();                                  
            }

            \DB::commit();
            return $this->sendResponse('1','Edit JO No '.$request->input('no_rfp').' berhasil');
        }catch(Exception $e){
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Edit JO Gagal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
