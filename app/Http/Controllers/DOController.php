<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Request as Req;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class DOController extends Controller
{
    //
    public function index()
    {
        $param['pel'] = \DB::select("SELECT * FROM ref_pelanggan WHERE i_pel<>0");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";    
        return view('do.index',compact('reqAjax','param'));
    }

    public function data_sj()
    {
        $data = \DB::select("SELECT (ROW_NUMBER () OVER (ORDER BY mdo.i_id)) as urutan, mdo.*, rp.e_nama_pel, rw.definition FROM mst_do_sj mdo 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel 
        LEFT JOIN ref_workflow rw ON rw.i_id=mdo.i_status
        ORDER BY mdo.i_id desc");
        return DataTables::of($data)
        ->addColumn('action', function ($data) {
            if($data->i_status=='' || $data->i_status==18){
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-clipboard-check"></i> Lihat SJ</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="editSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-pencil"></i> Edit SJ</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" style="cursor: pointer;" onclick="requestPrintSJ('.$data->i_id.',\''.$data->i_no_sj.'\')"><i class="mdi mdi-file-send"></i> Request Print SJ</a>
                    </div>
                </div>';
            } else if($data->i_status==16) {
                return '<div class="dropdown">
                    <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                    <i class="mdi mdi-apps"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                        <a class="dropdown-item" style="cursor: pointer;" onclick="lihatSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-clipboard-check"></i> Lihat SJ</a>                        
                    </div>
                </div>';
            } else {
                if($data->f_nota){
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-clipboard-check"></i> Lihat SJ</a>
                            <div class="dropdown-divider"></div>                            
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-printer"></i> Print SJ</a>
                        </div>
                    </div>';
                } else {
                    return '<div class="dropdown">
                        <button type="button" class="btn btn-info dropdown-toggle" id="dropdownMenuIconButton8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                        <i class="mdi mdi-apps"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton8" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 38px, 0px);">                    
                            <a class="dropdown-item" style="cursor: pointer;" onclick="lihatSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-clipboard-check"></i> Lihat SJ</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="editSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-pencil"></i> Edit SJ</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" style="cursor: pointer;" onclick="printSJ('.$data->i_id.','.$data->i_id_kartu.','.$data->i_id_rfp.')"><i class="mdi mdi-printer"></i> Print SJ</a>
                        </div>
                    </div>';
                }               
            }            
        })
        ->editColumn('d_sj',function($data){
            return date('d-m-Y',strtotime($data->d_sj));
        })
        ->make(true);
    }

    public function list($pel)
    {                
        $data = \DB::select("SELECT mk.i_no_kartu, mk.i_id as id_kartu, rfp.i_id as id_rfp, rfp.i_no_rfp, ms.i_no_so 
        FROM mst_kartu mk 
        LEFT JOIN rfp ON rfp.i_id=mk.i_id_rfp 
        LEFT JOIN mst_so ms ON rfp.i_id_so=ms.i_id
        WHERE rfp.i_pel='".$pel."' AND mk.d_beres isnull
        ORDER BY mk.i_id desc");
        return json_encode($data);
    }

    public function create_do(Request $request)
    {
        $idkartu = $request->get('idkartu');
        $idrfp = $request->get('idrfp');
        $param['idkartu'] = $idkartu;
        $param['idrfp'] = $idrfp;
        $param['data'] = \DB::select("SELECT mk.i_no_kartu, rfp.i_no_rfp, rfp.i_id, ms.i_no_so, ms.i_id as id_so,
            ms.i_desain, ms.e_motif, ms.e_jenis_printing, ms.i_no_po, rfp.i_pel, rjp.e_jns_printing as nama_printing,
            sum(mkc.n_qty_roll) as roll_pem, sum(mkc.n_qty_pjg) as mtr_pem, sum(mkc.n_qty_kg) as kg_pem,
            count(mkc.i_id) as total_cw, tw.d_tgl as tgl_produksi, rp.e_nama_pel, rp.e_alamat_pel, rp.n_jth_tempo
        FROM mst_kartu mk
        JOIN mst_so ms ON ms.i_id=mk.i_id_so
        JOIN rfp ON rfp.i_id=mk.i_id_rfp
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mk.i_id        
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=ms.i_pel AND rfp.i_pel=rp.i_pel
        LEFT JOIN ref_jns_printing rjp ON rjp.i_id=cast(ms.e_jenis_printing as integer)
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=6
        group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=mk.i_id_rfp
        WHERE mk.i_id='".$idkartu."'
        GROUP BY mk.i_no_kartu, rfp.i_no_rfp, rfp.i_id, ms.i_no_so, ms.i_id, tw.d_tgl, rp.e_nama_pel,
            rp.e_alamat_pel, rp.n_jth_tempo, rfp.i_pel, rjp.e_jns_printing");
        $param['idsj'] = '';
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$idrfp."'");
        $param['exsj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['packing'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw, rk.e_kain FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so LEFT JOIN ref_kain rk ON cast(msi.e_jenis_kain as integer)=rk.i_id WHERE rfp.i_id='".$idrfp."'");
        $cek_no = \DB::select("SELECT * FROM ref_no_urut WHERE code='SJ' AND thn='".date('Y')."'");
        if($cek_no){
            $no = $cek_no[0]->no_urut+1;
            $no = str_pad($no,5,"0",STR_PAD_LEFT); 
            $param['no'] = 'SJ-'.$no;
        } else {
            $param['no'] = 'SJ-00001';
        }
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('do.form',compact('reqAjax','param'));
    }

    public function lihat_do(Request $request)
    {
        $idkartu = $request->get('idkartu');
        $idrfp = $request->get('idrfp');
        $param['data'] = \DB::select("SELECT mdo.*, rp.e_nama_pel, rp.e_alamat_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, sum(mkc.n_qty_roll) as roll_pem, sum(mkc.n_qty_pjg) as mtr_pem, sum(mkc.n_qty_kg) as kg_pem,
        tw.d_tgl as tgl_produksi, ms.i_no_so, ms.i_no_po, rjp.e_jns_printing as nama_printing
        FROM mst_do_sj mdo 
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel        
        JOIN rfp ON rfp.i_id=mdo.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so        
        LEFT JOIN ref_jns_printing rjp ON rjp.i_id=cast(ms.e_jenis_printing as integer)
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mdo.i_id_kartu
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=6
        group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=rfp.i_id
        WHERE mdo.i_id='".$request->get('id')."'
        GROUP BY mdo.i_id, rp.e_nama_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, tw.d_tgl,
            ms.i_no_so, ms.i_no_po, rp.e_alamat_pel, rjp.e_jns_printing
        ORDER BY mdo.i_id");
        $param['idsj'] = $request->get('id');
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$idrfp."'");
        $param['exsj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['packing'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$idrfp."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('do.form_lihat', compact('reqAjax','param'));
    }

    public function print_do(Request $request)
    {
        $idsj = $request->get('id');
        $param['data'] = \DB::select("SELECT mdo.*, ms.i_no_so, ms.i_desain, ms.e_motif, ms.e_jenis_printing, ms.i_no_po, 
            rp.e_nama_pel, rp.e_alamat_pel, sum(mkc.n_qty_roll) as roll_pem, sum(mkc.n_qty_pjg) as mtr_pem, 
            sum(mkc.n_qty_kg) as kg_pem, rjp.e_jns_printing as nama_printing, ms.i_penyedia
        FROM mst_do_sj mdo
        JOIN rfp ON rfp.i_id=mdo.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mdo.i_id_kartu        
        LEFT JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel AND rfp.i_pel=rp.i_pel
        LEFT JOIN ref_jns_printing rjp ON rjp.i_id=cast(ms.e_jenis_printing as integer)
        WHERE mdo.i_id=:idsj
        GROUP BY mdo.i_id, ms.i_no_so, ms.i_desain, ms.e_motif, ms.e_jenis_printing, rjp.e_jns_printing,
            ms.i_no_po, rp.e_nama_pel, rp.e_alamat_pel, ms.i_penyedia",['idsj' => $idsj]);
        $param['idsj'] = $request->get('id');
        $idkartu = $request->get('idkartu');
        $idrfp = $request->get('idrfp');
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp=:idrfp",['idrfp'=>$idrfp]);
        $param['exsj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu=:idkartu",['idkartu'=>$idkartu]);
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu=:idkartu",['idkartu'=>$idkartu]);
        $param['packing'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id=:idrfp",['idrfp'=>$idrfp]);
        $param['asal_sj'] = $request->get('asalsj');
        $param['asal_kp'] = $request->get('asalkp');
        $param['jadi_kp'] = $request->get('jadikp');        
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('do.print',compact('reqAjax','param'));
    }

    public function cek_qc($id)
    {
        $data = \DB::select("SELECT * FROM mst_packing_list WHERE i_id_rfp='".$id."'");
        if(!empty($data)){
            $tot = \DB::select("SELECT sum(n_asal_kp) as asal_kp, sum(n_jadi_kp) as jadi_kp FROM mst_packing_list WHERE i_id_rfp='".$id."' AND i_id_sj isnull");
            if($tot[0]->asal_kp==0 || $tot[0]->jadi_kp==0){
                return $this->sendResponse('2','nilai keseluruhan asal kp atau jadi kp memiliki nilai 0 pada Packing List RFP yang dipilih!, Tanyakan ke bagian QC');
            } else {
                return $this->sendResponse('1','Lanjutkan');
            }            
        } else {
            return $this->sendResponse('2','Lakukan Proses Input Persiapan terlebih dahulu!, Tanyakan ke bagian persiapan');
        }
    }

    public function simpan(Request $request)
    {        
        \DB::beginTransaction();
        try{
            if($request->input('id_sj')){
                \DB::table('mst_do_sj')
                    ->where('i_id', $request->input('id_sj'))
                    ->update([                                                                        
                        'd_sj' => date('Y-m-d', strtotime($request->input('tgl_sj'))),                        
                        'd_due_date' => date('Y-m-d', strtotime($request->input('tgl_jth_tempo'))),                        
                        'e_ket' => $request->input('keterangan'),
                        'n_tot_roll' => $request->input('tot_roll'),
                        'n_tot_asal_sj' => $request->input('tot_asal_sj'),
                        'n_tot_asal_kp' => $request->input('tot_asal_kp'),
                        'n_tot_jadi_kp' => $request->input('tot_jadi_kp'),                        
                        'expedisi' => $request->input('expedisi'),                        
                        'updated_at' => date('Y-m-d H:i:s')                        
                    ]);

                    for($i=1; $i<=$request->input('totalcw'); $i++){                
                        $huruf = $this->toNum($i-1);
                        $i_cw = $request->input('id_cw'.$i);
                        for($j=1; $j<=$request->input('jml'.$i); $j++){                    
                            $kode = $request->input('kode_dyeing'.$huruf.$j);
                            for($z=1; $z<=$request->input('count_list'.$kode); $z++){
                                if($request->input('pilih'.$kode.'-'.$z)=='y'){                        
                                    \DB::table('mst_packing_list')
                                        ->where('i_id', $request->input('id_roll_pack'.$kode.'-'.$z))
                                        ->update([
                                            'f_jadi_sj' => true,
                                            'i_id_sj' => $request->input('id_sj'),
                                            'updated_at' => date('Y-m-d H:i:s')
                                        ]);                                    
                                } else {
                                    \DB::table('mst_packing_list')
                                        ->where('i_id', $request->input('id_roll_pack'.$kode.'-'.$z))
                                        ->update([
                                            'f_jadi_sj' => false,
                                            'i_id_sj' => null,
                                            'updated_at' => date('Y-m-d H:i:s')
                                        ]);    
                                }              
                            }
                        }
                    }  
                \DB::commit();
                return $this->sendResponse('1','Berhasil Update Data SJ No '.$request->input('no_sj')); 
            } else {
                $qId = \DB::select("SELECT i_id FROM mst_do_sj ORDER BY i_id DESC LIMIT 1");
                if($qId){
                    $idsj = $qId[0]->i_id+1;
                } else {
                    $idsj = 1;
                }
                $tagihan=0;
                for($i=1; $i<=$request->input('totalcw'); $i++){
                    $tagihan += $request->input('hrg_cw'.$i) * $request->input('tot_cw'.$i);
                }
                \DB::table('mst_do_sj')
                    ->insert([
                        'i_id' => $idsj,
                        'i_id_kartu' => $request->input('idkartu'),
                        'i_id_rfp' => $request->input('idrfp'),
                        'i_no_sj' => $request->input('no_sj'),
                        'd_sj' => date('Y-m-d', strtotime($request->input('tgl_sj'))),
                        'i_pel' => $request->input('idpel'),
                        'd_due_date' => date('Y-m-d', strtotime($request->input('tgl_jth_tempo'))),
                        'n_total_cw' => $request->input('totalcw'),
                        'e_ket' => $request->input('keterangan'),
                        'n_tot_roll' => $request->input('tot_roll'),
                        'n_tot_asal_sj' => $request->input('tot_asal_sj'),
                        'n_tot_asal_kp' => $request->input('tot_asal_kp'),
                        'n_tot_jadi_kp' => $request->input('tot_jadi_kp'),
                        'n_total_tagihan' => round($tagihan),
                        'n_sisa_tagihan' => round($tagihan),
                        'expedisi' => $request->input('expedisi'),
                        'f_print' => false,
                        'f_lunas' => false,
                        'created_at' => date('Y-m-d H:i:s'),
                        'tgl_produksi' => date('Y-m-d', strtotime($request->input('tgl_produksi'))),
                        'i_id_so' => $request->input('idso')
                    ]);
                
                \DB::table('mst_kartu')
                    ->where('i_id', $request->input('idkartu'))
                    ->update(['tgl_produksi' => $request->input('tgl_produksi')]);

                for($i=1; $i<=$request->input('totalcw'); $i++){                
                    $huruf = $this->toNum($i-1);
                    $i_cw = $request->input('id_cw'.$i);
                    for($j=1; $j<=$request->input('jml'.$i); $j++){                    
                        $kode = $request->input('kode_dyeing'.$huruf.$j);
                        for($z=1; $z<=$request->input('count_list'.$kode); $z++){
                            if($request->input('pilih'.$kode.'-'.$z)=='y'){                        
                                \DB::table('mst_packing_list')
                                    ->where('i_id', $request->input('id_roll_pack'.$kode.'-'.$z))
                                    ->update([
                                        'f_jadi_sj' => true,
                                        'i_id_sj' => $idsj,
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ]);                                    
                            }              
                        }
                    }
                }  
                
                $cek_no = \DB::select("SELECT * FROM ref_no_urut WHERE code='SJ' AND thn='".date('Y')."'");
                if($cek_no){
                    $no = $cek_no[0]->no_urut+1;
                    $no = str_pad($no,5,"0",STR_PAD_LEFT);
                    \DB::table('ref_no_urut')
                        ->where('code','SJ')
                        ->update(['no_urut' => $no]);
                } else {
                    \DB::table('ref_no_urut')
                        ->where('code','SJ')
                        ->insert([
                            'code' => 'SJ',
                            'no_urut' => '00001',
                            'thn' => date('Y')
                        ]);
                }
                \DB::commit();
                return $this->sendResponse('1','Berhasil Simpan Data SJ No '.$request->input('no_sj'));   
            }
        }catch(Exception $e){
            \DB::rollback();
            if($request->input('id_sj')){
                return $this->sendResponse('2','Gagal Update Data SJ');
            } else {
                return $this->sendResponse('2','Gagal Simpan Data SJ');
            }
        }
    }

    function toNum($data) 
    {
        $alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
        return $alpha[$data];
    }

    function update_print($id) 
    {        
        \DB::beginTransaction();
        try{
            \DB::table('mst_do_sj')
                ->where('i_id', $id)
                ->update(['f_print' => true, 'updated_at' => date('Y-m-d H:i:s')]);
            \DB::commit();
            return $this->sendResponse('1','Lanjutkan');
        } catch(Exception $e) {
            \DB::rollback();
            return $this->sendResponse('2','Terjadi Kesalahan, Error Print SJ, Coba Refresh Halaman');
        }
    }

    function cek_print_sj($id) 
    {
        $data = \DB::select("SELECT f_print FROM mst_do_sj WHERE i_id='".$id."'");
        if($data[0]->f_print=='t'){
            return $this->sendResponse('2','Data tidak boleh di edit karena sudah di Print untuk dikirim ke pelanggan!');
        } else {
            return $this->sendResponse('1','Lanjutkan');
        }
    }

    function edit_do(Request $request)
    {
        $idkartu = $request->get('idkartu');
        $idrfp = $request->get('idrfp');
        $param['idkartu'] = $idkartu;
        $param['idrfp'] = $idrfp;
        $param['data'] = \DB::select("SELECT mdo.*, rp.e_nama_pel, rp.e_alamat_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, sum(mkc.n_qty_roll) as roll_pem, sum(mkc.n_qty_pjg) as mtr_pem, sum(mkc.n_qty_kg) as kg_pem,
        tw.d_tgl as tgl_produksi, ms.i_no_so, ms.i_no_po, rp.n_jth_tempo
        FROM mst_do_sj mdo 
        JOIN ref_pelanggan rp ON rp.i_pel=mdo.i_pel
        JOIN rfp ON rfp.i_id=mdo.i_id_rfp
        JOIN mst_so ms ON ms.i_id=rfp.i_id_so        
        JOIN mst_kartu_cw mkc ON mkc.i_id_kartu=mdo.i_id_kartu
        JOIN (select x.i_id_rfp, x.i_id_bagian, min(x.i_cw) as cw, min(x.d_tgl) as d_tgl
        from (select i_id_rfp, i_id_bagian,min(i_cw) as i_cw, min(d_tgl) as d_tgl 
            from tx_workstation group by i_id_rfp,i_id_bagian, i_cw, d_tgl order by i_cw) as x
        where x.i_id_bagian=6
        group by x.i_id_rfp, x.i_id_bagian) as tw ON tw.i_id_rfp=rfp.i_id
        WHERE mdo.i_id='".$request->get('id')."'
        GROUP BY mdo.i_id, rp.e_nama_pel, rfp.i_desain, rfp.e_motif, ms.e_jenis_printing, tw.d_tgl,
            ms.i_no_so, ms.i_no_po, rp.e_alamat_pel, rp.n_jth_tempo
        ORDER BY mdo.i_id");
        $param['idsj'] = $request->get('id');
        $param['lpk'] = \DB::select("SELECT * FROM rfp_lpk WHERE i_id_rfp='".$idrfp."'");
        $param['exsj'] = \DB::select("SELECT * FROM tx_sj_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['jp'] = \DB::select("SELECT * FROM tx_jns_proses_kartu WHERE i_id_kartu='".$idkartu."'");
        $param['packing'] = \DB::select("SELECT rfp.i_id, rfp.i_no_rfp, msi.e_uraian_pekerjaan, msi.i_id as id_cw FROM mst_so_item msi JOIN rfp ON msi.i_id_so=rfp.i_id_so WHERE rfp.i_id='".$idrfp."'");
        $reqAjax = (Req::ajax()) ? "ajax" : "normal";
        return view('do.form_edit', compact('reqAjax','param'));
    }

    public function request_print($id)
    {
        \DB::beginTransaction();
        try{
            \DB::table('mst_do_sj')->where('i_id', $id)->update(['i_status' => 16]);
            \DB::commit();
            return $this->sendResponse('1', 'Berhasil Request Print SJ!');   
        } catch(Exception $e) {
            \DB::rollbakc();
            return $this->sendResponse('2', 'Gagal Request Print SJ!');   
        }
        
    }
}
