<?php
namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
class ExportExcel implements FromView, ShouldAutoSize
{
    public function __construct($view,$module,$data){
        $this->view = $view;
        $this->module = $module;
        $this->data = $data;
    }
    use Exportable;
    public function view(): View
    {
        // dd($this->module);exit();
        return view($this->view, [
            'module' => $this->module
        ]);
    }

    /* public function sheets(): array
    {
        $sheets = [];

        for ($month = 1; $month <= 12; $month++) {
            $sheets[] = new ExportExcel($this->view, $this->module, $this->data);
        }

        return $sheets;
    } */
}