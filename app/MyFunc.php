<?php

namespace App;
use Excel;
use Auth;
use Request;
use App\ExportExcel;
use Illuminate\Database\Eloquent\Model;

class MyFunc extends Model
{
    public function __construct()
    {
        $this->middleware('auth');   
    }

    public static function printto($module,$judul,$fields,$perpage=20,$view=false,$arah='portrait',$kertas="a4"){
		return MyFunc::excel($module,$judul,$fields,$view['excel']);
	}
	

	public static function excel($module,$judul,$fields,$view='layouts.excel'){
		$title=$judul.'.xlsx';			
		$data=['module'=>$module,'title'=>$title,'fields'=>$fields,'view'=>$view];		
		return Excel::download(new ExportExcel($data['view'],$data['module'],$data), $title);
	}
}
