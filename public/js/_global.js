var base_url = '//' + window.location.host + "/kharisma_printex";
var sidebar = $('.sidebar');

function addActiveClass(element, current, page) {
    if (current === "") {
        //for root url
        if (current == page) {
            element.parents('.nav-item').last().addClass('active');
            if (element.parents('.sub-menu').length) {
                element.closest('.collapse').addClass('show');
                element.addClass('active');
            }
        }
    } else {
        //for other url
        if (current == page) {
            element.parents('.nav-item').last().addClass('active');
            if (element.parents('.sub-menu').length) {
                element.closest('.collapse').addClass('show');
                element.addClass('active');
            }
            if (element.parents('.submenu-item').length) {
                element.addClass('active');
            }
        }
    }
}

$(window).on('popstate', function(ev) {
    if (window.history.state != null) {
        loadNewPage(window.history.state.url);
    }
});

function loadNewPage(page) {
    $.ajax({
        type: "GET",
        url: page,
        data: {},
        beforeSend: function() {
            // animateCSS('#pageLoad', 'fadeOutDown');
            $('#pageLoad').fadeOut('slow');
            // loadingPage();            
            $('#loading').show();
        },
        success: function(data) {
            // $('#pageLoad').html(data).fadeIn();
        },
    }).done(function(data) {
        // endLoadingPage();
        countPosisi();
        $('#loading').hide();
        var state = { name: "name", page: 'History', url: page };
        window.history.replaceState(state, "History", page);

        $('#pageLoad').html(data).fadeIn('slow');
    }).fail(function(xhr, textStatus, errorThrowns) {
        $('#loading').hide();
        swal('Terjadi Kesalahan / Koneksi ke server melambat, Silahkan refresh halaman!');
    });;
    // $("#pageLoad").load(page);
}

function countPosisi() {
    $.ajax({
        type: 'GET',
        url: base_url + '/select/count_posisi',
        success: function(res) {
            //var data = $.parseJSON(res);                
        }
    }).done(function(res) {
        var data = $.parseJSON(res);
        /* $.each(data, function(k, v) {
            console.log(v)
            $('#so_baru').text(v.so_baru);
            $('#approved_so').text(v.approved_so);
            $('#reject_so').text(v.rejected_so);
            $('#rfp').text(v.rfp);
            $('#approved_rfp_mkt').text(v.approved_rfp_mkt);
            $('#rejected_rfp_mkt').text(v.rejected_rfp_mkt);
            $('#approved_rfp_pro').text(v.approved_rfp_prod);
            $('#reject_rfp_pro').text(v.rejected_rfp_prod);
            $('#kartu_produksi').text(v.kartu_produksi);
            $('#selesai').text(v.selesai);
        }); */
        $.each(data.countPos, function(k, v) {
            $('#so_baru').text(v.so_baru);
            $('#so_outs').text(v.so_outs);
            $('#kirim_rfp').text(v.kirim_rfp);
            $('#reject_so').text(v.rejected_so);
            $('#rfp').text(v.rfp);
            $('#approved_rfp_mkt').text(v.approved_rfp_mkt);
            $('#rejected_rfp_mkt').text(v.rejected_rfp_mkt);
            $('#approved_rfp_pro').text(v.approved_rfp_prod);
            $('#reject_rfp_pro').text(v.rejected_rfp_prod);
            $('#kartu_produksi').text(v.kartu_produksi);
            $('#selesai').text(v.selesai);
        });
        $('#req_print_sj').text(data.countReqPSJ[0].req_print_sj);
    }).fail(function(xhr, textStatus, errorThrowns) {
        $('#loading').hide();
        swal('Terjadi Kesalahan / Koneksi ke server melambat, Silahkan refresh halaman!');
    });
}

function formatRp(objek) {
    objek.value = formatRupiah(objek.value);
}

function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^.\d]/g, '').toString(),
        split = number_string.split('.'),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
    }

    rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}

function formatuang(uang) {
    // format money co: 2.300.500,22
    var bilangan = uang;
    var number_string = bilangan.toString(),
        split = number_string.split('.'),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
        separator = sisa ? ',' : '';
        rupiah += separator + ribuan.join(',');
    }
    rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;

    return rupiah ? rupiah : '';
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

/*  end umum  */
function formatulang(a) {
    var s = a.replace(/\,/g, '');
    return s;
}

function hanyaangka() {
    key = event.which || event.keyCode;
    if (
        key != 8 // Backspace 
        &&
        key != 9 // tab
        &&
        key != 11 //  tab
        &&
        key != 16 &&
        key != 190 &&
        key != 110
        // && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
        &&
        key != 37 && key != 39 &&
        (key < 48 || key > 57) // Non digit
        &&
        (key < 96 || key > 105)
        // Dan masih banyak lagi seperti tombol del, panah kiri dan kanan, tombol tab, dll
    ) {
        event.preventDefault();
        return;
    }
}

function spasitidak() {
    key = event.which || event.keyCode;
    if (key === 32) {
        event.preventDefault();
        return;
    }
}

$('.dropdown').on('show.bs.dropdown', function(e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
});

$('.dropdown').on('hide.bs.dropdown', function(e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
});

function menuMobile(klas) {
    if (window.matchMedia('(max-width: 991px)').matches) {
        if (!($(klas).hasClass('show-submenu'))) {
            $(klas).removeClass('show-submenu');
        }
        $(klas).toggleClass('show-submenu');
    }
}

function hideMenuMobile() {
    if (window.matchMedia('(max-width: 991px)').matches) {
        $('.bottom-navbar').removeClass('header-toggled');
    }
}

String.prototype.replaceAll = function(searchStr, replaceStr) {
    var str = this;
    
    // escape regexp special characters in search string
    searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    
    return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
}