$(function() {

    var start = moment();
    var end = moment();

    function cb(start, end) {
        $('#tgl_awal').val(start.format("YYYY-MM-DD"));
        $('#tgl_akhir').val(end.format("YYYY-MM-DD"));
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        // manggil function
        getDashboard();
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

});

function getDashboard() {
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();
    $.ajax({
        type: "GET",
        url: base_url + '/select/getDataDashboard/' + tgl_awal + '/' + tgl_akhir,
        beforeSend: function() {
            $('#loading').show();
            $('#productorder-gage').html('');
        },
        success: function(res) {

        },
    }).done(function(res) {
        console.log(res)
        var rangedate = res.tgl_awal + ' s/d ' + res.tgl_akhir;
        var totalprint = res.totalPrinting == null ? 0 : addCommas(res.totalPrinting);
        var totbag1 = res.totbag1 == null ? 0 : addCommas(res.totbag1);
        var totbag2 = res.totbag2 == null ? 0 : addCommas(res.totbag2);
        var totbag3 = res.totbag3 == null ? 0 : addCommas(res.totbag3);
        var totbag4 = res.totbag4 == null ? 0 : addCommas(res.totbag4);
        var totbag5 = res.totbag5 == null ? 0 : addCommas(res.totbag5);
        var totbag7 = res.totbag7 == null ? 0 : addCommas(res.totbag7);
        var totbag8 = res.totbag8 == null ? 0 : addCommas(res.totbag8);
        var totbag9 = res.totbag9 == null ? 0 : addCommas(res.totbag9);
        var totbag10 = res.totalDeliv == null ? 0 : addCommas(res.totalDeliv);        
        $('#loading').hide();
        $('#total_so').text(res.totalSO);
        $('#rangedate1').text(rangedate);
        $('#total_rfp').text(res.totalRFP);
        $('#rangedate2').text(rangedate);
        $('#total_kp').text(res.totalKP);
        $('#rangedate3').text(rangedate);
        $('#bag1').text(totbag1 + ' Kg');
        $('#bag2').text(totbag2 + ' Kg');
        $('#bag3').text(totbag3 + ' Kg');
        $('#bag4').text(totbag4 + ' Kg');
        $('#bag5').text(totbag5 + ' Kg');
        $('#total_print').text(totalprint + ' Kg');
        $('#bag7').text(totbag7 + ' Kg');
        $('#bag8').text(totbag8 + ' Kg');
        $('#bag9').text(totbag9 + ' Kg');
        $('#bag10').text(totbag10 + ' Kg');
        $('.rangedate').text(rangedate);
        orderDelivery(res.qtyOrder, res.totalDeliv, res.tgl_awal, res.tgl_akhir);
        soNota(res.nilaiSO, res.nilaiNota, res.tgl_awal, res.tgl_akhir);
        salesOrderPerBulan(res.qBarChart);
        pemanisLaporanWS();
    }).fail(function(xhr, textStatus, errorThrown) {
        $('#loading').hide();
        swal(textStatus, errorThrown, 'error');
    });
}

function orderDelivery(qtyorder, qtydeliv, tgl_awal, tgl_akhir) {
    qtyorder = qtyorder == null ? 0 : qtyorder;
    qtydeliv = qtydeliv == null ? 0 : qtydeliv;
    var textOrderDeliv = 'Order:' + addCommas(qtyorder) + ' Kg, Deliv:' + addCommas(qtydeliv) + ' Kg';
    $('#order_deliv').text(textOrderDeliv);
    var percentTot = (qtydeliv / qtyorder) * 100 || 0;
    var productorderGage = new JustGage({
        id: 'productorder-gage',
        value: qtydeliv,
        min: 0,
        max: qtyorder,
        hideMinMax: true,
        symbol: 'Kg',
        label: percentTot.toFixed(2) + '% \n ' + tgl_awal + ' s/d ' + tgl_akhir,
        valueFontColor: "#001737",
        labelFontColor: "#001737",
        gaugeWidthScale: 0.3,
        counter: true,
        relativeGaugeSize: true,
        gaugeColor: "#f0f0f0",
        levelColors: ["#fcd53b"],
        textRenderer: function(val) {
            return addCommas(val) + 'Kg';
        }
    });
    $("#productorder-gage").append('<div class="product-order"><div class="icon-inside-circle"><i class="mdi mdi-basket"></i></div></div>');
}

// var barChart = null;

function soNota(nilaiSO, nilaiNota, tgl_awal, tgl_akhir) {
    // if (barChart != null) {
    //     barChart.destroy();
    // }
    nilaiSO = nilaiSO == null ? 0 : nilaiSO;
    nilaiNota = nilaiNota == null ? 0 : nilaiNota;
    var percentTot = (nilaiNota / nilaiSO) * 100 || 0;
    $('#sonota').text(percentTot.toFixed(2) + '%, range = ' + tgl_awal + ' s/d ' + tgl_akhir);

    var data_fix = [];
    var data_fix = [nilaiSO, nilaiNota];
    var supportTrackerData = {
        // labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ],
        labels: ['Revenue', 'Invoice'],
        datasets: [{
            label: 'Total',
            // data: [640, 750, 500, 400, 1200, 650, 550, 450, 400],
            data: data_fix,
            backgroundColor: [
                '#00ffff', '#0ddbb9',
            ],
            borderColor: [
                '#00ffff', '#0ddbb9'
            ],
            borderWidth: 1,
            fill: false
        }, ]
    };
    var supportTrackerOptions = {
        scales: {
            xAxes: [{
                stacked: false,
                barPercentage: 0.6,
                position: 'bottom',
                display: true,
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    display: true, //this will remove only the label
                    stepSize: 300,
                }
            }],
            yAxes: [{
                stacked: false,
                display: true,
                gridLines: {
                    drawBorder: false,
                    display: true,
                    color: "#f0f3f6",
                    borderDash: [8, 4],
                },
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        return addCommas(value);
                    }
                },
            }]
        },
        legend: {
            display: false
        },
        legendCallback: function(chart) {
            console.log(chart)
            var text = [];
            text.push('<ul class="' + chart.id + '-legend">');
            for (var i = 0; i < chart.data.datasets.length; i++) {
                text.push('<li><span class="legend-box" style="background:' + chart.data.datasets[i].backgroundColor[i] + ';"></span><span class="legend-label text-dark">');
                if (chart.data.datasets[i].label) {
                    text.push(chart.data.datasets[i].label);
                }
                text.push('</span></li>');
            }
            text.push('</ul>');
            return text.join("");
        },
        tooltips: {
            backgroundColor: 'rgba(0, 0, 0, 1)',
            callbacks: {
                label: function(t, d) {
                    var xLabel = d.datasets[t.datasetIndex].label;
                    var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : +t.yLabel;
                    return xLabel + ': ' + yLabel;
                }
            }
        },
        plugins: {
            datalabels: {
                display: false,
                align: 'center',
                anchor: 'center'
            }
        }
    };
    if (window.barChart != undefined) {
        window.barChart.destroy();
    }
    if ($("#supportSONOTA").length) {
        var barChartCanvas = $("#supportSONOTA").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        window.barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: supportTrackerData,
            options: supportTrackerOptions
        });
        // barChart.clear();
        document.getElementById('support-tracker-legend-so-nota').innerHTML = window.barChart.generateLegend();
    }
}

function GetMonthName(monthNumber) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
    return months[monthNumber - 1];
}

function salesOrderPerBulan(data) {
    var data1 = [],
        data2 = [],
        data3 = [],
        label = [],
        label1, label2, label3;
    $.each(data, function(k, v) {
        var bulan = GetMonthName(v.bulan);
        label.push(bulan);
        data2.push(v.totalorderatas);
        data1.push(v.totalorderbawah);
        data3.push(v.sales_plan);
        label2 = v.tahun_atas;
        label1 = v.tahun_bawah;
        label3 = 'Sales Plan';
    });
    var supportTrackerData = {
        // labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ],
        labels: label,
        datasets: [{
                // label: 'New Tickets',
                label: label1,
                // data: [640, 750, 500, 400, 1200, 650, 550, 450, 400],
                data: data1,
                backgroundColor: [
                    '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8',
                ],
                borderColor: [
                    '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8', '#d8d8d8',
                ],
                borderWidth: 1,
                fill: false
            },
            {
                // label: 'Open Tickets',
                label: label2,
                // data: [800, 550, 700, 600, 1100, 650, 550, 650, 850],
                data: data2,
                backgroundColor: [
                    '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee',
                ],
                borderColor: [
                    '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee', '#464dee',
                ],
                borderWidth: 1,
                fill: false
            },
            {
                // label: 'Open Tickets',
                label: label3,
                // data: [800, 550, 700, 600, 1100, 650, 550, 650, 850],
                data: data3,
                backgroundColor: [
                    '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff',
                ],
                borderColor: [
                    '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff', '#00ffff',
                ],
                borderWidth: 1,
                fill: false
            },
        ]
    };
    var supportTrackerOptions = {
        scales: {
            xAxes: [{
                stacked: false,
                barPercentage: 0.6,
                position: 'bottom',
                display: true,
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    display: true, //this will remove only the label
                    stepSize: 300,
                }
            }],
            yAxes: [{
                stacked: false,
                display: true,
                gridLines: {
                    drawBorder: false,
                    display: true,
                    color: "#f0f3f6",
                    borderDash: [8, 4],
                },
                ticks: {
                    beginAtZero: true,
                    callback: function(value, index, values) {
                        return addCommas(value);
                    }
                },
            }]
        },
        legend: {
            display: false
        },
        legendCallback: function(chart) {
            var text = [];
            text.push('<ul class="' + chart.id + '-legend">');
            for (var i = 0; i < chart.data.datasets.length; i++) {
                text.push('<li><span class="legend-box" style="background:' + chart.data.datasets[i].backgroundColor[i] + ';"></span><span class="legend-label text-dark">');
                if (chart.data.datasets[i].label) {
                    text.push(chart.data.datasets[i].label);
                }
                text.push('</span></li>');
            }
            text.push('</ul>');
            return text.join("");
        },
        tooltips: {
            backgroundColor: 'rgba(0, 0, 0, 1)',
            callbacks: {
                label: function(t, d) {
                    var xLabel = d.datasets[t.datasetIndex].label;
                    var yLabel = t.yLabel >= 1000 ? t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : +t.yLabel;
                    return xLabel + ': ' + yLabel;
                }
            }
        },
        plugins: {
            datalabels: {
                display: false,
                align: 'center',
                anchor: 'center'
            }
        }
    };
    if ($("#supportTracker").length) {
        var barChartCanvas = $("#supportTracker").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: supportTrackerData,
            options: supportTrackerOptions
        });
        document.getElementById('support-tracker-legend').innerHTML = barChart.generateLegend();
    }
}

function pemanisLaporanWS() {
    var newClientData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May"],
        datasets: [{
            label: 'Margin',
            data: [35, 37, 34, 36, 32],
            backgroundColor: [
                '#f7f7f7',
            ],
            borderColor: [
                '#dcdcdc'
            ],
            borderWidth: 2,
            fill: true,
        }, ],
    };
    var newClientOptions = {
        scales: {
            yAxes: [{
                display: false,
            }],
            xAxes: [{
                display: false,
            }],
        },
        legend: {
            display: false,
        },
        elements: {
            point: {
                radius: 0
            },
        },
        plugins: {
            datalabels: {
                display: false,
                align: 'center',
                anchor: 'center'
            }
        }
    };
    if ($(".pemanis").length) {
        var lineChartCanvas = $(".pemanis").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis2").length) {
        var lineChartCanvas = $(".pemanis2").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis3").length) {
        var lineChartCanvas = $(".pemanis3").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis4").length) {
        var lineChartCanvas = $(".pemanis4").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis5").length) {
        var lineChartCanvas = $(".pemanis5").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis6").length) {
        var lineChartCanvas = $(".pemanis6").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis7").length) {
        var lineChartCanvas = $(".pemanis7").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis8").length) {
        var lineChartCanvas = $(".pemanis8").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis9").length) {
        var lineChartCanvas = $(".pemanis9").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
    if ($(".pemanis10").length) {
        var lineChartCanvas = $(".pemanis10").get(0).getContext("2d");
        var saleschart = new Chart(lineChartCanvas, {
            type: 'line',
            data: newClientData,
            options: newClientOptions
        });
    }
}