<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Auth::routes();
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::post('change_pass', 'Auth\LoginController@change_pass')->name('change_pass');
Route::group(['middleware' => 'auth'], function () {
    Route::resource('sales_order', 'SalesOrderController');
    Route::resource('rfp', 'RFPController');
    Route::resource('kartu_produksi', 'KartuProduksiController');
    Route::resource('workstation', 'WorkStationController');
    // Route::resource('referensi', 'ReferensiController');

    //data
    Route::get('data/sales_order', 'SalesOrderController@data')->name('data.sales_order');
    Route::get('data_approved/sales_order', 'SalesOrderController@data_approved')->name('data_approved.sales_order');
    Route::get('data/rfp', 'RFPController@data')->name('data.rfp');
    Route::get('data_rfp_mkt/rfp', 'RFPController@data_approved_mkt')->name('data_rfp_mkt.rfp');
    Route::get('data_rfp_prod/rfp', 'RFPController@data_approved_prod')->name('data_rfp_prod.rfp');
    Route::get('data/kartu_produksi', 'KartuProduksiController@data')->name('data.kartu_produksi');
    Route::get('data_history/kartu_produksi', 'KartuProduksiController@data_history')->name('data_history.kartu_produksi');
    Route::get('data/workstation', 'WorkStationController@data')->name('data.workstation');
    Route::get('data/sales_plan', 'SalesOrderController@data_sales_plan')->name('data.sales_plan');

    //form
    Route::get('form/sales_order', 'SalesOrderController@form')->name('form.sales_order');
    Route::get('list_approved/sales_order', 'SalesOrderController@list_approved')->name('list_approved.sales_order');
    Route::get('list_approved_mkt/rfp', 'RFPController@list_approved_mkt')->name('list_approved_mkt.rfp');
    Route::get('list_approved_prod/rfp', 'RFPController@list_approved_prod')->name('list_approved_prod.rfp');
    Route::get('history_kartu/kartu_produksi', 'KartuProduksiController@history_kartu')->name('history_kartu.kartu_produksi');

    Route::get('sales_plan', 'SalesOrderController@sales_plan')->name('plan.sales_order');
    Route::get('sales_plan/form', 'SalesOrderController@form_sales_plan')->name('form_plan.sales_order');
    Route::post('sales_plan/save', 'SalesOrderController@save_sales_plan');
    Route::get('sales_plan/ambil_data/{tahun}', 'SalesOrderController@get_data_sales_plan');

    Route::get('so_outsourcing', 'SalesOrderController@so_outsourcing')->name('so_outsourcing');
    Route::get('data/so_outsourcing', 'SalesOrderController@data_so_outs')->name('so_outsourcing.data');

    Route::get('sales_order/alamat/{id}', 'SalesOrderController@getAlamat')->name('alamat.sales_order');
    Route::get('sales_order/repeat/{id}', 'SalesOrderController@repeatOrder')->name('repeat.sales_order');
    Route::get('sales_order/get_kain/{id}/{desain}', 'SalesOrderController@getKain')->name('get_kain.sales_order');
    Route::get('sales_order/kirim_approve/{id}', 'SalesOrderController@kirim_approve');
    Route::get('sales_order/edit/{id}', 'SalesOrderController@edit')->name('edit.sales_order');
    Route::get('sales_order/rfp/{id}', 'SalesOrderController@rfp')->name('rfp.sales_order');
    Route::get('sales_order/lihat/{id}/{link}', 'SalesOrderController@lihat')->name('lihat.sales_order');
    Route::post('sales_order/simpan', 'SalesOrderController@simpan')->name('simpan.sales_order');
    Route::post('sales_order/update', 'SalesOrderController@update')->name('update.sales_order');
    Route::get('sales_order/approved/{id}', 'SalesOrderController@approved');
    Route::get('sales_order/reject/{id}', 'SalesOrderController@reject');
    Route::get('sales_order/print/{id}/{link}', 'SalesOrderController@print_so');
    Route::get('sales_order/download/{id}', 'SalesOrderController@download')->name('sales_order.download'); 


    Route::get('rfp/create/{id}', 'RFPController@create');
    Route::post('rfp/simpan', 'RFPController@simpan')->name('simpan.rfp');
    Route::post('rfp/update', 'RFPController@update')->name('update.rfp');
    Route::get('rfp/kirim_approved_mkt/{id}', 'RFPController@kirim_approved_mkt');
    Route::get('rfp/approved_mkt/{id}', 'RFPController@approved_mkt');
    Route::get('rfp/reject_mkt/{id}', 'RFPController@reject_mkt');
    Route::get('rfp/kirim_approved_prod/{id}', 'RFPController@kirim_approved_prod');
    Route::get('rfp/approved_prod/{id}', 'RFPController@approved_prod');
    Route::get('rfp/reject_prod/{id}', 'RFPController@reject_prod');
    Route::get('rfp/lihat/{id}/{link}', 'RFPController@lihat')->name('lihat.rfp');
    Route::get('rfp/kirim_ke_ppc/{id}', 'RFPController@kirim_ke_ppc');
    Route::get('rfp/edit_rfp/{id}', 'RFPController@edit_rfp');
    Route::get('rfp/print_rfp/{id}/{link}', 'RFPController@print_rfp');
    Route::post('rfp/back_to_so', 'RFPController@backToSO');

    Route::get('kartu_produksi/create/{id}', 'KartuProduksiController@create');
    Route::post('kartu_produksi/simpan', 'KartuProduksiController@simpan');
    Route::get('kartu_produksi/edit_kartu/{id}', 'KartuProduksiController@edit_kartu');
    Route::post('kartu_produksi/update', 'KartuProduksiController@update');
    Route::get('kartu_produksi/lihat_kartu/{id}/{link}', 'KartuProduksiController@lihat_kartu');
    Route::get('kartu_produksi/download/{id_kartu}', 'KartuProduksiController@download')->name('kartu_produksi.download'); 
    Route::get('kartu_produksi/print/{id_kartu}/{link}', 'KartuProduksiController@print')->name('kartu_produksi.print');
    Route::get('kartu_produksi/pindah_slot/{id_rfp}', 'KartuProduksiController@pindah_slot');  
    Route::get('kartu_produksi/cek_accept_rfp/{id}', 'KartuProduksiController@cek_accept_rfp');
    // rfp_matrix
    Route::get('rfp_info/kartu_produksi', 'KartuProduksiController@rfp_info')->name('rfp_matrix.kartu_produksi');  
    Route::get('rfp_matrix/kartu_produksi/lihat/{id}/{link}', 'KartuProduksiController@lihat_rfp');    
    Route::get('rfp_matrix/persiapan/{id}/{pembagi}/{ori_cond}', 'KartuProduksiController@persiapan');
    Route::post('rfp_info/save_persiapan', 'KartuProduksiController@save_persiapan');
    Route::get('rfp_matrix/cek_perisapan/{id}', 'KartuProduksiController@cek_persiapan');
    Route::get('rfp_matrix/cek_lihat_persiapan/{id}', 'KartuProduksiController@cek_lihat_persiapan');
    Route::get('rfp_matrix/lihat_persiapan/{id}', 'KartuProduksiController@lihat_persiapan');
    Route::get('rfp_matrix/edit_persiapan/{id}', 'KartuProduksiController@edit_persiapan');
    Route::post('rfp_matrix/update_persiapan', 'KartuProduksiController@update_persiapan');

    Route::get('workstation/list_cw/{id_kartu}/{id_rfp}/{id_so}/{link}', 'WorkStationController@list_cw');    
    Route::get('workstation/bagian/{id}/{id_bagian}', 'WorkStationController@bagian');
    Route::post('workstation/update_cw', 'WorkStationController@update_cw');
    Route::get('workstation/detail/{id_rfp}/{id_so}/{link}', 'WorkStationController@detail_ws');
    Route::get('workstation/ambilSisa/{id_cw}/{bagian}', 'WorkStationController@ambilSisa');
    Route::get('workstation/selesai/{id_kartu}', 'WorkStationController@selesai');
    Route::get('workstation/get_det_ws/{id}', 'WorkStationController@edit_detail');
    Route::post('workstation/update_det_ws', 'WorkStationController@update_detail');
    Route::post('workstation/excel_det_ws', 'WorkStationController@excel_det_ws');

    Route::get('/referensi/ket_umum', 'ReferensiController@ket_umum')->name('referensi.ket_umum');
    Route::get('/referensi/form_ket_umum', 'ReferensiController@form_ket_umum')->name('referensi.form_ket_umum');
    Route::post('/referensi/ket_umum/save', 'ReferensiController@save_ket_umum');
    Route::post('/referensi/ket_umum/update', 'ReferensiController@update_ket_umum');
    Route::get('/referensi/child/{id}', 'ReferensiController@child');
    Route::get('/referensi/ket_umum/edit/{id}', 'ReferensiController@edit_ket_umum');

    Route::get('/referensi/print_logo', 'ReferensiController@print_logo')->name('referensi.print_logo');
    Route::get('/referensi/form_print_logo', 'ReferensiController@form_print_logo')->name('referensi.form_print_logo');
    Route::post('/referensi/print_logo/save', 'ReferensiController@save_print_logo');
    Route::post('/referensi/print_logo/aktif', 'ReferensiController@aktif_print_logo');
    Route::post('/referensi/print_logo/non_aktif', 'ReferensiController@non_aktif_print_logo');

    Route::get('/referensi/pelanggan', 'ReferensiController@pelanggan')->name('referensi.pelanggan');
    Route::get('/data_pelanggan', 'ReferensiController@data_pelanggan')->name('data_pelanggan');
    Route::get('/referensi/pelanggan/edit/{id}', 'ReferensiController@edit_pelanggan');
    Route::post('/referensi/pelanggan/simpan', 'ReferensiController@simpan_pelanggan');

    Route::get('/referensi/bagian', 'ReferensiController@bagian')->name('referensi.bagian');
    Route::get('/data_bagian', 'ReferensiController@data_bagian')->name('data_bagian');
    Route::get('/referensi/bagian/edit/{id}', 'ReferensiController@edit_bagian');
    Route::post('/referensi/bagian/simpan', 'ReferensiController@simpan_bagian');
    Route::get('/referensi/bagian/hapus/{id}', 'ReferensiController@hapus_bagian');
    Route::get('/referensi/bagian/make_last/{id}', 'ReferensiController@make_last_bagian');

    Route::get('/referensi/jns_bahan', 'ReferensiController@jns_bahan')->name('referensi.jns_bahan');
    Route::get('/data_jns_bahan', 'ReferensiController@data_jns_bahan')->name('data_jns_bahan');
    Route::get('/referensi/jns_bahan/edit/{id}', 'ReferensiController@edit_jns_bahan');
    Route::post('/referensi/jns_bahan/simpan', 'ReferensiController@simpan_jns_bahan');
    Route::get('/referensi/jns_bahan/hapus/{id}', 'ReferensiController@hapus_jns_bahan');

    Route::get('/referensi/jns_printing', 'ReferensiController@jns_printing')->name('referensi.jns_printing');
    Route::get('/data_jns_printing', 'ReferensiController@data_jns_printing')->name('data_jns_printing');
    Route::get('/referensi/jns_printing/edit/{id}', 'ReferensiController@edit_jns_printing');
    Route::post('/referensi/jns_printing/simpan', 'ReferensiController@simpan_jns_printing');
    Route::get('/referensi/jns_printing/hapus/{id}', 'ReferensiController@hapus_jns_printing');

    Route::get('/referensi/kain', 'ReferensiController@kain')->name('referensi.kain');
    Route::get('/data_kain', 'ReferensiController@data_kain')->name('data_kain');
    Route::get('/referensi/kain/edit/{id}', 'ReferensiController@edit_kain');
    Route::post('/referensi/kain/simpan', 'ReferensiController@simpan_kain');
    Route::get('/referensi/kain/hapus/{id}', 'ReferensiController@hapus_kain');

    Route::get('/referensi/kondisi_kain', 'ReferensiController@kondisi_kain')->name('referensi.kondisi_kain');
    Route::get('/data_kondisi_kain', 'ReferensiController@data_kondisi_kain')->name('data_kondisi_kain');
    Route::get('/referensi/kondisi_kain/edit/{id}', 'ReferensiController@edit_kondisi_kain');
    Route::post('/referensi/kondisi_kain/simpan', 'ReferensiController@simpan_kondisi_kain');
    Route::get('/referensi/kondisi_kain/hapus/{id}', 'ReferensiController@hapus_kondisi_kain');

    Route::get('/referensi/ori_kondisi', 'ReferensiController@ori_kondisi')->name('referensi.ori_kondisi');
    Route::get('/data_ori_kondisi', 'ReferensiController@data_ori_kondisi')->name('data_ori_kondisi');
    Route::get('/referensi/ori_kondisi/edit/{id}', 'ReferensiController@edit_ori_kondisi');
    Route::post('/referensi/ori_kondisi/simpan', 'ReferensiController@simpan_ori_kondisi');
    Route::get('/referensi/ori_kondisi/hapus/{id}', 'ReferensiController@hapus_ori_kondisi');

    Route::get('/referensi/role', 'ReferensiController@role')->name('referensi.role');
    Route::get('/data_role', 'ReferensiController@data_role')->name('data_role');
    Route::get('/referensi/role/edit/{id}', 'ReferensiController@edit_role');
    Route::post('/referensi/role/simpan', 'ReferensiController@simpan_role');
    Route::get('/referensi/role/hapus/{id}', 'ReferensiController@hapus_role');

    Route::get('/referensi/so_flag', 'ReferensiController@so_flag')->name('referensi.so_flag');
    Route::get('/data_so_flag', 'ReferensiController@data_so_flag')->name('data_so_flag');
    Route::get('/referensi/so_flag/edit/{id}', 'ReferensiController@edit_so_flag');
    Route::post('/referensi/so_flag/simpan', 'ReferensiController@simpan_so_flag');
    Route::get('/referensi/so_flag/hapus/{id}', 'ReferensiController@hapus_so_flag');

    Route::get('/referensi/tekstur_akhir', 'ReferensiController@tekstur_akhir')->name('referensi.tekstur_akhir');
    Route::get('/data_tekstur_akhir', 'ReferensiController@data_tekstur_akhir')->name('data_tekstur_akhir');
    Route::get('/referensi/tekstur_akhir/edit/{id}', 'ReferensiController@edit_tekstur_akhir');
    Route::post('/referensi/tekstur_akhir/simpan', 'ReferensiController@simpan_tekstur_akhir');
    Route::get('/referensi/tekstur_akhir/hapus/{id}', 'ReferensiController@hapus_tekstur_akhir');

    Route::get('/referensi/warna_dasar', 'ReferensiController@warna_dasar')->name('referensi.warna_dasar');
    Route::get('/data_warna_dasar', 'ReferensiController@data_warna_dasar')->name('data_warna_dasar');
    Route::get('/referensi/warna_dasar/edit/{id}', 'ReferensiController@edit_warna_dasar');
    Route::post('/referensi/warna_dasar/simpan', 'ReferensiController@simpan_warna_dasar');
    Route::get('/referensi/warna_dasar/hapus/{id}', 'ReferensiController@hapus_warna_dasar');

    Route::get('/referensi/user', 'ReferensiController@user')->name('referensi.user');
    Route::get('/data_user', 'ReferensiController@data_user')->name('data_user');
    Route::get('/referensi/user/edit/{id}', 'ReferensiController@edit_user');
    Route::post('/referensi/user/simpan', 'ReferensiController@simpan_user');
    Route::get('/referensi/user/reset_password/{id}', 'ReferensiController@reset_password');

    Route::get('/referensi/menu_head', 'ReferensiController@menu_head')->name('referensi.menu_head');

    Route::get('/referensi/menu_item', 'ReferensiController@menu_item')->name('referensi.menu_item');

    Route::get('/referensi/hak_akses', 'ReferensiController@hak_akses')->name('referensi.hak_akses');
    Route::get('/data_hak_akses', 'ReferensiController@data_hak_akses')->name('data_hak_akses');
    Route::get('/tambah_hak_akses', 'ReferensiController@tambah_hak_akses');
    Route::post('/simpan_hak_akses', 'ReferensiController@simpan_hak_akses');
    Route::post('/hapus_hak_akses', 'ReferensiController@hapus_hak_akses');
    Route::get('/edit_hak_akses/{roleid}', 'ReferensiController@edit_hak_akses');
    Route::post('/update_hak_akses', 'ReferensiController@update_hak_akses');

    Route::get('/export_excel/so', 'ExportExcelController@so')->name('so_export');
    Route::get('/export_excel/rfp', 'ExportExcelController@rfp')->name('rfp_export');
    Route::get('/export_excel/kp', 'ExportExcelController@kp')->name('kp_export');
    Route::get('/export_excel/ws', 'ExportExcelController@ws')->name('ws_export');

    Route::get('qc', 'QCController@index')->name('qc');
    Route::get('qc/data', 'QCController@dataqc')->name('dataqc');
    Route::get('qc/create_pack', 'QCController@create_pack');
    Route::get('qc/cek_perisapan/{id}', 'QCController@cek_persiapan');
    Route::post('qc/save_pack', 'QCController@save_pack');
    Route::get('qc/lihat_packing', 'QCController@lihat_pack');

    Route::get('do', 'DOController@index')->name('do');
    Route::get('do/list/{pel}', 'DOController@list');
    Route::get('do/create_do', 'DOController@create_do');
    Route::get('do/cek_qc/{id}', 'DOController@cek_qc');
    Route::post('do/simpan', 'DOController@simpan');
    Route::get('do/data', 'DOController@data_sj')->name('do_data');
    Route::get('do/print_do', 'DOController@print_do');
    Route::get('do/lihat_do', 'DOController@lihat_do');
    Route::get('do/update_print/{id}', 'DOController@update_print');
    Route::get('do/cek_print_sj/{id}', 'DOController@cek_print_sj');
    Route::get('do/edit_do', 'DOController@edit_do');
    Route::get('do/request_print_sj/{id}', 'DOController@request_print');

    Route::get('keuangan/nota', 'KeuanganController@index')->name('keuangan.nota');
    Route::get('keuangan/data_nota', 'KeuanganController@data_nota')->name('data_nota');
    Route::get('keuangan/lihat_nota', 'KeuanganController@lihat_nota');
    Route::get('keuangan/print_nota', 'KeuanganController@print_nota');
    Route::get('keuangan/edit_nota', 'KeuanganController@edit_nota');
    Route::post('keuangan/input_nota', 'KeuanganController@input_nota');
    Route::post('keuangan/nota/simpan_nota', 'KeuanganController@simpan_nota');
    Route::post('keuangan/nota/update_nota', 'KeuanganController@update_nota');
    Route::get('keuangan/list_sj/{pel}', 'KeuanganController@list_sj');
    Route::get('keuangan/list_approved_print_sj', 'KeuanganController@list_approved_print_sj')->name('list_approved_print_sj');
    Route::get('keuangan/data_print_sj', 'KeuanganController@data_print_sj')->name('data_request_print_sj');
    Route::get('keuangan/lihat_do', 'KeuanganController@lihat_do');
    Route::get('keuangan/approved_sj/{id}', 'KeuanganController@approved_print_sj');
    Route::get('keuangan/reject_sj/{id}', 'KeuanganController@reject_print_sj');

    Route::get('invoice', 'InvoiceController@index')->name('invoice');
    Route::get('invoice/data_nota/{id}', 'InvoiceController@data_nota')->name('data_nota_invoice');
    Route::get('invoice/data_invoice', 'InvoiceController@data_invoice')->name('data_invoice');
    Route::post('invoice/form_invoice', 'InvoiceController@form_invoice');
    Route::post('invoice/simpan', 'InvoiceController@simpan');
    Route::get('invoice/lihat/{id}', 'InvoiceController@lihat');
    Route::post('invoice/cancel_kontrabon', 'InvoiceController@cancel');

    Route::get('pelunasan', 'PelunasanController@index')->name('pelunasan');
    Route::get('pelunasan/data_pelunasan', 'PelunasanController@data_pelunasan')->name('data_pelunasan');
    Route::get('pelunasan/form', 'PelunasanController@form_pelunasan')->name('form_pelunasan');
    Route::get('pelunasan/list_kontrabon/{id}', 'PelunasanController@list_kontrabon');
    Route::get('pelunasan/list_nota/{id}', 'PelunasanController@list_nota');
    Route::get('pelunasan/list_sisa_uang_masuk/{id}', 'PelunasanController@list_sisa_uang_masuk');
    Route::post('pelunasan/simpan', 'PelunasanController@simpan');
    Route::get('pelunasan/lihat/{id}', 'PelunasanController@lihat_pelunasan');

    Route::get('/select/count_posisi', 'HomeController@countPosisi');
    Route::get('/select/getDataDashboard/{tgl_awal}/{tgl_akhir}', 'HomeController@getDataDashboard');
    Route::get('/home/accept/{id}', 'HomeController@accept_rfp');
    Route::get('/rfp_known', 'HomeController@rfp_known')->name('rfp_known');
    Route::get('/rfp_known/data', 'HomeController@data_rfp_known')->name('rfp_known.data');
    Route::get('/rfp_known/detail/{id}/{link}', 'HomeController@det_rfp_known');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index');
    Route::post('/logout2', 'HomeController@logout2');
    Route::post('/home/change_pass', 'HomeController@change_pass');
    Route::get('/home/menu', 'HomeController@menu');

    //hanya route fake
    Route::get('/tes')->name('tes');
});
