PGDMP         )                 x            kharisma_printex    11.4    11.2    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    65801    kharisma_printex    DATABASE     �   CREATE DATABASE kharisma_printex WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
     DROP DATABASE kharisma_printex;
             postgres    false            �            1259    65802 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    65805    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    196            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    197            �            1259    66070 	   mst_do_sj    TABLE     ,  CREATE TABLE public.mst_do_sj (
    i_id integer NOT NULL,
    i_id_kartu integer,
    i_id_rfp integer,
    i_no_sj character varying(16) NOT NULL,
    d_sj date,
    i_pel integer,
    d_due_date date,
    n_total_cw integer,
    e_ket character varying(250),
    n_tot_roll numeric,
    n_tot_asal_sj numeric,
    n_tot_asal_kp numeric,
    n_tot_jadi_kp numeric,
    n_total_tagihan numeric,
    n_sisa_tagihan numeric,
    f_print boolean DEFAULT false,
    f_lunas boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    tgl_produksi date,
    expedisi character varying(25),
    i_status integer,
    alasan_reject character varying(200),
    d_approved timestamp without time zone,
    f_nota boolean DEFAULT false,
    i_id_so integer
);
    DROP TABLE public.mst_do_sj;
       public         postgres    false            �            1259    82149    mst_invoice    TABLE     �  CREATE TABLE public.mst_invoice (
    i_invoice integer NOT NULL,
    i_invoice_code character varying(20) NOT NULL,
    i_pel integer,
    d_invoice date,
    v_total_invoice numeric DEFAULT 0,
    v_total_invoice_sisa numeric DEFAULT 0,
    f_lunas boolean DEFAULT false,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.mst_invoice;
       public         postgres    false                       1259    82261    mst_invoice_item    TABLE     v  CREATE TABLE public.mst_invoice_item (
    i_invoice_item integer NOT NULL,
    i_invoice integer NOT NULL,
    i_nota integer NOT NULL,
    v_qty_nota numeric DEFAULT 0,
    v_nilai_nota numeric DEFAULT 0,
    f_lunas boolean DEFAULT false,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 $   DROP TABLE public.mst_invoice_item;
       public         postgres    false                       1259    82259 #   mst_invoice_item_i_invoice_item_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_invoice_item_i_invoice_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.mst_invoice_item_i_invoice_item_seq;
       public       postgres    false    269            �           0    0 #   mst_invoice_item_i_invoice_item_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.mst_invoice_item_i_invoice_item_seq OWNED BY public.mst_invoice_item.i_invoice_item;
            public       postgres    false    268            �            1259    65807 	   mst_kartu    TABLE     H  CREATE TABLE public.mst_kartu (
    i_id integer NOT NULL,
    i_no_kartu character varying(25) NOT NULL,
    d_pengiriman date,
    i_id_so integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_lebar_blanket character varying(75),
    e_handfeel character varying(75),
    d_beres timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_ket character varying(250),
    i_status integer,
    tot_roll numeric,
    tot_kg numeric,
    tot_pjg numeric,
    e_lebar_kain_jadi numeric,
    tgl_produksi date
);
    DROP TABLE public.mst_kartu;
       public         postgres    false            �            1259    65813    mst_kartu_cw    TABLE     �  CREATE TABLE public.mst_kartu_cw (
    i_id integer NOT NULL,
    i_id_kartu integer NOT NULL,
    e_cw character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0,
    d_strike_off date,
    e_jenis_kain character varying(50)
);
     DROP TABLE public.mst_kartu_cw;
       public         postgres    false                       1259    90365    mst_menu_role    TABLE       CREATE TABLE public.mst_menu_role (
    i_id integer NOT NULL,
    role_id integer NOT NULL,
    id_menu_head integer NOT NULL,
    id_menu_item integer NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.mst_menu_role;
       public         postgres    false                       1259    90363    mst_menu_role_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_menu_role_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.mst_menu_role_i_id_seq;
       public       postgres    false    273            �           0    0    mst_menu_role_i_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.mst_menu_role_i_id_seq OWNED BY public.mst_menu_role.i_id;
            public       postgres    false    272            �            1259    73957    mst_nota    TABLE     �  CREATE TABLE public.mst_nota (
    i_nota integer NOT NULL,
    i_nota_code character varying(20) NOT NULL,
    i_pel integer,
    d_nota date,
    d_due_date date,
    n_diskon real DEFAULT 0,
    v_diskon numeric DEFAULT 0,
    n_tot_qty numeric DEFAULT 0,
    v_total_nota numeric DEFAULT 0,
    v_total_nppn numeric DEFAULT 0,
    v_total_nppn_sisa numeric DEFAULT 0,
    f_invoice boolean DEFAULT false,
    f_lunas boolean DEFAULT false,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    n_ppn real DEFAULT 0,
    v_ppn numeric DEFAULT 0,
    i_qty_from integer
);
    DROP TABLE public.mst_nota;
       public         postgres    false            �            1259    73976    mst_nota_item    TABLE     .  CREATE TABLE public.mst_nota_item (
    i_nota_item integer NOT NULL,
    i_nota integer NOT NULL,
    i_sj integer,
    n_qty numeric DEFAULT 0,
    v_hrg_satuan numeric DEFAULT 0,
    i_qty_from integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.mst_nota_item;
       public         postgres    false            �            1259    73974    mst_nota_item_i_nota_item_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_nota_item_i_nota_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.mst_nota_item_i_nota_item_seq;
       public       postgres    false    249            �           0    0    mst_nota_item_i_nota_item_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.mst_nota_item_i_nota_item_seq OWNED BY public.mst_nota_item.i_nota_item;
            public       postgres    false    248            �            1259    65820    mst_packing_list    TABLE     �  CREATE TABLE public.mst_packing_list (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    i_cw integer,
    e_kode character varying(6),
    n_asal_sj numeric DEFAULT 0,
    n_asal_kp numeric DEFAULT 0,
    n_jadi_kp numeric DEFAULT 0,
    f_jadi_sj boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_kartu integer,
    i_no_packing character varying(40),
    i_id_sj integer
);
 $   DROP TABLE public.mst_packing_list;
       public         postgres    false            �            1259    65830    mst_packing_list_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_packing_list_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.mst_packing_list_i_id_seq;
       public       postgres    false    200            �           0    0    mst_packing_list_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.mst_packing_list_i_id_seq OWNED BY public.mst_packing_list.i_id;
            public       postgres    false    201            �            1259    82176    mst_pelunasan    TABLE     �  CREATE TABLE public.mst_pelunasan (
    i_pelunasan integer NOT NULL,
    i_pelunasan_code character varying(20) NOT NULL,
    i_pel integer,
    d_pelunasan date,
    v_pelunasan numeric DEFAULT 0,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    keterangan character varying(100),
    i_invoice integer
);
 !   DROP TABLE public.mst_pelunasan;
       public         postgres    false            �            1259    82188    mst_pelunasan_item    TABLE     E  CREATE TABLE public.mst_pelunasan_item (
    i_pelunasan_item integer NOT NULL,
    i_pelunasan integer NOT NULL,
    i_invoice integer,
    i_nota integer,
    v_bayar_nota numeric DEFAULT 0,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 &   DROP TABLE public.mst_pelunasan_item;
       public         postgres    false            �            1259    82186 '   mst_pelunasan_item_i_pelunasan_item_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_pelunasan_item_i_pelunasan_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.mst_pelunasan_item_i_pelunasan_item_seq;
       public       postgres    false    253            �           0    0 '   mst_pelunasan_item_i_pelunasan_item_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.mst_pelunasan_item_i_pelunasan_item_seq OWNED BY public.mst_pelunasan_item.i_pelunasan_item;
            public       postgres    false    252            �            1259    65832    mst_sales_plan    TABLE     �   CREATE TABLE public.mst_sales_plan (
    i_id integer NOT NULL,
    n_qty numeric DEFAULT 0,
    bulan character varying(2),
    tahun character varying(5),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 "   DROP TABLE public.mst_sales_plan;
       public         postgres    false            �            1259    65839    mst_sales_plan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_sales_plan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.mst_sales_plan_i_id_seq;
       public       postgres    false    202            �           0    0    mst_sales_plan_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.mst_sales_plan_i_id_seq OWNED BY public.mst_sales_plan.i_id;
            public       postgres    false    203            �            1259    65841    mst_so    TABLE       CREATE TABLE public.mst_so (
    i_id integer NOT NULL,
    i_no_so character varying(16) NOT NULL,
    d_so date,
    i_pel integer,
    f_repeat boolean,
    i_desain character varying(16),
    n_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_color_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    d_strike_off date,
    d_approval_strike_off date,
    d_penyerahan_brg date,
    e_keterangan_kirim character varying(250),
    i_status integer,
    d_approved timestamp(6) without time zone,
    v_pekerjaan numeric,
    v_pekerjaan_plus_ppn numeric,
    v_discount numeric,
    n_discount real,
    v_sisa numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_reject character varying(200),
    i_no_po character varying(25),
    flag_so integer,
    hitung_by integer,
    n_ppn real,
    v_ppn numeric,
    exclude_include character varying(1),
    cara_bayar integer
);
    DROP TABLE public.mst_so;
       public         postgres    false            �           0    0    TABLE mst_so    COMMENT     �   COMMENT ON TABLE public.mst_so IS '- Hitung By: 1 -> KG, 2 -> Roll, 3 -> Yard/Meter
- Exclude_Include: E adalah Exclude, I adalah Include
- cara_bayar:
1 -> Cash
2 -> Transfer
3 -> Lain Lain';
            public       postgres    false    204            �            1259    65847    mst_so_item    TABLE     �  CREATE TABLE public.mst_so_item (
    i_id integer NOT NULL,
    i_id_so integer NOT NULL,
    e_uraian_pekerjaan character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    v_harga_sat numeric,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0,
    d_strike_off date,
    e_jenis_kain character varying(50)
);
    DROP TABLE public.mst_so_item;
       public         postgres    false            �            1259    65854    mst_so_item_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_so_item_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.mst_so_item_i_id_seq;
       public       postgres    false    205            �           0    0    mst_so_item_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.mst_so_item_i_id_seq OWNED BY public.mst_so_item.i_id;
            public       postgres    false    206            �            1259    65856    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false            �            1259    65862 
   ref_bagian    TABLE     �   CREATE TABLE public.ref_bagian (
    i_id integer NOT NULL,
    nama_bagian character varying(50) NOT NULL,
    e_penanggung_jawab character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_bagian;
       public         postgres    false                       1259    82229    ref_jns_bahan    TABLE     �   CREATE TABLE public.ref_jns_bahan (
    i_id integer NOT NULL,
    e_jns_bahan character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.ref_jns_bahan;
       public         postgres    false                       1259    82227    ref_jns_bahan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_jns_bahan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.ref_jns_bahan_i_id_seq;
       public       postgres    false    261            �           0    0    ref_jns_bahan_i_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.ref_jns_bahan_i_id_seq OWNED BY public.ref_jns_bahan.i_id;
            public       postgres    false    260                       1259    82213    ref_jns_printing    TABLE     �   CREATE TABLE public.ref_jns_printing (
    i_id integer NOT NULL,
    e_jns_printing character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 $   DROP TABLE public.ref_jns_printing;
       public         postgres    false                        1259    82211    ref_jns_printing_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_jns_printing_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.ref_jns_printing_i_id_seq;
       public       postgres    false    257            �           0    0    ref_jns_printing_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.ref_jns_printing_i_id_seq OWNED BY public.ref_jns_printing.i_id;
            public       postgres    false    256            �            1259    65865    ref_kain    TABLE     �   CREATE TABLE public.ref_kain (
    i_id integer NOT NULL,
    e_kain character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_kain;
       public         postgres    false            �            1259    65868    ref_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_kain_i_id_seq;
       public       postgres    false    209                        0    0    ref_kain_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_kain_i_id_seq OWNED BY public.ref_kain.i_id;
            public       postgres    false    210            �            1259    65870    ref_ket_umum_so    TABLE     �   CREATE TABLE public.ref_ket_umum_so (
    i_id integer NOT NULL,
    e_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_ket_umum_so;
       public         postgres    false            �            1259    65873    ref_ket_umum_so_child    TABLE       CREATE TABLE public.ref_ket_umum_so_child (
    i_id integer NOT NULL,
    i_id_ket_umum integer NOT NULL,
    e_child_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 )   DROP TABLE public.ref_ket_umum_so_child;
       public         postgres    false            �            1259    65876    ref_ket_umum_so_child_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_ket_umum_so_child_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.ref_ket_umum_so_child_i_id_seq;
       public       postgres    false    212                       0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.ref_ket_umum_so_child_i_id_seq OWNED BY public.ref_ket_umum_so_child.i_id;
            public       postgres    false    213                       1259    82221    ref_kondisi_kain    TABLE     �   CREATE TABLE public.ref_kondisi_kain (
    i_id integer NOT NULL,
    e_kondisi_kain character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 $   DROP TABLE public.ref_kondisi_kain;
       public         postgres    false                       1259    82219    ref_kondisi_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kondisi_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.ref_kondisi_kain_i_id_seq;
       public       postgres    false    259                       0    0    ref_kondisi_kain_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.ref_kondisi_kain_i_id_seq OWNED BY public.ref_kondisi_kain.i_id;
            public       postgres    false    258            �            1259    65878    ref_logo    TABLE     �   CREATE TABLE public.ref_logo (
    i_id integer NOT NULL,
    logo_name character varying(100),
    f_active boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_logo;
       public         postgres    false            �            1259    65882    ref_logo_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_logo_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_logo_i_id_seq;
       public       postgres    false    214                       0    0    ref_logo_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_logo_i_id_seq OWNED BY public.ref_logo.i_id;
            public       postgres    false    215                       1259    90352    ref_menu_head    TABLE     @  CREATE TABLE public.ref_menu_head (
    i_id integer NOT NULL,
    nama_menu character varying(150) NOT NULL,
    icon_menu character varying(100),
    rut_name character varying(150),
    stand_alone boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.ref_menu_head;
       public         postgres    false                       1259    90358    ref_menu_item    TABLE     D  CREATE TABLE public.ref_menu_item (
    i_id integer NOT NULL,
    id_menu_head integer NOT NULL,
    nama_sub_menu character varying(150) NOT NULL,
    icon_sub_menu character varying(100),
    rut_name character varying(150),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.ref_menu_item;
       public         postgres    false            �            1259    65884    ref_no_urut    TABLE     �   CREATE TABLE public.ref_no_urut (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    no_urut character varying(11),
    bln character varying(2),
    thn character varying(5),
    flag_so integer
);
    DROP TABLE public.ref_no_urut;
       public         postgres    false            �            1259    65887    ref_no_urut_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_no_urut_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.ref_no_urut_id_seq;
       public       postgres    false    216                       0    0    ref_no_urut_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.ref_no_urut_id_seq OWNED BY public.ref_no_urut.id;
            public       postgres    false    217                       1259    82237    ref_ori_kondisi    TABLE     �   CREATE TABLE public.ref_ori_kondisi (
    i_id integer NOT NULL,
    e_ori_kondisi character varying(100),
    tipe integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_ori_kondisi;
       public         postgres    false                       1259    82235    ref_ori_kondisi_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_ori_kondisi_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_ori_kondisi_i_id_seq;
       public       postgres    false    263                       0    0    ref_ori_kondisi_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.ref_ori_kondisi_i_id_seq OWNED BY public.ref_ori_kondisi.i_id;
            public       postgres    false    262            �            1259    65889    ref_pelanggan    TABLE       CREATE TABLE public.ref_pelanggan (
    i_pel integer NOT NULL,
    e_nama_pel character varying(255) NOT NULL,
    e_alamat_pel character varying(255),
    f_pkp boolean DEFAULT false,
    e_npwp_pel character varying(30),
    e_telp_pel character varying(30),
    e_kont_pel character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_fax_pel character varying(30),
    e_kota_pel character varying(60),
    e_kode_marketing character varying(16),
    n_jth_tempo integer
);
 !   DROP TABLE public.ref_pelanggan;
       public         postgres    false            �            1259    65896    ref_pelanggan_i_pel_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_pelanggan_i_pel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.ref_pelanggan_i_pel_seq;
       public       postgres    false    218                       0    0    ref_pelanggan_i_pel_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.ref_pelanggan_i_pel_seq OWNED BY public.ref_pelanggan.i_pel;
            public       postgres    false    219            �            1259    65898    ref_role    TABLE     �   CREATE TABLE public.ref_role (
    i_id integer NOT NULL,
    e_role_name character varying(25) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.ref_role;
       public         postgres    false            �            1259    65901    ref_role_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_role_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_role_i_id_seq;
       public       postgres    false    220                       0    0    ref_role_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_role_i_id_seq OWNED BY public.ref_role.i_id;
            public       postgres    false    221            �            1259    65903    ref_sat_qty    TABLE     �   CREATE TABLE public.ref_sat_qty (
    i_id integer NOT NULL,
    e_sat character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_sat_qty;
       public         postgres    false            �            1259    65906    ref_sat_qty_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_sat_qty_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_sat_qty_i_id_seq;
       public       postgres    false    222                       0    0    ref_sat_qty_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_sat_qty_i_id_seq OWNED BY public.ref_sat_qty.i_id;
            public       postgres    false    223            �            1259    65908    ref_so_flag    TABLE     �   CREATE TABLE public.ref_so_flag (
    i_id integer NOT NULL,
    flag_name character varying(100),
    f_active boolean DEFAULT true,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_so_flag;
       public         postgres    false            �            1259    65912    ref_so_flag_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_so_flag_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_so_flag_i_id_seq;
       public       postgres    false    224            	           0    0    ref_so_flag_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_so_flag_i_id_seq OWNED BY public.ref_so_flag.i_id;
            public       postgres    false    225                       1259    82253    ref_tekstur_akhir    TABLE     �   CREATE TABLE public.ref_tekstur_akhir (
    i_id integer NOT NULL,
    e_tekstur character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 %   DROP TABLE public.ref_tekstur_akhir;
       public         postgres    false            
           1259    82251    ref_tekstur_akhir_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_tekstur_akhir_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.ref_tekstur_akhir_i_id_seq;
       public       postgres    false    267            
           0    0    ref_tekstur_akhir_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.ref_tekstur_akhir_i_id_seq OWNED BY public.ref_tekstur_akhir.i_id;
            public       postgres    false    266            	           1259    82245    ref_warna_dasar    TABLE     �   CREATE TABLE public.ref_warna_dasar (
    i_id integer NOT NULL,
    e_warna_dasar character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_warna_dasar;
       public         postgres    false                       1259    82243    ref_warna_dasar_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_warna_dasar_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_warna_dasar_i_id_seq;
       public       postgres    false    265                       0    0    ref_warna_dasar_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.ref_warna_dasar_i_id_seq OWNED BY public.ref_warna_dasar.i_id;
            public       postgres    false    264            �            1259    65914    ref_workflow    TABLE     �   CREATE TABLE public.ref_workflow (
    i_id integer NOT NULL,
    definition character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
     DROP TABLE public.ref_workflow;
       public         postgres    false            �            1259    65917    ref_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_workstation_i_id_seq;
       public       postgres    false    208                       0    0    ref_workstation_i_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public.ref_workstation_i_id_seq OWNED BY public.ref_bagian.i_id;
            public       postgres    false    227            �            1259    65919    rfp_urutan_fifo_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_urutan_fifo_seq
    START WITH 51
    INCREMENT BY 1
    MINVALUE 51
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.rfp_urutan_fifo_seq;
       public       postgres    false            �            1259    65921    rfp    TABLE       CREATE TABLE public.rfp (
    i_id integer NOT NULL,
    i_no_rfp character varying(20) NOT NULL,
    d_selesai date,
    i_pel integer,
    e_contact_person character varying(50),
    f_repeat boolean,
    i_desain character varying(16),
    e_motif character varying(50),
    e_material character varying(50),
    e_original_cond1 character varying(25),
    e_ket_ori_cond character varying(50),
    e_color character varying(50),
    i_penyedia integer,
    d_tgl_material_in date,
    n_qty_material numeric,
    n_pengkerutan numeric,
    e_gramasi_from character varying(50),
    e_gramasi_to character varying(50),
    e_penyesuaian_lebar_from character varying(50),
    e_penyesuaian_lebar_to character varying(50),
    e_pakan_from character varying(50),
    e_pakan_to character varying(50),
    e_lusi_from character varying(50),
    e_lusi_to character varying(50),
    e_tekstur_akhir_from character varying(50),
    e_tekstur_akhir_to character varying(50),
    e_cw_1 character varying(50),
    e_cw_2 character varying(50),
    e_cw_3 character varying(50),
    e_cw_4 character varying(50),
    e_cw_5 character varying(50),
    e_cw_6 character varying(50),
    e_cw_7 character varying(50),
    e_cw_8 character varying(50),
    i_status integer,
    d_approved_mrk timestamp(6) without time zone,
    d_approved_pro timestamp(6) without time zone,
    d_approved_ppc timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_so integer,
    e_reject_mkt character varying(200),
    e_reject_prod character varying(200),
    e_original_cond2 character varying(25),
    e_jenis_printing character varying(25),
    i_jns_kain integer,
    urutan_fifo integer DEFAULT nextval('public.rfp_urutan_fifo_seq'::regclass) NOT NULL,
    f_proses boolean DEFAULT false,
    old_urutan_fifo integer,
    d_proses timestamp with time zone,
    e_ket_rfp character varying(250),
    e_material_others character varying(50),
    e_color_others character varying(50)
);
    DROP TABLE public.rfp;
       public         postgres    false    228            �            1259    65929    rfp_lpk    TABLE     �   CREATE TABLE public.rfp_lpk (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_nomor_lpk character varying(150),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    qty_roll numeric
);
    DROP TABLE public.rfp_lpk;
       public         postgres    false            �            1259    65932    rfp_lpk_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_lpk_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.rfp_lpk_i_id_seq;
       public       postgres    false    230                       0    0    rfp_lpk_i_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.rfp_lpk_i_id_seq OWNED BY public.rfp_lpk.i_id;
            public       postgres    false    231            �            1259    65934    tx_jns_proses_kartu    TABLE     �   CREATE TABLE public.tx_jns_proses_kartu (
    i_id integer NOT NULL,
    description character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 '   DROP TABLE public.tx_jns_proses_kartu;
       public         postgres    false            �            1259    65937    tx_jns_proses_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_jns_proses_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tx_jns_proses_kartu_i_id_seq;
       public       postgres    false    232                       0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tx_jns_proses_kartu_i_id_seq OWNED BY public.tx_jns_proses_kartu.i_id;
            public       postgres    false    233            �            1259    65939    tx_rfp_accepted    TABLE       CREATE TABLE public.tx_rfp_accepted (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    d_accepted timestamp(4) without time zone NOT NULL,
    user_accept integer NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.tx_rfp_accepted;
       public         postgres    false            �            1259    65942    tx_rfp_accepted_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_rfp_accepted_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.tx_rfp_accepted_i_id_seq;
       public       postgres    false    234                       0    0    tx_rfp_accepted_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.tx_rfp_accepted_i_id_seq OWNED BY public.tx_rfp_accepted.i_id;
            public       postgres    false    235            �            1259    82201    tx_sisa_uang_masuk    TABLE     �   CREATE TABLE public.tx_sisa_uang_masuk (
    i_uang_masuk integer NOT NULL,
    i_pel integer,
    v_sisa_uang_masuk numeric DEFAULT 0,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 &   DROP TABLE public.tx_sisa_uang_masuk;
       public         postgres    false            �            1259    82199 #   tx_sisa_uang_masuk_i_uang_masuk_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sisa_uang_masuk_i_uang_masuk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.tx_sisa_uang_masuk_i_uang_masuk_seq;
       public       postgres    false    255                       0    0 #   tx_sisa_uang_masuk_i_uang_masuk_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.tx_sisa_uang_masuk_i_uang_masuk_seq OWNED BY public.tx_sisa_uang_masuk.i_uang_masuk;
            public       postgres    false    254            �            1259    65944    tx_sj_kartu    TABLE     �   CREATE TABLE public.tx_sj_kartu (
    i_id integer NOT NULL,
    i_no_sj character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.tx_sj_kartu;
       public         postgres    false            �            1259    65947    tx_sj_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sj_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tx_sj_kartu_i_id_seq;
       public       postgres    false    236                       0    0    tx_sj_kartu_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tx_sj_kartu_i_id_seq OWNED BY public.tx_sj_kartu.i_id;
            public       postgres    false    237            �            1259    65949    tx_spec_kain    TABLE     �  CREATE TABLE public.tx_spec_kain (
    i_id integer NOT NULL,
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_desain character varying(16)
);
     DROP TABLE public.tx_spec_kain;
       public         postgres    false            �            1259    65952    tx_spec_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tx_spec_kain_i_id_seq;
       public       postgres    false    238                       0    0    tx_spec_kain_i_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tx_spec_kain_i_id_seq OWNED BY public.tx_spec_kain.i_id;
            public       postgres    false    239            �            1259    65954    tx_spec_pekerjaan    TABLE     �  CREATE TABLE public.tx_spec_pekerjaan (
    i_id integer NOT NULL,
    i_desain character varying(16) NOT NULL,
    i_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_colow_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 %   DROP TABLE public.tx_spec_pekerjaan;
       public         postgres    false            �            1259    65957    tx_spec_pekerjaan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_pekerjaan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tx_spec_pekerjaan_i_id_seq;
       public       postgres    false    240                       0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tx_spec_pekerjaan_i_id_seq OWNED BY public.tx_spec_pekerjaan.i_id;
            public       postgres    false    241            �            1259    65959    tx_workstation    TABLE       CREATE TABLE public.tx_workstation (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_cw character varying(100) NOT NULL,
    i_id_bagian integer NOT NULL,
    n_tot_qty numeric,
    n_proses numeric,
    n_sisa numeric,
    e_ket character varying(250),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_pelaksana character varying(75),
    e_shift character varying(50),
    d_tgl date,
    i_id_kartu integer NOT NULL,
    i_cw integer,
    d_time character varying(10)
);
 "   DROP TABLE public.tx_workstation;
       public         postgres    false            �            1259    65965    tx_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tx_workstation_i_id_seq;
       public       postgres    false    242                       0    0    tx_workstation_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tx_workstation_i_id_seq OWNED BY public.tx_workstation.i_id;
            public       postgres    false    243            �            1259    65967    users    TABLE     A  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    username character varying(255) NOT NULL,
    role integer,
    login_pertama boolean DEFAULT true,
    is_active boolean DEFAULT true,
    count_reset_password integer,
    status character varying(255)
);
    DROP TABLE public.users;
       public         postgres    false            �            1259    65973    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 15
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    244                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    245            �           2604    65975    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196            �           2604    82264    mst_invoice_item i_invoice_item    DEFAULT     �   ALTER TABLE ONLY public.mst_invoice_item ALTER COLUMN i_invoice_item SET DEFAULT nextval('public.mst_invoice_item_i_invoice_item_seq'::regclass);
 N   ALTER TABLE public.mst_invoice_item ALTER COLUMN i_invoice_item DROP DEFAULT;
       public       postgres    false    269    268    269            �           2604    90368    mst_menu_role i_id    DEFAULT     x   ALTER TABLE ONLY public.mst_menu_role ALTER COLUMN i_id SET DEFAULT nextval('public.mst_menu_role_i_id_seq'::regclass);
 A   ALTER TABLE public.mst_menu_role ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    272    273    273            �           2604    73979    mst_nota_item i_nota_item    DEFAULT     �   ALTER TABLE ONLY public.mst_nota_item ALTER COLUMN i_nota_item SET DEFAULT nextval('public.mst_nota_item_i_nota_item_seq'::regclass);
 H   ALTER TABLE public.mst_nota_item ALTER COLUMN i_nota_item DROP DEFAULT;
       public       postgres    false    248    249    249            �           2604    65976    mst_packing_list i_id    DEFAULT     ~   ALTER TABLE ONLY public.mst_packing_list ALTER COLUMN i_id SET DEFAULT nextval('public.mst_packing_list_i_id_seq'::regclass);
 D   ALTER TABLE public.mst_packing_list ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    201    200            �           2604    82191 #   mst_pelunasan_item i_pelunasan_item    DEFAULT     �   ALTER TABLE ONLY public.mst_pelunasan_item ALTER COLUMN i_pelunasan_item SET DEFAULT nextval('public.mst_pelunasan_item_i_pelunasan_item_seq'::regclass);
 R   ALTER TABLE public.mst_pelunasan_item ALTER COLUMN i_pelunasan_item DROP DEFAULT;
       public       postgres    false    253    252    253            �           2604    65977    mst_sales_plan i_id    DEFAULT     z   ALTER TABLE ONLY public.mst_sales_plan ALTER COLUMN i_id SET DEFAULT nextval('public.mst_sales_plan_i_id_seq'::regclass);
 B   ALTER TABLE public.mst_sales_plan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    203    202            �           2604    65978    mst_so_item i_id    DEFAULT     t   ALTER TABLE ONLY public.mst_so_item ALTER COLUMN i_id SET DEFAULT nextval('public.mst_so_item_i_id_seq'::regclass);
 ?   ALTER TABLE public.mst_so_item ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    206    205            �           2604    65979    ref_bagian i_id    DEFAULT     w   ALTER TABLE ONLY public.ref_bagian ALTER COLUMN i_id SET DEFAULT nextval('public.ref_workstation_i_id_seq'::regclass);
 >   ALTER TABLE public.ref_bagian ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    227    208            �           2604    82232    ref_jns_bahan i_id    DEFAULT     x   ALTER TABLE ONLY public.ref_jns_bahan ALTER COLUMN i_id SET DEFAULT nextval('public.ref_jns_bahan_i_id_seq'::regclass);
 A   ALTER TABLE public.ref_jns_bahan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    261    260    261            �           2604    82216    ref_jns_printing i_id    DEFAULT     ~   ALTER TABLE ONLY public.ref_jns_printing ALTER COLUMN i_id SET DEFAULT nextval('public.ref_jns_printing_i_id_seq'::regclass);
 D   ALTER TABLE public.ref_jns_printing ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    256    257    257            �           2604    65980    ref_kain i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kain_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    210    209            �           2604    65981    ref_ket_umum_so_child i_id    DEFAULT     �   ALTER TABLE ONLY public.ref_ket_umum_so_child ALTER COLUMN i_id SET DEFAULT nextval('public.ref_ket_umum_so_child_i_id_seq'::regclass);
 I   ALTER TABLE public.ref_ket_umum_so_child ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    213    212            �           2604    82224    ref_kondisi_kain i_id    DEFAULT     ~   ALTER TABLE ONLY public.ref_kondisi_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kondisi_kain_i_id_seq'::regclass);
 D   ALTER TABLE public.ref_kondisi_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    258    259    259            �           2604    65982    ref_logo i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_logo ALTER COLUMN i_id SET DEFAULT nextval('public.ref_logo_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_logo ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    215    214            �           2604    65983    ref_no_urut id    DEFAULT     p   ALTER TABLE ONLY public.ref_no_urut ALTER COLUMN id SET DEFAULT nextval('public.ref_no_urut_id_seq'::regclass);
 =   ALTER TABLE public.ref_no_urut ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    216            �           2604    82240    ref_ori_kondisi i_id    DEFAULT     |   ALTER TABLE ONLY public.ref_ori_kondisi ALTER COLUMN i_id SET DEFAULT nextval('public.ref_ori_kondisi_i_id_seq'::regclass);
 C   ALTER TABLE public.ref_ori_kondisi ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    263    262    263            �           2604    65984    ref_pelanggan i_pel    DEFAULT     z   ALTER TABLE ONLY public.ref_pelanggan ALTER COLUMN i_pel SET DEFAULT nextval('public.ref_pelanggan_i_pel_seq'::regclass);
 B   ALTER TABLE public.ref_pelanggan ALTER COLUMN i_pel DROP DEFAULT;
       public       postgres    false    219    218            �           2604    65985    ref_role i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_role ALTER COLUMN i_id SET DEFAULT nextval('public.ref_role_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_role ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    221    220            �           2604    65986    ref_sat_qty i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_sat_qty ALTER COLUMN i_id SET DEFAULT nextval('public.ref_sat_qty_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_sat_qty ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    223    222            �           2604    65987    ref_so_flag i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_so_flag ALTER COLUMN i_id SET DEFAULT nextval('public.ref_so_flag_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_so_flag ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    225    224            �           2604    82256    ref_tekstur_akhir i_id    DEFAULT     �   ALTER TABLE ONLY public.ref_tekstur_akhir ALTER COLUMN i_id SET DEFAULT nextval('public.ref_tekstur_akhir_i_id_seq'::regclass);
 E   ALTER TABLE public.ref_tekstur_akhir ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    266    267    267            �           2604    82248    ref_warna_dasar i_id    DEFAULT     |   ALTER TABLE ONLY public.ref_warna_dasar ALTER COLUMN i_id SET DEFAULT nextval('public.ref_warna_dasar_i_id_seq'::regclass);
 C   ALTER TABLE public.ref_warna_dasar ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    265    264    265            �           2604    65988    rfp_lpk i_id    DEFAULT     l   ALTER TABLE ONLY public.rfp_lpk ALTER COLUMN i_id SET DEFAULT nextval('public.rfp_lpk_i_id_seq'::regclass);
 ;   ALTER TABLE public.rfp_lpk ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    231    230            �           2604    65989    tx_jns_proses_kartu i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_jns_proses_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_jns_proses_kartu_i_id_seq'::regclass);
 G   ALTER TABLE public.tx_jns_proses_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    233    232            �           2604    65990    tx_rfp_accepted i_id    DEFAULT     |   ALTER TABLE ONLY public.tx_rfp_accepted ALTER COLUMN i_id SET DEFAULT nextval('public.tx_rfp_accepted_i_id_seq'::regclass);
 C   ALTER TABLE public.tx_rfp_accepted ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    235    234            �           2604    82204    tx_sisa_uang_masuk i_uang_masuk    DEFAULT     �   ALTER TABLE ONLY public.tx_sisa_uang_masuk ALTER COLUMN i_uang_masuk SET DEFAULT nextval('public.tx_sisa_uang_masuk_i_uang_masuk_seq'::regclass);
 N   ALTER TABLE public.tx_sisa_uang_masuk ALTER COLUMN i_uang_masuk DROP DEFAULT;
       public       postgres    false    255    254    255            �           2604    65991    tx_sj_kartu i_id    DEFAULT     t   ALTER TABLE ONLY public.tx_sj_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_sj_kartu_i_id_seq'::regclass);
 ?   ALTER TABLE public.tx_sj_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    237    236            �           2604    65992    tx_spec_kain i_id    DEFAULT     v   ALTER TABLE ONLY public.tx_spec_kain ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_kain_i_id_seq'::regclass);
 @   ALTER TABLE public.tx_spec_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    239    238            �           2604    65993    tx_spec_pekerjaan i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_spec_pekerjaan ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_pekerjaan_i_id_seq'::regclass);
 E   ALTER TABLE public.tx_spec_pekerjaan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    241    240            �           2604    65994    tx_workstation i_id    DEFAULT     z   ALTER TABLE ONLY public.tx_workstation ALTER COLUMN i_id SET DEFAULT nextval('public.tx_workstation_i_id_seq'::regclass);
 B   ALTER TABLE public.tx_workstation ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    243    242            �           2604    65995    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    245    244            �          0    65802 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    196   �b      �          0    66070 	   mst_do_sj 
   TABLE DATA               F  COPY public.mst_do_sj (i_id, i_id_kartu, i_id_rfp, i_no_sj, d_sj, i_pel, d_due_date, n_total_cw, e_ket, n_tot_roll, n_tot_asal_sj, n_tot_asal_kp, n_tot_jadi_kp, n_total_tagihan, n_sisa_tagihan, f_print, f_lunas, created_at, updated_at, tgl_produksi, expedisi, i_status, alasan_reject, d_approved, f_nota, i_id_so) FROM stdin;
    public       postgres    false    246   �b      �          0    82149    mst_invoice 
   TABLE DATA               �   COPY public.mst_invoice (i_invoice, i_invoice_code, i_pel, d_invoice, v_total_invoice, v_total_invoice_sisa, f_lunas, f_cancel, created_at, updated_at) FROM stdin;
    public       postgres    false    250   �c      �          0    82261    mst_invoice_item 
   TABLE DATA               �   COPY public.mst_invoice_item (i_invoice_item, i_invoice, i_nota, v_qty_nota, v_nilai_nota, f_lunas, f_cancel, created_at, updated_at) FROM stdin;
    public       postgres    false    269   �d      �          0    65807 	   mst_kartu 
   TABLE DATA               �   COPY public.mst_kartu (i_id, i_no_kartu, d_pengiriman, i_id_so, i_id_rfp, e_lebar_blanket, e_handfeel, d_beres, created_at, updated_at, e_ket, i_status, tot_roll, tot_kg, tot_pjg, e_lebar_kain_jadi, tgl_produksi) FROM stdin;
    public       postgres    false    198   -e      �          0    65813    mst_kartu_cw 
   TABLE DATA               �   COPY public.mst_kartu_cw (i_id, i_id_kartu, e_cw, n_qty_roll, n_qty_pjg, n_qty_kg, created_at, updated_at, n_qty_kg_sisa, i_id_bagian, d_strike_off, e_jenis_kain) FROM stdin;
    public       postgres    false    199   �e      �          0    90365    mst_menu_role 
   TABLE DATA               j   COPY public.mst_menu_role (i_id, role_id, id_menu_head, id_menu_item, created_at, updated_at) FROM stdin;
    public       postgres    false    273   �e      �          0    73957    mst_nota 
   TABLE DATA               �   COPY public.mst_nota (i_nota, i_nota_code, i_pel, d_nota, d_due_date, n_diskon, v_diskon, n_tot_qty, v_total_nota, v_total_nppn, v_total_nppn_sisa, f_invoice, f_lunas, f_cancel, created_at, updated_at, n_ppn, v_ppn, i_qty_from) FROM stdin;
    public       postgres    false    247   �f      �          0    73976    mst_nota_item 
   TABLE DATA               {   COPY public.mst_nota_item (i_nota_item, i_nota, i_sj, n_qty, v_hrg_satuan, i_qty_from, created_at, updated_at) FROM stdin;
    public       postgres    false    249   mg      �          0    65820    mst_packing_list 
   TABLE DATA               �   COPY public.mst_packing_list (i_id, i_id_rfp, i_cw, e_kode, n_asal_sj, n_asal_kp, n_jadi_kp, f_jadi_sj, created_at, updated_at, i_id_kartu, i_no_packing, i_id_sj) FROM stdin;
    public       postgres    false    200   �g      �          0    82176    mst_pelunasan 
   TABLE DATA               �   COPY public.mst_pelunasan (i_pelunasan, i_pelunasan_code, i_pel, d_pelunasan, v_pelunasan, f_cancel, created_at, updated_at, keterangan, i_invoice) FROM stdin;
    public       postgres    false    251   .i      �          0    82188    mst_pelunasan_item 
   TABLE DATA               �   COPY public.mst_pelunasan_item (i_pelunasan_item, i_pelunasan, i_invoice, i_nota, v_bayar_nota, f_cancel, created_at, updated_at) FROM stdin;
    public       postgres    false    253   �i      �          0    65832    mst_sales_plan 
   TABLE DATA               [   COPY public.mst_sales_plan (i_id, n_qty, bulan, tahun, created_at, updated_at) FROM stdin;
    public       postgres    false    202   j      �          0    65841    mst_so 
   TABLE DATA                 COPY public.mst_so (i_id, i_no_so, d_so, i_pel, f_repeat, i_desain, n_qty_warna, e_motif, e_jenis_printing, n_color_way, e_color_way, e_toleransi_cacat, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, d_strike_off, d_approval_strike_off, d_penyerahan_brg, e_keterangan_kirim, i_status, d_approved, v_pekerjaan, v_pekerjaan_plus_ppn, v_discount, n_discount, v_sisa, created_at, updated_at, e_reject, i_no_po, flag_so, hitung_by, n_ppn, v_ppn, exclude_include, cara_bayar) FROM stdin;
    public       postgres    false    204   �j      �          0    65847    mst_so_item 
   TABLE DATA               �   COPY public.mst_so_item (i_id, i_id_so, e_uraian_pekerjaan, n_qty_roll, n_qty_pjg, n_qty_kg, v_harga_sat, created_at, updated_at, n_qty_kg_sisa, i_id_bagian, d_strike_off, e_jenis_kain) FROM stdin;
    public       postgres    false    205   �k      �          0    65856    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    207   �l      �          0    65862 
   ref_bagian 
   TABLE DATA               c   COPY public.ref_bagian (i_id, nama_bagian, e_penanggung_jawab, created_at, updated_at) FROM stdin;
    public       postgres    false    208   �l      �          0    82229    ref_jns_bahan 
   TABLE DATA               R   COPY public.ref_jns_bahan (i_id, e_jns_bahan, created_at, updated_at) FROM stdin;
    public       postgres    false    261   Sm      �          0    82213    ref_jns_printing 
   TABLE DATA               X   COPY public.ref_jns_printing (i_id, e_jns_printing, created_at, updated_at) FROM stdin;
    public       postgres    false    257   �m      �          0    65865    ref_kain 
   TABLE DATA               H   COPY public.ref_kain (i_id, e_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    209   2n      �          0    65870    ref_ket_umum_so 
   TABLE DATA               S   COPY public.ref_ket_umum_so (i_id, e_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    211   o      �          0    65873    ref_ket_umum_so_child 
   TABLE DATA               n   COPY public.ref_ket_umum_so_child (i_id, i_id_ket_umum, e_child_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    212   �p      �          0    82221    ref_kondisi_kain 
   TABLE DATA               X   COPY public.ref_kondisi_kain (i_id, e_kondisi_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    259   r      �          0    65878    ref_logo 
   TABLE DATA               U   COPY public.ref_logo (i_id, logo_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    214   mr      �          0    90352    ref_menu_head 
   TABLE DATA               r   COPY public.ref_menu_head (i_id, nama_menu, icon_menu, rut_name, stand_alone, created_at, updated_at) FROM stdin;
    public       postgres    false    270   �r      �          0    90358    ref_menu_item 
   TABLE DATA               {   COPY public.ref_menu_item (i_id, id_menu_head, nama_sub_menu, icon_sub_menu, rut_name, created_at, updated_at) FROM stdin;
    public       postgres    false    271   ds      �          0    65884    ref_no_urut 
   TABLE DATA               K   COPY public.ref_no_urut (id, code, no_urut, bln, thn, flag_so) FROM stdin;
    public       postgres    false    216   nv      �          0    82237    ref_ori_kondisi 
   TABLE DATA               \   COPY public.ref_ori_kondisi (i_id, e_ori_kondisi, tipe, created_at, updated_at) FROM stdin;
    public       postgres    false    263   �v      �          0    65889    ref_pelanggan 
   TABLE DATA               �   COPY public.ref_pelanggan (i_pel, e_nama_pel, e_alamat_pel, f_pkp, e_npwp_pel, e_telp_pel, e_kont_pel, created_at, updated_at, e_fax_pel, e_kota_pel, e_kode_marketing, n_jth_tempo) FROM stdin;
    public       postgres    false    218   ?w      �          0    65898    ref_role 
   TABLE DATA               M   COPY public.ref_role (i_id, e_role_name, created_at, updated_at) FROM stdin;
    public       postgres    false    220   ��      �          0    65903    ref_sat_qty 
   TABLE DATA               J   COPY public.ref_sat_qty (i_id, e_sat, created_at, updated_at) FROM stdin;
    public       postgres    false    222   \�      �          0    65908    ref_so_flag 
   TABLE DATA               X   COPY public.ref_so_flag (i_id, flag_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    224   y�      �          0    82253    ref_tekstur_akhir 
   TABLE DATA               T   COPY public.ref_tekstur_akhir (i_id, e_tekstur, created_at, updated_at) FROM stdin;
    public       postgres    false    267   σ      �          0    82245    ref_warna_dasar 
   TABLE DATA               V   COPY public.ref_warna_dasar (i_id, e_warna_dasar, created_at, updated_at) FROM stdin;
    public       postgres    false    265   $�      �          0    65914    ref_workflow 
   TABLE DATA               P   COPY public.ref_workflow (i_id, definition, created_at, updated_at) FROM stdin;
    public       postgres    false    226   j�      �          0    65921    rfp 
   TABLE DATA               �  COPY public.rfp (i_id, i_no_rfp, d_selesai, i_pel, e_contact_person, f_repeat, i_desain, e_motif, e_material, e_original_cond1, e_ket_ori_cond, e_color, i_penyedia, d_tgl_material_in, n_qty_material, n_pengkerutan, e_gramasi_from, e_gramasi_to, e_penyesuaian_lebar_from, e_penyesuaian_lebar_to, e_pakan_from, e_pakan_to, e_lusi_from, e_lusi_to, e_tekstur_akhir_from, e_tekstur_akhir_to, e_cw_1, e_cw_2, e_cw_3, e_cw_4, e_cw_5, e_cw_6, e_cw_7, e_cw_8, i_status, d_approved_mrk, d_approved_pro, d_approved_ppc, created_at, updated_at, i_id_so, e_reject_mkt, e_reject_prod, e_original_cond2, e_jenis_printing, i_jns_kain, urutan_fifo, f_proses, old_urutan_fifo, d_proses, e_ket_rfp, e_material_others, e_color_others) FROM stdin;
    public       postgres    false    229   H�      �          0    65929    rfp_lpk 
   TABLE DATA               `   COPY public.rfp_lpk (i_id, i_id_rfp, e_nomor_lpk, created_at, updated_at, qty_roll) FROM stdin;
    public       postgres    false    230   -�      �          0    65934    tx_jns_proses_kartu 
   TABLE DATA               d   COPY public.tx_jns_proses_kartu (i_id, description, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    232   z�      �          0    65939    tx_rfp_accepted 
   TABLE DATA               j   COPY public.tx_rfp_accepted (i_id, i_id_rfp, d_accepted, user_accept, created_at, updated_at) FROM stdin;
    public       postgres    false    234   Ć      �          0    82201    tx_sisa_uang_masuk 
   TABLE DATA               l   COPY public.tx_sisa_uang_masuk (i_uang_masuk, i_pel, v_sisa_uang_masuk, created_at, updated_at) FROM stdin;
    public       postgres    false    255   U�      �          0    65944    tx_sj_kartu 
   TABLE DATA               X   COPY public.tx_sj_kartu (i_id, i_no_sj, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    236   ��      �          0    65949    tx_spec_kain 
   TABLE DATA               �   COPY public.tx_spec_kain (i_id, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, i_pel, created_at, updated_at, i_desain) FROM stdin;
    public       postgres    false    238   އ      �          0    65954    tx_spec_pekerjaan 
   TABLE DATA               �   COPY public.tx_spec_pekerjaan (i_id, i_desain, i_qty_warna, e_motif, e_jenis_printing, n_colow_way, e_color_way, e_toleransi_cacat, i_pel, created_at, updated_at) FROM stdin;
    public       postgres    false    240   ,�      �          0    65959    tx_workstation 
   TABLE DATA               �   COPY public.tx_workstation (i_id, i_id_rfp, e_cw, i_id_bagian, n_tot_qty, n_proses, n_sisa, e_ket, created_at, updated_at, e_pelaksana, e_shift, d_tgl, i_id_kartu, i_cw, d_time) FROM stdin;
    public       postgres    false    242   ��      �          0    65967    users 
   TABLE DATA               �   COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, username, role, login_pertama, is_active, count_reset_password, status) FROM stdin;
    public       postgres    false    244   R�                 0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    197                       0    0 #   mst_invoice_item_i_invoice_item_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.mst_invoice_item_i_invoice_item_seq', 9, true);
            public       postgres    false    268                       0    0    mst_menu_role_i_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.mst_menu_role_i_id_seq', 46, true);
            public       postgres    false    272                       0    0    mst_nota_item_i_nota_item_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.mst_nota_item_i_nota_item_seq', 21, true);
            public       postgres    false    248                       0    0    mst_packing_list_i_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.mst_packing_list_i_id_seq', 293, true);
            public       postgres    false    201                       0    0 '   mst_pelunasan_item_i_pelunasan_item_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.mst_pelunasan_item_i_pelunasan_item_seq', 8, true);
            public       postgres    false    252                       0    0    mst_sales_plan_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.mst_sales_plan_i_id_seq', 42, true);
            public       postgres    false    203                       0    0    mst_so_item_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.mst_so_item_i_id_seq', 98, true);
            public       postgres    false    206                       0    0    ref_jns_bahan_i_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.ref_jns_bahan_i_id_seq', 4, true);
            public       postgres    false    260                       0    0    ref_jns_printing_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_jns_printing_i_id_seq', 9, true);
            public       postgres    false    256                        0    0    ref_kain_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_kain_i_id_seq', 21, true);
            public       postgres    false    210            !           0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ref_ket_umum_so_child_i_id_seq', 7, true);
            public       postgres    false    213            "           0    0    ref_kondisi_kain_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_kondisi_kain_i_id_seq', 5, true);
            public       postgres    false    258            #           0    0    ref_logo_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.ref_logo_i_id_seq', 4, true);
            public       postgres    false    215            $           0    0    ref_no_urut_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.ref_no_urut_id_seq', 22, true);
            public       postgres    false    217            %           0    0    ref_ori_kondisi_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ref_ori_kondisi_i_id_seq', 8, true);
            public       postgres    false    262            &           0    0    ref_pelanggan_i_pel_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ref_pelanggan_i_pel_seq', 66, true);
            public       postgres    false    219            '           0    0    ref_role_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_role_i_id_seq', 16, true);
            public       postgres    false    221            (           0    0    ref_sat_qty_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.ref_sat_qty_i_id_seq', 1, false);
            public       postgres    false    223            )           0    0    ref_so_flag_i_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ref_so_flag_i_id_seq', 6, true);
            public       postgres    false    225            *           0    0    ref_tekstur_akhir_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.ref_tekstur_akhir_i_id_seq', 6, true);
            public       postgres    false    266            +           0    0    ref_warna_dasar_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ref_warna_dasar_i_id_seq', 5, true);
            public       postgres    false    264            ,           0    0    ref_workstation_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.ref_workstation_i_id_seq', 11, false);
            public       postgres    false    227            -           0    0    rfp_lpk_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.rfp_lpk_i_id_seq', 51, true);
            public       postgres    false    231            .           0    0    rfp_urutan_fifo_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.rfp_urutan_fifo_seq', 61, true);
            public       postgres    false    228            /           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tx_jns_proses_kartu_i_id_seq', 69, true);
            public       postgres    false    233            0           0    0    tx_rfp_accepted_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.tx_rfp_accepted_i_id_seq', 18, true);
            public       postgres    false    235            1           0    0 #   tx_sisa_uang_masuk_i_uang_masuk_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.tx_sisa_uang_masuk_i_uang_masuk_seq', 3, true);
            public       postgres    false    254            2           0    0    tx_sj_kartu_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_sj_kartu_i_id_seq', 66, true);
            public       postgres    false    237            3           0    0    tx_spec_kain_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_spec_kain_i_id_seq', 7, true);
            public       postgres    false    239            4           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tx_spec_pekerjaan_i_id_seq', 7, true);
            public       postgres    false    241            5           0    0    tx_workstation_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tx_workstation_i_id_seq', 63, true);
            public       postgres    false    243            6           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 18, true);
            public       postgres    false    245            �           2606    65997    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    196                       2606    66079    mst_do_sj mst_do_sj_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_do_sj
    ADD CONSTRAINT mst_do_sj_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_do_sj DROP CONSTRAINT mst_do_sj_pkey;
       public         postgres    false    246            !           2606    82276 &   mst_invoice_item mst_invoice_item_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.mst_invoice_item
    ADD CONSTRAINT mst_invoice_item_pkey PRIMARY KEY (i_invoice_item);
 P   ALTER TABLE ONLY public.mst_invoice_item DROP CONSTRAINT mst_invoice_item_pkey;
       public         postgres    false    269                       2606    82160    mst_invoice mst_invoice_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.mst_invoice
    ADD CONSTRAINT mst_invoice_pkey PRIMARY KEY (i_invoice);
 F   ALTER TABLE ONLY public.mst_invoice DROP CONSTRAINT mst_invoice_pkey;
       public         postgres    false    250            �           2606    65999    mst_kartu_cw mst_kartu_cw_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mst_kartu_cw
    ADD CONSTRAINT mst_kartu_cw_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.mst_kartu_cw DROP CONSTRAINT mst_kartu_cw_pkey;
       public         postgres    false    199            �           2606    66001    mst_kartu mst_kartu_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_kartu
    ADD CONSTRAINT mst_kartu_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_kartu DROP CONSTRAINT mst_kartu_pkey;
       public         postgres    false    198            '           2606    90370     mst_menu_role mst_menu_role_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.mst_menu_role
    ADD CONSTRAINT mst_menu_role_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.mst_menu_role DROP CONSTRAINT mst_menu_role_pkey;
       public         postgres    false    273                       2606    73986     mst_nota_item mst_nota_item_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.mst_nota_item
    ADD CONSTRAINT mst_nota_item_pkey PRIMARY KEY (i_nota_item);
 J   ALTER TABLE ONLY public.mst_nota_item DROP CONSTRAINT mst_nota_item_pkey;
       public         postgres    false    249            	           2606    73973    mst_nota mst_nota_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_nota
    ADD CONSTRAINT mst_nota_pkey PRIMARY KEY (i_nota);
 @   ALTER TABLE ONLY public.mst_nota DROP CONSTRAINT mst_nota_pkey;
       public         postgres    false    247            �           2606    66003 &   mst_packing_list mst_packing_list_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mst_packing_list
    ADD CONSTRAINT mst_packing_list_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.mst_packing_list DROP CONSTRAINT mst_packing_list_pkey;
       public         postgres    false    200                       2606    82198 *   mst_pelunasan_item mst_pelunasan_item_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.mst_pelunasan_item
    ADD CONSTRAINT mst_pelunasan_item_pkey PRIMARY KEY (i_pelunasan_item);
 T   ALTER TABLE ONLY public.mst_pelunasan_item DROP CONSTRAINT mst_pelunasan_item_pkey;
       public         postgres    false    253                       2606    82185     mst_pelunasan mst_pelunasan_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.mst_pelunasan
    ADD CONSTRAINT mst_pelunasan_pkey PRIMARY KEY (i_pelunasan);
 J   ALTER TABLE ONLY public.mst_pelunasan DROP CONSTRAINT mst_pelunasan_pkey;
       public         postgres    false    251            �           2606    66005 "   mst_sales_plan mst_sales_plan_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mst_sales_plan
    ADD CONSTRAINT mst_sales_plan_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.mst_sales_plan DROP CONSTRAINT mst_sales_plan_pkey;
       public         postgres    false    202            �           2606    66007    mst_so_item mst_so_item_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.mst_so_item
    ADD CONSTRAINT mst_so_item_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.mst_so_item DROP CONSTRAINT mst_so_item_pkey;
       public         postgres    false    205            �           2606    66009    mst_so mst_so_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.mst_so
    ADD CONSTRAINT mst_so_pkey PRIMARY KEY (i_id);
 <   ALTER TABLE ONLY public.mst_so DROP CONSTRAINT mst_so_pkey;
       public         postgres    false    204                       2606    82234     ref_jns_bahan ref_jns_bahan_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.ref_jns_bahan
    ADD CONSTRAINT ref_jns_bahan_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.ref_jns_bahan DROP CONSTRAINT ref_jns_bahan_pkey;
       public         postgres    false    261                       2606    82218 &   ref_jns_printing ref_jns_printing_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ref_jns_printing
    ADD CONSTRAINT ref_jns_printing_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.ref_jns_printing DROP CONSTRAINT ref_jns_printing_pkey;
       public         postgres    false    257            �           2606    66011    ref_kain ref_kain_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_kain
    ADD CONSTRAINT ref_kain_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_kain DROP CONSTRAINT ref_kain_pkey;
       public         postgres    false    209            �           2606    66013 0   ref_ket_umum_so_child ref_ket_umum_so_child_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.ref_ket_umum_so_child
    ADD CONSTRAINT ref_ket_umum_so_child_pkey PRIMARY KEY (i_id);
 Z   ALTER TABLE ONLY public.ref_ket_umum_so_child DROP CONSTRAINT ref_ket_umum_so_child_pkey;
       public         postgres    false    212            �           2606    66015 $   ref_ket_umum_so ref_ket_umum_so_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_ket_umum_so
    ADD CONSTRAINT ref_ket_umum_so_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_ket_umum_so DROP CONSTRAINT ref_ket_umum_so_pkey;
       public         postgres    false    211                       2606    82226 &   ref_kondisi_kain ref_kondisi_kain_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ref_kondisi_kain
    ADD CONSTRAINT ref_kondisi_kain_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.ref_kondisi_kain DROP CONSTRAINT ref_kondisi_kain_pkey;
       public         postgres    false    259            �           2606    66017    ref_logo ref_logo_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_logo
    ADD CONSTRAINT ref_logo_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_logo DROP CONSTRAINT ref_logo_pkey;
       public         postgres    false    214            #           2606    90357     ref_menu_head ref_menu_head_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.ref_menu_head
    ADD CONSTRAINT ref_menu_head_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.ref_menu_head DROP CONSTRAINT ref_menu_head_pkey;
       public         postgres    false    270            %           2606    90362     ref_menu_item ref_menu_item_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.ref_menu_item
    ADD CONSTRAINT ref_menu_item_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.ref_menu_item DROP CONSTRAINT ref_menu_item_pkey;
       public         postgres    false    271            �           2606    66019    ref_no_urut ref_no_urut_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ref_no_urut
    ADD CONSTRAINT ref_no_urut_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ref_no_urut DROP CONSTRAINT ref_no_urut_pkey;
       public         postgres    false    216                       2606    82242 $   ref_ori_kondisi ref_ori_kondisi_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_ori_kondisi
    ADD CONSTRAINT ref_ori_kondisi_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_ori_kondisi DROP CONSTRAINT ref_ori_kondisi_pkey;
       public         postgres    false    263            �           2606    66021     ref_pelanggan ref_pelanggan_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.ref_pelanggan
    ADD CONSTRAINT ref_pelanggan_pkey PRIMARY KEY (i_pel);
 J   ALTER TABLE ONLY public.ref_pelanggan DROP CONSTRAINT ref_pelanggan_pkey;
       public         postgres    false    218            �           2606    66023    ref_role ref_role_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_role
    ADD CONSTRAINT ref_role_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_role DROP CONSTRAINT ref_role_pkey;
       public         postgres    false    220            �           2606    66025    ref_sat_qty ref_sat_qty_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_sat_qty
    ADD CONSTRAINT ref_sat_qty_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_sat_qty DROP CONSTRAINT ref_sat_qty_pkey;
       public         postgres    false    222            �           2606    66027    ref_so_flag ref_so_flag_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_so_flag
    ADD CONSTRAINT ref_so_flag_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_so_flag DROP CONSTRAINT ref_so_flag_pkey;
       public         postgres    false    224                       2606    82258 (   ref_tekstur_akhir ref_tekstur_akhir_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.ref_tekstur_akhir
    ADD CONSTRAINT ref_tekstur_akhir_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.ref_tekstur_akhir DROP CONSTRAINT ref_tekstur_akhir_pkey;
       public         postgres    false    267                       2606    82250 $   ref_warna_dasar ref_warna_dasar_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_warna_dasar
    ADD CONSTRAINT ref_warna_dasar_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_warna_dasar DROP CONSTRAINT ref_warna_dasar_pkey;
       public         postgres    false    265            �           2606    66029    ref_workflow ref_workflow_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.ref_workflow
    ADD CONSTRAINT ref_workflow_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.ref_workflow DROP CONSTRAINT ref_workflow_pkey;
       public         postgres    false    226            �           2606    66031    ref_bagian ref_workstation_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ref_bagian
    ADD CONSTRAINT ref_workstation_pkey PRIMARY KEY (i_id);
 I   ALTER TABLE ONLY public.ref_bagian DROP CONSTRAINT ref_workstation_pkey;
       public         postgres    false    208            �           2606    66033    rfp_lpk rfp_lpk_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.rfp_lpk
    ADD CONSTRAINT rfp_lpk_pkey PRIMARY KEY (i_id);
 >   ALTER TABLE ONLY public.rfp_lpk DROP CONSTRAINT rfp_lpk_pkey;
       public         postgres    false    230            �           2606    66035    rfp rfp_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.rfp
    ADD CONSTRAINT rfp_pkey PRIMARY KEY (i_id);
 6   ALTER TABLE ONLY public.rfp DROP CONSTRAINT rfp_pkey;
       public         postgres    false    229            �           2606    66037 ,   tx_jns_proses_kartu tx_jns_proses_kartu_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.tx_jns_proses_kartu
    ADD CONSTRAINT tx_jns_proses_kartu_pkey PRIMARY KEY (i_id);
 V   ALTER TABLE ONLY public.tx_jns_proses_kartu DROP CONSTRAINT tx_jns_proses_kartu_pkey;
       public         postgres    false    232            �           2606    66039 $   tx_rfp_accepted tx_rfp_accepted_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tx_rfp_accepted
    ADD CONSTRAINT tx_rfp_accepted_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.tx_rfp_accepted DROP CONSTRAINT tx_rfp_accepted_pkey;
       public         postgres    false    234                       2606    82210 *   tx_sisa_uang_masuk tx_sisa_uang_masuk_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.tx_sisa_uang_masuk
    ADD CONSTRAINT tx_sisa_uang_masuk_pkey PRIMARY KEY (i_uang_masuk);
 T   ALTER TABLE ONLY public.tx_sisa_uang_masuk DROP CONSTRAINT tx_sisa_uang_masuk_pkey;
       public         postgres    false    255            �           2606    66041    tx_sj_kartu tx_sj_kartu_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tx_sj_kartu
    ADD CONSTRAINT tx_sj_kartu_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.tx_sj_kartu DROP CONSTRAINT tx_sj_kartu_pkey;
       public         postgres    false    236            �           2606    66043    tx_spec_kain tx_spec_kain_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tx_spec_kain
    ADD CONSTRAINT tx_spec_kain_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.tx_spec_kain DROP CONSTRAINT tx_spec_kain_pkey;
       public         postgres    false    238            �           2606    66045 (   tx_spec_pekerjaan tx_spec_pekerjaan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tx_spec_pekerjaan
    ADD CONSTRAINT tx_spec_pekerjaan_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.tx_spec_pekerjaan DROP CONSTRAINT tx_spec_pekerjaan_pkey;
       public         postgres    false    240                       2606    66047 "   tx_workstation tx_workstation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tx_workstation
    ADD CONSTRAINT tx_workstation_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.tx_workstation DROP CONSTRAINT tx_workstation_pkey;
       public         postgres    false    242                       2606    66049    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    244                       2606    66051    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    244            �           1259    66052    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    207            �   H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.#d��(
����R�R�SKZb���� ]      �   �   x�}��j�0E����$H�e[~$Zڍ�{�y_��gvJ�4-��.���#P���cYI{���0\�l��L��7�=��1^��< !VQ��� �Go�*��s�#5H������Tl�͋0'ћ�3P��y�!1Y-���v�˴���#�js���{\:yUԦ��.$�D_J�E=�G_2*���S�z��h��
6p�ޠ�$C��K"{O�n��+��".�ס�?2�i�      �   �   x�}�1
�0�Y>�/��%[��k�'��%c%��vK�҂��L�e^���2; L�<8'L"A���Dk�y����h��n�7$G(k��y�$٪�F���G�b�z[�2x3�7��vM�ֲ��+�=�%p`���k=��<�ՖQ|,!5h�c^ջL      �   �   x�}���0�o<�pt��Ct�|g��⪍TU�����Qj��Z�t��ga)<3�1�폤a��$���g�{+=dsƲ����Kw�_}\�"���hXz���㼱A��p5מ&Y�1�G�"�#�p,X[�5h�RJO\<�      �   O   x�3���5 s0i�id`d�k`�k`�i�1~�>��N�! �������������������	V1�RC0	GU\1z\\\ d��      �   ]   x�}���0��,��vbg&���;H@��?�3�:@*���n%�AY<��I���Dї����J�#����]�h��t��+$Q�Dt A��      �   �   x�}��	�0Eѵ]�4����EL鿎��]�N�p�B�\�*����W�o�GH�"�h~���6n�oW�&����}�+�nȕ��3��]�'t_���<`� O�3�#�fx��
�2��h�|G��C�f e�I�����^=��箵� �)��      �   �   x�e�K� D��� �?� �^ �t�uN����)Ŗ0�?����K�HYj��(�#�Q*I҄>�.���I�X{�ե�I�%oi!�ӂ,�=W�s�Ҥ��)��L��C�G�&�O��@�@pij�q���ngw�ʃ@��Tq������{�O�Ӡ�q����0t��!� �B      �   `   x�]���0��,��N�d&��s�� K��|�ʊOT10�1X�&�R��2�{�^l�}�3'�Xې��Omh`�:�gb=��U����      �   A  x����m�0�3U���.��-)���
�?B#B��\t �i5��h	��F!���������,+����U��Y9W�J��~>Y$�V��$�w�Rz����3����\ �(|����+�3�*��M9�ڀx3��9
r�璝���r2�q��WU�9F �A��9=�q�c '��r��<�>'�a�# GA�%υ���-�+rs�h���?a���,��V5I�APJ�	e�� �ܝ��A�P� AA�������2�PPFA��z�g�~9� FA���d��2�PPFA��z�gZBA��):�e��>�      �   r   x�m̽
�0���)����$B���K
Q24R��B,���0��Z�K�[ ��$p�ǔ��zF���JӍ�:�h�Z���$sr��j��E΢߯�m����Q�t�8�>�&(�      �   E   x�mʹ�0��T�9�0*�8���9oA0hV�F����Ȏk����ٽ~^j��Ny~u�      �   �   x�U��� �g3�)!��%n�����,���,5vu��Yt���O��]�fR��R#]���tfv�Ilg+��v���}���6�����*���J�l~�IS={��\ 4G�LjQ�HK�\H=Nv�GT҆z��������@�w��e��LqY1�Lq9??:�8�����|�_6      �   *  x���=O�@�g�Wxa�ۗ#��P$`a�D?�4I���=�4�TBL���-��NY��HI��I�����-���P�*��SXa�v�o7�jmC3}`�����x��a#�ާ�X?k��S��htI.%^NҔ��}�O]��^-|���`Y�8�irH޲���݋�J�v.j��n�{�91>3�ڸ������ ��,!ʢ_ɍA���hf�����4�]����v�l�#Tp�/�,^D��"����2�e����T����~u���s��f����.I�/�S�6      �   �   x���A!�u{
��[$����0�gAG�ɬ�6M?/�9	��Hk�����R�'�$�tX/��k��")�rE����p2����9�*�-�=�z�.�[�[��ÅD�� _W&h��j�dj���O�OPt      �      x������ � �      �   �   x�]�K�0D��)zB��">�@��X��Z�P�i%nO�*T���ٞ�A�}@F'Ϯ��vtl��E�Yl��'n(��{�q[*Kh���%�������sWp"y�Q:Ak����츟D��^&c>�=Y�w�db�3!���Ib      �   C   x�3����,)��K��".#�����<(Ϙӿ$#���5202�50�50U04�24�21����� {�r      �   |   x�3��t�u�Q������".#����c�#L�!������1�tv�	�7E�p��d�����
0�ru��t��-8�C<�� �������������������1W� ��+;      �   �   x�Uѽ� ��|�{#�_kqk�A�vq��I���:Է/�4�����9H'1�i�紮ӈ4Z�>CsU+�#�����{8~�����n��1s����!���#���J%e�K��S�k�
-^��d��#ĲWOb�:�zI�*���Xb��CT�	]N�!��{�}zT��.�3�F�Z��g�m��y;�	� ��6h�      �   �  x�}QK��0>��b����.�J�8�����^�x�L������ܤ�H�%��=��?���YKd곦*�h[d�t���L^�8����N���<`Td���Z�V�s�`��r!/�5QhǙ� l� �i��z�xO���#��뮗 ���+�'��U���H�<I�C��1 A�4p��Xo����������5��� �p�,�زn�7��s��q�Z�;���%�b��,�(��x��
�Z��V����M�����aG
�N2�ݓ����|���cc2��?+{�=��<��m�����нo��V��l�����s��A���2PW���qO�4b������;
mg��6[�H�lc�"_�?�e��[�<r��\����n]f�$�im&��Ze_����;'6�{      �   e  x����N�0���S���u��+ �	8��.^��I�4ִ��i��*R��?�]e�l�	�;�Z�K��i0��j�4rZ(�K�Z�tGtu�xr�EtPa�"�5�#K�tt�*	Oh"C�0P�@�����i��2�[��.��{��M�|}�nНQ�'�[l�Z�h41R��,�b#��^}{WKulÈ|--��:Aly&����",+L��$Ŀ+��u"tpNk��F�S�����u#���5Nݷ�\��Y��Ǣd�q?C�4��Md-*�">��@`�%��m��}ĠA<����eY�e	�F �!���<.O^���!��x���|)��O����˕�6      �   G   x�3�tr�tw��".#� � (ۘ3�1���3���p
q��tuL���ͬL-�b���� �.G      �   B   x�3�420�440470720�,ȍ��OϏ�.�+�K�,K���+�[XXr��q��qqq �<      �   �   x�]�=�0��9�' 6b��&.��:�$*�' (���}�[�C[{�u������D�Ny��(E�עjq����]�Y�M]pE�Bl�)[Å���_a*7pp�l���	��c�s���-�7/�h�G�=�)��J�'wTPS      �   �  x���MS�0��ʯб=��o8B)�4i(Å�boa[2�L��ʑ?b��\��y��V�����	�����RqX�LI�箶	O� y���~���_?�5��E [G��HS%� �3z���L��$��x̥��1D�|A�͕2�p)�^3����Z���ꥠ��K��tQ�Dc2 S�LF�B"�K�F���$��^�V�� �ENrX���j8C͓T�6̾�&����~��z���|Mp�_��t���S�`�S!7�ǅ�Cjp/�]Ӆ�z�`oފ�TI^��(�=F&��'%����g{����_A�� 7���.�Fe~DYd�dH�6����ðv54�E����S�4�2�#,`e��:f!�IE`�6���
Åc��)&c�>&Y����l����P�c���Ƌ�(gh��ޫ<�C�f�B^��sT܁��^�u�+|�[�!7_NKvnK���Y<-r��s�0e��ё]wL�`���_���Ww�c�x��Y�G*t���=�Ĕ`�i�z��<�㪍u��g�������[�Z�uǜ9�DFo�m`�!omC�>�ӷZ;=�����i��ыH�n�Y�<�=2�0~>�G�*�k��p���� �wG��>�nś����0D��'FWH�
p� 6�Q�^�&x�����!RH�ߌ���	쎢M#2&���T��Y�8�c�G����"?��|�J6J�b狏y;����u�u~����d���������t:�6n�	      �   ]   x�Uλ�0����L^,@R@���sl�W|ՏL� ����f�
S�����*��Ht���d�
���_P�������:���o8,!�      �   T   x�3�t/J�LO�4��".#� � 8Ϙ3 3'3#1O��4'���*n��X���&j�鑚X���2��	�ś/F��� Q~      �   ;  x��Z�r��}���� ��.�7�2
��IU^Ӂ��ڥK��Y�-l�����N�kf`@�׵��-�L]�lX!�-��B����vM~W��O.���`tRg��s��X�>���]گ��~&Sڡײۡ��O�e��/���g]��t�閭j�Ȋ}Nز�0���;V�Kv����Z�T�:�!�ڝ {����gþ%/��$�e�:�ձ{�v~r\Xp���k�d.3I��Q�����$S^�[���Ӻt�2�����xA�x��n��9�G�nD/�.�ש�D&i>U�lw��"?��xY�q�^b��GF��혱ه���@�&���my�R���ͪ�e�tv�E4�n�*qF'�@��I@�al���	���;N��,/l�Y�.^cusct`
��B�t"��J�DE^��8��І�`pŶL7EJT��-+T� ��C�nO���}�M:�EPq�r'K��"�k���9�[�-?h�V���8�+�����.���x�ӭ�W�xQ�=�|#�<c�%�v�T���ѐ��Zh`7}㦧�|��Tk�G�\ˌN9��4X�w��������a2��wl������+���KG��#}��
�pyE��d_�-�cy�g�_�)��`<8�~y��_�^��e�����,;tNe���-җ�\֫��D!��JU�0��/VH��5�ovt��6<r�}�L����;���+QV�����m��U���؄Q'��aw��`�2P(��Kѵ�X%�h����:g.]���;�W�4���X�[`�@�%}���r�0�I"�f���w��j;�օ�)�x�IXh���X%+�ϲ��Te�(�4Q���m�Ȁg|Yp#NX�>l�NR� ���\Z*E���3Ӆ�L�������}����tu{�o�d����L�2�j�=_�u&[p�=�� jU��
�π��,I@w�lexD%���6�'`ˎ:A�b�l�'˻.��;�����U�a����<13$��R�A�z%����g9�Ӓ{ ��zb�z.�o)$cjD��7@Q�ǈ�|R�S�tCA�.��A�ʜ�B�s��w,5�˧,e��iY���J����9Ry��'-oz�q�6��ڝ	���I$bUgoj�7'ɱ�9�~0���NX�N�LA���z�AߪO�M�ke��U��K�x�<x���8����(y�t�	�Rׁc��?x�.8���t��`��� Ρ;�<��f4M�S�JmB�������њg\��Q�����C~y
�et,X~�
qX�Qׄ��^؉�Z~Y�
^��j����xd�e��� �z��(cS�vӜ���:yz��`a���7߷p������rM�������B���N�)}�������
�Rm�����:n�D�mG�$�r���c�;���=->;�8����GN���N�Gu��)��W�/s^�=��]�J�U�f��Z4�%9dÌ���Z/?�Bru�ģ�+�8a~U��[�W��;�W��WR��=�ss�F���a�;Ш��W>�sMćn�1��+�3��e�ڬ3�|B��.�`�Nli\��!���Cz��?"3L2��*�F�<8��B��D���w�&�1簻�(���K�]=ܿSۼ�@��/�V��:�R��D��
c����vI�A��12qtug��?j��Pਃ���]�$���-s��Bm�Q䚗���L\�)��sm:�`�ش����L���S�yX�7k<�|������I�;1�sA3����]�S�j�ԕP�����0�;8�y��m��nP{/����G��A�w+M~n@n:jS�J��w���fYC�[������l+��Ph�~�����#�����������l�T�k@��eQ��6
T�۸��l|Q�/�7u���&o2�rǳm��kC7"W�P�6($��+T���f,vc�/oU��*���Ǽi�*N�� ���lx����=ٜ�=��t���j�گ�;qJ�]���>b��`E��:¡^ HT�p�*�H$ز�i����e�g�	ۤݾ�N�#=�LD)�r��~T����<N�Sʇq[����(���]]��C�W����K��$4�� �P�F��^�����:S�)*5ǨʋT��o�11����z�aY�eǳ�'y�ȗ��"�EE�A[&��a�;�����pk>ݡl�L�F�Mv�#�B�~��ڽ&/�=���9z�Up�k�gN�U_)��=7�Ҫ�����@�9��C�w�e�
��]]�bǚ�̎�_��ݮ�;�����A��U��2�B�{e�J�5$	` ��lي���\ِ!�2SLR�u�w�߶�bC~��|���O��h�^��,��n��J�ʬ��ӥ���f���L�,\��Ɵ �T|:a�|������a����E�v�B�r�:f�μ���ѨQ� �d��LK������#�.�9��׃/Wt1�|�}��O���������f;d^����Fi����]2��l��nR���fbOl23k ��a��Q�G�F���n&Ⴤh�<��U�}!�ț�J�e8\��͂�i�([n������l������B����QOa�����M.���Yt�~ا�v�Y �7e�Q��9�ë[T��?$�B<��>��j���ڬؓ�Tk��k����8'N{D5��F{p┛(��W��t(��O?<����*\��m[�D���wOע�j'�o�D^dWO"`JΑ<�7���\j���c+���Hc�gMc�2
���>�1����o��t�7;qg�N$��z��x#q���.= ��go�DQD#/jִ�=^/�[�~���oX�      �   �   x�u�?�0���Str4��hD�i��rJS/�B
�����%�ĤC��w�z�C���i�JԨ���lO����]!a�z���YO2!o_@�VpEń4=a�~��+g|���K~��`'�����	v����Ґν�'p���3���e��I!���l�����i�M����\H��#j5-�}>3^-�(z��x�      �      x������ � �      �   F   x�3�t*U.��,��".#�@@jB�$☛�X2��/-)�/-J��K���r��d��b���� �>�      �   E   x�3�ttw�V�v���".#�`WG?w(ט����)4�5�pR@4��q���P�=... ���      �   6   x�3���,I��".#N��4]dcN������(߄ӿ$#��ʍ���� �0�      �   �   x�u��� E�ӯ��Z�K5�h��uCd��V
~���R�	r��Ѓ�����2�ՙ�j���|�>�����a�'<�I`)�	;#ȭG�
��n�=��x��غ0>���`J#�8��򹭤B�6� ���L�2�D�2��}�l�(�!�A`�E>gt�֚�ʣ�s�.�OS*�a^�x|q�����꺭p߉���9�k      �   �   x�]P=��0��_���d;	��naD���H��Jj�� �[���#���k�(���5
W���˩�c
p����>©��J�����@��Jk*�r�̯,�C0��3J6��}6��D<���^o}9ٷ_�G��7g�#g���{�>8���=&�ϸ&���B��Zʎ�&��S[Oj�RHA�Ul��ɺ�~�)��EQ��Q:      �   =   x�35�4��	��7Է�7��420��54�5�P02�2��22�!�ej�jD�Vc�=... ���      �   :   x�33��
0�4�420��54�5�T00�21�24�&�ef�`D�K�4��qqq S��      �   �   x�u���0D�3�"8�
I�p�r��7��4va^��,.Jw[�g��8�A�����edmM1�P�8��$R�9/B����Ȧ8��$��s|Pڜ�T�؟��o�̤��Z��6�����1~�~\=      �   .   x�3�4�4�4202�50�50V04�21�21G35�22����� ��      �   ;   x�33��2�4�420��54�5�T00�21�24�&�ef
�`aA�3�4��qqq u��      �   >   x�3���4�45�5� 1M�NcN#CK]C#]C+s+c�b�ř%\1z\\\ k}�      �   g   x�m�;
�0��)��2�ݤ�sхD%��{x��Ny�dp�I,�u�{\O`�䣚��̲>o�5��Jƶ7XC��ēakxD���ٍN-�R��M�      �   �   x�m��
�0���S�V���dW��iW/��Q����B7�$$��/�G@H[)p�l��ma|La�a�(R��-��4�˖��=��|�<�_��T���{2�u[8oS��2�x�oj���b��lU��s�F��� )wS;B�����I݌R��AR�      �   C  x���Ko�0��ί�͎�N�s5��(�Bi�Q���'&M��ǐ��hvA�)��w��ɵ|,0%����#��˪i���x�Į�3��_��	W��&�k�!0����k?�V�g5�����J%予�)� ���d ����ǰR^.
K�����mJi�"P�^3#��aR�Q�`J�a��)�B],*Er|%QO��8�$f�Vn��1Z��M0KS:%G,(�2g��<F%�(��ܿS3�n{�+"ku�ա��N�.�R^�ܬ��l�S5 a�pP�u���fἝ%�&�k�s�V�� ��y���90���2��]�a���]�K��.ܸ��v�SG�T�k[������K2]T��Z>?o@M�g�E%���!>UM,TQy����OP4.�����ܯp��$�=J���4��h��^����u@NoZ���}���������oF�#s�����|���D}d�y���+?P��d�Ϡ5��S}?��`a�����3W���ej���u}i�<�y����u`Nގ�1�G��P�����]��� �n~T���A`Adk(�e�S�"��NQ���~�     