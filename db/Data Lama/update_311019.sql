PGDMP     &    .            	    w            kharisma_printex    11.4    11.2 �    0           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            1           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            2           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            3           1262    65801    kharisma_printex    DATABASE     �   CREATE DATABASE kharisma_printex WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
     DROP DATABASE kharisma_printex;
             postgres    false            �            1259    65802 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    65805    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    196            4           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    197            �            1259    66070 	   mst_do_sj    TABLE     �  CREATE TABLE public.mst_do_sj (
    i_id integer NOT NULL,
    i_id_kartu integer,
    i_id_rfp integer,
    i_no_sj character varying(16) NOT NULL,
    d_sj date,
    i_pel integer,
    d_due_date date,
    n_total_cw integer,
    e_ket character varying(250),
    n_tot_roll numeric,
    n_tot_asal_sj numeric,
    n_tot_asal_kp numeric,
    n_tot_jadi_kp numeric,
    n_total_tagihan numeric,
    n_sisa_tagihan numeric,
    f_print boolean DEFAULT false,
    f_lunas boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    tgl_produksi date,
    expedisi character varying(25)
);
    DROP TABLE public.mst_do_sj;
       public         postgres    false            �            1259    65807 	   mst_kartu    TABLE     H  CREATE TABLE public.mst_kartu (
    i_id integer NOT NULL,
    i_no_kartu character varying(25) NOT NULL,
    d_pengiriman date,
    i_id_so integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_lebar_blanket character varying(75),
    e_handfeel character varying(75),
    d_beres timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_ket character varying(250),
    i_status integer,
    tot_roll numeric,
    tot_kg numeric,
    tot_pjg numeric,
    e_lebar_kain_jadi numeric,
    tgl_produksi date
);
    DROP TABLE public.mst_kartu;
       public         postgres    false            �            1259    65813    mst_kartu_cw    TABLE     c  CREATE TABLE public.mst_kartu_cw (
    i_id integer NOT NULL,
    i_id_kartu integer NOT NULL,
    e_cw character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0
);
     DROP TABLE public.mst_kartu_cw;
       public         postgres    false            �            1259    65820    mst_packing_list    TABLE     �  CREATE TABLE public.mst_packing_list (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    i_cw integer,
    e_kode character varying(6),
    n_asal_sj numeric DEFAULT 0,
    n_asal_kp numeric DEFAULT 0,
    n_jadi_kp numeric DEFAULT 0,
    f_jadi_sj boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_kartu integer,
    i_no_packing character varying(40),
    i_id_sj integer
);
 $   DROP TABLE public.mst_packing_list;
       public         postgres    false            �            1259    65830    mst_packing_list_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_packing_list_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.mst_packing_list_i_id_seq;
       public       postgres    false    200            5           0    0    mst_packing_list_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.mst_packing_list_i_id_seq OWNED BY public.mst_packing_list.i_id;
            public       postgres    false    201            �            1259    65832    mst_sales_plan    TABLE     �   CREATE TABLE public.mst_sales_plan (
    i_id integer NOT NULL,
    n_qty numeric DEFAULT 0,
    bulan character varying(2),
    tahun character varying(5),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 "   DROP TABLE public.mst_sales_plan;
       public         postgres    false            �            1259    65839    mst_sales_plan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_sales_plan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.mst_sales_plan_i_id_seq;
       public       postgres    false    202            6           0    0    mst_sales_plan_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.mst_sales_plan_i_id_seq OWNED BY public.mst_sales_plan.i_id;
            public       postgres    false    203            �            1259    65841    mst_so    TABLE     �  CREATE TABLE public.mst_so (
    i_id integer NOT NULL,
    i_no_so character varying(16) NOT NULL,
    d_so date,
    i_pel integer,
    f_repeat boolean,
    i_desain character varying(16),
    n_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_color_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    d_strike_off date,
    d_approval_strike_off date,
    d_penyerahan_brg date,
    e_keterangan_kirim character varying(100),
    i_status integer,
    d_approved timestamp(6) without time zone,
    v_pekerjaan numeric,
    v_pekerjaan_plus_ppn numeric,
    v_discount numeric,
    n_discount real,
    v_sisa numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_reject character varying(200),
    i_no_po character varying(25),
    flag_so integer,
    hitung_by integer,
    n_ppn real,
    v_ppn numeric,
    exclude_include character varying(1)
);
    DROP TABLE public.mst_so;
       public         postgres    false            �            1259    65847    mst_so_item    TABLE     �  CREATE TABLE public.mst_so_item (
    i_id integer NOT NULL,
    i_id_so integer NOT NULL,
    e_uraian_pekerjaan character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    v_harga_sat numeric,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0
);
    DROP TABLE public.mst_so_item;
       public         postgres    false            �            1259    65854    mst_so_item_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_so_item_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.mst_so_item_i_id_seq;
       public       postgres    false    205            7           0    0    mst_so_item_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.mst_so_item_i_id_seq OWNED BY public.mst_so_item.i_id;
            public       postgres    false    206            �            1259    65856    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false            �            1259    65862 
   ref_bagian    TABLE     �   CREATE TABLE public.ref_bagian (
    i_id integer NOT NULL,
    nama_bagian character varying(50) NOT NULL,
    e_penanggung_jawab character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_bagian;
       public         postgres    false            �            1259    65865    ref_kain    TABLE     �   CREATE TABLE public.ref_kain (
    i_id integer NOT NULL,
    e_kain character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_kain;
       public         postgres    false            �            1259    65868    ref_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_kain_i_id_seq;
       public       postgres    false    209            8           0    0    ref_kain_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_kain_i_id_seq OWNED BY public.ref_kain.i_id;
            public       postgres    false    210            �            1259    65870    ref_ket_umum_so    TABLE     �   CREATE TABLE public.ref_ket_umum_so (
    i_id integer NOT NULL,
    e_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_ket_umum_so;
       public         postgres    false            �            1259    65873    ref_ket_umum_so_child    TABLE       CREATE TABLE public.ref_ket_umum_so_child (
    i_id integer NOT NULL,
    i_id_ket_umum integer NOT NULL,
    e_child_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 )   DROP TABLE public.ref_ket_umum_so_child;
       public         postgres    false            �            1259    65876    ref_ket_umum_so_child_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_ket_umum_so_child_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.ref_ket_umum_so_child_i_id_seq;
       public       postgres    false    212            9           0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.ref_ket_umum_so_child_i_id_seq OWNED BY public.ref_ket_umum_so_child.i_id;
            public       postgres    false    213            �            1259    65878    ref_logo    TABLE     �   CREATE TABLE public.ref_logo (
    i_id integer NOT NULL,
    logo_name character varying(100),
    f_active boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_logo;
       public         postgres    false            �            1259    65882    ref_logo_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_logo_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_logo_i_id_seq;
       public       postgres    false    214            :           0    0    ref_logo_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_logo_i_id_seq OWNED BY public.ref_logo.i_id;
            public       postgres    false    215            �            1259    65884    ref_no_urut    TABLE     �   CREATE TABLE public.ref_no_urut (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    no_urut character varying(11),
    bln character varying(2),
    thn character varying(5),
    flag_so integer
);
    DROP TABLE public.ref_no_urut;
       public         postgres    false            �            1259    65887    ref_no_urut_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_no_urut_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.ref_no_urut_id_seq;
       public       postgres    false    216            ;           0    0    ref_no_urut_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.ref_no_urut_id_seq OWNED BY public.ref_no_urut.id;
            public       postgres    false    217            �            1259    65889    ref_pelanggan    TABLE       CREATE TABLE public.ref_pelanggan (
    i_pel integer NOT NULL,
    e_nama_pel character varying(255) NOT NULL,
    e_alamat_pel character varying(255),
    f_pkp boolean DEFAULT false,
    e_npwp_pel character varying(30),
    e_telp_pel character varying(30),
    e_kont_pel character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_fax_pel character varying(30),
    e_kota_pel character varying(60),
    e_kode_marketing character varying(16),
    n_jth_tempo integer
);
 !   DROP TABLE public.ref_pelanggan;
       public         postgres    false            �            1259    65896    ref_pelanggan_i_pel_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_pelanggan_i_pel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.ref_pelanggan_i_pel_seq;
       public       postgres    false    218            <           0    0    ref_pelanggan_i_pel_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.ref_pelanggan_i_pel_seq OWNED BY public.ref_pelanggan.i_pel;
            public       postgres    false    219            �            1259    65898    ref_role    TABLE     �   CREATE TABLE public.ref_role (
    i_id integer NOT NULL,
    e_role_name character varying(25) NOT NULL,
    created_at timestamp without time zone,
    update_at timestamp without time zone
);
    DROP TABLE public.ref_role;
       public         postgres    false            �            1259    65901    ref_role_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_role_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_role_i_id_seq;
       public       postgres    false    220            =           0    0    ref_role_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_role_i_id_seq OWNED BY public.ref_role.i_id;
            public       postgres    false    221            �            1259    65903    ref_sat_qty    TABLE     �   CREATE TABLE public.ref_sat_qty (
    i_id integer NOT NULL,
    e_sat character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_sat_qty;
       public         postgres    false            �            1259    65906    ref_sat_qty_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_sat_qty_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_sat_qty_i_id_seq;
       public       postgres    false    222            >           0    0    ref_sat_qty_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_sat_qty_i_id_seq OWNED BY public.ref_sat_qty.i_id;
            public       postgres    false    223            �            1259    65908    ref_so_flag    TABLE     �   CREATE TABLE public.ref_so_flag (
    i_id integer NOT NULL,
    flag_name character varying(100),
    f_active boolean DEFAULT true,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_so_flag;
       public         postgres    false            �            1259    65912    ref_so_flag_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_so_flag_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_so_flag_i_id_seq;
       public       postgres    false    224            ?           0    0    ref_so_flag_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_so_flag_i_id_seq OWNED BY public.ref_so_flag.i_id;
            public       postgres    false    225            �            1259    65914    ref_workflow    TABLE     �   CREATE TABLE public.ref_workflow (
    i_id integer NOT NULL,
    definition character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
     DROP TABLE public.ref_workflow;
       public         postgres    false            �            1259    65917    ref_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_workstation_i_id_seq;
       public       postgres    false    208            @           0    0    ref_workstation_i_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public.ref_workstation_i_id_seq OWNED BY public.ref_bagian.i_id;
            public       postgres    false    227            �            1259    65919    rfp_urutan_fifo_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_urutan_fifo_seq
    START WITH 51
    INCREMENT BY 1
    MINVALUE 51
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.rfp_urutan_fifo_seq;
       public       postgres    false            �            1259    65921    rfp    TABLE     �  CREATE TABLE public.rfp (
    i_id integer NOT NULL,
    i_no_rfp character varying(20) NOT NULL,
    d_selesai date,
    i_pel integer,
    e_contact_person character varying(50),
    f_repeat boolean,
    i_desain character varying(16),
    e_motif character varying(50),
    e_material character varying(50),
    e_original_cond1 character varying(25),
    e_ket_ori_cond character varying(50),
    e_color character varying(50),
    i_penyedia integer,
    d_tgl_material_in date,
    n_qty_material numeric,
    n_pengkerutan numeric,
    e_gramasi_from character varying(50),
    e_gramasi_to character varying(50),
    e_penyesuaian_lebar_from character varying(50),
    e_penyesuaian_lebar_to character varying(50),
    e_pakan_from character varying(50),
    e_pakan_to character varying(50),
    e_lusi_from character varying(50),
    e_lusi_to character varying(50),
    e_tekstur_akhir_from character varying(50),
    e_tekstur_akhir_to character varying(50),
    e_cw_1 character varying(50),
    e_cw_2 character varying(50),
    e_cw_3 character varying(50),
    e_cw_4 character varying(50),
    e_cw_5 character varying(50),
    e_cw_6 character varying(50),
    e_cw_7 character varying(50),
    e_cw_8 character varying(50),
    i_status integer,
    d_approved_mrk timestamp(6) without time zone,
    d_approved_pro timestamp(6) without time zone,
    d_approved_ppc timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_so integer,
    e_reject_mkt character varying(200),
    e_reject_prod character varying(200),
    e_original_cond2 character varying(25),
    e_jenis_printing character varying(25),
    i_jns_kain integer,
    urutan_fifo integer DEFAULT nextval('public.rfp_urutan_fifo_seq'::regclass) NOT NULL,
    f_proses boolean DEFAULT false,
    old_urutan_fifo integer,
    d_proses timestamp with time zone
);
    DROP TABLE public.rfp;
       public         postgres    false    228            �            1259    65929    rfp_lpk    TABLE     �   CREATE TABLE public.rfp_lpk (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_nomor_lpk character varying(150),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    qty_roll numeric
);
    DROP TABLE public.rfp_lpk;
       public         postgres    false            �            1259    65932    rfp_lpk_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_lpk_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.rfp_lpk_i_id_seq;
       public       postgres    false    230            A           0    0    rfp_lpk_i_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.rfp_lpk_i_id_seq OWNED BY public.rfp_lpk.i_id;
            public       postgres    false    231            �            1259    65934    tx_jns_proses_kartu    TABLE     �   CREATE TABLE public.tx_jns_proses_kartu (
    i_id integer NOT NULL,
    description character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 '   DROP TABLE public.tx_jns_proses_kartu;
       public         postgres    false            �            1259    65937    tx_jns_proses_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_jns_proses_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tx_jns_proses_kartu_i_id_seq;
       public       postgres    false    232            B           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tx_jns_proses_kartu_i_id_seq OWNED BY public.tx_jns_proses_kartu.i_id;
            public       postgres    false    233            �            1259    65939    tx_rfp_accepted    TABLE       CREATE TABLE public.tx_rfp_accepted (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    d_accepted timestamp(4) without time zone NOT NULL,
    user_accept integer NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.tx_rfp_accepted;
       public         postgres    false            �            1259    65942    tx_rfp_accepted_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_rfp_accepted_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.tx_rfp_accepted_i_id_seq;
       public       postgres    false    234            C           0    0    tx_rfp_accepted_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.tx_rfp_accepted_i_id_seq OWNED BY public.tx_rfp_accepted.i_id;
            public       postgres    false    235            �            1259    65944    tx_sj_kartu    TABLE     �   CREATE TABLE public.tx_sj_kartu (
    i_id integer NOT NULL,
    i_no_sj character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.tx_sj_kartu;
       public         postgres    false            �            1259    65947    tx_sj_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sj_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tx_sj_kartu_i_id_seq;
       public       postgres    false    236            D           0    0    tx_sj_kartu_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tx_sj_kartu_i_id_seq OWNED BY public.tx_sj_kartu.i_id;
            public       postgres    false    237            �            1259    65949    tx_spec_kain    TABLE     �  CREATE TABLE public.tx_spec_kain (
    i_id integer NOT NULL,
    i_jns_kain integer NOT NULL,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_desain character varying(16)
);
     DROP TABLE public.tx_spec_kain;
       public         postgres    false            �            1259    65952    tx_spec_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tx_spec_kain_i_id_seq;
       public       postgres    false    238            E           0    0    tx_spec_kain_i_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tx_spec_kain_i_id_seq OWNED BY public.tx_spec_kain.i_id;
            public       postgres    false    239            �            1259    65954    tx_spec_pekerjaan    TABLE     �  CREATE TABLE public.tx_spec_pekerjaan (
    i_id integer NOT NULL,
    i_desain character varying(16) NOT NULL,
    i_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_colow_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 %   DROP TABLE public.tx_spec_pekerjaan;
       public         postgres    false            �            1259    65957    tx_spec_pekerjaan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_pekerjaan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tx_spec_pekerjaan_i_id_seq;
       public       postgres    false    240            F           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tx_spec_pekerjaan_i_id_seq OWNED BY public.tx_spec_pekerjaan.i_id;
            public       postgres    false    241            �            1259    65959    tx_workstation    TABLE       CREATE TABLE public.tx_workstation (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_cw character varying(100) NOT NULL,
    i_id_bagian integer NOT NULL,
    n_tot_qty numeric,
    n_proses numeric,
    n_sisa numeric,
    e_ket character varying(250),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_pelaksana character varying(75),
    e_shift character varying(50),
    d_tgl date,
    i_id_kartu integer NOT NULL,
    i_cw integer,
    d_time character varying(10)
);
 "   DROP TABLE public.tx_workstation;
       public         postgres    false            �            1259    65965    tx_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tx_workstation_i_id_seq;
       public       postgres    false    242            G           0    0    tx_workstation_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tx_workstation_i_id_seq OWNED BY public.tx_workstation.i_id;
            public       postgres    false    243            �            1259    65967    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    username character varying(255) NOT NULL,
    role integer
);
    DROP TABLE public.users;
       public         postgres    false            �            1259    65973    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    244            H           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    245            $           2604    65975    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196            *           2604    65976    mst_packing_list i_id    DEFAULT     ~   ALTER TABLE ONLY public.mst_packing_list ALTER COLUMN i_id SET DEFAULT nextval('public.mst_packing_list_i_id_seq'::regclass);
 D   ALTER TABLE public.mst_packing_list ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    201    200            ,           2604    65977    mst_sales_plan i_id    DEFAULT     z   ALTER TABLE ONLY public.mst_sales_plan ALTER COLUMN i_id SET DEFAULT nextval('public.mst_sales_plan_i_id_seq'::regclass);
 B   ALTER TABLE public.mst_sales_plan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    203    202            .           2604    65978    mst_so_item i_id    DEFAULT     t   ALTER TABLE ONLY public.mst_so_item ALTER COLUMN i_id SET DEFAULT nextval('public.mst_so_item_i_id_seq'::regclass);
 ?   ALTER TABLE public.mst_so_item ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    206    205            /           2604    65979    ref_bagian i_id    DEFAULT     w   ALTER TABLE ONLY public.ref_bagian ALTER COLUMN i_id SET DEFAULT nextval('public.ref_workstation_i_id_seq'::regclass);
 >   ALTER TABLE public.ref_bagian ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    227    208            0           2604    65980    ref_kain i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kain_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    210    209            1           2604    65981    ref_ket_umum_so_child i_id    DEFAULT     �   ALTER TABLE ONLY public.ref_ket_umum_so_child ALTER COLUMN i_id SET DEFAULT nextval('public.ref_ket_umum_so_child_i_id_seq'::regclass);
 I   ALTER TABLE public.ref_ket_umum_so_child ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    213    212            3           2604    65982    ref_logo i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_logo ALTER COLUMN i_id SET DEFAULT nextval('public.ref_logo_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_logo ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    215    214            4           2604    65983    ref_no_urut id    DEFAULT     p   ALTER TABLE ONLY public.ref_no_urut ALTER COLUMN id SET DEFAULT nextval('public.ref_no_urut_id_seq'::regclass);
 =   ALTER TABLE public.ref_no_urut ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    216            6           2604    65984    ref_pelanggan i_pel    DEFAULT     z   ALTER TABLE ONLY public.ref_pelanggan ALTER COLUMN i_pel SET DEFAULT nextval('public.ref_pelanggan_i_pel_seq'::regclass);
 B   ALTER TABLE public.ref_pelanggan ALTER COLUMN i_pel DROP DEFAULT;
       public       postgres    false    219    218            7           2604    65985    ref_role i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_role ALTER COLUMN i_id SET DEFAULT nextval('public.ref_role_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_role ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    221    220            8           2604    65986    ref_sat_qty i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_sat_qty ALTER COLUMN i_id SET DEFAULT nextval('public.ref_sat_qty_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_sat_qty ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    223    222            :           2604    65987    ref_so_flag i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_so_flag ALTER COLUMN i_id SET DEFAULT nextval('public.ref_so_flag_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_so_flag ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    225    224            =           2604    65988    rfp_lpk i_id    DEFAULT     l   ALTER TABLE ONLY public.rfp_lpk ALTER COLUMN i_id SET DEFAULT nextval('public.rfp_lpk_i_id_seq'::regclass);
 ;   ALTER TABLE public.rfp_lpk ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    231    230            >           2604    65989    tx_jns_proses_kartu i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_jns_proses_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_jns_proses_kartu_i_id_seq'::regclass);
 G   ALTER TABLE public.tx_jns_proses_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    233    232            ?           2604    65990    tx_rfp_accepted i_id    DEFAULT     |   ALTER TABLE ONLY public.tx_rfp_accepted ALTER COLUMN i_id SET DEFAULT nextval('public.tx_rfp_accepted_i_id_seq'::regclass);
 C   ALTER TABLE public.tx_rfp_accepted ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    235    234            @           2604    65991    tx_sj_kartu i_id    DEFAULT     t   ALTER TABLE ONLY public.tx_sj_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_sj_kartu_i_id_seq'::regclass);
 ?   ALTER TABLE public.tx_sj_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    237    236            A           2604    65992    tx_spec_kain i_id    DEFAULT     v   ALTER TABLE ONLY public.tx_spec_kain ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_kain_i_id_seq'::regclass);
 @   ALTER TABLE public.tx_spec_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    239    238            B           2604    65993    tx_spec_pekerjaan i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_spec_pekerjaan ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_pekerjaan_i_id_seq'::regclass);
 E   ALTER TABLE public.tx_spec_pekerjaan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    241    240            C           2604    65994    tx_workstation i_id    DEFAULT     z   ALTER TABLE ONLY public.tx_workstation ALTER COLUMN i_id SET DEFAULT nextval('public.tx_workstation_i_id_seq'::regclass);
 B   ALTER TABLE public.tx_workstation ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    243    242            D           2604    65995    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    245    244            �          0    65802 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    196   ��       -          0    66070 	   mst_do_sj 
   TABLE DATA                 COPY public.mst_do_sj (i_id, i_id_kartu, i_id_rfp, i_no_sj, d_sj, i_pel, d_due_date, n_total_cw, e_ket, n_tot_roll, n_tot_asal_sj, n_tot_asal_kp, n_tot_jadi_kp, n_total_tagihan, n_sisa_tagihan, f_print, f_lunas, created_at, updated_at, tgl_produksi, expedisi) FROM stdin;
    public       postgres    false    246   ��       �          0    65807 	   mst_kartu 
   TABLE DATA               �   COPY public.mst_kartu (i_id, i_no_kartu, d_pengiriman, i_id_so, i_id_rfp, e_lebar_blanket, e_handfeel, d_beres, created_at, updated_at, e_ket, i_status, tot_roll, tot_kg, tot_pjg, e_lebar_kain_jadi, tgl_produksi) FROM stdin;
    public       postgres    false    198   X�       �          0    65813    mst_kartu_cw 
   TABLE DATA               �   COPY public.mst_kartu_cw (i_id, i_id_kartu, e_cw, n_qty_roll, n_qty_pjg, n_qty_kg, created_at, updated_at, n_qty_kg_sisa, i_id_bagian) FROM stdin;
    public       postgres    false    199   �       �          0    65820    mst_packing_list 
   TABLE DATA               �   COPY public.mst_packing_list (i_id, i_id_rfp, i_cw, e_kode, n_asal_sj, n_asal_kp, n_jadi_kp, f_jadi_sj, created_at, updated_at, i_id_kartu, i_no_packing, i_id_sj) FROM stdin;
    public       postgres    false    200   <�                 0    65832    mst_sales_plan 
   TABLE DATA               [   COPY public.mst_sales_plan (i_id, n_qty, bulan, tahun, created_at, updated_at) FROM stdin;
    public       postgres    false    202   ��                 0    65841    mst_so 
   TABLE DATA                 COPY public.mst_so (i_id, i_no_so, d_so, i_pel, f_repeat, i_desain, n_qty_warna, e_motif, e_jenis_printing, n_color_way, e_color_way, e_toleransi_cacat, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, d_strike_off, d_approval_strike_off, d_penyerahan_brg, e_keterangan_kirim, i_status, d_approved, v_pekerjaan, v_pekerjaan_plus_ppn, v_discount, n_discount, v_sisa, created_at, updated_at, e_reject, i_no_po, flag_so, hitung_by, n_ppn, v_ppn, exclude_include) FROM stdin;
    public       postgres    false    204   M�                 0    65847    mst_so_item 
   TABLE DATA               �   COPY public.mst_so_item (i_id, i_id_so, e_uraian_pekerjaan, n_qty_roll, n_qty_pjg, n_qty_kg, v_harga_sat, created_at, updated_at, n_qty_kg_sisa, i_id_bagian) FROM stdin;
    public       postgres    false    205   ��                 0    65856    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    207   w�                 0    65862 
   ref_bagian 
   TABLE DATA               c   COPY public.ref_bagian (i_id, nama_bagian, e_penanggung_jawab, created_at, updated_at) FROM stdin;
    public       postgres    false    208   ��                 0    65865    ref_kain 
   TABLE DATA               H   COPY public.ref_kain (i_id, e_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    209   =�       
          0    65870    ref_ket_umum_so 
   TABLE DATA               S   COPY public.ref_ket_umum_so (i_id, e_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    211   ��                 0    65873    ref_ket_umum_so_child 
   TABLE DATA               n   COPY public.ref_ket_umum_so_child (i_id, i_id_ket_umum, e_child_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    212   C�                 0    65878    ref_logo 
   TABLE DATA               U   COPY public.ref_logo (i_id, logo_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    214   ��                 0    65884    ref_no_urut 
   TABLE DATA               K   COPY public.ref_no_urut (id, code, no_urut, bln, thn, flag_so) FROM stdin;
    public       postgres    false    216   
�                 0    65889    ref_pelanggan 
   TABLE DATA               �   COPY public.ref_pelanggan (i_pel, e_nama_pel, e_alamat_pel, f_pkp, e_npwp_pel, e_telp_pel, e_kont_pel, created_at, updated_at, e_fax_pel, e_kota_pel, e_kode_marketing, n_jth_tempo) FROM stdin;
    public       postgres    false    218   n�                 0    65898    ref_role 
   TABLE DATA               L   COPY public.ref_role (i_id, e_role_name, created_at, update_at) FROM stdin;
    public       postgres    false    220   �                0    65903    ref_sat_qty 
   TABLE DATA               J   COPY public.ref_sat_qty (i_id, e_sat, created_at, updated_at) FROM stdin;
    public       postgres    false    222   �                0    65908    ref_so_flag 
   TABLE DATA               X   COPY public.ref_so_flag (i_id, flag_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    224   �                0    65914    ref_workflow 
   TABLE DATA               P   COPY public.ref_workflow (i_id, definition, created_at, updated_at) FROM stdin;
    public       postgres    false    226                   0    65921    rfp 
   TABLE DATA               �  COPY public.rfp (i_id, i_no_rfp, d_selesai, i_pel, e_contact_person, f_repeat, i_desain, e_motif, e_material, e_original_cond1, e_ket_ori_cond, e_color, i_penyedia, d_tgl_material_in, n_qty_material, n_pengkerutan, e_gramasi_from, e_gramasi_to, e_penyesuaian_lebar_from, e_penyesuaian_lebar_to, e_pakan_from, e_pakan_to, e_lusi_from, e_lusi_to, e_tekstur_akhir_from, e_tekstur_akhir_to, e_cw_1, e_cw_2, e_cw_3, e_cw_4, e_cw_5, e_cw_6, e_cw_7, e_cw_8, i_status, d_approved_mrk, d_approved_pro, d_approved_ppc, created_at, updated_at, i_id_so, e_reject_mkt, e_reject_prod, e_original_cond2, e_jenis_printing, i_jns_kain, urutan_fifo, f_proses, old_urutan_fifo, d_proses) FROM stdin;
    public       postgres    false    229   �                0    65929    rfp_lpk 
   TABLE DATA               `   COPY public.rfp_lpk (i_id, i_id_rfp, e_nomor_lpk, created_at, updated_at, qty_roll) FROM stdin;
    public       postgres    false    230   Q                0    65934    tx_jns_proses_kartu 
   TABLE DATA               d   COPY public.tx_jns_proses_kartu (i_id, description, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    232   	      !          0    65939    tx_rfp_accepted 
   TABLE DATA               j   COPY public.tx_rfp_accepted (i_id, i_id_rfp, d_accepted, user_accept, created_at, updated_at) FROM stdin;
    public       postgres    false    234   �      #          0    65944    tx_sj_kartu 
   TABLE DATA               X   COPY public.tx_sj_kartu (i_id, i_no_sj, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    236   	      %          0    65949    tx_spec_kain 
   TABLE DATA               �   COPY public.tx_spec_kain (i_id, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, i_pel, created_at, updated_at, i_desain) FROM stdin;
    public       postgres    false    238   �	      '          0    65954    tx_spec_pekerjaan 
   TABLE DATA               �   COPY public.tx_spec_pekerjaan (i_id, i_desain, i_qty_warna, e_motif, e_jenis_printing, n_colow_way, e_color_way, e_toleransi_cacat, i_pel, created_at, updated_at) FROM stdin;
    public       postgres    false    240   
      )          0    65959    tx_workstation 
   TABLE DATA               �   COPY public.tx_workstation (i_id, i_id_rfp, e_cw, i_id_bagian, n_tot_qty, n_proses, n_sisa, e_ket, created_at, updated_at, e_pelaksana, e_shift, d_tgl, i_id_kartu, i_cw, d_time) FROM stdin;
    public       postgres    false    242   �
      +          0    65967    users 
   TABLE DATA               �   COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, username, role) FROM stdin;
    public       postgres    false    244   �      I           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    197            J           0    0    mst_packing_list_i_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.mst_packing_list_i_id_seq', 173, true);
            public       postgres    false    201            K           0    0    mst_sales_plan_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.mst_sales_plan_i_id_seq', 42, true);
            public       postgres    false    203            L           0    0    mst_so_item_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.mst_so_item_i_id_seq', 84, true);
            public       postgres    false    206            M           0    0    ref_kain_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_kain_i_id_seq', 1, false);
            public       postgres    false    210            N           0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ref_ket_umum_so_child_i_id_seq', 7, true);
            public       postgres    false    213            O           0    0    ref_logo_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.ref_logo_i_id_seq', 4, true);
            public       postgres    false    215            P           0    0    ref_no_urut_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.ref_no_urut_id_seq', 20, true);
            public       postgres    false    217            Q           0    0    ref_pelanggan_i_pel_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.ref_pelanggan_i_pel_seq', 1, true);
            public       postgres    false    219            R           0    0    ref_role_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_role_i_id_seq', 1, false);
            public       postgres    false    221            S           0    0    ref_sat_qty_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.ref_sat_qty_i_id_seq', 1, false);
            public       postgres    false    223            T           0    0    ref_so_flag_i_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ref_so_flag_i_id_seq', 5, true);
            public       postgres    false    225            U           0    0    ref_workstation_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_workstation_i_id_seq', 1, false);
            public       postgres    false    227            V           0    0    rfp_lpk_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.rfp_lpk_i_id_seq', 45, true);
            public       postgres    false    231            W           0    0    rfp_urutan_fifo_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.rfp_urutan_fifo_seq', 60, true);
            public       postgres    false    228            X           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tx_jns_proses_kartu_i_id_seq', 57, true);
            public       postgres    false    233            Y           0    0    tx_rfp_accepted_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tx_rfp_accepted_i_id_seq', 8, true);
            public       postgres    false    235            Z           0    0    tx_sj_kartu_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_sj_kartu_i_id_seq', 54, true);
            public       postgres    false    237            [           0    0    tx_spec_kain_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_spec_kain_i_id_seq', 4, true);
            public       postgres    false    239            \           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tx_spec_pekerjaan_i_id_seq', 4, true);
            public       postgres    false    241            ]           0    0    tx_workstation_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tx_workstation_i_id_seq', 60, true);
            public       postgres    false    243            ^           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 1, false);
            public       postgres    false    245            H           2606    65997    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    196            �           2606    66079    mst_do_sj mst_do_sj_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_do_sj
    ADD CONSTRAINT mst_do_sj_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_do_sj DROP CONSTRAINT mst_do_sj_pkey;
       public         postgres    false    246            L           2606    65999    mst_kartu_cw mst_kartu_cw_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mst_kartu_cw
    ADD CONSTRAINT mst_kartu_cw_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.mst_kartu_cw DROP CONSTRAINT mst_kartu_cw_pkey;
       public         postgres    false    199            J           2606    66001    mst_kartu mst_kartu_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_kartu
    ADD CONSTRAINT mst_kartu_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_kartu DROP CONSTRAINT mst_kartu_pkey;
       public         postgres    false    198            N           2606    66003 &   mst_packing_list mst_packing_list_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mst_packing_list
    ADD CONSTRAINT mst_packing_list_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.mst_packing_list DROP CONSTRAINT mst_packing_list_pkey;
       public         postgres    false    200            P           2606    66005 "   mst_sales_plan mst_sales_plan_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mst_sales_plan
    ADD CONSTRAINT mst_sales_plan_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.mst_sales_plan DROP CONSTRAINT mst_sales_plan_pkey;
       public         postgres    false    202            T           2606    66007    mst_so_item mst_so_item_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.mst_so_item
    ADD CONSTRAINT mst_so_item_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.mst_so_item DROP CONSTRAINT mst_so_item_pkey;
       public         postgres    false    205            R           2606    66009    mst_so mst_so_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.mst_so
    ADD CONSTRAINT mst_so_pkey PRIMARY KEY (i_id);
 <   ALTER TABLE ONLY public.mst_so DROP CONSTRAINT mst_so_pkey;
       public         postgres    false    204            Y           2606    66011    ref_kain ref_kain_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_kain
    ADD CONSTRAINT ref_kain_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_kain DROP CONSTRAINT ref_kain_pkey;
       public         postgres    false    209            ]           2606    66013 0   ref_ket_umum_so_child ref_ket_umum_so_child_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.ref_ket_umum_so_child
    ADD CONSTRAINT ref_ket_umum_so_child_pkey PRIMARY KEY (i_id);
 Z   ALTER TABLE ONLY public.ref_ket_umum_so_child DROP CONSTRAINT ref_ket_umum_so_child_pkey;
       public         postgres    false    212            [           2606    66015 $   ref_ket_umum_so ref_ket_umum_so_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_ket_umum_so
    ADD CONSTRAINT ref_ket_umum_so_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_ket_umum_so DROP CONSTRAINT ref_ket_umum_so_pkey;
       public         postgres    false    211            _           2606    66017    ref_logo ref_logo_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_logo
    ADD CONSTRAINT ref_logo_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_logo DROP CONSTRAINT ref_logo_pkey;
       public         postgres    false    214            a           2606    66019    ref_no_urut ref_no_urut_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ref_no_urut
    ADD CONSTRAINT ref_no_urut_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ref_no_urut DROP CONSTRAINT ref_no_urut_pkey;
       public         postgres    false    216            c           2606    66021     ref_pelanggan ref_pelanggan_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.ref_pelanggan
    ADD CONSTRAINT ref_pelanggan_pkey PRIMARY KEY (i_pel);
 J   ALTER TABLE ONLY public.ref_pelanggan DROP CONSTRAINT ref_pelanggan_pkey;
       public         postgres    false    218            e           2606    66023    ref_role ref_role_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_role
    ADD CONSTRAINT ref_role_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_role DROP CONSTRAINT ref_role_pkey;
       public         postgres    false    220            g           2606    66025    ref_sat_qty ref_sat_qty_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_sat_qty
    ADD CONSTRAINT ref_sat_qty_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_sat_qty DROP CONSTRAINT ref_sat_qty_pkey;
       public         postgres    false    222            i           2606    66027    ref_so_flag ref_so_flag_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_so_flag
    ADD CONSTRAINT ref_so_flag_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_so_flag DROP CONSTRAINT ref_so_flag_pkey;
       public         postgres    false    224            k           2606    66029    ref_workflow ref_workflow_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.ref_workflow
    ADD CONSTRAINT ref_workflow_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.ref_workflow DROP CONSTRAINT ref_workflow_pkey;
       public         postgres    false    226            W           2606    66031    ref_bagian ref_workstation_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ref_bagian
    ADD CONSTRAINT ref_workstation_pkey PRIMARY KEY (i_id);
 I   ALTER TABLE ONLY public.ref_bagian DROP CONSTRAINT ref_workstation_pkey;
       public         postgres    false    208            o           2606    66033    rfp_lpk rfp_lpk_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.rfp_lpk
    ADD CONSTRAINT rfp_lpk_pkey PRIMARY KEY (i_id);
 >   ALTER TABLE ONLY public.rfp_lpk DROP CONSTRAINT rfp_lpk_pkey;
       public         postgres    false    230            m           2606    66035    rfp rfp_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.rfp
    ADD CONSTRAINT rfp_pkey PRIMARY KEY (i_id);
 6   ALTER TABLE ONLY public.rfp DROP CONSTRAINT rfp_pkey;
       public         postgres    false    229            q           2606    66037 ,   tx_jns_proses_kartu tx_jns_proses_kartu_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.tx_jns_proses_kartu
    ADD CONSTRAINT tx_jns_proses_kartu_pkey PRIMARY KEY (i_id);
 V   ALTER TABLE ONLY public.tx_jns_proses_kartu DROP CONSTRAINT tx_jns_proses_kartu_pkey;
       public         postgres    false    232            s           2606    66039 $   tx_rfp_accepted tx_rfp_accepted_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tx_rfp_accepted
    ADD CONSTRAINT tx_rfp_accepted_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.tx_rfp_accepted DROP CONSTRAINT tx_rfp_accepted_pkey;
       public         postgres    false    234            u           2606    66041    tx_sj_kartu tx_sj_kartu_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tx_sj_kartu
    ADD CONSTRAINT tx_sj_kartu_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.tx_sj_kartu DROP CONSTRAINT tx_sj_kartu_pkey;
       public         postgres    false    236            w           2606    66043    tx_spec_kain tx_spec_kain_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tx_spec_kain
    ADD CONSTRAINT tx_spec_kain_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.tx_spec_kain DROP CONSTRAINT tx_spec_kain_pkey;
       public         postgres    false    238            y           2606    66045 (   tx_spec_pekerjaan tx_spec_pekerjaan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tx_spec_pekerjaan
    ADD CONSTRAINT tx_spec_pekerjaan_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.tx_spec_pekerjaan DROP CONSTRAINT tx_spec_pekerjaan_pkey;
       public         postgres    false    240            {           2606    66047 "   tx_workstation tx_workstation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tx_workstation
    ADD CONSTRAINT tx_workstation_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.tx_workstation DROP CONSTRAINT tx_workstation_pkey;
       public         postgres    false    242            }           2606    66049    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    244                       2606    66051    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    244            U           1259    66052    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    207            �   H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.#d��(
����R�R�SKZb���� ]      -   k   x�M�;�@Ck�)����~�CI�"--�@r1�"ز���^_�.�&1%�Bu�s�o��
�=���`��
J�M9o�q_W*lQ9��y`{��g�Y|,aK!��3      �   �   x�}�M
�0����@df��O� ��F��Qm�oBL8ڔGxm���`��/�	�4X"��<���vN��-� }�N���SV��"q�UJhIZ�ӣ�L��2���j�z�x>�������3���N��H���$UblHu�֝*�"���/㉶�_�{�.8V�oi]��qc�y�p2      �     x�}�=n�0Fg��@�����v*z�,�R�(2��K�J�����G?���0�O��:^>�����}��x��;}��|@w�<8_8���5sF�E�ie<k�_��0�2<��F��B($��LX2%���u��XG �2τ�%�F���@d�2Z�ʈ��"
�LRL*���9�F��w�k��!�����3!t}V���-)�H-��DRm|�RS�x��Ym�,�0��p�p.a)w��]hyw+6e+���mL=�-v�d�1��a�L      �   L  x����n�0 г���"EQRn[��>��}����3b�m��@��L�=abJ?R�	sH�O���ܞ؞y��J��������߿��6� ���0�� SX�-�Q�����"E�hH�pꎠ'V�Ȍ4Ej5#R���P�	dE͈,yT�3R�O,�"�������Vľ��)R�HW��T,ۇ3�#1��t���}�������s?,���#�/؂�;t���I��O�t�-\�����J�k���*�/�k���l��&�Jޘ�J��u���+���=蚄*���kRQ�}7�&�J�[�5Irw�/]��Jޘ�JΝ��J�* �]�H%oL5,&V�Yu[-%����:k���Z0��~GP���#*b�P�6#Y�]�2�X�"��eE�n�^=-���`�Qr���9��@JBH��Q���}�a�΢�&a�T�$
��NP�R��A�Q-�Q�=��ArR5,(�$}��A�a�Ix���$��@w��ArR=*�!�sw�ä&�Arv��k69�7?��>��Jp�c�J��f����^/>��VH���{�,�ym��u��Ȗ�=�Pm�_h̃ü�è���ޚ��z1G�|v�=6Rl�p�D��n�Hn�V��ӓ9�
�-}�.VM�����6��ɯ|��27��̦�\�oڡ��G r><�A?E(G p@�
�(rF<k�>F�ؕ
G [�r�r<�.��1�O�j�����n@b;��8FSq�v��U{�++b���qL��k���P�>'����A0?�e�Xr�	��qLlU�1�R������bW�*�ؤdّ���'��"��E���r����*�         �   x�U��� �g3�)!��%n�����,���,5vu��Yt���O��]�fR��R#]���tfv�Ilg+��v���}���6�����*���J�l~�IS={��\ 4G�LjQ�HK�\H=Nv�GT҆z��������@�w��e��LqY1�Lq9??:�8�����|�_6         �  x�ՖMo�0���_�'H��}���*���^������M$���?�����ֲ9�ı�3O��D� �\��$��p�H>�(�8���Xr6m�-�蟆�@�~���k��;��S����]���M�:��3�����n�M#}{�P|��?n�~�ee��gb�O͂���Â��� �8)^/q�90"���E�\�=�^�jLz�D���J�y��䗜�4���.�}aZ˛�|���M�����Lk	7���I�6n8W������b5 ��:�	F9��&J��$i���+�!��o����{�PBfj R�D�Q��2���Ƙ�1s�`e=3	]T] �=#8^��	��Lx���倠�E��͞���.�m{O��mB����<��f�S2-��,c�kmB��c�,2�Y u��lŔȘ�!2������,��?2���+o�rF���m���@��6���J[�,WQpNX��:ć���Iǌ<�_���T��+:���3��*d9��R���k�+Y����<ԟ�W(��*4�2D����:��tY&�¹��2wR�*#�R�kd�+ˍ�1��$��^��ٺB�^��N��]p�#�KL��9k��r��8f�&�=0��8㱓DP���+X�g�iݞ�V����         n  x����J�0���)z�-�I&issA��� {�ъ����4Φ�"U(-�����I-���?�����?~����1XD�n��wB���TW#z�=�tk��`e��oO	���"�	��h���L�z�w�J/�x����w�Ѷ���C����!���t����\�ɘ#
�z�4�\Nz+�����*^��op����ޤ���RzR�i�(�d%�)�d�|�1a
�|�� |���i��t�+��x]�x#���PiM��6�ë�dq��	ke���|QN���l�L>�p��f�~��'U����V��/��m���Gw|	��ڕ��t�n6�7ڈ?�ƚ��O*��r�22J�P
�:4UU}	(\�            x������ � �         �   x�]�K�0D��)zB��">�@��X��Z�P�i%nO�*T���ٞ�A�}@F'Ϯ��vtl��E�Yl��'n(��{�q[*Kh���%�������sWp"y�Q:Ak����츟D��^&c>�=Y�w�db�3!���Ib         Z   x�3�tsV01(Vp���".#�g_S$!c��*��1P(&d
2�@2�tvAUdA1�$bd�$b��X�������� �&/      
   �  x�}QK��0>��b����.�J�8�����^�x�L������ܤ�H�%��=��?���YKd곦*�h[d�t���L^�8����N���<`Td���Z�V�s�`��r!/�5QhǙ� l� �i��z�xO���#��뮗 ���+�'��U���H�<I�C��1 A�4p��Xo����������5��� �p�,�زn�7��s��q�Z�;���%�b��,�(��x��
�Z��V����M�����aG
�N2�ݓ����|���cc2��?+{�=��<��m�����нo��V��l�����s��A���2PW���qO�4b������;
mg��6[�H�lc�"_�?�e��[�<r��\����n]f�$�im&��Ze_����;'6�{         e  x����N�0���S���u��+ �	8��.^��I�4ִ��i��*R��?�]e�l�	�;�Z�K��i0��j�4rZ(�K�Z�tGtu�xr�EtPa�"�5�#K�tt�*	Oh"C�0P�@�����i��2�[��.��{��M�|}�nНQ�'�[l�Z�h41R��,�b#��^}{WKulÈ|--��:Aly&����",+L��$Ŀ+��u"tpNk��F�S�����u#���5Nݷ�\��Y��Ǣd�q?C�4��Md-*�">��@`�%��m��}ĠA<����eY�e	�F �!���<.O^���!��x���|)��O����˕�6         B   x�3�420�440470720�,ȍ��OϏ�.�+�K�,K���+�[XXr��q��qqq �<         T   x�34���4 C�?N#CKN#.C��)\ԘHx����q`a�eh��Qm��ڐ3�M5P��f�9\ؐ+F��� r�K         T  x��Z�r��}���� à�ț c�$9����m	�Ւ␯?���-<x;ة3550H���ڻe�k�F'y%h�aR�[F.�.�u�z]t]�X���T�w���~MV�Z�������Gf����L����_����_sH�U��i���n���Ζd�v頖,���HiG�֡��O����?b�u�gb~�]�R�[��i\T�s�Vݘ�	�wL�Kv����o���gV����g���-طⲪKb��~��u�>��l}��YYA�Q�D�ZrV����m`�����Y����aV��4����5�i4�uC�ҳ��"Y��HV�ݱ:^��0$�U�'��m82�@0Wlǌ�l��xM�my�R���ͫn�g��}�������2� �����K��Y�m��=7h�Y�W���}�j�}S�LȢ������ȋ�'Im8� Wl�tSİ�D�L�ٲB��-a:$���[��Gڤ�[w_슲xl��eb�k.�Vw����~�w|�`���M�v|�u���c<�����x!�����K���
m��*��4�ѷX�Mϸ�j7��4���,�"�3� �V�]�d)�U�R:�'t��e����zEV���V~�� }��;\^ѫ(^D�Q��Xޢ��W|Jy2�ϯG_�Dף��cz���D�hIݞا2�R�[=2(dد%�D1C�RU0����5�F�<��͎΁SԂG�xD5K`�<;4�t%�Ja����,Һ��s��0jGt(6�NTl�M�
E�b)����P��pIA�-���R��wE^I�i��{��[`�P�%}�V��8A`���s���FTߵ�>���Z֦��qV�Bӄ�w̪���dQ�L� %��&�)����y�W��p�*��p#v%K`iN/����Qz293]8t�418�:8���Wo;�iOG��e�f�L�k~���,�F��EZgEn���@�ʒ��|���'Cq<�@�%[Q� ����	ز�����\����K'���g�zd�yU�+���O��Bi�* ���$��(��~��:-��*�'�������B2��d$�)p���}�(�'U?I7���1ZT�,�*�Wo~�R��|�R��:-KY}r�J�D+�j�#E�}��w,㎧ݙB]'O� �:{S�9Iv����� 3Z��5��T�$��m�G�1���D���V�\�|�n�������/K�Szi0�v���J'�.u�9���Dr!��B��T)1��{Y���Ќf���m�[�M��\���5P��1Z���6!JEr�~����"O�ü�`���%�oU!�j	uM(�����WOW����C-����y!��2�wʍɁ26�k5�i�����L�;��op�}�퓅��ؑ�/�t�N
a�G.�!��'pH��f�0Eo�U`�j3~?�n�q�&zN;z&ᐔ	�E�X���l���|>^�Z�>�&ٝ�j��)��W�_�{�˻`�B�*T�ݵh�9JrȆ-u��^~(+���G�W���U��/l�^���_}2YsH�j�����_$mð{�Q��K�8|\皈�`8� W�g����Ĭ3�bJ�.�`�NliT��!']؀!����&�ZQ�MBn�@tU"�@�ӻb�Iٌ��]Rsx��%Ʈ��ܩm^"P+FG�˽U�;6�T�5Uk"i�R�Y�.i��8�&����`����)8��_�q�!1.-`��Z��0�\�Ң���e��0�<צc!+Ŧ�����dJ>z��w��:�Y�n�����x$Ɲ�޹��@�����.�Y5O�J�F�Q�t���	���6:���˥���k��JD��㓆�����,G����fUC�[�y����:V2C��"y�����#��	���������l�T���u%�6ن���swV"/_����Mݪ�ɛ��q-�;���	����
��B�
�F�j���m�X��[��ŭ�۳�c�4[;�d��n6<{���
�lΜ>\E��j>���k��T܂��m��g������/�B�R�P�$*v8OO��R�rý�����)ۤݾ�N�#]�LE)�o�
����?׍'Z��6�R���/�b��6/w�<��.^� �6]�ؑ�F��^�����:S�)*5ǨʋT��o�11��ɸ��ò�˶�s���%_F��3��mIl����E����­�t���3�71��0����z���4����G�i�����\Ϝ�KU_)��=7�Ҫ���}�'_�9��Cֿw�e����]]�bǚ�̎C�_���n�����z� ���C����T��f��*E�I�n6[��#��@�4Wd�A���i]�䷭�؈��-�,ߵ���'�;��$?���h���2���ti��)�/g<&�����E� U�N�-�G��0Pʰ�YZrY���P�ܲ�Y��3�Y��b4l��#�����{2?@��H�Kzλ"����]N�_��/����(�i�`�52|��6Y�F���|�Q��'�<�,�1�i����(7���[��5�B� h拓#h�UY7��A�?4f.Y�*���R��a��2.x�f�k�W��7���˿C�wZ��ydY��b��QOa������M^l���2�z��O��X�": �co�ԣ0s�W���N@�R<����j���۬�[ӶTk��k���c�'N{H�5��F{p┛(���b[�Q����~���(-�W�u lۚ|(*^K^�=]��S�M� ���y�e\=��)9G�9�4�Rv�[Q6ho��?�Dlrg3�Q��o��q�"�D~�����ى�(t�$1_��cXś��@�<����{~��v�4t�fM����򾹑����O����.         �   x�u�?�0���Sdr���_G��H%���!Ŵ\[�oo�DbAȐ��{���j�ֈ,M�:��Ө"���/dLQ[��[;�,��R;�\��/"+'��JS��A��	�x}���9f����{A�2	��v������5�(#g������H���w�}��4=z^�9�/8di�            x������ � �         F   x�3�t*U.��,��".#�@@jB�$☛�X2��/-)�/-J��K���r��d��b���� �>�         �   x�u�K� EǏU���ϰ�hI;��K���Pp��,M4a@ν��qe�����K%4�z��V�"Ⱥ���R(�W=�Bj��������Gf���c�CDϭ���[79�a��7\�+�s#��w�ob+m�2�w4��s��h3m/]%�
��s+�q�X����f���ȥm�         v  x���Mo�@���_�1U�;�_��j�I�(��r�+�ޖ��@����6��؍�B��.����
��zbt�O�8`"A3d��Ѳ�}X*�8���lU�4����b|���۳��V�-����6c����V�n29�H��&�)`�w
bz�S�f]���|���:���<������>�~s�q��o��*�05�o(xK_?�(���$ 4 F�X�F	���bt�I���lg_����"X�yZu��X5��� ��%�-Dl0���j0���bB��΢��Nj��>�		�2bB�ј;��FBL����N9�|�6'>(��?r"dNq�UD�ZLw��>�iY�ť��aBS�3+�<{�����oY�ѧwT�>�c���x�����W�l�=�>m�������O[�6G[{U		����lh�AUʞv|iU��eŹ��%�϶����˹�>>,��v(�A�k�� p�i���9���4.&�6�P�W���_Vgn��+�9�q�z:��TW�L��+�7\�5ڊ��t�g ���C��6u0��|ӓI��nL׃ަ�goz�Z�7��|��AM���m<3Ƌ~�p�k��E2&�Ys�ʡK�3��/=�����b����T�         �   x�����0�v4@�I������1�ߛ�ٓ����D`���6�$%�,
5�/Cp�[&W,�v�r���U�4�w��E�4�V��-�Y��0���aՖ�Vl�dlS�o���-�e(�6 ���f.TZ)\�(��ܛ)4���ݬ
ӊ�=��y���zh         ~   x�����0 �0EHl�yu��?GH*UJ���;�@�	ǛA@�}g��7ja3�b�����Ր6*�:����P����^7�J����(]	8'����bh��'{X�P�������O�R�      !   p   x�m���0C��)X�S���f&`�9��:A�|=��!n+mu[��YZ�+��,�$��o���S,��?���#k�6�4��jr���J���(4�'�pJ%��R���3p      #   ~   x���=
�0��Y:E.���X��v���Q��B���������<�x�L;ύ4=�t���VK9�G���s�F��P�H���]o�p�0
�
����י]Cׯ����3���~�SĂx=��R�      %   i   x�m�1
�0��99�h}MM;n��U���㫠��=��P�q_�u!��N�D����NA�<d4�T�
�mC�NE�uΔ_� ��T���VU�u��!�5�#3��|!�      '   �   x�m�=� ����+�⤵p+�4&�1qv���U0��Mj��{����r&�t�A�2��ō{p��v'�&w���ٌG�qq� ���O��~�`���p\� ���\GQR���U��j��zb�Ѓ��1e����ߜ������Bk��gH�_~Te$��5��2���c@
      )   �  x����n� ���~��`�pk{h��U�\s��d�6�����w g�������>����d?.���p>6_���[��_IF!S���|z���g��8tgB�;)�kz4dV��z�.o�Ù��ώ�9�0�I��\a�L���}N��k�d�K7�S�ҝ�_A�L�5p�KL�v�`��4L�ʔ��P����6K�ZׇЖ�\�o��j����-Qu�����8j;1F�+Q�-T�Ymg����D��n87���5�x�Y-��0�GY��7�1�\M9Mp(�]�B� 2ِ[mV��-�b��q��e��f�[�W��c΋MfG-��-�q\��}�~���"5���Z�h�$�+�dV+���6�Qq4y�[���Ԟ�nN+�У��&�P����!8Y�����k;! f0������^�d����q�dDG#�d�-үf����j�x�)-�tH�⡮�a�&%x!<������ ײ�@�B.nsm'�.p%.�P;�Q���NB�)�k�>�.PF��<�E|��p�/EV�6�(՘�!@rj#5#|o3=b�M��JfZ�ʵ���Z��.�b��k�.t�Mf�ٷE�V"��[ȵ����㢽�X�Z9K�Vl�b��U�$���fU(���l�	G+�X�Z�NȀfV�XP�h��+�H���eV�:p�e����n Z{��Õ!��{h`Mt^�S4i�.0�p܈��4�O�wЦ>����3�Ás��>�      +   �  x���Oo�0��}?���(��)j04s�m1!E*V�t��O?�d;����������($��T*!������VM��p��꟱io���j,��Ħ�3U�5���=:����Rۏ����HW��̀04�B�+r�l'�a�B��%d�%���jʠE�&hhB��O|F�2�����F-�o�e���˘�S�K|EPy`D^n�y�u���T"������6tQ�O�ͧ�]hz�'e�8;l˾�i���<���[��Y�?�.`��!/�G��|��`�>�*d�16��(�%9�-zP�
��<&i'�TIMkӊ�*x���"�&�\�_���}}�����8�w�q4O�������&�i`���I��~�I;��g{0լ�q1��մ�L��ª ?Kq     