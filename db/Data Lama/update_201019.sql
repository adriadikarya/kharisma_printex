PGDMP                     	    w            kharisma_printex    11.4    11.2 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    57629    kharisma_printex    DATABASE     �   CREATE DATABASE kharisma_printex WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
     DROP DATABASE kharisma_printex;
             postgres    false            �            1259    57630 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    57633    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    196                       0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    197            �            1259    57635 	   mst_kartu    TABLE     1  CREATE TABLE public.mst_kartu (
    i_id integer NOT NULL,
    i_no_kartu character varying(25) NOT NULL,
    d_pengiriman date,
    i_id_so integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_lebar_blanket character varying(75),
    e_handfeel character varying(75),
    d_beres timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_ket character varying(250),
    i_status integer,
    tot_roll numeric,
    tot_kg numeric,
    tot_pjg numeric,
    e_lebar_kain_jadi numeric
);
    DROP TABLE public.mst_kartu;
       public         postgres    false            �            1259    58088    mst_kartu_cw    TABLE     c  CREATE TABLE public.mst_kartu_cw (
    i_id integer NOT NULL,
    i_id_kartu integer NOT NULL,
    e_cw character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0
);
     DROP TABLE public.mst_kartu_cw;
       public         postgres    false            �            1259    57638    mst_sales_plan    TABLE     �   CREATE TABLE public.mst_sales_plan (
    i_id integer NOT NULL,
    n_qty numeric DEFAULT 0,
    bulan character varying(2),
    tahun character varying(5),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 "   DROP TABLE public.mst_sales_plan;
       public         postgres    false            �            1259    57645    mst_sales_plan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_sales_plan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.mst_sales_plan_i_id_seq;
       public       postgres    false    199                       0    0    mst_sales_plan_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.mst_sales_plan_i_id_seq OWNED BY public.mst_sales_plan.i_id;
            public       postgres    false    200            �            1259    57647    mst_so    TABLE     �  CREATE TABLE public.mst_so (
    i_id integer NOT NULL,
    i_no_so character varying(16) NOT NULL,
    d_so date,
    i_pel integer,
    f_repeat boolean,
    i_desain character varying(16),
    n_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_color_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    d_strike_off date,
    d_approval_strike_off date,
    d_penyerahan_brg date,
    e_keterangan_kirim character varying(100),
    i_status integer,
    d_approved timestamp(6) without time zone,
    v_pekerjaan numeric,
    v_pekerjaan_plus_ppn numeric,
    v_discount numeric,
    n_discount real,
    v_sisa numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_reject character varying(200),
    i_no_po character varying(25),
    flag_so integer,
    hitung_by integer
);
    DROP TABLE public.mst_so;
       public         postgres    false            �            1259    57653    mst_so_item    TABLE     �  CREATE TABLE public.mst_so_item (
    i_id integer NOT NULL,
    i_id_so integer NOT NULL,
    e_uraian_pekerjaan character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    v_harga_sat numeric,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0
);
    DROP TABLE public.mst_so_item;
       public         postgres    false            �            1259    57660    mst_so_item_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_so_item_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.mst_so_item_i_id_seq;
       public       postgres    false    202                       0    0    mst_so_item_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.mst_so_item_i_id_seq OWNED BY public.mst_so_item.i_id;
            public       postgres    false    203            �            1259    57662    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false            �            1259    57668 
   ref_bagian    TABLE     �   CREATE TABLE public.ref_bagian (
    i_id integer NOT NULL,
    nama_bagian character varying(50) NOT NULL,
    e_penanggung_jawab character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_bagian;
       public         postgres    false            �            1259    57671    ref_kain    TABLE     �   CREATE TABLE public.ref_kain (
    i_id integer NOT NULL,
    e_kain character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_kain;
       public         postgres    false            �            1259    57674    ref_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_kain_i_id_seq;
       public       postgres    false    206                       0    0    ref_kain_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_kain_i_id_seq OWNED BY public.ref_kain.i_id;
            public       postgres    false    207            �            1259    57676    ref_ket_umum_so    TABLE     �   CREATE TABLE public.ref_ket_umum_so (
    i_id integer NOT NULL,
    e_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_ket_umum_so;
       public         postgres    false            �            1259    57679    ref_ket_umum_so_child    TABLE       CREATE TABLE public.ref_ket_umum_so_child (
    i_id integer NOT NULL,
    i_id_ket_umum integer NOT NULL,
    e_child_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 )   DROP TABLE public.ref_ket_umum_so_child;
       public         postgres    false            �            1259    57682    ref_ket_umum_so_child_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_ket_umum_so_child_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.ref_ket_umum_so_child_i_id_seq;
       public       postgres    false    209                       0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.ref_ket_umum_so_child_i_id_seq OWNED BY public.ref_ket_umum_so_child.i_id;
            public       postgres    false    210            �            1259    57684    ref_logo    TABLE     �   CREATE TABLE public.ref_logo (
    i_id integer NOT NULL,
    logo_name character varying(100),
    f_active boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_logo;
       public         postgres    false            �            1259    57688    ref_logo_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_logo_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_logo_i_id_seq;
       public       postgres    false    211                       0    0    ref_logo_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_logo_i_id_seq OWNED BY public.ref_logo.i_id;
            public       postgres    false    212            �            1259    57690    ref_no_urut    TABLE     �   CREATE TABLE public.ref_no_urut (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    no_urut character varying(11),
    bln character varying(2),
    thn character varying(5),
    flag_so integer
);
    DROP TABLE public.ref_no_urut;
       public         postgres    false            �            1259    57693    ref_no_urut_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_no_urut_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.ref_no_urut_id_seq;
       public       postgres    false    213                       0    0    ref_no_urut_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.ref_no_urut_id_seq OWNED BY public.ref_no_urut.id;
            public       postgres    false    214            �            1259    57695    ref_pelanggan    TABLE       CREATE TABLE public.ref_pelanggan (
    i_pel integer NOT NULL,
    e_nama_pel character varying(255) NOT NULL,
    e_alamat_pel character varying(255),
    f_pkp boolean DEFAULT false,
    e_npwp_pel character varying(30),
    e_telp_pel character varying(30),
    e_kont_pel character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_fax_pel character varying(30),
    e_kota_pel character varying(60),
    e_kode_marketing character varying(16)
);
 !   DROP TABLE public.ref_pelanggan;
       public         postgres    false            �            1259    57702    ref_pelanggan_i_pel_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_pelanggan_i_pel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.ref_pelanggan_i_pel_seq;
       public       postgres    false    215                        0    0    ref_pelanggan_i_pel_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.ref_pelanggan_i_pel_seq OWNED BY public.ref_pelanggan.i_pel;
            public       postgres    false    216            �            1259    58099    ref_role    TABLE     �   CREATE TABLE public.ref_role (
    i_id integer NOT NULL,
    e_role_name character varying(25) NOT NULL,
    created_at timestamp without time zone,
    update_at timestamp without time zone
);
    DROP TABLE public.ref_role;
       public         postgres    false            �            1259    58097    ref_role_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_role_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_role_i_id_seq;
       public       postgres    false    241            !           0    0    ref_role_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_role_i_id_seq OWNED BY public.ref_role.i_id;
            public       postgres    false    240            �            1259    57704    ref_sat_qty    TABLE     �   CREATE TABLE public.ref_sat_qty (
    i_id integer NOT NULL,
    e_sat character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_sat_qty;
       public         postgres    false            �            1259    57707    ref_sat_qty_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_sat_qty_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_sat_qty_i_id_seq;
       public       postgres    false    217            "           0    0    ref_sat_qty_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_sat_qty_i_id_seq OWNED BY public.ref_sat_qty.i_id;
            public       postgres    false    218            �            1259    57709    ref_so_flag    TABLE     �   CREATE TABLE public.ref_so_flag (
    i_id integer NOT NULL,
    flag_name character varying(100),
    f_active boolean DEFAULT true,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_so_flag;
       public         postgres    false            �            1259    57713    ref_so_flag_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_so_flag_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_so_flag_i_id_seq;
       public       postgres    false    219            #           0    0    ref_so_flag_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_so_flag_i_id_seq OWNED BY public.ref_so_flag.i_id;
            public       postgres    false    220            �            1259    57715    ref_workflow    TABLE     �   CREATE TABLE public.ref_workflow (
    i_id integer NOT NULL,
    definition character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
     DROP TABLE public.ref_workflow;
       public         postgres    false            �            1259    57718    ref_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_workstation_i_id_seq;
       public       postgres    false    205            $           0    0    ref_workstation_i_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public.ref_workstation_i_id_seq OWNED BY public.ref_bagian.i_id;
            public       postgres    false    222            �            1259    57720    rfp_urutan_fifo_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_urutan_fifo_seq
    START WITH 51
    INCREMENT BY 1
    MINVALUE 51
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.rfp_urutan_fifo_seq;
       public       postgres    false            �            1259    57722    rfp    TABLE     �  CREATE TABLE public.rfp (
    i_id integer NOT NULL,
    i_no_rfp character varying(20) NOT NULL,
    d_selesai date,
    i_pel integer,
    e_contact_person character varying(50),
    f_repeat boolean,
    i_desain character varying(16),
    e_motif character varying(50),
    e_material character varying(50),
    e_original_cond1 character varying(25),
    e_ket_ori_cond character varying(50),
    e_color character varying(50),
    i_penyedia integer,
    d_tgl_material_in date,
    n_qty_material numeric,
    n_pengkerutan numeric,
    e_gramasi_from character varying(50),
    e_gramasi_to character varying(50),
    e_penyesuaian_lebar_from character varying(50),
    e_penyesuaian_lebar_to character varying(50),
    e_pakan_from character varying(50),
    e_pakan_to character varying(50),
    e_lusi_from character varying(50),
    e_lusi_to character varying(50),
    e_tekstur_akhir_from character varying(50),
    e_tekstur_akhir_to character varying(50),
    e_cw_1 character varying(50),
    e_cw_2 character varying(50),
    e_cw_3 character varying(50),
    e_cw_4 character varying(50),
    e_cw_5 character varying(50),
    e_cw_6 character varying(50),
    e_cw_7 character varying(50),
    e_cw_8 character varying(50),
    i_status integer,
    d_approved_mrk timestamp(6) without time zone,
    d_approved_pro timestamp(6) without time zone,
    d_approved_ppc timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_so integer,
    e_reject_mkt character varying(200),
    e_reject_prod character varying(200),
    e_original_cond2 character varying(25),
    e_jenis_printing character varying(25),
    i_jns_kain integer,
    urutan_fifo integer DEFAULT nextval('public.rfp_urutan_fifo_seq'::regclass) NOT NULL,
    f_proses boolean DEFAULT false,
    old_urutan_fifo integer,
    d_proses timestamp with time zone
);
    DROP TABLE public.rfp;
       public         postgres    false    223            �            1259    57730    rfp_lpk    TABLE     �   CREATE TABLE public.rfp_lpk (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_nomor_lpk character varying(150),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.rfp_lpk;
       public         postgres    false            �            1259    57733    rfp_lpk_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_lpk_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.rfp_lpk_i_id_seq;
       public       postgres    false    225            %           0    0    rfp_lpk_i_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.rfp_lpk_i_id_seq OWNED BY public.rfp_lpk.i_id;
            public       postgres    false    226            �            1259    57735    tx_jns_proses_kartu    TABLE     �   CREATE TABLE public.tx_jns_proses_kartu (
    i_id integer NOT NULL,
    description character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 '   DROP TABLE public.tx_jns_proses_kartu;
       public         postgres    false            �            1259    57738    tx_jns_proses_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_jns_proses_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tx_jns_proses_kartu_i_id_seq;
       public       postgres    false    227            &           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tx_jns_proses_kartu_i_id_seq OWNED BY public.tx_jns_proses_kartu.i_id;
            public       postgres    false    228            �            1259    58121    tx_rfp_accepted    TABLE       CREATE TABLE public.tx_rfp_accepted (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    d_accepted timestamp(4) without time zone NOT NULL,
    user_accept integer NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.tx_rfp_accepted;
       public         postgres    false            �            1259    58119    tx_rfp_accepted_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_rfp_accepted_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.tx_rfp_accepted_i_id_seq;
       public       postgres    false    243            '           0    0    tx_rfp_accepted_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.tx_rfp_accepted_i_id_seq OWNED BY public.tx_rfp_accepted.i_id;
            public       postgres    false    242            �            1259    57740    tx_sj_kartu    TABLE     �   CREATE TABLE public.tx_sj_kartu (
    i_id integer NOT NULL,
    i_no_sj character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.tx_sj_kartu;
       public         postgres    false            �            1259    57743    tx_sj_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sj_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tx_sj_kartu_i_id_seq;
       public       postgres    false    229            (           0    0    tx_sj_kartu_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tx_sj_kartu_i_id_seq OWNED BY public.tx_sj_kartu.i_id;
            public       postgres    false    230            �            1259    57745    tx_spec_kain    TABLE     �  CREATE TABLE public.tx_spec_kain (
    i_id integer NOT NULL,
    i_jns_kain integer NOT NULL,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_desain character varying(16)
);
     DROP TABLE public.tx_spec_kain;
       public         postgres    false            �            1259    57748    tx_spec_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tx_spec_kain_i_id_seq;
       public       postgres    false    231            )           0    0    tx_spec_kain_i_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tx_spec_kain_i_id_seq OWNED BY public.tx_spec_kain.i_id;
            public       postgres    false    232            �            1259    57750    tx_spec_pekerjaan    TABLE     �  CREATE TABLE public.tx_spec_pekerjaan (
    i_id integer NOT NULL,
    i_desain character varying(16) NOT NULL,
    i_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_colow_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 %   DROP TABLE public.tx_spec_pekerjaan;
       public         postgres    false            �            1259    57753    tx_spec_pekerjaan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_pekerjaan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tx_spec_pekerjaan_i_id_seq;
       public       postgres    false    233            *           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tx_spec_pekerjaan_i_id_seq OWNED BY public.tx_spec_pekerjaan.i_id;
            public       postgres    false    234            �            1259    57755    tx_workstation    TABLE       CREATE TABLE public.tx_workstation (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_cw character varying(100) NOT NULL,
    i_id_bagian integer NOT NULL,
    n_tot_qty numeric,
    n_proses numeric,
    n_sisa numeric,
    e_ket character varying(250),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_pelaksana character varying(75),
    e_shift character varying(50),
    d_tgl date,
    i_id_kartu integer NOT NULL,
    i_cw integer,
    d_time character varying(10)
);
 "   DROP TABLE public.tx_workstation;
       public         postgres    false            �            1259    57761    tx_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tx_workstation_i_id_seq;
       public       postgres    false    235            +           0    0    tx_workstation_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tx_workstation_i_id_seq OWNED BY public.tx_workstation.i_id;
            public       postgres    false    236            �            1259    57763    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    username character varying(255) NOT NULL,
    role integer
);
    DROP TABLE public.users;
       public         postgres    false            �            1259    57769    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    237            ,           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    238                       2604    57771    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196                       2604    57772    mst_sales_plan i_id    DEFAULT     z   ALTER TABLE ONLY public.mst_sales_plan ALTER COLUMN i_id SET DEFAULT nextval('public.mst_sales_plan_i_id_seq'::regclass);
 B   ALTER TABLE public.mst_sales_plan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    200    199                       2604    57773    mst_so_item i_id    DEFAULT     t   ALTER TABLE ONLY public.mst_so_item ALTER COLUMN i_id SET DEFAULT nextval('public.mst_so_item_i_id_seq'::regclass);
 ?   ALTER TABLE public.mst_so_item ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    203    202                       2604    57774    ref_bagian i_id    DEFAULT     w   ALTER TABLE ONLY public.ref_bagian ALTER COLUMN i_id SET DEFAULT nextval('public.ref_workstation_i_id_seq'::regclass);
 >   ALTER TABLE public.ref_bagian ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    222    205                       2604    57775    ref_kain i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kain_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    207    206                       2604    57776    ref_ket_umum_so_child i_id    DEFAULT     �   ALTER TABLE ONLY public.ref_ket_umum_so_child ALTER COLUMN i_id SET DEFAULT nextval('public.ref_ket_umum_so_child_i_id_seq'::regclass);
 I   ALTER TABLE public.ref_ket_umum_so_child ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    210    209                        2604    57777    ref_logo i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_logo ALTER COLUMN i_id SET DEFAULT nextval('public.ref_logo_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_logo ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    212    211            !           2604    57778    ref_no_urut id    DEFAULT     p   ALTER TABLE ONLY public.ref_no_urut ALTER COLUMN id SET DEFAULT nextval('public.ref_no_urut_id_seq'::regclass);
 =   ALTER TABLE public.ref_no_urut ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    214    213            #           2604    57779    ref_pelanggan i_pel    DEFAULT     z   ALTER TABLE ONLY public.ref_pelanggan ALTER COLUMN i_pel SET DEFAULT nextval('public.ref_pelanggan_i_pel_seq'::regclass);
 B   ALTER TABLE public.ref_pelanggan ALTER COLUMN i_pel DROP DEFAULT;
       public       postgres    false    216    215            1           2604    58102    ref_role i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_role ALTER COLUMN i_id SET DEFAULT nextval('public.ref_role_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_role ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    240    241    241            $           2604    57780    ref_sat_qty i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_sat_qty ALTER COLUMN i_id SET DEFAULT nextval('public.ref_sat_qty_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_sat_qty ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    218    217            &           2604    57781    ref_so_flag i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_so_flag ALTER COLUMN i_id SET DEFAULT nextval('public.ref_so_flag_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_so_flag ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    220    219            )           2604    57782    rfp_lpk i_id    DEFAULT     l   ALTER TABLE ONLY public.rfp_lpk ALTER COLUMN i_id SET DEFAULT nextval('public.rfp_lpk_i_id_seq'::regclass);
 ;   ALTER TABLE public.rfp_lpk ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    226    225            *           2604    57783    tx_jns_proses_kartu i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_jns_proses_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_jns_proses_kartu_i_id_seq'::regclass);
 G   ALTER TABLE public.tx_jns_proses_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    228    227            2           2604    58124    tx_rfp_accepted i_id    DEFAULT     |   ALTER TABLE ONLY public.tx_rfp_accepted ALTER COLUMN i_id SET DEFAULT nextval('public.tx_rfp_accepted_i_id_seq'::regclass);
 C   ALTER TABLE public.tx_rfp_accepted ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    242    243    243            +           2604    57784    tx_sj_kartu i_id    DEFAULT     t   ALTER TABLE ONLY public.tx_sj_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_sj_kartu_i_id_seq'::regclass);
 ?   ALTER TABLE public.tx_sj_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    230    229            ,           2604    57785    tx_spec_kain i_id    DEFAULT     v   ALTER TABLE ONLY public.tx_spec_kain ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_kain_i_id_seq'::regclass);
 @   ALTER TABLE public.tx_spec_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    232    231            -           2604    57786    tx_spec_pekerjaan i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_spec_pekerjaan ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_pekerjaan_i_id_seq'::regclass);
 E   ALTER TABLE public.tx_spec_pekerjaan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    234    233            .           2604    57787    tx_workstation i_id    DEFAULT     z   ALTER TABLE ONLY public.tx_workstation ALTER COLUMN i_id SET DEFAULT nextval('public.tx_workstation_i_id_seq'::regclass);
 B   ALTER TABLE public.tx_workstation ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    236    235            /           2604    57788    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    238    237            �          0    57630 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    196   /�       �          0    57635 	   mst_kartu 
   TABLE DATA               �   COPY public.mst_kartu (i_id, i_no_kartu, d_pengiriman, i_id_so, i_id_rfp, e_lebar_blanket, e_handfeel, d_beres, created_at, updated_at, e_ket, i_status, tot_roll, tot_kg, tot_pjg, e_lebar_kain_jadi) FROM stdin;
    public       postgres    false    198   ��                 0    58088    mst_kartu_cw 
   TABLE DATA               �   COPY public.mst_kartu_cw (i_id, i_id_kartu, e_cw, n_qty_roll, n_qty_pjg, n_qty_kg, created_at, updated_at, n_qty_kg_sisa, i_id_bagian) FROM stdin;
    public       postgres    false    239   F�       �          0    57638    mst_sales_plan 
   TABLE DATA               [   COPY public.mst_sales_plan (i_id, n_qty, bulan, tahun, created_at, updated_at) FROM stdin;
    public       postgres    false    199   S�       �          0    57647    mst_so 
   TABLE DATA               �  COPY public.mst_so (i_id, i_no_so, d_so, i_pel, f_repeat, i_desain, n_qty_warna, e_motif, e_jenis_printing, n_color_way, e_color_way, e_toleransi_cacat, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, d_strike_off, d_approval_strike_off, d_penyerahan_brg, e_keterangan_kirim, i_status, d_approved, v_pekerjaan, v_pekerjaan_plus_ppn, v_discount, n_discount, v_sisa, created_at, updated_at, e_reject, i_no_po, flag_so, hitung_by) FROM stdin;
    public       postgres    false    201   �       �          0    57653    mst_so_item 
   TABLE DATA               �   COPY public.mst_so_item (i_id, i_id_so, e_uraian_pekerjaan, n_qty_roll, n_qty_pjg, n_qty_kg, v_harga_sat, created_at, updated_at, n_qty_kg_sisa, i_id_bagian) FROM stdin;
    public       postgres    false    202   S�       �          0    57662    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    204   ��       �          0    57668 
   ref_bagian 
   TABLE DATA               c   COPY public.ref_bagian (i_id, nama_bagian, e_penanggung_jawab, created_at, updated_at) FROM stdin;
    public       postgres    false    205   ��       �          0    57671    ref_kain 
   TABLE DATA               H   COPY public.ref_kain (i_id, e_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    206   p�       �          0    57676    ref_ket_umum_so 
   TABLE DATA               S   COPY public.ref_ket_umum_so (i_id, e_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    208   ��       �          0    57679    ref_ket_umum_so_child 
   TABLE DATA               n   COPY public.ref_ket_umum_so_child (i_id, i_id_ket_umum, e_child_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    209   v�       �          0    57684    ref_logo 
   TABLE DATA               U   COPY public.ref_logo (i_id, logo_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    211   ��       �          0    57690    ref_no_urut 
   TABLE DATA               K   COPY public.ref_no_urut (id, code, no_urut, bln, thn, flag_so) FROM stdin;
    public       postgres    false    213   =�       �          0    57695    ref_pelanggan 
   TABLE DATA               �   COPY public.ref_pelanggan (i_pel, e_nama_pel, e_alamat_pel, f_pkp, e_npwp_pel, e_telp_pel, e_kont_pel, created_at, updated_at, e_fax_pel, e_kota_pel, e_kode_marketing) FROM stdin;
    public       postgres    false    215   ��                 0    58099    ref_role 
   TABLE DATA               L   COPY public.ref_role (i_id, e_role_name, created_at, update_at) FROM stdin;
    public       postgres    false    241   ��       �          0    57704    ref_sat_qty 
   TABLE DATA               J   COPY public.ref_sat_qty (i_id, e_sat, created_at, updated_at) FROM stdin;
    public       postgres    false    217   ��       �          0    57709    ref_so_flag 
   TABLE DATA               X   COPY public.ref_so_flag (i_id, flag_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    219   ��       �          0    57715    ref_workflow 
   TABLE DATA               P   COPY public.ref_workflow (i_id, definition, created_at, updated_at) FROM stdin;
    public       postgres    false    221   !�       �          0    57722    rfp 
   TABLE DATA               �  COPY public.rfp (i_id, i_no_rfp, d_selesai, i_pel, e_contact_person, f_repeat, i_desain, e_motif, e_material, e_original_cond1, e_ket_ori_cond, e_color, i_penyedia, d_tgl_material_in, n_qty_material, n_pengkerutan, e_gramasi_from, e_gramasi_to, e_penyesuaian_lebar_from, e_penyesuaian_lebar_to, e_pakan_from, e_pakan_to, e_lusi_from, e_lusi_to, e_tekstur_akhir_from, e_tekstur_akhir_to, e_cw_1, e_cw_2, e_cw_3, e_cw_4, e_cw_5, e_cw_6, e_cw_7, e_cw_8, i_status, d_approved_mrk, d_approved_pro, d_approved_ppc, created_at, updated_at, i_id_so, e_reject_mkt, e_reject_prod, e_original_cond2, e_jenis_printing, i_jns_kain, urutan_fifo, f_proses, old_urutan_fifo, d_proses) FROM stdin;
    public       postgres    false    224   ��                  0    57730    rfp_lpk 
   TABLE DATA               V   COPY public.rfp_lpk (i_id, i_id_rfp, e_nomor_lpk, created_at, updated_at) FROM stdin;
    public       postgres    false    225   �                 0    57735    tx_jns_proses_kartu 
   TABLE DATA               d   COPY public.tx_jns_proses_kartu (i_id, description, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    227   ��                 0    58121    tx_rfp_accepted 
   TABLE DATA               j   COPY public.tx_rfp_accepted (i_id, i_id_rfp, d_accepted, user_accept, created_at, updated_at) FROM stdin;
    public       postgres    false    243   B�                 0    57740    tx_sj_kartu 
   TABLE DATA               X   COPY public.tx_sj_kartu (i_id, i_no_sj, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    229   ��                 0    57745    tx_spec_kain 
   TABLE DATA               �   COPY public.tx_spec_kain (i_id, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, i_pel, created_at, updated_at, i_desain) FROM stdin;
    public       postgres    false    231   #�                 0    57750    tx_spec_pekerjaan 
   TABLE DATA               �   COPY public.tx_spec_pekerjaan (i_id, i_desain, i_qty_warna, e_motif, e_jenis_printing, n_colow_way, e_color_way, e_toleransi_cacat, i_pel, created_at, updated_at) FROM stdin;
    public       postgres    false    233   ��       
          0    57755    tx_workstation 
   TABLE DATA               �   COPY public.tx_workstation (i_id, i_id_rfp, e_cw, i_id_bagian, n_tot_qty, n_proses, n_sisa, e_ket, created_at, updated_at, e_pelaksana, e_shift, d_tgl, i_id_kartu, i_cw, d_time) FROM stdin;
    public       postgres    false    235   Z�                 0    57763    users 
   TABLE DATA               �   COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, username, role) FROM stdin;
    public       postgres    false    237   ��       -           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    197            .           0    0    mst_sales_plan_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.mst_sales_plan_i_id_seq', 42, true);
            public       postgres    false    200            /           0    0    mst_so_item_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.mst_so_item_i_id_seq', 78, true);
            public       postgres    false    203            0           0    0    ref_kain_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_kain_i_id_seq', 1, false);
            public       postgres    false    207            1           0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ref_ket_umum_so_child_i_id_seq', 7, true);
            public       postgres    false    210            2           0    0    ref_logo_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.ref_logo_i_id_seq', 4, true);
            public       postgres    false    212            3           0    0    ref_no_urut_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.ref_no_urut_id_seq', 20, true);
            public       postgres    false    214            4           0    0    ref_pelanggan_i_pel_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.ref_pelanggan_i_pel_seq', 1, true);
            public       postgres    false    216            5           0    0    ref_role_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_role_i_id_seq', 1, false);
            public       postgres    false    240            6           0    0    ref_sat_qty_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.ref_sat_qty_i_id_seq', 1, false);
            public       postgres    false    218            7           0    0    ref_so_flag_i_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ref_so_flag_i_id_seq', 5, true);
            public       postgres    false    220            8           0    0    ref_workstation_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_workstation_i_id_seq', 1, false);
            public       postgres    false    222            9           0    0    rfp_lpk_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.rfp_lpk_i_id_seq', 40, true);
            public       postgres    false    226            :           0    0    rfp_urutan_fifo_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.rfp_urutan_fifo_seq', 59, true);
            public       postgres    false    223            ;           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tx_jns_proses_kartu_i_id_seq', 46, true);
            public       postgres    false    228            <           0    0    tx_rfp_accepted_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tx_rfp_accepted_i_id_seq', 3, true);
            public       postgres    false    242            =           0    0    tx_sj_kartu_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_sj_kartu_i_id_seq', 43, true);
            public       postgres    false    230            >           0    0    tx_spec_kain_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_spec_kain_i_id_seq', 4, true);
            public       postgres    false    232            ?           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tx_spec_pekerjaan_i_id_seq', 4, true);
            public       postgres    false    234            @           0    0    tx_workstation_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tx_workstation_i_id_seq', 44, true);
            public       postgres    false    236            A           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 1, false);
            public       postgres    false    238            4           2606    57790    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    196            e           2606    58096    mst_kartu_cw mst_kartu_cw_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mst_kartu_cw
    ADD CONSTRAINT mst_kartu_cw_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.mst_kartu_cw DROP CONSTRAINT mst_kartu_cw_pkey;
       public         postgres    false    239            6           2606    57792    mst_kartu mst_kartu_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_kartu
    ADD CONSTRAINT mst_kartu_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_kartu DROP CONSTRAINT mst_kartu_pkey;
       public         postgres    false    198            8           2606    57794 "   mst_sales_plan mst_sales_plan_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mst_sales_plan
    ADD CONSTRAINT mst_sales_plan_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.mst_sales_plan DROP CONSTRAINT mst_sales_plan_pkey;
       public         postgres    false    199            <           2606    57796    mst_so_item mst_so_item_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.mst_so_item
    ADD CONSTRAINT mst_so_item_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.mst_so_item DROP CONSTRAINT mst_so_item_pkey;
       public         postgres    false    202            :           2606    57798    mst_so mst_so_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.mst_so
    ADD CONSTRAINT mst_so_pkey PRIMARY KEY (i_id);
 <   ALTER TABLE ONLY public.mst_so DROP CONSTRAINT mst_so_pkey;
       public         postgres    false    201            A           2606    57800    ref_kain ref_kain_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_kain
    ADD CONSTRAINT ref_kain_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_kain DROP CONSTRAINT ref_kain_pkey;
       public         postgres    false    206            E           2606    57802 0   ref_ket_umum_so_child ref_ket_umum_so_child_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.ref_ket_umum_so_child
    ADD CONSTRAINT ref_ket_umum_so_child_pkey PRIMARY KEY (i_id);
 Z   ALTER TABLE ONLY public.ref_ket_umum_so_child DROP CONSTRAINT ref_ket_umum_so_child_pkey;
       public         postgres    false    209            C           2606    57804 $   ref_ket_umum_so ref_ket_umum_so_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_ket_umum_so
    ADD CONSTRAINT ref_ket_umum_so_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_ket_umum_so DROP CONSTRAINT ref_ket_umum_so_pkey;
       public         postgres    false    208            G           2606    57806    ref_logo ref_logo_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_logo
    ADD CONSTRAINT ref_logo_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_logo DROP CONSTRAINT ref_logo_pkey;
       public         postgres    false    211            I           2606    57808    ref_no_urut ref_no_urut_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ref_no_urut
    ADD CONSTRAINT ref_no_urut_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ref_no_urut DROP CONSTRAINT ref_no_urut_pkey;
       public         postgres    false    213            K           2606    57810     ref_pelanggan ref_pelanggan_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.ref_pelanggan
    ADD CONSTRAINT ref_pelanggan_pkey PRIMARY KEY (i_pel);
 J   ALTER TABLE ONLY public.ref_pelanggan DROP CONSTRAINT ref_pelanggan_pkey;
       public         postgres    false    215            g           2606    58104    ref_role ref_role_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_role
    ADD CONSTRAINT ref_role_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_role DROP CONSTRAINT ref_role_pkey;
       public         postgres    false    241            M           2606    57812    ref_sat_qty ref_sat_qty_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_sat_qty
    ADD CONSTRAINT ref_sat_qty_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_sat_qty DROP CONSTRAINT ref_sat_qty_pkey;
       public         postgres    false    217            O           2606    57814    ref_so_flag ref_so_flag_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_so_flag
    ADD CONSTRAINT ref_so_flag_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_so_flag DROP CONSTRAINT ref_so_flag_pkey;
       public         postgres    false    219            Q           2606    57816    ref_workflow ref_workflow_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.ref_workflow
    ADD CONSTRAINT ref_workflow_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.ref_workflow DROP CONSTRAINT ref_workflow_pkey;
       public         postgres    false    221            ?           2606    57818    ref_bagian ref_workstation_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ref_bagian
    ADD CONSTRAINT ref_workstation_pkey PRIMARY KEY (i_id);
 I   ALTER TABLE ONLY public.ref_bagian DROP CONSTRAINT ref_workstation_pkey;
       public         postgres    false    205            U           2606    57820    rfp_lpk rfp_lpk_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.rfp_lpk
    ADD CONSTRAINT rfp_lpk_pkey PRIMARY KEY (i_id);
 >   ALTER TABLE ONLY public.rfp_lpk DROP CONSTRAINT rfp_lpk_pkey;
       public         postgres    false    225            S           2606    57822    rfp rfp_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.rfp
    ADD CONSTRAINT rfp_pkey PRIMARY KEY (i_id);
 6   ALTER TABLE ONLY public.rfp DROP CONSTRAINT rfp_pkey;
       public         postgres    false    224            W           2606    57824 ,   tx_jns_proses_kartu tx_jns_proses_kartu_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.tx_jns_proses_kartu
    ADD CONSTRAINT tx_jns_proses_kartu_pkey PRIMARY KEY (i_id);
 V   ALTER TABLE ONLY public.tx_jns_proses_kartu DROP CONSTRAINT tx_jns_proses_kartu_pkey;
       public         postgres    false    227            i           2606    58126 $   tx_rfp_accepted tx_rfp_accepted_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tx_rfp_accepted
    ADD CONSTRAINT tx_rfp_accepted_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.tx_rfp_accepted DROP CONSTRAINT tx_rfp_accepted_pkey;
       public         postgres    false    243            Y           2606    57826    tx_sj_kartu tx_sj_kartu_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tx_sj_kartu
    ADD CONSTRAINT tx_sj_kartu_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.tx_sj_kartu DROP CONSTRAINT tx_sj_kartu_pkey;
       public         postgres    false    229            [           2606    57828    tx_spec_kain tx_spec_kain_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tx_spec_kain
    ADD CONSTRAINT tx_spec_kain_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.tx_spec_kain DROP CONSTRAINT tx_spec_kain_pkey;
       public         postgres    false    231            ]           2606    57830 (   tx_spec_pekerjaan tx_spec_pekerjaan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tx_spec_pekerjaan
    ADD CONSTRAINT tx_spec_pekerjaan_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.tx_spec_pekerjaan DROP CONSTRAINT tx_spec_pekerjaan_pkey;
       public         postgres    false    233            _           2606    57832 "   tx_workstation tx_workstation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tx_workstation
    ADD CONSTRAINT tx_workstation_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.tx_workstation DROP CONSTRAINT tx_workstation_pkey;
       public         postgres    false    235            a           2606    57834    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    237            c           2606    57836    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    237            =           1259    57837    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    204            �   H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.#d��(
����R�R�SKZb���� ]      �   �   x�}�A
�0����@$3��i� ��F��Qm��-$ڔO�iy���+��8DC����p;���g�r�!ھ�ɫ�R�[Qa(�I���,��/����ṀfWc\0���L�N&��39�����j���������`��]�����k��j2d5������]͌��U�ko         �   x���=n�0�z|
.���x�@�����r�m�MDm�6���Ax#@i���{�X��p�������|_G��@�����H݉�D]��k�c��[��� ����GA��
�A�D��H���5��Q�B,9�&��X��܅�;0�� �v�U���g�e��i����*����&��"�i�d�lHm<��Y�A��������H���B@w��YTͫ��_(�LQ��e��P{k)
����|ޣ�W�Ӗ1��w�����      �   �   x�U��� �g3�)!��%n�����,���,5vu��Yt���O��]�fR��R#]���tfv�Ilg+��v���}���6�����*���J�l~�IS={��\ 4G�LjQ�HK�\H=Nv�GT҆z��������@�w��e��LqY1�Lq9??:�8�����|�_6      �   ;  x���Ko�0 �3�+t�is+RO�b�]���^<�K��v�����G�N"�-vM� ��0���*Q�Ī��BH!�+@�˅ц�_z�p5v�Z\�/}׋ۿ��~4�zh���w=�Ǻ�����z#����'�w� >^��x�ۦ]5`\i<<|/�XyH��p��Dp��ĺ�U��|R�&�$2�i �v1�`x�K��|��AـnqM�t1��[�χr+zm�5�S���gq]o��;P�����O��NZ��Ħ�n�Sf +K�r�r���������gr3��2Y"΅�Q'�S%P�V���eV¥ҙW����`�f;�/o��*h&!�q�R+q���'���R�ZgN��gYV�k*(��0�P9���=?0��4}�$1��ϵo�c�I���\Wq��
�s��c�˒�g$t,o��P���ΟNr=����f���O�}[���3�Ѽ4�^\�v�*J��r����sKӗR�DJ���$(oAdR�.�*���2����6)!��l�3?��i<͓���;Y�4�!7��'G�x�'"�*.������      �   G  x����J�0���)z�-3I&issA��� {YA�"{(��6�Φ�"Js�f�w�<��?������|�=��u�m��Ꮊ1Z9�`X+� +o5�"�=X��p�ۆ;0�����d��9�]�sYx��l�	��������wc�H�9�-x ��K2s���R�e�[�_��3K���_�@��Fod���F�&�i�V6�ɼ�*K+�[�sL΍ξ�O#|�����u���F�-y]�i��$kj8�	'��Jg��`M.:;>�/��{��v��[�����" a��ߋ�J_��o���:�����N�������W�0@      �      x������ � �      �   �   x�]�K�0D��)zB��">�@��X��Z�P�i%nO�*T���ٞ�A�}@F'Ϯ��vtl��E�Yl��'n(��{�q[*Kh���%�������sWp"y�Q:Ak����츟D��^&c>�=Y�w�db�3!���Ib      �   Z   x�3�tsV01(Vp���".#�g_S$!c��*��1P(&d
2�@2�tvAUdA1�$bd�$b��X�������� �&/      �   �  x�}QK��0>��b����.�J�8�����^�x�L������ܤ�H�%��=��?���YKd곦*�h[d�t���L^�8����N���<`Td���Z�V�s�`��r!/�5QhǙ� l� �i��z�xO���#��뮗 ���+�'��U���H�<I�C��1 A�4p��Xo����������5��� �p�,�زn�7��s��q�Z�;���%�b��,�(��x��
�Z��V����M�����aG
�N2�ݓ����|���cc2��?+{�=��<��m�����нo��V��l�����s��A���2PW���qO�4b������;
mg��6[�H�lc�"_�?�e��[�<r��\����n]f�$�im&��Ze_����;'6�{      �   e  x����N�0���S���u��+ �	8��.^��I�4ִ��i��*R��?�]e�l�	�;�Z�K��i0��j�4rZ(�K�Z�tGtu�xr�EtPa�"�5�#K�tt�*	Oh"C�0P�@�����i��2�[��.��{��M�|}�nНQ�'�[l�Z�h41R��,�b#��^}{WKulÈ|--��:Aly&����",+L��$Ŀ+��u"tpNk��F�S�����u#���5Nݷ�\��Y��Ǣd�q?C�4��Md-*�">��@`�%��m��}ĠA<����eY�e	�F �!���<.O^���!��x���|)��O����˕�6      �   B   x�3�420�440470720�,ȍ��OϏ�.�+�K�,K���+�[XXr��q��qqq �<      �   I   x�34���4 C�?N#CKN#.C��)\Ԙ��&j5�24�r@S��$��`Zh����� �      �   E  x��Z�r��}nE�!�.�M�c���9���􈶄D5R����na�c���J�i_�Z{7�,at����+&�v��U֦��Co����˗U���t��o7dQ-����{�!�y��nt"E^��?���3��R2:�2X2�#Y���ɜ�6�U�崗)m)�Zt:����G̳��&��9ۊt͖���}�٢3�2a��Iz���s�\Q��Ϟu��j�~��l���\�Ֆ��۲:-�K-�7ہG�{��EfEV�ޙK�|)9ے	�՚�`񤒂Y�~�h?��%�4����%�i4�MC��2��"Y��HV3ܱZ^s�0$�E��&��m5�1C$l�`���D"Rc���5�Y�2@�i��t,:�o#�>��c�8�c&Z�C��}���?k�m�<���տ���kL��h��7�Ǆ,�t\|�������?�  ��V#���dk�!��(�.ܖ(�>>"LW����I��Hk�t[���]�-��&a���3���p[���_{�"}E#�-�v�!����n��|I/�r�����\����S*����6��2>z�GW���1$���e��pDpa�a��K�,�d�A<��w,����o�����+��Kzht�1���^G�,��θ�2M���*�FԿ�|~4�����U��M�9u;V`��j���^!`{%�f�	��:al�_,�.:�	�owt
T�ܱA�A��١)�k�-U�ol�"�[(#�\��vD�b�6��Hsl�W���5KѦ�E�b�K
���}�ЅJ{�+�R�43^݋��^�l �3���	�����Wt���v�_S���l�Y�L���!+���EY����m.�����.��/$7��Te��J�
J��̜^�*�
.�����\�9������;���_����݌���>�/�=n����rO
i�d��+~�*9���G�'}�nE`h�-[�P��!s�iq�Q�Q`��yd�i��=>󉪦��^��N��v�ƀ�!֋���A2�����gY��q��m-����\d�R?�3$����|���c,A/���	8�&�G���߸P�1c�~z\��M��S��	y�ꒋT
%BYY���?�FÕn`|��/��e��<)�X,��M�����r_� P��cV���4U�;�w������m��Z5�G����
$�j�^�,N�"̶��w.�!�{�����A������+�UOB���6���tc.�K�pT�x�g�q��Q$w��M~y
:��O�I���Nɠ�pP�Laq7hE��::I/�/e\pJg�ĸR�'d,��vɐ2���$�j�b3:P��f��C#uu9hE��)���Ӡ�|�b�ȥ�ӳ��y�*UX���z�z���ѷi��w&��r	���<;���9!Z�><�l�F�J�B�~�"�
0��c6��!o��3�F�LD�՜�����YK�����4 �`bdY�#�5l��gyE���Bry1�/O��.-1f�r��^̦�?��6��=�&s��tX�#�� ��
�z�d��ىY�٘ėmzɶ;���Vm��0i� L�g<�ћ��*U��:x���KU�>�m�In�K �"/|a�À�uP��ڏ%�gԫ�ݛd�c�+ENc�~��&5	���p���`h"��N����"�{�����!1�+`��V��*
[���w�;e�j0<��C!ۊU�?��qȔy�e�r�#��L�u�r<�1L﬇R \�b��xj����u�z@�e�GB8�r��M���*h"EqDe�x�ʰ��Դv0;�P�f�Xl
��Ei����5g��'�X����K�#6'�{6UcN��[����L@�����$
˩}Y���Qr+�VU��®s��ŵ,�[8'$����V��g�zS����z��-Y/�,�wnǶ��R�,�0�>��]����+l���.�D��j8�������X܁k�u���݇��ض�wB<O~P� *�H�(Q��y�Q����1[�ھ�N*C\���Vп�"�Qy~��N��rmx���F'%L��Jqi��U� Ul:{��|'�t�c�G�Fw�⦐:ܘ1�Ԝ8*"�B9ۿ�Ɣ�����n�if�%��V���V��L����tL�5-iEV_ݢ��LK�hF�g�'����ܽ&#�3t�xd�FEI�H���1�Te��L�s� ��i�7�u���!=�����6U�g����,���<�=��=Ƽ�=�i�Ý��q���j<f�zn)Tr�Ӆ}ݮ�lIl�Q�i�,���<��$��j����5�X��gY�k{'�L��H�7~/dY�j@��d�,�!��SA=%Nx&L�/�}�L�ы� 2 �,O]2����|C'��t��u��4zJL�:�s.�jg.��w�eVA��y��k���H�{�����L��]��{�=^������G�/ӟw�n�0:�okh�r2g�Mf��9���F�|�9�2������E�Ş�̬GAsA-=l�zN>���E�%�;���^��>7Tۇ�5lP�a�梲qq=�?��7$by',�G�:+���-�N'N�z��g��Z|1=��VSe;Vo� ��<�2����9ë;�Gf= s)�Pn�el����e��|k�Tw��~��rl���ɼ�`Yi�O�iħK���<g�V��@�qBZx[*����������f��2�A�}U��"˸�
 �9+����@�ؽ���2@�2Ԁ��=`p��8㘳��o~���]��E���VOT'nr��&Q&�xNb����:~�E�D#���
�C �����a���5m���O��`�������         �   x�u�?�0���Sdr���_G��H%���!Ŵ\[�oo�DbAȐ��{���j�ֈ,M�:��Ө"���/dLQ[��[;�,��R;�\��/"+'��JS��A��	�x}���9f����{A�2	��v������5�(#g������H���w�}��4=z^�9�/8di�      �      x������ � �      �   F   x�3�t*U.��,��".#�@@jB�$☛�X2��/-)�/-J��K���r��d��b���� �>�      �   �   x�u�K� EǏU���ϰ�hI;��K���Pp��,M4a@ν��qe�����K%4�z��V�"Ⱥ���R(�W=�Bj��������Gf���c�CDϭ���[79�a��7\�+�s#��w�ob+m�2�w4��s��h3m/]%�
��s+�q�X����f���ȥm�      �   *  x���Mo�0��ʯбÐ��>��`k݂��K/�&�\gp����c7ql�Ɇ� ��(�}D����j�S�գ ��YC�b]>zćYY�E�oy��I.�"��X�>�o�M��M*>������L	ZP�,°͜�s��?�Bf��bU��*+?�oI)��!孷����7Ǆ۴`��`�	����q7\o����B^�S���7C��/��G~�׍�U�����8�H����y�ݒ��N����RkM����6��̒�hƊ�z:Q-����r�ٶ�m��wq�v�w�N~����]�6���O6}L�L���2�xV�ס�Tj�:�1�б�
�SQ]$�T�N4� r���$�	�P	����"A�{&gm��&f��=Ϥj	�Ԁ���6᮰�6�H�1&F�n-��u�o!��='�⤏�b|)����4?�!ٳ�Әmy�l�o8�ۖ6�v�M_#��2`W���c�Ǉf0-�M=��ɗ5Cuk���ٌ���c:�=�	���7ٮ9�;��<�v*�~��m^.C�������d2�¨�          �   x��α�0�Z���u��i(�������<���֩����T�d�_D�X�:�ؤ����E�|[u�U��Q�f��a������D�`el���sj*�IH��N�F�S����3,���Q�Y>��ye�'`L         z   x��л�@�خ�8���@D�_Ǚ;	��d��`F���A��ʴr_H�[Pe��@f@I�3�&��~�]zeh��أ��ZG�y������~�B{p�ϸeh�Q�L�ト_7�R�         H   x�m���0�ޗ)X��.Q*�!����A�B%�,[8uq8f��	�ݘ#�A��;�G��YNȷ��u�����         y   x��л�0�ڞ�@���hi�HD��wz��ʰ�B��L#/ix*�UH��,�� �6ȿ����(A!�Ok!shieh盄@��7�����?���\�3^���Z;�>!�8�R�         i   x�m�1
�0��99�h}MM;n��U���㫠��=��P�q_�u!��N�D����NA�<d4�T�
�mC�NE�uΔ_� ��T���VU�u��!�5�#3��|!�         �   x�m�=� ����+�⤵p+�4&�1qv���U0��Mj��{����r&�t�A�2��ō{p��v'�&w���ٌG�qq� ���O��~�`���p\� ���\GQR���U��j��zb�Ѓ��1e����ߜ������Bk��gH�_~Te$��5��2���c@
      
   M  x����N!�3<ž@��pԋ��1������6Y��k���s������>u?�[5�3�u���rL  �ɥ��m��RڸJ�
��f��@6(d���&���ԉ����dM6���B�i�Gr����TA��ج�Il�:n�
��	FImj��7�GPe�l-K��5T�}o���@A]�:d�-[[o��v5�nqI��-����j�U�j��ld�)/���T{�J��FzT����ԯ���L_26 �{��[IX�7���T���Ng\�p���`)얻�l6E;I�lO���Q��g3��̭�W0}�|���=d{Ү�˹����K�         �  x���Oo�0��}?������Sq&�?qnƄ�X�R+:��D!َ��!��y����*�	'.��O�GC�]�ƫ�^J�R
;�o�Eh��g�}X�z���TE 4j�l�f���j�j��]��dL�h�^h�C�g� WĶ"F*hi,!��X���ƪ��T�QZ�e�+�Q;W�G,!VH��_�˻���C-���U�F����l@���-��p#נ��dJv2%�B��<)k��a��m��&+��W���������d<�1�+��ǜ���Ɯ�0�*�ܕ䔷(�b��&�e��զ����%��U��(���>a��*��-�g��y��Yo�5�q�|]��xܷ�F0�T����﬷��|LOڠ�����|imyv��`�,� ��V0     