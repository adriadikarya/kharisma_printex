PGDMP                  	    	    w            kharisma_printex    11.4    11.2 x    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    57402    kharisma_printex    DATABASE     �   CREATE DATABASE kharisma_printex WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
     DROP DATABASE kharisma_printex;
             postgres    false            �            1259    57403 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    57406    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    196            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    197            �            1259    57408 	   mst_kartu    TABLE     �  CREATE TABLE public.mst_kartu (
    i_id integer NOT NULL,
    i_no_kartu character varying(25) NOT NULL,
    d_pengiriman date,
    i_id_so integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_lebar_blanket character varying(75),
    e_handfeel character varying(75),
    d_beres timestamp(4) with time zone,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    e_ket character varying(250),
    i_status integer
);
    DROP TABLE public.mst_kartu;
       public         postgres    false            �            1259    57411    mst_so    TABLE     n  CREATE TABLE public.mst_so (
    i_id integer NOT NULL,
    i_no_so character varying(16) NOT NULL,
    d_so date,
    i_pel integer,
    f_repeat boolean,
    i_desain character varying(16),
    n_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_color_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    d_strike_off date,
    d_approval_strike_off date,
    d_penyerahan_brg date,
    e_keterangan_kirim character varying(100),
    i_status integer,
    d_approved timestamp(6) with time zone,
    v_pekerjaan numeric,
    v_pekerjaan_plus_ppn numeric,
    v_discount numeric,
    n_discount real,
    v_sisa numeric,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    e_reject character varying(200),
    i_no_po character varying(25)
);
    DROP TABLE public.mst_so;
       public         postgres    false            �            1259    57417    mst_so_item    TABLE     �  CREATE TABLE public.mst_so_item (
    i_id integer NOT NULL,
    i_id_so integer NOT NULL,
    e_uraian_pekerjaan character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    v_harga_sat numeric,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0
);
    DROP TABLE public.mst_so_item;
       public         postgres    false            �            1259    57423    mst_so_item_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_so_item_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.mst_so_item_i_id_seq;
       public       postgres    false    200            �           0    0    mst_so_item_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.mst_so_item_i_id_seq OWNED BY public.mst_so_item.i_id;
            public       postgres    false    201            �            1259    57425    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false            �            1259    57431 
   ref_bagian    TABLE     �   CREATE TABLE public.ref_bagian (
    i_id integer NOT NULL,
    nama_bagian character varying(50) NOT NULL,
    e_penanggung_jawab character varying(100),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
    DROP TABLE public.ref_bagian;
       public         postgres    false            �            1259    57434    ref_kain    TABLE     �   CREATE TABLE public.ref_kain (
    i_id integer NOT NULL,
    e_kain character varying(100),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
    DROP TABLE public.ref_kain;
       public         postgres    false            �            1259    57437    ref_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_kain_i_id_seq;
       public       postgres    false    204            �           0    0    ref_kain_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_kain_i_id_seq OWNED BY public.ref_kain.i_id;
            public       postgres    false    205            �            1259    57439    ref_no_urut    TABLE     �   CREATE TABLE public.ref_no_urut (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    no_urut character varying(11),
    bln character varying(2),
    thn character varying(5)
);
    DROP TABLE public.ref_no_urut;
       public         postgres    false            �            1259    57442    ref_no_urut_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_no_urut_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.ref_no_urut_id_seq;
       public       postgres    false    206            �           0    0    ref_no_urut_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.ref_no_urut_id_seq OWNED BY public.ref_no_urut.id;
            public       postgres    false    207            �            1259    57444    ref_pelanggan    TABLE     �  CREATE TABLE public.ref_pelanggan (
    i_pel integer NOT NULL,
    e_nama_pel character varying(255) NOT NULL,
    e_alamat_pel character varying(255),
    f_pkp boolean DEFAULT false,
    e_npwp_pel character varying(30),
    e_telp_pel character varying(30),
    e_kont_pel character varying(100),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
 !   DROP TABLE public.ref_pelanggan;
       public         postgres    false            �            1259    57451    ref_pelanggan_i_pel_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_pelanggan_i_pel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.ref_pelanggan_i_pel_seq;
       public       postgres    false    208            �           0    0    ref_pelanggan_i_pel_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.ref_pelanggan_i_pel_seq OWNED BY public.ref_pelanggan.i_pel;
            public       postgres    false    209            �            1259    57453    ref_sat_qty    TABLE     �   CREATE TABLE public.ref_sat_qty (
    i_id integer NOT NULL,
    e_sat character varying(100),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
    DROP TABLE public.ref_sat_qty;
       public         postgres    false            �            1259    57456    ref_sat_qty_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_sat_qty_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_sat_qty_i_id_seq;
       public       postgres    false    210            �           0    0    ref_sat_qty_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_sat_qty_i_id_seq OWNED BY public.ref_sat_qty.i_id;
            public       postgres    false    211            �            1259    57458    ref_workflow    TABLE     �   CREATE TABLE public.ref_workflow (
    i_id integer NOT NULL,
    definition character varying(100),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
     DROP TABLE public.ref_workflow;
       public         postgres    false            �            1259    57461    ref_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_workstation_i_id_seq;
       public       postgres    false    203            �           0    0    ref_workstation_i_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public.ref_workstation_i_id_seq OWNED BY public.ref_bagian.i_id;
            public       postgres    false    213            �            1259    57463    rfp_urutan_fifo_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_urutan_fifo_seq
    START WITH 51
    INCREMENT BY 1
    MINVALUE 51
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.rfp_urutan_fifo_seq;
       public       postgres    false            �            1259    57465    rfp    TABLE     v  CREATE TABLE public.rfp (
    i_id integer NOT NULL,
    i_no_rfp character varying(16) NOT NULL,
    d_selesai date,
    i_pel integer,
    e_contact_person character varying(50),
    f_repeat boolean,
    i_desain character varying(16),
    e_motif character varying(50),
    e_material character varying(50),
    e_original_cond1 character varying(25),
    e_ket_ori_cond character varying(50),
    e_color character varying(50),
    i_penyedia integer,
    d_tgl_material_in date,
    n_qty_material numeric,
    n_pengkerutan numeric,
    e_gramasi_from character varying(50),
    e_gramasi_to character varying(50),
    e_penyesuaian_lebar_from character varying(50),
    e_penyesuaian_lebar_to character varying(50),
    e_pakan_from character varying(50),
    e_pakan_to character varying(50),
    e_lusi_from character varying(50),
    e_lusi_to character varying(50),
    e_tekstur_akhir_from character varying(50),
    e_tekstur_akhir_to character varying(50),
    e_cw_1 character varying(50),
    e_cw_2 character varying(50),
    e_cw_3 character varying(50),
    e_cw_4 character varying(50),
    e_cw_5 character varying(50),
    e_cw_6 character varying(50),
    e_cw_7 character varying(50),
    e_cw_8 character varying(50),
    i_status integer,
    d_approved_mrk timestamp(6) with time zone,
    d_approved_pro timestamp(6) with time zone,
    d_approved_ppc timestamp(6) with time zone,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    i_id_so integer,
    e_reject_mkt character varying(200),
    e_reject_prod character varying(200),
    e_original_cond2 character varying(25),
    e_jenis_printing character varying(25),
    i_jns_kain integer,
    urutan_fifo integer DEFAULT nextval('public.rfp_urutan_fifo_seq'::regclass) NOT NULL,
    f_proses boolean DEFAULT false,
    old_urutan_fifo integer,
    d_proses timestamp with time zone
);
    DROP TABLE public.rfp;
       public         postgres    false    214            �            1259    57473    rfp_lpk    TABLE     �   CREATE TABLE public.rfp_lpk (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_nomor_lpk character varying(150),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
    DROP TABLE public.rfp_lpk;
       public         postgres    false            �            1259    57476    rfp_lpk_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_lpk_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.rfp_lpk_i_id_seq;
       public       postgres    false    216            �           0    0    rfp_lpk_i_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.rfp_lpk_i_id_seq OWNED BY public.rfp_lpk.i_id;
            public       postgres    false    217            �            1259    57478    tx_jns_proses_kartu    TABLE     �   CREATE TABLE public.tx_jns_proses_kartu (
    i_id integer NOT NULL,
    description character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
 '   DROP TABLE public.tx_jns_proses_kartu;
       public         postgres    false            �            1259    57481    tx_jns_proses_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_jns_proses_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tx_jns_proses_kartu_i_id_seq;
       public       postgres    false    218            �           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tx_jns_proses_kartu_i_id_seq OWNED BY public.tx_jns_proses_kartu.i_id;
            public       postgres    false    219            �            1259    57483    tx_sj_kartu    TABLE     �   CREATE TABLE public.tx_sj_kartu (
    i_id integer NOT NULL,
    i_no_sj character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
    DROP TABLE public.tx_sj_kartu;
       public         postgres    false            �            1259    57486    tx_sj_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sj_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tx_sj_kartu_i_id_seq;
       public       postgres    false    220            �           0    0    tx_sj_kartu_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tx_sj_kartu_i_id_seq OWNED BY public.tx_sj_kartu.i_id;
            public       postgres    false    221            �            1259    57488    tx_spec_kain    TABLE     �  CREATE TABLE public.tx_spec_kain (
    i_id integer NOT NULL,
    i_jns_kain integer NOT NULL,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    i_pel integer,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    i_desain character varying(16)
);
     DROP TABLE public.tx_spec_kain;
       public         postgres    false            �            1259    57491    tx_spec_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tx_spec_kain_i_id_seq;
       public       postgres    false    222            �           0    0    tx_spec_kain_i_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tx_spec_kain_i_id_seq OWNED BY public.tx_spec_kain.i_id;
            public       postgres    false    223            �            1259    57493    tx_spec_pekerjaan    TABLE     �  CREATE TABLE public.tx_spec_pekerjaan (
    i_id integer NOT NULL,
    i_desain character varying(16) NOT NULL,
    i_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_colow_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_pel integer,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone
);
 %   DROP TABLE public.tx_spec_pekerjaan;
       public         postgres    false            �            1259    57496    tx_spec_pekerjaan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_pekerjaan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tx_spec_pekerjaan_i_id_seq;
       public       postgres    false    224            �           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tx_spec_pekerjaan_i_id_seq OWNED BY public.tx_spec_pekerjaan.i_id;
            public       postgres    false    225            �            1259    57498    tx_workstation    TABLE       CREATE TABLE public.tx_workstation (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_cw character varying(100) NOT NULL,
    i_id_bagian integer NOT NULL,
    n_tot_qty numeric,
    n_proses numeric,
    n_sisa numeric,
    e_ket character varying(250),
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    e_pelaksana character varying(75),
    e_shift character varying(50),
    d_tgl date,
    i_id_kartu integer NOT NULL,
    i_cw integer,
    d_time character varying(10)
);
 "   DROP TABLE public.tx_workstation;
       public         postgres    false            �            1259    57504    tx_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tx_workstation_i_id_seq;
       public       postgres    false    226            �           0    0    tx_workstation_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tx_workstation_i_id_seq OWNED BY public.tx_workstation.i_id;
            public       postgres    false    227            �            1259    57506    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    username character varying(255) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false            �            1259    57512    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    228            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    229            �
           2604    57514    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196            �
           2604    57515    mst_so_item i_id    DEFAULT     t   ALTER TABLE ONLY public.mst_so_item ALTER COLUMN i_id SET DEFAULT nextval('public.mst_so_item_i_id_seq'::regclass);
 ?   ALTER TABLE public.mst_so_item ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    201    200            �
           2604    57516    ref_bagian i_id    DEFAULT     w   ALTER TABLE ONLY public.ref_bagian ALTER COLUMN i_id SET DEFAULT nextval('public.ref_workstation_i_id_seq'::regclass);
 >   ALTER TABLE public.ref_bagian ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    213    203            �
           2604    57517    ref_kain i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kain_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    205    204            �
           2604    57518    ref_no_urut id    DEFAULT     p   ALTER TABLE ONLY public.ref_no_urut ALTER COLUMN id SET DEFAULT nextval('public.ref_no_urut_id_seq'::regclass);
 =   ALTER TABLE public.ref_no_urut ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    206            �
           2604    57519    ref_pelanggan i_pel    DEFAULT     z   ALTER TABLE ONLY public.ref_pelanggan ALTER COLUMN i_pel SET DEFAULT nextval('public.ref_pelanggan_i_pel_seq'::regclass);
 B   ALTER TABLE public.ref_pelanggan ALTER COLUMN i_pel DROP DEFAULT;
       public       postgres    false    209    208            �
           2604    57520    ref_sat_qty i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_sat_qty ALTER COLUMN i_id SET DEFAULT nextval('public.ref_sat_qty_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_sat_qty ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    211    210            �
           2604    57521    rfp_lpk i_id    DEFAULT     l   ALTER TABLE ONLY public.rfp_lpk ALTER COLUMN i_id SET DEFAULT nextval('public.rfp_lpk_i_id_seq'::regclass);
 ;   ALTER TABLE public.rfp_lpk ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    217    216            �
           2604    57522    tx_jns_proses_kartu i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_jns_proses_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_jns_proses_kartu_i_id_seq'::regclass);
 G   ALTER TABLE public.tx_jns_proses_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    219    218            �
           2604    57523    tx_sj_kartu i_id    DEFAULT     t   ALTER TABLE ONLY public.tx_sj_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_sj_kartu_i_id_seq'::regclass);
 ?   ALTER TABLE public.tx_sj_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    221    220            �
           2604    57524    tx_spec_kain i_id    DEFAULT     v   ALTER TABLE ONLY public.tx_spec_kain ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_kain_i_id_seq'::regclass);
 @   ALTER TABLE public.tx_spec_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    223    222            �
           2604    57525    tx_spec_pekerjaan i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_spec_pekerjaan ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_pekerjaan_i_id_seq'::regclass);
 E   ALTER TABLE public.tx_spec_pekerjaan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    225    224            �
           2604    57526    tx_workstation i_id    DEFAULT     z   ALTER TABLE ONLY public.tx_workstation ALTER COLUMN i_id SET DEFAULT nextval('public.tx_workstation_i_id_seq'::regclass);
 B   ALTER TABLE public.tx_workstation ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    227    226            �
           2604    57527    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    229    228            �          0    57403 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    196   �       �          0    57408 	   mst_kartu 
   TABLE DATA               �   COPY public.mst_kartu (i_id, i_no_kartu, d_pengiriman, i_id_so, i_id_rfp, e_lebar_blanket, e_handfeel, d_beres, created_at, updated_at, e_ket, i_status) FROM stdin;
    public       postgres    false    198   g�       �          0    57411    mst_so 
   TABLE DATA               �  COPY public.mst_so (i_id, i_no_so, d_so, i_pel, f_repeat, i_desain, n_qty_warna, e_motif, e_jenis_printing, n_color_way, e_color_way, e_toleransi_cacat, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, d_strike_off, d_approval_strike_off, d_penyerahan_brg, e_keterangan_kirim, i_status, d_approved, v_pekerjaan, v_pekerjaan_plus_ppn, v_discount, n_discount, v_sisa, created_at, updated_at, e_reject, i_no_po) FROM stdin;
    public       postgres    false    199   "�       �          0    57417    mst_so_item 
   TABLE DATA               �   COPY public.mst_so_item (i_id, i_id_so, e_uraian_pekerjaan, n_qty_roll, n_qty_pjg, n_qty_kg, v_harga_sat, created_at, updated_at, n_qty_kg_sisa, i_id_bagian) FROM stdin;
    public       postgres    false    200   Þ       �          0    57425    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    202   ̟       �          0    57431 
   ref_bagian 
   TABLE DATA               c   COPY public.ref_bagian (i_id, nama_bagian, e_penanggung_jawab, created_at, updated_at) FROM stdin;
    public       postgres    false    203   �       �          0    57434    ref_kain 
   TABLE DATA               H   COPY public.ref_kain (i_id, e_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    204   ��       �          0    57439    ref_no_urut 
   TABLE DATA               B   COPY public.ref_no_urut (id, code, no_urut, bln, thn) FROM stdin;
    public       postgres    false    206   ��       �          0    57444    ref_pelanggan 
   TABLE DATA               �   COPY public.ref_pelanggan (i_pel, e_nama_pel, e_alamat_pel, f_pkp, e_npwp_pel, e_telp_pel, e_kont_pel, created_at, updated_at) FROM stdin;
    public       postgres    false    208   ;�       �          0    57453    ref_sat_qty 
   TABLE DATA               J   COPY public.ref_sat_qty (i_id, e_sat, created_at, updated_at) FROM stdin;
    public       postgres    false    210   ��       �          0    57458    ref_workflow 
   TABLE DATA               P   COPY public.ref_workflow (i_id, definition, created_at, updated_at) FROM stdin;
    public       postgres    false    212   ȡ       �          0    57465    rfp 
   TABLE DATA               �  COPY public.rfp (i_id, i_no_rfp, d_selesai, i_pel, e_contact_person, f_repeat, i_desain, e_motif, e_material, e_original_cond1, e_ket_ori_cond, e_color, i_penyedia, d_tgl_material_in, n_qty_material, n_pengkerutan, e_gramasi_from, e_gramasi_to, e_penyesuaian_lebar_from, e_penyesuaian_lebar_to, e_pakan_from, e_pakan_to, e_lusi_from, e_lusi_to, e_tekstur_akhir_from, e_tekstur_akhir_to, e_cw_1, e_cw_2, e_cw_3, e_cw_4, e_cw_5, e_cw_6, e_cw_7, e_cw_8, i_status, d_approved_mrk, d_approved_pro, d_approved_ppc, created_at, updated_at, i_id_so, e_reject_mkt, e_reject_prod, e_original_cond2, e_jenis_printing, i_jns_kain, urutan_fifo, f_proses, old_urutan_fifo, d_proses) FROM stdin;
    public       postgres    false    215   ��       �          0    57473    rfp_lpk 
   TABLE DATA               V   COPY public.rfp_lpk (i_id, i_id_rfp, e_nomor_lpk, created_at, updated_at) FROM stdin;
    public       postgres    false    216   ��       �          0    57478    tx_jns_proses_kartu 
   TABLE DATA               d   COPY public.tx_jns_proses_kartu (i_id, description, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    218   "�       �          0    57483    tx_sj_kartu 
   TABLE DATA               X   COPY public.tx_sj_kartu (i_id, i_no_sj, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    220   ��       �          0    57488    tx_spec_kain 
   TABLE DATA               �   COPY public.tx_spec_kain (i_id, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, i_pel, created_at, updated_at, i_desain) FROM stdin;
    public       postgres    false    222   I�       �          0    57493    tx_spec_pekerjaan 
   TABLE DATA               �   COPY public.tx_spec_pekerjaan (i_id, i_desain, i_qty_warna, e_motif, e_jenis_printing, n_colow_way, e_color_way, e_toleransi_cacat, i_pel, created_at, updated_at) FROM stdin;
    public       postgres    false    224   ɦ       �          0    57498    tx_workstation 
   TABLE DATA               �   COPY public.tx_workstation (i_id, i_id_rfp, e_cw, i_id_bagian, n_tot_qty, n_proses, n_sisa, e_ket, created_at, updated_at, e_pelaksana, e_shift, d_tgl, i_id_kartu, i_cw, d_time) FROM stdin;
    public       postgres    false    226   ��       �          0    57506    users 
   TABLE DATA                  COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, username) FROM stdin;
    public       postgres    false    228   Z�       �           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    197            �           0    0    mst_so_item_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.mst_so_item_i_id_seq', 55, true);
            public       postgres    false    201            �           0    0    ref_kain_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_kain_i_id_seq', 1, false);
            public       postgres    false    205            �           0    0    ref_no_urut_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.ref_no_urut_id_seq', 12, true);
            public       postgres    false    207            �           0    0    ref_pelanggan_i_pel_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.ref_pelanggan_i_pel_seq', 1, true);
            public       postgres    false    209            �           0    0    ref_sat_qty_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.ref_sat_qty_i_id_seq', 1, false);
            public       postgres    false    211            �           0    0    ref_workstation_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_workstation_i_id_seq', 1, false);
            public       postgres    false    213            �           0    0    rfp_lpk_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.rfp_lpk_i_id_seq', 28, true);
            public       postgres    false    217            �           0    0    rfp_urutan_fifo_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.rfp_urutan_fifo_seq', 54, true);
            public       postgres    false    214            �           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.tx_jns_proses_kartu_i_id_seq', 24, true);
            public       postgres    false    219            �           0    0    tx_sj_kartu_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_sj_kartu_i_id_seq', 21, true);
            public       postgres    false    221            �           0    0    tx_spec_kain_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.tx_spec_kain_i_id_seq', 4, true);
            public       postgres    false    223            �           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tx_spec_pekerjaan_i_id_seq', 4, true);
            public       postgres    false    225            �           0    0    tx_workstation_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.tx_workstation_i_id_seq', 32, true);
            public       postgres    false    227            �           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 1, false);
            public       postgres    false    229            �
           2606    57529    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    196            �
           2606    57531    mst_kartu mst_kartu_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_kartu
    ADD CONSTRAINT mst_kartu_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_kartu DROP CONSTRAINT mst_kartu_pkey;
       public         postgres    false    198                       2606    57533    mst_so_item mst_so_item_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.mst_so_item
    ADD CONSTRAINT mst_so_item_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.mst_so_item DROP CONSTRAINT mst_so_item_pkey;
       public         postgres    false    200            �
           2606    57535    mst_so mst_so_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.mst_so
    ADD CONSTRAINT mst_so_pkey PRIMARY KEY (i_id);
 <   ALTER TABLE ONLY public.mst_so DROP CONSTRAINT mst_so_pkey;
       public         postgres    false    199                       2606    57537    ref_kain ref_kain_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_kain
    ADD CONSTRAINT ref_kain_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_kain DROP CONSTRAINT ref_kain_pkey;
       public         postgres    false    204                       2606    57539    ref_no_urut ref_no_urut_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ref_no_urut
    ADD CONSTRAINT ref_no_urut_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ref_no_urut DROP CONSTRAINT ref_no_urut_pkey;
       public         postgres    false    206            
           2606    57541     ref_pelanggan ref_pelanggan_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.ref_pelanggan
    ADD CONSTRAINT ref_pelanggan_pkey PRIMARY KEY (i_pel);
 J   ALTER TABLE ONLY public.ref_pelanggan DROP CONSTRAINT ref_pelanggan_pkey;
       public         postgres    false    208                       2606    57543    ref_sat_qty ref_sat_qty_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_sat_qty
    ADD CONSTRAINT ref_sat_qty_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_sat_qty DROP CONSTRAINT ref_sat_qty_pkey;
       public         postgres    false    210                       2606    57545    ref_workflow ref_workflow_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.ref_workflow
    ADD CONSTRAINT ref_workflow_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.ref_workflow DROP CONSTRAINT ref_workflow_pkey;
       public         postgres    false    212                       2606    57547    ref_bagian ref_workstation_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ref_bagian
    ADD CONSTRAINT ref_workstation_pkey PRIMARY KEY (i_id);
 I   ALTER TABLE ONLY public.ref_bagian DROP CONSTRAINT ref_workstation_pkey;
       public         postgres    false    203                       2606    57549    rfp_lpk rfp_lpk_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.rfp_lpk
    ADD CONSTRAINT rfp_lpk_pkey PRIMARY KEY (i_id);
 >   ALTER TABLE ONLY public.rfp_lpk DROP CONSTRAINT rfp_lpk_pkey;
       public         postgres    false    216                       2606    57551    rfp rfp_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.rfp
    ADD CONSTRAINT rfp_pkey PRIMARY KEY (i_id);
 6   ALTER TABLE ONLY public.rfp DROP CONSTRAINT rfp_pkey;
       public         postgres    false    215                       2606    57553 ,   tx_jns_proses_kartu tx_jns_proses_kartu_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.tx_jns_proses_kartu
    ADD CONSTRAINT tx_jns_proses_kartu_pkey PRIMARY KEY (i_id);
 V   ALTER TABLE ONLY public.tx_jns_proses_kartu DROP CONSTRAINT tx_jns_proses_kartu_pkey;
       public         postgres    false    218                       2606    57555    tx_sj_kartu tx_sj_kartu_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tx_sj_kartu
    ADD CONSTRAINT tx_sj_kartu_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.tx_sj_kartu DROP CONSTRAINT tx_sj_kartu_pkey;
       public         postgres    false    220                       2606    57557    tx_spec_kain tx_spec_kain_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tx_spec_kain
    ADD CONSTRAINT tx_spec_kain_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.tx_spec_kain DROP CONSTRAINT tx_spec_kain_pkey;
       public         postgres    false    222                       2606    57559 (   tx_spec_pekerjaan tx_spec_pekerjaan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tx_spec_pekerjaan
    ADD CONSTRAINT tx_spec_pekerjaan_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.tx_spec_pekerjaan DROP CONSTRAINT tx_spec_pekerjaan_pkey;
       public         postgres    false    224                       2606    57561 "   tx_workstation tx_workstation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tx_workstation
    ADD CONSTRAINT tx_workstation_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.tx_workstation DROP CONSTRAINT tx_workstation_pkey;
       public         postgres    false    226                       2606    57563    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    228                        2606    57565    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    228                       1259    57566    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    202            �   H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.#d��(
����R�R�SKZb���� ]      �   �   x�}�M�@�םS�7c�3`�`b< �1�1"��L7����R���2RMh�!�V!��#\��y|�.�*���q�喬�,�u��R�0>�g��)��L�'�[b��M	���b�����¨�+�,OZ2h�
i�W�K�w����o�݈�#'�7�����T�      �   �  x����N�0 ���S��̌=���J !�U���� �%@�M���kg-v�]Gz����4f@D�7�<��S	�Tf�: ��뙳�Xf`�ͺR�m_߫���~����.��jP׫�zP���D=Uu�^ھm�a�E���+(0�o`B�b��Sw�ݨ�8����jo�':ED��2��^.��u�����8.�6���q8�}��8�f���~OJ�0�܁KTW���i����}��DuW5��v���Ջ����/j3l�^��
��qB���.�#h,o�I,P�Y�[��7���G�4z�G�����f"��<�����n-7*�QD��eʲ�ŅGwDʅ��H�$EQ���֒I�ѰؾG��=�IMfG��*I�q$}H�=��fBF�ly6����7�      �   �   x����
�0���)܋��sݺ�.�B�.�Z�A��kbѫF����.w'*�٭)붬��R��.8wU�" `h=D
��R�`��]���097��6���e��k�)d��X*ưM�j��M��j �m@g@��ӓ��������|3�%S�|7����"^���vM'<���*�����z>R����@��A[9XtĄwO�-.!-�O�����½�8��?�(�hW
����p#�s=���?6̿      �      x������ � �      �   �   x�]���0D��W�-��E,�@��X��Z�Pe��ߓcZi.3��8E���7��lZXC���c��-qK	XA����iY����o�n�F�hd�	�������]E�mAś�'vPk��Nl��j��/�H=�dI�,���I�      �   Z   x�3�tsV01(Vp���".#�g_S$!c��*��1P(&d
2�@2�tvAUdA1�$bd�$b��X�������� �&/      �   0   x�����4000���420��24�r �� Č8�P���qqq gH      �   `   x�3���LLOT��+�T��H,�,�M����S���R���31UpJ�K)�K�,�4@�1~�I�)� :Əˀ3 Dn�B@Qf^IjH2���b���� ��!�      �      x������ � �      �   �   x�u��� E��W��O�jt!QI���$"
~�Հ��&,ȹ�3Cka����2�N�Θ�is�AU׽�Y	5^�lG��V*w
/dH��p���B�B�/����!�m}��?�*�1�i��7\e(�S#��,v�or/�H9�Z�?b�X��9ݫP)���"�vFy*mk�      �     x���MO�0��ί��
;�J�` U,�pqU7�6$($B���vR�WBb�Ȳg<����E�7���� @�����"h�nj��E�HĂRtՖ���Uc�x��-Ѳ4Mc�������F�;�h�oA�H�R�R@ܯ�_m�Я�A�ڔ�=|g��?�F۞;|� �}~~�p�k?^���a}ڧm��G�q���H����<,�5��$ɦ\RW��}�;�7Om�m�'#�����M�=��*�Zo�,�u��g^z���?ڔ���վ���i��q����,7ED��x���la����;�Lav��Wm�$�W�&��9|��!���\P��L{:K�Z�~����f&(zm�9H����#�r
�8`{�e�2�9�L�y8Q�HjyH�L�R��NS$�`�&	H���ӳ�K5"���a��ȉ��H��#��	߾��^ߑ?�v���
�#�ƅ�I����	�N���(��]_a      �   |   x���;�@���>=
{�ɮ�@����X�<
���FAB�z����v��5�~�|�Y%�f��,Y�o9
Fl/�~�����fy���m�I���ed�)d�ʙ�D�K9����BNI��1�hZc�      �   �   x���1�0��NѽJ�;�9B���?GH)������@��GR�X ��C��Sڟ̨�<\hy�ҟ���W�ٕ=XͳlM3�`�lޛW�agfE��gBe>��Ɲ��v��ۿ�v�3o��q�      �   �   x���1�0��NѽJ�;�9B׮��9�HU�t`�� ��SR�� ����C��]ڟ�Ѐ����,�?�ռ�
��Q�<��y�&�d�y�vfF�l�٘Be]���U��;~f�?��f� �r�      �   p   x�uͱ
�0�Y�����Y�,�s [� �	Y;���@%����wB��W߷NjA�օG��i��H��5� �WOQ����9S��A�>wٚJS��?�ݪ����O\�#�      �   �   x�u�=�0����W�ũ�&���n��PDpv���ؚH��{���z��>���J"]hаL�b��7�<n}�aw2m�g X۫Ix4���0Zs��n�}�SĬ�q8@ q1Ϲ�K�bV���ʸ�3�r`h5��h�?d�6!&�Q�o��L�K^��X�w����U%�������~�{бBR      �   �  x����n�0E��Wh_���!>vA��@Ѣ@��ȱ�u����CZ��.�dq3�=/������?�/����|>��B05����c��ʤ � �A�F� !h�Aؚ��ݱoݡ{�<�o�2,��=�{-.ނA���m�?W.&ď+�3���Ks͌��Bs7/����Ja�\���֝���Ȟ�����|PX�*2���:O0����t%Ű	`���\���>4��3�P�|��**Q������+�� 5�?�u���ѿ�j:��L�`�����Q&fa��g��T��T֯���<�/h^�/��J,�<wcS��4���9�d7$э�D����˷&;b���]툜&b�&hE�����RNL'��t�
�ɦ�ޑv�T�<Sl�]���j�B�K�ET��̰+;��s����v�� �Q����z�ǩ�\�2��kSa�"2k�s�b?�p}��RKN�,�����}����Z~���1�n�+��Z�K;�w���Se�q���.OdWA"�����4$��#�1�Z��"���M�@:=�-|�� ����;��%	�{w#Y�ŭ��+�a��$�$�w�/PC���]6ެ���$�+z��ыY���������ܟ��&�ȄL�geJ��W��
Ho7�.�����*�ǃ蒽��_`�O��	��aW��Z�>�|�A �?��� �      �   �   x�%�I�0  ������DBm�"��!\�&��|�[2��đ�����[M(���"����K"?�)�p�^>�'Kt���e$Űk�U�-�:U�3	vZ 3�x(.g��R�˰ꫫ��q��=��0��S��~��l;v��&E�拗8��?��@$  ޙ�4�     