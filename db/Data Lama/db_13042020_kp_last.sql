PGDMP         -                x            kharisma_printex    11.5    11.5    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    33041    kharisma_printex    DATABASE     �   CREATE DATABASE kharisma_printex WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
     DROP DATABASE kharisma_printex;
             postgres    false            �            1259    33042 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false            �            1259    33045    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    196            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    197            �            1259    33047 	   mst_do_sj    TABLE       CREATE TABLE public.mst_do_sj (
    i_id integer NOT NULL,
    i_id_kartu integer,
    i_id_rfp integer,
    i_no_sj character varying(16) NOT NULL,
    d_sj date,
    i_pel integer,
    d_due_date date,
    n_total_cw integer,
    e_ket text,
    n_tot_roll numeric,
    n_tot_asal_sj numeric,
    n_tot_asal_kp numeric,
    n_tot_jadi_kp numeric,
    n_total_tagihan numeric,
    n_sisa_tagihan numeric,
    f_print boolean DEFAULT false,
    f_lunas boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    tgl_produksi date,
    expedisi character varying(25),
    i_status integer,
    alasan_reject character varying(200),
    d_approved timestamp without time zone,
    f_nota boolean DEFAULT false,
    i_id_so integer
);
    DROP TABLE public.mst_do_sj;
       public         postgres    false            �            1259    33056    mst_invoice    TABLE     �  CREATE TABLE public.mst_invoice (
    i_invoice integer NOT NULL,
    i_invoice_code character varying(20) NOT NULL,
    i_pel integer,
    d_invoice date,
    v_total_invoice numeric DEFAULT 0,
    v_total_invoice_sisa numeric DEFAULT 0,
    f_lunas boolean DEFAULT false,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.mst_invoice;
       public         postgres    false            �            1259    33066    mst_invoice_item    TABLE     v  CREATE TABLE public.mst_invoice_item (
    i_invoice_item integer NOT NULL,
    i_invoice integer NOT NULL,
    i_nota integer NOT NULL,
    v_qty_nota numeric DEFAULT 0,
    v_nilai_nota numeric DEFAULT 0,
    f_lunas boolean DEFAULT false,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 $   DROP TABLE public.mst_invoice_item;
       public         postgres    false            �            1259    33076 #   mst_invoice_item_i_invoice_item_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_invoice_item_i_invoice_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.mst_invoice_item_i_invoice_item_seq;
       public       postgres    false    200            �           0    0 #   mst_invoice_item_i_invoice_item_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.mst_invoice_item_i_invoice_item_seq OWNED BY public.mst_invoice_item.i_invoice_item;
            public       postgres    false    201            �            1259    33078 	   mst_kartu    TABLE     6  CREATE TABLE public.mst_kartu (
    i_id integer NOT NULL,
    i_no_kartu character varying(25) NOT NULL,
    d_pengiriman date,
    i_id_so integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_lebar_blanket character varying(75),
    e_handfeel character varying(75),
    d_beres timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_ket text,
    i_status integer,
    tot_roll numeric,
    tot_kg numeric,
    tot_pjg numeric,
    e_lebar_kain_jadi numeric,
    tgl_produksi date
);
    DROP TABLE public.mst_kartu;
       public         postgres    false            �            1259    33084    mst_kartu_cw    TABLE     �  CREATE TABLE public.mst_kartu_cw (
    i_id integer NOT NULL,
    i_id_kartu integer NOT NULL,
    e_cw character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0,
    d_strike_off date,
    e_jenis_kain character varying(50)
);
     DROP TABLE public.mst_kartu_cw;
       public         postgres    false            �            1259    33091    mst_menu_role    TABLE       CREATE TABLE public.mst_menu_role (
    i_id integer NOT NULL,
    role_id integer NOT NULL,
    id_menu_head integer NOT NULL,
    id_menu_item integer NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.mst_menu_role;
       public         postgres    false            �            1259    33094    mst_menu_role_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_menu_role_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.mst_menu_role_i_id_seq;
       public       postgres    false    204            �           0    0    mst_menu_role_i_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.mst_menu_role_i_id_seq OWNED BY public.mst_menu_role.i_id;
            public       postgres    false    205            �            1259    33096    mst_nota    TABLE     �  CREATE TABLE public.mst_nota (
    i_nota integer NOT NULL,
    i_nota_code character varying(20) NOT NULL,
    i_pel integer,
    d_nota date,
    d_due_date date,
    n_diskon real DEFAULT 0,
    v_diskon numeric DEFAULT 0,
    n_tot_qty numeric DEFAULT 0,
    v_total_nota numeric DEFAULT 0,
    v_total_nppn numeric DEFAULT 0,
    v_total_nppn_sisa numeric DEFAULT 0,
    f_invoice boolean DEFAULT false,
    f_lunas boolean DEFAULT false,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    n_ppn real DEFAULT 0,
    v_ppn numeric DEFAULT 0,
    i_qty_from integer
);
    DROP TABLE public.mst_nota;
       public         postgres    false            �            1259    33113    mst_nota_item    TABLE     .  CREATE TABLE public.mst_nota_item (
    i_nota_item integer NOT NULL,
    i_nota integer NOT NULL,
    i_sj integer,
    n_qty numeric DEFAULT 0,
    v_hrg_satuan numeric DEFAULT 0,
    i_qty_from integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.mst_nota_item;
       public         postgres    false            �            1259    33121    mst_nota_item_i_nota_item_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_nota_item_i_nota_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.mst_nota_item_i_nota_item_seq;
       public       postgres    false    207            �           0    0    mst_nota_item_i_nota_item_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.mst_nota_item_i_nota_item_seq OWNED BY public.mst_nota_item.i_nota_item;
            public       postgres    false    208            �            1259    33123    mst_packing_list    TABLE     �  CREATE TABLE public.mst_packing_list (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    i_cw integer,
    e_kode character varying(6),
    n_asal_sj numeric DEFAULT 0,
    n_asal_kp numeric DEFAULT 0,
    n_jadi_kp numeric DEFAULT 0,
    f_jadi_sj boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_kartu integer,
    i_no_packing character varying(40),
    i_id_sj integer
);
 $   DROP TABLE public.mst_packing_list;
       public         postgres    false            �            1259    33133    mst_packing_list_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_packing_list_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.mst_packing_list_i_id_seq;
       public       postgres    false    209            �           0    0    mst_packing_list_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.mst_packing_list_i_id_seq OWNED BY public.mst_packing_list.i_id;
            public       postgres    false    210            �            1259    33135    mst_pelunasan    TABLE     w  CREATE TABLE public.mst_pelunasan (
    i_pelunasan integer NOT NULL,
    i_pelunasan_code character varying(20) NOT NULL,
    i_pel integer,
    d_pelunasan date,
    v_pelunasan numeric DEFAULT 0,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    keterangan text,
    i_invoice integer
);
 !   DROP TABLE public.mst_pelunasan;
       public         postgres    false            �            1259    33143    mst_pelunasan_item    TABLE     E  CREATE TABLE public.mst_pelunasan_item (
    i_pelunasan_item integer NOT NULL,
    i_pelunasan integer NOT NULL,
    i_invoice integer,
    i_nota integer,
    v_bayar_nota numeric DEFAULT 0,
    f_cancel boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 &   DROP TABLE public.mst_pelunasan_item;
       public         postgres    false            �            1259    33151 '   mst_pelunasan_item_i_pelunasan_item_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_pelunasan_item_i_pelunasan_item_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.mst_pelunasan_item_i_pelunasan_item_seq;
       public       postgres    false    212            �           0    0 '   mst_pelunasan_item_i_pelunasan_item_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.mst_pelunasan_item_i_pelunasan_item_seq OWNED BY public.mst_pelunasan_item.i_pelunasan_item;
            public       postgres    false    213            �            1259    33153    mst_sales_plan    TABLE     �   CREATE TABLE public.mst_sales_plan (
    i_id integer NOT NULL,
    n_qty numeric DEFAULT 0,
    bulan character varying(2),
    tahun character varying(5),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 "   DROP TABLE public.mst_sales_plan;
       public         postgres    false            �            1259    33160    mst_sales_plan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_sales_plan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.mst_sales_plan_i_id_seq;
       public       postgres    false    214            �           0    0    mst_sales_plan_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.mst_sales_plan_i_id_seq OWNED BY public.mst_sales_plan.i_id;
            public       postgres    false    215            �            1259    33162    mst_so    TABLE     �  CREATE TABLE public.mst_so (
    i_id integer NOT NULL,
    i_no_so character varying(16) NOT NULL,
    d_so date,
    i_pel integer,
    f_repeat boolean,
    i_desain character varying(16),
    n_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_color_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    d_strike_off date,
    d_approval_strike_off date,
    d_penyerahan_brg date,
    e_keterangan_kirim text,
    i_status integer,
    d_approved timestamp(6) without time zone,
    v_pekerjaan numeric,
    v_pekerjaan_plus_ppn numeric,
    v_discount numeric,
    n_discount real,
    v_sisa numeric,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_reject character varying(200),
    i_no_po character varying(25),
    flag_so integer,
    hitung_by integer,
    n_ppn real,
    v_ppn numeric,
    exclude_include character varying(1),
    cara_bayar integer
);
    DROP TABLE public.mst_so;
       public         postgres    false            �           0    0    TABLE mst_so    COMMENT     �   COMMENT ON TABLE public.mst_so IS '- Hitung By: 1 -> KG, 2 -> Roll, 3 -> Yard/Meter
- Exclude_Include: E adalah Exclude, I adalah Include
- cara_bayar:
1 -> Cash
2 -> Transfer
3 -> Lain Lain';
            public       postgres    false    216            �            1259    33168    mst_so_item    TABLE     �  CREATE TABLE public.mst_so_item (
    i_id integer NOT NULL,
    i_id_so integer NOT NULL,
    e_uraian_pekerjaan character varying(100),
    n_qty_roll numeric,
    n_qty_pjg numeric,
    n_qty_kg numeric,
    v_harga_sat numeric,
    created_at timestamp(6) with time zone,
    updated_at timestamp(6) with time zone,
    n_qty_kg_sisa numeric,
    i_id_bagian integer DEFAULT 0,
    d_strike_off date,
    e_jenis_kain character varying(50)
);
    DROP TABLE public.mst_so_item;
       public         postgres    false            �            1259    33175    mst_so_item_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.mst_so_item_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.mst_so_item_i_id_seq;
       public       postgres    false    217            �           0    0    mst_so_item_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.mst_so_item_i_id_seq OWNED BY public.mst_so_item.i_id;
            public       postgres    false    218            �            1259    33177    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false            �            1259    33183 
   ref_bagian    TABLE     +  CREATE TABLE public.ref_bagian (
    i_id integer NOT NULL,
    nama_bagian character varying(50) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    last_workstation boolean,
    e_penanggung_jawab integer,
    e_penanggung_jawab2 integer
);
    DROP TABLE public.ref_bagian;
       public         postgres    false            �            1259    33186    ref_jns_bahan    TABLE     �   CREATE TABLE public.ref_jns_bahan (
    i_id integer NOT NULL,
    e_jns_bahan character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.ref_jns_bahan;
       public         postgres    false            �            1259    33189    ref_jns_bahan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_jns_bahan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.ref_jns_bahan_i_id_seq;
       public       postgres    false    221            �           0    0    ref_jns_bahan_i_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.ref_jns_bahan_i_id_seq OWNED BY public.ref_jns_bahan.i_id;
            public       postgres    false    222            �            1259    33191    ref_jns_printing    TABLE     �   CREATE TABLE public.ref_jns_printing (
    i_id integer NOT NULL,
    e_jns_printing character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 $   DROP TABLE public.ref_jns_printing;
       public         postgres    false            �            1259    33194    ref_jns_printing_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_jns_printing_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.ref_jns_printing_i_id_seq;
       public       postgres    false    223            �           0    0    ref_jns_printing_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.ref_jns_printing_i_id_seq OWNED BY public.ref_jns_printing.i_id;
            public       postgres    false    224            �            1259    33196    ref_kain    TABLE     �   CREATE TABLE public.ref_kain (
    i_id integer NOT NULL,
    e_kain character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_kain;
       public         postgres    false            �            1259    33199    ref_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_kain_i_id_seq;
       public       postgres    false    225                        0    0    ref_kain_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_kain_i_id_seq OWNED BY public.ref_kain.i_id;
            public       postgres    false    226            �            1259    33201    ref_ket_umum_so    TABLE     �   CREATE TABLE public.ref_ket_umum_so (
    i_id integer NOT NULL,
    e_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_ket_umum_so;
       public         postgres    false            �            1259    33204    ref_ket_umum_so_child    TABLE       CREATE TABLE public.ref_ket_umum_so_child (
    i_id integer NOT NULL,
    i_id_ket_umum integer NOT NULL,
    e_child_ket_umum character varying(250) NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 )   DROP TABLE public.ref_ket_umum_so_child;
       public         postgres    false            �            1259    33207    ref_ket_umum_so_child_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_ket_umum_so_child_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.ref_ket_umum_so_child_i_id_seq;
       public       postgres    false    228                       0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.ref_ket_umum_so_child_i_id_seq OWNED BY public.ref_ket_umum_so_child.i_id;
            public       postgres    false    229            �            1259    33209    ref_kondisi_kain    TABLE     �   CREATE TABLE public.ref_kondisi_kain (
    i_id integer NOT NULL,
    e_kondisi_kain character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 $   DROP TABLE public.ref_kondisi_kain;
       public         postgres    false            �            1259    33212    ref_kondisi_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_kondisi_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.ref_kondisi_kain_i_id_seq;
       public       postgres    false    230                       0    0    ref_kondisi_kain_i_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.ref_kondisi_kain_i_id_seq OWNED BY public.ref_kondisi_kain.i_id;
            public       postgres    false    231            �            1259    33214    ref_logo    TABLE     �   CREATE TABLE public.ref_logo (
    i_id integer NOT NULL,
    logo_name character varying(100),
    f_active boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_logo;
       public         postgres    false            �            1259    33218    ref_logo_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_logo_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_logo_i_id_seq;
       public       postgres    false    232                       0    0    ref_logo_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_logo_i_id_seq OWNED BY public.ref_logo.i_id;
            public       postgres    false    233            �            1259    33220    ref_menu_head    TABLE     @  CREATE TABLE public.ref_menu_head (
    i_id integer NOT NULL,
    nama_menu character varying(150) NOT NULL,
    icon_menu character varying(100),
    rut_name character varying(150),
    stand_alone boolean DEFAULT false,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.ref_menu_head;
       public         postgres    false            �            1259    33224    ref_menu_item    TABLE     D  CREATE TABLE public.ref_menu_item (
    i_id integer NOT NULL,
    id_menu_head integer NOT NULL,
    nama_sub_menu character varying(150) NOT NULL,
    icon_sub_menu character varying(100),
    rut_name character varying(150),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 !   DROP TABLE public.ref_menu_item;
       public         postgres    false            �            1259    33227    ref_no_urut    TABLE     �   CREATE TABLE public.ref_no_urut (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    no_urut character varying(11),
    bln character varying(2),
    thn character varying(5),
    flag_so integer
);
    DROP TABLE public.ref_no_urut;
       public         postgres    false            �            1259    33230    ref_no_urut_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_no_urut_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.ref_no_urut_id_seq;
       public       postgres    false    236                       0    0    ref_no_urut_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.ref_no_urut_id_seq OWNED BY public.ref_no_urut.id;
            public       postgres    false    237            �            1259    33232    ref_ori_kondisi    TABLE     �   CREATE TABLE public.ref_ori_kondisi (
    i_id integer NOT NULL,
    e_ori_kondisi character varying(100),
    tipe integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_ori_kondisi;
       public         postgres    false            �            1259    33235    ref_ori_kondisi_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_ori_kondisi_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_ori_kondisi_i_id_seq;
       public       postgres    false    238                       0    0    ref_ori_kondisi_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.ref_ori_kondisi_i_id_seq OWNED BY public.ref_ori_kondisi.i_id;
            public       postgres    false    239            �            1259    33237    ref_pelanggan    TABLE       CREATE TABLE public.ref_pelanggan (
    i_pel integer NOT NULL,
    e_nama_pel character varying(255) NOT NULL,
    e_alamat_pel character varying(255),
    f_pkp boolean DEFAULT false,
    e_npwp_pel character varying(30),
    e_telp_pel character varying(30),
    e_kont_pel character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_fax_pel character varying(30),
    e_kota_pel character varying(60),
    e_kode_marketing character varying(16),
    n_jth_tempo integer
);
 !   DROP TABLE public.ref_pelanggan;
       public         postgres    false            �            1259    33244    ref_pelanggan_i_pel_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_pelanggan_i_pel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.ref_pelanggan_i_pel_seq;
       public       postgres    false    240                       0    0    ref_pelanggan_i_pel_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.ref_pelanggan_i_pel_seq OWNED BY public.ref_pelanggan.i_pel;
            public       postgres    false    241            �            1259    33246    ref_role    TABLE     �   CREATE TABLE public.ref_role (
    i_id integer NOT NULL,
    e_role_name character varying(25) NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.ref_role;
       public         postgres    false            �            1259    33249    ref_role_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_role_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.ref_role_i_id_seq;
       public       postgres    false    242                       0    0    ref_role_i_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.ref_role_i_id_seq OWNED BY public.ref_role.i_id;
            public       postgres    false    243            �            1259    33251    ref_sat_qty    TABLE     �   CREATE TABLE public.ref_sat_qty (
    i_id integer NOT NULL,
    e_sat character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_sat_qty;
       public         postgres    false            �            1259    33254    ref_sat_qty_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_sat_qty_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_sat_qty_i_id_seq;
       public       postgres    false    244                       0    0    ref_sat_qty_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_sat_qty_i_id_seq OWNED BY public.ref_sat_qty.i_id;
            public       postgres    false    245            �            1259    33256    ref_so_flag    TABLE     �   CREATE TABLE public.ref_so_flag (
    i_id integer NOT NULL,
    flag_name character varying(100),
    f_active boolean DEFAULT true,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.ref_so_flag;
       public         postgres    false            �            1259    33260    ref_so_flag_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_so_flag_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ref_so_flag_i_id_seq;
       public       postgres    false    246            	           0    0    ref_so_flag_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.ref_so_flag_i_id_seq OWNED BY public.ref_so_flag.i_id;
            public       postgres    false    247            �            1259    33262    ref_tekstur_akhir    TABLE     �   CREATE TABLE public.ref_tekstur_akhir (
    i_id integer NOT NULL,
    e_tekstur character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 %   DROP TABLE public.ref_tekstur_akhir;
       public         postgres    false            �            1259    33265    ref_tekstur_akhir_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_tekstur_akhir_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.ref_tekstur_akhir_i_id_seq;
       public       postgres    false    248            
           0    0    ref_tekstur_akhir_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.ref_tekstur_akhir_i_id_seq OWNED BY public.ref_tekstur_akhir.i_id;
            public       postgres    false    249            �            1259    33267    ref_warna_dasar    TABLE     �   CREATE TABLE public.ref_warna_dasar (
    i_id integer NOT NULL,
    e_warna_dasar character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.ref_warna_dasar;
       public         postgres    false            �            1259    33270    ref_warna_dasar_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_warna_dasar_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_warna_dasar_i_id_seq;
       public       postgres    false    250                       0    0    ref_warna_dasar_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.ref_warna_dasar_i_id_seq OWNED BY public.ref_warna_dasar.i_id;
            public       postgres    false    251            �            1259    33272    ref_workflow    TABLE     �   CREATE TABLE public.ref_workflow (
    i_id integer NOT NULL,
    definition character varying(100),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
     DROP TABLE public.ref_workflow;
       public         postgres    false            �            1259    33275    ref_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ref_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ref_workstation_i_id_seq;
       public       postgres    false    220                       0    0    ref_workstation_i_id_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public.ref_workstation_i_id_seq OWNED BY public.ref_bagian.i_id;
            public       postgres    false    253            �            1259    33277    rfp_urutan_fifo_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_urutan_fifo_seq
    START WITH 51
    INCREMENT BY 1
    MINVALUE 51
    MAXVALUE 2147483647
    CACHE 1;
 *   DROP SEQUENCE public.rfp_urutan_fifo_seq;
       public       postgres    false            �            1259    33279    rfp    TABLE     1  CREATE TABLE public.rfp (
    i_id integer NOT NULL,
    i_no_rfp character varying(20) NOT NULL,
    d_selesai date,
    i_pel integer,
    e_contact_person character varying(50),
    f_repeat boolean,
    i_desain character varying(16),
    e_motif character varying(50),
    e_material character varying(50),
    e_original_cond1 character varying(25),
    e_ket_ori_cond character varying(50),
    e_color character varying(50),
    i_penyedia integer,
    d_tgl_material_in date,
    n_qty_material numeric,
    n_pengkerutan numeric,
    e_gramasi_from character varying(50),
    e_gramasi_to character varying(50),
    e_penyesuaian_lebar_from character varying(50),
    e_penyesuaian_lebar_to character varying(50),
    e_pakan_from character varying(50),
    e_pakan_to character varying(50),
    e_lusi_from character varying(50),
    e_lusi_to character varying(50),
    e_tekstur_akhir_from character varying(50),
    e_tekstur_akhir_to character varying(50),
    e_cw_1 character varying(50),
    e_cw_2 character varying(50),
    e_cw_3 character varying(50),
    e_cw_4 character varying(50),
    e_cw_5 character varying(50),
    e_cw_6 character varying(50),
    e_cw_7 character varying(50),
    e_cw_8 character varying(50),
    i_status integer,
    d_approved_mrk timestamp(6) without time zone,
    d_approved_pro timestamp(6) without time zone,
    d_approved_ppc timestamp(6) without time zone,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_id_so integer,
    e_reject_mkt character varying(200),
    e_reject_prod character varying(200),
    e_original_cond2 character varying(25),
    e_jenis_printing character varying(25),
    i_jns_kain integer,
    urutan_fifo integer DEFAULT nextval('public.rfp_urutan_fifo_seq'::regclass) NOT NULL,
    f_proses boolean DEFAULT false,
    old_urutan_fifo integer,
    d_proses timestamp with time zone,
    e_ket_rfp character varying(250),
    e_material_others character varying(50),
    e_color_others character varying(50),
    alasan_pindah_slot character varying(200)
);
    DROP TABLE public.rfp;
       public         postgres    false    254                        1259    33287    rfp_lpk    TABLE     �   CREATE TABLE public.rfp_lpk (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_nomor_lpk character varying(150),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    qty_roll numeric
);
    DROP TABLE public.rfp_lpk;
       public         postgres    false                       1259    33293    rfp_lpk_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rfp_lpk_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.rfp_lpk_i_id_seq;
       public       postgres    false    256                       0    0    rfp_lpk_i_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.rfp_lpk_i_id_seq OWNED BY public.rfp_lpk.i_id;
            public       postgres    false    257                       1259    33295    tx_jns_proses_kartu    TABLE     �   CREATE TABLE public.tx_jns_proses_kartu (
    i_id integer NOT NULL,
    description character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 '   DROP TABLE public.tx_jns_proses_kartu;
       public         postgres    false                       1259    33298    tx_jns_proses_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_jns_proses_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.tx_jns_proses_kartu_i_id_seq;
       public       postgres    false    258                       0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.tx_jns_proses_kartu_i_id_seq OWNED BY public.tx_jns_proses_kartu.i_id;
            public       postgres    false    259                       1259    33300    tx_rfp_accepted    TABLE       CREATE TABLE public.tx_rfp_accepted (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    d_accepted timestamp(4) without time zone NOT NULL,
    user_accept integer NOT NULL,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 #   DROP TABLE public.tx_rfp_accepted;
       public         postgres    false                       1259    33303    tx_rfp_accepted_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_rfp_accepted_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.tx_rfp_accepted_i_id_seq;
       public       postgres    false    260                       0    0    tx_rfp_accepted_i_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.tx_rfp_accepted_i_id_seq OWNED BY public.tx_rfp_accepted.i_id;
            public       postgres    false    261                       1259    33305    tx_sisa_uang_masuk    TABLE     �   CREATE TABLE public.tx_sisa_uang_masuk (
    i_uang_masuk integer NOT NULL,
    i_pel integer,
    v_sisa_uang_masuk numeric DEFAULT 0,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 &   DROP TABLE public.tx_sisa_uang_masuk;
       public         postgres    false                       1259    33312 #   tx_sisa_uang_masuk_i_uang_masuk_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sisa_uang_masuk_i_uang_masuk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.tx_sisa_uang_masuk_i_uang_masuk_seq;
       public       postgres    false    262                       0    0 #   tx_sisa_uang_masuk_i_uang_masuk_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.tx_sisa_uang_masuk_i_uang_masuk_seq OWNED BY public.tx_sisa_uang_masuk.i_uang_masuk;
            public       postgres    false    263                       1259    33314    tx_sj_kartu    TABLE     �   CREATE TABLE public.tx_sj_kartu (
    i_id integer NOT NULL,
    i_no_sj character varying(50) NOT NULL,
    i_id_kartu integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
    DROP TABLE public.tx_sj_kartu;
       public         postgres    false            	           1259    33317    tx_sj_kartu_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_sj_kartu_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tx_sj_kartu_i_id_seq;
       public       postgres    false    264                       0    0    tx_sj_kartu_i_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.tx_sj_kartu_i_id_seq OWNED BY public.tx_sj_kartu.i_id;
            public       postgres    false    265            
           1259    33319    tx_spec_kain    TABLE     �  CREATE TABLE public.tx_spec_kain (
    i_id integer NOT NULL,
    i_jns_kain integer,
    e_kondisi_kain character varying(100),
    e_lebar character varying(50),
    e_satuan_lebar character varying(25),
    e_gramasi character varying(25),
    e_sat_gramasi character varying(25),
    i_penyedia integer,
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    i_desain character varying(16)
);
     DROP TABLE public.tx_spec_kain;
       public         postgres    false                       1259    33322    tx_spec_kain_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_kain_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tx_spec_kain_i_id_seq;
       public       postgres    false    266                       0    0    tx_spec_kain_i_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.tx_spec_kain_i_id_seq OWNED BY public.tx_spec_kain.i_id;
            public       postgres    false    267                       1259    33324    tx_spec_pekerjaan    TABLE     �  CREATE TABLE public.tx_spec_pekerjaan (
    i_id integer NOT NULL,
    i_desain character varying(16) NOT NULL,
    i_qty_warna integer,
    e_motif character varying(50),
    e_jenis_printing character varying(25),
    n_colow_way integer,
    e_color_way character varying(100),
    e_toleransi_cacat character varying(250),
    i_pel integer,
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone
);
 %   DROP TABLE public.tx_spec_pekerjaan;
       public         postgres    false                       1259    33327    tx_spec_pekerjaan_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_spec_pekerjaan_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.tx_spec_pekerjaan_i_id_seq;
       public       postgres    false    268                       0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.tx_spec_pekerjaan_i_id_seq OWNED BY public.tx_spec_pekerjaan.i_id;
            public       postgres    false    269                       1259    33329    tx_workstation    TABLE       CREATE TABLE public.tx_workstation (
    i_id integer NOT NULL,
    i_id_rfp integer NOT NULL,
    e_cw character varying(100) NOT NULL,
    i_id_bagian integer NOT NULL,
    n_tot_qty numeric,
    n_proses numeric,
    n_sisa numeric,
    e_ket character varying(250),
    created_at timestamp(6) without time zone,
    updated_at timestamp(6) without time zone,
    e_pelaksana character varying(75),
    e_shift character varying(50),
    d_tgl date,
    i_id_kartu integer NOT NULL,
    i_cw integer,
    d_time character varying(10)
);
 "   DROP TABLE public.tx_workstation;
       public         postgres    false                       1259    33335    tx_workstation_i_id_seq    SEQUENCE     �   CREATE SEQUENCE public.tx_workstation_i_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.tx_workstation_i_id_seq;
       public       postgres    false    270                       0    0    tx_workstation_i_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.tx_workstation_i_id_seq OWNED BY public.tx_workstation.i_id;
            public       postgres    false    271                       1259    33337    users    TABLE     A  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    username character varying(255) NOT NULL,
    role integer,
    login_pertama boolean DEFAULT true,
    is_active boolean DEFAULT true,
    count_reset_password integer,
    status character varying(255)
);
    DROP TABLE public.users;
       public         postgres    false                       1259    33345    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 15
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    272                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    273            �           2604    33347    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196            �           2604    33348    mst_invoice_item i_invoice_item    DEFAULT     �   ALTER TABLE ONLY public.mst_invoice_item ALTER COLUMN i_invoice_item SET DEFAULT nextval('public.mst_invoice_item_i_invoice_item_seq'::regclass);
 N   ALTER TABLE public.mst_invoice_item ALTER COLUMN i_invoice_item DROP DEFAULT;
       public       postgres    false    201    200            �           2604    33349    mst_menu_role i_id    DEFAULT     x   ALTER TABLE ONLY public.mst_menu_role ALTER COLUMN i_id SET DEFAULT nextval('public.mst_menu_role_i_id_seq'::regclass);
 A   ALTER TABLE public.mst_menu_role ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    205    204            �           2604    33350    mst_nota_item i_nota_item    DEFAULT     �   ALTER TABLE ONLY public.mst_nota_item ALTER COLUMN i_nota_item SET DEFAULT nextval('public.mst_nota_item_i_nota_item_seq'::regclass);
 H   ALTER TABLE public.mst_nota_item ALTER COLUMN i_nota_item DROP DEFAULT;
       public       postgres    false    208    207            �           2604    33351    mst_packing_list i_id    DEFAULT     ~   ALTER TABLE ONLY public.mst_packing_list ALTER COLUMN i_id SET DEFAULT nextval('public.mst_packing_list_i_id_seq'::regclass);
 D   ALTER TABLE public.mst_packing_list ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    210    209            �           2604    33352 #   mst_pelunasan_item i_pelunasan_item    DEFAULT     �   ALTER TABLE ONLY public.mst_pelunasan_item ALTER COLUMN i_pelunasan_item SET DEFAULT nextval('public.mst_pelunasan_item_i_pelunasan_item_seq'::regclass);
 R   ALTER TABLE public.mst_pelunasan_item ALTER COLUMN i_pelunasan_item DROP DEFAULT;
       public       postgres    false    213    212            �           2604    33353    mst_sales_plan i_id    DEFAULT     z   ALTER TABLE ONLY public.mst_sales_plan ALTER COLUMN i_id SET DEFAULT nextval('public.mst_sales_plan_i_id_seq'::regclass);
 B   ALTER TABLE public.mst_sales_plan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    215    214            �           2604    33354    mst_so_item i_id    DEFAULT     t   ALTER TABLE ONLY public.mst_so_item ALTER COLUMN i_id SET DEFAULT nextval('public.mst_so_item_i_id_seq'::regclass);
 ?   ALTER TABLE public.mst_so_item ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    218    217            �           2604    33355    ref_bagian i_id    DEFAULT     w   ALTER TABLE ONLY public.ref_bagian ALTER COLUMN i_id SET DEFAULT nextval('public.ref_workstation_i_id_seq'::regclass);
 >   ALTER TABLE public.ref_bagian ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    253    220            �           2604    33356    ref_jns_bahan i_id    DEFAULT     x   ALTER TABLE ONLY public.ref_jns_bahan ALTER COLUMN i_id SET DEFAULT nextval('public.ref_jns_bahan_i_id_seq'::regclass);
 A   ALTER TABLE public.ref_jns_bahan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    222    221            �           2604    33357    ref_jns_printing i_id    DEFAULT     ~   ALTER TABLE ONLY public.ref_jns_printing ALTER COLUMN i_id SET DEFAULT nextval('public.ref_jns_printing_i_id_seq'::regclass);
 D   ALTER TABLE public.ref_jns_printing ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    224    223            �           2604    33358    ref_kain i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kain_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    226    225            �           2604    33359    ref_ket_umum_so_child i_id    DEFAULT     �   ALTER TABLE ONLY public.ref_ket_umum_so_child ALTER COLUMN i_id SET DEFAULT nextval('public.ref_ket_umum_so_child_i_id_seq'::regclass);
 I   ALTER TABLE public.ref_ket_umum_so_child ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    229    228            �           2604    33360    ref_kondisi_kain i_id    DEFAULT     ~   ALTER TABLE ONLY public.ref_kondisi_kain ALTER COLUMN i_id SET DEFAULT nextval('public.ref_kondisi_kain_i_id_seq'::regclass);
 D   ALTER TABLE public.ref_kondisi_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    231    230            �           2604    33361    ref_logo i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_logo ALTER COLUMN i_id SET DEFAULT nextval('public.ref_logo_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_logo ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    233    232            �           2604    33362    ref_no_urut id    DEFAULT     p   ALTER TABLE ONLY public.ref_no_urut ALTER COLUMN id SET DEFAULT nextval('public.ref_no_urut_id_seq'::regclass);
 =   ALTER TABLE public.ref_no_urut ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    237    236            �           2604    33363    ref_ori_kondisi i_id    DEFAULT     |   ALTER TABLE ONLY public.ref_ori_kondisi ALTER COLUMN i_id SET DEFAULT nextval('public.ref_ori_kondisi_i_id_seq'::regclass);
 C   ALTER TABLE public.ref_ori_kondisi ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    239    238            �           2604    33364    ref_pelanggan i_pel    DEFAULT     z   ALTER TABLE ONLY public.ref_pelanggan ALTER COLUMN i_pel SET DEFAULT nextval('public.ref_pelanggan_i_pel_seq'::regclass);
 B   ALTER TABLE public.ref_pelanggan ALTER COLUMN i_pel DROP DEFAULT;
       public       postgres    false    241    240            �           2604    33365    ref_role i_id    DEFAULT     n   ALTER TABLE ONLY public.ref_role ALTER COLUMN i_id SET DEFAULT nextval('public.ref_role_i_id_seq'::regclass);
 <   ALTER TABLE public.ref_role ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    243    242            �           2604    33366    ref_sat_qty i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_sat_qty ALTER COLUMN i_id SET DEFAULT nextval('public.ref_sat_qty_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_sat_qty ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    245    244            �           2604    33367    ref_so_flag i_id    DEFAULT     t   ALTER TABLE ONLY public.ref_so_flag ALTER COLUMN i_id SET DEFAULT nextval('public.ref_so_flag_i_id_seq'::regclass);
 ?   ALTER TABLE public.ref_so_flag ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    247    246            �           2604    33368    ref_tekstur_akhir i_id    DEFAULT     �   ALTER TABLE ONLY public.ref_tekstur_akhir ALTER COLUMN i_id SET DEFAULT nextval('public.ref_tekstur_akhir_i_id_seq'::regclass);
 E   ALTER TABLE public.ref_tekstur_akhir ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    249    248            �           2604    33369    ref_warna_dasar i_id    DEFAULT     |   ALTER TABLE ONLY public.ref_warna_dasar ALTER COLUMN i_id SET DEFAULT nextval('public.ref_warna_dasar_i_id_seq'::regclass);
 C   ALTER TABLE public.ref_warna_dasar ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    251    250            �           2604    33370    rfp_lpk i_id    DEFAULT     l   ALTER TABLE ONLY public.rfp_lpk ALTER COLUMN i_id SET DEFAULT nextval('public.rfp_lpk_i_id_seq'::regclass);
 ;   ALTER TABLE public.rfp_lpk ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    257    256            �           2604    33371    tx_jns_proses_kartu i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_jns_proses_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_jns_proses_kartu_i_id_seq'::regclass);
 G   ALTER TABLE public.tx_jns_proses_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    259    258            �           2604    33372    tx_rfp_accepted i_id    DEFAULT     |   ALTER TABLE ONLY public.tx_rfp_accepted ALTER COLUMN i_id SET DEFAULT nextval('public.tx_rfp_accepted_i_id_seq'::regclass);
 C   ALTER TABLE public.tx_rfp_accepted ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    261    260            �           2604    33373    tx_sisa_uang_masuk i_uang_masuk    DEFAULT     �   ALTER TABLE ONLY public.tx_sisa_uang_masuk ALTER COLUMN i_uang_masuk SET DEFAULT nextval('public.tx_sisa_uang_masuk_i_uang_masuk_seq'::regclass);
 N   ALTER TABLE public.tx_sisa_uang_masuk ALTER COLUMN i_uang_masuk DROP DEFAULT;
       public       postgres    false    263    262            �           2604    33374    tx_sj_kartu i_id    DEFAULT     t   ALTER TABLE ONLY public.tx_sj_kartu ALTER COLUMN i_id SET DEFAULT nextval('public.tx_sj_kartu_i_id_seq'::regclass);
 ?   ALTER TABLE public.tx_sj_kartu ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    265    264            �           2604    33375    tx_spec_kain i_id    DEFAULT     v   ALTER TABLE ONLY public.tx_spec_kain ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_kain_i_id_seq'::regclass);
 @   ALTER TABLE public.tx_spec_kain ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    267    266            �           2604    33376    tx_spec_pekerjaan i_id    DEFAULT     �   ALTER TABLE ONLY public.tx_spec_pekerjaan ALTER COLUMN i_id SET DEFAULT nextval('public.tx_spec_pekerjaan_i_id_seq'::regclass);
 E   ALTER TABLE public.tx_spec_pekerjaan ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    269    268            �           2604    33377    tx_workstation i_id    DEFAULT     z   ALTER TABLE ONLY public.tx_workstation ALTER COLUMN i_id SET DEFAULT nextval('public.tx_workstation_i_id_seq'::regclass);
 B   ALTER TABLE public.tx_workstation ALTER COLUMN i_id DROP DEFAULT;
       public       postgres    false    271    270            �           2604    33378    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    273    272            �          0    33042 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    196   �b      �          0    33047 	   mst_do_sj 
   TABLE DATA               F  COPY public.mst_do_sj (i_id, i_id_kartu, i_id_rfp, i_no_sj, d_sj, i_pel, d_due_date, n_total_cw, e_ket, n_tot_roll, n_tot_asal_sj, n_tot_asal_kp, n_tot_jadi_kp, n_total_tagihan, n_sisa_tagihan, f_print, f_lunas, created_at, updated_at, tgl_produksi, expedisi, i_status, alasan_reject, d_approved, f_nota, i_id_so) FROM stdin;
    public       postgres    false    198   �b      �          0    33056    mst_invoice 
   TABLE DATA               �   COPY public.mst_invoice (i_invoice, i_invoice_code, i_pel, d_invoice, v_total_invoice, v_total_invoice_sisa, f_lunas, f_cancel, created_at, updated_at) FROM stdin;
    public       postgres    false    199   ng      �          0    33066    mst_invoice_item 
   TABLE DATA               �   COPY public.mst_invoice_item (i_invoice_item, i_invoice, i_nota, v_qty_nota, v_nilai_nota, f_lunas, f_cancel, created_at, updated_at) FROM stdin;
    public       postgres    false    200   �g      �          0    33078 	   mst_kartu 
   TABLE DATA               �   COPY public.mst_kartu (i_id, i_no_kartu, d_pengiriman, i_id_so, i_id_rfp, e_lebar_blanket, e_handfeel, d_beres, created_at, updated_at, e_ket, i_status, tot_roll, tot_kg, tot_pjg, e_lebar_kain_jadi, tgl_produksi) FROM stdin;
    public       postgres    false    202   �g      �          0    33084    mst_kartu_cw 
   TABLE DATA               �   COPY public.mst_kartu_cw (i_id, i_id_kartu, e_cw, n_qty_roll, n_qty_pjg, n_qty_kg, created_at, updated_at, n_qty_kg_sisa, i_id_bagian, d_strike_off, e_jenis_kain) FROM stdin;
    public       postgres    false    203   �u      �          0    33091    mst_menu_role 
   TABLE DATA               j   COPY public.mst_menu_role (i_id, role_id, id_menu_head, id_menu_item, created_at, updated_at) FROM stdin;
    public       postgres    false    204   ��      �          0    33096    mst_nota 
   TABLE DATA               �   COPY public.mst_nota (i_nota, i_nota_code, i_pel, d_nota, d_due_date, n_diskon, v_diskon, n_tot_qty, v_total_nota, v_total_nppn, v_total_nppn_sisa, f_invoice, f_lunas, f_cancel, created_at, updated_at, n_ppn, v_ppn, i_qty_from) FROM stdin;
    public       postgres    false    206   ߇      �          0    33113    mst_nota_item 
   TABLE DATA               {   COPY public.mst_nota_item (i_nota_item, i_nota, i_sj, n_qty, v_hrg_satuan, i_qty_from, created_at, updated_at) FROM stdin;
    public       postgres    false    207   ��      �          0    33123    mst_packing_list 
   TABLE DATA               �   COPY public.mst_packing_list (i_id, i_id_rfp, i_cw, e_kode, n_asal_sj, n_asal_kp, n_jadi_kp, f_jadi_sj, created_at, updated_at, i_id_kartu, i_no_packing, i_id_sj) FROM stdin;
    public       postgres    false    209   �      �          0    33135    mst_pelunasan 
   TABLE DATA               �   COPY public.mst_pelunasan (i_pelunasan, i_pelunasan_code, i_pel, d_pelunasan, v_pelunasan, f_cancel, created_at, updated_at, keterangan, i_invoice) FROM stdin;
    public       postgres    false    211   ��      �          0    33143    mst_pelunasan_item 
   TABLE DATA               �   COPY public.mst_pelunasan_item (i_pelunasan_item, i_pelunasan, i_invoice, i_nota, v_bayar_nota, f_cancel, created_at, updated_at) FROM stdin;
    public       postgres    false    212   ��      �          0    33153    mst_sales_plan 
   TABLE DATA               [   COPY public.mst_sales_plan (i_id, n_qty, bulan, tahun, created_at, updated_at) FROM stdin;
    public       postgres    false    214   ��      �          0    33162    mst_so 
   TABLE DATA                 COPY public.mst_so (i_id, i_no_so, d_so, i_pel, f_repeat, i_desain, n_qty_warna, e_motif, e_jenis_printing, n_color_way, e_color_way, e_toleransi_cacat, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, d_strike_off, d_approval_strike_off, d_penyerahan_brg, e_keterangan_kirim, i_status, d_approved, v_pekerjaan, v_pekerjaan_plus_ppn, v_discount, n_discount, v_sisa, created_at, updated_at, e_reject, i_no_po, flag_so, hitung_by, n_ppn, v_ppn, exclude_include, cara_bayar) FROM stdin;
    public       postgres    false    216   ��      �          0    33168    mst_so_item 
   TABLE DATA               �   COPY public.mst_so_item (i_id, i_id_so, e_uraian_pekerjaan, n_qty_roll, n_qty_pjg, n_qty_kg, v_harga_sat, created_at, updated_at, n_qty_kg_sisa, i_id_bagian, d_strike_off, e_jenis_kain) FROM stdin;
    public       postgres    false    217   �      �          0    33177    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    219   X)      �          0    33183 
   ref_bagian 
   TABLE DATA               �   COPY public.ref_bagian (i_id, nama_bagian, created_at, updated_at, last_workstation, e_penanggung_jawab, e_penanggung_jawab2) FROM stdin;
    public       postgres    false    220   u)      �          0    33186    ref_jns_bahan 
   TABLE DATA               R   COPY public.ref_jns_bahan (i_id, e_jns_bahan, created_at, updated_at) FROM stdin;
    public       postgres    false    221   x*      �          0    33191    ref_jns_printing 
   TABLE DATA               X   COPY public.ref_jns_printing (i_id, e_jns_printing, created_at, updated_at) FROM stdin;
    public       postgres    false    223   �*      �          0    33196    ref_kain 
   TABLE DATA               H   COPY public.ref_kain (i_id, e_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    225   W+      �          0    33201    ref_ket_umum_so 
   TABLE DATA               S   COPY public.ref_ket_umum_so (i_id, e_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    227   x,      �          0    33204    ref_ket_umum_so_child 
   TABLE DATA               n   COPY public.ref_ket_umum_so_child (i_id, i_id_ket_umum, e_child_ket_umum, created_at, updated_at) FROM stdin;
    public       postgres    false    228   .      �          0    33209    ref_kondisi_kain 
   TABLE DATA               X   COPY public.ref_kondisi_kain (i_id, e_kondisi_kain, created_at, updated_at) FROM stdin;
    public       postgres    false    230   �/      �          0    33214    ref_logo 
   TABLE DATA               U   COPY public.ref_logo (i_id, logo_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    232   �/      �          0    33220    ref_menu_head 
   TABLE DATA               r   COPY public.ref_menu_head (i_id, nama_menu, icon_menu, rut_name, stand_alone, created_at, updated_at) FROM stdin;
    public       postgres    false    234   20      �          0    33224    ref_menu_item 
   TABLE DATA               {   COPY public.ref_menu_item (i_id, id_menu_head, nama_sub_menu, icon_sub_menu, rut_name, created_at, updated_at) FROM stdin;
    public       postgres    false    235   �0      �          0    33227    ref_no_urut 
   TABLE DATA               K   COPY public.ref_no_urut (id, code, no_urut, bln, thn, flag_so) FROM stdin;
    public       postgres    false    236   �3      �          0    33232    ref_ori_kondisi 
   TABLE DATA               \   COPY public.ref_ori_kondisi (i_id, e_ori_kondisi, tipe, created_at, updated_at) FROM stdin;
    public       postgres    false    238   4      �          0    33237    ref_pelanggan 
   TABLE DATA               �   COPY public.ref_pelanggan (i_pel, e_nama_pel, e_alamat_pel, f_pkp, e_npwp_pel, e_telp_pel, e_kont_pel, created_at, updated_at, e_fax_pel, e_kota_pel, e_kode_marketing, n_jth_tempo) FROM stdin;
    public       postgres    false    240   �4      �          0    33246    ref_role 
   TABLE DATA               M   COPY public.ref_role (i_id, e_role_name, created_at, updated_at) FROM stdin;
    public       postgres    false    242   �C      �          0    33251    ref_sat_qty 
   TABLE DATA               J   COPY public.ref_sat_qty (i_id, e_sat, created_at, updated_at) FROM stdin;
    public       postgres    false    244   �D      �          0    33256    ref_so_flag 
   TABLE DATA               X   COPY public.ref_so_flag (i_id, flag_name, f_active, created_at, updated_at) FROM stdin;
    public       postgres    false    246   E      �          0    33262    ref_tekstur_akhir 
   TABLE DATA               T   COPY public.ref_tekstur_akhir (i_id, e_tekstur, created_at, updated_at) FROM stdin;
    public       postgres    false    248   fE      �          0    33267    ref_warna_dasar 
   TABLE DATA               V   COPY public.ref_warna_dasar (i_id, e_warna_dasar, created_at, updated_at) FROM stdin;
    public       postgres    false    250   �E      �          0    33272    ref_workflow 
   TABLE DATA               P   COPY public.ref_workflow (i_id, definition, created_at, updated_at) FROM stdin;
    public       postgres    false    252   F      �          0    33279    rfp 
   TABLE DATA               �  COPY public.rfp (i_id, i_no_rfp, d_selesai, i_pel, e_contact_person, f_repeat, i_desain, e_motif, e_material, e_original_cond1, e_ket_ori_cond, e_color, i_penyedia, d_tgl_material_in, n_qty_material, n_pengkerutan, e_gramasi_from, e_gramasi_to, e_penyesuaian_lebar_from, e_penyesuaian_lebar_to, e_pakan_from, e_pakan_to, e_lusi_from, e_lusi_to, e_tekstur_akhir_from, e_tekstur_akhir_to, e_cw_1, e_cw_2, e_cw_3, e_cw_4, e_cw_5, e_cw_6, e_cw_7, e_cw_8, i_status, d_approved_mrk, d_approved_pro, d_approved_ppc, created_at, updated_at, i_id_so, e_reject_mkt, e_reject_prod, e_original_cond2, e_jenis_printing, i_jns_kain, urutan_fifo, f_proses, old_urutan_fifo, d_proses, e_ket_rfp, e_material_others, e_color_others, alasan_pindah_slot) FROM stdin;
    public       postgres    false    255   �F      �          0    33287    rfp_lpk 
   TABLE DATA               `   COPY public.rfp_lpk (i_id, i_id_rfp, e_nomor_lpk, created_at, updated_at, qty_roll) FROM stdin;
    public       postgres    false    256   {y      �          0    33295    tx_jns_proses_kartu 
   TABLE DATA               d   COPY public.tx_jns_proses_kartu (i_id, description, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    258   �      �          0    33300    tx_rfp_accepted 
   TABLE DATA               j   COPY public.tx_rfp_accepted (i_id, i_id_rfp, d_accepted, user_accept, created_at, updated_at) FROM stdin;
    public       postgres    false    260   ��      �          0    33305    tx_sisa_uang_masuk 
   TABLE DATA               l   COPY public.tx_sisa_uang_masuk (i_uang_masuk, i_pel, v_sisa_uang_masuk, created_at, updated_at) FROM stdin;
    public       postgres    false    262   b�      �          0    33314    tx_sj_kartu 
   TABLE DATA               X   COPY public.tx_sj_kartu (i_id, i_no_sj, i_id_kartu, created_at, updated_at) FROM stdin;
    public       postgres    false    264   �      �          0    33319    tx_spec_kain 
   TABLE DATA               �   COPY public.tx_spec_kain (i_id, i_jns_kain, e_kondisi_kain, e_lebar, e_satuan_lebar, e_gramasi, e_sat_gramasi, i_penyedia, i_pel, created_at, updated_at, i_desain) FROM stdin;
    public       postgres    false    266   �      �          0    33324    tx_spec_pekerjaan 
   TABLE DATA               �   COPY public.tx_spec_pekerjaan (i_id, i_desain, i_qty_warna, e_motif, e_jenis_printing, n_colow_way, e_color_way, e_toleransi_cacat, i_pel, created_at, updated_at) FROM stdin;
    public       postgres    false    268   r�      �          0    33329    tx_workstation 
   TABLE DATA               �   COPY public.tx_workstation (i_id, i_id_rfp, e_cw, i_id_bagian, n_tot_qty, n_proses, n_sisa, e_ket, created_at, updated_at, e_pelaksana, e_shift, d_tgl, i_id_kartu, i_cw, d_time) FROM stdin;
    public       postgres    false    270   �      �          0    33337    users 
   TABLE DATA               �   COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, username, role, login_pertama, is_active, count_reset_password, status) FROM stdin;
    public       postgres    false    272   �g                 0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 2, true);
            public       postgres    false    197                       0    0 #   mst_invoice_item_i_invoice_item_seq    SEQUENCE SET     R   SELECT pg_catalog.setval('public.mst_invoice_item_i_invoice_item_seq', 12, true);
            public       postgres    false    201                       0    0    mst_menu_role_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.mst_menu_role_i_id_seq', 635, true);
            public       postgres    false    205                       0    0    mst_nota_item_i_nota_item_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.mst_nota_item_i_nota_item_seq', 23, true);
            public       postgres    false    208                       0    0    mst_packing_list_i_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.mst_packing_list_i_id_seq', 5114, true);
            public       postgres    false    210                       0    0 '   mst_pelunasan_item_i_pelunasan_item_seq    SEQUENCE SET     V   SELECT pg_catalog.setval('public.mst_pelunasan_item_i_pelunasan_item_seq', 12, true);
            public       postgres    false    213                       0    0    mst_sales_plan_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.mst_sales_plan_i_id_seq', 126, true);
            public       postgres    false    215                       0    0    mst_so_item_i_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.mst_so_item_i_id_seq', 321, true);
            public       postgres    false    218                       0    0    ref_jns_bahan_i_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.ref_jns_bahan_i_id_seq', 4, true);
            public       postgres    false    222                       0    0    ref_jns_printing_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_jns_printing_i_id_seq', 9, true);
            public       postgres    false    224                        0    0    ref_kain_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_kain_i_id_seq', 26, true);
            public       postgres    false    226            !           0    0    ref_ket_umum_so_child_i_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.ref_ket_umum_so_child_i_id_seq', 7, true);
            public       postgres    false    229            "           0    0    ref_kondisi_kain_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_kondisi_kain_i_id_seq', 5, true);
            public       postgres    false    231            #           0    0    ref_logo_i_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.ref_logo_i_id_seq', 4, true);
            public       postgres    false    233            $           0    0    ref_no_urut_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ref_no_urut_id_seq', 149, true);
            public       postgres    false    237            %           0    0    ref_ori_kondisi_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ref_ori_kondisi_i_id_seq', 8, true);
            public       postgres    false    239            &           0    0    ref_pelanggan_i_pel_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ref_pelanggan_i_pel_seq', 74, true);
            public       postgres    false    241            '           0    0    ref_role_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.ref_role_i_id_seq', 21, true);
            public       postgres    false    243            (           0    0    ref_sat_qty_i_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.ref_sat_qty_i_id_seq', 1, false);
            public       postgres    false    245            )           0    0    ref_so_flag_i_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ref_so_flag_i_id_seq', 6, true);
            public       postgres    false    247            *           0    0    ref_tekstur_akhir_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.ref_tekstur_akhir_i_id_seq', 6, true);
            public       postgres    false    249            +           0    0    ref_warna_dasar_i_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ref_warna_dasar_i_id_seq', 5, true);
            public       postgres    false    251            ,           0    0    ref_workstation_i_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.ref_workstation_i_id_seq', 12, true);
            public       postgres    false    253            -           0    0    rfp_lpk_i_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.rfp_lpk_i_id_seq', 341, true);
            public       postgres    false    257            .           0    0    rfp_urutan_fifo_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.rfp_urutan_fifo_seq', 182, true);
            public       postgres    false    254            /           0    0    tx_jns_proses_kartu_i_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.tx_jns_proses_kartu_i_id_seq', 722, true);
            public       postgres    false    259            0           0    0    tx_rfp_accepted_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tx_rfp_accepted_i_id_seq', 320, true);
            public       postgres    false    261            1           0    0 #   tx_sisa_uang_masuk_i_uang_masuk_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.tx_sisa_uang_masuk_i_uang_masuk_seq', 4, true);
            public       postgres    false    263            2           0    0    tx_sj_kartu_i_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.tx_sj_kartu_i_id_seq', 307, true);
            public       postgres    false    265            3           0    0    tx_spec_kain_i_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.tx_spec_kain_i_id_seq', 114, true);
            public       postgres    false    267            4           0    0    tx_spec_pekerjaan_i_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.tx_spec_pekerjaan_i_id_seq', 114, true);
            public       postgres    false    269            5           0    0    tx_workstation_i_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.tx_workstation_i_id_seq', 1688, true);
            public       postgres    false    271            6           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 20, true);
            public       postgres    false    273            �           2606    33380    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    196            �           2606    33382    mst_do_sj mst_do_sj_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_do_sj
    ADD CONSTRAINT mst_do_sj_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_do_sj DROP CONSTRAINT mst_do_sj_pkey;
       public         postgres    false    198            �           2606    33384 &   mst_invoice_item mst_invoice_item_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.mst_invoice_item
    ADD CONSTRAINT mst_invoice_item_pkey PRIMARY KEY (i_invoice_item);
 P   ALTER TABLE ONLY public.mst_invoice_item DROP CONSTRAINT mst_invoice_item_pkey;
       public         postgres    false    200            �           2606    33386    mst_invoice mst_invoice_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.mst_invoice
    ADD CONSTRAINT mst_invoice_pkey PRIMARY KEY (i_invoice);
 F   ALTER TABLE ONLY public.mst_invoice DROP CONSTRAINT mst_invoice_pkey;
       public         postgres    false    199            �           2606    33388    mst_kartu_cw mst_kartu_cw_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.mst_kartu_cw
    ADD CONSTRAINT mst_kartu_cw_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.mst_kartu_cw DROP CONSTRAINT mst_kartu_cw_pkey;
       public         postgres    false    203            �           2606    33390    mst_kartu mst_kartu_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_kartu
    ADD CONSTRAINT mst_kartu_pkey PRIMARY KEY (i_id);
 B   ALTER TABLE ONLY public.mst_kartu DROP CONSTRAINT mst_kartu_pkey;
       public         postgres    false    202            �           2606    33392     mst_menu_role mst_menu_role_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.mst_menu_role
    ADD CONSTRAINT mst_menu_role_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.mst_menu_role DROP CONSTRAINT mst_menu_role_pkey;
       public         postgres    false    204            �           2606    33394     mst_nota_item mst_nota_item_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.mst_nota_item
    ADD CONSTRAINT mst_nota_item_pkey PRIMARY KEY (i_nota_item);
 J   ALTER TABLE ONLY public.mst_nota_item DROP CONSTRAINT mst_nota_item_pkey;
       public         postgres    false    207            �           2606    33396    mst_nota mst_nota_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.mst_nota
    ADD CONSTRAINT mst_nota_pkey PRIMARY KEY (i_nota);
 @   ALTER TABLE ONLY public.mst_nota DROP CONSTRAINT mst_nota_pkey;
       public         postgres    false    206            �           2606    33398 &   mst_packing_list mst_packing_list_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.mst_packing_list
    ADD CONSTRAINT mst_packing_list_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.mst_packing_list DROP CONSTRAINT mst_packing_list_pkey;
       public         postgres    false    209            �           2606    33400 *   mst_pelunasan_item mst_pelunasan_item_pkey 
   CONSTRAINT     v   ALTER TABLE ONLY public.mst_pelunasan_item
    ADD CONSTRAINT mst_pelunasan_item_pkey PRIMARY KEY (i_pelunasan_item);
 T   ALTER TABLE ONLY public.mst_pelunasan_item DROP CONSTRAINT mst_pelunasan_item_pkey;
       public         postgres    false    212            �           2606    33402     mst_pelunasan mst_pelunasan_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.mst_pelunasan
    ADD CONSTRAINT mst_pelunasan_pkey PRIMARY KEY (i_pelunasan);
 J   ALTER TABLE ONLY public.mst_pelunasan DROP CONSTRAINT mst_pelunasan_pkey;
       public         postgres    false    211            �           2606    33404 "   mst_sales_plan mst_sales_plan_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.mst_sales_plan
    ADD CONSTRAINT mst_sales_plan_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.mst_sales_plan DROP CONSTRAINT mst_sales_plan_pkey;
       public         postgres    false    214            �           2606    33406    mst_so_item mst_so_item_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.mst_so_item
    ADD CONSTRAINT mst_so_item_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.mst_so_item DROP CONSTRAINT mst_so_item_pkey;
       public         postgres    false    217            �           2606    33408    mst_so mst_so_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.mst_so
    ADD CONSTRAINT mst_so_pkey PRIMARY KEY (i_id);
 <   ALTER TABLE ONLY public.mst_so DROP CONSTRAINT mst_so_pkey;
       public         postgres    false    216            �           2606    33410     ref_jns_bahan ref_jns_bahan_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.ref_jns_bahan
    ADD CONSTRAINT ref_jns_bahan_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.ref_jns_bahan DROP CONSTRAINT ref_jns_bahan_pkey;
       public         postgres    false    221            �           2606    33412 &   ref_jns_printing ref_jns_printing_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ref_jns_printing
    ADD CONSTRAINT ref_jns_printing_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.ref_jns_printing DROP CONSTRAINT ref_jns_printing_pkey;
       public         postgres    false    223            �           2606    33414    ref_kain ref_kain_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_kain
    ADD CONSTRAINT ref_kain_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_kain DROP CONSTRAINT ref_kain_pkey;
       public         postgres    false    225            �           2606    33416 0   ref_ket_umum_so_child ref_ket_umum_so_child_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.ref_ket_umum_so_child
    ADD CONSTRAINT ref_ket_umum_so_child_pkey PRIMARY KEY (i_id);
 Z   ALTER TABLE ONLY public.ref_ket_umum_so_child DROP CONSTRAINT ref_ket_umum_so_child_pkey;
       public         postgres    false    228            �           2606    33418 $   ref_ket_umum_so ref_ket_umum_so_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_ket_umum_so
    ADD CONSTRAINT ref_ket_umum_so_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_ket_umum_so DROP CONSTRAINT ref_ket_umum_so_pkey;
       public         postgres    false    227            �           2606    33420 &   ref_kondisi_kain ref_kondisi_kain_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ref_kondisi_kain
    ADD CONSTRAINT ref_kondisi_kain_pkey PRIMARY KEY (i_id);
 P   ALTER TABLE ONLY public.ref_kondisi_kain DROP CONSTRAINT ref_kondisi_kain_pkey;
       public         postgres    false    230            �           2606    33422    ref_logo ref_logo_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_logo
    ADD CONSTRAINT ref_logo_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_logo DROP CONSTRAINT ref_logo_pkey;
       public         postgres    false    232            �           2606    33424     ref_menu_head ref_menu_head_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.ref_menu_head
    ADD CONSTRAINT ref_menu_head_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.ref_menu_head DROP CONSTRAINT ref_menu_head_pkey;
       public         postgres    false    234            �           2606    33426     ref_menu_item ref_menu_item_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.ref_menu_item
    ADD CONSTRAINT ref_menu_item_pkey PRIMARY KEY (i_id);
 J   ALTER TABLE ONLY public.ref_menu_item DROP CONSTRAINT ref_menu_item_pkey;
       public         postgres    false    235                       2606    33428    ref_no_urut ref_no_urut_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ref_no_urut
    ADD CONSTRAINT ref_no_urut_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ref_no_urut DROP CONSTRAINT ref_no_urut_pkey;
       public         postgres    false    236                       2606    33430 $   ref_ori_kondisi ref_ori_kondisi_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_ori_kondisi
    ADD CONSTRAINT ref_ori_kondisi_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_ori_kondisi DROP CONSTRAINT ref_ori_kondisi_pkey;
       public         postgres    false    238                       2606    33432     ref_pelanggan ref_pelanggan_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.ref_pelanggan
    ADD CONSTRAINT ref_pelanggan_pkey PRIMARY KEY (i_pel);
 J   ALTER TABLE ONLY public.ref_pelanggan DROP CONSTRAINT ref_pelanggan_pkey;
       public         postgres    false    240                       2606    33434    ref_role ref_role_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.ref_role
    ADD CONSTRAINT ref_role_pkey PRIMARY KEY (i_id);
 @   ALTER TABLE ONLY public.ref_role DROP CONSTRAINT ref_role_pkey;
       public         postgres    false    242            	           2606    33436    ref_sat_qty ref_sat_qty_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_sat_qty
    ADD CONSTRAINT ref_sat_qty_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_sat_qty DROP CONSTRAINT ref_sat_qty_pkey;
       public         postgres    false    244                       2606    33438    ref_so_flag ref_so_flag_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ref_so_flag
    ADD CONSTRAINT ref_so_flag_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.ref_so_flag DROP CONSTRAINT ref_so_flag_pkey;
       public         postgres    false    246                       2606    33440 (   ref_tekstur_akhir ref_tekstur_akhir_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.ref_tekstur_akhir
    ADD CONSTRAINT ref_tekstur_akhir_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.ref_tekstur_akhir DROP CONSTRAINT ref_tekstur_akhir_pkey;
       public         postgres    false    248                       2606    33442 $   ref_warna_dasar ref_warna_dasar_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.ref_warna_dasar
    ADD CONSTRAINT ref_warna_dasar_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.ref_warna_dasar DROP CONSTRAINT ref_warna_dasar_pkey;
       public         postgres    false    250                       2606    33444    ref_workflow ref_workflow_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.ref_workflow
    ADD CONSTRAINT ref_workflow_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.ref_workflow DROP CONSTRAINT ref_workflow_pkey;
       public         postgres    false    252            �           2606    33446    ref_bagian ref_workstation_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.ref_bagian
    ADD CONSTRAINT ref_workstation_pkey PRIMARY KEY (i_id);
 I   ALTER TABLE ONLY public.ref_bagian DROP CONSTRAINT ref_workstation_pkey;
       public         postgres    false    220                       2606    33448    rfp_lpk rfp_lpk_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.rfp_lpk
    ADD CONSTRAINT rfp_lpk_pkey PRIMARY KEY (i_id);
 >   ALTER TABLE ONLY public.rfp_lpk DROP CONSTRAINT rfp_lpk_pkey;
       public         postgres    false    256                       2606    33450    rfp rfp_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.rfp
    ADD CONSTRAINT rfp_pkey PRIMARY KEY (i_id);
 6   ALTER TABLE ONLY public.rfp DROP CONSTRAINT rfp_pkey;
       public         postgres    false    255                       2606    33452 ,   tx_jns_proses_kartu tx_jns_proses_kartu_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.tx_jns_proses_kartu
    ADD CONSTRAINT tx_jns_proses_kartu_pkey PRIMARY KEY (i_id);
 V   ALTER TABLE ONLY public.tx_jns_proses_kartu DROP CONSTRAINT tx_jns_proses_kartu_pkey;
       public         postgres    false    258                       2606    33454 $   tx_rfp_accepted tx_rfp_accepted_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.tx_rfp_accepted
    ADD CONSTRAINT tx_rfp_accepted_pkey PRIMARY KEY (i_id);
 N   ALTER TABLE ONLY public.tx_rfp_accepted DROP CONSTRAINT tx_rfp_accepted_pkey;
       public         postgres    false    260                       2606    33456 *   tx_sisa_uang_masuk tx_sisa_uang_masuk_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.tx_sisa_uang_masuk
    ADD CONSTRAINT tx_sisa_uang_masuk_pkey PRIMARY KEY (i_uang_masuk);
 T   ALTER TABLE ONLY public.tx_sisa_uang_masuk DROP CONSTRAINT tx_sisa_uang_masuk_pkey;
       public         postgres    false    262                       2606    33458    tx_sj_kartu tx_sj_kartu_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.tx_sj_kartu
    ADD CONSTRAINT tx_sj_kartu_pkey PRIMARY KEY (i_id);
 F   ALTER TABLE ONLY public.tx_sj_kartu DROP CONSTRAINT tx_sj_kartu_pkey;
       public         postgres    false    264                       2606    33460    tx_spec_kain tx_spec_kain_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tx_spec_kain
    ADD CONSTRAINT tx_spec_kain_pkey PRIMARY KEY (i_id);
 H   ALTER TABLE ONLY public.tx_spec_kain DROP CONSTRAINT tx_spec_kain_pkey;
       public         postgres    false    266            !           2606    33462 (   tx_spec_pekerjaan tx_spec_pekerjaan_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.tx_spec_pekerjaan
    ADD CONSTRAINT tx_spec_pekerjaan_pkey PRIMARY KEY (i_id);
 R   ALTER TABLE ONLY public.tx_spec_pekerjaan DROP CONSTRAINT tx_spec_pekerjaan_pkey;
       public         postgres    false    268            #           2606    33464 "   tx_workstation tx_workstation_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.tx_workstation
    ADD CONSTRAINT tx_workstation_pkey PRIMARY KEY (i_id);
 L   ALTER TABLE ONLY public.tx_workstation DROP CONSTRAINT tx_workstation_pkey;
       public         postgres    false    270            %           2606    33466    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public         postgres    false    272            '           2606    33468    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    272            �           1259    33469    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    219            �   H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.#d��(
����R�R�SKZb���� ]      �   q  x��V�n�6]S_�e�����! v['�ua�(Zds�Gz�&���~~ϐ�HɍQ[�Pc�:3s��0N�(nޜ)�xa�Qgʞ)'l���x�!�J	�\�]k���Ee��u�E<4;J��7>҃n��Ź��^^]�����|:��7;�"�S�����0p;?���%��0��a�
�n�s�-i�,"��6m�2��7>�U}*pd���m��<n����.mPOUu¿k��7�����u���?X�bW�'�"�5���!�nΗ93�&���EL଻.���ulQ��a�Z���W������g�����(��t���C�>>>�o�+EQ������Z�49Sϣ#��O�Zfp��̋�%dP�)�;�YAn�k9Մ�9�v������R�\��k�Ӹ�����8�b.�l(��Ɛ�!͋�<���_k���"B������4+[$�ʃ � �N�2��M�;��$[��Z�"qUP�r�EŎ}�ONZ$���w�{��B�<� ����h�	���
:��S����@�,'=)�к�b�����$[*�Cҁ\��������N~ p侳"��A�pQ��Ӭ}���:�x�=�� =�.���l� �,�"g=�p����9�˅�t��ڳj�V{�zP�5Vy��i�,�,Vmj�T���V��K�7iy�IE�4���*��J�CA_���C561t�wּش��Li��'�
�I�$xI���
%��<r4_���Z+�iӑi�lS������;/V�O�n��Q[�(����Z
���ϼ���[�v�_H��@�7�U�ͼX��Y��������^b��-��E_���x��_1��:�s����J/V�ˑz�IEu�"���[L(��a<m�_��O>ǻ��C�-�|��!/��4Kcs��`^�:�]9:d'p&�b����!�QLu��������-�1�.�d�}�w�'��;|���Z�/�m"�z|%�w#��Fl��/p8��4��G"�|l ��!c����rS��yL͌E���e���W�vh���K�z�?����������۟����x���Gd�2��G��!�H!�4��X^R=3��1k������?g���������_�ID�      �      x������ � �      �      x������ � �      �     x��[Yo�F~�~E�a�B��n��;:�ȧ�����D�X	����j��*N�46����_uu�ݲ�xw���?�_m*��zO�=ߖ �?�?\^���jetmCm��{6*k��1x�	��<;Vsvݱ��?w~yz|�d��B泵�1Q�����N[��҄��]ձ�͊������:hn�R��W������Y/�/�M�V���:Z�w�b�f׍Z7K�z�|yZ�����n��Q��Հ�t9/�1E��P��m�D7J����]p��f�l@��ӣzl��8��p����\}����F]/��������m����~i@�o갹j��E<ˣ���-&P�0q�Iy�v��2��U��e=��r�:���7�����qHϖ���7�	E��2O�*�G;:Q�f'��������s���G���쎯�����]t@�;�Q�jq�vV�f3�wp`���������0����];�.x�/�ԓ\6�Ā)v8�}zP�W�O���F��-�����\��v��٧��|�>��Yn[��nAa���u�6eoS_�3p��V�wsA�_>�W����
������73~�*n,����b�Gfy�	�y|!!��~,ƞ=b~�t�x̖�ڧ2�Y�E�׃x��	\�Ӹ�EHE�vj�]�]��al�6B��Bb�Uƍ0���
�P�dJ�OBI)�F�	J���V��ɔ��q�PD������$�QL����"s+02��u<�8�g EB,�Z�~���?�����هٻ�Gut��������^��ٯg��S�8�艂c�)���l��:S!�#͌�]f��R�Hn< ��e_&J�/�dx�]�^�s�^p|#I��0��)�V{%4HX�1��Kaf�����ua��܁[F���>K���F���\,5�˔�@��l�$����;Ũ����4�:걳����@x�WM���X�隖��:Ҙ�"�r��b����D�֯�����څu5�����y	Owh����V���,׋�w�u.%T���z���VU��Ao�7�ko������Uws�J�B��V�%�m߃D�/O�⩡�ys;�{$��Q°(9��`�<�%&��ǧf��o��]g�a�rF���Aw�[l���sA��f��Z�U�ǚ��7B��5�0۹mk)����6�ke`3vy���?^��@�*�i���c��ےg����v�(���D������"�����M�2�c����Y�R��
4ֱF9�R���gLn���~^"k���L�f�o�9�Y-b/]����X��m�Zܩ�ߋ;p���v?�{
.��Aݟ?���5�����ے��11���X��Lv�f$���t(`0�����O������8^�:27�
o���'2�h�P<9��fׅ|[*/u��>KB4P�MN@<�]�M'��E�ӚC�-�����:?��\�g�c��M��ڷ\E�З��.���y&<#s��\#a�%+�&��r�]хw���.h��(�W���2x>����?i%P��e��Y��,!y�d�D2$�&�|��{qL���*ײ>��dW����~�@`U�R1wm]�x|z������q[�\��9�x!O7�阊��On[ŲhnsC@�$L6_^�8R�Th7��Yh)��²�9���_�5�x~=3��WЃR8�N�TM##�C��:����p��|o��z,��B)b���|��/R�g�@��2٪�ǥ�������k����Q����U��k�Y,�_�]]]ڎ}O |���_� 3��GPH=@�w�Lr]@=	�`z���]��3S�(br��ե,p �T}�H}��ܰ:e<���N�B�J?g�l�o�'2���ȴ/��wW�>k�S���2�!;'�g1�Ux�'���D��{���s1��W�.Rv�zn�x"�_����F��x�F�}m���.�DLv�Iɀ!3��;�h��Q���<c�E�ʬx��{� ri-b�<�˜4��N���xb��e0|I���$|�]�+�e����i�Y%�za����w'D���h�3Ԉ�,�����x���8?K"&*�&o{x�=Վ��U>�"&o���*#���;�f�:��EL6Q�N� �hY����v�kb����։��.���U�S��JA�l?��筓��"�����XD��L����D��.��]�*�7+��q��.���A=�W_�9�'w�����V^��
�+�H7r;�������ն�AĶ����1�c�YKU�2��L�Cv�,�3B��}���x�z��Xi���h��i%O1��wO�[P� _��z�[騇`"e�BҌ���R�칎V��)�c%�(m�F�t���)�`2%w�d�+�v1�E	2	��)�c$�(}��S�6��M�dJ��3�P$�((Y�8�q"&��=F������I�d2� Rdd�H�(����b"Y�]�|v	��(}Q���E�Ú;��L�]�4���X�ۘ-
&�qg(-#�K!��@���d2��cd�(B��ŉ�9�d2n�e�Ȁ)�3�s���u}-&Sr�,#��E9z�_r�EL��{�:J૊j<1%�8⭺$b2%υe�VY"�8%w��)y.,+�JXb5��< 3�V	&RV��+����\Q9F��(��a2��ad���W2� �L����� ���ez�`2%OI�g���Ƴ`�z�/�dJq*�(CQ��bۼ���iGH	���	^�P���PEP���x *s����Ϙ�\���(a���ꏸ9&S� T%F	�x4h_�*�ɔ< U%��
�ǃ^��@��(&rr�d
mw1<���T]�m����@��������kV�5�wk����������C�6�,�Ł��j�y2��\x�J<�၎nDě��'æS��}sD���1��1|c�EN,FQ�z����5��/X�Oo��������4�w2v�C��̨+[�7����%���:�_�����޲z��|� >��ϐ����Qß�fy=�{�Fډ|K"�������@	F�"��Ppa߿2�b[��xO�	�R�,\uY�2�~�� �pizە`����>Xu|���L�����HxU��ڪ�W@?�2�i����X�Zz�t�#��Elό�\9��d��!�es@�u�������nmO\�g<�D�B0���'s�o����OF	����γO��=Q<km炮?��x`N� (&��D;�qDS�f�8ZJ6���f��DL�!q9Ĉؗ�#�%�_pᵆ���# �,w�T2*|&d��8e�gkzE0Y)REe %�#�����l�G�
�DJ^m���̝f9r����]�nXl�7m��-&ʀ>ϢBW��iX���|{X���^��*E[�g9��?=��ݑ�����s��]�_Y������s>���Z��n������ˑ}`��y7 Pl�/r����_�0�b�[���\�����g�k�	eͿ�I�/������!��4kU��`�8k�YS��$�c�m�ɬ��6Fs��Zm�[��5�b�<�^�)3�lG!_g�_��|�_����RE�      �   �  x��Zˎ[�]_}W���E���`�V��7{ O$���0��>�T�����d��45,V��ԩ��O�N����p�\�l�e�)��L�8�����Lٻ��v����Lv��6M��f��7w��ۛ�N���㷌���s-���gv&��ۼQ�^���3�ҔD��L)�����:��Խۚy
�l����O�ǻ�'a��?�v�q�Sv���[w���3��\y*�N�;�d�W�L��8����up�DW�����Sd���8�+Nߖv��}š�OT�+e*'���}���3�;'�̈́�.�Bļ:�֤��r���&�K֘$*"T�tw��z��@;��ݿ摼)\y���ӏ���1edo�q����.6�4�x%��(�	^�h�E�0y�������Z��FS�EL������?w�b��P8�?Wӫ�,6�R%87#�)���%�9����as��F
!�3�e��
���K~p�G9��j��K�b�p��;ИRPF��6z$J�	n�DI�����]�	���}���@	�E'Q<1ܭ��㐉�6۩���y>�P{����I����0q^޿�׃}i�XӀyR	c�_�a�z4A�� E1W�"���O��c[�k��"3f�R�Ҕ�����'�#�%V�R�Pq��s��~�����wb�+b��t�M�`7�?�: �� ;p�C�+����͟��@� 8�RX�	�f+2΍%�����PX�  _V�͆���
� :���$��0�~����8!!�W��K4�W��������V��k.J�P��D�C"�Q%V�1��aw�2���>������dv J��i�{fZ���(�M��U#= ���FD�*�Nm �
�����"&���uΣ�������L��Ħ�}|������Hd��?g�;AQy*dᐹ�%�5(侓\���N�d )�l�$����Z�Hd��.m�/�1�:�H(�to`q�8�QcPРv���5��y��w���;5�+��ǝ숕$��:�u��\�����p��p%��c��1��y.����<�޳O!�諮E�%F�L1���|����x�K*J:��]�v���r5�V����]pګ�~=ȈT�C/MC�z*	�E�4i݀X���U�A\�on���K��i�D#v����W��(G�[[���9��)��)�H�*
������3��R��PA�t>�⣬#4�xt7�%+�e�]�f'��W����҂�P�:d!^�M΀�0,�;�Bl�ѩڜ m�v�v���ς �S�P=�?p�?������O����!��ʗ���w7��C��@� �.��9rù�_�f�9�	�u��R��FOR�h�Q�M�O���Op0	����aL�9W���'G_×Q&�Be,�rp0�������_v�n��2�¼�i9�Cq����F�!.0��E˝�q�~Q�)`Wg�[���䗰_M"P��-��Ͱ}x<�ܽ}�e�܎y��Bmb��&�gC�z9�Eʣa�!gsVN���>��h���?��'2�SR���O=�s(�|���AKb�e6��Y�[N����{b柽~�'�"�*�0�Ȏ�X�h��\WR�N��'F�a=�4*�n���s�"��-�$ ����(�-�㗥ʁ�R�������u �(�'�̩=��g��I�<I�&�c��D�Y��p;�M茲2�"u$ZMZZ�<yZ�6-]�d6�����ڶM��#V�C��ϓeܬ[K�*']���N��>nk��N��3շG_G?ň��l}rˤ&]W9� �/Kӭb�ј�~�F�7��S���H���0W�@5$�v[zw#��%�'EG����*ʀ�Zd/���3�9ɘ9�Χ�XJ�IW��PFZښ�#�j�fh��{�d�����1��ıIW��lp�}��^ٳ��ߑ�lҰ�j�O|{M��,Ȑ�!u)O��7�MW."�W;}����ã�L�m�Q����TZF�E�0r�iL��4U�����<M����+�q�8�I�m�!�o ���PP�J vJF0	9?�W&{d/�iҮMhzAg�^�}x�cw��'fL�f�HZ��z�7���B�w��������VLF��k��`�����E�R	�z>��n��#��`r�H]y�x�mŚtɈ�`c9�5T޽��lwOP�������;w-��4� ���A%�B����в�Z�g7�+�8I�k5-����ZF��v6v[R�@Z�)��f�����Dȭ$f�<��ۉ0��D��XJH�'-�!ΡC/쥞�=�����^}�VR����t�_����hX���5�oG����.v����:�������z����$��!�||U�A�M���7L��q%�hadz�{��W� �*�0!�g�C�a�cȺ|<Jf�[X&Q��ţD}��du�\�(�O�3�݅�4������bd���by���V{���H�W�,0���1��i��B*U)��?����?�IT(���]VNC�Mb � �ڞ��6RS�8S�LS^�|����'k�)Q�k�3.��\�|��2��ER�=�ˡ������Τ���w/P^yAG���]�+j�2uy$C����!
�؆�D��0*�4o����%~E�N��U�m�x�X�K�f�K�� k6yV}�K��i�mΠ��v���R8��<z`v�Z/X�����*�DY]���#�]�)�Cd���eʻs�qbeq]��'�\Σ�0Z��&ǅL썩-}/\;r2���Yu��x���^��sJ�7 K��u��&NC���^�h�ʕȞW��b������ڤ��]&�w����g�2��̅Ўe�����҂���	+�um�12�ؕ��g�94�2�,[vL��L`�)�ES7���E�[r���o����q��f�(���S��������ndH�O��>�S��/n�4��|0��s�{��{<�jf�pQ���e{ߤ�l�'\.�w+�?m�v���NM�|zе2`{U4i+mY����S�2�����I����[=�D��?�	��Ff����v�z�lO�0�k����6�:���P~������ p[�i���67�О�f��<u��˞�-P۩�bS�R������mu�����S�c�^��}��C�p�"���hxŞ^�|_�4d�k�Q����-v/��v��f���W2�x�����
��%�'��ʡI�E*0�6�����t\+�9٭�������;U0~p��8-�L�ɕ�[t�"����T#��^����"+�N���.TQ�nyn���`��]�-��eU�Ɉ��7��2�Y]�ۉ��c�� �ª� �c.���5�u��7�F��3*��~��A�֞�V��&޼[�V��rf��U����"�������X�$�vX����ag����[Y'@oH�3eU��v/ۤ!�2Ǔ/@��`�ɥ���v����u�͆��xP�wO�̧C�E�7�n�T�b��^�o°�1�f����4UA�n�/z�D�>er5� Y/�T@А���&�}& 4*�~�
�]�*Vn���
�W���&��ګ�@��n���f䵋@�J ��nsd�DX�O_꒞f�O(��)�������8l-�웷�_��<Eps�뙭e*=�,�/�=4�g��ÕQu�ח#��A[R�h�t}.�4�r�[��u��m���Ze��e~�����tR      �   6  x�}�ّ�0 �ow��lY�|�F��Ǳ:0�E���
tB�!�߿�����!�S����7ґ�b��@V�|P�2���e��{���7� H
JT�| 
���z�r�$�%�n"���T�}g�<�3pc1	��IA�*��r��ozA~����U�	�\�g�?����9�q|O�wA���Z���L'?�i�D������wM�R�_P�s��~ ����Dq?d#�D�� �1�g��bͅg�m�rΣ>�R��s�:E��,�D�U������Q�{�M��M[Ϧ_�y�5xI&�$���}�ק��N }�C� Y�{�WP���P�|�t��� ~�UNj.��lm��q��-�T��x)���ڒĦ��i��ZT0�k �`�� ��ل9q&%�5؀� ������>>!�MN��+o-������w�S�e��3��A6��؄q�jb��M�.���a����#�M,��D�!��+M�u�l�|��:��ѣtm��z3 �6�� ��z� �y��[2��`ْA�@�3�|. n�v�W��fծ�.ؔ��@2"�l�L��M,����&֋ڇѼ
�9c].� �
��� ��}��_^�@��� �1/� x�7�6��܈j�%�f�M5���Yo�:�p��_�g=g"�}ݐt����I�'t��L�����I��x;)���zɄ���?�??&�\J�C?�A5�u�����l&��s�NQ����d��M��M�$��)I��Y�d��&�A4s���&�Xo"�v�^�ߟ���]��      �      x������ � �      �      x������ � �      �      x���]�,+Φy�{gy$�Ϻ˯�F��=����{�8�|!��XX���� p��L ���@����[ß������'�B�I�C����?��PI���2��>��l'i��l����΃r�{^.:z
^%n��^LE?�����G�'
����'�����I�w�2��?~������g7)�(��uE�p�8�>^EI��R��Fyp,�E���V�f�$������)D�f������s���	�����C�'��sqK�\X�s�߶�fn�s����u�
�P�M�����\�12�R��xT�2��3x�� IO׋��E��m��׋؜ �D	.sW�$[� 
p�!Ip�#�Yzd��+l�X5U��ߜ1x�����'��
n�n09���;fƥ�+抚b��*���i�t����.����tEM�o	�^�)Ip�U�Y��Z�"�-V	�[lN���U�\��t�9�2�4�1Iry� ���k�g�T%��"M�\�����(���#�:fs���)��/	(��B�?�@�Zr�R�9'���̧d��߳S��S����/9%QJ� ~]��|O��蔤�K�K3·d~�����c�($�_�d�$���� %y��)�d�.I��H��)�J2�_�d�=Uǥ3������T^���\&����á,�-��2/�shĪ�d�N���ڗ�:)��q,E)����@I����I��s�7/�H�h�DdA����D�-�U��F����P�jP��z�o���2�~�D֍���V�5)�8�s<+����h���)\����8tr�sg��@��H���t���h�l</i�)z����k`�಻�x�s�Yd�;fW��[Yl��ͼ�� �W �5�ch3�m�}���B�]�Y�����"*Y��]-lbhz����%�6S%g���P������X��&��B�7�+�����^0ip�]�x.ݯ��8��1G�8*�����j�$j1�cm�<Ei�3����z,H<�H��`CŜL��S"�Z�&�N��T�d4?nR�`�?5A��X
^���H�� ����h�(YX��]���K�_�W�V��k�rv�zL�(IC�+�R��U���%�Z�'g��&��
)��ZSU��M���S���U62���)WHe�8^P���Jb��a�^V�`��7�^U��Skzl��X<�W�[C���H����{l�������9��{(^E3i{zTrz����b���h�3�~��6Cm��U�ܢ0k���+�EC�i(�`�l-	6�\&�>�b����mW4�#�W-V���ָIS�����`t;��J�U�Џ&���H��t�2g-�J֩KU��9D������D3���Ŋ�W<ekPD��Z��11�4��H5�s��]S��+�l�+m/e96n��da t���!ʂ�a�z�[�yĤ�{<ޡ���A&/�`�t����L�#����f����A�2�]�=���&i�}�G^t<�s�~=7W,��G���-� �����Y�8*��f=q<���/�L����g��u��3�p��	x�:�E�4��>�~�~�`:������'��`����eA�%fO	g��ȏ���K��,��Y�)V�(�}�M��w�yK}4Eo�8/���K��3j��KD!y�	��]bR���NS"iɰ9R��o
�d�Cli�H����5��9�c�=Z2�˿T_��E�H���hY�c<�8���~V��so��� ׻>�5I9��S�U\E_{/vZ�}]�
�����O!j���l4y��o�+�BR��$��d�B��>IR��ZD���,�[7_��P?&�ᒄ��y]2rI�]Ѻ$�%�^���AT��IIN{Ϻ$]��{��YJ����`̒dQ�l2畬R�o�vK�1�����S�I�<�����X |��x<&˛�c����!-���mҫ:q�8=Zc��v�9�8_ߒhM�ǳհ�5����+�y��;�y�<y�'���`�"O�����_=x�����ol?{�!
~�m΃�ol`��X|������$x{���sx�.�y�~Q���5�Y�UG�����1��cP���1*���e�Vˏ,���O�7�?���Y��;��}��؈@�wp^���H���H>c]x�z(&�I��nP�2U�k��XG��@��h��SP�u�z���7�q9[�΄�.Fyg�y|=f}��T�l�|�l�e�%������Y�\�6g�`k���b���`K+�r���y!�M��@���f�1�D6��w~�K%�T��Ny�:��)X�=7��#[�|�$�8����G����.H�z� �e6	"�|��$���sZܶ<�O���a~>��;GҖ�E(V����X�"���nB���x8�����2��.��Ni G��(�~��$ûe��$q�hW�a��_��<y.��o��c���`;��~Υ��Xá�_WL_W$��~]�P�_W,��G.v�G[��R�_�0X�_�lR�� %��:�� �%��r?��w��$J�ybb]2)��2��$}_2�%��%��{>��9�%�*%y�xޔ�~]� %{�t���^���N6����;i�W���e�`G����,�
��dG�.�l�>�6�N�&�
����FkE�lx�<�;}��-u]CRp�X&�	>���/_��~RRl�l�����v�n��4@칚���
ψ����#.�ݭ��4J����E8)x:��`ڱ�,���M���<�`Ĝm��T�Z"�d��ψ��j%k�Ő.�߼�����£)�%<���"�4��a
W�e肦*�R��+�Q�G���͇Fh�9޹$9�|�Z��ѻ��E�p�8�4�q�c���v�Y���&S�|�]g+c����,v�R`��{e�Y��g�>��$��N��W\�˸V++������>�;�~�m����r�������K��>q�}��rX�Q��|>B��j�GDo�q���y�͌�������q�)[7��g�R�l�ldlzmj�E$K>�"����'Zd��zt��eڋlf�;+��s,޲M�u�m���珽���yd@L�F[9�}l���@�k�+FW�#�ܟk��?p��pcٜ.��t��]4[3_Ə��G�+�h>���
�"yv�u�"�*��~�&��߉�d���\�V�<U�}( GGR�1�����H�/{x��xK��^��4�A��z���?��Tω���;��:��������i�MI�O���%��"o$}��K_��$�K�dR��@��y�c'�D��ά�gX!�ɂE/9O�pt<�����8~?���Q��M��i��ma���v6�U��dM�S��M�`a�
)��[d�`'ot:��v�.[Ea����ʟ3�f����f���Z�h{���b�d�h���ش��,-NX��=�.�W@كi�͌��푋l鬭��3���i[7���@
5c���S�Ot�*�Q{HI������>)K�-����ky��+��ϐ�$k�g
��u�ªi����1%�	�����p�P����m�왠�G����F_7���G�D��g��y�N��k��|�}�6u��}�~.�OH�VM5����C�h�DW�v��7B����h|��\�v�.2v����I����Kv��b�`���`M�ԁ��zn�}w����gT�2ɢ�j	�
~9j��>˺���v6K�T�"X[q���j�6?ۮ�⑼�-J���X����~�p`�������<�Q���r��r�Ϗ7����Ͽ!�w��ǆ�xF&�. ���eA�|}4�Q�6�#����B�֋�C<�A1�sX�w.��ϙ�<���C.7?h�*���̔e�q�懨V�2���M�*W�r�/6�^ez�{��k¿���kz�Ev�����\�����_��H�3����HV%�/ۍ������e�Jr����kj{�'~]c�SD�8c�WL�;*����Us�Y��>f�#�9�6�􎃯����O�:St�u��|���+~�B�U�{��.� ���}8~�$��    ���$�T���+⣳<��0=��y�{��rs�\1?Z���(j�_�~6� �C�7����q:K^V���}COC���-���j0�*/��%���;���L����,+�ȟ[�S�\_C����c�>��)���
rx*x��T��|������">�L�}/�5�R�_��{���%9�_�:sŻ;���T�)j���[稍+���V[�3�;m�"W���ǲ"H�^/+����w�e�$}�o\�z�}�z�Ӟe�,�;�S�H�/�L����\p�ؤ�v�i����]ϙ�T��3- W�k��SD�8��dY1����}��w*f��W�3 g�T��n/l�I��X]�n����U���DRq{7xny��[����R�^4K@���ڷ�<$��?s�G�eE��ڷ�p��|I|>�,.�cK��:��]ٝ����e�$��˘�z$��o�z���=�V��~G�N�|�ј���.���|�����?~Y�\�é��}SbzwS�U=z�E�U�,��S�U��|d�\��T������ט��{Q���o��AQ�����?���ު2�/��)I�X���{"�����,��w��?�w�
���hL��
�E��Yt�s�_�?xl��81�~Z��IaH�o�]�I����"�%n=j\���?�3�����_-z.��?���T�Ί�Ф"�M�f����i��8�G�U��{�"���`��G֗;)��7�E��&��b�4	�8�`����e�v���bs�]t4W[|�ە�Ė�Gp����XJ�I�i����,�(�mt�t�Ѵ�y��\4�)u����ηs�xV�Ư���:6b>>���HUM���z���|�,�����"���A�z�����Bp��Xl���� xc8�;3��ו�$����*�!	R�/�>��$�R�������g�B�_�����R�_�d���u~���%�"%���K��{�{j/]cZ|�z(�X4= r���8�Y=��u�4��$���V�zWy]��^�!�Hi~���<�~�~I�(�$]��/��=m>��l#
J�?o�~R�~�?J���W��ڈX?���K�4y�f��1����f����������??v��uj�~�_�����|�ЌJ��k�6�;i�5Q�'��8笙�#�do?ʤ4����5���B�#�?�eg�H%��}37�9M�.k��4�{��5㍟ξ{�/K�JI��pΰco�w@�Nl H��>���R��f��% �L����~�uIT�~��e��Zh.�m�,%�II��������C�VvF�)�]�yq4'��D7��H|,��&�E����h��j�[mn�R�h$0��������B'�p6X%��Mh-kQ��j�[m�(��أ)F�(��D�!&��	ɏ�h"��ŏV�Z�q4[F����GS6�p�����Ҫ��S�;L54N��T��5�b8U��6�fTY��M�XM�UT��j��!��e��Ԭ5\�h�����B׭� Q�ؔ{4�`�8�fvQ��J�i�.��R�k�ʹz ��|�q�J�[O��_��{x�m�� ^�I�ƪ��:_n�3���-G��wz�8��l��[4
�X�.::�H�F߳�M�y�U;�_�|�= ?��{���$d�S{��TD���B��9�+�c��(��}�č�B��)<}*�=<��p[lfܱ~^��y|܃�	h���<�i;Ţ�"��v�/�$��H�<~����V����+Q�����d�"���?叆����j�?��Ć�/|��Z�HN,0X�^R�B����?��ZW,�"� {�TY��R���iK�MJ�ܱ��󂣙��.���u9�/H��5�$�S�����F�W����ǔ���4i�9̞.i曲�����ԥ�nB��Uh���mI�i��v����}��>��$�;<6��r��I^�?��sf��wZ~+.�5,��Y��x�%(ְrQت�5�S`�E�*KڰK��������٢X2�\k���k(o~���U�f��,l��XClԤX��a%����ch��(�ҾU���j~�-6?g��,�+kZ��b-vQ���HK�5�)����X\Y�A�(��mU��M���F�����d`Y\,�R�]/�y{��k`��k�{�dZgY\�B�qq�Z�*v��{���xu�v�̶u���EK�,��6w�,Z� ��e/o�+0ν���dן��m�ּ�٦}^g�&/0�����>�m^`'�>/��$�l*/)���d[��8�-l��1,�U���j��:˶{=YC_`�� �v��Ėoҗ�Z	ͯ,3%�|�>Px�z�,J�/]R,iV��o)r/��&4�Q����Exu���W���L2;Kj���0�o]5T��|m޹�_���_I�=4?�v�"3�:��zپ.ق�du�l��d?��L����oy�J�u��ނ�Lҝ�jYJ�Ԗ;�z�Q�{��S�$Om9w0�&%��&W�,ex/ʿ[�2�<��)c�{_�#���+U�!G����<���Jh���eD�`l�>r�g��2^�ٸG[�(�?�ǰI<�P<���1���ͩ]��:��Y�Sݩ�B�w�(�y��Q���t)�,u��1_���_��ox��$��|<q�Ƞ&S�UJ� ~]��S��9�X��·������Z�W���yI>&����R�B/8y$S.�!-^cm}]t�!+��]�&}�}�c������B����+����d�^w��
<b�ľ��+i��[��X�GIɃ�n���#P�Zp�W�+ׄ\�l�X�i>x��,/*�{Q���|/�`�6|f��de�`m����r�A�y��~5v=�݈�װ�Ԭ�7Vdz/�v䒔�}�T�to�-�D�Y�k��a�V&&��N���~&��e�(�6���vu�,:8���{�����-I�6�-39|���_�v<����;f��|�cUj�	rL,ox����sU�B�����E:	��o�&I����|��˱�x����[���M���]�}�l��"�nv����;��(��i�[<)�vG���j����{~d���:jr��{V�[k�m�)H��G]�ఇ�č5�Ҟu����{Ա�NY��T8><_u�W�[�޶p
{x���ӟ������HB�۝�č�ў�=��)C��,���W�_7:.�M�ƪ�A���"���>�5d�A�Ơ�(hk�Ӗ�$hc��|ѳs��xQ���U��9Yn7��������F�%8�_��'DA�۝O
�M�
I���G�gZS�ĭ��=�m�A�ƙA�Q�TU����U��q��IY7V�9ߣ�aS�ĭW%nm�&qcͷ�p�@�z�eG�7������č/�-I�Zu��n�֣�3+jE�֪�o�횋x��-h1���<1|�xz]\{�_!��������!�����C:	�fb2�I�73���=��t��x��"Z%J�I4��ǔG:L�h�[����b�,0VSL}�8�Q�V-e�ʪ-�*��G{4�Uܡ$׭B���w9jjH�*��hJ���2�9����E�D!U����0�U��DƲ"p4�P�h3���0)�PM�o�GS�ϋh娭���jxp���l�t�����3�������IJ6�q׵���L�ܵZ�{C�;w!��s�d���7��ÿ�����6뒍K��G%8�'&y��{7�{70�(�|nP��g�K��x����K��(����H�6z���~�>�_�$q�z~K��}�[��K�X_��q�u��%���e�=t~y�����d�%��pvH�@�+���3�>�돘��y<�y�M�ZJz�Fo���Q؞�N�$�ٻ섦-���g�T��e����-�m��n���O�p����2�9n�wJ{�i��=���T$��t���g�m�9p��G����	7�{�=<I秫��$n��G�}~a��=�r�^um/a�Q�e/����$n�2��u[/=���"�ird�W�[�޶�n{H�u�ud�}pe	�=9n��������	N{���{��d�H���*�ƪk7F]��7�{ԩ[�y��q�O��=e)�Z��    �~�ф/�7_��|Ӽi�O!l�,��wMxP��>
�:����}S��@ʾ������~Є/����*���^
M�ok�o?Rd��O�A�������?&��'����~����6�	_?�2���&x��}�����X�9&�	@���T���I�o-?����/`��x�HP4o���xc�AS�t{ΘǰW~d�W���	A���Q�O���I���CR���X��?�����U�F�M����°����%2^���?Ke����Kf<�<|:�O�7�p&�� ��Gb)����4��H,�1���-��x��p�����;Ol�xT�1~yj��s��/�� ���n���ɷ=�%8|<���	��n�	��͟Y���'���?�r<y��9��(/��m��U�����7�۟�:|<���a��)�	��mm���ǩ���;��4Ǆ'���%<�x��㏥<||����ϒ���?o�C�o�,�����>��Y~���,�����M�M���a�h��e�|�\���?|<*�Z��xc��������'�����	_4o��*���M������>��:6�����X��i���H,���I��W�b���g����z,�u�o������x�K�<�no����d_�!����?��@�^#b�_o��,���m���.�Y������O����O�7�?��#�C,���w�L�"x����7�q�d�t�_��?�z��&��|Ҽm�d�t�O��?Б ����_����M���g�t�?��?�c��X�#9�?���'��?��H�1~��G��Is������|Ӽ-~X��e��?�����?�c�����~R�1~Y��ǳ�s�?"��H�������c�����?\�Y��ǳ�ss ~�ñ������i�4o����ǹr��g^?Y���r}�l���������?����$�����'�a=�D,���b��'o[�%��H������,�<�o��H����G�����ɿo�_�����/��W?��w_�HA��O�&���|�XdA��/�$,Y�����k`i�.��K��-�%��SUv-m�k�4������}&P���	7�&m���`M�K,�����`mmT�]ØCM��z�a��q���s��7��k`�`M��Iٵ�=�0Z�(����\k+o�%l�,����-���d���E�ݴ��f>��B�3��Z�*6ئXCyk��5�,��u��s�F��5��k)/	�4�TWd-oQ�!�jU�!�j��zy[P�!6��l��5�s�6)�W�k�+Wֹw+�%[k����lA��c],���}k �Z����޾5�f�eqլv�[k���Д�����+���uT�q�=�f����q!�K���'���{o�(���F395�k�/��~���5��gR���pj��d�`��.�|S���,]S䎥*4���J�K�)MK��LA�����>S���ɜ�?>*�/�!���[��u�|�R��Lgo�6�HnI��ך���IJ^���.H|�W�<�KR�}�K�I��vK������aO��k��pk�Ť�|&�&�ZF�}�����ѢP0����4ol�T3ew}i�+06�b�C2����C���A┌a��A��䃟��� ��K�xQJ~a������
��l��K�]��YS��wr����x�I��l\RN��0��^��;S��
�^f�G�q����w�����R,Q���*�^e~=�<zm�W�z�sI��ϟX|�=����~�X�`�� ���+�&Pk��Y�]�#� $�A���3*����������")��I��"+[4k�n�m�5��gdgY\��+�g��9���%c���}Fɘs�l�ѵp�eqe�WT��(k]Um��6���32���q�4��N������4��#��t[�����v�:��=o�؞�����U��M����<ly�Þu�x���'e=Z�G:�=��=?=��ksq<f�M��|DZ<%멘&���%E�+�f�����\R��f�K�EJ^G~�+�+�g7xI�����׊䙒xI&]p�d�l��3�]� %I�,I�Lr��$���L����{!��u�e�\�+�^������^�$�'x��Iɾ(�����d�-�7.k��K�xYAJ��e��D5ͽ������R���W�pI|����[��Kʕ%�dSuy�����V��_�w{��~LTڹ�H�s��_ۈV�"�W~v�N���6�{��ق�Ƣ	���C�f$��Gs�FI����ϵ�U��L�����~��A����xp��x��M��i�Z:W�n7p0UX��Ӊ�כ�*MҶ��,i��Eٮ�Z�#z���~����+��������Y�n�fNF��k)n�(�d�A�9^���h���6�I��F�E�^��x��t�b���V&xU�ۼI<��z#��b�q幬���u�l�刋(ix���$�6[��$i�y���x��c�׍W��zjl�.�q
�-�N𨜷�t �'��#��x2�k�f� �?G+)8	�9�Y�IZ�Nt�p�˖�t�f�J�Va��b�>Ӣ���d�"%Mۣw�!��5����rC:)����+��k�L�g?3�˖���rC�,/�M����NAV�t�:����
��4Hz��>�Q���!�=}Ҥ<7��)+M��8;�.��,�^��������$��}&N7�(�D��EMa:��x���z��I�F;�kี�e�{e�7������mU��čeϰg�ʞ����]�{ԡ��Y�V�����u�zS���Jز~nW8w�¯ct�?����nF�4ېFI�cfH'F/U��IҟW���YЃ�n�rѿSC���m?o|�������a�z�����!�n��J�:���W��F�ڣ�aj�� ����x����(�meo��'�%��<	y�|>���5$����1����QÉ���Gn`���/�xq��YZ&�E�8~���u����n�>4g(���
A����P����z3$�Ƌ
3�<O���!��W��ײ���Y��}3�H���\%M5�$l
R6����BHz�e��(i4�$�)P����]3:+����}<�ϣ��3��+�}����te���S�t�gO�	}�,3ۃ��o�e={��h��٭��x��l�3�I�F�ĭ5W8>����{�����fO�$n��%n�l	�u[���i��=�ng�3<+��|ٳ^%n�&�٢����O^1f�u�Ϲ��1N��O$\tֹփ��id�u�w�N���?�i�vޢ�E���z�U�[K�8nv�\m����|���d�A�Le�=�حY�x��I�hk��%'k3�H�ZuuT����Q��%H�u%r���c�AY����čUW�_��WԽ���gI�B���n�
�Z�M��cg�O�m�/::���čEE�;KM
��{����Q��ۭG�f��t�t�э�כ�"݂����U:�x�@�6�=��"�򌢥��]p�ǯ3��xV���TmEZ7��V��F��9&T���_=uDb�!��Γ��_	>#��9�|eF��DA�.2���"����P���Z���UC���Z�J����fh$0VS
]w8F�U���q"J��kb����f(I�b5K�RME��k<fh����.3�GS2���[�(QCp��� �V{4=:�8:�Rf�f�B�D-�S�h��|=�P5X�(����38:��j���j	��T��?�D-V�D]��jp�rt|��=&���׶�<9����#�$}��Ѓ�ꄎ��"�� mO�6����g��]/w��l�pB�E�G��Y�l%/
7Z�
/6�1�^���<96�_����s�3m�wBIWSO�$��-b3�:}Vz>��	��m*��ʄ.��m��U�~Nm�zϓC�3<2�^���C�3�q����=u��rt6�$�١��݌�V:}Vz�Uz�����v�|p^�K�t�����Q���p��^���z/IY�u����\oh���p����S?v��1�
z�H;�����s6�N�*h����M�d���}�    �����	%�&��3$�t�ԗp��l�q�'U�`+�3���Ӌ��{t66[٢����Fk�6\퉘%N��i����g���F�׫�p�|0]����,�leZ4ق�UA��֤mS�a��+/�Q�1(�Tt��'���)����x��ַ�|��q�1��5��9�f��yN�-><�Lb�i��p�:��E��ə��q{ɳ��/�s��Y�oF�=�`�9��GM�w^�����®~G|:">\�|�!�y~�������B򚓂��M�L�tKV�e�^&�d�z�<���^#�[Rނ��	R�Zj��D�Y����f~_��~P�&)?�m��f�n?{z�n��2V�Y��ޔ&�%o�!HM~/���U}�ׂˎ&�5��1�~�R3��3I��3��4a?>!���/�Y�f��������~T����N'5�y�m>XakZRq��It��*I|�l�����l-{����|���k�`�A]oq@/�ZAۼ�V��go�s�.::�Z��-���"�`�^��o��=���Q.�ql��jå ��=ܳ�n��h�z��-�S�Q��R�#|��������x����j���䱜ߚT<(~x����������W#yR��d��?�G�/���_�|���?�;s��9�>^��Ge�X����*só�#O�����I�/��E>+����h���ȳ�ˎ����kx���%~r��}��W`�g�W�_���I���ϊ7��R6y��h���)��5(��$�"5�����ǟ��9�&�ۯ���ϛ��&����o�7??ZP���ۢ�Ͽ{�o=��嵗�O-i���}`o�ϛ��M���s�x�i0z��C@�Mң��h�7F�Ѷ�:Jl4Hz��sC��G��7t���q�>z����G�:o�"���h�
m�����G{�x|E\|��E��%mk��~��FI�vG��Izn��H�n�o������>�����6A��U�]�7xS�h#�� qc�@T�m�~��;�t�Ld0��(?GE)��lB'I���ohڲ�=�3�.=�r�����F�rŝ��n����g���W���%74
zxI���4�u��^�,=�e����z�������p[�S�r>�.��v��c������g��'��8�'�����h��D#�/�&��%�\r�(���'9�r�;r �K�r(�^?�l���jl�Z�mէGRvK�����h>�}ze\^o$�*�jףu=dzmP^k�u�H�K�!�(� ;��-��ӣ��ϙd����Ə�8��N�*�{�P��chB14Y�$6�J[���-Oң&��Iحɘ�bsE|/��ͩߊ�/y��*na���Hr*�P,��^SW������j[غ"	����c~+�d>��X�"n�9T�x�!���.��E���%��Ex���%���\%��U��[��#�����~?p۩�!�R�5μG:�d��W.s�K�KF�d��e���$Y.�+����e����z�)/Y��O�1|��KF)y��oH�ޓe��%QI2/�AT��D6`:����%�.�)�{O�R�,UJ~ap+MI��딬�������=G��O����m�NA����%:nن-�=3=Ɠ�myŚHY7:���/{�٦��_ě�M)�Ja��m��|C���g�xGñ��h8���f�
7V]����խ�mŔ�|�x�����FL�p��-���ݲ�[�gՋ�c3�����oU�'�����)��G=��l_��E.�$qۮ��vu������|L�l[+��)gxk8�����^�(.�Q�Ʋ����щ�v����n��:�����^���2l+gq-����iۈZ�F�/�g~Y�}���plgu�l��z���x�=�.�n�lg�H��l����.��o�����g�7�sC��a�7��������ik=�3��'|R��I��o��g�[ۯh�m�Y��?9��7�[�o!(޸7�M��ɳ�C{���o�H��%���/!+����C����,	�|~!4ś�/�\�Jφ�T%gs�	�F�r���r~{<��G�9��=U��4����mM�$�i�Oh��Ϻ܄�[���~T��b��j��N7&x����ż��/��>�'x��{����%n��>�-C���g��G_��f���C�����50��E�I�ӗ�1^�ħ˩<*|����=�G�O�<�Y�=<�9ߣ��l��{x��1�kP�����q����ˎ��(��[�O�����署��+�J�Z�&q�Ԥ��z\�{x���u�	�
7�=I�X���g�O��r���k��U���|��c��|q�z���a[��7��ɓ�m�]y�>{��������7��'m��5��^�#�?��OcD�[˟6yR�te������ϺO��x����������G�ۖ"��sL��O�I����i���ɳ��ϵO��x��M���a�>�M���}�|£��I���Cڴ�s�ӄg�w�6��U����=>忱�RT����X��߁4�Q�����o�7���[˟7�g��<|U�����S���������T�k��i�'�dƕ����X:����{�A���n�m�gY�?��8���5�u����ȣ��O�<	����Gn$����Wk�U���Ko�x��x���%8��݉,��ՖX�,��Cմ�Y��;T'<i�m�,����,��7���s\��a��`ȷ=�,����>*����u<�P5�?,����'���,�����,���~����R��yD���h����,����Y��y��q�cy�'o|d�p�m"�|���s�>"�}<y���x[��e?���X��g������x[���#,����`�_����)�e�j�4����ӆc��?||�q��t��O?���;��u��b���']�����y�Ә^�E��W8��c�9&]�@(h���"�=��i�����"���Uk���o7��);֏�-밇��#<e�=<K|�qa��=�Uᶐ���c�:t8�q��q�z��I�ƚ?��|Y'1V�h����"��!]G�G�MЯ�W������y��~S�����ĭ��N
7�\޳^�����m��M�6�)��Q�ƈ'�*;��><�u��I�Ʋg�ߌ�C��9_��7��ӚS�h�Gm��8y�{����(�qJcNÖm�]w�Iھ��M[���u��e�
���#�m�%��Q�w�e����u��(q�����N{x��1hK�+{��_G��$~7��$nl��pP���*��G���s����u�I�ib�l�jNgA�S�s�lٮ[�n}�[`������9�p��#�~qܳ�<UG
�3���q��Eᶠku��uh�1��jC��,�0�a�:J붚�У�技9N�;�nl��g���=�nvL�8>K,��(q�C#�YG�?��h����s�$n-w��m��Xn�.���7���qe����$>N�񸇃če�y��u7 �8I|��l�g���n3�(|�m��=�=�n2�S�č�cܳ
��;��uw7��y�9�1+���ss�h�6����?{��9=9�S��~�ʾ��h�m�F�M�Y���>��xc���xk��M�U�7�?�?�ܞ���#~�z�PT���l����Y�%G�)~|ޜϊ7��M�j�X,�����A����Q��݋so�������E�ş�e#���	���}��#��'Wm����,�nvOO�o~���Ƀ��_P�����vR�����x��Q���k�*���#s�şg�^���)�7���_q�>[S��ci���K���<�/����n�=��f��g���	�o������M�,�n���xR���������5id�������3>���&�g|����Xn����bɍ�}��?��8���7@~�p�ς����%8���9_�}��/��s�c�%���+[�?%�� ����e9���Wbi��ӟs������9O�7�_����E�����w�?�<�?G�!������9o�?,�᳏�<��    z�hƓ�K�~��Y�V���g�xf|��Y�5G���Ǔ7�K~<y��,�qwzsΣ����?������?��lW���ھ�����?����#�������,������x�ivƃ����x�qu�����.��NNo�y&4��ӛxJ�%)3R��{�6�,5m�؛h����c�ߪ�J�n9zES�/�G�W4Q驻����J��oޯ�%���K��e�G��<�M�h��o�ӪDy�&�(k��M8����5(QR^O+(�o4TE��B��n�^y�����DyHy{T�J��-Z��<��PU��8�����!�l}
A���r�B��(��7�G����=�����P}���)l��D���f��QR�|�rzz�����.=�'�QH^���z,Y�x���N�� 2��g����D��ֵ�H%�')�~��MD����X:ml0��z"�J�{�m���`��-Ja��� ��;z�D�5m����?���_x��BT�Q�����%=��7�� ���L���!j�'��F�;�ޣ�K�t��5ӗ
�
M���-;���]ę�F�{���m;e���w��Ej~c�+ה��}cèL����66U��m#64}O��7̨�R�V�Sᚲݽ��T�Cɺ9�=��5*�EY�B�7�D�QY���S�z�����<H�dk��f���~�Y�&o%���� �-;���9�+��������_��x���'<�ѹr$$�<�'����+Y�䵯�M>�*$��O�]����g�ti�g2�&O�6�yN�k^ۤ����ٗ��Mt�����ǫ�|ӷ�y�׿(ZnD���i�#�&�������4���J�i"��́O�}�N<WH�0�W�7��Y�ʇ�O�p��@��~�z㩻�l|�_=�J�5T���Q���EECm������R�=
�&x5�k�W]���(����J���QQ�,F��Yg~nh�΄���Aj���O3��tk������e�~�~����e��|R�.{��j�ާY�&Yj^ͪ4�����{?"���>K��|�4��5��̍�^@���1��\�;)��s�}����F/Yj�~�n�����D�Թ�n�ޏʷ�fRs���<d!v:�v�vҩI�7:��,\S����4�$��&|K4!*�����D��m�u"�2��n��r#����D�
Q�j�X���$S��s+QRj�Å\�X�E%ʷ�EA��G�����e��>�(��E����#J�yڴ������a�֌sMg�Sd9��"��ڞ��~�g�D��J���S\�2�P�*Q^���'�,�j(�����O,���ܞ�������O����ؖ7�7�w{�BJ�6��d�E��uV�?�$�9G����Glw�:��I۞�=���l/|�l��&�P�Tܢ�Ӣ�S�h�ߩSbU���D���}b�I�����rG�������m����^��8�{����VJ,՘_:�_�k״n{�h�,�m��D�?���������c'��-�%Y�5�$66}O��5�����c��d%�&�Op�8�=��Ҽegòc��D�~[�̊�^��޻_Ă>���!o:��_3�������EWA�/�����o��<�ϔ��WK��(��͠s������Z�9N7�\�_�0ǋe�{e�Qws#��A�F�5�᠜�mE�{\�Q��^�㳻X�x޳^��Wi}|��QG��o��f�[�֍U�@��p�Qw�͐9N��|V���l�x�s��Y�Qw�ɑ�C����r���{��7�=�s�G]�X'���ݛ�y�z����C����z��C?kx7m�{8��(qc��uw���$x�H�cּ�����x�|�.�u�����N�!�l��o�? ���P�7�/�{o��5',�츒c"Z���e���G4�T�^k�����=r�$�B���z����R`elM��/y+��'}����F%j�'ODY�ӑ��\��<��6Sx������92:�Q,���lt��y��-��d����C2�G�q�g�}�QH��^��� $�b�n �$
I�KqK�K�s�޿z_�A�f�:1m��R��!-T��W�rM9~��w!x����k��5�����)J�D���'pMy*�z5Q�'��XJ�=OzS���|"�����Ϭ�de�^��5��{�i���~��z�`���豦���h+>)pM~���IQj�YH�j�,;�y�:B��|�h����)���8���7�(K�o���5�e�\_�s�F�~�6��+�5s��r���g�J��u�19���s3���1�{?*ߊ�LR����!9sͻ<�A�H?��U�Ӊ>�*5���KG(�?�\ �j$?�?1���s<���	I���"�=�h~N�-�u�v۱}~��E���?��ߏ?�q�z��I�x�q�){���zݳ޶�$>����[eO=����	��p���=����O=��9V�EM�I��&�;���5q�n��q�6H�&bF4
�:z�H���u��p��Y��&?^~��.�u�z۪��#U��č�A��~[�Q9o,{R�m5�i�z��_��^$nu���m/A��u%�Y?��\��j���N�x+�4
���!��h�M��t޲].Zf�*qk�7�ߌ#��#pcُh۲�f��čUW�N
��,�Y��Xu=���"^%n-{��1lZزޢ*��ǵu��G��%�϶	�qR�k>��=�nv���7�[�/!��q��z��|�v�����x⸵˔@��%d��z\	=��z�"^%n�%4��k>��p[���=eJ�QWQ��v�Ӟu�֯[�,��7$��u�W�!^9Β��x���� H�UQZ7V��{��o����'��,�&���1��+��^�9_o��&��w���4o��⍣���m�eȣ����g���<)~��x�gŗ��]�Y��㉋U���6���OA����v��{�'�I~���"���� ��o쿉����#e��b��k�*x�����o��}㼉��?<�4�Y�eG�n�I�v�Iٷ�_V�q�'����*���Dm��A�W�o|U�,�����Q���oN���O�|��Y�5O�W�[��=�%h���o~~�D�s��q�a��뼌���d�k��m�b���Y��1�E��ȫm�*x��%5���q�5�e5��,ᜏ�7��� ��#Ol���xf�e�4ok?�� ��%7�榐9_7��ǳ�����Q�ϋ0���,��P���d)���Y�I��y�aYuI�"��<�W7�&xk�U��x�7�_e���}[�U��l/e��o}~V�����ʾm񷲄O�U�[�_+Ky�x�� Gң���� 2�Y�U�����8�_*�{��5s�6���?����?,�7wq��&x����Ӿ��]Y��uӫ-~Y���f?X�����O��?���PY���������e�tl��,����>>n�,��Q~����I������x�,������s����es�*�6�,������xٷ��,���Y�%��Oe��'o|~��Ǔ7>?X����r���?09�_��xzn-�9�X��+/�����O��X���0�c�t�W�,��7w����^��<)���/�������g��?�xڿ)���g���������@����?P���'m���Y����u����:�^U����i��x�����y�`���?�������u@��$}��#���,軬͈.[��E�v\m�7�k��x�����ر{�q��I�ws�����ߏ�#�*�ƪk[x�Q��Q�!�p����5ܳ��p�Ï�;g��uT�������H�E��#X�+�or�I�d�������k���_�V]ogZ��`*y;���lR;���m��Μ��-U�<e/�:_9�^�)��p��FedC&x�:�L^�A�F븇'�O3A�����c�(�W$l=.�=�q�:H7[֡G�}
y��č�g=q�z_�IZ7���cߐ[ǋčC%T���:h����0���u��F��Q�.�i��[�����{�U��"q�h����.�MY�9��    Gݵ\b�A�Ʋ'�8��,�I��"��Uף�_����[ݯ���]�_�}
ھ-r��R�/�/��@��1�P�7�PQ��I�ok?b���-�㇪�o,��?��������r�g���2����o�?����_r�O.�oUX��*��6���	C~�~a�G�Ϗ�����7�O��I���ϔ�1�9����>��c�UMF߾��*��&�	�}s�tL�	��g��E���y���qsŝ�
�O�{������?2��o2����M:o,{[�[���uC$>MJOpT�m��o�C�8�>Q����K���[۽���7�aFx
!��=��	�q�k�Ǎas�(�OwbL�pc�i�Q�=x�ë�M�o
�<�R�A��~�=��w�Mp��t+�G���O�V��=��o���e�{x㸹� lY�u���'8H��p�{x��1h�8n|Hx��x�cM�j�2�?��x�!{�#{��ϫon~�M�I�Ʈ���?�?p<p��^��x�D��;���;�/;�Y��'����E6�Q�ƺKi�>)����Ş������}'U�[�o����o�b�w�d�mc��M�I�o�?"���{Ox��E&|�����?=/6����a�~f�W<<��5o���6yR�7�of�w�}����7Wm�X��x�G	���y�	��yx��U���o|~���ۿ��m�ǒ�����W6���7���X2���`�?�t�/�MMxP���,�����'e�?,����x��y۪�jܟW��M����5<�^,����x�}Լ-~Xn���&<m�����}��(�8�����o���#���>�~y�&�����r>o��ȲW�M�M�,��2�����m￑�:\��\��>Kv��W6&<����,���}_>)���c	pd{"�x�c�?��Ǔ7�_���徬	�o}~E��x�o�?��xݷe+?�{���!���/�uM���?m�����}W�(�j�j���=�Y��ǳ�-��Cd����܇�O�<)��X����&|Q�q�a��?�>�����Ƴ�����:&<�?��;��:����?��{�>	�,�������x��/����n�M�����/��Mx��Ed�$G����c�`��_���$x����?����(��X���"���g���,���}So|~���od�'��X�����,����}k�(�Z�U�������y�`��}�����}LxP�����O~���	�o�,������H��?�<���?,���O^���?Rp��$G�%��Gr�?"��xT�1~Y��Ǔ����������~0���&��x��H��Ox��7�$G�"��G���G��Mj��W~��3���;<��|���B&|� �H����Gr�߁�?���>*�V���>�����X�#9�O����xk��=��?���'<�����MxP<yT���Y��Ǔ�m�/�?��4��F� ?��3$E�-��ϙ�n���['��a�E_�&�[��\��������G��������|��1�0��=�n�˘ᰇ�ĭΧO�qj�Ӟ�=�nw���"q��:��ߏ��Ka��ԣ�v�u���ϧ�����4r~�=�G]��E�ƠMu��m�y
#��aC=�n���p៫�39���u�id}��h��=�؎����[����������x77\�Q�<8p��pG��˞���_�ޣ��Fg��χ.�uh�o�7�~	C~�~a��yu�M7�4���O���?�?ϛA)�|�<�5����r�k|~�������3î�x��]qh��k�9~*�_~�U�I~-���_�_����&�����@��������i��M���;��1�����w94]$=ل1���6<ǣ�8=�>>��E�ᰇ��$>��1�I�ƪ�w{i�/�����e�!ޔu[�� qc��u�=�8H|v�i���������ޣ.9�.�[���M�ƨ��ez�ݞԚ�p�p(qc��g�8n.�G��!�^$nu�r��e�I�ƨàp[�a������ۭ��n�:L
�l.�ᤜ7�|��z������޶��ʞz���
1�A��vO�q{ٓ�m�M"��Nt��u��4��"x{�Wm�X{m����76>�W��)f<l��'��w4�xR�������3���Ƞ��7��F�ol�����F��7?33j��~9)��6y�Yz.����
>?�?���_�&���[@�V���'��g��6y�;:g|���W�����ڿe�#tƳ�+��O��w��xT�lG�O���_�6���:�VkQ�����m�[�߂���X�ݞ�� xs�5T�����M�6y���7Ke�{aԲ<ʒ�ya�p�?%�и��o������Co}~%����I�����j�'���7�Yx��w���"_o�'��x��3�	�\�<���?�����x34o�o]�N,�����/���c��'����g|U�m�L,�q�߄gY�=�6�Y�9���9�o��i?m����?�u���!�d���|3�
���c鎧}c��|����Qoƃ�m����e3>)���aI_�Y�9�=N~���_7�7�[��|���+���ި=�A���Ē�<�'�럥?��kD3��G�g�t�?$����o�7>�X��#~X�������?��?БH,�q���'�ۏ�?�7���?�o���U�f|S���X�����~��~,�q#ތ�MO�����c���������?,���g�ݜ�U�@��E�	~�E�	���y�`����f<(��X����n4��I۷���x��?��<>����c���}k�7��ڟ�?������O��@G�"���~ uƣ���/��@G�$�����,�������������)�?,�����g�������?�o��i�Ҽ��Y���䴍�,�q�ތ��7�߶���`o?b��׍v��Wb����f<*����?��\��Ǔ���$��?��?^��~��������Gr������M��'��<m�,�V�x|�e�����_۳�Oy��O>x���?|<*����>)�?,��V�<�����_6���7���?|<����g<(�����Gm�?,��V�<)����C���~��������?1��Q�k�=�J<&�$	�kA�9��If&����>�2�2{%��K�d�$������<�Uh�6G�f���:�qT`^vo ����,�w���Ά/���W<�BT��[��x�l�덨�NYg�V�7��:��D�R Co���
�ߊS !*��n��D�������]�z#J^Q֣���s�� D��M1*O#��{EA��:u{����G�m~��4�Y�~��c���X��7d=��(zgg%�����S�2���O�<�F�'�����z��� f�(�D��t>ѬD� }T�S�L5�=�HU�^��ctbJ�`oD%z��l<�)*�o�|SQ�{�B�;��X���N����S�pJY�~�N��Fק�D�աQ֣��ާIJ�Z���4G%�'}nQP���zC*���2�Ŏޣ�5�υ�&�����U�����e�����ʞ�Z�5<k�g�����<���Fר�D�5XuR��O}�E�������:�J�����|�ǖb!�Q�[�l-��i�>QО��w(e��O��x�#��z2#��������N��F��|�A�G����(Ԟz�%z�wD����h�St�iE-z�͢�����$�O+	Q�4u?OE�_�2w7�U��uo7��G},y�!���������u�`*��4�����j�GeRnOI�~��婧�E�Vn����z㩻NY�R�'��ia*��j�BT�A:{T�D����oD��_�Q�G�:�Q� ]CV�_�j(7�C��(�|�~F���W�㍨�SP�/��:G�qZ�ѽA�M�?r��6;�_m�X�b��    ��	A��f�>��nM��O�j�����Ʀ��ӬsM�v;6)��?zۈ��?�n�8�ӻZ�f�ߚ;g6!Wig��if�q�vuO�3���q�m���|6�.{�in�R�e�J|�~�1k4�'��=����1�f��ׁ4�k����n#������J�f�G;F�IR��V+53�t�ے��/�Y�B}�~Ծ�'��̔�f�k��������Lt�Ju��c��߾Ь�4�r?X�\����F������m�>o�ڠ�EG
�v.�(��E7xڳN���,�=�X&j�z���iG�x�����I��T�m5O=������[G�Æ�����Dʺ���+�ʟ�����WA�m���c;���u:~Ԛ��3\����9n/y�������t>�k���gi�8D碜��k��Β�'n�^��m�d�g��E�ucw+(�ht�Gz�N7Y��r�aU��ڏ�A�'�cr�s�,S�?xL�B]��$��T��YH�m��� Y���G���:���l�d���GlM�=ܭ�ի	R��g�j��dk��6�ޅ�r}[�^�6�ҍ��F)+�y�� Z�h|�{�꽇��7��:��"?%�w��(�x#�-~.*�?�f���=���ꥑ��F���f��_��_��I��w�C�i��Ŀ�	R�]p��w4{?zdK�Ϸ|l�4h�Ԝ�4hf���6*sM�
U���������m[c�����_�������R���7>���G��Y�����F,aV��h�"5� o��*5�񊈽�o�=�ɮ��h���� 5�[�[�����A�������Hj~c������T�f~�嗲w�Ku^�n�ޏ�.��9H�o�ſ�	��_�O¹����^�rn])���C�������=�Y�u��5Aj������R��mF��f�4�|nⰍ�4Ii�1��g�������f�r���ݫ��E����f�4�����pAlE���i�g��>[���h�J�yݒ&�5G���5�����vfɖ�:��9��@UЉ���4����l��H,Y�����ȵb�8�O���g�z��n�L�w��d*���)	H2u-D�"��H��%�&�,C�?7�nW��(�Q�i�ܕ�Z�#�ϙ��>�~�X�c�G�c�����Ś�����BO4{�� �g��i�O?ܙk�^�;»qVhV��FtGQ�ٰfx��э���Q�����8�~��R�X��,�������ޝk�{��|�� �k�n6���`[*�}�lt:B7`� a��I.���_���+�x���G�xu$��D��_\����#�'�Y�������}����7�`Ȃ/��] �����5�ҽ͑��o���\_�����#��,�d,?��т��7����/�W�_	�������Ι�Ϯ���]@���e	�k���_[0ֿ�����/F�^7yg����w����w��0q�³�Y����'c�`���o������"x��*z�_L��0����q�[3ܟ��[s>^����'¯�n�Z�b����za|y��/��|+z�W�k��l|t�W�?v��W]�パ��O�q��O�|m���J����K�/�W��X)��_�ş�����=^�M}�~���H��ґ�1~0����r�������|�K���������C�z�������[�������'�/J	y��������-_���_to������O�J��'x��t����Lu����K�	�-?"~��5�-��'<��]w�w�������/���^��f�3����������W��	��?��u�W��ӡo�w�[/}&|A�b�1�+-_��Fxe����_;��w�&|@|>���M�>"^��������������˯��.=&|��ߜ����^�L�@���˄��W�����?P^7��'�s��]����h~=��ڣ�ew!�z��="D�q���ՉK-�����g+!�+7"6�^�"d��\ �z�Y*|�,da51W������r}�x,|l@��܈XU7����Y�v�J��[�]񍔫�Up4fy�B�+Жl$�"�!!V�� �r;_���Q�Ăb&+�VSn����V1���W��ڂ�܀X��e1�rcNLH̊6:N�Ɩ˝��O�x��ba��ǻ�
�f
�8Q���S���ڕb�`�#���T���z�O�����uֻ�c�b�+��i6�u���W�R,��?Rt|2���hL��W�R,_�|%��������8�K܁܄���>��������H�酘	��僱������^�\���Q�7[��^����o}�:�◧B,��'c�`,��6��O�Z|ut��ǿ\�߫��d�ZL�r�m�Z�նǝ�8����e��j�qD[;.���rw���X ���$�9�G�������Vm�[���ȩ1�Q-KC��b�+��������:��p�ʥ��iEZuкW{.����W��<i�Z���޷��_n�E\�����%N�v�oM,7:�:�������n�t�h��9�i9��Oݟ�ҹe�3�A���p	kBO���b�tfʖ��Tv�4��f��%�X�(�=���L�Ĵ�n	�{��ӽ���N���Ic�/����&�E����g�䚮!/�<����ylRo̝��v%��F�w���K9ܧ 0�wJ����=������_H׎��n}��%�P�^7�f�i��=�Õ0�j��z���}�P`�s�unxJo����q���"zڏ&tCt֕��z}ٳ�Mh�ѯ�E�����&��	�f�لN���s�OH��o����}/.��-��ڴOh��;ettC�ih�#w���	���-�%1YKܒhBc��v���bҬ����L�~���Y5&��K�X���z����5N,��g�1Էr�3Է���#9N�~������R �a������v3���1�63��
Z�6��SeV��4����{V�?��W��g�3�,�Ox�Y��'�:,p4tch�'9&r��,�Kce�B���v)b\��K	���]\w��.e�ny�
W�x}��:�/5�+Wx�0�tx.x���6�҉��h��u�͎	�m�]2���+c�iڽr��u���O��.g�A�����T1E�@p��#ƕ�DN.*3p���L�\���[�j���Y���Z�����<^Ww�pQ��pQ��[�����<_��b�)�/O]�W�6�p��3_I��IA�k��<u�q]F\z=]��z�<�[J	z��ǍxĥO��&x�x]�u#_��#����+��eÍx�8,7�G�q��Ko��?{G��Ɉ{l���@�|RϺN�'\����u�q~���F��\�{�5δ�v�������麤~�Ƨ�&x�xV�l����ib�s������2�NSz�8,'#�0��q�#�˼'��˧�'��:�y�:e��@:\l8��ϟ"G<s���������m�xK�/J<׽6^��o>�\����������}�CHgD?�&�C��a�:�{��g��d��9G����ۄ,�1=��fI�5�+;"���?	��o��w���ĝ�-��^�1簖��@�a�z����9���Zh��Ň�a#9����z���0�_�'ϯj���`Wr�n�Km��7[�J�r�'�px�Wp|�W��j�'��x��p|�S�∞qt)�]^e��D��9?3s�T/=<����g�@R����?��~�XozR�׸���{����½�@������Q#7���˶tL?m����?w�޳q����z�GUiӧr���!�D'��8:���i�\�r�Z������5$���̒��O!9cSl�(��$H���_f��ۦ�1w��R� g1J��(��J��e����<뻱W��^Ꮡγw�r�1}�~��W����l���ѸF,���)�	5c'��T��QjFr����3DW�5Jc���͡�ct�c�}�£-�p��N�-=0�3�l�+>[A�Ukt�^�~�}bh�{�ΰ ��������3��m��7�H����{9�M=��Zz�׃ �X&�$�l ;   #���KdkS�e���F��λG���A��g}`l�lI�^a>�O��������I/�      �      x������ � �      �      x������ � �      �   �   x�U��q�0�P0.\đ�#����!׵&��Q�-e��bfRV��/�����]�IJ�"XI"|`C�U�v�J�AZwˢ8H�2�j����f��qZxpl����cM��t{
bH{h-�_�8�a~�\|��Vr�\�vя��#���v��s$�4����H��VG�őBrY;�}޵���#�Z��\�@u�sa�����}���y��gu      �      x��]Ys�H�~F�
�LGO�e��o�==��x��<�n��h�#������ff�!	��;������"�1�/����<J	%N����:��u��U�iu*��*~�/�e�Zܶ���b�,��Uy��/t������Y�z�v_���\�B֚^��D6_¦�#�H/�/������ݵ�ϸ�|�lo��~��ߔ��U�����BZvu)�LV3�
W)�W�n�U��-�%���?Uu�RZ�FJ���
��a����A�c� �s�:�O�;��/���prxj�G%x,����1|����,��i5Ӻ�N�5�$���'-�% ��3;�)���fR����4 ���4�G4�@��H/PR�(��|�/V ?�8[l���D���%�gF� ���	�m�f��	��(P(�L'wv�!"2�|��o�(&�_ ���]����b�{H��*N���L��V��eeI
�(z��#.�K$(���s3�f����[E2��ǕikC�R�m���D��f��~7/������Y�W�[��o�+���R�J�_�U�/D5��M������J�gp��Z�
���n<@�V�*ĮBuL�5�3;5=�
�j.� �~h�N� j5؞S���}l�`�/���}�	-���_d�^\����Yy�.� ��g�Mʻ5�*��r�^�?�iAQ�U��:;�S8��m՝ԉ�|���cU�?�/7�v���N+f �Uc�&�2����L�%�)53LCE)�H�3A�QT����ӿ���d1l�-��aU���`F����m�~}�ޑRY;Tʇ������u_+.�z;��5j����L�@Ttj�lS����JX���&DS��oW�9�	m�4��?$��6�Z�2��&� kǰI&\��5�w�mc���~�����N:�n6�N�u��'\FQ!�hĶ���<Z2.�Cہ�D4� 7i�_�&e���Y93=w��舜q�੝m�<u����͟�ݏ@u�9�9�/BI�
gm#�-+%M���3=��嘞:"�@T���=	�&��Mҧ�C��������~���&*��;'p�t�!�!��V��_"��xrJ?�P����YF�:��D�v��+��枌���z��sǡ������9t�
6��<2$��c0����R��W{�K_�ͪ;����l�l�˗���9���r~]j�-/�PdD�~qY�6%��]ݷ�-:�n�� B���W��b�����\".8�� �7�{9���"��=��y#�/��11�y��-�9��My����O��|�K��\���D��c��?jY�cE��D5��"����,@�Q���Zh�W]G�~�����ټ� Z뫛r;q��j��Y	ޅ�u���!u ��L 7D��gu�� E /5�
�(a��%���{t�������ee��n�����b�.���=�$0x�����R�:	{�l�� u���X[�0H�l%=,q�_Jiu:����{Jՠ���1f|Gy�^9Y����n��%�T�`�t��!޾��s|�\v��gD�`<��D�a��%�����ٚHh��;�&M��?//ޕ��O_��,[Q����xv�ۜ��9���H���혴@"Q�s��G/ɵ��M!Q5^�0#�F_?B�d3�����EJ=����]R �5�GqR�0�b5�>.��pH4�Qa�	S\�.�'�Ԍb�^�[���:a��.��ЬI�t��� �!���Ju����C[�ڢ{rM����ՖL$�͸��"�<e�%L7i���;�p.�����7�<�c߶�%��(K�U��"ZU�$%<F��?sl��r�D3��Dird�a�ۘ���N�Tt����r�u9� z��w99í�7d�!dظ`��1]Fvz�pz3ܶ���%x��4.˭����c��3�h\kS�ʰ,w\p��0��JHYnCN2�'�g�3؜}�!�mc�{�.��l� D|��/�%*�$��c�4]�*� 3@��]%�YJ�\�e�xLH�P
{z%뙢zP�DL+�q��o�@r ����z��O������o���Ze 	,����Y4�-▾����rD�/vT�e��ZoV)	YޡTmw��b�����c�o�s%gZ8T)P�%cu�.���buɲJ��*I� ���~nV6Q���鋯����������6X г�7�L��"��艈�</G"��ց?�<�`,�E."�]]-���9�WS�w�/�APfנ��2���q`�a�_����Ġ�CA����p�S���6��'��g~�i)�*[0+�I�l�5=�Pu�����F�ɈN\�K�	Ԙ�3�،*�$; �	$�X�j[��T���� A,@��U^�*ǰ/z�[+�i�Y���T�qFC 0��]%E*��`a�{#-3ͪ�j��z~�l<0y��;��ǝt1&}�n�-�q��ܑ2�t�`N�M�=߄�f�QW��*'E*֦Enn1�d���]aO�ɕ�g�!�e�,�Vwa�k���SV�%�O�dm�[zY
 2}�XE?-rR,\�� !с�l�ۃZYV539Qf�$]�eL�)Ժ���!4"��KDO����Ĕ�ay[%��:�� ��#,]e~��T7G�4u�5$;7���2�Lw{�i��BR*��|H�RRSv@�Ū���G>�PA�!�g�N�Ł��M3�3�@b����q�[T�MHi���@�ҟޯ��W�����1�
GY�ψ͢�	��HC1ӫ��O�<Z�Ǣ,��|����t��o`�ʗ�k�"3��B��X_�!���曜�B����k#*�2�R���*�Unu@U����f�r���g}Lϑ��w��F�a�\��g���(�Xt�Ҫ�;D�����U��a���g�ź�w�=CK�á�NF����59�B:�Fh["SrA|�j�]�s�������au�LOX���gyR>`��7����}�G�� R�+x��%��ϗ��{��r��,
���V��x���A��x���\�(ϝ���v��*�a��7�v��:�:O�9A��=��_T��C"�=q�gI�=����a{����E&O�0˪�u��{���^�O2��.�c��L�=����T�Pb�je-�)3h"'��c��|�&�㳧���G�@��B4%1h��h��<�xb���S�\Я��$��n����V]���M��2k���`=��jiC�u��1��M\�l���.E#4�vl�����9=;�鍈T��ɚ^Ve�vta�i8�ܔ�h����kD~�Q.t��\�j�P����!'�{��nZ�9�� �,H���S��	��/b��AG>�-y��6Ԝ%V*]�^�W����g��S�бR.���T�|����Yy;ߴ_�K���������XF&�'�|M��x��
����5��� _a�3K(N�J�>��S�簵!��)%}N�9�6��(��Ml	�d|)T��D�O���<�ё�0쌭35���~ʈGnV���q��*� cB����!:���R��=H�~�P��P	��6A�ZtCi�&�Kh���+S[�y��!\n��:1M�4����/��]��w�l����s��8�Cy����zZ��X�h��t�H��h.SC=R�.Ѷٓ�P�۫v*�<�(ax1�o&����D	��(m�쓨�CY����,�_�̛�c��v�Z-�����zl��b���������<Rʯ�oyTo5N�A@﹨tu�E�"��o:�E��դ���N����%��1��>Z0���� ��Ӽ�D��fƷ�f�^M��9DX$	}��ɚ����bsq耶J���:��>&�ښ��;tVf�*љx+U��5�&k��ު��ԭ�,�:���n���k�?/Y�b���P�~���a�֢9ot,x�ru��v�ƸHfʌ�����-y0�>�s��Y��F<-cc�%4��R�sj��цO|�$�GLl���BA�)qhg]J9	ˇe��I�G'    X��h���34¦�J*e��I\�K$6�f���ö����t��7�������ss�.��rG�����l�W���95�S��ß���F������@9��F2}�-&�a���+�#P�	���30E�_��I��jboA�
1�f0\�`��̗ �i��	G�>#MU)���Z�V7a:5�����T��R�	��Hj��挬7#~���ױ�,E��1R�*�Q�'}dl���b�N&)yO.K�h����d�h���.t#�r����\�0��K����̳Kl(����hX�#/C$Od�.��{��ry�\Nok������e���ޣ'ښ��@�r��p���"�P[�mrX��]H�X�A���d�q�[���=�\�,�����LW8��$ɡ�:���[���*C�/h��1+�9N��X[��I�rF�a�0�㉕Sy+O�4�y�4u��ݤْ�=*p�-8�Xu���"��j��4�kp�۸F A-,bj�SZ�vJ��gMy3#�4x�]��v�O���(n #�htj���͗x_�nqS^�Ϳ)�f={�j�����Y�#H-�R�.�:S��tX��L��ch�,SV8��QF�ӑNNx�L���\��=�F��`���C��[ǆ��x�	�'O�RFh �ϣ�*ϣˑK��]�g��
nd��������ۧK�E��.�CČL��C
t	���XI�u�6t���v8�6�xtXy��?��Ŗ�ظLQ��x"�����d1�й��L��DL=.b����X���&�ʟ.��p�Ez����Cj������^�R�y�N��4$�c8 ��Rۮ�%Ls
�5�ӑξ�����-� V�ю=�A3��h>z�����~g�oO�߽=,�fs��u-C�*r):�7�U G���>�"�P�7��J�w� ��t��XW�
s#�`��]�{�x����	'Cu��xN�9>�c]ΊW�|zCU�e{�y�[��<�oC��xK �r?�#Ǩ/%�x<A}B0�M�}S	���.�"��@�t6T�Qp��oޟ��lu�7�C�׫dQr���Tݩ������6]���`�Ӎ�?�Fa�>F� úߢӚ��5JU�!�֊�fH�YGt��U��ҽ�5@"��̓$������LfC%����[��'��+�+��&E��R��f~�XFE�V�B�����f3�;��j�]|^����p�RTn����z��o3�%�q?r�2J�(�1�&��аD=N���Ce���yR�s0T_�'�#�G������du���s&�ձ!�`y�	+R~���:S�6�\Ufi�6Z	E�_����WO����.��ȝ�>
�d�&���t9�e_�m���g_8�"5Ut�$�F�,��s
�\8�|.z��]���d�fV�����xr�U�*�Ï"ٶln}�1tlLO���=	)[���!� ��6�X\�v�y�_��|Yl�V8��G�򰑵�T��'�Th�(�>��g�t'�ο�����s|���%��t�&���zY���ٜ�(��O��Sc��*�*̴�Y��0���2�b��^.��;6�X�u��Ҋ�c� �%<,�c�����k�D�ti��
d��@�F=h.e�1t�����p�'Ν�`-i0f���Y�+XK�ڼT���6��H��"����il���翕��#T��"�� �>�D�dr�O��͓P:���R�<�u��ٮo�����d36mZ'M6sb�T��%��P��w��8?T_N����Ż������B�o�y�[m���a���Ld��͉w�\��̉�ݐ�������wa�.�4:N�w�ա	ǏrA������_^�r*4c��#|�|�K�����b�������쯢�d*�W���c��t89�����$l��wَ� RC|:|���,���r��1g�b�۬Wk�.O��=�/���������p� �)�0�V=�2]GV�)�q�=�PkC�$/X2��\�P�C���f0.��M{y�ؕ��~�}>��>��Cq6���\�N������E	n�����ͱ�m���}[�7{����fwrv��|���#�o>{'1Yd5��Y���cUQ��+_b��*�k/�����6� ��r&��?AN�U�8B{�Lc0��/��3�st��f��|4='������4<jZK�_
��9hZd��C��r������9�"�A3�0�T:Uȗ,���jh0��3?:�
o���s�d�1K�f9s��Uy�ATC���'��̙eza�^|�od�����^vg��q��-L�!nL�Sk��g�*��[A�+�i���t����/�v�+#�W���Sy��KM�v�<u|��U����O*�7�`����3N�� � &�x��!���K�g�Xi��w���J�\�,=�! �yG��h�1�;ֺ�UȋC�2�2DD�[=V Vr�Y.�g*�7KGʉ.=�7��w���_=GlE�D���]�7�mߩN��ߝr�`8=���Z����KV�37O��1�88)[����S��gΦ3�c��v��\���?�s�~?�).?3=��w1�f�6š�8:U:#}�v���&4Ł�h��"/w�,P�B뼤1�.����|ê���\R��d���$hƟ���9�xj+�vu|̐�uӂ]��0.{�bli�tZ��Ħ�*#37��6\l5z�^����l���:K�'�Լf��7�D�}9��lz
è!O��L����!U�蔆yH֊�ƱU�
ƪ�N3?|������T[W꘹��ݘ���\�Lv��"ӯ�`2�C���1ނ�:W���G�q)ܼ[/�����i`ϣ<�q�c�7=�3̉w�=#&gk�Ě�3]�0-r�g-��ճ�l��V���c���a#r��Q��WSh��cyu��vؗxr�#	`�����P��C`U�j$�h�>sk��iĴ�#x러CMZe'�[y��K��Vy��"�0�a�����<Vg���`��֟���#�-������Y�\���v<J˼��)�3n�#�7�t�c�rL��=>���c�Y$�  d��ɟ`�Ml�?&Tj�\���K��>K��4���Pս����l��q�L�����8�0S�>1VS�f�Y	��� 8�o�Ȉ�C�]8n|@q�y+�qD����-�2����U�qdM����d��3��k]�q'}[D(���R#�*��QL�xJW���O�Ԥ�c�����&z�׋�y�t�!m�{��>�ϳk&�&�n��U��}�����k�'�0�'�R��.~I���돭f~<.C�kY��;Ů�>�Q����F�x~�O-����*GW�ѳX��933�3��Z���l���^��Z��i���X�̠@)z��x{�|���Dl(���3Lc�^�5}�0�(�K~�g/$�ӶG$%��m8XSה��9���/ђ��w��0��4�Y�����8 *�Ѓ�Ϥ87�b�7_��몪v�iMc�R�E~��3v�8I�;t#�n����A�����17�����i�o�,Y�߆��>(�y��6��n�z����w0�������ֺ;�x����q=�%�n�1�rM%ܦ�,�|��"�H�LQ>j|?ntֳ �o�;�3d��dP�9;|"Q��ɋP/6�Ŧ�sY�Q��B���:��1��FL�Ŧ�Q�v1M3ף�2!E��jp�g*�����LJ���db뒙mtx/�A�]!�(��(�P��p�*W=�Jm�vh�o4>�����&��g* �� �k�:����<du�g���Y����+>`&�<�hV�����t%���,�4�lz�C��ὼAA�:�}��f�1%I�N_��;�$�Y��}y�^޴��A͔ Q�n)1�Ϸ�a�ݾ}�Uo�Z�L���pIE3�����&O<` .�ݦ����:�����=T=��(��-�ꚠ�6M\m������oʞfoǗ|�8T�(+'�|)��f���P�@����+%�J=���udo/�ͶoKF�׬�y�(�$��J���܇��u��?p�{�P� �   �c���rU^,7G�������&��x�����F��=4+�sL�͟�}���^T3$�������.�`Nd��	E�b���F��(����GG�辤d��䄦�ZF�{�ֹ1�`�Əc�Q�o}(�t��R�9�6�"�|w�"ܔֿ�)R�q�(;����P�Y�P/QnS\� (Y��hV�I0�s�]L��&�>W$��G�r0>>������      �   �  x���Ko��ϣO��@>p����lH�D���%��LȌ$*��	�����~-wMq�]��O�S]�K)&5���_�ä�]
!&G�h���j7)�ĕPW�o�܉���J�Sb~���WR]	=�R�IN��޽�����RXA�k��"nE�u=D%q~:�D��v2�)�~��?�K?�az���w�w�&쟶$�5�l�m�I���"���8�>HzI�1,���C8 �Ŭ��K!ƶl�N����L�&ˈ����_.�؝qf�������~��z+�s6g���&��۷5���[��B�b[�w���R�����HH�M�z��s;	ef�펜J8%nw��N�)$�8��͎!��rq�JF��)^����b'd�Dkq��y6�0E�E)��V�F��;{�"�g�3h��fW�lN�֐9�u8���SÃ�m���JlD�)�tubq~z6 ��ub�b���H��	��T��Z�ybk��W_(�+�ngB�+�o�W�I�9��ωS�[L�Ė>'���{DH��o���0���3�R	�;x��U���c����$sT	�~i�í����-�W�C��аH�H֢���6H�4ˆU���;��	'%���6<آ}$$��^7z��q�3	��2�o)�e;F��5�!S0fgM��ź���ä1�ș�Z;�0�Y�U;9 ,�����p�F=	Az��r3?p�e���j6R�e�dh�����8��5���,�yr<0���v��)��T��R<?��,2�:�M��������'�3��PF28���/8�*hĮ�DEl�Ź�ӯw�1�Ac����V���`����/b_�F�zC�"ɨ �e��X1��↣��UN��~:�ӡQ�O�ɹ��[
M��"�6m��g�)tP'G��0t�l�i4~��y����Q�֙�%�?ʷ�B<L9m ƨ�|�>���.)ڲ2'6`�����#�Az�t!Tk�#�p��ӑ�����{3b��q$�=g�
#,ca~���\�)����	���w���C�9>����& Cj��F�Z�w�����'�/����O4���О����e��R�N%6��rT��Q̴�!b[�R��4/��j����܂xN�Y*;��P>�|�z����d�mh�e\C���+¤}�x{��7o%�G}ak��E��u�[��LzT����2��b�8�+{��V�����ty4������B\Sd��Jtӛ��n?_�J��)�m( L��y�����\��!�S/e���T����7�����,2?0�9��QK��`P0��	E�o.��_�����f�O,h8N�C��k�M�`�f?^�'�{*q)�ʈ`���j��C;��ԲM~kqz�ح��h����~A� wg�ކDj��<lOm)NO��8��ϛf����<�~9G���+����xk1?Z�p"^��(Ode/��E�$q��ND����ʨ���V����uT�Ů2a�& b�����Ʋ�Mz\��k�P=J`����G�6�V!�7�q�	��@�Y�6���E����x����t���e���K�o��-�4����j�ʠ潉��],��6'�.OK�ƙnq�x~��9���Q&%��MK9��c#R9��Kv�����`���+�4��"�H8l�����+�o8���)<�U��6�TVCxBpx\�,�7�>��ק~�0��؍�-Nv^�5�N�ZPf��R�΋ְSb [Ķ�A�������~�Ԍ��zus��}s�����:,�:���p������͗�74͡�Q�D�T��C�=H��s���	�	f�f�pE����ge4�L�k[�:�`Xb����!�!VB��_�A�2M��D�/��A��J���rssW����m�dh��;@!.?�G����ᮢE�M�9I-tM�tM]���d{V���g,b~�*�$ 8�azu����f}��<kEI��E\7�55���Aϱy�@^9{0K�C�A��xiVU��NL�69�R�h�\�y1d�1�����{n����c	���z5
~��
I�,OL:C(ģ[!
U7_FN���g�
ϝL]�ub�PS@O��*mڎ�Q�%m�שּׂ ��U+�``=s;��RX�T���r)tI�7G�]}&�`�P��B���4�TZ�|r�U�{�*�N�a7m�Z!�|k!`[�������n�n�8n��a�a ��`��p�z�1K0:��ǋ@�MRLrDJ����?����X���S۶�\,�Cq�Y\�Lr���1w�=��?�j$�6ķ�4�<�3�^�	1?{�3J����z�H�QY�Z�)q�#Tsݗ
[���q]����$�5kj�Rԅ�N-9e��%�O_Io$_�ys���Io,�7F��?��ߧ�����=��l�~҅-���-b��!e�P�u��j�dݨ҂
-a���Mn$��t�Z�"���1R+����S�a���b~��=�ٜ���)�q�R�K
�k��)k���2]5�?�=�[e^t�#�|J<?^��8ɰ������8R����S⑉��1��K�5�Z�X@`����%�{T�pΟf
rp/�GW�k�"q{��OK���<�/���(���)��I�~�hh-x��B�b+*׋�>����yxPc���ϵ���1a
s���=�1;]I�I]�������q�+���h�&3�1�c�xFx����@9|��tg���9NZZ� 1�,�ŷ8fHf(b��u5�&FN��e٧S�b����Ֆ����X+���89xlp�x؊'$�MN�^�ˀ�5���"/<#���?%��MWN��g�x�yR���nVH�+˗��Iq��'��+��鉄�U �K�a��X2�#B=�
�E�-A{Ѿ+0������VJ\��kFD5�a �/&�FB.f`��NQ^��/������{BH�ʍ�sq"}�&��Z\U��($�Q�����A���{V������G�v���|**�oj箲�4���E�(���N�	���mg�X�p��E�o��mX���eQJWMU��80�BL�����SPؓ�]��-%p��s՟�z%������W�G
i��|������9���b
q}��/�+�q�/M�� �AL�6m���%&d��Űl�Y?�G�t3��
9V�oےq5�!C�y�+��ǃ�f�_�;����ѧd/T�*��ύ������oB?�!R��[�2L�u���q=b��H-;	��C53~���~=R ����K
Λ��~L~Lw�&'�%�)q[Ӆ3��4�޼�9�f6f��[���1g�l�u�o�j����ޣ@V�~��,q����TU'6���AJ��*��B<���y)��~���ˁ�����tBu������r����>������n���NYh��n���A���+SSɒ��i��՘�^�e�q�BT�5���i|�9���c�\��9��Iʋs�h̋���-O2����t�[��KIj���]��Z��Fܐ�f���`�nಶ_�����mk��HK�L��럏g����ȟ��Sb�-�0x�rI��8�[2��pQ�ub׼���U?�O��U���v�şXj�4!�!�cF�(�<a�8�xh� ��7��T#$}�QU0��ץ�G�J���0i�4�R���·�4-��4��G[h��_C#��_`av-�L�7Nf�[IZ�r�(zRܿ8�,�ܾx���Qwס      �      x������ � �      �   �   x�u��J�0���)�kg������/�T���P�n�FI���D�!���&�
:o�l� �G(p�jC2G���J�A��%�va����F��	���]�H��"���:;��6Z�Qb�[؝�L�Y	�l�=��CN��Ը]����j�{c]�̋�l2��5 a߿)ڑM�n"幒��Gs�o<ہ��Ā�\�����X�;7}p���R�p�o�(�0XiE�R�%	$��E�e_�Vr(      �   C   x�3����,)��K��".#�����<(Ϙӿ$#���5202�50�50U04�24�21����� {�r      �   |   x�3��t�u�Q������".#����c�#L�!������1�tv�	�7E�p��d�����
0�ru��t��-8�C<�� �������������������1W� ��+;      �     x�U��n�0�g�~���&�d��!��%"UjB�0���}ƭ-�����7�⩿�,?�e�o��C����=*�|��o���q�['�S��8u/u2M5	Ҍ��Q�Ud��^"%s�'YR�I���a�D��F�C�C�$���H�����*�F�������mR���[��W�A�.cX��~_c��U��P�
K���l�l�o�R�֋�Vh���J�$mIY�,�:�IT���jcq�R�'�
�*�K���UQ��K�5      �   �  x�}QK��0>��b����.�J�8�����^�x�L������ܤ�H�%��=��?���YKd곦*�h[d�t���L^�8����N���<`Td���Z�V�s�`��r!/�5QhǙ� l� �i��z�xO���#��뮗 ���+�'��U���H�<I�C��1 A�4p��Xo����������5��� �p�,�زn�7��s��q�Z�;���%�b��,�(��x��
�Z��V����M�����aG
�N2�ݓ����|���cc2��?+{�=��<��m�����нo��V��l�����s��A���2PW���qO�4b������;
mg��6[�H�lc�"_�?�e��[�<r��\����n]f�$�im&��Ze_����;'6�{      �   e  x����N�0���S���u��+ �	8��.^��I�4ִ��i��*R��?�]e�l�	�;�Z�K��i0��j�4rZ(�K�Z�tGtu�xr�EtPa�"�5�#K�tt�*	Oh"C�0P�@�����i��2�[��.��{��M�|}�nНQ�'�[l�Z�h41R��,�b#��^}{WKulÈ|--��:Aly&����",+L��$Ŀ+��u"tpNk��F�S�����u#���5Nݷ�\��Y��Ǣd�q?C�4��Md-*�">��@`�%��m��}ĠA<����eY�e	�F �!���<.O^���!��x���|)��O����˕�6      �   G   x�3�tr�tw��".#� � (ۘ3�1���3���p
q��tuL���ͬL-�b���� �.G      �   B   x�3�420�440470720�,ȍ��OϏ�.�+�K�,K���+�[XXr��q��qqq �<      �   �   x�U�K!�u9����	ԝƸ�M��� ���2�`H�n��M�p��z�h`0~Qz�d ��ݵ���㓲�GU}�d5&C ��8�_suF&��e{8{F�T�ƲԘ-ܢ�Qg/\���k��di�������n��� b�L�      �   �  x��UMs�0=�_�c{(0ɑ4M�J��%3a/�ؖ\I��wmK� ������[i��y@��Š�2f�$!����V6�@R|��9*E��7�x���)���s��2'�#?�2]*f��y�@;�U?����ORE�0å8f��R�v��;��To��}LܕE�h�o�j�(���1�_���o 毀]���
lTD$�;"#���E���n��`[Ml ���
d�(��}\�ۘm�S�.-����5~ʭ��<wŅ�cd8�KTh�AW%6�.�5���"W(1�5�f��!���������̏���sz�pVa`������Q^����Hr�?�8���Y���[.X�ĺt�B�2n����0�:��0?,`VaR/�_�m��j�d����%ǻ�[ݱ��"���c��
��	�vH�2��3�{K�n(�?���Nō�6w8��-����Λ�� Sx�wc�dL�ū�7"���2|�1Y�4̡=D���u#�
�*:B�
R���0m$FE&�b�k&�,�0{��9�:�c?�Yș���|A?����gD�&mE���͢� ��@x����#�^8���{/6R%e�<0����$�s���������T%���y�gI��9^柼]dF��&_c7�s_?�K�Lu�x���b�6�eC�T�~q�I���3˟_�������Ӈ�['�飞��^�������2lp���^�?�d��      �   A   x�34�����40044���4202 �\�Ɯ�aa3N� ��!���g�P���Y8F��� �s      �   a   x�3�t/J�LO�4��".#� � 8τ3<�(/Q��4'���*j�鑚X���2��	�śϘI�������������������%W� ���      �     x��Z�r�ʶ}nE?�'�[Wx���P@�Ϊ�Kc����\ŋ��g�n��v�wV��s����6昣�a���/T��+Ň�N7�_����Q��3���M���H6��U:S���'�ry�;E�R�I�%o���G���8|���'��~|1Qk�\�Y���F}��E_�?�*ݪ�ߨ'�'1>.�C�؆9ǿ�#C����K��M��M�|S��tD�!��hr![��
j�;<C�q�d�C�z����X��0΋�Z`��"��J%�-�&�z�}+��gq��Wxn�o�>.�Y���w`Ӧn��h�V�G�M�K��\�޼pDdgN�V�5�Xs1���g�Vq����F��|�to�Vm�9�V[��b�Y�έ�_\���s��\��/��U�/��l"*�gk>�~�t��>y^��1"d��,y�o�J���ckdN׮7Ȟ.�ҶB�M2��]S&S"ȸ�l���C���U��x�8^�j����a�n`l�z������\�e��XK#X��������9�~g7q�VS���ؐ���C��^Aa�����1s�T���l�%|ÉS�;�o�R�2�����o���:X�zGT�s#��&��ɽ`��-�m����v��.n���7\E�t{��w�_���.��]��xN{Ԟp�����Z�pX'˳i1+r���pגF���0C��(�������S\�"����%��/#�K�z��$��z��*O�uvc�&��6��zԛ߼"ɺ�"�ߪ%�V�U�)
n����s�O)��6K79�4��'=W�ܮF������놡`�����w"�+�&Z��j=֍�x�Ƕi`����ی�	p,���ck�0���
�����U rw���n`K�5�!l�w��t?�1}��wR��zY$Y�ԟC���yH�N���c���%m 4z�Z�,�St�e��pֲ�2��&О�}6}��'��S~���M���<y�̐uARV��3���XW(_�'�
�@�K��W�����+�d��K�~� ������ �%������	uT.$SƊ�ӗ�:İX�v'�!D�}^���<E�К�uR��Zs�5�7��l�'E�׳"��"�� I�}F���ӃL���ĄjDp8�
C��-�uY릫{5���ST�:��x���v��VZL�l���[v��������ϣ^����X��E�8zd����+�ֳ�����M��@�ћ-UD�Ƙ�@7�F��y�:��Va�>�������ñ�C����2\���������/��Zo����49��� �Z���k���S�.!bob  ,D�o0E~�)���%�cݛxe���°�<�.һ�H�\#Wl;3���V�JvC�2�I)��(�7�A��1�~t�G��]j%4B��?�����I9���������g}�I��*�-1����xi��A��TƋ�K�~C5�0.R#_�M�1�L���\g5 m����P��T�E��+����N��c�;z1-М@ǃs���O"���������ЊX�;��]�c�a�" ��M�b^�(��A�j`�iV�r�6:)E��dPF�b�'���s��[� �S$�2" �D�E5,%�����@��Q�s����d6A����E���s��d�r�0�ʩ��+��"��~ h�W<:oY�(�Dx%i~�\׌������U�5}�=
��:�9l�K�n�`MI<�z������]�ws�qm�4ِnn9a�i�	a�ϮI�N��9f�IJm{�%��m��ve�R>�=j�M[��ɠod5��oQ�B��Z�o"�c�]���`�O�z^͵���^�ʗ� �e}|�H�1&���}5��4�<��^P���+�k�֋ʍ�bj���m�md� �����K�4[R�d��l�II� <��l�bZN��b�Yf����S�[;�"aԾ�T?�J6GN��16�L�d��F�=vg�<�kvYl���#)�;�,6U����sB	nk�4]�X�" ��q>� ��:����`S����F-�}ɦ}�Z�@�{"�Eb���lA�1]o���H>~�P��I~�H�g�t�ԟ:������e.~�㶄��^D�J��L����d�R��F1ͳMf����υ�C^6Y��O:=�_AZH'x��_ �I�H�E�@��v������eMͰ�>���1~�OfP!��^�C5�zR[�hx�n!i2Q{��%T`���rٝV@�^J�_�yc8�3կv�2��cvǒ�-K2�ߥj��/ꙡw8��1U�,=�=�籯������^.�z�4���o�d�L���~4�@n"�4/�E!�����=,�$8>��,�������未~��۬��~9�6N��S��d���Z�v����bk��i>]�_:�U-W6���N��$�X���Cm��`��[־�~�����蹚�U7���}�Ql�[�-ٸ�T{�~G��$|����̐���1�L�W���=T`aX�aM�ͼ�.Jţ3]�C���$��Ʃ9��������4[飭tT�9P$[U�g2���KE;��Y�O��ovY�&�fa���eO�]���}p4���ډ�	���b'��H9��J�j!�N�ˏP����(�x�E��X�����|Q	�`5��k�/Y�)J�<��䵪>˨�2���0N�M�����ٲ��_��#A+j�7i�t��K���=!1F�i	Q?�Iٔ�9���&$M4�����$�s�������+�:���[Z��k�Sm>�ُ�������7�Sb�l@��+�૬t�z�chW�s�y��e�͠��Qr�������തC|�L�#b.]�6vBdɉKYv��XFhO)ᱲ�w[5?�)9�4�~�.V�Y��"�.����N�2�m�xZ�_�th�·9a����ut��ѣ�Y�@bPe������P�����ދ��l&Q�{���-��-��s1��$LTn����\�mdJnE���գ&ze:Fԅ�t��,��"M���h>�`�۠�������V��We'�yH�W	����qw4�פ2��b�m�wZ�7Y����VD+#Sm�k�N�4ԻcV����]��H��x�U����V�g�G�a k���'�r���3���&���a�����(�I����Cͱg8���G�9�t��4/nHb*!
}'�d����%�Ŭ���o;g���� �N�xm�V�$��rs�δc�dNʙ�2&}����~��R������$�p}
�5���X��E����/Uųj*|���<�;ñ���J��g���r9�MB�6H���R[�}����E�����%�����M_�ҖL��{ub�ۄ��6��]�O��$Z��}M̆eJJKH��c�ѱ��� ��'�h)aKz�ʙP�)�u��q�)G�Z4фհr1��&�ߊGe�2��Ѥj��������T:�䇶�}Q�vl��|���T���/O�P2R�Wx�ۯ��J
�3�%�0GIv	ֱ�1x����z�ٿ�q��a�>wdK�-
�`��R�T6w<Ђ�&ްk0N2b�/�"Q�~�y�S�~����SX�P)����V�"���>0���?�=�hWf�Z�^��{l0�ͷl���,MU���?}llĀD�'�Ĭ�\�՛�V<���xSډ��aX�-̮Z}�����X���!�ӕhF/j��֒��.�����+�HB��g*^��?� -�#�������4u���g��.풗����Nfx�6=�!�Oh������l��?b:�pW��'��^�=���'ˈ����f�?.�����d�      �   4  x�}�1o� �g�o��8v�Ҹ]�T:�EiIK�H��%.D��V�����d���tֵ��N�Ɠ�S�(��r�n]����]�t��&I��zs2�F�< /������:�Y$r����������װ��"���+�u�H�t�O���z����	���G�tIbyX�^��S�^�(3����W!ke����3ጳ��0��T����Ms�]8��P��~��-͇���.ٮH�
�sʑ��g��I9C%BXq�ɜb��ߢ���>(r��Z�Oq0ߌ�P ����!��(�_yh�      �      x������ � �      �   F   x�3�t*U.��,��".#�@@jB�$☛�X2��/-)�/-J��K���r��d��b���� �>�      �   E   x�3�ttw�V�v���".#�`WG?w(ט����)4�5�pR@4��q���P�=... ���      �   6   x�3���,I��".#N��4]dcN������(߄ӿ$#��ʍ���� �0�      �   �   x�m��n�0�ϛ��'hc���^�X�����0$u�>?Ad���|������a�����{���8s�u�\�OX�0�u]ߑ~���L��I�Ѧ="�-��_L�Px�N�*m0S��(]��m[fPPI��|����\��)��J:=8�%���_���Eʕ�{�-�,�=_���Y�8(� 0����݅O�}۰�6�.U������DQt�э6      �      x��}[s�H��3�W N���cd
u��y����c�g����-��H^����Yԕd˽�[� �����2��,1���s�����&���s|�'����/�ŏ�����äl��O�S�L^�w�b٭WŇM7_]o'�}�w��2���3>��t]��*'�.����p&�s�'�R�rR��p���ޯ��M�����&?w۽{yѭ��y����1�>EU��L�3!ܵ�(�3Y�	�tޭL�Iq&�++�Z�L�}��o]Ov����7g�����nVmq�n��b�޵�v~?��b;]�P����������ba�����2�?v:c�T�(֛vI�y��o`�ϖ��������=\���W��U���p|��s�r����IT�~ 'r��`��e$����r~7��e�H�K\��:c�����_I{+ȭn�g$�L�(j�����Q¥DQ�i��v{�[���+n6��f^\��~�~ŧ�E�h�U�O�唗N.5��D�b�]"���<�ܵ+�$u}��-^m��]q>o7�l��Z������j���v����!�1�Y�F��k���3��Qb��5�U+�4b[Φ�%�T؋]��5�spU�W�ȵ,;��d�_+��`g�
�R�6�pwp�Tⶀ%�Z�H�fc��	��r>�n����f�[���n��>��X�M�kP�b�^.O
���Ý�ΉX:�X��DIҼ��Y����������b�����eq��Wo	�����7��)qq9���兢��[+2U�S_���գ�g������Eذ�`��g�!Ga6g�>+�D�6����^�ϔ�o��nV��o�mW\��@V���Z�z��o���Ķ*�붸��5�
o'}}�?���@����ⷋ2��y�&��HFb|�6_��*ƍ	�r��V��(*fqT3%�V���V�s�o��+KXy@1���jC&t�mp��E� ��)�k�,l��2ֶ:�6�7�i+��ܷ�n����f%�[�q_��� ��
��uz���b�
���'/;_^Z�S�����j�Y��g>۴_lPE���s�>��jb�<��Tɋ-�>� �3�������[�_IPUp�j�H���H[�!���Q?�HC�T� ��i?�F&�:�Q&��֢$����rkz�C��r��_u\m6Q�)W��j
~�����
�� Z�(�YD\�L(R�E�3����8@��Р��P�����Y ,T�{z,r1_�EC�9�d��݂ky�ㅵwF:�i�1��^����;�.�����`�s�b�s*ӭo�Z�����'��k`� ������Z��5B9��x�~����m9K�~�z`׃�)����H x��M)xխ��9���������FO�ߍ�pP.1��_ �l��h���8j@yBfP��8(��!c���Ml����#�;�z��; ���T�X/�A�h�E��~9@�խr�D�iJy ��6+q�tp�b�HP�F_�5��Ç��,@3O���F9y�;V9G۪L<�Ee��9X��x��=mL�An��,��B�>�Ȗ�؛��� Fa>��D�>e�y놲����#[{��\�`K��ʆ�?�Ǉ�Y?���#���{���Gy}���,�SM L�2��
|d��cV��FY�����o�GR��ʄ����
GJ�Ƚ�t�i���A^/�w��f�/D�\��V	�@e�VBe���c��	��~~��\����1�F?�9c{%s�ļ�to7��.޼����9Q�+�2AIU�갿��\lo����q~H��nB\�ߏ�FG$v�y���k�@:^x�Mp+�u��4��ܺ�:4���n��n�cP����Wf�K������p��0�	��*��ea�����B��uhj�8�M!��`�`M%��{|����;����20L(#���G ��m��*Nvj�K�fkj��m��3=���|�W^]�t����f��͹��{���&@����f
.`�����K���~ ,�>�3)c��jZ6�Y�~���js���aax�#g�IR��a�8�
#N)� 
Ai�6�,<,/PAa�8����T�Ք�����CO$�c�r���n�;�ʜ;�I`,|E�������u���.�m��z������7��7k_1c����Vg2#� �����>S,�kJ�q��u�RW��(z�|rE�ߗAtYƊR<���G��U��[X�E`j6_����/�۶;�8����]�mA��Q�ێR��ݾ�O��q�+��cSk՜�(���~7����q3*�a������}K������j�M��B?!!�7��g�E�H��޾�Ȓb<��}7�ǥX���%�ݒ'%��۪'�O���[h�\���>��A�]v��(Zvp�q��]a�F]��d���L�FLa�[���s挖c�a+4Z;�m��1v���,�!����'Z�{t�];�k
��1�0�^���R#A�_S���Z�S\��9#&AIDj���e���r�\Y�w<T�7A�y��������q�,�Q�us<-�8��U�@pQM�x%�_h�B2h�h��o�@房�k2x6ی���ެA/m1@7�@C5�5���ի�IW9C��>߉��(p���������w`M��'�Jʂ*��~����[ɏ�z"z����&F�(�1D�w���v��d%�i�z�&��9|Au�eۭV�����|Gܤ<Mb;|���M7�ߢ �]�B�j���Jsќ
O䎄��"�$��7V��<d���%�h�z��y5Q��0QR*n�ȿm�UL�)QNǥ*tu�	r55��2QV"x;ɂ[�%LE��zee%�$����^d�o�Rb�͵|��^*/�U��P�և~D�B���cY@�av�l��	�h����J�2�!�+"��d�U�W5s�'��VXS����a�@N'�=M�⣭�!!���w��~U�-V����L]�{T�$�ˈQKnrP����b�� �Qb��T�~w�ݏ-������}��FT�wѭܔcZJ�H������1QM���TV©Oj��'L�äc�D�{�n��W�/48��r����L��򄶰q��u�#���-?�(�׻��w+���L�
6M$���D`��a;k�'���!��Y���h�O����
\O�r��Z_w���+"V2~���<.�k�%f�G��%=�H�O��3�>��.J5��fН]�Zd�H�eq�/�<bO#�Xq��U)�<�8�Xwgy�Иݨb��MDˌ��
`
%B̭�����ʻ�_ߒ׾|[����|3�#�΀�����������N�|.�� �z��+)��[H�|���D����դG_OQ���q6	���U�2#>u�$�%��"�B5q5��؀b�@�L�$ћm�,v���
����!O+�5,"���f�o[Xw#��6�����~fy˻�W蛦�"�} 2�� ���|"Kl�5v�w흍۹rD/��lxM�HCo��Ew�a�f�4��8�&#u�&��TR�GĖ�����J�؟�Շ�|i�8yf��@���=\�� �g�5(��\�O�;���1d,E7�	�#�$�3�.��_��'���
��F� ]���B����J��~G�->����w�	����\�����X�/�×��m��Bb��mX�2��3�4����O:ﺯL6`v YI��d�����t�G�LtPF�cR��ܛ���>�l6����n�)�&(E��q��d�<��N�I��e�D	r��f���"3����T��������8QUj@���F�b��5�K�k����{vxn|B��n|��jo�����A^�2��8��cd,�|!Z-u'�G��>�FeS��R���%�ݬ�:
���4?�o��ք,n�[�&��(Q�s1�o��|ξ��v��'Y��92.`�������]A�Ë�,��g��b�T����r~�V���1���(����!�zI�A�/�Nn��A�*�e��Ԗ{�`4���_V���    ��Ѡ,����%,�I �H���n�ҫ����O�hg��u�(�(�:j������RDy?4����`HB(0)���2C�K�T�	}�`R�Cׯ���F��Ze4�J���}ɨ�bu���Z����#�S���\2A��Al�X�v~-�c��<��!M����n٭���y�a�m����/��X���#:q8j�o�M'�M�=Et e�8���g�*%�r�/:VJ�q3�0n_k���`Տ�|����sm�mّ�6����R����Y�7�f�^�~�Q�^�@`r��������O��,*p��mb�`m��9��2����h:�@�ۮX�;�X}=b�=p$���dpd��)�����W_���J�p�S4l����@RUg"��@N�	���e �Q=��;#�m�g�a�w���c����:m��ߧaf�s�9�v��)ŏ��&��Y��n�~�Q����(TK��!T���U�7l���V4�v=�voE@�%�6��.�z��jå��+��V1\
�|�(�)H�{��y�
QAM!�Q��c�q�[P��6.�4�����5���n
����_���A�}���̀�,�_�v���l���w�'}'�{�0�\?�z�B�Vޠ�r��>>����&��Ih��+��~�/���U`i|�*�"�؀]��V�Ŵ1���
pW&0���(��Ķ�1ۖ�<�/��ɩU���Sr6��'�&�ǄV0_uwů�;ت����TeH�M�`��h0EĖ�P���봶��7ԶnFwȸI��g�	����{��ǥ��R������S����!�SO.?����>�����Վa�L>q�vZ�X��Y�n��U>"��Z�d�Yz�j�j���'
���;MF���3�9av�1��xWy��r������7� R;{[~�$�e.� ה-+WP%_ѵ"�/C"�꣛�������J~�������,p��%�n���#����!N��#���-���o��bѭ����[�w�)�d�@�`P�����N����˵���2��7ݮ�i�s�	�^�d�<��Z* i���4}7� 9��(��ϋ��I$�M�������|���9��M��m E\���,τN7 �3����*�K���:�����U��o1�K��X|�=�v#:k{�Qi�Aϖw6
����X[����Gm>���6+�B&�;)�dt��E����V�c�G�Z�&��
�_[�0�jg
Ѿ��%�I�����q�6�$h˧\���/C�m��5��`p�ز����H�g[���-燇ɵ���M�Q���q��(%&���ͳ8G$���Î�p˱��K��D�Ef|�Ii�L�LrMO���7=�I�L�����U��S�]��ddQ֩,`݇n6��rd�'E�'��-�_�F�B2�b�\l��~VluP��BZC��x�ŉ���0�e�ԕk��3Od<'���N��t9���yj[+0@��ߧZnD���ZPR&l�?��k�l���Y,�h?m�`��3TX0;���P����`�a���U0��As*�W�6��	zx�館u�1n3�J�s�>H�1���^`��Gƽ�ayR/�]�a"��{w�e�ќ���"�{��x�G���k=[�}4&�{s;H�{��f�/��!}Q�c�.-1@h�k�J5.�j ��dw�1.#�o����B�A�̳�*7F1�C뚎QTJ��t�7������R_��+�"�Ud$��<�];r�vXN��I9����b	�#\��5;3Q�����T��T�A��d�>*®�z��<�cR@c-�������#3N'3M'�X���k�!�~(��b:sQ��E�w�iE35��o�"!��("Ȕ�	z+��m�T+����\i��=��"���E�IpF�&�F'ŵ���UnL\�:B�K�p������%�E{�g�iK,�w��nW�n���ĺ�K�E3��;���l�*�������<��b�,eO�1A��vNP������Fй�r�	�#��<�Fx-"��$���%vT�0�2fyq;����'���Eb��$��LY��ԛn���Ň�����HK+�� Yy9)8�e�R�B|���g״�Owx83A'M���2Z^8+��a�i��0j�,��?��8��fr���	���1B�3�Q u2�T`���Ú�1�%S���צ�'��l	�+����/��t��)�؈�2��G(�L�H��1�G��Z��\�4�;6֦g4�f�B+�8f�*KȒ�O�l6����<�FJ �L"�R��ȟf)t8g�|8K�s���
��D�!��X[�F��$���ڬ̴e3쳗��g,�.�e9�Ҹ��=B4��8>�����<O1p}n��(�J��0�D�H%%�dF�Se��C}�v�+{�t��ζ�Rٞ`XCd�ZЃ�V��J��.��0�I�ͼ��]�驒Q�9R���-uDm���&��a)}b_ّu,��J��[1o� IU%�3�՜f#~�����C$�Ӂ D�η��V^M"�{��q+����t�:
�Ʋ���v�.�.��y��t[�>ɢQ/>3�m"��	y���C)3�7,�Y6��|=�e,����F�<��B'�m�u����7���鸋]DB���7ǅ0���E@�8@`}�I>��NX\�á'�b#�	��.�*��l{
:�?��;e�ő�����.����e��E� ���������^�0�֥��J4I���ա �����L��*����n;�ļ0�l<ۇ% ���>��rR ZV�G���cy��C3ќ��FG�Ѡ��6��ٔ��?�<sb����"V��p�-�i�}�D^�W�� ��)��J�/�C��C;��2��N0�@�Á�	v���'p�r�f|\#ƌ�wJ���"0����4��Z���	�c
\�@\�\4mBskp����Ǵ3�2	?�	�R6~�hv����ͅ4�W��<�y|�丈FF����(��Q�ߌ&���܊ݫ��uR��MJ��i��X�O�h�Ґ�*�+!�����t��
 /,�9���y�K�ɝ0���o7�U>�(�N�C�e�#����dX��jF��,��B�U*�.��?Go��'��k���&:�1%7�|��G�FG�R&Ki�K8���uI��"]��������@��ToA�j9�"��������CY��IyZ��彉bN�{x�7���T�Y?�������m�?tCe��Q��<@U�XQ��n�Q<�.�+�ԕ��i; �����B���_~(p3�Ք��)Zf��?Q6�Vf^a��ƿr�s2:H��+�NU�@5H	㭆j���d�	 �Ô��x/�،�5���~�Ż{�9��p����83sX�����M����7ơ#�*��H굸�X�_�[{xCi����!����[��^7j�yێ����*��<������3�I?k�5���0c6C��[� rR%�@�-��#��7f����v�y�QR�r���Y�F|��l����H��n���V��N	2�hј)Jo���
#���Ahls���>0���z���������u�.� V��$MW��)Eh{�33����t��[0��v���3��kH����r}�q�|��[���-���L�3��`\����pa�t
�1�PFJ	�繙�=�qH�����vf-.ZY�jF'�9�I��W9�+%��B/�4~<\h=Cb��#��B
�-b��d�l1�[�
WN�5� ��0�mN`����;��3hu���)�������� �d�i��0���ETx�Dl�L!�����ȧ�-���XЖ��ʱc+��m,��֕l��}��Orn�u��	��E�����[<-Xa��i��8?�:O����Nǲ&��b���n��
bC#;��������R�d_W�<��ԅ�Ҍ0r�2R��2Wi+�6B���ʼ{���#̍=�~0��m�N�n���u�x;p::�J2���6�π^ǷC�    0�Vu�����A{��^�9����\BŌb9�P^B�E���e��eO�e,F 2�D�؋���wu2���ւ�"�q�����S��%d�R�y�A͗�r�)vt���f�5��|3ϛ��O8ee9��k�̝	�}����%�|jSjOca���N�,�tKC�"e��$r 8�8y!��*M��m����e��%-{y�kt۫��a_�S�_�d����2���Ճ���hh(]�(�����1Ry��O�|6�a�w���,�]b[&I�4&�i��Y;|1߶��>h��g��ҁ����n�$�G�Gh^��TM'Nj�V�  
�N�:"Nm��?���'��|��v��tZV)�Uin���l�۴���%�X��6�Y�95U���x���6ʏ�����,5 $�2V�GrC������5�c=�'7�-m�6�Ɗ��.��_��K�r�4�C��~���۹�Hƀ�=1_��b��5�/��/M�[m��kV���x�X�>���܍�J��2�X/�02�F�+�'@��mY٦/�Zo��r�9H���(��&��>j<��){T�:h20z�gf�:��S�z=�c�<�ռ���%�?�98�i\���_M�δ�a�o���(vG��<�X_�&�d$N�I�����u����3��/;�2�I���f�ٛ��[|IԬ"ω�F�_C����4݊Cet��Is�:��˸zO]���Õ�/��=`�6n���`��®P�J��c��퀤Μ��p����~f�q�&���ګ,����/k��<��g��uP��}��T����&�s:�����Åe���;d$B��ik3M%�ǳn$ZrTJ~��c_�w��
>��-��-P�����d4>����$r��BQ��JNӐ13{ �`2��&���(BC�C��Hg�Q�\�&[���� /E.�ȃ#;�)�.F��ʟ���I�����j�%��I��d@��:4�fɱ�e;bd��O�T��-�W$[j݅�/���N+;|�/���9[�B<&��ItX���z {t�4<i�N0#�X��4�Ǔ8X?
�/m�3����@���H�٘4+~�Q�vÓ�ea8p۫�P{��v�	��n�"��p�^��-�Ǽo�oЛ��Mggx�o��Iq�y䈼�;����>t:F%�!��ϻ\ۓ1���&��a΂�F�Zo���^��?��e����L�.#�MfZU}�m�,,���'k�i�щN�*<(27*�7t��Pg��ɾi��=�7�6?�3�F�SdfQ�zm���Լ[ĖI������#u���} ��ċIB$��ix52�c2�ϭ[��r��f�'B���K������ąx�T?I>&�cw��#��*��L������]�������e���}����f�c�E�9
?i6�'����#�t���<��}^M��ܵ���CʹX)pt���ޑ{��C34}�
[��b���z�iW�j�2���g��9�����#D�%~�/�ƀ�wkL���vn3���-�~Z�#�����W��/��k��8�7.�2HN��/� �$���8���(�__0x+����w^�����95����=e�U��"��'��N���Ȫ܃=�����>#�^���hF�\\������&�Fꤚ�b0�mS�Na�v%��3c2��T@�- ۭݯ<��9 ?ls���)�����b�0�:f�Am��~���ϩz���U=�i`��
�8��س'�$�H�U��:f�4��u��� ���<@�Yp0q�%�T���#܏���)Qac.�b��zA���nJ��@�oj9P�P�}ˇ2/��xʖ
��n��^&R,c)������u��|&�:j����C��V9���6�^P��'�|�J_9Fm2PB�h�8��&:��M$=l��0VK����8w�Y�$��P��:�6���!3��� ��頮�A���;ubP1᳁���B2qu��U��X��v�^�]�"���5�*���H��dd8'VF<�<��д���RE�;0w��FӃ���~/� �c�%�f�)&��Иc鏧SĮo���H�
���%��Ӟ����?�o[��T�-0����x��]Y�o޺�t�O�i�Ģ �H�V�a���k��3c�f�Y��ay��;PU 6j���5�@i��t�~���ج��9��s-4N:Z��Wo~s:�&�1���\����)�MP��q|�e6|0-xV�K_�q� ?H%;�Ҩ�@������U�y���t�xl{��q2���Q�M)��<\K���d
��nw��j��'��L��ix]ŏ1��X�X;UKŴ�&�I��IR<:J�.4c���H�5�$��0-U�97:9���f��c�![bz+ [¶~pr�![ni�U0"ݍT&�[i^����%��7�3�A�n�$��ڧbj_m�?rL� b���Y�T	:������[Z�aUc:[����媸\n&�v�E���{���7� @��9�����9���߂���kxZ������H��O`��~C�g;޿�|CE�Y{��Y�W��3j���ˇ�'(v��N��o��lt�_�O�9�8�s�Ւ��|wM����~g
/���b���n�f�mQ|�/�^�~�o�k��[�/�s|��|�M	���W��}�@��A+����ŧ]�(#[��;ǡ�N�mbͩdJ�yNM���b@Ndv���4L����4����=}�9�z����=��?p]�b�q�e�8�*�oƲ�ߤC�}V���1?��;*@P�
.����Tb"ȹ`̭�ՠ�$��sMӏ%���B�	�����V��9V����{d��ԫ���N=��^C�$�!�� ��kS�������(G�77QN��~��К�;�j�=�x�\9F�T�e����0vh��&�k���0�I��Z�Љ�GS��蜃#�����gA��[�qs�<L䒤J�[$	���zCM�R[�+�n������ڧ����Nv�S��o:�gE�}I��0J�#Yp'k���b�$��ynC��U*��`��'o�G�쇡U��O�����JC~��U<��� �������x�� \~����z{��c҂Gv���X?w���[���~�8`Ra��*s�\ϙg��S��S~cS����
�1QJݘ
��$<9�cJ*y�n� �~d�6�������$ׯ�7V��y�	a���nn��%y��P� 3�A�O��`���U��ee����@Ei\UF�Y�d<U0v�z�!���Z�5S���qr{x����U����q�ќ6��T�'�%H�+�t5��0�H4��;�+>&���z���녁�Z4��̩1�A�t7�C��lG���St:�Ro����_4�#���b�g磞n@=�X��h_#�)�sK�+�7�t�a>��&��r2n��iMQGe[ޟ����x^��2t�,�hH��5���O���Ç�+W~; W��D[�-b���%z�!� �l"��G�*�ȵ��orO���]��A1���L��u����C�_�Tx���2������%��x�ȥ�Z	�q}P�28�Ҫ��g��5��j�L�6:� ����S4��H�Y��l���������~��ge�0�0ٴ`B��0�'=:����Ԋ1�/	� ���bNh2�#Zd1���� d8|}��-1�������3���&g"�b c%ø���a����EX+�����1�גr���L5V����R�����8��h�g'�2|bJ�|p��^�9���80��s��*�sGm	�X��~_\}�Zz��W!��2N�s�Tvdަ=QK��^�۞�5\��ʿzD�)��Q*�f�NI�؜_���m��|xn�vH��[y�
�V
��jLw����:��(������O?:d�w;oL]2S�Y�D�1ؗ"g��.Ѿ�L~�X�w���ٗ���KA��Iμ�!�a��۱�OΛ�2��蔸S�13H,+V�@���"��CgAj)��� Nss y��I�N�Q]�_9}�=4s�����s�s��p��7/� k  �L�(�p���/���i tt��l"Q"߮�!9!9.贮���i��MM��gs�l��C)E�)�.����nD����p���\~C�a7d��_-�x�n�Ͽ����H[�����J �� �����������\�|Z�0����b�vA�"�v�k��I�){�M#L���v>�w4�Gm�r� 'm�GmS,O��(X0b�8U#.i�!���B-H:O��F�G���Sl@s7������P$�r���
����8A�򯙌�,c�k{�MTlp6+�h%��<9?\d.m��t��K�u1��x=1����㾸X/��c&�� ^��|���$RɅ��6{Vpe�����8����0b�}ߚ���n<�����<~]ɥ�C;on-�r>���(tV&僪�����'"����!���G5ĊI�iό�0{����-�<��t��'TD�8`�X�����f�lB��~	��i��Uh+���T��wg�e�M~���t���]i�_�fJ����1e��� ��=�ǿ�̌�V<�q�<
��4<$dD⡿�˂3׻�]�#i�Nd��L膿/�H��}����+d�      �   �
  x����u亲D�YV��	����oǋH�T`E��_��#��l�9q��y�y����'�����Z��T7RdE��K]jսR�dxFF�ʮ��9)���W��H�3����������jc'�rG�-n�j���H���$�.�.5y�����ĤEQ��q�7�o�ܶ�9������ʞ�R�NK�2�Ȼ�y�O�����ZK�Ҕ����n+�SVd�����V�*I�$�.��H��m"N���d�`�{tK��]J_e��;0�j�`~z����m��6q�e�u�q�m��
?kn�����@�s��V���1D��������	��K�h��v�w�.�W���4m¨L���w("K-���s��H0n�r�?W�8	k-��)]�y�^j�`�	]5X'Ǹ��e�5�0
�P/�9@�Y�O�AEZ����K-�i��ydC�b�	�y���CF���Ś/Y)/ĥ������i�ۤ	Ί���5�;kf���r�(���Z��À�C�D�b%m$�a)�~�8/��nmx��Ҟ�R�F��g(�8~R'ϻ��yX����7:.DL���r8�0�3�i�r���Ξ��K��!{:p�Ο�p�B�s�4�`�`�A������u�>��Q���i�6������^DY����Qۂ�"k�:.d�[jd����߰2�����6P�*{
v7,b�E�ͼ�Yr�_�*u6�e�<؇�
�����|����8oS���#V�KP�)+�ľc�a�!^�9ɡ��4�s�[��o�{hKMY�7�ܷ�>@�:5��FLn��}�b�H)!��CJ)�sv��B��Su����&�CMKM�E%ېs�2<����������b���jR2=[����!sO�4��#�uy=�5eqEm����|HݓF�gg�1ǞseC��~��z�Z��ع�5�,+yζ6��7��u����#YkV}ʒ���4��b�8�f����E�B����� �y���w�.�e��)>��!�&���Kl|��6Jl��;[�l��[;[�U�Y���	%�Xخ��S��^R�`�X�l��X�dm[�9��13m�U�Z۰d�������>�X�"*���a�lV���i޷&d��������H���m��48�}\��	RJ��u�+�eSg�٬�Y��q9/Bq,K�)��-1LV{Ga�7Ƶ6Z��dk_��h)§d8jt��b���$�vo���t�B~y�:۬ݰuMaʧ��=�}^j8+ȃ-��L6t�Z��a�G���Z�[3�*m���R�tGp6�Q\k,�J�m�j^�jXj�_�U[��>���:X��7l�m��nX�ͷ�myG?���6���f^?���y��8�g,Z+/ӓ�k�jn�C��i�v���+y�v�7��U�P�ڗ��qh�m�*Y�#�400ipJ��'��a�h��~�1p`xg�3����Jx�&�~ˑ�6�`��C^kد�3�:��:��CR�4Y��a��aaX�r� �����lV�b�ơ�W��k��5Ҥ���6���&Q�ю�:�58l3��[�hc}>�5�Qe����@�oŭ��S�|a��E��M�7,�9��z��e�i��~oX�̺��8�'��T�ϴ�������E�!�?��Y�9We�7��W,b���u5/�/�Z����K6��s����6�vf?:hdaZ���E���Z(`�����Y��z�/�_�uk6�i=�R(t��u�=c���m�V��8�5i`kg�=ci��υ�#S���Hz>/5�:�H�l����$�,ߪ�Z�m��(�r�q�9�hd�V���E�$��66��̓��X�:TޝM�X��:���m�5C�w�n�����YTf������z�ƺ������QCI��.a��|�����úayI��*���'VXv��/5eaX�f�C�-�5eaXuu�A�\4̚t�:�akg�5�VϹvqV?��/2wg�U?2we�-��}L�n�Y��'��$�w�8�p���Q�Mx;�O,�JG�3�+n[�ү�Rٽ\h/���l����2�;+Y����6lܰ�ě�Z���~cg�M7l���1m��E�ws�rЀ����1��U��d\,9�%rv�Ic�/h�b�Z�|�p�0-���6	ޡ���`���S��4�ƌ2��tu��h�7�洼!�vޡe������/���RPL-5A%��*�p�v�&�;ҏlRh̖X�/K-�Wpn���o���1�4{�:��\]M�������!����i\���Z j����J�]�5+@����'����\f5����W��3b崄��Gͣ&�����sX��(�V�_�H#�zu;YjHA?=�?�m��fMi�W���=Ǭ)�Q�@��~���>�|Ҕ�����ͽ�)��]��V}q�)M;c�M����2���+����3�7���p{���o`Z�wo�<��-R3?���#��.5!��7ܞ�ܳ.�=���[���u�01~M}���˧���-�7k#����2�� �7�p�j#�?\v�y�h�ho*1nD����1Wp�yaX腐�0���W�G��ɏ�:lM�恅8�S ����%��/��Y���߿��˦���U}L������l����S����Ù	�7���%L�d�ǯ��-���Î#/�i����V���Y�k���������      �   �  x���M����Ϝ_��ܜ��H6o� �;�`)�\A�N)�?E���-�7�s3��P;5�z��{�-No�������?�y���~~�����8���9~և��9�s�n[�~|� yzc��g�ϔ���gf������O��4�}���������X�R�0��j����Ջ��C�L߿z�����w�Z�7������:��k��{|���~��c��G}��>ב��Y�H�УH���H��Ge	$�B���%�s�Ӷ�ed3,�m���`�)*�-=E]��M�>m���V��δ:��P�`q~
$���p��.֡X����l���'�f>�J��׋KJ�#�a�|ل[ۗM���`�)� J��pRB�˲[����fX�@.�@*�@�<�@��pkdu.�>�{XG6�6	$�j$���I��HE	$��
r���y�c�sٌ�&��C�H����e!J�,J���'�=�#�-m�ZV"LqMr���@�m���@���#�q�:\{�8�5�!Ԣ҃@ў��Ϻ��*D����$�$��� !J����՟^������xx|��ͫ�?��l������ͮ��C��>�Z]�0���[.��\�#�]G��z�!��>���C�sM8����=2d��nk!TՈ p�%"����Lٌ��Ds�p=G�r��etKX��F�ݿ5Z��ͮ���zoC(���>�Q�ſ�Z��l�P�F\��1����⸜K���ad�k��!�I�T�(��(>�1�V���8]F!�ʌ��sq��A\KDi� p��G��o�4�.��ͮ!� �Z;@��� �����c-�Q�f�v��nqz+�ߊ�r�"�V��'��{�lƹ[��z+.�H�~+�݊ԭ#��y�f�#�qn󈀺}D8�@"\�B"��D"�7�+��Ǣ(�l��t��@�HB��k�#��wD��/t�|r�8����tq�G��z��􈀪G�خǚ�|�:����c"D��p]�夈�*E��Xۘ�<�l�9)"�J��J��I�YP$��y��6��5$u�RQ�:|�N���=f�!�mv)8�k�Pn����<�>jt��꼍lv=�@�~�P��E�Rr>����_lƩV!�k����2�hM ����"P+e��qs��n3N���B�����C(W{�\�x;�wi/��f���!\O@m.u�\����c41��5����B��C@�=�ŧD�v�q��P�d�i�����Kj�ٜO>��8����N��z�!��:�f9=5��܎��s�7���2�Y\�*�Z��(m�(s���O>q�=���s��q2�A\�� J�6��M��j>eOC�-�� P�6�=B\�#D�!P�(G�S�m�2��z����>E�H�*�!P�S'�9f:�r6�T���$���
!JU��B9_|r荤��*�@Q!�%Q!�5B��U�\�Mx�Cٌs*D@U!©
����
PUHb��r����vK٩�N�D�.@�rD�ᡓ,mV��Z+���fѴ!\OB��!���v��6��vKe��	��B%��D����%)�l���x?φ9φ�v�Qz�����g+��9����9�q���A��ĵ҂(--|�<[���T�ud�kHeA\�,��ʂ�gγ�,�;��l��J/���� J{J/SD���1���8�e(��4u�S�P.u�\����,���Ҧ�C��:�r�C@M	G9�uװu�q.u��C8M���!�K>�:��9�1�l���h�S�P.u���^|��I��f�Kj�NS�p=u�R���
�S�Y_H#�-���#P�x��^�p��!��u(s�R/>)��(g3N�x�}%��0qm�G(w�2�[拏-^�;m��0����8��!�M��=��q0�">��,OLq6㶩?;��\��8
���ą�'� *�3dX�O-�R��s?B�l����o=��x%��K-���J��|�9N
_��fܢY9��f�z%A6	%�j%���=��㯟�����i)�%_'��f�&
����˧#T���?5Q��O\�F6���M�U"B��}߄��}�����A[��챎l��t����*��M�,�wo��|��ʲ�<�W�0p��~��M��D!�K s���|�c!���͸��$`��� IBI��BI���$�"���AG6�w�q���������??��$\�xn�[<	4��Yg�ϳ���f\�R�]
E|�p�����6�KA�:����#NZĵV�P��*�x��tڌ�O��m��'�䁖�k��D�>����[��'��،ӵ��pUx�k<D��U�ٛ�p?!3��:���1��$`�xN�'�z�$��������Ѻ�mƹ�I�U�I8�����P��Oj�\�>Yb3��Oj�$��O���I(W?	�������ٌs���Z?�2���I(W?	�$��>�\���f\�x�H<	�H<	��xj�xP��^}���v+��U��zD��G�rzD@ѣ���r�N��lƩ!P�q�G�kzD(7zD�̔6Y�/=UXF6��L	QI�L�m���=^��f\S#D�!P�J[���nU�mƵ��t�D�"c������i3N�JC��lF��'�%ĵ��t��@�-mW���J���%nO��l�pK�-!JgK�O�s���5/�f\�x0K<	W$��[Z<	�j<	�ɣƥ\�3�ڌ���q �sߐG��<-B�	��D�j�l�s�:88��3�P.S�]��eۗ%�
_�׻@�)$e]��2��;�-E��!�I�T�(T��gs��mƩ!P$q"A��M���@���O8��/���q*A	B�ĵ(-, \f�c��'��x��s/��q��@\[i JWw������i䮫�6CD)הQ���~-�gً<��ٌ�>e��8�J�5�$TT�D��y�z}:��wWBT+	׵�PN+	�Z�^}܉7�紒��=�Q�\�2I('�T�ܮ>e�_]��d��*��S�$\�IB9�$�6�UH����%R�2��5�Mg���Ug��w�!0>�y�R<��s#�f���1�� �a��<F�K���;��[�ߡ���ս]����t�U�
�2��,��2jT��i��Ј���JȬQ%`Ѩp�Q%��J�M���N���MlVU@.�F��A�J�أJ��JȬQ�	}VŘF6��*!�*�=4�/�A�{!W}�߻�2����JȨQ%�S+�Z̫!�Zy��w��jEH�Vtj�MԊ`^� Y]�-��E,��*}�f�� �t���P* �|���:�
�]��.O{����U �
@@� ����ԋSr'��f�� �t��� 0H �� ��~U�w�c�M��f��W��*�_E`�W��UDn}
�wZ�ぢid3���A�у|l'��G�����n��=���      �   ]  x�u�ٙ�6���Q8��}t����a�(���}�ڿI� 	R�\�S�SRI��w�����S�3C����ҟ����'���U0�Y����'7�\�|jQn���)�0W��ZU��ˇ��`�S�u�'-�@e���v��1���aҭ��?u��p̾^���I]��T�(O�^�A*�uR~z�2�L���'�K�����JޜOmShdBebh�����eBeb>My��ǧ�X	��eڠ�P��#���m��=���jԅJDK��W�'9���D�=c���gӿ�`��.���:}���������L4�4�Ǹu�;We��_�i���"T&Ƴ�WsD�?�x3$0��zr6fz<��;�S;�����ܯ�7C���=:y�v�YX{�B&�R1�ݎ:yS�,�2ў��IN�f�戴t��A�i媌3��{@�ZU&�s�Rz�6���2n�>xU��Q�ئW�Ɗ&�!�J�HjH�_���B��G�2��\��x���*#4�՛yEHƦ5�GJ�ԛ��H�Ұ����#�c�x�0J��i�C��#;DN�h�o�!Y<���5�'5�@%d��f.�1!g��B�ZV*#�?��E�I�Y����V�C��H��B�O/�Be��V2W3��*##D�ݻ��Դ}˼�PD#j�P��Y��*�� ���:�[.�M^�m��H��㢀7�Xe����v�|��*#��c�r�M�PU&d���hI���.Y��xa��1�A��#�[T��V�k�[�ݒ�R��ל!��^[-�פ�7@Φ��Z5� ��� We$�I�cWe���:f��UF�M���HG~�fR�M��=��y5x���{<A-��SVn�/z�|Dm��-R�Fyˋ����EO^�'ω����(�
�U0�288uU�
�}6./<�1GC)Y�[�>JL���n$���A;�ۅ
�� 1:�DTPuU�TW�]<v�
��������
��jܒ�ڱ�Q��E�Y7��F�`���0g�dZ�\ܨ�L���`�g�ܭ��*�f�F{>g}��W�M�Uǌk^j�u�C�P3=ÅOu���}�ll�����6��$s�%T\-Q�ѫX������<��m-b�9�Θu���l�2�j�i\�6�W�V��^�U���`���;�>LSW�OK���	�����ﺺ���'����yW?�P�Lu�y����c���p������/��:��yveu(*3t��ӻ�jV�`�S�w�}�P�Pl�V	��;>�z�6ޝm��
���̻�e�
����C�}�����Gg�\4zL;V%;>WC�C���czU0�iջ�F̻���5�֐y�����2�e������옣��?�S�?���yw��
�����<t�4sU0�'׭ɷsU0�g.W�YZ#�@�b�*��������U0�'w����団��`(��0��XCy ~�⦪�::B���n����ʃm�._#hL{z�n��2Tte��1缛�ŻbM�*��b]QAu>�P�lPu�<Z����]�P���ƭ B�1G�m�
*��o�6]Pfy.�*�����Je�|�U0�ͻ�e�V�P8w�),V��t�b;�
f���[=d��Y�9��5�v�
f����>�혣2��e2W�:w�
�:!�*.|56CLy�6.�rm���0no���P�P�_<5�c�]��I��n���]������\̴1�+���
f}۲��*�m�'c��};*3;��y_����`��u�n[�g�u�.j�[U-�U06�-<�S�z�����-=b���� ��Tw��@�`h���C֣Z3};��l������[ļ��<k�y9���+Tb
_t�1�>(�VJ���B���*�b��\I���`h�^pT��=T��!y�oe�c�
��`+��Q��m �����~;1C̔Me���X�f����!���Pݓ�m0��h�L2���LN�����4ݻ*��Y���v����5���u(mmjS�*
SQ.=� ݔ0����m\�DX{K���ͻu����[��ޥ�)�΅Gᗰ��7�3T�,�CN�1�Lļ}���+b�T��Mǽb���\��"����\X�
�~йY^�h���n�s��ߒ���+�j�H����q�oX{�azx\7�l>+��h��0�Q����������v�����UT�Wt�k �re�8G��g�XSTS���4/U0U���sU0MnKzG�*���wܬOqR3�U��w8���	�E�n��$`�Yݻ�7���bx������B�5��R^��i�K�,_�iL1M��n<`ʝ���гK[nz\�P�4w^�S�L7A?n��N��-p[ȼ��M���g��c��Z˺�WU;D_ضX��x��U��Ɇ��QW�U��6�-S�LQ�n-s�[�j���[��)�-U0�2������1Y��mJ�`�b&��)�p�
f���j>�U3�� FÞՏV,o�a_�Bź��#��(:�FX��0��_������^�Y������$��[�+������h�R�c�۬B�#>�p�g�����N�:B3m�[��[��)�D�.��
f��-J7uN�*3t����n��2Y�m�o����BSL��r�y�~�[�6�q[�t���<^�6��s�m�e���*���[�P���[�3I偎j×�v����}_r�4*J���������n��
Ya �������%p�7��v��@�`j�V��+�j:-ë�6�p��*�a�nOs>w-�E]���3p���i^�����m[�I���t�k�T��\cݔ�R�]K��<|�0BPq-�V���
���g�z�#�LB�YѬ]�_�hc_�]��G�긨���TM׽�~U@���v���jA�"F����+*�G� �&N����W%T@���Y~ĠU@�K��}O=BP��;�U��*��[�vot��3��J5'�wt+|������E�TR�)���?�x���O�?������m�&� �X����~|V3�F�v��tT@9����\�J��i>+��*b�p>]�������u��{L��QgC�Y���{L�?��S��wL�C-ů����v���a�|��e�Jr��!�N~��ϟ?�f0��      �      x������ � �      �   �	  x���O����ҧ�/��,���M��n{�vĀ/b�8@���GJݪ��R��޼���UEJ�������#�x����z"�:�n��;�.�_(?Q�m�ϴ5����O�A.�E���FSm�Y�ق�VZ%��-n���O� �?]����c��d��-���L[�q�a�>�W������/���������L[K> o�J���~s���qY��!�mn���=��k��Ņ��P�l����V��t	,��{��KL��Y3m�,�`� �Fz��������}iC��0�֚�gB%,U����Fy���,.�O���'W6�[�3m������_>~�E�Zhrn��;qR�&.D�����L�[����|�}�W_Dn�S*�f�g����*�ʖ7b��N�+�	Mm�)����=�(
(R׍�L���qp��E?�@#��}����3�L��w.+~&�f�J$���I���V�G>�SEX�G��Mu��q�i�y䳍����Ny�<���>ʅ�+�1�Z���L�U�B��^:��S�~j�U��<}��
�n8�
ʈl���L[	$��l0���}a#�)?� {�f	�(Q�A��;�4�A{g��]~���v +O�>�g�J��K6.�&M�K2��c�3����+WeD��-��X�U6���*LchX`W����l�U\e�l��>�8ai�t���ad�k'ql2�L��<ä�a�A����(ql��;7И��νr��ٕ�[�����!a�q�P�(������ɛ/��85p���)�?JN}�&&������f�4£ѷ�7� ��Q�G{�ypF��ݩ�.����riO���Vɒ��N����PM�~��`�6��L���e�;��|_�k��{�ik�A�>���Cs�}��K�=�T����h��S{/g���;���*]=�(J��k�.!�@g+\�\����8����	��i+%�N��^����U��^�����6�qAHQ!���	�[ę�m������G�	YZ[s��K38Ҕ/׿��mY���mw�Z�Pr�\§�.��C���I������[8��8��g��S�6gw�4༼~�������0G�Q����<�0@���|~� -�<]������]QOXV�*J��"����s/a�b�#=R�i������PbL���mǬ�`����M�ȩ����m��l��,�hc�#�����o9i�<��G���h�eTu'�"*3tg��ت5�L�^n`�M��.o�U���[�
$�����3��@�G�`U 1��F�'V�0^��}!�	V�.(����F�k������w�J�ҏa�x_��Z����$���*��*���/ᎅ��N}F�k�x�wq�mT�����#A���alKO|D���YEJ���MV�S��7�'������L+�j0���-�y9�Q:c�\���;�4�U�ބ�(�Ph��~�\.K��ߩ%}.3Ⱦ��%���&j1���2<H�6O�ld������������)s���2�0@kP���@��W=�F�4��z<���|ȹa����Y}$p�����k����N��{�<���`�ml�q��[���S�{��i��4��L�B�%[ml9�Z��q^b�m̴�uP;��:�h�n��8/m#6������u��J�7�G�*��R?�3(4�� �lC�@�M�w?\o���v.E�z>V��2�$��v����7�����~d��,����ԩ��8\n��eG��n�ϯp�t���o�����;��.q0�ع�Ww�ѭ����S<��Nhh��񁶏Lh���Y�ކ�����,��Ig{[?� ����Eס`�?WNtu��s)��6����L�Eɩᙱ�("��#m�esj��-
R[u�%M��c-��z{i�4!*�΢B��@GM{��b��ꕍ��=�0\h@�@ن�ZMhrM6��m(k4�6Vm۩-64
��м��PW++C�H��S�C�D��%GeĖA��W��H��S��nx�_�-��-�� �O>ۧ0&��J��o�Y�-��hT�~��-p�'��o��!.��޸�7c6|R|��������̭�'���8� ����`]^��o�����v�L����īwpB[=z����7��z�Ǌg��?>k��L���o�w܏�$�L����<Z�<���\:��{^r�>�d{�i��N瓎�S5eB�vX�Aw^{�iO5��ص}�FbùȆ�/�ũ�^�����l4)�i��d�YѴ�(P��n���@E{#�F:):���y[��f���du�#4�:֪�F"���FZŚg#->B�g�񙙌C��I� ������5�:����c�����M������)�����&��u]�����      �   H  x��Z�n,7��_����H=��@��ԷI�"@�*@��9����,ǘ�0���MJ�����7~��P��D��D�������iӺ��b?|Re�k�r��������b�L����ŨJ���(uPf�QJ�o���bTS*�2�rJ�-f��U��.�_��M�(f�r-��t�L9�\Do*�<��&�Ũ���?��1�;�u��b8f�W�c:O���)�h�O����x��	�)s��v�f�p`̔w�nR]�L�s;f�;(�37N�.<�0�;!Y�d��(v�H���8�mJ�������Pʋ<Cx��bT�ٵ?L"a������y��0Pr@(e�Nz.I��7�[p#r�Š9�D���L6]��7c�r�p1%Ep(�b�Gz�Y��b�����8�-�� ����l��
�ƍ}�(+�c�q�G�T��=վ��Z\#��Q��Cq9�8� �"�"[�����x��B	=9�����2�DY7�Ì2�'[�����b�����eFd��u�г'��@]%"3J�H�;P|�q'.Vt]�*������q(5?۲cF)��8{�(u#�vQpì���9�����UeC�k���E�cj5'o�.�~���]�����C��\�&�ƃQ��;)�.�;��i��b6�>�I~��Zlv1���|���@O�0j�+���|�q;:vv1ز�^�t͝_�����L�6��01���Q�R�&Qx���6a�Kb*�.��C_�.ft:�֌�돿?���l�J�>���@��Q9���MXa0q1+g�WH=�̆a/-��Am�C��YͥL�c&�,9�v�g�����bFɽ���NA�B9bCx˨�zvV��q���w���'C�UE�+a��b��pN�3g�`h癲z�(ǚ���K
�PJ|D��<�%Ӯ�$��3��n��8�{��)}6O����*ζ�Pt1��J���s2۷
��%.f�l����ӰA!ԛ%��}߆xl�I}�>�f^U��k�Ԡ�x��\�uX�p˙�ɛ�`��Sv̦�8,�˜���z� �t�Y�n�|�k�J	�)�T�W����)�2��b?�oԤ)?��Ey�#9�:b�xf�0�`
hf̧nٺ8��bV�ng<��\J4hr1J6��z�w-�0�{W%��.fɭ}(�ɷ�.��0�Kq1+���ڻˁ��1^�*#>KbG�唷��A��;a�6����!��Js<Θ��g��6�����B�V��bT2*Z+?�p���M<���ڊ�Rj�r��x�����R����W(u��3J���&#�{�EU���O�f�64��m�ٝ�YM�>���er(ӠT��� ���XnV��]}�Ë��,�0����p�y����)��Q��[w�à�Wm��f��1+k�O�u���ݳ6�.n��_�~���r����<0nD�=�:4��@8��욏]�4�b�/�`����iOر��ԕZG3��*.�u���}��k�C�mª�|�	��%��[G�yܛH��2>�{�]��ܿg�2\{�׃���e��Ǔ���m�.u�9G�{����Ɗ��V{��N�j�6X����̔�M��{[>�5t(�{��k���Ƴ�[OXΉ)c�[i�c�g�CE��[�azz�q��:AjzV�[��'6��G��I�2L�]���G�G����Ń<1W>
i�8Һ���mlB�ŵ��] ���#��T�AP�[���2���g9�!��..f�s+��o?�vM#��5h��5���/��������K�u���ٖ���OrJkb2�ӌ��Ƒ�|j�W�u��3�&m��_���q���:/���/������O����ڴzo�+fg��0�^.���U"      �   �  x��Z�r�F=���.R���Z���ɴ"=����I�A�����Of
[�C:���_֒�ˬ*:�!�D�c�E6:�o�]��$��*��?-�>�H�H�2��"�"ޘl#ecBGq���8A�$����)�#���>�w�J��8�0�gm������U����-��n�:���e�p����-��5�l�
aLX��ͱ9�qd�_��ik����ض9���l�JB	���i�藾�ʺ�wy��۪/�}��n��u��k؈٘#�1�Fq�Y�/��-�yŷ}]������m���ɶ�7}~į�x3���~ǟ۲��.�s��s�򶩆���e����F��`�i�kܴ�.�}y˯��Ν#�b�Ô�Qf#dc@��e�/\����+Iq%�\�s	\]��i��>V���+�?��[����c�p�	c�<���3їf�tp������dE"���B�
o6V�f�˺�Us��O=����������l���e�cTp� �$ƯMS.�m�?��"o�Q�⾸ˁm:cpPC�e6"�f�3�Ip�1��i_?8�(��oq�daA �&�!�It#$8'X�h���7�隥���d���0&)�E���U����3�;��]d���1V�!uc2�%&�����!^w�.�y-�d�B��R%~�t���\UF�8z�*�K�[�V� By곦�+��/7H
9C��Djj�X������Q��P��s�cJW�\i���/]�χ��}ͨ�b*H�)#�^��6�m�J!}��-!^��Ҡ�� �����D�~�A�UhL%����|[V�G��f�����0�(�@�]9�h�}A�~��XE�!9��P!�)
�X�����B�J�O����CSoF�*�#����Q񇲭���|ۼ��.Cpu����Ic*��-}^��o^�������!�1E%QǾ$����[
~�6��]Yo���F��tL��x�>ͫ���Du��@2��SjE�� �0�QkO}�c�(�y"r�d6���ƴ��i�-��s�W.kbMy��1Di6�٬�e��ƨF�c�[}�x���T�����2y��MS�U�-f'#B�ۇzk��/5�Й����'����
K92�6��y�����r}����)2��=aL�D����$R�E\���}�nT�pNbe1�%�����A-:@~"D��0Z#���:I�����EW��yӷ�)�ab�aLc�,���v��~;ȣ�w|Qv9�Q*,�J�-�q� I�ƌk��p!<"#�'�x��&�����		s�����v�m�'���ƌ$F�o��|�36�IcFQ�8�̢�Aq�S�p�;?k^���#�*�-���I\�\VQ@���@�7�����P�0f�������k<�d.��.g)Ek-Ic����W��3�
�<�X��&����'U�ĕM�}�� �aŜQ��B3X.���)���AZ�ˆf
����!�:۩�{f��_���C�N�c�Rt���N��Z���p]˭bQ��8a�B`%*�X{�+ڷ��JJ�u*�5Ͳ�cV���ӹ������C�O��Y�.�R߭�7O[pV�M9ѺΎWy�qy���|�?m�ȷy�G�]4�H�Pc��Bq����U�R�b!�s����-^��F���4�*�1K!(�z7���,د�)�\��'bŇ��a�Z�@w��Y<�]	N~Z���c[5؀�P)'"Ez-��O�Yj"e6^l�B։�g��R�5mc���X�+:��9�e��^���?�7�����F���lF�PO��OCӦ]^������fW��C�>)X[&�%�J'S?VT��]��pl;��)���߇�<R��g�i����F��?�� c��q?��7�e_Wv��QZ����^`,�.6�4�dh�� ��
=�(��_&�%��y���[�{���^G��a,�|`��r�y[�G��f���CKhV��o��8�-�hJid_��ʺ o�2����_�4'Jbh-BK\y���۾k��ȡ�2���CNCK0h�<�l� �Dl"��eC몃KhB���!���D��w_��-i�j�X�\���*a�W����w���b�� דr��3���i�}�zw�mY������XEV�Wм�Yc)Ι���:k @O7Ιt�^}Y�i��X�a���_�ϩ���K�B�*����RE��P�����l�u@k�<�X�i.����=C���<p8���i�/N�	c,5���|���!���i��N���Fs�?���$��j��D���O3~z���x��rRd���%m_O��ciJ��|�N�^��?�0�f�:���y��sQ��q�؇İa�1qcYLyV�Z�}�m�ݖ��u��7DtD9�:�3�eTdE�鯚Ǧ���|`;�;� �2�6�q����{~�����H��	cĢQI�1��~�o����e8�G��h�X��\z�����L�X =�-�2*�f|�����m�2U�����_�@u�a,�(L��e�乩�f��4��M>eۙ�\Ę�|�Ʋ��Y�f����[h���p~�����)z׈�
f��,�n�.n����ոW�xzw[`,#ٜa����՟�v��Q�$_���[4���	��0�	;=-.0&�nɌ���W��w�E����n�L:�_��՗/_�8���U=7~]��L���8����`��\^(�ԣ+�ԍ�/NB>�'2|v=)	�d�q�0X�6[��Ϛ��U�]W��g��l�?��Π���B,��3���]�6ޟ�u�B�M, �J"������iJ��[�m��z�h�R��'̐ږ�d���,�W�<b�II:R��~�9��L��&ʿ�E;Ӕ$aiV�&��:��)���s|�wO}������w�P0ˣ'
��Q��������u�
n��W]��0X%��=K�|²DS�=�(�;l[w/Ȯ.����1N�Ƅ U�*���v�aE*�I�8�	b��r�QD���I�m���t���00��e�8�z���a��V4��e��7À��C2�/�gUͯ��[�����6��5M;�Kus��˖�v+٣:1@��@qF64��[`��?0��Bѣ�      �      x���[���&���+��q�c�r�0����k˒5���q^��b�d�ӗ�����D(\��P��3Z�'��U�H��Nv�������5'c��t��?��OW�b�8����������9~�pw����Q��g�&����,K�Ā4�B������K���������?�7��:	5/��+˓P�-T��%�?�?�Kc2H�o_�=<�(T���D�j�.J�I�2I�T�{1�J�ؘbyR�JB7��B�؆byR�NèV��#��O�&{����������y������ç�w���_�����O]�����ے�Il^0�j��z��~�4�]̃'~��|��3�4e'v��_�
�����O��k�W���H8/��	&��#��,������g�;%OҤ�N�~���H��5�O�^c��+�n��>yS���IlT�Lr�N����n���mܜ���}��Q�*��o��)V�v�'l[�
bu+�i%��	6�++Ib����/����٤Y5�������<�z��������������������������b^���������~�pd���g������x���y�`��ڗF��;����X��ܞKlk{Ϋ������+�u�����ώ�E�����?��������J}���(�>)�{�����9	��?��Db;>A����/�t���;�����%�z�_�㟎^��>}��H�������O�w�c��|�x���"�Y�P`2n~9��/��K�Ҵ��aI����GO���| �����#�@���\�*�p�?X�ܰ�~���g}iȕX��W8���*�(`�Dӑ��y��n�Y�◬ ���q��U�&��D��B����b?��o�z��}x~��?��s\���s�޾?�����O?�~�{��ý_�wߜ�?��?�;����mC\�9�I������Hj����5����3�9'?ӝ`0���v	4�Q�HldF��K6	R�>Y����L�����z��۫�k>]���5kw��L��a�\�Ü�N�>\sy�U�#�k���q���y�����y�����d���Z ���p�p�B
���!��Hϧ8	Tw���;~{����{�������-g�?��룢 ^�9��J�w�wwg`v����������g����燫?�{���~��|�����9����>�|��n}{q�|�����/��J����}F8C�i�����Wb���X�n�w�d'������;>}�}w���@�)H�M�3C��(1�Xiܥ����I�a�S27e��ׇ<\�y>>=����	�|��ɰ�3���߾�ɓ4��]�Ķ�P�~t�����ۏ@�d6l�m_0Ib��En���=��yې�y��Nb�7��W�+�_i}��&��yeߟn������� � ���=����_�O���+����&�j����~K[�Q����U~�¥):]�Ł]Db+�2/��u:er�ˀ:�k��X�߯-�|���0�7Th�:�EMF��帑��f��X��<��󁫥������ᅿo9y���/6~��8��Lb{����'��A[KL��|b�b�$v�3�0y&?O8#y�K�4ح~�H�X��H����{�O4�~=�]ڷ�aa��|A9�:oQT����؎aʓ]��J+@A��~�|���U��$�Ub�l��V3�i�мzŮ^�W��|��^�W�n�a�+ӣ��若�F )}0�/<����I��So8��<�$����ҭ?�L�H��e�J� .��-�Q�6�^8� ^��sA�بx�ŻZ�b@���*�i�$��e���R�#�U�LSb��m����H7/&Nb�wŬx>�֟�6{�;����㧷�Go����~�Lך36`챃;����6p�`�!hh�%L0�#�a,3��A���R�M'��a'��y��t�', ���J�e�a��8�N�b.-�'��P�+�յ�VW��|[�5�me��.~u�=���E�ev�j����^��k6]3ϒ�k�y2y��2��b+��ɜh��
J4�4�g�[�H�C��^3D��_s�LԔ/%6t��D�>��e���@��n�h�A������{��^s]�<]��^b�h���x�չ�2�`���Zx�B_����H^��ՍXM��f#C�<CF0�!������_����������?���w�����<������-������s\;"���u���ZN���ɯ%l�Z���Z�����$6t��(�5Q�[Z~����-��M������t������������t�����aS�+�XcpЯ}~��Gl�sF$��`x��"\�Y�Čm==�ď��Y��O���{�o���^
 ��$fÖ U"�:b����< 6߈���������p-�D����0j�$�!d9�^�pY�Z�lZo�d=�=b�rr��}>������lT:��]��r[>g/d�~��g�)�sMM�W�R	G/zu{d�y������El�8���7�EA40e��T��������3�޷-��z�����x&�ކ����o�Q����x{�s��~x��x�x<���oSs�f,�y����,��D��3����/�~v���J6Wƪ(z��p����2��"�����3+3���R~�^�t�Ҟ�N?~�cy�{��#�g��W���N�����NzG1��d5���s��/��� �5��1�T}q��Wp���ژ��}A�;�ZA]������[�"����Cb��LsSߔ��!��Otbm�`5 ����Ԁm���`�\��2���H�R2yD�Ԍ���	����tf_f��;*~IKb�S�G+;˚g�jH�?U5�-����5�l�D�́�p��e; �m��^	4ᵬ|W�s�4���V=AضpV�� �U��-e�Fe�,[׮�魷4c��y�n*��᪹Hgle�G�����cF�R�#"͋���,�t��G�v'[�6��$�L�nնL�oO�^ݾ��?�>㫣���z���go�R?�^�LDR�
'�a��G��G�K��̇ף2�HY�F<3���;ӭ#$D�S`#k1�_�dej�QH׭�"a�֟������t�[�q	�ઝ�}v���� m�@n��z�-�e+l�IՀ@�<el�9+��5`l��p��~��5�(���$l;8|��`�i\-^l�WS�Sb�!�:���>����R��ʔ�[	J��H+�����h$ls��sn��|5��l�o��c�7(֮����&�y�}*�P]Xqv�C�!���Hl��3��! ��,S�Ȅ8Ib���I!C�Aƙ.����?5��d%�5�GR6X�p�F�i��16���on�*���
b;��Äo֐�f
*���^jx8�����o�"���SLZ"�
	��eHx�� ��7��
��B�����-p��j,�E�QeMX����)OL�͍�9��ؚ�81�t�l�����A@oY��z���T��y1z�W����.<��xD�rH�z��ç����O��y��/n�{|�}w~x~���)K�*+^чV�e��@�<�b�I||i� x�%�mo^X1(��B�=��1USu��0�i��-�5��m�!8�c��+�ݞŪ*d��P`�,r��3�!�W��NP,0̫{����%��������W�P�X$��y5Knq|���t��i��Z��Fb�>?��-'������X�v@�����a��<`�x,��l��.��4a���Z��Q�!��O���H��_�]�L���[���P��5�,0Ib�|��V�Y^�v�fGN�~��s�]�d�֨vl�&x;�]D*n�q��y��Y-���c��Y�̪�D\ZĂ	�7֊��:�Y�'�ɐ�<׈o߾������۷��ż�����i�M�1�}�N�Ƅ�^�1{����#�[4P�hԬSb�$1J�����Oj.���������_N��Vq|�����������$ H�}˥K{R� �:i{��x�v���    ;�on�C��Db7�?=����ǻ|�\�Ђ��3��̦�Ӊ�c�[U\���L�L/��n������c��_�n���{u~���O� �o2�
W9��[���
v���M��6]]��sIOY:�����а@�4Vn�)1��:�h�ݟ5�f#c�Z��-��X�H(Q�a�������[~!����T�UB2l�pZP�Hȩr��R�53����t�'0��1r��/E�*'Ub��>��_BD�stK�:��O����b��r��G����C�*-^Z��iOȍuqX�US`�W�0�R��A��l�,��^���D��X`��lf&��ė�̴�+aQJ`��������Ғ�x�P����@������9m�!TN3m�5s]�礉�w���=aZ�Y#mekؖD�%���j[���l1��~��>1��v����Vv�yx���o��`p���2�Z��3���&��Me��vP7'5��ƣ�3l��K�'�mkv�Jω*C+�Ą���,��s����#�EZ���P��w��S`#�L�88�y�����Ȭ?T�,��3;�1��j)V��/��a�����ٙ�	�I���d���,R��Wb�����Q.��p���8����*h����Y%�X�B��d�p?����'� ]�R꣭�v�,��B.QTU �Q�LԔ~�-����v9e&&����l����[Ξ�!�P6KTm�Y`�*F$
?\-��?}z~ ���)KvrS����;�����/6M���x���	��3�����_gj�x(��Z�9Np� ��x�K���=A���W��|������o^��������m����6�����LY�Tw���Jya=>$\��??A�W��ͧ�o�?�TeI�q��j�*�k���Z��Q�3��0�+9��Db���O�K �^�����2s5�^˵��LUڭ��|����Ӗl�y�6��k=��mu�/�A�:KV�d���ͳO��3%��/D�T6��X�v������tR����TR���b�{oCCX��h�X���V|B�p����S�� ���t
l��G��l�%V�9
��	���H�kxc"�H�gzN��
]�J�}(��:�����8ɏ��?��c'x'�J($�l���p����s nn���M'��!:�)HlX4Ϣ���`q��P�(��Ib�;ԣ�"3�A���k����9��@W�f`��@��p�t��x��D��Of���ƒ	������)t��)���Γc��������QL���߶�e�\`�x�d�\�R2C���v7�f�7��z<����%Q`��D�f�N]/u�*�uf+�f��󒙗��U|�{�J?��A`�]`ii�����g�=��ҹ�g���%-\��s���!���f���F��NE�p�ь��Ӱ�b5.S�1���*g^�m���Q�miN1�L�#���?��W��
lĄ�u��n�s5.CԲ+Ko�t`�jK��)I,ٖ�xuw,�������D�����_���̴�=����沺;������=�	���t�I�I������F0�s	\��
o`0�u����7���K�!e�5c;�Dg�Hk�3Z���(���q$>G�>�#W���5�����Wwo���ji��e�d��O�Kb��?`r}p7G���1��,>�C�z���(.gHl�Y^/;�aW�"�,�����*)��6[��S�Bq1�̛@I���W�����p�c�U�j)a��ș"� ��{)Q��R��]C	[v���XXen�_�M4N���|1Ʉc���ٹ/��f�A�u�BW`���&�'���Y�ʢ���~�u�$��{�"��V��yO�_0F�H�����d�o3��A94;Wū�BX��B�RYk=1d�\��� �e��� ŧ`�!��Cj[|��L3�t�+�i��Gp�y�&�C�[S6ck�EBt��"m��DjBd��g�-8�e�i���1�4�����G)��L���	,���<�HIU4��9@Pqhj`	�Es�&l�9���5NpH���0mVw�m�0Q6���S��v�����������ӱZ����1�'�m��.7Y4� ���������_������᏷n��Y���\c����"[N�e�%Y�ώ��!��u�����p	Y�%�!�E�Q��Hu����eul��y���VE�:
���t\���!� �Ru�%X���Jj2��	���ĮH��4�z:k�:��S�Db�,n��X�!^����=��M�wP��0v��u�m�M��@�����e�6��q����i��..�X�ɾ��?�5+�^�FÞHb������r�������y'�z'���P�R�*
��ض?��`�8N0�Lj�r��DR���w�ǇJl�%jl�k3�0m<c��Y�گjX�̴�Z�^�^�[c[^�=���щ4j��{ϵi	�+���Ab�}Dδ��І�C�^�e�,�,��Vk��[r�C�z���l�ԉ��椛1�Y(Q�Hl!I���33<��!�%J����i�,��PFIr�wZ�	��4�UJjֱM�;
��Nb����!<NBЩs �JC]]�FklT�����leJH �`��!W�v.'lD.l/2zЅ?�ܜ�%X�n���`~�S�`�o�zJ?�\ӘQ��l޸�4��$��;�[��u	Gb#�!��(Y �Ϧh=�6�ؐY�]z�`x�CQ���x݉?��2�3d�+�+A�x�ПMe�}��x�������7�`�xn��q����T���(�f.*Rc;U"5B]P�*��+�X~�΂�iSc#q���|����|��7m�b�̔(�j+%�mB�n�[t� ��:/��_>"Wϱ�5�!ׄ&F�s��?C`�����݇OiX������I���y�	�ť�X�"��T�\U$z�ض��TЙ,@J
R�A*Y��;��ؘ\������7��F�-�?b��X=��\3�1��M�<�4D#Lb�A'�֤Hؾ�]s�i���݃�tV�1��}ͨb3q��-w^�t�۵����1~�3|nݾ!��/Zc��J�X�@𹁻�]U�Ǜ�?"j?_zS?Z'5�^n6����-{f����Xa�w�Ȳ�L�m�S��8�I�41H
7�'��𻎁h���1P�s
j���H����D'_��o����D���fu����/�D)��[���C���/_�6Y�6��,ߤ)�_|����B̢�;�
o�}�W|TVc��u����]5���^�]���f�\ļQ�P�s_��ȟ�f�)�saiV�P!6ľ�J��6����ΰ�3�iA�Gu��Ô+9�9<��pS���M�	β��E�Ox�B�3����ux�$��-Y(��B����)W"��?�*3�;��K��`B�433���UN�T����8��7A�z�#�s-��OWe���yN#L9�T�~���r����[T$*'u�ꠤ�P���?��xz�_���i���׫�z��ȳ�k��u֫H�i����$kl��uֱK�*�Z�
/N�@{Q�(l���SL,X�2�0Y����"L�8��C�n9��
V`��\F�����"���+#��]u���џ���B�D�#ӹ�E̢��.9�3!��M����X �%2=W�������	�X��:W��W�*�<\?��n"s��LB��2Y����s���6���9s� ��?�y5o5x��R>����Cׅ�C�;�a�M(4�K���������*�j��*A�>R���En4�?�?ڍsJ�F�m���F3��	z��!�\��FL�?	o���������oZ���M�����K�6�{�(w��4P��������f�
h�6�Uko+(�5+!c�77č$y� ��lS���é�
 �T�۳�m����`��ɤ0�Y(����`����[�-����������"��C*����A�\2@��
��6��-�"?��K�%��ۥ������    ��A�#J�t.-(?�Nf��F)D�D'
��l�۵�x�lʴ�eS֝X}�1s�M$�Y�gγCf��i/9��˹�+{�? �%�Qj:Q�U"��rL{�$l�!#���f�>H�m��EE�ʉP`�dt&ÓTO%�z��1�09ͥ�k�Bg�)�'�\�z"�=�jJzj�$y��-�/`�3K9G~������Y�2u�iM��J,��#�������8���*O��ឞ��nĄ��#a,T!�^!̭u�[^��ޒ�������ky/�Y���:>tO�w�RӚ� a�3`&��R��B%�j�D�����ln��Ь+4�:�l`�X��=�g���hx��m�xf�ǉ)2Qdf�~�~�Og�8�㟡-�v����sw���7��й��;�Q��fl-(���EΊ��f�$ J[*#O�?h,ޒ�����J#�TB�(a8IE[����%A���j첝=��ҩ��� e8��YQ�U�r�����E����pH�
��4���y��˜�vϾF�C�t%�̙�56��`�ϊ�@B�	"'8+�nO겳W�x�v[@lm+�klL#�Va��Y�ȌTm�ޓW��\�Ě���u#��3v��i��W��E����El��b�Ma��=M��8��0(�:"�����Y���0�	�	1���i�1���Vn�I&��x�l�9�2�@�E��./���YN�0#��PK��/��(�+���G0�'�E¸��Ue��B�a=]����*�ZcyP��;?@��޲F����BlӬ���述����3Z6(�S��J���b�2Tb�h�*���˹�g�yjmzR�ۏ���JOk��Sf�[{?a{؉��4Z��nˁZ����hY�w�4Xv�в�A�<9U�����LXW��Wz	Ғؖ�����)��o}$���lK\^�����W�t�F�yK'����H�}N'Q�?2�ʷq��>�C�L�*��6�U���8Q)L� =u�p��?�d�z6,Yg��X�>֘�CI�s���l�d��0�p��CI6s���h@�d�����B/
�c���C�h�2v�y���b{���?�R9C)��w���"S�����G?>0�R1k�'�s�W���'y���(+a�9l�M.� �6��>��ߜ�Ω�eK��<Ζ&<k��"5	�������������ÃJ�&6KVUJ@���+
i@�q�~ݘ�����)i�%���̤�ԥۍ7a�y&�����@�>N�I�Bcp� /��u�+)t�k����F��>�����wt9}Nhfd, �p�8Z~�S]��fs'4/Ѻ��s���Gv�`i�y�|sA���	۽�5�T�?~z��j�h��?��X<���v"6���z�1CT�hs�ܾ�Z����6����F����P|Er2�@��<�1�Ch}�X;���Q`e�ϥ+�\�+�����g:B�e懳~NЖҘ�ӯ��J%�%���rr�H�V��Җ�7��1�ޖh�����G�-f��e�)8�����m������#��1���NS��a�6JJH�Y���΂�u��EF{Z6!U��0�����	�VA/����Tu�*��B7�4�8�*��q�ٲ�\e�W��hQ�0LS��x�+����Qn(��,�L�)
}�X^E�j���@�
6��K��p{3L'm�]ڀ:��6+E�q1�����L�ܥ���;)�*<S�I��D�Y=�e�����.������X'!�:�n-���e�������yb�o�����uǠ,H��<�2�X�.�ܐXm���ԉ4����0O/���d�\7|���f��mr�ԜYp���o��������A�T���:z�g��˥a��@�A�U��
�\�9/��^���X����*�c����4�X�D������1�2VT'&u	�#���m*�R���vY���m$�ʙW`��kD<"�R� ��I�6��X�0aU�v�]��e&����ƛBp�j���N��JJ[얽:l�&Ŵ@2����P��$ښc26���ٖ2�)ޝ37永;t	�/=��>Dl��/�K4h��IdGl�:����J�+�=��Y�Y����#čO�$U;o<ö�RaUBj�]�A�5(�hF~D�矎2�p�� �x�#.�=�tY�*�<�]�æ��/����Ϟ�۟��τ�dKb�.�z!Nxꂃ7'.�c�R\s{����?J�a{+ZN�m� @��e�!��bݜ�$�����o����b$�H�J��*�_�������ړ��88�/qxR��v]k�FN��ĦC}#gLh��0��&�h��m��(a{7�H+IHsu�+�:hf:�~��%�mS���N�I���DH�P~�K�A��7!���;?�������������?u=��|'6WxҜ���ؗ|��@o+RNbۛ�Hұ�����n��C�/Ib[Vq�,q�J���[� a;m�DD�
TE�Q�3'c�gV�����w������� �PŊ+�P���%/���L�T�EVt��M��4��G(��Ȃ�Q��78EȆ7���]+�Л
�P�ĶN��q����+���%��W�W&����1�M$���J�䀗�^E��cދ��l�%�V�m���r^����b�ȃ,��8�`���ӄ�<w!���U�d�e^��4�ɵ��U[D��/UE��RՈ�
�O*AQQJ@f�J�T=�t�� bM�Kغ�&��kL�%6P�TL���qU���xg��2�K71NH�.n�4BDY�RHز���`�pvC3�)؉��+��l�y����ʀ��������NhCcډS�/�~����[�g�څ�����y��]@Y���"�M�$��nM�6��N$vau�j���HFrd;��Kn�2�Հ��Q�P�����jnˮ	H��B���:/���/7y�u0�4#�9�N
Z#*y2�ˢl[� 	VE�����т�8�4(�/���]�ӷ偐E�U`8�˦�74-(6P�	ʖs�ڼMKkJV5
l|��-0�3��6(��O�&"��7#]m����'>z����W�T�̵�4�����F-��4��q���>:�J�[t1�*��-!��x�-�o �!�lH�q{~#^e!�^'2K�h�ۨ[}e�x�)��-�#��q��<t��*�h0�-?�L�ߚ*9�\V9Q��ͨ�ߥf�Z�-�j5�fF�������yϰ����h,�'(h��m��_`uR��偑�L�\j�g1��C7XIb�i�<����<�wY/r�����i�"�5�gVc^�,�0"RUy��%�d�j�H�c�YH}$�ӱ���L�it���Q5Z�r�%�Lf�R��Z�3:�x>���?���*������I���s�d�������������������7�w�Ϸ����`
8q��ICc�'��ʭ�x���!��
�-�-'bb7���U67�ld�hV`�|�D�^H����������ݫ��4څ�;��]�Ν~z
cۡ��i6���Q�O
��}�PD]�������#�ȟ?"��q^`[U̝�V�T��WP���Ms��خ�a�n��U��y��K[`��P��΅EW8qy�!4�Ćr�sEL���O��/�8��R��CJfR6+G�V�DA�x�#Ϣ"=��)T�F�ׁ�wv �8;Bh��q�pbwB�ry	��Y���~.�S���tf�������ӫ��F_����7���jXIU�v؟o�2�%�11��	���.�(`�C������xs9un>�o�N$���x^��X')N\2h���5S�L��*R"���L��k���]�A�xJ�T��lr,��}>�Ʃ������%� �:�HS���b�˺@y;�[��fE�7�[���v|N��ۀ0�[���e�_M� K^=�G�l,�Ɠ��v��%�)K���{�.I���$�[�X���ߐ�@5 P��彌z�Y�ɳ`��`QgO�vev����$�t�[Z�?�c�c�9�TL�g��������ڶBg�    ��s�����n+���6�$V�8T�LD\b���,U�߫M*f�+Cb;
��bP��xEB�+j-?��*4������RӉ�_���"� N��.c��ׯ���Ϫ��/��T���zH��+�x��W 4�q���������ݜ۶����Ӝ�_��z��T�M�
�Rm�j�B3������rT�Shq�t�m�^mۉ2LU`�r�>��[��b�7U���L����>�����V�6���b���9�(P�7h�^,u|���B>�K�L��P�!4�C�T'X`뾡<��ҧ��T�ˬ6��1WQu�e���/C%�c��s�� A�~k���_��d;x$.m����kj
>)����؍�[�����I"�O"�����_V����
פ���S1�pٍ�.x��'c�� g�b[��f�%��sS:�{)6�n{~@QEb���	����vvif8�*'M���Tdտ��v�˓����8�ĺ(ɵ��R՝���_�ۣ3�p�S|.n�si���;x?�z=(�u�:b�xpAP�T�s��ڻ&�����6�8u��F|>X�*TT�����t���{�����:s�p��m��?��y���0���i��d�q����a�AU��}��g�r�`��������KX�-ybd.?jj�6ҳh��f~����J��>���w�{�C�9rq�M���H��U
l���$���h�/��I�eEP��P���2���z�*��	�[�r��*»;Yh��HF�A����L&w���0�N�B���'��m�%Q�tnHl���|x�'�z��R2`we��T��ؒc�d�<�z��Td�7���x��<�U^w��Sә��f�Y��Ġ)C8P�� lo��йU�t��A��C�
lɑ�w$Oh
�~Q��e���nu�:[ ���&a+���+�R\�d�!�>�E2�*�Q`��xd�y&s.bpz�Ɉ��B����nĖ8����w+�|}��ΐ�J��!d�Ŏ;1c�"����0�۬�`D�)�1)x��[�N�������&��4��@�K�M2{k�D�$$�@P���K��ow������\�%W�q�$j�zͷ��Ek�&l55#�	i"J���&6�1��+ #���'v��𧱦vQ��Y_!�SI�1�Ԇ}Cl�͈|���՛ �6�L�]"���	y�g��5Rh]V��)��'gC)ri�vSa�z_-�=��]t��n,g2��W]Y� �-U�Y����{b�~m���\p�@`D�	ɐ;|���Q �QSk�&le���
���߆T*�˽�����\��jt�H+B����T�>�})��&��h�K��j���v�m,O����&h�������{%>�����۝G���Ilg�N(��|(������A�~���N��ڕ���<�
`����B�E��Y�P���*X�@lIp�4�po�"I5�R���z
l>����er4!!p~ ݒi���u��0Q��K��'���x��v�E��l!�h��ա;Z�������bv��8Jz�f���hd��p��n�i?�B_�]x��/#�*��Ф�PY3U��3�>����oWA88D$��?�d:�������ŵN��udDUL��v���B�s]����)d�Ƀ���������[����,��$��a�zn�Tm9E��ߐ�d��]`�u&�Pq|��Nu��Y�ڼXn�/{�u߫۳�̫ �-�����2�L"�{.�5UT[���V"�)��eA�.�f�b[��W�X�b��� �1OD͊\e2{bV�>�P�
ؒ��奋��ՙ�)�aM�?Z`��@l{�M�Źe
�&�T��2�o"�ڻ�K�g�Ķ���'n�ڒ|�&�����@6W= ͬ�%��\�ʎ��`����Ř�0Z�h��7��HYy9
l����v�Pg�:fn��`σ��i��[gP  ����b��:��LN��N�;H��I*��m%�!��:El�B�#(6�E�X+���mUɠ�V-���>HIM�l����%B:7�ҏB*H�~��l��1��Q�,��
���M�����o^��}؞%g��ra�i��;9X��� 5�"'������9枡8Frt��0���������ۇ7�;��>ܾ�#�4=Yh�bI�a��&�h�銍�P(;)�MQ�P
�����p�D9�2w$5ȵ��0��YE�k*$��fb�CPV�;�e̙\$�Bذk4.a�Ć���!�<�;L��"'���ߑ=A%$�������h6PRu)����l�xq�D�e~�~r� 7�==f]��U$n�mӔER=�X�X��y���}�e]LsU��. *׈rW���+���HJWO��Y @g������<�W�#��[N��iw�D�?�xF�ζ�M$��T��c��fv���c٨֌MX�u��	���+�Ӯ3.��څv�{v  kOÄm��O����y�ZBx�E�~l����"�p9\�h���n<�.�T`�0���б�e�7-m8��9�Bi�
bCپ���'�ʥTho\���Ǎ��;���6��/����('��	��I]6֡ް$���A�r������^tb�Z����A8�����I2'�٣/;C�^����c�#��â9�Qx��n)v��t�N��H��C&�ύ"�Nc�F�=��F�.�/���N��'�x(h@���Ɓ�T�d�*
��S�B��ʑ�Y�hwVK)t�Al䔍�Y�pm�� 軳�t����:���F�@�Y�è��=����.�X���6x��h��h�,���q��B(���[`'{j�泞�&Ϣk�����経�rЙ���Yt��భ��=��!K"���B��l��x!B1:�L�VǛ�ϟ�����.��f6D�O�3	i ���b]�i�c�(����-�5!����ρ���zƭ�	���/��C�YR\![veO�N4�X"6v�AӜ�C%��]�����D87ʶ��m�F,d��6)aQ;k[$���[$b��I;!����rQ-��3�Q����+z7��8�#_.)�ګ[ٲJ�)�!��Z��_$�5s����C��jFf� �/5��"���ՊZt�3"6���"i6Y'��ubZgx�Fu�-6[�<[��{��b.Il3�ec9<�jo�dO��<��P��9�X��dׅÏ-|+Yj�ɞ����^�`�d-���I�@E4��%HE6��6�V�a[����(d,��'��li���
�E8�-��MV�k����i���KA���HlK�βY��䅖��-T�3�-&�(L]�@�BwG%E��	Nb���q�,�9͖�Z�<m��hp=��`�.'�3AI^�dwiv�k/*	[�x�s(\W�Ѽ	I؆h6eњ����nL¶d�,ې:W���9�$�˦�
��jn�QY$�'qKD��l�.&ǦL�(�-�CE�%��E�-��B��u��b��W�r^��d ��5���B'����߼�Cx�z.;�V:���K	�Mb<��ׂS�TG�j;n�l݆�$lgk�������Ⱦ�&4�i�b+�<��C�B-ʲ��+�Ad\�ʑ����R��ε>,�a��¦��E�m��b�]�}M�H�6"����z,�\T�����+XX�Ǧ�����lDDi׾ϥZb���$Ժ�-.�`ho���vW�qIHi("֝��-���H�Ѕۓ���9}�quI����(yI$`b��1u�DƖ&4\F=�iH]�� 2l��+ʷUi�[�� >�xN����젌	79��"_�'Y�Za�Q�7
CQ�ґ_bM���C����0r�S��&�uEM�/�xJl#��eZ�@�9,a�]�}��4AV����(G��9%�[\�O�)f�I�ȁ�9ȗ�|��_yp��m�)��=a�Lt�J�*:��-D&ǟ���(��Tb����+S,����njQ�خ����Sb��Xfd�G���IG!΍�b�J̪ǈ�?.�y��`=����#kaM�;��u�{q2p�_�W���i$��    �F=?޶�l�%�^�!�b	��K/�-1C�؀/y���Q"*[pLz�0eB�d��-O�J�W�#�}7xe��إ���FѨZ��[RP��e�)e8�U~��&�wT��%vэ,����D��늃 ���C�M�'t�+#�Jl�2΢��>d��T�[~�&�ĚD�����{6��ܖ�V~����A�F� M
cz1rPS����6�@Ǖn���S���F��F�?�9��V�/j��p �1i��ZX�v)�S7a��ñ6��|=��p�d���~��V�)��%�i�0(X�b\��\<��2n'��ğ��ʮ�����G<&*j�
L���h-ԕ[K,�f-K��0� �ȩ��
0��/[�'a�yOl�q�M�T�o6:�|x�����������ǥ�3e�@�����������=?�_�h�w��)��o��o�jO��R��n�ےXPSJ����=��������~~�S}68F��eBC��.��������I6�����	��b3�l��K%��ӕ/=X�CkE��,�b�f���ÞQ��W�<Xa�虐�/��*����8��X�߉��,�$��%pVׄ�2;Θ=A�NgB嗪yi�&���Zb�l�Bu0�-��H��߻v�C*^k�d����� wH7M\ݼC��YMJ����ee�̃ۑ���R�5$�����0�?�!�bF����}�1:K~�A�h�(fLi	gq�Mr�"����F���̚�>�N�s.���皶��'�錎ĆG�#�	�YdTp��N��Ûa�OԂa��5'���;7j=��[���_k�����۵�$�]p�yS��B+c$v���	�u\��(��?>{c zh�ľ�ϊ�
�d�-�6ٵjm88�Ib��0�y�� 05�Oﺢ�Ǫ��%�yfhk��}�3e�r�m��QS1����r�5g��8�qJ��#.���v�{��͞l���^��F�@�blQ�Kwe�8��T��%6F$���T����\__��c���g��)����Zdt�,TtF则���ɕ�LJl��e������k�����޿(M��&���K��L���d�Dẩ)�MZ(�Ӯd�����vo%u��p��'c�+�������QFHf��F��Le�piv�i�Ѳ���i3�=պy33mf���Q��Td�5d⬰;fE�5i���]l=����y���Ft�i(�ľ�����Iռ7)�O��-i~m"S*��2ۏoB�q�]�q)�X�ǉ���w<K5�����:$l$�9�@�n�$A(==Eb��[ᶌ�+��7�H)GN-먩�&�4�5Y/��{=��sK�-¡u�gbR�;7��-Q����8�wc�l�)ލ{#K[����F������*��_Պs�[9c0�<��X�S�Ḅ���*��R���Q)�#"35��}�D �� 6�z3vQ#�9�QM�&ˍ{ln�㘅#61	�R�|A2x�ڹ�x3n���:~��W�񾐞{�����8�Ϋj O���@!�D�Ժ�v)��:A���v�<�sRvO	��Ey��p�1�~�'3.}��gO��ؓi��H+Y6e*���z�#S8 �D���j��4���{�V�J�B�T�:t���m��G��z'l�m0<Z0�R��T��C\kr>�8$��PV�����p���	[��,�wP�ғ�4����/>��oC�h@Vx;���=�b$`�u$��d"�my6n�z �B{L%�k�J����
��+�;��2��S�oz;���W.��h#��ɟ���鶆�$��
"Ϛ� a��s˭��	f�ю�V����S)aÞy]��c-���vX�*��$���t���|�]���|�i��J*�2����	�LPe���]�!~���%�����P���`/r9h8�i�.�'3?��U���`jb�`�ļ�E����$�"�1��� eWT������.��6tk��]���˱�y�͗�b��N4��u-���Q�]�e�������y���S6���&����b�֪2�x��!@xk�w���%d�����o��Oߚ�M�O�.�Z��i=X;��d��Xv�U@G�Ï���f�V��&fnfϻ
�^�ËXHZS��r��va��]h���V!�m��'5�fv'�޶�oD"�E�E�b�ԋBԽ�ZGv�#��P�\L��Ѭ�N�T�æ�=�S�[��Sߐ��r�7q������G�����E��^(�en����J��r9�n:g�.r5̙�0b��zW2X�B���������D�ʖ�/��Q"F��aЦ�L�G�����{�.*�ь�>#ҿJ�B,J۟l��5�o�.�����c�̎m�����7���+4�)��:[����7�����m���:Gw��tW�(��Hi������@�?6�Yǟ)�#�����m��T��+�ݔ�v��N����r�!N�񗧘���E��(�͘��*Bo��`��a��Leq�TUl�ĆH�Lʂ� �SI��,�ԮF�����!�:b�]�.?��wr,���Ze5�ؘT��J���m5M��/6�m���d�,�%��7;ʰU�O��ɕY.o��l}t����Ĝc��4�Kcl��8$�&N���Y��x�[m�R*��"Vyt�?ն8ae��g^��SU���~�.e���*5�ሴ$6�ϯ�H$5���7CC�����9�.-�N1|�2
��9Djĳ�S�x�rz���sP4�|�)�A�<�w�盎Ap�qȏ�������ɟ��wpU��M<~�p���Gj(ʪ�@���'�X�'�Tk��KjL��͔��jRC'V;��0Y�~�,帶	��xL:�2F���w<��"���jE�r$�F:3��ʻ���;"x:�o#*K��6Zs��5��g�m�Qr&(�$4Ttً�ĚB��y��#A�.䞠�	�AS�k];ȳb�:���Y�}����m������͜��D8<��p.b���kvN�u)}"�W��@��!��Y'؎��9��D̶�I!���Q��*��Z��Y`;O";�c�g����= �ş��ZH�M>�>>�TU���ˑ'�e�n˱O�=�������ԥ͹���Q3�hb��q�(��/�� W}�K�~�^ܪ���Ex��E0��|�d4AF_@&FЇ�8�E~�t�Kf����R�ؾQ��1"M���u�K�N,f44�I��d&4B�]!�EkB�\sQ�إ�Bb��C4�(�`�R	�<B�O��%��B�h���.�I^�(��}�Y_.)�����*L��i?��1nZ-'�\�3��7z��i����-�AY�nmȕ�cQ��/�&D�Jmz�I���^װ]�a$,�d���_|������Y��Q��r�Q]�
���r,X��<Kذbr���)��ĩ1-��
E��#�=m�Ss�P�Ø�1�gǴ2���m�.����cݸ`2V_^蠮�
��))F�I�Vk
��s�!����1����T�f�YM��n�<�.�k������T�.���n}�=6���ǜ��HW��w�vVaM�΂�dE�"�ț��θ�'!�C�Y3o���퉈˯%a�-�,Q֫Z{I���6c{6��S<�U��gd ���(��������Ě6.k�*1~���=�";ڹ��?���OwO�B}������|���͇����������,��v"��6���_���Ʋ���htZ�y��Z��{��mo�	�.�7��ad�
��jv����\��Z`���>���~��l��w%HlM�BZ��yᐰPC9�v֐�`$�/
&�9��x���Ep��!��7it����ۻ�wwSUĊ����J��Bu`O�p)s�O�ÅN��Ã��حq��΂���)��
�c����[�h~~��Տ��gq��N�P��Wy
l�1�,��M�b��Fh��z���h�L����|������G�j2TB�?K��"��.y�$2B���;���_���@N��;WE�U/�%�    �[�I�ӻ���S���o�G������xl�6ޢ!E�3�3��zs"�Z5�-��wE�QX�E��9+��j�X���U� �b;+�!w,of�^�EF�e�	�pNV���ɷ��I�R^tEw�αT�w�րJطϏ��~�?�C��w��.Ŗ��Agg�����a� ?mټ�?�*`�K֞f	�1L�W�a�st���6����&[��&��'ř���\-?�"l{N؈-�����IʥxՉ�7��z��cCKUIb���SIr%��0��J�iԝj�n_��8#�LC ���z�6�L�.e��h����mIY";�
�8JǟZ � x7$V�H�:�Ԓ݂#��Q_R�KO?U[����*"��6Y�d�����m!�ʁA��)0��UvqYi�Y��0C�13Sf&}��HH?���s��J��������o� ����]��\`[N����e�,LQ��'ѡ����%Ug^���d��	a,�cݎ��F�r�����X��P��]8�]RWّͥ�������Ѵ��TEl3·�J�x�����ԓ#�6-���S�(;�ٹ�E!+�OJ6W��:#^lx��P^�l���:C�,�����I� �IJTQa6S��6��)�)�FM�C5Y&U����P'���4(�'R&��"�H�N���>[���KlQ1��Q	��r�� �.���R�xf��,��6��YxpDY�]�	��H���T��*foOb�a���Y�Hl�Nh�!�F�t��n�iL�ݵ;s���VYMތb���z�MlJc��Dl��S�7���mNZ�`�����g����4Jv��ً&�]V�
l[�I9e��\*S�W#x�,������Nx+��59۵x�p�3�&2_giM@�3Ib�ItpX8O�����f7����H��Ĳ��r�M+��P������\y0
��y؜�N?Է�~�Al�!����
�ƹ_+&9~�l�~�b�w_��d3��^�냑�Q>$�U��@#\-7�v����@U�#)��
UĢc��}����C.U?������\.?"�:�p���S`Cޖ\�FKLE���us��fY6gLO06xF���>�,��S�F
����<��X?KT��7&^�{b��E/@�ͧ���>�CT��H�0�	H ��Đt�	�KWY�"�`�*�c�EW�B�s}�g^��G@�"n�Y�i�}D4��=b�em����jH��r9֪��)�J�N��:���.y7(f^��TψL��	�uƧ�߹�xHn��%c_�Pd�l�a�z2@Dd�> 雱��Iq�S_�v:�Ú3vi^��d�K�&��D5�{v�^�W�i U�oؗPT� 粿*GA�@�t��xs99�ByX����x�$A�AJ�rl�d�����«Z�UWavQ@vQmch��
gzpnNuD��{�����dK�.[P`�F��B�b����#���$
��}9���A���OǗ��ͷ�����Q��\��Gl��f�T��]�j
����ܖ�*t%m���V[�j��k: D���bj���m&�!���SDLH�g�u"T���2*,D�A���fl5����j���P?�4����_�<{HC����!�i�H���0$v1A�	��gj7|���U�[.o���dy�F��;x��AD�2R<������+�ұ?��A����.99R�&�f�� xs� ��ke�ba� �T�BbP:�ľ�,ZD�RdW�/D�Z�x�uV-�֍������~ʭ��i���n0)Bjj����d���D�Dv��`J��/^<��uH�W���{IΟKR�nv�E�P��R/�3a����롚��m���X��(���k���E,E2���Vwq,=3*��EʥK�M�L��L/v��
6̓Q�.ك��E���9���g5rR���]�Og~��!f�BY�A-CyEb�ʱXX+S�:�36 �e��.��W:��j�݄��+��_�4�K�Դ��d�:�~2b�&�L�@���L�+�!�Q�ÞS��/��2'FrB2u�ؤ��B2�$�
^�f�.W�����h���ӏ"5DQ���Lxf"�q��9�&}��W�j�6w7p�j�2b{����0��g)5��˩�HŴ7���)֐��s޴��n#�S�� �}L�#50�{*�tR�o�����1Sfc���6��A�h}�	�94�cM kT���w~�)���x����6��;�ܜ���:��G��!\��i���;�JSR���o�`���o���U(��Il�Q��p�b֎����QdrA�6j�+�$$-�$�u蕪��Ә,���g�+\�%�Hl�pW`�Adj�g��E*�MD�1�*HO��2a#�c<�y��c��!41�tK@�����vf��^>2O?(���o��i"2v�ih��u<��I�3��$�2*:S�j=��Z�A2���.}���sa��=#JEBV���%�x&��S�Q��㆏��eU�m�V�xb�BY/؁ �����m�!x��b�z�n�Bn��*�,�0��rBl�.rG2x���+���]�x���VL�m�?6�Ǟ(��˦�k
�r����X�$����#<幭�������*q�X���'
l�@�l4@@�Z���]B�pP�I���t�+�$�ء9�����qwʚ�.Lg��7�~gX�"bDg�\`�i's"�;����Y����*��Al}^D����,wl��HEAb;���������ME�=�2�q�(֕����e�#�F(7 �;��Ŏ{��
_�;����E�3kHvu�r��e�����p�upO�� ��[�]XY�'������a=6\ْE�`���亖~�?g�41+�羈��%*UֆLf�-r���&X��BH���b�*M�A�ŉܤ�!+����F��n9�v9�C�dʸ�������B{�uR[�V��ȓ&�}���ug�EZ5�ǹbmKHV����fmd6~df��ީc�B���ʌ�>N<s�h�Na�	=��+�:��ל���J̟wl�92�zþ�nm���U�l��~3��^s�W��ּ�J��4(�Hl'�x��6���8��
]i��4[�2cK[5�݈�ܡJ+�e�6�QH(eIt橈0X"'�}[u4u'�z�K��f����9� uQt�o�gk\��l��7^We/��LH�Bʡ�r]I��{�=�"�7FVƖZL�\Ey��K��\E90��(�	�K&7��}y�1y�5E
l�[�E�`bٹ��\�:׾&��<��|�$��)U��[����\�M]q�8A���V8��S�S(�:O��x�<��ǯ��%��[i��r�d!��+�\[:��n)@��oCɶ?=�?��`��fe1����l�,l�,?XVd�ƿ���P����(��=?��W�.�����)�uQx��ؘpx��f�?M1�/&<�_��#6&�oj*�8 �Ӗo.<S���"<2G��4�f#�׿���& �)B�	��K)	jP9SR�w]�%¨e�\��B�y;]`�9�<ENC�7�,l9=BAW����2GK�.X���c1a�4t�!&r<T��Pn�*�L���뜆(D�8��!"X��d�_`�R�'FMu������g�4B�b�:,j�J�=5���wA���UQ��z�2?.��q9�z_-b��h�Ć�{~��������I�E������l�И��1X�K��0�:��Ï�V�Lf����7g��0X��H�NB�.�>&`#i#��0��	;Wo]���GH�~�.��<�X��\б��:|%Wb�X�M��0��'�\bCt%6�Yɐ +Ild�����I6k���cQ��e"[�f¶�J��!u�I����MJCɍ�&e�A������F*O��݀�+/�JKb˧H9b�_��\�E}�,����v=6FrФ�L�+O^��d�1�Hlh��:+p����/���Ʌ�2Ċ��(c�{r�T8k�dZ��PY�ϲ�-A�+��rUk���ž�EC�t�/�5�)����    �A&���o��	�Į�$��Vͽ)dN5vڝd��'�7/�!�WN$�T��j�ڂ$�%"���F��*�����mN\f���$��"� �f"[�K<l��8-��Ct�wH�"������������,���?����8������"�H�g��.��Bh�hG]��������#����#wM<��Ķ����UuƎK��\�,�TA���}#Y�*M��v�4�%���")�Z]K@�o�v􃋤�r�eJ�̉Ih&�&W��h�#	9���K�N^,�r$�H�Y�KC��el_�Ƙ���QOu���r\�67p�YF�p�9�O�g���n�AIMtc߻񸯟��oBr��aMj^��Dd�JO����z�.���،�Qᙊ$�����=G驽��v��!8�n�S`1�"s��,N+�O��T�^n;�S�,Mb�!f3+����T�CbN��=�(����dpU�P:.j�Z��E�K��*FI���\`��e�n�U3���2�I?�W�*���F�-����r�yrcHx+��fU�T�v�^�����L�Õ&:\��M�R`#�c_+,z��a�d^ N��Q�gA���lc���LzQ�܇Dw}HHѮ��+�Mѩ	��p+E�m��_3~� &
+�K?���m�1CИ&��91�B 3�H:�.4ų����-�Xd�\�!t�af�n26B$\�S�3
��(�7�� �CC7{Qƶ�&�`))�4����tNe��x��/�a,�(�襆���3�d���z�V�X�R���Ԝ��ԡH��E�5�r��	�4���s4�t`���M�~URU���m�J�$n3r�v8��6�1@K�����:V&���B,�دnx��`)�CF?��T^�?ث�����N��	������'x���-d��кIw����f^�J���	55����tU���.��Rp�PX�Ad��G9��r�ϨV_�S�1Kb���Uܲ!!�c�\Glr����5��B�셍,�Ɨ��~w������&]��ohM�?�/\c flg*e08�ei�d6�ro�Y5��	
��[-��ېiJJ�f��#�R�������+î��RU��6}�9a����I$N��� |�r���z�GR�1�36��4k�?���q���>�SS��+��-0i��-бe�����J��x�Z(�a��R��2����5�%&*�C��͙�]2��J?Ήe��w�@�H��N^�S�q�NUY462{e;{�LZ&҆x��m��{�X�%�֘�ؠ�DPI毹���X���h��z�,�委yN:dMP�V�ش��&ۋ]�V�Wc7i��,s	?d S+V�`��ƨ`�KTX���&'�4��#uE��>u4����,N�0��)c�UF*x5p*\x�W���(L��qiIl+�Ow�-�����Vx
+�����C&ĚAb�;a�/[�˭�mV�k�X��t���'ﵞ57�Cc���Ip���B�Y��A8H�T�6r�H(���T,���շV�p�����l�A~����E~��]=?6�5������F}j�ޞb8�V��Ԏ%dr\`��3*��� ZN��������(�);��_��Y`��0�Z{3�s��p���oG����E�2*n�b��ѦĔ*`u�I�:�֎y��	X��(��۲e؝/�A�B��vDls�d�h²�`�*gmZy�,\�����2x)��W��E�`d9���I=�H�q���^Н2Uhi��[?�<�!>R���8$�,���|{w|�}:rWs�~"��@R�����aSI��Ԅ`��N�&TS�;��w>�~b���"ըË�'*)��!�!T�prp���/ ac+=�4qb����Y
�*��3�#��gI�҉k &��t>�����$�	��0����p��D�u΁���:f�M�ľ�n��{�vN�	(\��C�Fv����p7�C�u��'��	M�	�fBj���ʶ��{��jQ���P���]�U`c~Ø�,fγx9"��-P
��A�|^��X5ħ��P`��$���8&sv�ܑ����u:~�-����L���D-3Yɿj��&�(c�/���i�S"C{�cO�[h�"�� �3��9;��s�TG�Ґ%5$�*U��ve@��.p�9K���D4����A��jy��LA��SE~n����M�iK�%�c�H�KHJ��I���\.�<y�L�C<膆�5�_�-�$�'�`$C&��?���S��F��H�[ �Kvs�V��49ES0��w>l�\TY[�H�A�nm��T��)(���հ7�ғl��Cl��~b�	Q~�y
h�0���$�?8ot��$��XO����V�xE������ch��M�W`kX�p�h��INJ�
h�&&X/ݵ����z��ڐ£P�n�}�]h	+��1eyx;ǲ��R$Z�|�VcmR kH��r]�:(��$l[�̂���X�v,)H�UM��5��N
u-Y�c����-6([Ͳy
c59|Uh���ĉ��Y{*&l[z�bM�E/=���<l$���Bj3��s��a�mQZ�#����W�9r����Ui5�C
fL���uQ�:>�f>����ȡ���4�j�5�>b����#W''�!BWYtΰ��� �zba���A��Dl��i���%��X�����ݲ��*�S`�R�j��j�XfNp�g��0�KD�V��T���lF��<�I���e۪�v���OU7��9��(��e 	���L���j���4Σ�9��(\�C����o��7���;�m�H�i�%A�Oz�?c~�V�]5o)����9�U�����:�Y�]�AJOH�hWIhT�\���־�g��H����.�0���n� %���*�7A��P2�e(K	)��OWU��f�g(c�����y�X]ǌ]t���.5f�\�w�U�6v��V$ҍ.�����'(�hE��@�����C��`�S��c��"���{AT�Ǭ*�p�ըfg켩J�RDZ@x	��67���н�-K�E:�I��,��;wĻQ��BLf�AHOۄu�֑�<�"2N=a~�S�FZt��8[�r�8x!w�Zl-<5E�S�ϙ�����K���}8�^!rd4X�β؝9�����ҿM	�z<	�SB�x��c c��5Gn��t�54X�"�ؒk35jJGx�K
GS8*�����|�A��`!��/S��s�2~�g6�g?˔<yKl�\�ɧZ��t���a�b7�?U�I���~%�����]Jy�7�d�u"��ݢ�Ĳ�%�Yr����r��B�y~���T�ڊ����BZ�`����t��o�tc:Z���]y�j]�������ؽ̧lv�i~��jS������P���)���̸i��)G�'�˹N�=ٕ-��M�D�E}�N#�9:Hc�TQn����'m��N�@�p'�R��G�ܤ|��t�vy�j���a�VU���Ќ��B�`
<��������D��A(=11B�fdJ~�[ZT���mN�p�p#�'M�֗�-ŸӒ�M5�P*���C0��1=V͉D��n,���8O�f�(�����G�sI` �xҌ]Iol�������GP._»�Sq5KΎ���]�a�eeYnD�[XB7ڈ2���}3gK]���s$,<G���gbƖ���7Hr��Y�i��M���M�-/�Z��8p��9o~"�Ŝ좝�,y����� R ���K���	u�$@8}v(�.����NXп���M���3I��jQ�c��K�U�.k��=�'Q���">�[:Ă*���Cg�l��
N���m��B��מ��EY�w7ؒ|��q�Ն���E�`��M����]/;'I�-��U�c���N~\��f�ۢ�|Մ��z�������]=�;����B9
ņNk�s�*��V� ��u�j5؉פ qh�-��(��l�5ѡ����w����QZ1�`���wi�U��{���(�v�d�8�\��,�b�u��z���s�
7y�QG1    Ge�Wr�]*��#a.d;N!�b�|��6�9I���y]�LW�u�v鎵G[�z{�!�G��`�ԯt$��+�dx9,�$�H�;��Mj�Dξ8���*Fԯ�(�v>v�?�v�G(�É�����|y�Q��b�le$�#��B�HT�#������k�7�$k�����Yĺ��FT��4 �|#^����q�|c�+\t�"���^�z>�Ng��P|{�	���-9;�.��%B$�:�������ɶ����@�Td�Y��tǂ�Rp[�`
rm|gm�e��ʷ�JP� ������m&C��ʤE��1�ɴS2��g�>��m�`ظ����'�׳'� Ȳ�A.FN<TЍ"2	�?'�z�P�x�vT#%;�(�XD=��uq���qK�� .�Ѹ�� ��e�ary\�����ǥ�0�W�6���.q�-����������`�El(��YJ �x"rOm�E���-pI�1!��ș38�N�N���-�)��T��:1��l�W�m�ӂW�
�J��U�^��q�V'� s���bk���Y��BD�x9d�#!��&Q�����@��7
u!���ُ�@.�tj�b��WnƑn�P�:�z���b�v9�ӱ�-�P��R� ���Zh�и�0�c	Jb���2c_�P�Q��s��Ը����b6�BF&1��E˟�����b��&�d�䥊�k��lN�B���
��MQ�9)�;u�6��CqUg��"�ޝ�`_B(TBMJW�����20�J:�f��]�^s�P�ks�*;)����ƃl�F�G��Kɕ��'�
�6%M���,��">�<���p)+Q�aܢm�"Mm��)�� sNC��C��K}�d1�AxD+��)7C+�9���7��"���.�]�����`Ӏ�Y���:�n����n��>g-������[��VI).Ih���|��JTV|�0�=�}t@��.���.�-�'1����JPx���� ��	n�ȓQ#5��z���D�V��#��b�a7�.n[��Ms?����.�l�i�[FH?�BO���6�E�l�đ�2�Ů��T<�%i�@3��|�Zo��x��NF2���Md7� .�Kxi���k���Ƹ	���qJ\��� 6ϑ�u����Ƌ�����;k��Zf0Zq� y�;j^��!�����t.��� 웷������8�S�E�M���/Q��ɺ��F�2�i����ĳ0_���?t���aʠ@�l$+�V�Q�L�fc���P]�N�]g!+,�Y.��Gd�5e�:U���em@LWt����Е��H�C��߭��]YnA�q�v��]^5�B���D�7\F����]��+A��������)M�v��;�.�����"~�������� a7h�JC����"tI!v���,��[D���~C�)��x�f켁aZ��yh��:�G��d��"Yi��1��=5�"U���tH��ks���b�lte!|O����F)��i� ��/\���WZ�bZY�������E�
�;���v��Cjb��dl�Z�IΤ֜��i+5儀��-��C��u�=���Ӱ�W��b��Ch�	CQ�m8�䬄�E�3x�8Z��뻨�3�����0��-t�)�N ���,�7H3%��� T�O����_�j�.YB:��;;x)W�>\��T�����Ȋ�Ο��5�b�vڴ����T
�h�����ѡ-����9������;!�4���I�Ͽ|~[.5pzC:(}���`�,V��Xloy��oɱW�e)���0�^�,��I����d!D�z�9��C��x�4�2J�"��{�9��W��FOEƎG	�
U��Ƥ�p�M]�C[G�M:	�_��i���R^�(���5nGC����k�E��hr�glouO�����Թ{���U�Ͽ��;wҏ�l��������o���<K{Ә��)��M>�ۥ�Z����Ө�|~�zw̔�>5�*�����RJ���r�,�H�?(��o�����lR5�#�L}qM�)�C�X�5���Ѳ�+7"��Y��8����������	��ŗ�5nb��g���ͪ�8��t�Ɍ-r����-A�U7"�۝�H">J�}K�궨���
)�.�0�֒p�,��Xl�h*$a���3�1$���w�%������VI��d6�����C��^9�'v)ExXL��z�}R4W�j�i�0�[;O����芖5ؕ���� v^�O���G�+]6ؚ#ڕ�0�X���<NVc&�u9x�V��'�w�x���Wߢ��|x�������vEzlOO�������0�]o��&��vm�娤��)*�б���`]���*>v�����L���ʎޛ���� �:7N�ئyOݑ@mp;P�]�7�c��X,P�Ů#��,v�EU.a.M�k�F��ϟ�}z~�����?�@4��҇��~����u4H��,:+\�Q�]��\�4��%�w�qE��8�8��{��{��n^e����([P�ӯ������
;f������m�v��_%ECB��D(Y>{��9{����;�j�|C�+�Ky�ƅ�|��݇����lx���������Ŗ(A����SYB��D)*����;�tc�,ɷ�-���3�+'�����g�>�6��Rv�L5L�Q�,	5y��~�~\����b�lD.�i�`�����2��^��㿄Δߎ�U[�`Wn�B(ihʄ�Z*�r�O���1Fح�TW2�%�rCI��I�H�u��K�J(���eF�ad���-�`o�YK� �˰����&��=��͓��Ct�����T���"���6�"�����_�E4�I]�r0�pH�������Z�`�:s��`���%�s֙�B��^�*U�4l���W�z 
�Y�]f�q�h��q��u_���V�������nsR���H�C$U��D��f�k��æ���CEU*5�?I\sT��3W�#٣N[�;|dl�q��V3��eMA%�ݼ65�Gj��eXZ=�t���%|D�#�[�׷���6Z3������t��ݽaӻ)�R��2�+��<�e�`g�~��ڿ�dS�[�s+�@!6�A���8�b�Yl7���!!R�+fGO���@��i�?
j�5�q�U�M���r_�G%�r�m�n�?6�D�ǩ�VST��8e�펔kF��H�
-��$���iv;�Y�9ldփzM��;�)�L��_����fF���Ī*�)���[C�5��΄�*T>Yꭚ�sOB�e8}�D+�Y��b�K��,����e��3��L�^O.L|�}����k~4��&������\3M��n�g<����iy�*��#_��x��L�dW������ƛA���lR��T;K1��pxL>�k7ؙ��7i�4�վ�+��]�i�-�
�WB0I����'(Ϟ��H# �-}��Q�Z��y��d���>�����GWu�Q>;<�x2el���<���o�?���nHΨ����2�P��=����~|�v��X�9yR��r����D�BPk:�``Y���i���oi��m�)��|�{s6�3��fX�|�\&������ؒ�؊��`0#�[�[��p 3�˞h�%J�$M��fJq�9��
���?I���R�]`�-Mm�$%͌R���éSf�F2�����F��Cy]@�'�5�m�Ɍ^��y�i_�v�|U�ưW	��3�A2��aה�E�#��m�]��y*}�����9GV֛&�l� �_�Y@lݠ__:1�3���2+d1���E���e�������8��~�	rsPD�ߑ�/$$GC��4ؙ�����z(0ΫD�ߐfoC��r'Ĉ�_�i��r��\0LQm�[��n1V�_Fg�p.�	#�\v�@`6_�˅\2�X�@�[��G�x���;3v�-P�^����� ���mO����v�V,��J�t-VeO�U,Y������S����YBFSʹCE*��+�t�,�"��])��1���X����>�ώx�]��,^U�Q��2���    @-RK���-������xy#ߨ�,��bK�6R$�G�^1��x~����~c^��7n #�p�l����ʼ��G��]��Mxy#�73#��7XW��H�M�p�nGg{�W!�ķ���Ý��v�Z�|�+��I��}ϩ��I�]�c�-�ry�mT��M�N���Hl�,�v���T�M.�_�z��^H�Uո�	[X~E9�iܷ��K���\9����.�i9��ץ�5�|�cvE+��Crc�q���M�/�)C��;|�I��_��8Q�O�iF[]���גu�;�z�@� th>w�|q��]��C��昝}�stDm��w\FW?�����N\�~t����H�gtHniXl߫���.b��!��!x[�"nTV^�����.8`���)�}ʣ�?�m�����F���f�`���'�Vxֽy�� {�?���������\���[֟H	2+�2�Ůo:&�e�)����V��nxn����`��0��y�LS����3՟��,#5*��X�;�ܝh���5����s9�=�2,/N94��J,����C�����`�G��MP�ئ 6&ϔ9z� ����4S_��D\(Q}�DX&J���01���j\�Q1]�O��P����oB�"����j_\��o�l*�j���]m����&��V�k��o���s�>%��lKJ�Q�]\�5`W�W=�QQ�aPM�gXz�+��`7�h��.-����=��k4gd��p��IL�e&���l�/d�x�UP�V�q��지�K1*B{���K$����=�cx+�
�,c��L�J��8H����M�h��9<Hj�Ŋ�q��	�"���Xl��b����u��8]dvX�UMo�s#a�� ����t��V)�2��	f}HH��"!Ʒ)-�:,���5=89N���b�-ىAh�l���P����s�s�=8��~W��Zl͑>̔��E��-vf��T6���>X305��;�lN�-��?�������w�~y�������m�W�����|����O���?�9�6���v2I��|�$��*��]n�_��o�N��1=Y��P�K67�Ŋ7�BB�EW��ŎKݔ�22�����z��'Q����ï)�ڇ_���m�P]�������w�4��4|��b�4L����/�x��C��L�b�<��?��Gsɏ���w�kT�Ya��t,����t�>�$���j�C��{)�R���9w�K:P�J���M���~9a��dT�\o�L�>���$ڨ���bGt��.i�jS��â�D׈�`����۟�7�\�p���m�cCe�H�����&�obc[�D�-�VU�a�6w��PL�v,v�"il�����V[Z/���g��J�ݨ0.��E�Ŗ�J�wjѢt5�L[�n���h?nA������b�+�tI�&�����-\Tгh���F"ߕd�RQۙ��E
��U�bKL� �{��2��:��<����bK\l�N��Ag�Gy=9���G6�b�g;V!~>-��QA��O&>z!��>;����]�����5�0*����?�b�|̸�	��GU>�q������WU`�����P�Du�� �Х�7c�yz�h{֚�p���A4�YUt"�9r:��w[%�̈�(G"�ZsX�])�^إ� 
�)�"��Il[7<%2v�,]=I�LY-
��{��*�أN�SF�=6Eˆ�%}ן�j(o�b��$E����x�W?����%��Mi�����L7pԇ8��glH锋��Z)�9R�p�ڰ[l����EUC��#!r�f<K�z>E�m�����z����ś�����@IXUQ�b��!��r������t�vk�V�xcH�IH~7.��{��l������u�<qT�AI�M�q����]/��BR��`�'8�l�Jft���A�G}.cg1]e!��3�ďޥy�N����U��Ӑ�J7�����;� ��r�ԗ�m��BY�D
��J�X�^q&�3.8����
ڔ�]7^ ��݆wO&�)%��U��\R(p���iB��xF����9�UA*�M�C��6�'�`��U�����
-%�_N��հ�V
v��v��wj�d/��OA�V�;Q���>��q_A�=��5�Z����U�%b.�E,��Xli�ѫ2s�J�5���K�8ji�t�^���?1�c!cK����N�p��as˲���+����'��S��ݖ�γ��FN�˔
��2����C����j���T�C:�r吞�S%X����ݚ�v��M�Z�(�&-W�\��G�����R�a��t�q�d6.���0>2v��Yu�<i��r�f�x��	[���2��P���݀M��x6�,\U���\�!��F��d���8mq���x^���LƖ)�Ja8��t� �j��fl�T�7�'�
�Dī3}v(���D�-S0�B�Bʈ������󁁡Z�ڔ�K	�%,
�5����6R����c��C��������`�%Z����B��2�l~L����@���A����q�i�E휫����&B�铱�jm�>Z��:�'�@�;Sv��G�ܨ�el/�bkr���,���CB3]	���5>���d���T�@*{��<v��v����9hU�$�6����<!���`��c�ȟ�~�9��Kϥ�L��V�S&n�L�is^��3ݱ	$�)�>����d]�)v���h_z�9B��p��ڍ�@W��ź\w���� �y������_���:�gc7�Νa�]'��nYFq�e�1�F�v`d�w�|INx�b��Ǜ��!�p�*��냰�EK��d�߮��Ј�*�wG|�N�;�b�8�P�Lu��u��|(�m뭀���V�T�瀰��Əq��g�,���Q���`��_d�a�;�~/���P���8�?;�M�P�b�3�=Y���V��Qن�z�B�>���ّn;�g�-H�]c���z@R-i��?;�]��`��m �������G��M�h�U�w[w�6�j��h�<�)R"EK����S�}X�T��Reiw>�\�);�i��&ɪ/Ih�J�,�:/T����P�Pa�1��;�9�I�x�lA8ds�!=ڪny)o��ԛC����[�T�D5#%�T�z�ro�̈́�N<gV�M����q}����6���n��-?�<*S�t��P9S8o�o��18�m�Tk[���%|-�*��慄T&���`�|d�`�%'�*0��B=�����v�����������?�>x����>��û�
����]��B��}Q�c�H>���=`D��Mʗߔp�o,� <%  �zє��'%a�$;���-H�ʰً'-޵ǅa����j�-�d��Fu#*���t�Vj�u�J׵*�����`��uߔ����	?B�6��o?~~��y>^�v�d�5^����8�cy�����n�����S=�4�:V��P�I��%I_���!d��uC�(�y1XWO,;K�f7T}CѬ��~��K~Cl>p����{�Ǭ�.h�,Q�V<��qev%:�r��K�y������.�3�6��Օ����h���S|(�Ճ��`;�.OV��FQ�$�1B=�X	���t�՝x��YR0��~8FiF����(��VW��.Q�"U�������1��
v%�>��lL�ƭ��]f����w�� z��5Ѻ�8�b��J&U�p׾��ʻj���bg���ʁ��i�����C��`�N�B�@pa6�ȟ帨T�^��D��՚�$57���'-�Q�6&�Uو1�h���F�;|t�#�t�E2�+=�ׄ�!c*�f���"�ڢ�-v���D��>h�������%��b��дy���g��$��'!aw(�:H|tX���I�]>�S������C/R��2A�-J�R�r����A��i���a��S�FXn���-=�b/�d�Sj�@�؊]�/�e&JO��"��T�L�]?�H�"��eD�҈=_\�][%�I\%�D�!	KCb�NR-v���Lt]�!/    Ta�6����(ݍ&��bd*#�0"*�3}3�k���v�۔�@��y<� b_�Ҟū6���� �Yg��-�I�v�'Mni-�g��#|Q9�DP��%34�}b=W։��r�mՌ?Sl��p+��~b���MH�ưM�50OM�yL�8K�U8;Z�'�~�)�in��N
>'�}�b�-Q��9���cH���6�]�y&0An �6N
*%�����u�9t�)��ݬp\&�)	�Gk\�N_��햔¸�f
I6?+0���A�qDA+LZ��F�n�bԨg�ꛘ�@qdR}��x���po�� �؝;��T��"���si^,CI8��e��0�d��B	���X܎"o$�1K���LvN�&8�d�nؕ+ِ��B�U���E&����3��Y�jm�s8�k� ���g���Ji4ؾA�Bz������^�0�G-:c'�L���M}�W�e�,\�p&��� �n}9;��˾�<0����k=$f	�8��,"�p
�| ǩ�9d��1�q;^��1N�|���}ˮ����+�����sO?ޞo��`�[#�� ˾�=���C�c�ׅ*noE��Ɨ9�e]���m�c�؝����b��dy��4c�a&���ZT$vl|��B��.ˋ(O�g(%*��jE6�g�*�Y�>��կ��+?l�jT��d~sht�
[/�^���ѣ�]���
����!v�zz�)�$���3s�b�RNi�W�۔(�,UC�Z�3s�vF��_��/"�*!5�M�j��O��[��Pc�'�b��S0��踡�
m��W?ĎN��53Qt7�&3�F��C^W�=�o�[ZdR�~k=et��6�z�Uf�s;q����$�Rf�-�*tF{KC'Ѹ>�}{��p�����blq�;�D�t�v�i�N�x�K�8*5��;z�2�;]]	
����[�Y�(��/3���20@6~#���Eѓy:c��10
F��ڬ���]#c���b�iT?�l���e��dt�ɫ�2yQ�������m�l��O��$��N��xUJ���q lW�(y��l ���l�~�l;v���H� ��p�8��qPchU��1uuz�C�>�З�.Ǽ����ԕ��6�	LW>�`���웎f����"�L.��]��J��ts�tM4ks�G�kƮ�p\��tn���� ��xs�]��e3qDH���ɈѾ��kA�i�
)]�1z����G��5�*d@�^k�SE/=U��/x�N��*�vu��q�(�@~v�OV׌]�X2��º�h�>.�DR¬gv*9Tɾ)��,�~���s�bg���k�:Q�?�@:�pmd�T�(r�6δY��f|e�❕&?�E���|�2nL��2Ռ�d��Ə����`��FuKdi`�xe�Q�h�5�
A6ʣ�O%*lC�FƮA��7:�aS cj��pR-�*�լl��Z#M�\�p��֒�!����)T�O}�&W�@n��!�L(uM��49|;����6��s���^k��4N���=��Fn`��̨�@�V<nt*NV#&F��t�N��KÒHP���;r�T�1aW����=��7�Jו>o���Te'wؕ�R�@M�F�C��$�(�����f�lc�{�nҕ���و�� �S��(�P��Nb����ؕTq��L����|�Ԭ]{ː3�� >%�����T,==�P2��4y׽c�	�A�������K��%$����%l}Vr>ͩ��%R��mV<��@f�@�����5��ȇ>s&�̙`1V+���#���5(��G����|$5=�$������8Zx���� RM�t��Y�� +��!�9cW)Xy�8Ch���l�l����g�gw/?��e��g�J����Q��e LH�y犣�}�1F:��4��i�W�=��^��F�PƖgZ5!%��0�ڸ��^��C�b�3\�A>%�B�4V�g��H��3v���4�7�c��5bS�`�.�m��K�K�6_z����^���D�qWvk���q:����f����\����u����f�jnUaq9f�j�8�2���y�v���LD���Χ�by�� �BJv�T��P����_7:12v���,Tc�0���}��[�@���R�r�S�ؽp�����lz���7x�ӷ�2\�I#�W�g8~2��M���[M���#*�Og6���R��!m��.���@���qQ����#�3
��ק�XPuY�֖��)Q-�tkD=��d�ta_���C���M�S=�6h����=C?�J)�2[�(��?{��T���h���1��D���jG��dk����N[�f�-Q�퐨M���1r'�|t*Qh�%B�Y+U0�kV�R�hM���և43ˀ���+F��l������ԔT��ltJg�%���o�'��r*ت.�$YW�M�$W�&���@�%T���|�p���Y�PZ��,��
CH�P�d�#��Wb��L����<F8��t�Hʤ�R�-���|����<:�$nZ���zJX�p��2FT��Ȓ���Oɋ,�}�ǭC�`^���K������0 W`�k\�����༓+a!�5N٦ةz+�#!�uWs��[��*/��­(��#������#��G"���-]GK�y�0qTz]������7a{u�U�1��A5��K���4؉t�����'Z�@�rh뇡��<n��ٹ��j��	:W�2PdH�z��ϒ��������vb�j�� qK~��RH��!���i�\�����P�q�b{�T��snL\e�ǯ
�q������z2��iZ��ώP�=�����/�a�-z�-����fV���:e`������U�����Q[������8Tx��Q�)� ��T4؉XW�B�T3���\�X����2��7��V�����)
��%�x"a}?1n�.ެ�ڒ������)r�S���$�Sy��ClkDl�j������� W^��X�ڗ|o���J���Fl�� ��F��t)Q�E��e�&)���R������=F��x2�X�~?�(�J�"	��$||��&_tR`�c9���N��4�i���l�,X
�ũP���7��Rй2V�<�se�u�ڳ؉tS�+�V�_K���jT;o�x͸D�v�!Q�s�6؟������|��(C�2H����Ęx"t�]���oԩb��� ��ɜI�9�G�Q��ҌFF؊�6G��L<Jr�J?j,���l�$�����F��ȡ��F�:U\��-�/!z��؂��VTzt|�@����a%�xB	�w� ����X�1��DU��o/����&��]~�;i���R>�"=Q��灞`�%^��=ل���S�.�S�o3FfC�,��byc�9h�ƾYs�H�m��Q�vlQ���j�Q3G��}H�뻚Q`%6�,6*�{=b'UZ��aG��K8t}T��[p �['c8�Lu�`p�|���;�OmS�N�.>s,�H��h�3բJI����C�vn�'�6�k�D9V�֘�,��PƖ�KT)|N��ʹnP0-|��2��ե���5�wIr@)��ዒ�\xQ)��m�)he4afEt���ztfd�K�@��[q��s��O�l��FV6m��&D�T�Ͱ�}B����6>j|@g�,ve���jHw�?���(|���o� W�'}xnz�
��цwE�����7��%�F��m����O���������7[�I
k3���4�<׹�.ή����;�$�� ����ʔN�"���0*Q�=�J��[ǎ�
�~S�b�\hTRgc�U��������N| =�>;|�h���"��c�����v�O|�����a"�����IzZm"�j�9n�\�.��1�XtU�������f��%G�A�v��뙒�WN���N,C�X�����&��9`����J��*�&h�ћ��
�u�J�2�;pM���R]g��>����I�8��2v�T:g4�}�eI��`5��2v����8u=>X!�k��D��!}8���[��6.����8Ϟ'��<1�p��o 1  �p7�%��n���g{�d���`���u�HA��X�4$�iY�B���A_�LV�l�gg����g�܁����@�R��J%����4�y2f��v����t��/�?�lA�k�P�㠴�/O�u�G\KJ�zធ���m!Y��s@�QPm��N�8@�C|�r�O�I�ֺ���5�8Yv�V�6H ޡ��.1��nQ��ޱä���JJH�ߣ��5؍fe�,��s�#n�C�Y���`_D(��:��}&x>�#X�$Y?A]
��{:恙���e١S��\vܘ��}-��;h�m������EC:�.;kP��9)��5�tŋG�ۏ��@@3��@;C�t���<ig`�����n���ߤ�b9΄i��������
��6�!c���&�o���޿�g-��D�UZ���F�WCC/� �O�>���%?=���}���}5����jrL�b�5~��thq@�3^�5B��u�6�����&E�*�b�U�uQ�Q(��P6{a��=��0"���9�$�`w��@���5؉�}�t���o�E�{�4؊t��ۭ94���	RB��`g�ӑ�dQ��5�C��;l6墠��n����yEq�f\u�],O��9�� �8r^a�ը^��* ��5�z��5�l��ڎI;�*���LkH�1;�+u�6aǯ�������p#��
yx	��� �D����!z�g��ک�׏KK���Ƿ� JBtF���f7�:U	88�j���A����<՚��r�y��e�֋d��j��Ȥ�|X^d����v���t�)���"˰�w���Fl�`PϿ|�%
����>A⬍��e���'cG�gO��u.�쵐'�R�M���/�gR�k�X��F�kd<Dҫˊ}#��������f}T�)�������6���z
e�H��g�T�_��a�x�Ӏ���J���Ӛ#��^Ѳ�l�eN�TC+�q��k��*��{Y�]#���=�Q���LXh0M�.��kI�46ț�&[ 3�HZ�ϱ����>���'�[QP�!���+礰�_�ý`W�-�m��u��7;ס�1�ר���5�UR4�p�@6)šM�~D�ט�>Nʛ��j�A���-.�n��73B|Z*�A�N/cƈ���ۭ�td�'������\<�X�0r�cWb�X�bˁF���JYbiB�΀$(4�h�GI� 
��2�]���jb��p+^��,X��Ȥ���V��N�VH���a��rh۸K�L�y�q��)KF��/T��NP�K��Y%>�:����t8D3vc�t9ׄB��֑�2v��=�:\��AN����Ċ[`
��]]�
9�P���@�1xQ��&��f�\�<��b�ܪ&����Xn�?F*t����`b�̃\�V	�j)&�����XX��b�ԟG��#2-:�d��d���X���U2Dǀ��,����E�7�M6��ѳq9���bF�M�>�^��(��ˬ�N�!���)��*�ԝY��a:h���h���^���l�ؙk�K�l:�, [� ���y��߉�/��r=vN&���D�1d2��
]7W�������)�'1@�k���o��;Y"L�Rc�,����>k؏?;�=#�/Ɇ��Z�.��j!�dLw$����ة�d�k�"�l�dLy���2�&lM���eSM��`/�DM5豾���m)������o*W�������:����@t�@���j	�S�@	%��eO؉�#}Ik��s�����]<��YZ���e���͇M<�'Yr"��Z߼�����c_#��_�	3�	�=�I���O�1�,��`�~,��^��2yB̊ٶ4Z���J�;��FO�O���.�Q�{�8�WKI#g�.�4��g��.�{���϶*_�Ǡ)U�ء���y�=��7sH�w�	'��f�,�a�5�\Ѯ\ѐ��O��b;?jэ���tEoOc�f����!���1P5������-�O {m�8��"m������P����L�s��@JA�Z���%UFȴE\Ue\�a�mW�8Q{�&m.c��:�Nv��$x�w�Q���с�ʥ�U�r�ju(��;P��#�ٿ���5:�;ա��6.:��*W�I�_^6wOv�Ma���kO}c�'�l�/���ve�*�',�$��B���.$��O,�]#R��hun�Lf8+2v���dL���10Y(4Õ,�W�U�RD��0e/���CU�e�]�A1A��<��苒���BRZL�p=^��Dh�Z!�c"(8s�e�7٦�I����2���D-l�������c�ʝ�bw����#�Dgg%��}��WiP�$x����e�b�P�%CY������[��z��p$%%�D�N��ڟ^ֶ+�c�&%�ݔ�e�n�nzw�H=�	a�i�J���/
��Ӽ°�����|v��V�fF����4ޙ�w�����\�o`,�?z����=&��Tl��K��WP�j�?�Fho���b=v{��W�c#���Cx�oƎ-&̖e9b:�.�o�
���+���yx5B�Wh��f0�l8��젢M��n0�6��3v��h�Z�ֳ����"�M������c{l��g����L�8,)��^�aH�Ht��N���"��9���LTeR�Y����x@w-}�r��%F�u�.Ъ9xqx3��D�+j�Am���A���9���}nG���3//Pב�9����Fj����=v�����y�S��a[�bi�a��7�G8U=��5�C�䑣����7�����      �   I  x��VY��H}�7��J��4��؂(*�IK��J����v�uO��
		V�s�|�;�1'(`T����q�"�_��U�����+i�-U!VQ^���Hd�f4�!��;9�ہ�e�I��>K��Z��*t��`0�9	��SGe�GQ��GaPw�,V����'˅�M�p,���<��PG��nی�w&�l�o&�!��6�0��z���0]6��cff�	�Z �A�5Z��G��\I�W!�-xa�������������`����r�C���R}Z�O�}ׯ���)S^x�F����|CS#�4?K�I��+I��'A� ��<d��ш�& �.����*H�G"��j�S�a�����搬��2��y�e�[�<_5-��uj�J��D �>��QO"�p�o: [@��3+�bq���r����N�eڈ��2�L��=�Ma����F᜜�h��`�U�my5ǅ�)��)����ta�VH<�E��&���),���d��"%�Ć+w灭6KN0�ao���B<Ֆ.�N#�śm>�-ܻ�F�����)P�H�gܙ���*�ejA��R�5Piht��D�����q�7S7j�=/�y�����}�6 ���S����	��](SI �yN����nzR݋O� ���#Y���H���Ap�P8�>|H�����?������-���� 	J��E�\�E߼U�<͙Z��nh8�3i&=5)Vj��Ğ��h�9i��C.�[}��a��e�څ�V�O�T���n͠�q�f�m�Z��yV�2c�:m��%��j�zIȾס�WR�&ݕ���u�nr�}Ct�a����
�Lp��}��g��Pl�;w�Y��t�6U���s�=E�ժ��3c�jJC""V��$�O�|���m�쿽��;=����[���|h��;A=�T�&$'��)
� ��ԡ����|�� `? �>�\��ŧ>7~4�~���y"�Zmڛd"�EF�ˆ@y��GݩB��;q�ꤧ�2�y�9�/�ק��J�
`wړ��3��ĔѸA�؍ʞ++�E�3C�?H��>��`k���b&:�{�rgZR��
�}W��ѻ|�~�c��i�o(��(���f@`��$�!�!��j��1X��r]��(;�Yv�t[���m*���9$�σ]�~�".�r-Ӓ��'9ǟ�)$�.w�ę?�s�H�`d��Q(����q������=�/	rm���qF#N"�)8��h��43�1�T��\�I���m*%�`�� R���{x�.�1~>4�i�W�v�joV���crE5�L�4��Ra�l��ͼ����,�n{?�>Ic(�OHt���������/��     